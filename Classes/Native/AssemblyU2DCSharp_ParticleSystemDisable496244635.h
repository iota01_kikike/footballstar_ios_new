﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EZObjectPools_PooledObject2079175126.h"

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleSystemDisable
struct  ParticleSystemDisable_t496244635  : public PooledObject_t2079175126
{
public:
	// UnityEngine.ParticleSystem ParticleSystemDisable::Particles
	ParticleSystem_t3394631041 * ___Particles_3;

public:
	inline static int32_t get_offset_of_Particles_3() { return static_cast<int32_t>(offsetof(ParticleSystemDisable_t496244635, ___Particles_3)); }
	inline ParticleSystem_t3394631041 * get_Particles_3() const { return ___Particles_3; }
	inline ParticleSystem_t3394631041 ** get_address_of_Particles_3() { return &___Particles_3; }
	inline void set_Particles_3(ParticleSystem_t3394631041 * value)
	{
		___Particles_3 = value;
		Il2CppCodeGenWriteBarrier(&___Particles_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
