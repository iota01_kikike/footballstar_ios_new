﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"

// UnityEngine.ChannelPurchase.IPurchaseListener
struct IPurchaseListener_t2367674166;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ChannelPurchase.PurchaseForwardCallback
struct  PurchaseForwardCallback_t144617755  : public AndroidJavaProxy_t4274989947
{
public:
	// UnityEngine.ChannelPurchase.IPurchaseListener UnityEngine.ChannelPurchase.PurchaseForwardCallback::purchaseListener
	Il2CppObject * ___purchaseListener_1;

public:
	inline static int32_t get_offset_of_purchaseListener_1() { return static_cast<int32_t>(offsetof(PurchaseForwardCallback_t144617755, ___purchaseListener_1)); }
	inline Il2CppObject * get_purchaseListener_1() const { return ___purchaseListener_1; }
	inline Il2CppObject ** get_address_of_purchaseListener_1() { return &___purchaseListener_1; }
	inline void set_purchaseListener_1(Il2CppObject * value)
	{
		___purchaseListener_1 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseListener_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
