﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"

// UnityEngine.Purchasing.TizenStoreImpl
struct TizenStoreImpl_t274247241;
// UnityEngine.Purchasing.INativeTizenStore
struct INativeTizenStore_t513596045;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TizenStoreImpl
struct  TizenStoreImpl_t274247241  : public JSONStore_t1890359403
{
public:
	// UnityEngine.Purchasing.INativeTizenStore UnityEngine.Purchasing.TizenStoreImpl::m_Native
	Il2CppObject * ___m_Native_23;

public:
	inline static int32_t get_offset_of_m_Native_23() { return static_cast<int32_t>(offsetof(TizenStoreImpl_t274247241, ___m_Native_23)); }
	inline Il2CppObject * get_m_Native_23() const { return ___m_Native_23; }
	inline Il2CppObject ** get_address_of_m_Native_23() { return &___m_Native_23; }
	inline void set_m_Native_23(Il2CppObject * value)
	{
		___m_Native_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_Native_23, value);
	}
};

struct TizenStoreImpl_t274247241_StaticFields
{
public:
	// UnityEngine.Purchasing.TizenStoreImpl UnityEngine.Purchasing.TizenStoreImpl::instance
	TizenStoreImpl_t274247241 * ___instance_22;

public:
	inline static int32_t get_offset_of_instance_22() { return static_cast<int32_t>(offsetof(TizenStoreImpl_t274247241_StaticFields, ___instance_22)); }
	inline TizenStoreImpl_t274247241 * get_instance_22() const { return ___instance_22; }
	inline TizenStoreImpl_t274247241 ** get_address_of_instance_22() { return &___instance_22; }
	inline void set_instance_22(TizenStoreImpl_t274247241 * value)
	{
		___instance_22 = value;
		Il2CppCodeGenWriteBarrier(&___instance_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
