﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// LogoLayer/ActDelegate
struct ActDelegate_t1046046768;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogoLayer
struct  LogoLayer_t680261240  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image LogoLayer::logo
	Image_t2042527209 * ___logo_2;
	// LogoLayer/ActDelegate LogoLayer::OnActLogo
	ActDelegate_t1046046768 * ___OnActLogo_3;

public:
	inline static int32_t get_offset_of_logo_2() { return static_cast<int32_t>(offsetof(LogoLayer_t680261240, ___logo_2)); }
	inline Image_t2042527209 * get_logo_2() const { return ___logo_2; }
	inline Image_t2042527209 ** get_address_of_logo_2() { return &___logo_2; }
	inline void set_logo_2(Image_t2042527209 * value)
	{
		___logo_2 = value;
		Il2CppCodeGenWriteBarrier(&___logo_2, value);
	}

	inline static int32_t get_offset_of_OnActLogo_3() { return static_cast<int32_t>(offsetof(LogoLayer_t680261240, ___OnActLogo_3)); }
	inline ActDelegate_t1046046768 * get_OnActLogo_3() const { return ___OnActLogo_3; }
	inline ActDelegate_t1046046768 ** get_address_of_OnActLogo_3() { return &___OnActLogo_3; }
	inline void set_OnActLogo_3(ActDelegate_t1046046768 * value)
	{
		___OnActLogo_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnActLogo_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
