﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// UnityEngine.Purchasing.ProfileData
struct ProfileData_t3353328249;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProfileData
struct  ProfileData_t3353328249  : public Il2CppObject
{
public:
	// System.String UnityEngine.Purchasing.ProfileData::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.ProfileData::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_2;
	// System.UInt64 UnityEngine.Purchasing.ProfileData::<SessionId>k__BackingField
	uint64_t ___U3CSessionIdU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.ProfileData::<Platform>k__BackingField
	String_t* ___U3CPlatformU3Ek__BackingField_4;
	// System.Int32 UnityEngine.Purchasing.ProfileData::<PlatformId>k__BackingField
	int32_t ___U3CPlatformIdU3Ek__BackingField_5;
	// System.String UnityEngine.Purchasing.ProfileData::<SdkVer>k__BackingField
	String_t* ___U3CSdkVerU3Ek__BackingField_6;
	// System.String UnityEngine.Purchasing.ProfileData::<DeviceId>k__BackingField
	String_t* ___U3CDeviceIdU3Ek__BackingField_7;
	// System.String UnityEngine.Purchasing.ProfileData::<BuildGUID>k__BackingField
	String_t* ___U3CBuildGUIDU3Ek__BackingField_8;
	// System.String UnityEngine.Purchasing.ProfileData::<IapVer>k__BackingField
	String_t* ___U3CIapVerU3Ek__BackingField_9;
	// System.String UnityEngine.Purchasing.ProfileData::<AdsGamerToken>k__BackingField
	String_t* ___U3CAdsGamerTokenU3Ek__BackingField_10;
	// System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::<TrackingOptOut>k__BackingField
	Nullable_1_t2088641033  ___U3CTrackingOptOutU3Ek__BackingField_11;
	// System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::<AdsABGroup>k__BackingField
	Nullable_1_t334943763  ___U3CAdsABGroupU3Ek__BackingField_12;
	// System.String UnityEngine.Purchasing.ProfileData::<AdsGameId>k__BackingField
	String_t* ___U3CAdsGameIdU3Ek__BackingField_13;
	// System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::<StoreABGroup>k__BackingField
	Nullable_1_t334943763  ___U3CStoreABGroupU3Ek__BackingField_14;
	// System.String UnityEngine.Purchasing.ProfileData::<CatalogId>k__BackingField
	String_t* ___U3CCatalogIdU3Ek__BackingField_15;
	// System.String UnityEngine.Purchasing.ProfileData::<MonetizationId>k__BackingField
	String_t* ___U3CMonetizationIdU3Ek__BackingField_16;
	// System.String UnityEngine.Purchasing.ProfileData::<StoreName>k__BackingField
	String_t* ___U3CStoreNameU3Ek__BackingField_17;
	// System.String UnityEngine.Purchasing.ProfileData::<GameVersion>k__BackingField
	String_t* ___U3CGameVersionU3Ek__BackingField_18;
	// System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::<StoreTestEnabled>k__BackingField
	Nullable_1_t2088641033  ___U3CStoreTestEnabledU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CAppIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_1() const { return ___U3CAppIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_1() { return &___U3CAppIdU3Ek__BackingField_1; }
	inline void set_U3CAppIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAppIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CUserIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_2() const { return ___U3CUserIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_2() { return &___U3CUserIdU3Ek__BackingField_2; }
	inline void set_U3CUserIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CSessionIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CSessionIdU3Ek__BackingField_3)); }
	inline uint64_t get_U3CSessionIdU3Ek__BackingField_3() const { return ___U3CSessionIdU3Ek__BackingField_3; }
	inline uint64_t* get_address_of_U3CSessionIdU3Ek__BackingField_3() { return &___U3CSessionIdU3Ek__BackingField_3; }
	inline void set_U3CSessionIdU3Ek__BackingField_3(uint64_t value)
	{
		___U3CSessionIdU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CPlatformU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CPlatformU3Ek__BackingField_4)); }
	inline String_t* get_U3CPlatformU3Ek__BackingField_4() const { return ___U3CPlatformU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPlatformU3Ek__BackingField_4() { return &___U3CPlatformU3Ek__BackingField_4; }
	inline void set_U3CPlatformU3Ek__BackingField_4(String_t* value)
	{
		___U3CPlatformU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlatformU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CPlatformIdU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CPlatformIdU3Ek__BackingField_5)); }
	inline int32_t get_U3CPlatformIdU3Ek__BackingField_5() const { return ___U3CPlatformIdU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPlatformIdU3Ek__BackingField_5() { return &___U3CPlatformIdU3Ek__BackingField_5; }
	inline void set_U3CPlatformIdU3Ek__BackingField_5(int32_t value)
	{
		___U3CPlatformIdU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CSdkVerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CSdkVerU3Ek__BackingField_6)); }
	inline String_t* get_U3CSdkVerU3Ek__BackingField_6() const { return ___U3CSdkVerU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CSdkVerU3Ek__BackingField_6() { return &___U3CSdkVerU3Ek__BackingField_6; }
	inline void set_U3CSdkVerU3Ek__BackingField_6(String_t* value)
	{
		___U3CSdkVerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSdkVerU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CDeviceIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CDeviceIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CDeviceIdU3Ek__BackingField_7() const { return ___U3CDeviceIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CDeviceIdU3Ek__BackingField_7() { return &___U3CDeviceIdU3Ek__BackingField_7; }
	inline void set_U3CDeviceIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CDeviceIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDeviceIdU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CBuildGUIDU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CBuildGUIDU3Ek__BackingField_8)); }
	inline String_t* get_U3CBuildGUIDU3Ek__BackingField_8() const { return ___U3CBuildGUIDU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CBuildGUIDU3Ek__BackingField_8() { return &___U3CBuildGUIDU3Ek__BackingField_8; }
	inline void set_U3CBuildGUIDU3Ek__BackingField_8(String_t* value)
	{
		___U3CBuildGUIDU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBuildGUIDU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CIapVerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CIapVerU3Ek__BackingField_9)); }
	inline String_t* get_U3CIapVerU3Ek__BackingField_9() const { return ___U3CIapVerU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CIapVerU3Ek__BackingField_9() { return &___U3CIapVerU3Ek__BackingField_9; }
	inline void set_U3CIapVerU3Ek__BackingField_9(String_t* value)
	{
		___U3CIapVerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIapVerU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CAdsGamerTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CAdsGamerTokenU3Ek__BackingField_10)); }
	inline String_t* get_U3CAdsGamerTokenU3Ek__BackingField_10() const { return ___U3CAdsGamerTokenU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CAdsGamerTokenU3Ek__BackingField_10() { return &___U3CAdsGamerTokenU3Ek__BackingField_10; }
	inline void set_U3CAdsGamerTokenU3Ek__BackingField_10(String_t* value)
	{
		___U3CAdsGamerTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAdsGamerTokenU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CTrackingOptOutU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CTrackingOptOutU3Ek__BackingField_11)); }
	inline Nullable_1_t2088641033  get_U3CTrackingOptOutU3Ek__BackingField_11() const { return ___U3CTrackingOptOutU3Ek__BackingField_11; }
	inline Nullable_1_t2088641033 * get_address_of_U3CTrackingOptOutU3Ek__BackingField_11() { return &___U3CTrackingOptOutU3Ek__BackingField_11; }
	inline void set_U3CTrackingOptOutU3Ek__BackingField_11(Nullable_1_t2088641033  value)
	{
		___U3CTrackingOptOutU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CAdsABGroupU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CAdsABGroupU3Ek__BackingField_12)); }
	inline Nullable_1_t334943763  get_U3CAdsABGroupU3Ek__BackingField_12() const { return ___U3CAdsABGroupU3Ek__BackingField_12; }
	inline Nullable_1_t334943763 * get_address_of_U3CAdsABGroupU3Ek__BackingField_12() { return &___U3CAdsABGroupU3Ek__BackingField_12; }
	inline void set_U3CAdsABGroupU3Ek__BackingField_12(Nullable_1_t334943763  value)
	{
		___U3CAdsABGroupU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CAdsGameIdU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CAdsGameIdU3Ek__BackingField_13)); }
	inline String_t* get_U3CAdsGameIdU3Ek__BackingField_13() const { return ___U3CAdsGameIdU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CAdsGameIdU3Ek__BackingField_13() { return &___U3CAdsGameIdU3Ek__BackingField_13; }
	inline void set_U3CAdsGameIdU3Ek__BackingField_13(String_t* value)
	{
		___U3CAdsGameIdU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAdsGameIdU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CStoreABGroupU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CStoreABGroupU3Ek__BackingField_14)); }
	inline Nullable_1_t334943763  get_U3CStoreABGroupU3Ek__BackingField_14() const { return ___U3CStoreABGroupU3Ek__BackingField_14; }
	inline Nullable_1_t334943763 * get_address_of_U3CStoreABGroupU3Ek__BackingField_14() { return &___U3CStoreABGroupU3Ek__BackingField_14; }
	inline void set_U3CStoreABGroupU3Ek__BackingField_14(Nullable_1_t334943763  value)
	{
		___U3CStoreABGroupU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CCatalogIdU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CCatalogIdU3Ek__BackingField_15)); }
	inline String_t* get_U3CCatalogIdU3Ek__BackingField_15() const { return ___U3CCatalogIdU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CCatalogIdU3Ek__BackingField_15() { return &___U3CCatalogIdU3Ek__BackingField_15; }
	inline void set_U3CCatalogIdU3Ek__BackingField_15(String_t* value)
	{
		___U3CCatalogIdU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCatalogIdU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CMonetizationIdU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CMonetizationIdU3Ek__BackingField_16)); }
	inline String_t* get_U3CMonetizationIdU3Ek__BackingField_16() const { return ___U3CMonetizationIdU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CMonetizationIdU3Ek__BackingField_16() { return &___U3CMonetizationIdU3Ek__BackingField_16; }
	inline void set_U3CMonetizationIdU3Ek__BackingField_16(String_t* value)
	{
		___U3CMonetizationIdU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMonetizationIdU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_U3CStoreNameU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CStoreNameU3Ek__BackingField_17)); }
	inline String_t* get_U3CStoreNameU3Ek__BackingField_17() const { return ___U3CStoreNameU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CStoreNameU3Ek__BackingField_17() { return &___U3CStoreNameU3Ek__BackingField_17; }
	inline void set_U3CStoreNameU3Ek__BackingField_17(String_t* value)
	{
		___U3CStoreNameU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStoreNameU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CGameVersionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CGameVersionU3Ek__BackingField_18)); }
	inline String_t* get_U3CGameVersionU3Ek__BackingField_18() const { return ___U3CGameVersionU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CGameVersionU3Ek__BackingField_18() { return &___U3CGameVersionU3Ek__BackingField_18; }
	inline void set_U3CGameVersionU3Ek__BackingField_18(String_t* value)
	{
		___U3CGameVersionU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameVersionU3Ek__BackingField_18, value);
	}

	inline static int32_t get_offset_of_U3CStoreTestEnabledU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249, ___U3CStoreTestEnabledU3Ek__BackingField_19)); }
	inline Nullable_1_t2088641033  get_U3CStoreTestEnabledU3Ek__BackingField_19() const { return ___U3CStoreTestEnabledU3Ek__BackingField_19; }
	inline Nullable_1_t2088641033 * get_address_of_U3CStoreTestEnabledU3Ek__BackingField_19() { return &___U3CStoreTestEnabledU3Ek__BackingField_19; }
	inline void set_U3CStoreTestEnabledU3Ek__BackingField_19(Nullable_1_t2088641033  value)
	{
		___U3CStoreTestEnabledU3Ek__BackingField_19 = value;
	}
};

struct ProfileData_t3353328249_StaticFields
{
public:
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.ProfileData::ProfileInstance
	ProfileData_t3353328249 * ___ProfileInstance_0;

public:
	inline static int32_t get_offset_of_ProfileInstance_0() { return static_cast<int32_t>(offsetof(ProfileData_t3353328249_StaticFields, ___ProfileInstance_0)); }
	inline ProfileData_t3353328249 * get_ProfileInstance_0() const { return ___ProfileInstance_0; }
	inline ProfileData_t3353328249 ** get_address_of_ProfileInstance_0() { return &___ProfileInstance_0; }
	inline void set_ProfileInstance_0(ProfileData_t3353328249 * value)
	{
		___ProfileInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___ProfileInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
