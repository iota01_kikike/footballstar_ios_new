﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_EZObjectPools_PooledObject2079175126.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimedDisable
struct  TimedDisable_t2667936835  : public PooledObject_t2079175126
{
public:
	// System.Single TimedDisable::timer
	float ___timer_3;
	// System.Single TimedDisable::DisableTime
	float ___DisableTime_4;

public:
	inline static int32_t get_offset_of_timer_3() { return static_cast<int32_t>(offsetof(TimedDisable_t2667936835, ___timer_3)); }
	inline float get_timer_3() const { return ___timer_3; }
	inline float* get_address_of_timer_3() { return &___timer_3; }
	inline void set_timer_3(float value)
	{
		___timer_3 = value;
	}

	inline static int32_t get_offset_of_DisableTime_4() { return static_cast<int32_t>(offsetof(TimedDisable_t2667936835, ___DisableTime_4)); }
	inline float get_DisableTime_4() const { return ___DisableTime_4; }
	inline float* get_address_of_DisableTime_4() { return &___DisableTime_4; }
	inline void set_DisableTime_4(float value)
	{
		___DisableTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
