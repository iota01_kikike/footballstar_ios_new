﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_ModifiedShadow522613381.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleOutline
struct  CircleOutline_t2613177674  : public ModifiedShadow_t522613381
{
public:
	// System.Int32 CircleOutline::m_circleCount
	int32_t ___m_circleCount_7;
	// System.Int32 CircleOutline::m_firstSample
	int32_t ___m_firstSample_8;
	// System.Int32 CircleOutline::m_sampleIncrement
	int32_t ___m_sampleIncrement_9;

public:
	inline static int32_t get_offset_of_m_circleCount_7() { return static_cast<int32_t>(offsetof(CircleOutline_t2613177674, ___m_circleCount_7)); }
	inline int32_t get_m_circleCount_7() const { return ___m_circleCount_7; }
	inline int32_t* get_address_of_m_circleCount_7() { return &___m_circleCount_7; }
	inline void set_m_circleCount_7(int32_t value)
	{
		___m_circleCount_7 = value;
	}

	inline static int32_t get_offset_of_m_firstSample_8() { return static_cast<int32_t>(offsetof(CircleOutline_t2613177674, ___m_firstSample_8)); }
	inline int32_t get_m_firstSample_8() const { return ___m_firstSample_8; }
	inline int32_t* get_address_of_m_firstSample_8() { return &___m_firstSample_8; }
	inline void set_m_firstSample_8(int32_t value)
	{
		___m_firstSample_8 = value;
	}

	inline static int32_t get_offset_of_m_sampleIncrement_9() { return static_cast<int32_t>(offsetof(CircleOutline_t2613177674, ___m_sampleIncrement_9)); }
	inline int32_t get_m_sampleIncrement_9() const { return ___m_sampleIncrement_9; }
	inline int32_t* get_address_of_m_sampleIncrement_9() { return &___m_sampleIncrement_9; }
	inline void set_m_sampleIncrement_9(int32_t value)
	{
		___m_sampleIncrement_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
