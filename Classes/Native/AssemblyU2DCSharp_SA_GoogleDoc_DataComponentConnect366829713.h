﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// System.Collections.Generic.List`1<SA.GoogleDoc.DataDelegate>
struct List_1_t1221723303;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.DataComponentConnection
struct  DataComponentConnection_t366829713  : public Il2CppObject
{
public:
	// System.Boolean SA.GoogleDoc.DataComponentConnection::IsOpened
	bool ___IsOpened_0;
	// System.Boolean SA.GoogleDoc.DataComponentConnection::IsMarkForDel
	bool ___IsMarkForDel_1;
	// UnityEngine.Component SA.GoogleDoc.DataComponentConnection::component
	Component_t3819376471 * ___component_2;
	// UnityEngine.GameObject SA.GoogleDoc.DataComponentConnection::owner
	GameObject_t1756533147 * ___owner_3;
	// System.String SA.GoogleDoc.DataComponentConnection::componentName
	String_t* ___componentName_4;
	// System.Collections.Generic.List`1<SA.GoogleDoc.DataDelegate> SA.GoogleDoc.DataComponentConnection::delegates
	List_1_t1221723303 * ___delegates_5;
	// System.Collections.Generic.List`1<System.String> SA.GoogleDoc.DataComponentConnection::fieldsNames
	List_1_t1398341365 * ___fieldsNames_6;
	// System.Collections.Generic.List`1<System.Boolean> SA.GoogleDoc.DataComponentConnection::fieldConnectable
	List_1_t3194695850 * ___fieldConnectable_7;

public:
	inline static int32_t get_offset_of_IsOpened_0() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___IsOpened_0)); }
	inline bool get_IsOpened_0() const { return ___IsOpened_0; }
	inline bool* get_address_of_IsOpened_0() { return &___IsOpened_0; }
	inline void set_IsOpened_0(bool value)
	{
		___IsOpened_0 = value;
	}

	inline static int32_t get_offset_of_IsMarkForDel_1() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___IsMarkForDel_1)); }
	inline bool get_IsMarkForDel_1() const { return ___IsMarkForDel_1; }
	inline bool* get_address_of_IsMarkForDel_1() { return &___IsMarkForDel_1; }
	inline void set_IsMarkForDel_1(bool value)
	{
		___IsMarkForDel_1 = value;
	}

	inline static int32_t get_offset_of_component_2() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___component_2)); }
	inline Component_t3819376471 * get_component_2() const { return ___component_2; }
	inline Component_t3819376471 ** get_address_of_component_2() { return &___component_2; }
	inline void set_component_2(Component_t3819376471 * value)
	{
		___component_2 = value;
		Il2CppCodeGenWriteBarrier(&___component_2, value);
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___owner_3)); }
	inline GameObject_t1756533147 * get_owner_3() const { return ___owner_3; }
	inline GameObject_t1756533147 ** get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(GameObject_t1756533147 * value)
	{
		___owner_3 = value;
		Il2CppCodeGenWriteBarrier(&___owner_3, value);
	}

	inline static int32_t get_offset_of_componentName_4() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___componentName_4)); }
	inline String_t* get_componentName_4() const { return ___componentName_4; }
	inline String_t** get_address_of_componentName_4() { return &___componentName_4; }
	inline void set_componentName_4(String_t* value)
	{
		___componentName_4 = value;
		Il2CppCodeGenWriteBarrier(&___componentName_4, value);
	}

	inline static int32_t get_offset_of_delegates_5() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___delegates_5)); }
	inline List_1_t1221723303 * get_delegates_5() const { return ___delegates_5; }
	inline List_1_t1221723303 ** get_address_of_delegates_5() { return &___delegates_5; }
	inline void set_delegates_5(List_1_t1221723303 * value)
	{
		___delegates_5 = value;
		Il2CppCodeGenWriteBarrier(&___delegates_5, value);
	}

	inline static int32_t get_offset_of_fieldsNames_6() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___fieldsNames_6)); }
	inline List_1_t1398341365 * get_fieldsNames_6() const { return ___fieldsNames_6; }
	inline List_1_t1398341365 ** get_address_of_fieldsNames_6() { return &___fieldsNames_6; }
	inline void set_fieldsNames_6(List_1_t1398341365 * value)
	{
		___fieldsNames_6 = value;
		Il2CppCodeGenWriteBarrier(&___fieldsNames_6, value);
	}

	inline static int32_t get_offset_of_fieldConnectable_7() { return static_cast<int32_t>(offsetof(DataComponentConnection_t366829713, ___fieldConnectable_7)); }
	inline List_1_t3194695850 * get_fieldConnectable_7() const { return ___fieldConnectable_7; }
	inline List_1_t3194695850 ** get_address_of_fieldConnectable_7() { return &___fieldConnectable_7; }
	inline void set_fieldConnectable_7(List_1_t3194695850 * value)
	{
		___fieldConnectable_7 = value;
		Il2CppCodeGenWriteBarrier(&___fieldConnectable_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
