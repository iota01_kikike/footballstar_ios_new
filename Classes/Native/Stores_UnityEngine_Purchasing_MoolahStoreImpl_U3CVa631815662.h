﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4206626141;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// UnityEngine.WWW
struct WWW_t2919945039;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13
struct  U3CVaildateProductU3Ed__13_t631815662  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::appkey
	String_t* ___appkey_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::productInfo
	String_t* ___productInfo_3;
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::result
	Action_2_t1865222972 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<>4__this
	MoolahStoreImpl_t4206626141 * ___U3CU3E4__this_5;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<sign>5__1
	String_t* ___U3CsignU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<wf>5__2
	WWWForm_t3950226929 * ___U3CwfU3E5__2_7;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<w>5__3
	WWW_t2919945039 * ___U3CwU3E5__3_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_appkey_2() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___appkey_2)); }
	inline String_t* get_appkey_2() const { return ___appkey_2; }
	inline String_t** get_address_of_appkey_2() { return &___appkey_2; }
	inline void set_appkey_2(String_t* value)
	{
		___appkey_2 = value;
		Il2CppCodeGenWriteBarrier(&___appkey_2, value);
	}

	inline static int32_t get_offset_of_productInfo_3() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___productInfo_3)); }
	inline String_t* get_productInfo_3() const { return ___productInfo_3; }
	inline String_t** get_address_of_productInfo_3() { return &___productInfo_3; }
	inline void set_productInfo_3(String_t* value)
	{
		___productInfo_3 = value;
		Il2CppCodeGenWriteBarrier(&___productInfo_3, value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___result_4)); }
	inline Action_2_t1865222972 * get_result_4() const { return ___result_4; }
	inline Action_2_t1865222972 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_2_t1865222972 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier(&___result_4, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t4206626141 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t4206626141 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t4206626141 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_5, value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___U3CsignU3E5__1_6)); }
	inline String_t* get_U3CsignU3E5__1_6() const { return ___U3CsignU3E5__1_6; }
	inline String_t** get_address_of_U3CsignU3E5__1_6() { return &___U3CsignU3E5__1_6; }
	inline void set_U3CsignU3E5__1_6(String_t* value)
	{
		___U3CsignU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsignU3E5__1_6, value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t3950226929 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t3950226929 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t3950226929 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwfU3E5__2_7, value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t631815662, ___U3CwU3E5__3_8)); }
	inline WWW_t2919945039 * get_U3CwU3E5__3_8() const { return ___U3CwU3E5__3_8; }
	inline WWW_t2919945039 ** get_address_of_U3CwU3E5__3_8() { return &___U3CwU3E5__3_8; }
	inline void set_U3CwU3E5__3_8(WWW_t2919945039 * value)
	{
		___U3CwU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwU3E5__3_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
