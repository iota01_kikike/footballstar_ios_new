﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.PurchasingEvent/<>c
struct U3CU3Ec_t4258048749;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String>
struct Func_2_t1883580443;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Object>
struct Func_2_t2543809505;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingEvent/<>c
struct  U3CU3Ec_t4258048749  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t4258048749_StaticFields
{
public:
	// UnityEngine.Purchasing.PurchasingEvent/<>c UnityEngine.Purchasing.PurchasingEvent/<>c::<>9
	U3CU3Ec_t4258048749 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> UnityEngine.Purchasing.PurchasingEvent/<>c::<>9__2_0
	Func_2_t1883580443 * ___U3CU3E9__2_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Object> UnityEngine.Purchasing.PurchasingEvent/<>c::<>9__2_1
	Func_2_t2543809505 * ___U3CU3E9__2_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4258048749_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4258048749 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4258048749 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4258048749 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4258048749_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_2_t1883580443 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_2_t1883580443 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_2_t1883580443 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__2_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4258048749_StaticFields, ___U3CU3E9__2_1_2)); }
	inline Func_2_t2543809505 * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline Func_2_t2543809505 ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(Func_2_t2543809505 * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__2_1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
