﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2282579641.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_FontStyle2764949590.h"

// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// CodeStage.AdvancedFPSCounter.AFPSCounter
struct AFPSCounter_t1611953012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData
struct  BaseCounterData_t2350381201  : public Il2CppObject
{
public:
	// System.Text.StringBuilder CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::text
	StringBuilder_t1221177846 * ___text_4;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::dirty
	bool ___dirty_5;
	// CodeStage.AdvancedFPSCounter.AFPSCounter CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::main
	AFPSCounter_t1611953012 * ___main_6;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::colorCached
	String_t* ___colorCached_7;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::inited
	bool ___inited_8;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::enabled
	bool ___enabled_9;
	// CodeStage.AdvancedFPSCounter.Labels.LabelAnchor CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::anchor
	uint8_t ___anchor_10;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::color
	Color_t2020392075  ___color_11;
	// UnityEngine.FontStyle CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::style
	int32_t ___style_12;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.BaseCounterData::extraText
	String_t* ___extraText_13;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___text_4)); }
	inline StringBuilder_t1221177846 * get_text_4() const { return ___text_4; }
	inline StringBuilder_t1221177846 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(StringBuilder_t1221177846 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_dirty_5() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___dirty_5)); }
	inline bool get_dirty_5() const { return ___dirty_5; }
	inline bool* get_address_of_dirty_5() { return &___dirty_5; }
	inline void set_dirty_5(bool value)
	{
		___dirty_5 = value;
	}

	inline static int32_t get_offset_of_main_6() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___main_6)); }
	inline AFPSCounter_t1611953012 * get_main_6() const { return ___main_6; }
	inline AFPSCounter_t1611953012 ** get_address_of_main_6() { return &___main_6; }
	inline void set_main_6(AFPSCounter_t1611953012 * value)
	{
		___main_6 = value;
		Il2CppCodeGenWriteBarrier(&___main_6, value);
	}

	inline static int32_t get_offset_of_colorCached_7() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___colorCached_7)); }
	inline String_t* get_colorCached_7() const { return ___colorCached_7; }
	inline String_t** get_address_of_colorCached_7() { return &___colorCached_7; }
	inline void set_colorCached_7(String_t* value)
	{
		___colorCached_7 = value;
		Il2CppCodeGenWriteBarrier(&___colorCached_7, value);
	}

	inline static int32_t get_offset_of_inited_8() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___inited_8)); }
	inline bool get_inited_8() const { return ___inited_8; }
	inline bool* get_address_of_inited_8() { return &___inited_8; }
	inline void set_inited_8(bool value)
	{
		___inited_8 = value;
	}

	inline static int32_t get_offset_of_enabled_9() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___enabled_9)); }
	inline bool get_enabled_9() const { return ___enabled_9; }
	inline bool* get_address_of_enabled_9() { return &___enabled_9; }
	inline void set_enabled_9(bool value)
	{
		___enabled_9 = value;
	}

	inline static int32_t get_offset_of_anchor_10() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___anchor_10)); }
	inline uint8_t get_anchor_10() const { return ___anchor_10; }
	inline uint8_t* get_address_of_anchor_10() { return &___anchor_10; }
	inline void set_anchor_10(uint8_t value)
	{
		___anchor_10 = value;
	}

	inline static int32_t get_offset_of_color_11() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___color_11)); }
	inline Color_t2020392075  get_color_11() const { return ___color_11; }
	inline Color_t2020392075 * get_address_of_color_11() { return &___color_11; }
	inline void set_color_11(Color_t2020392075  value)
	{
		___color_11 = value;
	}

	inline static int32_t get_offset_of_style_12() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___style_12)); }
	inline int32_t get_style_12() const { return ___style_12; }
	inline int32_t* get_address_of_style_12() { return &___style_12; }
	inline void set_style_12(int32_t value)
	{
		___style_12 = value;
	}

	inline static int32_t get_offset_of_extraText_13() { return static_cast<int32_t>(offsetof(BaseCounterData_t2350381201, ___extraText_13)); }
	inline String_t* get_extraText_13() const { return ___extraText_13; }
	inline String_t** get_address_of_extraText_13() { return &___extraText_13; }
	inline void set_extraText_13(String_t* value)
	{
		___extraText_13 = value;
		Il2CppCodeGenWriteBarrier(&___extraText_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
