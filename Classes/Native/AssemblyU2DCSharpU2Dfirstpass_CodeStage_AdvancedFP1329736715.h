﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3744281392.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData
struct  MemoryCounterData_t1329736715  : public UpdatebleCounterData_t3744281392
{
public:
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::precise
	bool ___precise_24;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::total
	bool ___total_25;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::allocated
	bool ___allocated_26;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::monoUsage
	bool ___monoUsage_27;
	// System.Int64 CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::<LastTotalValue>k__BackingField
	int64_t ___U3CLastTotalValueU3Ek__BackingField_28;
	// System.Int64 CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::<LastAllocatedValue>k__BackingField
	int64_t ___U3CLastAllocatedValueU3Ek__BackingField_29;
	// System.Int64 CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData::<LastMonoValue>k__BackingField
	int64_t ___U3CLastMonoValueU3Ek__BackingField_30;

public:
	inline static int32_t get_offset_of_precise_24() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___precise_24)); }
	inline bool get_precise_24() const { return ___precise_24; }
	inline bool* get_address_of_precise_24() { return &___precise_24; }
	inline void set_precise_24(bool value)
	{
		___precise_24 = value;
	}

	inline static int32_t get_offset_of_total_25() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___total_25)); }
	inline bool get_total_25() const { return ___total_25; }
	inline bool* get_address_of_total_25() { return &___total_25; }
	inline void set_total_25(bool value)
	{
		___total_25 = value;
	}

	inline static int32_t get_offset_of_allocated_26() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___allocated_26)); }
	inline bool get_allocated_26() const { return ___allocated_26; }
	inline bool* get_address_of_allocated_26() { return &___allocated_26; }
	inline void set_allocated_26(bool value)
	{
		___allocated_26 = value;
	}

	inline static int32_t get_offset_of_monoUsage_27() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___monoUsage_27)); }
	inline bool get_monoUsage_27() const { return ___monoUsage_27; }
	inline bool* get_address_of_monoUsage_27() { return &___monoUsage_27; }
	inline void set_monoUsage_27(bool value)
	{
		___monoUsage_27 = value;
	}

	inline static int32_t get_offset_of_U3CLastTotalValueU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___U3CLastTotalValueU3Ek__BackingField_28)); }
	inline int64_t get_U3CLastTotalValueU3Ek__BackingField_28() const { return ___U3CLastTotalValueU3Ek__BackingField_28; }
	inline int64_t* get_address_of_U3CLastTotalValueU3Ek__BackingField_28() { return &___U3CLastTotalValueU3Ek__BackingField_28; }
	inline void set_U3CLastTotalValueU3Ek__BackingField_28(int64_t value)
	{
		___U3CLastTotalValueU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CLastAllocatedValueU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___U3CLastAllocatedValueU3Ek__BackingField_29)); }
	inline int64_t get_U3CLastAllocatedValueU3Ek__BackingField_29() const { return ___U3CLastAllocatedValueU3Ek__BackingField_29; }
	inline int64_t* get_address_of_U3CLastAllocatedValueU3Ek__BackingField_29() { return &___U3CLastAllocatedValueU3Ek__BackingField_29; }
	inline void set_U3CLastAllocatedValueU3Ek__BackingField_29(int64_t value)
	{
		___U3CLastAllocatedValueU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CLastMonoValueU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(MemoryCounterData_t1329736715, ___U3CLastMonoValueU3Ek__BackingField_30)); }
	inline int64_t get_U3CLastMonoValueU3Ek__BackingField_30() const { return ___U3CLastMonoValueU3Ek__BackingField_30; }
	inline int64_t* get_address_of_U3CLastMonoValueU3Ek__BackingField_30() { return &___U3CLastMonoValueU3Ek__BackingField_30; }
	inline void set_U3CLastMonoValueU3Ek__BackingField_30(int64_t value)
	{
		___U3CLastMonoValueU3Ek__BackingField_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
