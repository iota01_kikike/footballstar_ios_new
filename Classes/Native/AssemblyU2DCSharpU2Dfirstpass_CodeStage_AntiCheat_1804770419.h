﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1905710121.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.String
struct String_t;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1785723201;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// CodeStage.AntiCheat.Detectors.WallHackDetector
struct WallHackDetector_t1804770419;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.WallHackDetector
struct  WallHackDetector_t1804770419  : public ActDetectorBase_t1905710121
{
public:
	// UnityEngine.Vector3 CodeStage.AntiCheat.Detectors.WallHackDetector::rigidPlayerVelocity
	Vector3_t2243707580  ___rigidPlayerVelocity_20;
	// UnityEngine.WaitForEndOfFrame CodeStage.AntiCheat.Detectors.WallHackDetector::waitForEndOfFrame
	WaitForEndOfFrame_t1785723201 * ___waitForEndOfFrame_22;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkRigidbody
	bool ___checkRigidbody_23;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkController
	bool ___checkController_24;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkWireframe
	bool ___checkWireframe_25;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkRaycast
	bool ___checkRaycast_26;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::wireframeDelay
	int32_t ___wireframeDelay_27;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::raycastDelay
	int32_t ___raycastDelay_28;
	// UnityEngine.Vector3 CodeStage.AntiCheat.Detectors.WallHackDetector::spawnPosition
	Vector3_t2243707580  ___spawnPosition_29;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::maxFalsePositives
	uint8_t ___maxFalsePositives_30;
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.WallHackDetector::serviceContainer
	GameObject_t1756533147 * ___serviceContainer_31;
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.WallHackDetector::solidWall
	GameObject_t1756533147 * ___solidWall_32;
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.WallHackDetector::thinWall
	GameObject_t1756533147 * ___thinWall_33;
	// UnityEngine.Camera CodeStage.AntiCheat.Detectors.WallHackDetector::wfCamera
	Camera_t189460977 * ___wfCamera_34;
	// UnityEngine.MeshRenderer CodeStage.AntiCheat.Detectors.WallHackDetector::foregroundRenderer
	MeshRenderer_t1268241104 * ___foregroundRenderer_35;
	// UnityEngine.MeshRenderer CodeStage.AntiCheat.Detectors.WallHackDetector::backgroundRenderer
	MeshRenderer_t1268241104 * ___backgroundRenderer_36;
	// UnityEngine.Color CodeStage.AntiCheat.Detectors.WallHackDetector::wfColor1
	Color_t2020392075  ___wfColor1_37;
	// UnityEngine.Color CodeStage.AntiCheat.Detectors.WallHackDetector::wfColor2
	Color_t2020392075  ___wfColor2_38;
	// UnityEngine.Shader CodeStage.AntiCheat.Detectors.WallHackDetector::wfShader
	Shader_t2430389951 * ___wfShader_39;
	// UnityEngine.Material CodeStage.AntiCheat.Detectors.WallHackDetector::wfMaterial
	Material_t193706927 * ___wfMaterial_40;
	// UnityEngine.Texture2D CodeStage.AntiCheat.Detectors.WallHackDetector::shaderTexture
	Texture2D_t3542995729 * ___shaderTexture_41;
	// UnityEngine.Texture2D CodeStage.AntiCheat.Detectors.WallHackDetector::targetTexture
	Texture2D_t3542995729 * ___targetTexture_42;
	// UnityEngine.RenderTexture CodeStage.AntiCheat.Detectors.WallHackDetector::renderTexture
	RenderTexture_t2666733923 * ___renderTexture_43;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::whLayer
	int32_t ___whLayer_44;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::raycastMask
	int32_t ___raycastMask_45;
	// UnityEngine.Rigidbody CodeStage.AntiCheat.Detectors.WallHackDetector::rigidPlayer
	Rigidbody_t4233889191 * ___rigidPlayer_46;
	// UnityEngine.CharacterController CodeStage.AntiCheat.Detectors.WallHackDetector::charControllerPlayer
	CharacterController_t4094781467 * ___charControllerPlayer_47;
	// System.Single CodeStage.AntiCheat.Detectors.WallHackDetector::charControllerVelocity
	float ___charControllerVelocity_48;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::rigidbodyDetections
	uint8_t ___rigidbodyDetections_49;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::controllerDetections
	uint8_t ___controllerDetections_50;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::wireframeDetections
	uint8_t ___wireframeDetections_51;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::raycastDetections
	uint8_t ___raycastDetections_52;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::wireframeDetected
	bool ___wireframeDetected_53;

public:
	inline static int32_t get_offset_of_rigidPlayerVelocity_20() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___rigidPlayerVelocity_20)); }
	inline Vector3_t2243707580  get_rigidPlayerVelocity_20() const { return ___rigidPlayerVelocity_20; }
	inline Vector3_t2243707580 * get_address_of_rigidPlayerVelocity_20() { return &___rigidPlayerVelocity_20; }
	inline void set_rigidPlayerVelocity_20(Vector3_t2243707580  value)
	{
		___rigidPlayerVelocity_20 = value;
	}

	inline static int32_t get_offset_of_waitForEndOfFrame_22() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___waitForEndOfFrame_22)); }
	inline WaitForEndOfFrame_t1785723201 * get_waitForEndOfFrame_22() const { return ___waitForEndOfFrame_22; }
	inline WaitForEndOfFrame_t1785723201 ** get_address_of_waitForEndOfFrame_22() { return &___waitForEndOfFrame_22; }
	inline void set_waitForEndOfFrame_22(WaitForEndOfFrame_t1785723201 * value)
	{
		___waitForEndOfFrame_22 = value;
		Il2CppCodeGenWriteBarrier(&___waitForEndOfFrame_22, value);
	}

	inline static int32_t get_offset_of_checkRigidbody_23() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___checkRigidbody_23)); }
	inline bool get_checkRigidbody_23() const { return ___checkRigidbody_23; }
	inline bool* get_address_of_checkRigidbody_23() { return &___checkRigidbody_23; }
	inline void set_checkRigidbody_23(bool value)
	{
		___checkRigidbody_23 = value;
	}

	inline static int32_t get_offset_of_checkController_24() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___checkController_24)); }
	inline bool get_checkController_24() const { return ___checkController_24; }
	inline bool* get_address_of_checkController_24() { return &___checkController_24; }
	inline void set_checkController_24(bool value)
	{
		___checkController_24 = value;
	}

	inline static int32_t get_offset_of_checkWireframe_25() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___checkWireframe_25)); }
	inline bool get_checkWireframe_25() const { return ___checkWireframe_25; }
	inline bool* get_address_of_checkWireframe_25() { return &___checkWireframe_25; }
	inline void set_checkWireframe_25(bool value)
	{
		___checkWireframe_25 = value;
	}

	inline static int32_t get_offset_of_checkRaycast_26() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___checkRaycast_26)); }
	inline bool get_checkRaycast_26() const { return ___checkRaycast_26; }
	inline bool* get_address_of_checkRaycast_26() { return &___checkRaycast_26; }
	inline void set_checkRaycast_26(bool value)
	{
		___checkRaycast_26 = value;
	}

	inline static int32_t get_offset_of_wireframeDelay_27() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wireframeDelay_27)); }
	inline int32_t get_wireframeDelay_27() const { return ___wireframeDelay_27; }
	inline int32_t* get_address_of_wireframeDelay_27() { return &___wireframeDelay_27; }
	inline void set_wireframeDelay_27(int32_t value)
	{
		___wireframeDelay_27 = value;
	}

	inline static int32_t get_offset_of_raycastDelay_28() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___raycastDelay_28)); }
	inline int32_t get_raycastDelay_28() const { return ___raycastDelay_28; }
	inline int32_t* get_address_of_raycastDelay_28() { return &___raycastDelay_28; }
	inline void set_raycastDelay_28(int32_t value)
	{
		___raycastDelay_28 = value;
	}

	inline static int32_t get_offset_of_spawnPosition_29() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___spawnPosition_29)); }
	inline Vector3_t2243707580  get_spawnPosition_29() const { return ___spawnPosition_29; }
	inline Vector3_t2243707580 * get_address_of_spawnPosition_29() { return &___spawnPosition_29; }
	inline void set_spawnPosition_29(Vector3_t2243707580  value)
	{
		___spawnPosition_29 = value;
	}

	inline static int32_t get_offset_of_maxFalsePositives_30() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___maxFalsePositives_30)); }
	inline uint8_t get_maxFalsePositives_30() const { return ___maxFalsePositives_30; }
	inline uint8_t* get_address_of_maxFalsePositives_30() { return &___maxFalsePositives_30; }
	inline void set_maxFalsePositives_30(uint8_t value)
	{
		___maxFalsePositives_30 = value;
	}

	inline static int32_t get_offset_of_serviceContainer_31() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___serviceContainer_31)); }
	inline GameObject_t1756533147 * get_serviceContainer_31() const { return ___serviceContainer_31; }
	inline GameObject_t1756533147 ** get_address_of_serviceContainer_31() { return &___serviceContainer_31; }
	inline void set_serviceContainer_31(GameObject_t1756533147 * value)
	{
		___serviceContainer_31 = value;
		Il2CppCodeGenWriteBarrier(&___serviceContainer_31, value);
	}

	inline static int32_t get_offset_of_solidWall_32() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___solidWall_32)); }
	inline GameObject_t1756533147 * get_solidWall_32() const { return ___solidWall_32; }
	inline GameObject_t1756533147 ** get_address_of_solidWall_32() { return &___solidWall_32; }
	inline void set_solidWall_32(GameObject_t1756533147 * value)
	{
		___solidWall_32 = value;
		Il2CppCodeGenWriteBarrier(&___solidWall_32, value);
	}

	inline static int32_t get_offset_of_thinWall_33() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___thinWall_33)); }
	inline GameObject_t1756533147 * get_thinWall_33() const { return ___thinWall_33; }
	inline GameObject_t1756533147 ** get_address_of_thinWall_33() { return &___thinWall_33; }
	inline void set_thinWall_33(GameObject_t1756533147 * value)
	{
		___thinWall_33 = value;
		Il2CppCodeGenWriteBarrier(&___thinWall_33, value);
	}

	inline static int32_t get_offset_of_wfCamera_34() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wfCamera_34)); }
	inline Camera_t189460977 * get_wfCamera_34() const { return ___wfCamera_34; }
	inline Camera_t189460977 ** get_address_of_wfCamera_34() { return &___wfCamera_34; }
	inline void set_wfCamera_34(Camera_t189460977 * value)
	{
		___wfCamera_34 = value;
		Il2CppCodeGenWriteBarrier(&___wfCamera_34, value);
	}

	inline static int32_t get_offset_of_foregroundRenderer_35() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___foregroundRenderer_35)); }
	inline MeshRenderer_t1268241104 * get_foregroundRenderer_35() const { return ___foregroundRenderer_35; }
	inline MeshRenderer_t1268241104 ** get_address_of_foregroundRenderer_35() { return &___foregroundRenderer_35; }
	inline void set_foregroundRenderer_35(MeshRenderer_t1268241104 * value)
	{
		___foregroundRenderer_35 = value;
		Il2CppCodeGenWriteBarrier(&___foregroundRenderer_35, value);
	}

	inline static int32_t get_offset_of_backgroundRenderer_36() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___backgroundRenderer_36)); }
	inline MeshRenderer_t1268241104 * get_backgroundRenderer_36() const { return ___backgroundRenderer_36; }
	inline MeshRenderer_t1268241104 ** get_address_of_backgroundRenderer_36() { return &___backgroundRenderer_36; }
	inline void set_backgroundRenderer_36(MeshRenderer_t1268241104 * value)
	{
		___backgroundRenderer_36 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundRenderer_36, value);
	}

	inline static int32_t get_offset_of_wfColor1_37() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wfColor1_37)); }
	inline Color_t2020392075  get_wfColor1_37() const { return ___wfColor1_37; }
	inline Color_t2020392075 * get_address_of_wfColor1_37() { return &___wfColor1_37; }
	inline void set_wfColor1_37(Color_t2020392075  value)
	{
		___wfColor1_37 = value;
	}

	inline static int32_t get_offset_of_wfColor2_38() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wfColor2_38)); }
	inline Color_t2020392075  get_wfColor2_38() const { return ___wfColor2_38; }
	inline Color_t2020392075 * get_address_of_wfColor2_38() { return &___wfColor2_38; }
	inline void set_wfColor2_38(Color_t2020392075  value)
	{
		___wfColor2_38 = value;
	}

	inline static int32_t get_offset_of_wfShader_39() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wfShader_39)); }
	inline Shader_t2430389951 * get_wfShader_39() const { return ___wfShader_39; }
	inline Shader_t2430389951 ** get_address_of_wfShader_39() { return &___wfShader_39; }
	inline void set_wfShader_39(Shader_t2430389951 * value)
	{
		___wfShader_39 = value;
		Il2CppCodeGenWriteBarrier(&___wfShader_39, value);
	}

	inline static int32_t get_offset_of_wfMaterial_40() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wfMaterial_40)); }
	inline Material_t193706927 * get_wfMaterial_40() const { return ___wfMaterial_40; }
	inline Material_t193706927 ** get_address_of_wfMaterial_40() { return &___wfMaterial_40; }
	inline void set_wfMaterial_40(Material_t193706927 * value)
	{
		___wfMaterial_40 = value;
		Il2CppCodeGenWriteBarrier(&___wfMaterial_40, value);
	}

	inline static int32_t get_offset_of_shaderTexture_41() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___shaderTexture_41)); }
	inline Texture2D_t3542995729 * get_shaderTexture_41() const { return ___shaderTexture_41; }
	inline Texture2D_t3542995729 ** get_address_of_shaderTexture_41() { return &___shaderTexture_41; }
	inline void set_shaderTexture_41(Texture2D_t3542995729 * value)
	{
		___shaderTexture_41 = value;
		Il2CppCodeGenWriteBarrier(&___shaderTexture_41, value);
	}

	inline static int32_t get_offset_of_targetTexture_42() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___targetTexture_42)); }
	inline Texture2D_t3542995729 * get_targetTexture_42() const { return ___targetTexture_42; }
	inline Texture2D_t3542995729 ** get_address_of_targetTexture_42() { return &___targetTexture_42; }
	inline void set_targetTexture_42(Texture2D_t3542995729 * value)
	{
		___targetTexture_42 = value;
		Il2CppCodeGenWriteBarrier(&___targetTexture_42, value);
	}

	inline static int32_t get_offset_of_renderTexture_43() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___renderTexture_43)); }
	inline RenderTexture_t2666733923 * get_renderTexture_43() const { return ___renderTexture_43; }
	inline RenderTexture_t2666733923 ** get_address_of_renderTexture_43() { return &___renderTexture_43; }
	inline void set_renderTexture_43(RenderTexture_t2666733923 * value)
	{
		___renderTexture_43 = value;
		Il2CppCodeGenWriteBarrier(&___renderTexture_43, value);
	}

	inline static int32_t get_offset_of_whLayer_44() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___whLayer_44)); }
	inline int32_t get_whLayer_44() const { return ___whLayer_44; }
	inline int32_t* get_address_of_whLayer_44() { return &___whLayer_44; }
	inline void set_whLayer_44(int32_t value)
	{
		___whLayer_44 = value;
	}

	inline static int32_t get_offset_of_raycastMask_45() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___raycastMask_45)); }
	inline int32_t get_raycastMask_45() const { return ___raycastMask_45; }
	inline int32_t* get_address_of_raycastMask_45() { return &___raycastMask_45; }
	inline void set_raycastMask_45(int32_t value)
	{
		___raycastMask_45 = value;
	}

	inline static int32_t get_offset_of_rigidPlayer_46() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___rigidPlayer_46)); }
	inline Rigidbody_t4233889191 * get_rigidPlayer_46() const { return ___rigidPlayer_46; }
	inline Rigidbody_t4233889191 ** get_address_of_rigidPlayer_46() { return &___rigidPlayer_46; }
	inline void set_rigidPlayer_46(Rigidbody_t4233889191 * value)
	{
		___rigidPlayer_46 = value;
		Il2CppCodeGenWriteBarrier(&___rigidPlayer_46, value);
	}

	inline static int32_t get_offset_of_charControllerPlayer_47() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___charControllerPlayer_47)); }
	inline CharacterController_t4094781467 * get_charControllerPlayer_47() const { return ___charControllerPlayer_47; }
	inline CharacterController_t4094781467 ** get_address_of_charControllerPlayer_47() { return &___charControllerPlayer_47; }
	inline void set_charControllerPlayer_47(CharacterController_t4094781467 * value)
	{
		___charControllerPlayer_47 = value;
		Il2CppCodeGenWriteBarrier(&___charControllerPlayer_47, value);
	}

	inline static int32_t get_offset_of_charControllerVelocity_48() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___charControllerVelocity_48)); }
	inline float get_charControllerVelocity_48() const { return ___charControllerVelocity_48; }
	inline float* get_address_of_charControllerVelocity_48() { return &___charControllerVelocity_48; }
	inline void set_charControllerVelocity_48(float value)
	{
		___charControllerVelocity_48 = value;
	}

	inline static int32_t get_offset_of_rigidbodyDetections_49() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___rigidbodyDetections_49)); }
	inline uint8_t get_rigidbodyDetections_49() const { return ___rigidbodyDetections_49; }
	inline uint8_t* get_address_of_rigidbodyDetections_49() { return &___rigidbodyDetections_49; }
	inline void set_rigidbodyDetections_49(uint8_t value)
	{
		___rigidbodyDetections_49 = value;
	}

	inline static int32_t get_offset_of_controllerDetections_50() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___controllerDetections_50)); }
	inline uint8_t get_controllerDetections_50() const { return ___controllerDetections_50; }
	inline uint8_t* get_address_of_controllerDetections_50() { return &___controllerDetections_50; }
	inline void set_controllerDetections_50(uint8_t value)
	{
		___controllerDetections_50 = value;
	}

	inline static int32_t get_offset_of_wireframeDetections_51() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wireframeDetections_51)); }
	inline uint8_t get_wireframeDetections_51() const { return ___wireframeDetections_51; }
	inline uint8_t* get_address_of_wireframeDetections_51() { return &___wireframeDetections_51; }
	inline void set_wireframeDetections_51(uint8_t value)
	{
		___wireframeDetections_51 = value;
	}

	inline static int32_t get_offset_of_raycastDetections_52() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___raycastDetections_52)); }
	inline uint8_t get_raycastDetections_52() const { return ___raycastDetections_52; }
	inline uint8_t* get_address_of_raycastDetections_52() { return &___raycastDetections_52; }
	inline void set_raycastDetections_52(uint8_t value)
	{
		___raycastDetections_52 = value;
	}

	inline static int32_t get_offset_of_wireframeDetected_53() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419, ___wireframeDetected_53)); }
	inline bool get_wireframeDetected_53() const { return ___wireframeDetected_53; }
	inline bool* get_address_of_wireframeDetected_53() { return &___wireframeDetected_53; }
	inline void set_wireframeDetected_53(bool value)
	{
		___wireframeDetected_53 = value;
	}
};

struct WallHackDetector_t1804770419_StaticFields
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::instancesInScene
	int32_t ___instancesInScene_21;
	// CodeStage.AntiCheat.Detectors.WallHackDetector CodeStage.AntiCheat.Detectors.WallHackDetector::<Instance>k__BackingField
	WallHackDetector_t1804770419 * ___U3CInstanceU3Ek__BackingField_54;

public:
	inline static int32_t get_offset_of_instancesInScene_21() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419_StaticFields, ___instancesInScene_21)); }
	inline int32_t get_instancesInScene_21() const { return ___instancesInScene_21; }
	inline int32_t* get_address_of_instancesInScene_21() { return &___instancesInScene_21; }
	inline void set_instancesInScene_21(int32_t value)
	{
		___instancesInScene_21 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(WallHackDetector_t1804770419_StaticFields, ___U3CInstanceU3Ek__BackingField_54)); }
	inline WallHackDetector_t1804770419 * get_U3CInstanceU3Ek__BackingField_54() const { return ___U3CInstanceU3Ek__BackingField_54; }
	inline WallHackDetector_t1804770419 ** get_address_of_U3CInstanceU3Ek__BackingField_54() { return &___U3CInstanceU3Ek__BackingField_54; }
	inline void set_U3CInstanceU3Ek__BackingField_54(WallHackDetector_t1804770419 * value)
	{
		___U3CInstanceU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_54, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
