﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_4226477674.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion
struct  ObscuredQuaternion_t2380454615 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::currentCryptoKey
	int32_t ___currentCryptoKey_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion/RawEncryptedQuaternion CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::hiddenValue
	RawEncryptedQuaternion_t4226477674  ___hiddenValue_3;
	// UnityEngine.Quaternion CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::fakeValue
	Quaternion_t4030073918  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::inited
	bool ___inited_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_2() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_t2380454615, ___currentCryptoKey_2)); }
	inline int32_t get_currentCryptoKey_2() const { return ___currentCryptoKey_2; }
	inline int32_t* get_address_of_currentCryptoKey_2() { return &___currentCryptoKey_2; }
	inline void set_currentCryptoKey_2(int32_t value)
	{
		___currentCryptoKey_2 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_3() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_t2380454615, ___hiddenValue_3)); }
	inline RawEncryptedQuaternion_t4226477674  get_hiddenValue_3() const { return ___hiddenValue_3; }
	inline RawEncryptedQuaternion_t4226477674 * get_address_of_hiddenValue_3() { return &___hiddenValue_3; }
	inline void set_hiddenValue_3(RawEncryptedQuaternion_t4226477674  value)
	{
		___hiddenValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_t2380454615, ___fakeValue_4)); }
	inline Quaternion_t4030073918  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Quaternion_t4030073918 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Quaternion_t4030073918  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_inited_5() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_t2380454615, ___inited_5)); }
	inline bool get_inited_5() const { return ___inited_5; }
	inline bool* get_address_of_inited_5() { return &___inited_5; }
	inline void set_inited_5(bool value)
	{
		___inited_5 = value;
	}
};

struct ObscuredQuaternion_t2380454615_StaticFields
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::cryptoKey
	int32_t ___cryptoKey_0;
	// UnityEngine.Quaternion CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::initialFakeValue
	Quaternion_t4030073918  ___initialFakeValue_1;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_t2380454615_StaticFields, ___cryptoKey_0)); }
	inline int32_t get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline int32_t* get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(int32_t value)
	{
		___cryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_initialFakeValue_1() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_t2380454615_StaticFields, ___initialFakeValue_1)); }
	inline Quaternion_t4030073918  get_initialFakeValue_1() const { return ___initialFakeValue_1; }
	inline Quaternion_t4030073918 * get_address_of_initialFakeValue_1() { return &___initialFakeValue_1; }
	inline void set_initialFakeValue_1(Quaternion_t4030073918  value)
	{
		___initialFakeValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion
struct ObscuredQuaternion_t2380454615_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_2;
	RawEncryptedQuaternion_t4226477674  ___hiddenValue_3;
	Quaternion_t4030073918  ___fakeValue_4;
	int32_t ___inited_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion
struct ObscuredQuaternion_t2380454615_marshaled_com
{
	int32_t ___currentCryptoKey_2;
	RawEncryptedQuaternion_t4226477674  ___hiddenValue_3;
	Quaternion_t4030073918  ___fakeValue_4;
	int32_t ___inited_5;
};
