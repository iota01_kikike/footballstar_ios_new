﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/TileData
struct  TileData_t2249013992 
{
public:
	// UnityEngine.GameObject TrainingView/TileData::Trainee
	GameObject_t1756533147 * ___Trainee_0;

public:
	inline static int32_t get_offset_of_Trainee_0() { return static_cast<int32_t>(offsetof(TileData_t2249013992, ___Trainee_0)); }
	inline GameObject_t1756533147 * get_Trainee_0() const { return ___Trainee_0; }
	inline GameObject_t1756533147 ** get_address_of_Trainee_0() { return &___Trainee_0; }
	inline void set_Trainee_0(GameObject_t1756533147 * value)
	{
		___Trainee_0 = value;
		Il2CppCodeGenWriteBarrier(&___Trainee_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TrainingView/TileData
struct TileData_t2249013992_marshaled_pinvoke
{
	GameObject_t1756533147 * ___Trainee_0;
};
// Native definition for COM marshalling of TrainingView/TileData
struct TileData_t2249013992_marshaled_com
{
	GameObject_t1756533147 * ___Trainee_0;
};
