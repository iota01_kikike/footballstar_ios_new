﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AssemblyU2DCSharp_Assets_SimpleAndroidNotification3050020448.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.SimpleAndroidNotifications.NotificationParams
struct  NotificationParams_t1024736401  : public Il2CppObject
{
public:
	// System.Int32 Assets.SimpleAndroidNotifications.NotificationParams::Id
	int32_t ___Id_0;
	// System.TimeSpan Assets.SimpleAndroidNotifications.NotificationParams::Delay
	TimeSpan_t3430258949  ___Delay_1;
	// System.String Assets.SimpleAndroidNotifications.NotificationParams::Title
	String_t* ___Title_2;
	// System.String Assets.SimpleAndroidNotifications.NotificationParams::Message
	String_t* ___Message_3;
	// System.String Assets.SimpleAndroidNotifications.NotificationParams::Ticker
	String_t* ___Ticker_4;
	// System.Boolean Assets.SimpleAndroidNotifications.NotificationParams::Sound
	bool ___Sound_5;
	// System.Boolean Assets.SimpleAndroidNotifications.NotificationParams::Vibrate
	bool ___Vibrate_6;
	// System.Boolean Assets.SimpleAndroidNotifications.NotificationParams::Light
	bool ___Light_7;
	// Assets.SimpleAndroidNotifications.NotificationIcon Assets.SimpleAndroidNotifications.NotificationParams::SmallIcon
	int32_t ___SmallIcon_8;
	// UnityEngine.Color Assets.SimpleAndroidNotifications.NotificationParams::SmallIconColor
	Color_t2020392075  ___SmallIconColor_9;
	// System.String Assets.SimpleAndroidNotifications.NotificationParams::LargeIcon
	String_t* ___LargeIcon_10;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Delay_1() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Delay_1)); }
	inline TimeSpan_t3430258949  get_Delay_1() const { return ___Delay_1; }
	inline TimeSpan_t3430258949 * get_address_of_Delay_1() { return &___Delay_1; }
	inline void set_Delay_1(TimeSpan_t3430258949  value)
	{
		___Delay_1 = value;
	}

	inline static int32_t get_offset_of_Title_2() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Title_2)); }
	inline String_t* get_Title_2() const { return ___Title_2; }
	inline String_t** get_address_of_Title_2() { return &___Title_2; }
	inline void set_Title_2(String_t* value)
	{
		___Title_2 = value;
		Il2CppCodeGenWriteBarrier(&___Title_2, value);
	}

	inline static int32_t get_offset_of_Message_3() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Message_3)); }
	inline String_t* get_Message_3() const { return ___Message_3; }
	inline String_t** get_address_of_Message_3() { return &___Message_3; }
	inline void set_Message_3(String_t* value)
	{
		___Message_3 = value;
		Il2CppCodeGenWriteBarrier(&___Message_3, value);
	}

	inline static int32_t get_offset_of_Ticker_4() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Ticker_4)); }
	inline String_t* get_Ticker_4() const { return ___Ticker_4; }
	inline String_t** get_address_of_Ticker_4() { return &___Ticker_4; }
	inline void set_Ticker_4(String_t* value)
	{
		___Ticker_4 = value;
		Il2CppCodeGenWriteBarrier(&___Ticker_4, value);
	}

	inline static int32_t get_offset_of_Sound_5() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Sound_5)); }
	inline bool get_Sound_5() const { return ___Sound_5; }
	inline bool* get_address_of_Sound_5() { return &___Sound_5; }
	inline void set_Sound_5(bool value)
	{
		___Sound_5 = value;
	}

	inline static int32_t get_offset_of_Vibrate_6() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Vibrate_6)); }
	inline bool get_Vibrate_6() const { return ___Vibrate_6; }
	inline bool* get_address_of_Vibrate_6() { return &___Vibrate_6; }
	inline void set_Vibrate_6(bool value)
	{
		___Vibrate_6 = value;
	}

	inline static int32_t get_offset_of_Light_7() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___Light_7)); }
	inline bool get_Light_7() const { return ___Light_7; }
	inline bool* get_address_of_Light_7() { return &___Light_7; }
	inline void set_Light_7(bool value)
	{
		___Light_7 = value;
	}

	inline static int32_t get_offset_of_SmallIcon_8() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___SmallIcon_8)); }
	inline int32_t get_SmallIcon_8() const { return ___SmallIcon_8; }
	inline int32_t* get_address_of_SmallIcon_8() { return &___SmallIcon_8; }
	inline void set_SmallIcon_8(int32_t value)
	{
		___SmallIcon_8 = value;
	}

	inline static int32_t get_offset_of_SmallIconColor_9() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___SmallIconColor_9)); }
	inline Color_t2020392075  get_SmallIconColor_9() const { return ___SmallIconColor_9; }
	inline Color_t2020392075 * get_address_of_SmallIconColor_9() { return &___SmallIconColor_9; }
	inline void set_SmallIconColor_9(Color_t2020392075  value)
	{
		___SmallIconColor_9 = value;
	}

	inline static int32_t get_offset_of_LargeIcon_10() { return static_cast<int32_t>(offsetof(NotificationParams_t1024736401, ___LargeIcon_10)); }
	inline String_t* get_LargeIcon_10() const { return ___LargeIcon_10; }
	inline String_t** get_address_of_LargeIcon_10() { return &___LargeIcon_10; }
	inline void set_LargeIcon_10(String_t* value)
	{
		___LargeIcon_10 = value;
		Il2CppCodeGenWriteBarrier(&___LargeIcon_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
