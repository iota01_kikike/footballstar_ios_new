﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingEvent
struct  PurchasingEvent_t2028586576  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.PurchasingEvent::EventDict
	Dictionary_2_t309261261 * ___EventDict_0;

public:
	inline static int32_t get_offset_of_EventDict_0() { return static_cast<int32_t>(offsetof(PurchasingEvent_t2028586576, ___EventDict_0)); }
	inline Dictionary_2_t309261261 * get_EventDict_0() const { return ___EventDict_0; }
	inline Dictionary_2_t309261261 ** get_address_of_EventDict_0() { return &___EventDict_0; }
	inline void set_EventDict_0(Dictionary_2_t309261261 * value)
	{
		___EventDict_0 = value;
		Il2CppCodeGenWriteBarrier(&___EventDict_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
