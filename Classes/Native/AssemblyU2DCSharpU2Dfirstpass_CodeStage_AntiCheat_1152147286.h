﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1905710121.h"

// System.String
struct String_t;
// CodeStage.AntiCheat.Detectors.SpeedHackDetector
struct SpeedHackDetector_t1152147286;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.SpeedHackDetector
struct  SpeedHackDetector_t1152147286  : public ActDetectorBase_t1905710121
{
public:
	// System.Single CodeStage.AntiCheat.Detectors.SpeedHackDetector::interval
	float ___interval_19;
	// System.Byte CodeStage.AntiCheat.Detectors.SpeedHackDetector::maxFalsePositives
	uint8_t ___maxFalsePositives_20;
	// System.Int32 CodeStage.AntiCheat.Detectors.SpeedHackDetector::coolDown
	int32_t ___coolDown_21;
	// System.Byte CodeStage.AntiCheat.Detectors.SpeedHackDetector::currentFalsePositives
	uint8_t ___currentFalsePositives_22;
	// System.Int32 CodeStage.AntiCheat.Detectors.SpeedHackDetector::currentCooldownShots
	int32_t ___currentCooldownShots_23;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::ticksOnStart
	int64_t ___ticksOnStart_24;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::vulnerableTicksOnStart
	int64_t ___vulnerableTicksOnStart_25;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::prevTicks
	int64_t ___prevTicks_26;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::prevIntervalTicks
	int64_t ___prevIntervalTicks_27;

public:
	inline static int32_t get_offset_of_interval_19() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___interval_19)); }
	inline float get_interval_19() const { return ___interval_19; }
	inline float* get_address_of_interval_19() { return &___interval_19; }
	inline void set_interval_19(float value)
	{
		___interval_19 = value;
	}

	inline static int32_t get_offset_of_maxFalsePositives_20() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___maxFalsePositives_20)); }
	inline uint8_t get_maxFalsePositives_20() const { return ___maxFalsePositives_20; }
	inline uint8_t* get_address_of_maxFalsePositives_20() { return &___maxFalsePositives_20; }
	inline void set_maxFalsePositives_20(uint8_t value)
	{
		___maxFalsePositives_20 = value;
	}

	inline static int32_t get_offset_of_coolDown_21() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___coolDown_21)); }
	inline int32_t get_coolDown_21() const { return ___coolDown_21; }
	inline int32_t* get_address_of_coolDown_21() { return &___coolDown_21; }
	inline void set_coolDown_21(int32_t value)
	{
		___coolDown_21 = value;
	}

	inline static int32_t get_offset_of_currentFalsePositives_22() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___currentFalsePositives_22)); }
	inline uint8_t get_currentFalsePositives_22() const { return ___currentFalsePositives_22; }
	inline uint8_t* get_address_of_currentFalsePositives_22() { return &___currentFalsePositives_22; }
	inline void set_currentFalsePositives_22(uint8_t value)
	{
		___currentFalsePositives_22 = value;
	}

	inline static int32_t get_offset_of_currentCooldownShots_23() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___currentCooldownShots_23)); }
	inline int32_t get_currentCooldownShots_23() const { return ___currentCooldownShots_23; }
	inline int32_t* get_address_of_currentCooldownShots_23() { return &___currentCooldownShots_23; }
	inline void set_currentCooldownShots_23(int32_t value)
	{
		___currentCooldownShots_23 = value;
	}

	inline static int32_t get_offset_of_ticksOnStart_24() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___ticksOnStart_24)); }
	inline int64_t get_ticksOnStart_24() const { return ___ticksOnStart_24; }
	inline int64_t* get_address_of_ticksOnStart_24() { return &___ticksOnStart_24; }
	inline void set_ticksOnStart_24(int64_t value)
	{
		___ticksOnStart_24 = value;
	}

	inline static int32_t get_offset_of_vulnerableTicksOnStart_25() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___vulnerableTicksOnStart_25)); }
	inline int64_t get_vulnerableTicksOnStart_25() const { return ___vulnerableTicksOnStart_25; }
	inline int64_t* get_address_of_vulnerableTicksOnStart_25() { return &___vulnerableTicksOnStart_25; }
	inline void set_vulnerableTicksOnStart_25(int64_t value)
	{
		___vulnerableTicksOnStart_25 = value;
	}

	inline static int32_t get_offset_of_prevTicks_26() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___prevTicks_26)); }
	inline int64_t get_prevTicks_26() const { return ___prevTicks_26; }
	inline int64_t* get_address_of_prevTicks_26() { return &___prevTicks_26; }
	inline void set_prevTicks_26(int64_t value)
	{
		___prevTicks_26 = value;
	}

	inline static int32_t get_offset_of_prevIntervalTicks_27() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286, ___prevIntervalTicks_27)); }
	inline int64_t get_prevIntervalTicks_27() const { return ___prevIntervalTicks_27; }
	inline int64_t* get_address_of_prevIntervalTicks_27() { return &___prevIntervalTicks_27; }
	inline void set_prevIntervalTicks_27(int64_t value)
	{
		___prevIntervalTicks_27 = value;
	}
};

struct SpeedHackDetector_t1152147286_StaticFields
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.SpeedHackDetector::instancesInScene
	int32_t ___instancesInScene_18;
	// CodeStage.AntiCheat.Detectors.SpeedHackDetector CodeStage.AntiCheat.Detectors.SpeedHackDetector::<Instance>k__BackingField
	SpeedHackDetector_t1152147286 * ___U3CInstanceU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of_instancesInScene_18() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286_StaticFields, ___instancesInScene_18)); }
	inline int32_t get_instancesInScene_18() const { return ___instancesInScene_18; }
	inline int32_t* get_address_of_instancesInScene_18() { return &___instancesInScene_18; }
	inline void set_instancesInScene_18(int32_t value)
	{
		___instancesInScene_18 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(SpeedHackDetector_t1152147286_StaticFields, ___U3CInstanceU3Ek__BackingField_28)); }
	inline SpeedHackDetector_t1152147286 * get_U3CInstanceU3Ek__BackingField_28() const { return ___U3CInstanceU3Ek__BackingField_28; }
	inline SpeedHackDetector_t1152147286 ** get_address_of_U3CInstanceU3Ek__BackingField_28() { return &___U3CInstanceU3Ek__BackingField_28; }
	inline void set_U3CInstanceU3Ek__BackingField_28(SpeedHackDetector_t1152147286 * value)
	{
		___U3CInstanceU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
