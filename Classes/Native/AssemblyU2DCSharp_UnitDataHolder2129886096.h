﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// TraineeUnit
struct TraineeUnit_t2929148814;
// UnitUIView
struct UnitUIView_t3306525905;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitDataHolder
struct  UnitDataHolder_t2129886096  : public MonoBehaviour_t1158329972
{
public:
	// TraineeUnit UnitDataHolder::UnitData
	TraineeUnit_t2929148814 * ___UnitData_2;
	// UnitUIView UnitDataHolder::UnitUIView
	UnitUIView_t3306525905 * ___UnitUIView_3;

public:
	inline static int32_t get_offset_of_UnitData_2() { return static_cast<int32_t>(offsetof(UnitDataHolder_t2129886096, ___UnitData_2)); }
	inline TraineeUnit_t2929148814 * get_UnitData_2() const { return ___UnitData_2; }
	inline TraineeUnit_t2929148814 ** get_address_of_UnitData_2() { return &___UnitData_2; }
	inline void set_UnitData_2(TraineeUnit_t2929148814 * value)
	{
		___UnitData_2 = value;
		Il2CppCodeGenWriteBarrier(&___UnitData_2, value);
	}

	inline static int32_t get_offset_of_UnitUIView_3() { return static_cast<int32_t>(offsetof(UnitDataHolder_t2129886096, ___UnitUIView_3)); }
	inline UnitUIView_t3306525905 * get_UnitUIView_3() const { return ___UnitUIView_3; }
	inline UnitUIView_t3306525905 ** get_address_of_UnitUIView_3() { return &___UnitUIView_3; }
	inline void set_UnitUIView_3(UnitUIView_t3306525905 * value)
	{
		___UnitUIView_3 = value;
		Il2CppCodeGenWriteBarrier(&___UnitUIView_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
