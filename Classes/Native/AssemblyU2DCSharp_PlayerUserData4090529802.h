﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O337339225.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerUserData
struct  PlayerUserData_t4090529802  : public Il2CppObject
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt PlayerUserData::No
	ObscuredInt_t796441056  ___No_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt PlayerUserData::NationNo
	ObscuredInt_t796441056  ___NationNo_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt PlayerUserData::Lv
	ObscuredInt_t796441056  ___Lv_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt PlayerUserData::CardCount
	ObscuredInt_t796441056  ___CardCount_3;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool PlayerUserData::IsNew
	ObscuredBool_t337339225  ___IsNew_4;

public:
	inline static int32_t get_offset_of_No_0() { return static_cast<int32_t>(offsetof(PlayerUserData_t4090529802, ___No_0)); }
	inline ObscuredInt_t796441056  get_No_0() const { return ___No_0; }
	inline ObscuredInt_t796441056 * get_address_of_No_0() { return &___No_0; }
	inline void set_No_0(ObscuredInt_t796441056  value)
	{
		___No_0 = value;
	}

	inline static int32_t get_offset_of_NationNo_1() { return static_cast<int32_t>(offsetof(PlayerUserData_t4090529802, ___NationNo_1)); }
	inline ObscuredInt_t796441056  get_NationNo_1() const { return ___NationNo_1; }
	inline ObscuredInt_t796441056 * get_address_of_NationNo_1() { return &___NationNo_1; }
	inline void set_NationNo_1(ObscuredInt_t796441056  value)
	{
		___NationNo_1 = value;
	}

	inline static int32_t get_offset_of_Lv_2() { return static_cast<int32_t>(offsetof(PlayerUserData_t4090529802, ___Lv_2)); }
	inline ObscuredInt_t796441056  get_Lv_2() const { return ___Lv_2; }
	inline ObscuredInt_t796441056 * get_address_of_Lv_2() { return &___Lv_2; }
	inline void set_Lv_2(ObscuredInt_t796441056  value)
	{
		___Lv_2 = value;
	}

	inline static int32_t get_offset_of_CardCount_3() { return static_cast<int32_t>(offsetof(PlayerUserData_t4090529802, ___CardCount_3)); }
	inline ObscuredInt_t796441056  get_CardCount_3() const { return ___CardCount_3; }
	inline ObscuredInt_t796441056 * get_address_of_CardCount_3() { return &___CardCount_3; }
	inline void set_CardCount_3(ObscuredInt_t796441056  value)
	{
		___CardCount_3 = value;
	}

	inline static int32_t get_offset_of_IsNew_4() { return static_cast<int32_t>(offsetof(PlayerUserData_t4090529802, ___IsNew_4)); }
	inline ObscuredBool_t337339225  get_IsNew_4() const { return ___IsNew_4; }
	inline ObscuredBool_t337339225 * get_address_of_IsNew_4() { return &___IsNew_4; }
	inline void set_IsNew_4(ObscuredBool_t337339225  value)
	{
		___IsNew_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
