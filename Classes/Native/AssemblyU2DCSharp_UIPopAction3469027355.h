﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPopAction
struct  UIPopAction_t3469027355  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UIPopAction::PopupFrame
	GameObject_t1756533147 * ___PopupFrame_2;
	// UnityEngine.Vector3 UIPopAction::originPos
	Vector3_t2243707580  ___originPos_3;

public:
	inline static int32_t get_offset_of_PopupFrame_2() { return static_cast<int32_t>(offsetof(UIPopAction_t3469027355, ___PopupFrame_2)); }
	inline GameObject_t1756533147 * get_PopupFrame_2() const { return ___PopupFrame_2; }
	inline GameObject_t1756533147 ** get_address_of_PopupFrame_2() { return &___PopupFrame_2; }
	inline void set_PopupFrame_2(GameObject_t1756533147 * value)
	{
		___PopupFrame_2 = value;
		Il2CppCodeGenWriteBarrier(&___PopupFrame_2, value);
	}

	inline static int32_t get_offset_of_originPos_3() { return static_cast<int32_t>(offsetof(UIPopAction_t3469027355, ___originPos_3)); }
	inline Vector3_t2243707580  get_originPos_3() const { return ___originPos_3; }
	inline Vector3_t2243707580 * get_address_of_originPos_3() { return &___originPos_3; }
	inline void set_originPos_3(Vector3_t2243707580  value)
	{
		___originPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
