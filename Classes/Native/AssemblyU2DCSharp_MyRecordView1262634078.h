﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyRecordView
struct  MyRecordView_t1262634078  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button MyRecordView::Prevbutton
	Button_t2872111280 * ___Prevbutton_2;
	// UnityEngine.UI.Text MyRecordView::PlayTimeText
	Text_t356221433 * ___PlayTimeText_3;
	// UnityEngine.UI.Text MyRecordView::HighScoreText
	Text_t356221433 * ___HighScoreText_4;
	// UnityEngine.UI.Text MyRecordView::HighOverallText
	Text_t356221433 * ___HighOverallText_5;
	// UnityEngine.UI.Text[] MyRecordView::PlayerCountText
	TextU5BU5D_t4216439300* ___PlayerCountText_6;
	// UnityEngine.UI.Image[] MyRecordView::GaugeImage
	ImageU5BU5D_t590162004* ___GaugeImage_7;
	// UnityEngine.RectTransform MyRecordView::GaugeBgTransform
	RectTransform_t3349966182 * ___GaugeBgTransform_8;

public:
	inline static int32_t get_offset_of_Prevbutton_2() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___Prevbutton_2)); }
	inline Button_t2872111280 * get_Prevbutton_2() const { return ___Prevbutton_2; }
	inline Button_t2872111280 ** get_address_of_Prevbutton_2() { return &___Prevbutton_2; }
	inline void set_Prevbutton_2(Button_t2872111280 * value)
	{
		___Prevbutton_2 = value;
		Il2CppCodeGenWriteBarrier(&___Prevbutton_2, value);
	}

	inline static int32_t get_offset_of_PlayTimeText_3() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___PlayTimeText_3)); }
	inline Text_t356221433 * get_PlayTimeText_3() const { return ___PlayTimeText_3; }
	inline Text_t356221433 ** get_address_of_PlayTimeText_3() { return &___PlayTimeText_3; }
	inline void set_PlayTimeText_3(Text_t356221433 * value)
	{
		___PlayTimeText_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlayTimeText_3, value);
	}

	inline static int32_t get_offset_of_HighScoreText_4() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___HighScoreText_4)); }
	inline Text_t356221433 * get_HighScoreText_4() const { return ___HighScoreText_4; }
	inline Text_t356221433 ** get_address_of_HighScoreText_4() { return &___HighScoreText_4; }
	inline void set_HighScoreText_4(Text_t356221433 * value)
	{
		___HighScoreText_4 = value;
		Il2CppCodeGenWriteBarrier(&___HighScoreText_4, value);
	}

	inline static int32_t get_offset_of_HighOverallText_5() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___HighOverallText_5)); }
	inline Text_t356221433 * get_HighOverallText_5() const { return ___HighOverallText_5; }
	inline Text_t356221433 ** get_address_of_HighOverallText_5() { return &___HighOverallText_5; }
	inline void set_HighOverallText_5(Text_t356221433 * value)
	{
		___HighOverallText_5 = value;
		Il2CppCodeGenWriteBarrier(&___HighOverallText_5, value);
	}

	inline static int32_t get_offset_of_PlayerCountText_6() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___PlayerCountText_6)); }
	inline TextU5BU5D_t4216439300* get_PlayerCountText_6() const { return ___PlayerCountText_6; }
	inline TextU5BU5D_t4216439300** get_address_of_PlayerCountText_6() { return &___PlayerCountText_6; }
	inline void set_PlayerCountText_6(TextU5BU5D_t4216439300* value)
	{
		___PlayerCountText_6 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerCountText_6, value);
	}

	inline static int32_t get_offset_of_GaugeImage_7() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___GaugeImage_7)); }
	inline ImageU5BU5D_t590162004* get_GaugeImage_7() const { return ___GaugeImage_7; }
	inline ImageU5BU5D_t590162004** get_address_of_GaugeImage_7() { return &___GaugeImage_7; }
	inline void set_GaugeImage_7(ImageU5BU5D_t590162004* value)
	{
		___GaugeImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___GaugeImage_7, value);
	}

	inline static int32_t get_offset_of_GaugeBgTransform_8() { return static_cast<int32_t>(offsetof(MyRecordView_t1262634078, ___GaugeBgTransform_8)); }
	inline RectTransform_t3349966182 * get_GaugeBgTransform_8() const { return ___GaugeBgTransform_8; }
	inline RectTransform_t3349966182 ** get_address_of_GaugeBgTransform_8() { return &___GaugeBgTransform_8; }
	inline void set_GaugeBgTransform_8(RectTransform_t3349966182 * value)
	{
		___GaugeBgTransform_8 = value;
		Il2CppCodeGenWriteBarrier(&___GaugeBgTransform_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
