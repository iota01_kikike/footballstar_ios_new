﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Player
struct Player_t1147783557;
// PlayerUserData
struct PlayerUserData_t4090529802;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FormPlayer
struct  FormPlayer_t4046102703  : public MonoBehaviour_t1158329972
{
public:
	// Player FormPlayer::PlayerData
	Player_t1147783557 * ___PlayerData_2;
	// PlayerUserData FormPlayer::PlayerUserData
	PlayerUserData_t4090529802 * ___PlayerUserData_3;
	// UnityEngine.GameObject FormPlayer::_head
	GameObject_t1756533147 * ____head_4;

public:
	inline static int32_t get_offset_of_PlayerData_2() { return static_cast<int32_t>(offsetof(FormPlayer_t4046102703, ___PlayerData_2)); }
	inline Player_t1147783557 * get_PlayerData_2() const { return ___PlayerData_2; }
	inline Player_t1147783557 ** get_address_of_PlayerData_2() { return &___PlayerData_2; }
	inline void set_PlayerData_2(Player_t1147783557 * value)
	{
		___PlayerData_2 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerData_2, value);
	}

	inline static int32_t get_offset_of_PlayerUserData_3() { return static_cast<int32_t>(offsetof(FormPlayer_t4046102703, ___PlayerUserData_3)); }
	inline PlayerUserData_t4090529802 * get_PlayerUserData_3() const { return ___PlayerUserData_3; }
	inline PlayerUserData_t4090529802 ** get_address_of_PlayerUserData_3() { return &___PlayerUserData_3; }
	inline void set_PlayerUserData_3(PlayerUserData_t4090529802 * value)
	{
		___PlayerUserData_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerUserData_3, value);
	}

	inline static int32_t get_offset_of__head_4() { return static_cast<int32_t>(offsetof(FormPlayer_t4046102703, ____head_4)); }
	inline GameObject_t1756533147 * get__head_4() const { return ____head_4; }
	inline GameObject_t1756533147 ** get_address_of__head_4() { return &____head_4; }
	inline void set__head_4(GameObject_t1756533147 * value)
	{
		____head_4 = value;
		Il2CppCodeGenWriteBarrier(&____head_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
