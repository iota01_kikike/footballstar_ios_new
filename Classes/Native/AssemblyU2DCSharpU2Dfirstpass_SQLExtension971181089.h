﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`2<System.Object,System.String>
struct Func_2_t2165275119;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLExtension
struct  SQLExtension_t971181089  : public Il2CppObject
{
public:

public:
};

struct SQLExtension_t971181089_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> SQLExtension::<>f__mg$cache0
	Func_2_t2165275119 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(SQLExtension_t971181089_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline Func_2_t2165275119 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline Func_2_t2165275119 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(Func_2_t2165275119 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
