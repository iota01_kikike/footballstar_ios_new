﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ActDetectorBase
struct  ActDetectorBase_t1905710121  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.ActDetectorBase::autoStart
	bool ___autoStart_6;
	// System.Boolean CodeStage.AntiCheat.Detectors.ActDetectorBase::keepAlive
	bool ___keepAlive_7;
	// System.Boolean CodeStage.AntiCheat.Detectors.ActDetectorBase::autoDispose
	bool ___autoDispose_8;
	// UnityEngine.Events.UnityEvent CodeStage.AntiCheat.Detectors.ActDetectorBase::detectionEvent
	UnityEvent_t408735097 * ___detectionEvent_9;
	// UnityEngine.Events.UnityAction CodeStage.AntiCheat.Detectors.ActDetectorBase::detectionAction
	UnityAction_t4025899511 * ___detectionAction_10;
	// System.Boolean CodeStage.AntiCheat.Detectors.ActDetectorBase::detectionEventHasListener
	bool ___detectionEventHasListener_11;
	// System.Boolean CodeStage.AntiCheat.Detectors.ActDetectorBase::isRunning
	bool ___isRunning_12;
	// System.Boolean CodeStage.AntiCheat.Detectors.ActDetectorBase::started
	bool ___started_13;

public:
	inline static int32_t get_offset_of_autoStart_6() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___autoStart_6)); }
	inline bool get_autoStart_6() const { return ___autoStart_6; }
	inline bool* get_address_of_autoStart_6() { return &___autoStart_6; }
	inline void set_autoStart_6(bool value)
	{
		___autoStart_6 = value;
	}

	inline static int32_t get_offset_of_keepAlive_7() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___keepAlive_7)); }
	inline bool get_keepAlive_7() const { return ___keepAlive_7; }
	inline bool* get_address_of_keepAlive_7() { return &___keepAlive_7; }
	inline void set_keepAlive_7(bool value)
	{
		___keepAlive_7 = value;
	}

	inline static int32_t get_offset_of_autoDispose_8() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___autoDispose_8)); }
	inline bool get_autoDispose_8() const { return ___autoDispose_8; }
	inline bool* get_address_of_autoDispose_8() { return &___autoDispose_8; }
	inline void set_autoDispose_8(bool value)
	{
		___autoDispose_8 = value;
	}

	inline static int32_t get_offset_of_detectionEvent_9() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___detectionEvent_9)); }
	inline UnityEvent_t408735097 * get_detectionEvent_9() const { return ___detectionEvent_9; }
	inline UnityEvent_t408735097 ** get_address_of_detectionEvent_9() { return &___detectionEvent_9; }
	inline void set_detectionEvent_9(UnityEvent_t408735097 * value)
	{
		___detectionEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___detectionEvent_9, value);
	}

	inline static int32_t get_offset_of_detectionAction_10() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___detectionAction_10)); }
	inline UnityAction_t4025899511 * get_detectionAction_10() const { return ___detectionAction_10; }
	inline UnityAction_t4025899511 ** get_address_of_detectionAction_10() { return &___detectionAction_10; }
	inline void set_detectionAction_10(UnityAction_t4025899511 * value)
	{
		___detectionAction_10 = value;
		Il2CppCodeGenWriteBarrier(&___detectionAction_10, value);
	}

	inline static int32_t get_offset_of_detectionEventHasListener_11() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___detectionEventHasListener_11)); }
	inline bool get_detectionEventHasListener_11() const { return ___detectionEventHasListener_11; }
	inline bool* get_address_of_detectionEventHasListener_11() { return &___detectionEventHasListener_11; }
	inline void set_detectionEventHasListener_11(bool value)
	{
		___detectionEventHasListener_11 = value;
	}

	inline static int32_t get_offset_of_isRunning_12() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___isRunning_12)); }
	inline bool get_isRunning_12() const { return ___isRunning_12; }
	inline bool* get_address_of_isRunning_12() { return &___isRunning_12; }
	inline void set_isRunning_12(bool value)
	{
		___isRunning_12 = value;
	}

	inline static int32_t get_offset_of_started_13() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121, ___started_13)); }
	inline bool get_started_13() const { return ___started_13; }
	inline bool* get_address_of_started_13() { return &___started_13; }
	inline void set_started_13(bool value)
	{
		___started_13 = value;
	}
};

struct ActDetectorBase_t1905710121_StaticFields
{
public:
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.ActDetectorBase::detectorsContainer
	GameObject_t1756533147 * ___detectorsContainer_5;

public:
	inline static int32_t get_offset_of_detectorsContainer_5() { return static_cast<int32_t>(offsetof(ActDetectorBase_t1905710121_StaticFields, ___detectorsContainer_5)); }
	inline GameObject_t1756533147 * get_detectorsContainer_5() const { return ___detectorsContainer_5; }
	inline GameObject_t1756533147 ** get_address_of_detectorsContainer_5() { return &___detectorsContainer_5; }
	inline void set_detectorsContainer_5(GameObject_t1756533147 * value)
	{
		___detectorsContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___detectorsContainer_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
