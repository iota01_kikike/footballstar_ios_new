﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Player
struct Player_t1147783557;
// PlayerUserData
struct PlayerUserData_t4090529802;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerInfoPopView
struct  PlayerInfoPopView_t4205922565  : public MonoBehaviour_t1158329972
{
public:
	// Player PlayerInfoPopView::CurPlayer
	Player_t1147783557 * ___CurPlayer_2;
	// PlayerUserData PlayerInfoPopView::PlayerUserData
	PlayerUserData_t4090529802 * ___PlayerUserData_3;
	// UnityEngine.UI.Text PlayerInfoPopView::NewPlayerText
	Text_t356221433 * ___NewPlayerText_4;
	// UnityEngine.UI.Text PlayerInfoPopView::NationText
	Text_t356221433 * ___NationText_5;
	// UnityEngine.UI.Text PlayerInfoPopView::NameText
	Text_t356221433 * ___NameText_6;
	// UnityEngine.UI.Text PlayerInfoPopView::GradeText
	Text_t356221433 * ___GradeText_7;
	// UnityEngine.UI.Text PlayerInfoPopView::BackNumberText
	Text_t356221433 * ___BackNumberText_8;
	// UnityEngine.UI.Text PlayerInfoPopView::PositionText
	Text_t356221433 * ___PositionText_9;
	// UnityEngine.UI.Text PlayerInfoPopView::LevelText
	Text_t356221433 * ___LevelText_10;
	// UnityEngine.UI.Text PlayerInfoPopView::OverallText
	Text_t356221433 * ___OverallText_11;
	// UnityEngine.UI.Text PlayerInfoPopView::CardCountText
	Text_t356221433 * ___CardCountText_12;
	// UnityEngine.UI.Text PlayerInfoPopView::CostText
	Text_t356221433 * ___CostText_13;
	// UnityEngine.GameObject PlayerInfoPopView::PlayerBoxObj
	GameObject_t1756533147 * ___PlayerBoxObj_14;
	// UnityEngine.UI.Image PlayerInfoPopView::GaugeBg
	Image_t2042527209 * ___GaugeBg_15;
	// UnityEngine.UI.Image PlayerInfoPopView::Gauge
	Image_t2042527209 * ___Gauge_16;
	// UnityEngine.UI.Image PlayerInfoPopView::StarImage
	Image_t2042527209 * ___StarImage_17;
	// UnityEngine.UI.Image PlayerInfoPopView::FlagImage
	Image_t2042527209 * ___FlagImage_18;
	// UnityEngine.UI.Button PlayerInfoPopView::LevelUpButton
	Button_t2872111280 * ___LevelUpButton_19;
	// UnityEngine.UI.Button PlayerInfoPopView::BackButton
	Button_t2872111280 * ___BackButton_20;
	// System.Int32 PlayerInfoPopView::_addCardCount
	int32_t ____addCardCount_21;

public:
	inline static int32_t get_offset_of_CurPlayer_2() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___CurPlayer_2)); }
	inline Player_t1147783557 * get_CurPlayer_2() const { return ___CurPlayer_2; }
	inline Player_t1147783557 ** get_address_of_CurPlayer_2() { return &___CurPlayer_2; }
	inline void set_CurPlayer_2(Player_t1147783557 * value)
	{
		___CurPlayer_2 = value;
		Il2CppCodeGenWriteBarrier(&___CurPlayer_2, value);
	}

	inline static int32_t get_offset_of_PlayerUserData_3() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___PlayerUserData_3)); }
	inline PlayerUserData_t4090529802 * get_PlayerUserData_3() const { return ___PlayerUserData_3; }
	inline PlayerUserData_t4090529802 ** get_address_of_PlayerUserData_3() { return &___PlayerUserData_3; }
	inline void set_PlayerUserData_3(PlayerUserData_t4090529802 * value)
	{
		___PlayerUserData_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerUserData_3, value);
	}

	inline static int32_t get_offset_of_NewPlayerText_4() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___NewPlayerText_4)); }
	inline Text_t356221433 * get_NewPlayerText_4() const { return ___NewPlayerText_4; }
	inline Text_t356221433 ** get_address_of_NewPlayerText_4() { return &___NewPlayerText_4; }
	inline void set_NewPlayerText_4(Text_t356221433 * value)
	{
		___NewPlayerText_4 = value;
		Il2CppCodeGenWriteBarrier(&___NewPlayerText_4, value);
	}

	inline static int32_t get_offset_of_NationText_5() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___NationText_5)); }
	inline Text_t356221433 * get_NationText_5() const { return ___NationText_5; }
	inline Text_t356221433 ** get_address_of_NationText_5() { return &___NationText_5; }
	inline void set_NationText_5(Text_t356221433 * value)
	{
		___NationText_5 = value;
		Il2CppCodeGenWriteBarrier(&___NationText_5, value);
	}

	inline static int32_t get_offset_of_NameText_6() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___NameText_6)); }
	inline Text_t356221433 * get_NameText_6() const { return ___NameText_6; }
	inline Text_t356221433 ** get_address_of_NameText_6() { return &___NameText_6; }
	inline void set_NameText_6(Text_t356221433 * value)
	{
		___NameText_6 = value;
		Il2CppCodeGenWriteBarrier(&___NameText_6, value);
	}

	inline static int32_t get_offset_of_GradeText_7() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___GradeText_7)); }
	inline Text_t356221433 * get_GradeText_7() const { return ___GradeText_7; }
	inline Text_t356221433 ** get_address_of_GradeText_7() { return &___GradeText_7; }
	inline void set_GradeText_7(Text_t356221433 * value)
	{
		___GradeText_7 = value;
		Il2CppCodeGenWriteBarrier(&___GradeText_7, value);
	}

	inline static int32_t get_offset_of_BackNumberText_8() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___BackNumberText_8)); }
	inline Text_t356221433 * get_BackNumberText_8() const { return ___BackNumberText_8; }
	inline Text_t356221433 ** get_address_of_BackNumberText_8() { return &___BackNumberText_8; }
	inline void set_BackNumberText_8(Text_t356221433 * value)
	{
		___BackNumberText_8 = value;
		Il2CppCodeGenWriteBarrier(&___BackNumberText_8, value);
	}

	inline static int32_t get_offset_of_PositionText_9() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___PositionText_9)); }
	inline Text_t356221433 * get_PositionText_9() const { return ___PositionText_9; }
	inline Text_t356221433 ** get_address_of_PositionText_9() { return &___PositionText_9; }
	inline void set_PositionText_9(Text_t356221433 * value)
	{
		___PositionText_9 = value;
		Il2CppCodeGenWriteBarrier(&___PositionText_9, value);
	}

	inline static int32_t get_offset_of_LevelText_10() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___LevelText_10)); }
	inline Text_t356221433 * get_LevelText_10() const { return ___LevelText_10; }
	inline Text_t356221433 ** get_address_of_LevelText_10() { return &___LevelText_10; }
	inline void set_LevelText_10(Text_t356221433 * value)
	{
		___LevelText_10 = value;
		Il2CppCodeGenWriteBarrier(&___LevelText_10, value);
	}

	inline static int32_t get_offset_of_OverallText_11() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___OverallText_11)); }
	inline Text_t356221433 * get_OverallText_11() const { return ___OverallText_11; }
	inline Text_t356221433 ** get_address_of_OverallText_11() { return &___OverallText_11; }
	inline void set_OverallText_11(Text_t356221433 * value)
	{
		___OverallText_11 = value;
		Il2CppCodeGenWriteBarrier(&___OverallText_11, value);
	}

	inline static int32_t get_offset_of_CardCountText_12() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___CardCountText_12)); }
	inline Text_t356221433 * get_CardCountText_12() const { return ___CardCountText_12; }
	inline Text_t356221433 ** get_address_of_CardCountText_12() { return &___CardCountText_12; }
	inline void set_CardCountText_12(Text_t356221433 * value)
	{
		___CardCountText_12 = value;
		Il2CppCodeGenWriteBarrier(&___CardCountText_12, value);
	}

	inline static int32_t get_offset_of_CostText_13() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___CostText_13)); }
	inline Text_t356221433 * get_CostText_13() const { return ___CostText_13; }
	inline Text_t356221433 ** get_address_of_CostText_13() { return &___CostText_13; }
	inline void set_CostText_13(Text_t356221433 * value)
	{
		___CostText_13 = value;
		Il2CppCodeGenWriteBarrier(&___CostText_13, value);
	}

	inline static int32_t get_offset_of_PlayerBoxObj_14() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___PlayerBoxObj_14)); }
	inline GameObject_t1756533147 * get_PlayerBoxObj_14() const { return ___PlayerBoxObj_14; }
	inline GameObject_t1756533147 ** get_address_of_PlayerBoxObj_14() { return &___PlayerBoxObj_14; }
	inline void set_PlayerBoxObj_14(GameObject_t1756533147 * value)
	{
		___PlayerBoxObj_14 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerBoxObj_14, value);
	}

	inline static int32_t get_offset_of_GaugeBg_15() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___GaugeBg_15)); }
	inline Image_t2042527209 * get_GaugeBg_15() const { return ___GaugeBg_15; }
	inline Image_t2042527209 ** get_address_of_GaugeBg_15() { return &___GaugeBg_15; }
	inline void set_GaugeBg_15(Image_t2042527209 * value)
	{
		___GaugeBg_15 = value;
		Il2CppCodeGenWriteBarrier(&___GaugeBg_15, value);
	}

	inline static int32_t get_offset_of_Gauge_16() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___Gauge_16)); }
	inline Image_t2042527209 * get_Gauge_16() const { return ___Gauge_16; }
	inline Image_t2042527209 ** get_address_of_Gauge_16() { return &___Gauge_16; }
	inline void set_Gauge_16(Image_t2042527209 * value)
	{
		___Gauge_16 = value;
		Il2CppCodeGenWriteBarrier(&___Gauge_16, value);
	}

	inline static int32_t get_offset_of_StarImage_17() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___StarImage_17)); }
	inline Image_t2042527209 * get_StarImage_17() const { return ___StarImage_17; }
	inline Image_t2042527209 ** get_address_of_StarImage_17() { return &___StarImage_17; }
	inline void set_StarImage_17(Image_t2042527209 * value)
	{
		___StarImage_17 = value;
		Il2CppCodeGenWriteBarrier(&___StarImage_17, value);
	}

	inline static int32_t get_offset_of_FlagImage_18() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___FlagImage_18)); }
	inline Image_t2042527209 * get_FlagImage_18() const { return ___FlagImage_18; }
	inline Image_t2042527209 ** get_address_of_FlagImage_18() { return &___FlagImage_18; }
	inline void set_FlagImage_18(Image_t2042527209 * value)
	{
		___FlagImage_18 = value;
		Il2CppCodeGenWriteBarrier(&___FlagImage_18, value);
	}

	inline static int32_t get_offset_of_LevelUpButton_19() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___LevelUpButton_19)); }
	inline Button_t2872111280 * get_LevelUpButton_19() const { return ___LevelUpButton_19; }
	inline Button_t2872111280 ** get_address_of_LevelUpButton_19() { return &___LevelUpButton_19; }
	inline void set_LevelUpButton_19(Button_t2872111280 * value)
	{
		___LevelUpButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___LevelUpButton_19, value);
	}

	inline static int32_t get_offset_of_BackButton_20() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ___BackButton_20)); }
	inline Button_t2872111280 * get_BackButton_20() const { return ___BackButton_20; }
	inline Button_t2872111280 ** get_address_of_BackButton_20() { return &___BackButton_20; }
	inline void set_BackButton_20(Button_t2872111280 * value)
	{
		___BackButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___BackButton_20, value);
	}

	inline static int32_t get_offset_of__addCardCount_21() { return static_cast<int32_t>(offsetof(PlayerInfoPopView_t4205922565, ____addCardCount_21)); }
	inline int32_t get__addCardCount_21() const { return ____addCardCount_21; }
	inline int32_t* get_address_of__addCardCount_21() { return &____addCardCount_21; }
	inline void set__addCardCount_21(int32_t value)
	{
		____addCardCount_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
