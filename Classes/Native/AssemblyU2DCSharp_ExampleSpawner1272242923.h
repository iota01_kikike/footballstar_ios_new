﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSpawner1
struct  ExampleSpawner1_t272242923  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ExampleSpawner1::SpawnRange
	int32_t ___SpawnRange_2;
	// System.Single ExampleSpawner1::SpawnDelay
	float ___SpawnDelay_3;
	// UnityEngine.GameObject ExampleSpawner1::spawnPattern
	GameObject_t1756533147 * ___spawnPattern_4;
	// System.Boolean ExampleSpawner1::IsStarted
	bool ___IsStarted_5;

public:
	inline static int32_t get_offset_of_SpawnRange_2() { return static_cast<int32_t>(offsetof(ExampleSpawner1_t272242923, ___SpawnRange_2)); }
	inline int32_t get_SpawnRange_2() const { return ___SpawnRange_2; }
	inline int32_t* get_address_of_SpawnRange_2() { return &___SpawnRange_2; }
	inline void set_SpawnRange_2(int32_t value)
	{
		___SpawnRange_2 = value;
	}

	inline static int32_t get_offset_of_SpawnDelay_3() { return static_cast<int32_t>(offsetof(ExampleSpawner1_t272242923, ___SpawnDelay_3)); }
	inline float get_SpawnDelay_3() const { return ___SpawnDelay_3; }
	inline float* get_address_of_SpawnDelay_3() { return &___SpawnDelay_3; }
	inline void set_SpawnDelay_3(float value)
	{
		___SpawnDelay_3 = value;
	}

	inline static int32_t get_offset_of_spawnPattern_4() { return static_cast<int32_t>(offsetof(ExampleSpawner1_t272242923, ___spawnPattern_4)); }
	inline GameObject_t1756533147 * get_spawnPattern_4() const { return ___spawnPattern_4; }
	inline GameObject_t1756533147 ** get_address_of_spawnPattern_4() { return &___spawnPattern_4; }
	inline void set_spawnPattern_4(GameObject_t1756533147 * value)
	{
		___spawnPattern_4 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPattern_4, value);
	}

	inline static int32_t get_offset_of_IsStarted_5() { return static_cast<int32_t>(offsetof(ExampleSpawner1_t272242923, ___IsStarted_5)); }
	inline bool get_IsStarted_5() const { return ___IsStarted_5; }
	inline bool* get_address_of_IsStarted_5() { return &___IsStarted_5; }
	inline void set_IsStarted_5(bool value)
	{
		___IsStarted_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
