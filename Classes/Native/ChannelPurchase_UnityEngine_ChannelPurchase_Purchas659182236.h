﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t2973420583;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ChannelPurchase.PurchaseService
struct  PurchaseService_t659182236  : public Il2CppObject
{
public:

public:
};

struct PurchaseService_t659182236_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.ChannelPurchase.PurchaseService::serviceClass
	AndroidJavaClass_t2973420583 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(PurchaseService_t659182236_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_t2973420583 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_t2973420583 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_t2973420583 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier(&___serviceClass_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
