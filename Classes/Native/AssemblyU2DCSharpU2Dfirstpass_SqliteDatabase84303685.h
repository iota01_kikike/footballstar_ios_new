﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SqliteDatabase
struct  SqliteDatabase_t84303685  : public Il2CppObject
{
public:
	// System.IntPtr SqliteDatabase::_connection
	IntPtr_t ____connection_11;
	// System.Boolean SqliteDatabase::<IsConnectionOpen>k__BackingField
	bool ___U3CIsConnectionOpenU3Ek__BackingField_12;
	// System.Boolean SqliteDatabase::CanExQuery
	bool ___CanExQuery_13;
	// System.String SqliteDatabase::directoryPath
	String_t* ___directoryPath_14;
	// System.String SqliteDatabase::<pathDB>k__BackingField
	String_t* ___U3CpathDBU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of__connection_11() { return static_cast<int32_t>(offsetof(SqliteDatabase_t84303685, ____connection_11)); }
	inline IntPtr_t get__connection_11() const { return ____connection_11; }
	inline IntPtr_t* get_address_of__connection_11() { return &____connection_11; }
	inline void set__connection_11(IntPtr_t value)
	{
		____connection_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsConnectionOpenU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SqliteDatabase_t84303685, ___U3CIsConnectionOpenU3Ek__BackingField_12)); }
	inline bool get_U3CIsConnectionOpenU3Ek__BackingField_12() const { return ___U3CIsConnectionOpenU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsConnectionOpenU3Ek__BackingField_12() { return &___U3CIsConnectionOpenU3Ek__BackingField_12; }
	inline void set_U3CIsConnectionOpenU3Ek__BackingField_12(bool value)
	{
		___U3CIsConnectionOpenU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_CanExQuery_13() { return static_cast<int32_t>(offsetof(SqliteDatabase_t84303685, ___CanExQuery_13)); }
	inline bool get_CanExQuery_13() const { return ___CanExQuery_13; }
	inline bool* get_address_of_CanExQuery_13() { return &___CanExQuery_13; }
	inline void set_CanExQuery_13(bool value)
	{
		___CanExQuery_13 = value;
	}

	inline static int32_t get_offset_of_directoryPath_14() { return static_cast<int32_t>(offsetof(SqliteDatabase_t84303685, ___directoryPath_14)); }
	inline String_t* get_directoryPath_14() const { return ___directoryPath_14; }
	inline String_t** get_address_of_directoryPath_14() { return &___directoryPath_14; }
	inline void set_directoryPath_14(String_t* value)
	{
		___directoryPath_14 = value;
		Il2CppCodeGenWriteBarrier(&___directoryPath_14, value);
	}

	inline static int32_t get_offset_of_U3CpathDBU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(SqliteDatabase_t84303685, ___U3CpathDBU3Ek__BackingField_15)); }
	inline String_t* get_U3CpathDBU3Ek__BackingField_15() const { return ___U3CpathDBU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CpathDBU3Ek__BackingField_15() { return &___U3CpathDBU3Ek__BackingField_15; }
	inline void set_U3CpathDBU3Ek__BackingField_15(String_t* value)
	{
		___U3CpathDBU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathDBU3Ek__BackingField_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
