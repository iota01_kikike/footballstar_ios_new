﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O337339225.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TraineeUnit
struct  TraineeUnit_t2929148814  : public Il2CppObject
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt TraineeUnit::Index
	ObscuredInt_t796441056  ___Index_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt TraineeUnit::Level
	ObscuredInt_t796441056  ___Level_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt TraineeUnit::Value
	ObscuredInt_t796441056  ___Value_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool TraineeUnit::HaveCoin
	ObscuredBool_t337339225  ___HaveCoin_3;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool TraineeUnit::IsStar
	ObscuredBool_t337339225  ___IsStar_4;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool TraineeUnit::IsUpgraded
	ObscuredBool_t337339225  ___IsUpgraded_5;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(TraineeUnit_t2929148814, ___Index_0)); }
	inline ObscuredInt_t796441056  get_Index_0() const { return ___Index_0; }
	inline ObscuredInt_t796441056 * get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(ObscuredInt_t796441056  value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Level_1() { return static_cast<int32_t>(offsetof(TraineeUnit_t2929148814, ___Level_1)); }
	inline ObscuredInt_t796441056  get_Level_1() const { return ___Level_1; }
	inline ObscuredInt_t796441056 * get_address_of_Level_1() { return &___Level_1; }
	inline void set_Level_1(ObscuredInt_t796441056  value)
	{
		___Level_1 = value;
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(TraineeUnit_t2929148814, ___Value_2)); }
	inline ObscuredInt_t796441056  get_Value_2() const { return ___Value_2; }
	inline ObscuredInt_t796441056 * get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(ObscuredInt_t796441056  value)
	{
		___Value_2 = value;
	}

	inline static int32_t get_offset_of_HaveCoin_3() { return static_cast<int32_t>(offsetof(TraineeUnit_t2929148814, ___HaveCoin_3)); }
	inline ObscuredBool_t337339225  get_HaveCoin_3() const { return ___HaveCoin_3; }
	inline ObscuredBool_t337339225 * get_address_of_HaveCoin_3() { return &___HaveCoin_3; }
	inline void set_HaveCoin_3(ObscuredBool_t337339225  value)
	{
		___HaveCoin_3 = value;
	}

	inline static int32_t get_offset_of_IsStar_4() { return static_cast<int32_t>(offsetof(TraineeUnit_t2929148814, ___IsStar_4)); }
	inline ObscuredBool_t337339225  get_IsStar_4() const { return ___IsStar_4; }
	inline ObscuredBool_t337339225 * get_address_of_IsStar_4() { return &___IsStar_4; }
	inline void set_IsStar_4(ObscuredBool_t337339225  value)
	{
		___IsStar_4 = value;
	}

	inline static int32_t get_offset_of_IsUpgraded_5() { return static_cast<int32_t>(offsetof(TraineeUnit_t2929148814, ___IsUpgraded_5)); }
	inline ObscuredBool_t337339225  get_IsUpgraded_5() const { return ___IsUpgraded_5; }
	inline ObscuredBool_t337339225 * get_address_of_IsUpgraded_5() { return &___IsUpgraded_5; }
	inline void set_IsUpgraded_5(ObscuredBool_t337339225  value)
	{
		___IsUpgraded_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
