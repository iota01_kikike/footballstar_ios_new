﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// CollectMenuCell
struct CollectMenuCell_t2927503929;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.Dictionary`2<System.Int32,Nation>
struct Dictionary_2_t2439496172;
// NationSimpleInfo[]
struct NationSimpleInfoU5BU5D_t321925280;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectNationPop
struct  SelectNationPop_t2763424430  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button SelectNationPop::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// CollectMenuCell SelectNationPop::m_cellPrefab
	CollectMenuCell_t2927503929 * ___m_cellPrefab_3;
	// Tacticsoft.TableView SelectNationPop::m_tableView
	TableView_t3179510217 * ___m_tableView_4;
	// System.Int32 SelectNationPop::m_numInstancesCreated
	int32_t ___m_numInstancesCreated_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,Nation> SelectNationPop::nationDict
	Dictionary_2_t2439496172 * ___nationDict_6;
	// NationSimpleInfo[] SelectNationPop::nationInfoList
	NationSimpleInfoU5BU5D_t321925280* ___nationInfoList_7;
	// System.Action`1<System.Int32> SelectNationPop::Callback
	Action_1_t1873676830 * ___Callback_8;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_m_cellPrefab_3() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___m_cellPrefab_3)); }
	inline CollectMenuCell_t2927503929 * get_m_cellPrefab_3() const { return ___m_cellPrefab_3; }
	inline CollectMenuCell_t2927503929 ** get_address_of_m_cellPrefab_3() { return &___m_cellPrefab_3; }
	inline void set_m_cellPrefab_3(CollectMenuCell_t2927503929 * value)
	{
		___m_cellPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_3, value);
	}

	inline static int32_t get_offset_of_m_tableView_4() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___m_tableView_4)); }
	inline TableView_t3179510217 * get_m_tableView_4() const { return ___m_tableView_4; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_4() { return &___m_tableView_4; }
	inline void set_m_tableView_4(TableView_t3179510217 * value)
	{
		___m_tableView_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_4, value);
	}

	inline static int32_t get_offset_of_m_numInstancesCreated_5() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___m_numInstancesCreated_5)); }
	inline int32_t get_m_numInstancesCreated_5() const { return ___m_numInstancesCreated_5; }
	inline int32_t* get_address_of_m_numInstancesCreated_5() { return &___m_numInstancesCreated_5; }
	inline void set_m_numInstancesCreated_5(int32_t value)
	{
		___m_numInstancesCreated_5 = value;
	}

	inline static int32_t get_offset_of_nationDict_6() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___nationDict_6)); }
	inline Dictionary_2_t2439496172 * get_nationDict_6() const { return ___nationDict_6; }
	inline Dictionary_2_t2439496172 ** get_address_of_nationDict_6() { return &___nationDict_6; }
	inline void set_nationDict_6(Dictionary_2_t2439496172 * value)
	{
		___nationDict_6 = value;
		Il2CppCodeGenWriteBarrier(&___nationDict_6, value);
	}

	inline static int32_t get_offset_of_nationInfoList_7() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___nationInfoList_7)); }
	inline NationSimpleInfoU5BU5D_t321925280* get_nationInfoList_7() const { return ___nationInfoList_7; }
	inline NationSimpleInfoU5BU5D_t321925280** get_address_of_nationInfoList_7() { return &___nationInfoList_7; }
	inline void set_nationInfoList_7(NationSimpleInfoU5BU5D_t321925280* value)
	{
		___nationInfoList_7 = value;
		Il2CppCodeGenWriteBarrier(&___nationInfoList_7, value);
	}

	inline static int32_t get_offset_of_Callback_8() { return static_cast<int32_t>(offsetof(SelectNationPop_t2763424430, ___Callback_8)); }
	inline Action_1_t1873676830 * get_Callback_8() const { return ___Callback_8; }
	inline Action_1_t1873676830 ** get_address_of_Callback_8() { return &___Callback_8; }
	inline void set_Callback_8(Action_1_t1873676830 * value)
	{
		___Callback_8 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
