﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3265587138.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O337339225.h"

// System.Collections.Generic.Dictionary`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>
struct Dictionary_2_t1180426443;
// System.Collections.Generic.Dictionary`2<System.Int32,TraineeUnit>
struct Dictionary_2_t1936974449;
// System.Collections.Generic.Dictionary`2<System.Int32,PlayerUserData>
struct Dictionary_2_t3098355437;
// System.Collections.Generic.Dictionary`2<System.Int32,SquadUserData>
struct Dictionary_2_t2014872700;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1445386684;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// User
struct  User_t719925459  : public Il2CppObject
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::coin
	ObscuredInt_t796441056  ___coin_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::gem
	ObscuredInt_t796441056  ___gem_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::highScore
	ObscuredInt_t796441056  ___highScore_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::totalOverall
	ObscuredInt_t796441056  ___totalOverall_3;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::gameScore
	ObscuredInt_t796441056  ___gameScore_4;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::gameGem
	ObscuredInt_t796441056  ___gameGem_5;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt User::UnlockUnitLevel
	ObscuredInt_t796441056  ___UnlockUnitLevel_6;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble User::PlayTime
	ObscuredDouble_t3265587138  ___PlayTime_7;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble User::FreeScoutTime
	ObscuredDouble_t3265587138  ___FreeScoutTime_8;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble User::DirectBuyRefreshTime
	ObscuredDouble_t3265587138  ___DirectBuyRefreshTime_9;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble User::DoubleChanceTime
	ObscuredDouble_t3265587138  ___DoubleChanceTime_10;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble User::FreeGemTime
	ObscuredDouble_t3265587138  ___FreeGemTime_11;
	// System.Collections.Generic.Dictionary`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt> User::DirectPlayerDict
	Dictionary_2_t1180426443 * ___DirectPlayerDict_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,TraineeUnit> User::_unitDataDict
	Dictionary_2_t1936974449 * ____unitDataDict_13;
	// System.Collections.Generic.Dictionary`2<System.Int32,PlayerUserData> User::_playerDataDict
	Dictionary_2_t3098355437 * ____playerDataDict_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,SquadUserData> User::_squadDataDict
	Dictionary_2_t2014872700 * ____squadDataDict_15;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool User::IsGuideFin
	ObscuredBool_t337339225  ___IsGuideFin_16;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> User::CouponDict
	Dictionary_2_t1445386684 * ___CouponDict_17;
	// System.Boolean User::FacebookLiked
	bool ___FacebookLiked_18;
	// System.Boolean User::InstagramFollowed
	bool ___InstagramFollowed_19;

public:
	inline static int32_t get_offset_of_coin_0() { return static_cast<int32_t>(offsetof(User_t719925459, ___coin_0)); }
	inline ObscuredInt_t796441056  get_coin_0() const { return ___coin_0; }
	inline ObscuredInt_t796441056 * get_address_of_coin_0() { return &___coin_0; }
	inline void set_coin_0(ObscuredInt_t796441056  value)
	{
		___coin_0 = value;
	}

	inline static int32_t get_offset_of_gem_1() { return static_cast<int32_t>(offsetof(User_t719925459, ___gem_1)); }
	inline ObscuredInt_t796441056  get_gem_1() const { return ___gem_1; }
	inline ObscuredInt_t796441056 * get_address_of_gem_1() { return &___gem_1; }
	inline void set_gem_1(ObscuredInt_t796441056  value)
	{
		___gem_1 = value;
	}

	inline static int32_t get_offset_of_highScore_2() { return static_cast<int32_t>(offsetof(User_t719925459, ___highScore_2)); }
	inline ObscuredInt_t796441056  get_highScore_2() const { return ___highScore_2; }
	inline ObscuredInt_t796441056 * get_address_of_highScore_2() { return &___highScore_2; }
	inline void set_highScore_2(ObscuredInt_t796441056  value)
	{
		___highScore_2 = value;
	}

	inline static int32_t get_offset_of_totalOverall_3() { return static_cast<int32_t>(offsetof(User_t719925459, ___totalOverall_3)); }
	inline ObscuredInt_t796441056  get_totalOverall_3() const { return ___totalOverall_3; }
	inline ObscuredInt_t796441056 * get_address_of_totalOverall_3() { return &___totalOverall_3; }
	inline void set_totalOverall_3(ObscuredInt_t796441056  value)
	{
		___totalOverall_3 = value;
	}

	inline static int32_t get_offset_of_gameScore_4() { return static_cast<int32_t>(offsetof(User_t719925459, ___gameScore_4)); }
	inline ObscuredInt_t796441056  get_gameScore_4() const { return ___gameScore_4; }
	inline ObscuredInt_t796441056 * get_address_of_gameScore_4() { return &___gameScore_4; }
	inline void set_gameScore_4(ObscuredInt_t796441056  value)
	{
		___gameScore_4 = value;
	}

	inline static int32_t get_offset_of_gameGem_5() { return static_cast<int32_t>(offsetof(User_t719925459, ___gameGem_5)); }
	inline ObscuredInt_t796441056  get_gameGem_5() const { return ___gameGem_5; }
	inline ObscuredInt_t796441056 * get_address_of_gameGem_5() { return &___gameGem_5; }
	inline void set_gameGem_5(ObscuredInt_t796441056  value)
	{
		___gameGem_5 = value;
	}

	inline static int32_t get_offset_of_UnlockUnitLevel_6() { return static_cast<int32_t>(offsetof(User_t719925459, ___UnlockUnitLevel_6)); }
	inline ObscuredInt_t796441056  get_UnlockUnitLevel_6() const { return ___UnlockUnitLevel_6; }
	inline ObscuredInt_t796441056 * get_address_of_UnlockUnitLevel_6() { return &___UnlockUnitLevel_6; }
	inline void set_UnlockUnitLevel_6(ObscuredInt_t796441056  value)
	{
		___UnlockUnitLevel_6 = value;
	}

	inline static int32_t get_offset_of_PlayTime_7() { return static_cast<int32_t>(offsetof(User_t719925459, ___PlayTime_7)); }
	inline ObscuredDouble_t3265587138  get_PlayTime_7() const { return ___PlayTime_7; }
	inline ObscuredDouble_t3265587138 * get_address_of_PlayTime_7() { return &___PlayTime_7; }
	inline void set_PlayTime_7(ObscuredDouble_t3265587138  value)
	{
		___PlayTime_7 = value;
	}

	inline static int32_t get_offset_of_FreeScoutTime_8() { return static_cast<int32_t>(offsetof(User_t719925459, ___FreeScoutTime_8)); }
	inline ObscuredDouble_t3265587138  get_FreeScoutTime_8() const { return ___FreeScoutTime_8; }
	inline ObscuredDouble_t3265587138 * get_address_of_FreeScoutTime_8() { return &___FreeScoutTime_8; }
	inline void set_FreeScoutTime_8(ObscuredDouble_t3265587138  value)
	{
		___FreeScoutTime_8 = value;
	}

	inline static int32_t get_offset_of_DirectBuyRefreshTime_9() { return static_cast<int32_t>(offsetof(User_t719925459, ___DirectBuyRefreshTime_9)); }
	inline ObscuredDouble_t3265587138  get_DirectBuyRefreshTime_9() const { return ___DirectBuyRefreshTime_9; }
	inline ObscuredDouble_t3265587138 * get_address_of_DirectBuyRefreshTime_9() { return &___DirectBuyRefreshTime_9; }
	inline void set_DirectBuyRefreshTime_9(ObscuredDouble_t3265587138  value)
	{
		___DirectBuyRefreshTime_9 = value;
	}

	inline static int32_t get_offset_of_DoubleChanceTime_10() { return static_cast<int32_t>(offsetof(User_t719925459, ___DoubleChanceTime_10)); }
	inline ObscuredDouble_t3265587138  get_DoubleChanceTime_10() const { return ___DoubleChanceTime_10; }
	inline ObscuredDouble_t3265587138 * get_address_of_DoubleChanceTime_10() { return &___DoubleChanceTime_10; }
	inline void set_DoubleChanceTime_10(ObscuredDouble_t3265587138  value)
	{
		___DoubleChanceTime_10 = value;
	}

	inline static int32_t get_offset_of_FreeGemTime_11() { return static_cast<int32_t>(offsetof(User_t719925459, ___FreeGemTime_11)); }
	inline ObscuredDouble_t3265587138  get_FreeGemTime_11() const { return ___FreeGemTime_11; }
	inline ObscuredDouble_t3265587138 * get_address_of_FreeGemTime_11() { return &___FreeGemTime_11; }
	inline void set_FreeGemTime_11(ObscuredDouble_t3265587138  value)
	{
		___FreeGemTime_11 = value;
	}

	inline static int32_t get_offset_of_DirectPlayerDict_12() { return static_cast<int32_t>(offsetof(User_t719925459, ___DirectPlayerDict_12)); }
	inline Dictionary_2_t1180426443 * get_DirectPlayerDict_12() const { return ___DirectPlayerDict_12; }
	inline Dictionary_2_t1180426443 ** get_address_of_DirectPlayerDict_12() { return &___DirectPlayerDict_12; }
	inline void set_DirectPlayerDict_12(Dictionary_2_t1180426443 * value)
	{
		___DirectPlayerDict_12 = value;
		Il2CppCodeGenWriteBarrier(&___DirectPlayerDict_12, value);
	}

	inline static int32_t get_offset_of__unitDataDict_13() { return static_cast<int32_t>(offsetof(User_t719925459, ____unitDataDict_13)); }
	inline Dictionary_2_t1936974449 * get__unitDataDict_13() const { return ____unitDataDict_13; }
	inline Dictionary_2_t1936974449 ** get_address_of__unitDataDict_13() { return &____unitDataDict_13; }
	inline void set__unitDataDict_13(Dictionary_2_t1936974449 * value)
	{
		____unitDataDict_13 = value;
		Il2CppCodeGenWriteBarrier(&____unitDataDict_13, value);
	}

	inline static int32_t get_offset_of__playerDataDict_14() { return static_cast<int32_t>(offsetof(User_t719925459, ____playerDataDict_14)); }
	inline Dictionary_2_t3098355437 * get__playerDataDict_14() const { return ____playerDataDict_14; }
	inline Dictionary_2_t3098355437 ** get_address_of__playerDataDict_14() { return &____playerDataDict_14; }
	inline void set__playerDataDict_14(Dictionary_2_t3098355437 * value)
	{
		____playerDataDict_14 = value;
		Il2CppCodeGenWriteBarrier(&____playerDataDict_14, value);
	}

	inline static int32_t get_offset_of__squadDataDict_15() { return static_cast<int32_t>(offsetof(User_t719925459, ____squadDataDict_15)); }
	inline Dictionary_2_t2014872700 * get__squadDataDict_15() const { return ____squadDataDict_15; }
	inline Dictionary_2_t2014872700 ** get_address_of__squadDataDict_15() { return &____squadDataDict_15; }
	inline void set__squadDataDict_15(Dictionary_2_t2014872700 * value)
	{
		____squadDataDict_15 = value;
		Il2CppCodeGenWriteBarrier(&____squadDataDict_15, value);
	}

	inline static int32_t get_offset_of_IsGuideFin_16() { return static_cast<int32_t>(offsetof(User_t719925459, ___IsGuideFin_16)); }
	inline ObscuredBool_t337339225  get_IsGuideFin_16() const { return ___IsGuideFin_16; }
	inline ObscuredBool_t337339225 * get_address_of_IsGuideFin_16() { return &___IsGuideFin_16; }
	inline void set_IsGuideFin_16(ObscuredBool_t337339225  value)
	{
		___IsGuideFin_16 = value;
	}

	inline static int32_t get_offset_of_CouponDict_17() { return static_cast<int32_t>(offsetof(User_t719925459, ___CouponDict_17)); }
	inline Dictionary_2_t1445386684 * get_CouponDict_17() const { return ___CouponDict_17; }
	inline Dictionary_2_t1445386684 ** get_address_of_CouponDict_17() { return &___CouponDict_17; }
	inline void set_CouponDict_17(Dictionary_2_t1445386684 * value)
	{
		___CouponDict_17 = value;
		Il2CppCodeGenWriteBarrier(&___CouponDict_17, value);
	}

	inline static int32_t get_offset_of_FacebookLiked_18() { return static_cast<int32_t>(offsetof(User_t719925459, ___FacebookLiked_18)); }
	inline bool get_FacebookLiked_18() const { return ___FacebookLiked_18; }
	inline bool* get_address_of_FacebookLiked_18() { return &___FacebookLiked_18; }
	inline void set_FacebookLiked_18(bool value)
	{
		___FacebookLiked_18 = value;
	}

	inline static int32_t get_offset_of_InstagramFollowed_19() { return static_cast<int32_t>(offsetof(User_t719925459, ___InstagramFollowed_19)); }
	inline bool get_InstagramFollowed_19() const { return ___InstagramFollowed_19; }
	inline bool* get_address_of_InstagramFollowed_19() { return &___InstagramFollowed_19; }
	inline void set_InstagramFollowed_19(bool value)
	{
		___InstagramFollowed_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
