﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// Nation
struct Nation_t3431670537;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NationSimpleInfo
struct  NationSimpleInfo_t3057834637 
{
public:
	// Nation NationSimpleInfo::Nation
	Nation_t3431670537 * ___Nation_0;
	// System.Int32 NationSimpleInfo::PlayerTotalCnt
	int32_t ___PlayerTotalCnt_1;
	// System.Int32 NationSimpleInfo::PlayerHaveCnt
	int32_t ___PlayerHaveCnt_2;
	// System.Boolean NationSimpleInfo::BadgeOn
	bool ___BadgeOn_3;

public:
	inline static int32_t get_offset_of_Nation_0() { return static_cast<int32_t>(offsetof(NationSimpleInfo_t3057834637, ___Nation_0)); }
	inline Nation_t3431670537 * get_Nation_0() const { return ___Nation_0; }
	inline Nation_t3431670537 ** get_address_of_Nation_0() { return &___Nation_0; }
	inline void set_Nation_0(Nation_t3431670537 * value)
	{
		___Nation_0 = value;
		Il2CppCodeGenWriteBarrier(&___Nation_0, value);
	}

	inline static int32_t get_offset_of_PlayerTotalCnt_1() { return static_cast<int32_t>(offsetof(NationSimpleInfo_t3057834637, ___PlayerTotalCnt_1)); }
	inline int32_t get_PlayerTotalCnt_1() const { return ___PlayerTotalCnt_1; }
	inline int32_t* get_address_of_PlayerTotalCnt_1() { return &___PlayerTotalCnt_1; }
	inline void set_PlayerTotalCnt_1(int32_t value)
	{
		___PlayerTotalCnt_1 = value;
	}

	inline static int32_t get_offset_of_PlayerHaveCnt_2() { return static_cast<int32_t>(offsetof(NationSimpleInfo_t3057834637, ___PlayerHaveCnt_2)); }
	inline int32_t get_PlayerHaveCnt_2() const { return ___PlayerHaveCnt_2; }
	inline int32_t* get_address_of_PlayerHaveCnt_2() { return &___PlayerHaveCnt_2; }
	inline void set_PlayerHaveCnt_2(int32_t value)
	{
		___PlayerHaveCnt_2 = value;
	}

	inline static int32_t get_offset_of_BadgeOn_3() { return static_cast<int32_t>(offsetof(NationSimpleInfo_t3057834637, ___BadgeOn_3)); }
	inline bool get_BadgeOn_3() const { return ___BadgeOn_3; }
	inline bool* get_address_of_BadgeOn_3() { return &___BadgeOn_3; }
	inline void set_BadgeOn_3(bool value)
	{
		___BadgeOn_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NationSimpleInfo
struct NationSimpleInfo_t3057834637_marshaled_pinvoke
{
	Nation_t3431670537 * ___Nation_0;
	int32_t ___PlayerTotalCnt_1;
	int32_t ___PlayerHaveCnt_2;
	int32_t ___BadgeOn_3;
};
// Native definition for COM marshalling of NationSimpleInfo
struct NationSimpleInfo_t3057834637_marshaled_com
{
	Nation_t3431670537 * ___Nation_0;
	int32_t ___PlayerTotalCnt_1;
	int32_t ___PlayerHaveCnt_2;
	int32_t ___BadgeOn_3;
};
