﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2998525377.h"

// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.APITester
struct  APITester_t1937545187  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 CodeStage.AdvancedFPSCounter.APITester::selectedTab
	int32_t ___selectedTab_2;
	// System.String[] CodeStage.AdvancedFPSCounter.APITester::tabs
	StringU5BU5D_t1642385972* ___tabs_3;
	// CodeStage.AdvancedFPSCounter.FPSLevel CodeStage.AdvancedFPSCounter.APITester::currentFPSLevel
	uint8_t ___currentFPSLevel_4;

public:
	inline static int32_t get_offset_of_selectedTab_2() { return static_cast<int32_t>(offsetof(APITester_t1937545187, ___selectedTab_2)); }
	inline int32_t get_selectedTab_2() const { return ___selectedTab_2; }
	inline int32_t* get_address_of_selectedTab_2() { return &___selectedTab_2; }
	inline void set_selectedTab_2(int32_t value)
	{
		___selectedTab_2 = value;
	}

	inline static int32_t get_offset_of_tabs_3() { return static_cast<int32_t>(offsetof(APITester_t1937545187, ___tabs_3)); }
	inline StringU5BU5D_t1642385972* get_tabs_3() const { return ___tabs_3; }
	inline StringU5BU5D_t1642385972** get_address_of_tabs_3() { return &___tabs_3; }
	inline void set_tabs_3(StringU5BU5D_t1642385972* value)
	{
		___tabs_3 = value;
		Il2CppCodeGenWriteBarrier(&___tabs_3, value);
	}

	inline static int32_t get_offset_of_currentFPSLevel_4() { return static_cast<int32_t>(offsetof(APITester_t1937545187, ___currentFPSLevel_4)); }
	inline uint8_t get_currentFPSLevel_4() const { return ___currentFPSLevel_4; }
	inline uint8_t* get_address_of_currentFPSLevel_4() { return &___currentFPSLevel_4; }
	inline void set_currentFPSLevel_4(uint8_t value)
	{
		___currentFPSLevel_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
