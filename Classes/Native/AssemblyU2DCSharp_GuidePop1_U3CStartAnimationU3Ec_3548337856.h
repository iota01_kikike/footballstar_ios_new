﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// GuidePop1
struct GuidePop1_t959359454;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuidePop1/<StartAnimation>c__AnonStorey0
struct  U3CStartAnimationU3Ec__AnonStorey0_t3548337856  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 GuidePop1/<StartAnimation>c__AnonStorey0::originScale
	Vector3_t2243707580  ___originScale_0;
	// UnityEngine.Vector3 GuidePop1/<StartAnimation>c__AnonStorey0::originPos2
	Vector3_t2243707580  ___originPos2_1;
	// UnityEngine.Vector3 GuidePop1/<StartAnimation>c__AnonStorey0::originPos3
	Vector3_t2243707580  ___originPos3_2;
	// UnityEngine.Vector3 GuidePop1/<StartAnimation>c__AnonStorey0::originPos1
	Vector3_t2243707580  ___originPos1_3;
	// GuidePop1 GuidePop1/<StartAnimation>c__AnonStorey0::$this
	GuidePop1_t959359454 * ___U24this_4;

public:
	inline static int32_t get_offset_of_originScale_0() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__AnonStorey0_t3548337856, ___originScale_0)); }
	inline Vector3_t2243707580  get_originScale_0() const { return ___originScale_0; }
	inline Vector3_t2243707580 * get_address_of_originScale_0() { return &___originScale_0; }
	inline void set_originScale_0(Vector3_t2243707580  value)
	{
		___originScale_0 = value;
	}

	inline static int32_t get_offset_of_originPos2_1() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__AnonStorey0_t3548337856, ___originPos2_1)); }
	inline Vector3_t2243707580  get_originPos2_1() const { return ___originPos2_1; }
	inline Vector3_t2243707580 * get_address_of_originPos2_1() { return &___originPos2_1; }
	inline void set_originPos2_1(Vector3_t2243707580  value)
	{
		___originPos2_1 = value;
	}

	inline static int32_t get_offset_of_originPos3_2() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__AnonStorey0_t3548337856, ___originPos3_2)); }
	inline Vector3_t2243707580  get_originPos3_2() const { return ___originPos3_2; }
	inline Vector3_t2243707580 * get_address_of_originPos3_2() { return &___originPos3_2; }
	inline void set_originPos3_2(Vector3_t2243707580  value)
	{
		___originPos3_2 = value;
	}

	inline static int32_t get_offset_of_originPos1_3() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__AnonStorey0_t3548337856, ___originPos1_3)); }
	inline Vector3_t2243707580  get_originPos1_3() const { return ___originPos1_3; }
	inline Vector3_t2243707580 * get_address_of_originPos1_3() { return &___originPos1_3; }
	inline void set_originPos1_3(Vector3_t2243707580  value)
	{
		___originPos1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CStartAnimationU3Ec__AnonStorey0_t3548337856, ___U24this_4)); }
	inline GuidePop1_t959359454 * get_U24this_4() const { return ___U24this_4; }
	inline GuidePop1_t959359454 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GuidePop1_t959359454 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
