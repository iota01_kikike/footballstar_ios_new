﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredChar
struct  ObscuredChar_t1936922347 
{
public:
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::currentCryptoKey
	Il2CppChar ___currentCryptoKey_1;
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::hiddenValue
	Il2CppChar ___hiddenValue_2;
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::fakeValue
	Il2CppChar ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::inited
	bool ___inited_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredChar_t1936922347, ___currentCryptoKey_1)); }
	inline Il2CppChar get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline Il2CppChar* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(Il2CppChar value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredChar_t1936922347, ___hiddenValue_2)); }
	inline Il2CppChar get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline Il2CppChar* get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(Il2CppChar value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredChar_t1936922347, ___fakeValue_3)); }
	inline Il2CppChar get_fakeValue_3() const { return ___fakeValue_3; }
	inline Il2CppChar* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(Il2CppChar value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_inited_4() { return static_cast<int32_t>(offsetof(ObscuredChar_t1936922347, ___inited_4)); }
	inline bool get_inited_4() const { return ___inited_4; }
	inline bool* get_address_of_inited_4() { return &___inited_4; }
	inline void set_inited_4(bool value)
	{
		___inited_4 = value;
	}
};

struct ObscuredChar_t1936922347_StaticFields
{
public:
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::cryptoKey
	Il2CppChar ___cryptoKey_0;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredChar_t1936922347_StaticFields, ___cryptoKey_0)); }
	inline Il2CppChar get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline Il2CppChar* get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(Il2CppChar value)
	{
		___cryptoKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredChar
struct ObscuredChar_t1936922347_marshaled_pinvoke
{
	uint8_t ___currentCryptoKey_1;
	uint8_t ___hiddenValue_2;
	uint8_t ___fakeValue_3;
	int32_t ___inited_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredChar
struct ObscuredChar_t1936922347_marshaled_com
{
	uint8_t ___currentCryptoKey_1;
	uint8_t ___hiddenValue_2;
	uint8_t ___fakeValue_3;
	int32_t ___inited_4;
};
