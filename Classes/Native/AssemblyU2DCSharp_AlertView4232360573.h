﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlertView
struct  AlertView_t4232360573  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text AlertView::TitleText
	Text_t356221433 * ___TitleText_2;
	// UnityEngine.UI.Text AlertView::MessageText
	Text_t356221433 * ___MessageText_3;
	// UnityEngine.UI.Text AlertView::YesText
	Text_t356221433 * ___YesText_4;
	// UnityEngine.UI.Button AlertView::YesButton
	Button_t2872111280 * ___YesButton_5;
	// UnityEngine.UI.Button AlertView::CloseButton
	Button_t2872111280 * ___CloseButton_6;
	// UnityEngine.UI.Image AlertView::GemIcon
	Image_t2042527209 * ___GemIcon_7;
	// UnityEngine.UI.Image AlertView::CoinIcon
	Image_t2042527209 * ___CoinIcon_8;
	// System.Action`1<System.Int32> AlertView::Callback
	Action_1_t1873676830 * ___Callback_9;

public:
	inline static int32_t get_offset_of_TitleText_2() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___TitleText_2)); }
	inline Text_t356221433 * get_TitleText_2() const { return ___TitleText_2; }
	inline Text_t356221433 ** get_address_of_TitleText_2() { return &___TitleText_2; }
	inline void set_TitleText_2(Text_t356221433 * value)
	{
		___TitleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___TitleText_2, value);
	}

	inline static int32_t get_offset_of_MessageText_3() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___MessageText_3)); }
	inline Text_t356221433 * get_MessageText_3() const { return ___MessageText_3; }
	inline Text_t356221433 ** get_address_of_MessageText_3() { return &___MessageText_3; }
	inline void set_MessageText_3(Text_t356221433 * value)
	{
		___MessageText_3 = value;
		Il2CppCodeGenWriteBarrier(&___MessageText_3, value);
	}

	inline static int32_t get_offset_of_YesText_4() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___YesText_4)); }
	inline Text_t356221433 * get_YesText_4() const { return ___YesText_4; }
	inline Text_t356221433 ** get_address_of_YesText_4() { return &___YesText_4; }
	inline void set_YesText_4(Text_t356221433 * value)
	{
		___YesText_4 = value;
		Il2CppCodeGenWriteBarrier(&___YesText_4, value);
	}

	inline static int32_t get_offset_of_YesButton_5() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___YesButton_5)); }
	inline Button_t2872111280 * get_YesButton_5() const { return ___YesButton_5; }
	inline Button_t2872111280 ** get_address_of_YesButton_5() { return &___YesButton_5; }
	inline void set_YesButton_5(Button_t2872111280 * value)
	{
		___YesButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___YesButton_5, value);
	}

	inline static int32_t get_offset_of_CloseButton_6() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___CloseButton_6)); }
	inline Button_t2872111280 * get_CloseButton_6() const { return ___CloseButton_6; }
	inline Button_t2872111280 ** get_address_of_CloseButton_6() { return &___CloseButton_6; }
	inline void set_CloseButton_6(Button_t2872111280 * value)
	{
		___CloseButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_6, value);
	}

	inline static int32_t get_offset_of_GemIcon_7() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___GemIcon_7)); }
	inline Image_t2042527209 * get_GemIcon_7() const { return ___GemIcon_7; }
	inline Image_t2042527209 ** get_address_of_GemIcon_7() { return &___GemIcon_7; }
	inline void set_GemIcon_7(Image_t2042527209 * value)
	{
		___GemIcon_7 = value;
		Il2CppCodeGenWriteBarrier(&___GemIcon_7, value);
	}

	inline static int32_t get_offset_of_CoinIcon_8() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___CoinIcon_8)); }
	inline Image_t2042527209 * get_CoinIcon_8() const { return ___CoinIcon_8; }
	inline Image_t2042527209 ** get_address_of_CoinIcon_8() { return &___CoinIcon_8; }
	inline void set_CoinIcon_8(Image_t2042527209 * value)
	{
		___CoinIcon_8 = value;
		Il2CppCodeGenWriteBarrier(&___CoinIcon_8, value);
	}

	inline static int32_t get_offset_of_Callback_9() { return static_cast<int32_t>(offsetof(AlertView_t4232360573, ___Callback_9)); }
	inline Action_1_t1873676830 * get_Callback_9() const { return ___Callback_9; }
	inline Action_1_t1873676830 ** get_address_of_Callback_9() { return &___Callback_9; }
	inline void set_Callback_9(Action_1_t1873676830 * value)
	{
		___Callback_9 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
