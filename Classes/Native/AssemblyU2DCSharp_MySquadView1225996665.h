﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_InfoType242837520.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Collections.Generic.Dictionary`2<System.Int32,FormPlayerUIView>
struct Dictionary_2_t496639427;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MySquadView
struct  MySquadView_t1225996665  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button MySquadView::InfoTypeButton
	Button_t2872111280 * ___InfoTypeButton_2;
	// UnityEngine.UI.Button MySquadView::FormationButton
	Button_t2872111280 * ___FormationButton_3;
	// UnityEngine.UI.Button MySquadView::LeaderboardButton
	Button_t2872111280 * ___LeaderboardButton_4;
	// UnityEngine.UI.Button MySquadView::ShareButton1
	Button_t2872111280 * ___ShareButton1_5;
	// UnityEngine.UI.Button MySquadView::ShareButton2
	Button_t2872111280 * ___ShareButton2_6;
	// UnityEngine.UI.Button MySquadView::ShareButton3
	Button_t2872111280 * ___ShareButton3_7;
	// UnityEngine.UI.Button MySquadView::ShareButton4
	Button_t2872111280 * ___ShareButton4_8;
	// UnityEngine.UI.Button[] MySquadView::DeckButtonList
	ButtonU5BU5D_t3071100561* ___DeckButtonList_9;
	// UnityEngine.UI.Button[] MySquadView::FormButton
	ButtonU5BU5D_t3071100561* ___FormButton_10;
	// UnityEngine.GameObject MySquadView::Best11PlayersObj
	GameObject_t1756533147 * ___Best11PlayersObj_11;
	// UnityEngine.UI.Text MySquadView::FormationText
	Text_t356221433 * ___FormationText_12;
	// UnityEngine.UI.Text MySquadView::TotalOverallText
	Text_t356221433 * ___TotalOverallText_13;
	// UnityEngine.GameObject MySquadView::SelectFormPanel
	GameObject_t1756533147 * ___SelectFormPanel_14;
	// InfoType MySquadView::curInfoType
	int32_t ___curInfoType_15;
	// System.Int32 MySquadView::curSquadIndex
	int32_t ___curSquadIndex_16;
	// UnityEngine.GameObject MySquadView::formObj
	GameObject_t1756533147 * ___formObj_17;
	// System.Collections.Generic.Dictionary`2<System.Int32,FormPlayerUIView> MySquadView::formUIDict
	Dictionary_2_t496639427 * ___formUIDict_18;
	// System.Int32 MySquadView::selectedFormUIIndex
	int32_t ___selectedFormUIIndex_19;
	// System.Int32 MySquadView::totalOverall
	int32_t ___totalOverall_20;
	// System.Int32 MySquadView::totalOverallProgress
	int32_t ___totalOverallProgress_21;

public:
	inline static int32_t get_offset_of_InfoTypeButton_2() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___InfoTypeButton_2)); }
	inline Button_t2872111280 * get_InfoTypeButton_2() const { return ___InfoTypeButton_2; }
	inline Button_t2872111280 ** get_address_of_InfoTypeButton_2() { return &___InfoTypeButton_2; }
	inline void set_InfoTypeButton_2(Button_t2872111280 * value)
	{
		___InfoTypeButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___InfoTypeButton_2, value);
	}

	inline static int32_t get_offset_of_FormationButton_3() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___FormationButton_3)); }
	inline Button_t2872111280 * get_FormationButton_3() const { return ___FormationButton_3; }
	inline Button_t2872111280 ** get_address_of_FormationButton_3() { return &___FormationButton_3; }
	inline void set_FormationButton_3(Button_t2872111280 * value)
	{
		___FormationButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___FormationButton_3, value);
	}

	inline static int32_t get_offset_of_LeaderboardButton_4() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___LeaderboardButton_4)); }
	inline Button_t2872111280 * get_LeaderboardButton_4() const { return ___LeaderboardButton_4; }
	inline Button_t2872111280 ** get_address_of_LeaderboardButton_4() { return &___LeaderboardButton_4; }
	inline void set_LeaderboardButton_4(Button_t2872111280 * value)
	{
		___LeaderboardButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___LeaderboardButton_4, value);
	}

	inline static int32_t get_offset_of_ShareButton1_5() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___ShareButton1_5)); }
	inline Button_t2872111280 * get_ShareButton1_5() const { return ___ShareButton1_5; }
	inline Button_t2872111280 ** get_address_of_ShareButton1_5() { return &___ShareButton1_5; }
	inline void set_ShareButton1_5(Button_t2872111280 * value)
	{
		___ShareButton1_5 = value;
		Il2CppCodeGenWriteBarrier(&___ShareButton1_5, value);
	}

	inline static int32_t get_offset_of_ShareButton2_6() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___ShareButton2_6)); }
	inline Button_t2872111280 * get_ShareButton2_6() const { return ___ShareButton2_6; }
	inline Button_t2872111280 ** get_address_of_ShareButton2_6() { return &___ShareButton2_6; }
	inline void set_ShareButton2_6(Button_t2872111280 * value)
	{
		___ShareButton2_6 = value;
		Il2CppCodeGenWriteBarrier(&___ShareButton2_6, value);
	}

	inline static int32_t get_offset_of_ShareButton3_7() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___ShareButton3_7)); }
	inline Button_t2872111280 * get_ShareButton3_7() const { return ___ShareButton3_7; }
	inline Button_t2872111280 ** get_address_of_ShareButton3_7() { return &___ShareButton3_7; }
	inline void set_ShareButton3_7(Button_t2872111280 * value)
	{
		___ShareButton3_7 = value;
		Il2CppCodeGenWriteBarrier(&___ShareButton3_7, value);
	}

	inline static int32_t get_offset_of_ShareButton4_8() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___ShareButton4_8)); }
	inline Button_t2872111280 * get_ShareButton4_8() const { return ___ShareButton4_8; }
	inline Button_t2872111280 ** get_address_of_ShareButton4_8() { return &___ShareButton4_8; }
	inline void set_ShareButton4_8(Button_t2872111280 * value)
	{
		___ShareButton4_8 = value;
		Il2CppCodeGenWriteBarrier(&___ShareButton4_8, value);
	}

	inline static int32_t get_offset_of_DeckButtonList_9() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___DeckButtonList_9)); }
	inline ButtonU5BU5D_t3071100561* get_DeckButtonList_9() const { return ___DeckButtonList_9; }
	inline ButtonU5BU5D_t3071100561** get_address_of_DeckButtonList_9() { return &___DeckButtonList_9; }
	inline void set_DeckButtonList_9(ButtonU5BU5D_t3071100561* value)
	{
		___DeckButtonList_9 = value;
		Il2CppCodeGenWriteBarrier(&___DeckButtonList_9, value);
	}

	inline static int32_t get_offset_of_FormButton_10() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___FormButton_10)); }
	inline ButtonU5BU5D_t3071100561* get_FormButton_10() const { return ___FormButton_10; }
	inline ButtonU5BU5D_t3071100561** get_address_of_FormButton_10() { return &___FormButton_10; }
	inline void set_FormButton_10(ButtonU5BU5D_t3071100561* value)
	{
		___FormButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___FormButton_10, value);
	}

	inline static int32_t get_offset_of_Best11PlayersObj_11() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___Best11PlayersObj_11)); }
	inline GameObject_t1756533147 * get_Best11PlayersObj_11() const { return ___Best11PlayersObj_11; }
	inline GameObject_t1756533147 ** get_address_of_Best11PlayersObj_11() { return &___Best11PlayersObj_11; }
	inline void set_Best11PlayersObj_11(GameObject_t1756533147 * value)
	{
		___Best11PlayersObj_11 = value;
		Il2CppCodeGenWriteBarrier(&___Best11PlayersObj_11, value);
	}

	inline static int32_t get_offset_of_FormationText_12() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___FormationText_12)); }
	inline Text_t356221433 * get_FormationText_12() const { return ___FormationText_12; }
	inline Text_t356221433 ** get_address_of_FormationText_12() { return &___FormationText_12; }
	inline void set_FormationText_12(Text_t356221433 * value)
	{
		___FormationText_12 = value;
		Il2CppCodeGenWriteBarrier(&___FormationText_12, value);
	}

	inline static int32_t get_offset_of_TotalOverallText_13() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___TotalOverallText_13)); }
	inline Text_t356221433 * get_TotalOverallText_13() const { return ___TotalOverallText_13; }
	inline Text_t356221433 ** get_address_of_TotalOverallText_13() { return &___TotalOverallText_13; }
	inline void set_TotalOverallText_13(Text_t356221433 * value)
	{
		___TotalOverallText_13 = value;
		Il2CppCodeGenWriteBarrier(&___TotalOverallText_13, value);
	}

	inline static int32_t get_offset_of_SelectFormPanel_14() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___SelectFormPanel_14)); }
	inline GameObject_t1756533147 * get_SelectFormPanel_14() const { return ___SelectFormPanel_14; }
	inline GameObject_t1756533147 ** get_address_of_SelectFormPanel_14() { return &___SelectFormPanel_14; }
	inline void set_SelectFormPanel_14(GameObject_t1756533147 * value)
	{
		___SelectFormPanel_14 = value;
		Il2CppCodeGenWriteBarrier(&___SelectFormPanel_14, value);
	}

	inline static int32_t get_offset_of_curInfoType_15() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___curInfoType_15)); }
	inline int32_t get_curInfoType_15() const { return ___curInfoType_15; }
	inline int32_t* get_address_of_curInfoType_15() { return &___curInfoType_15; }
	inline void set_curInfoType_15(int32_t value)
	{
		___curInfoType_15 = value;
	}

	inline static int32_t get_offset_of_curSquadIndex_16() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___curSquadIndex_16)); }
	inline int32_t get_curSquadIndex_16() const { return ___curSquadIndex_16; }
	inline int32_t* get_address_of_curSquadIndex_16() { return &___curSquadIndex_16; }
	inline void set_curSquadIndex_16(int32_t value)
	{
		___curSquadIndex_16 = value;
	}

	inline static int32_t get_offset_of_formObj_17() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___formObj_17)); }
	inline GameObject_t1756533147 * get_formObj_17() const { return ___formObj_17; }
	inline GameObject_t1756533147 ** get_address_of_formObj_17() { return &___formObj_17; }
	inline void set_formObj_17(GameObject_t1756533147 * value)
	{
		___formObj_17 = value;
		Il2CppCodeGenWriteBarrier(&___formObj_17, value);
	}

	inline static int32_t get_offset_of_formUIDict_18() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___formUIDict_18)); }
	inline Dictionary_2_t496639427 * get_formUIDict_18() const { return ___formUIDict_18; }
	inline Dictionary_2_t496639427 ** get_address_of_formUIDict_18() { return &___formUIDict_18; }
	inline void set_formUIDict_18(Dictionary_2_t496639427 * value)
	{
		___formUIDict_18 = value;
		Il2CppCodeGenWriteBarrier(&___formUIDict_18, value);
	}

	inline static int32_t get_offset_of_selectedFormUIIndex_19() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___selectedFormUIIndex_19)); }
	inline int32_t get_selectedFormUIIndex_19() const { return ___selectedFormUIIndex_19; }
	inline int32_t* get_address_of_selectedFormUIIndex_19() { return &___selectedFormUIIndex_19; }
	inline void set_selectedFormUIIndex_19(int32_t value)
	{
		___selectedFormUIIndex_19 = value;
	}

	inline static int32_t get_offset_of_totalOverall_20() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___totalOverall_20)); }
	inline int32_t get_totalOverall_20() const { return ___totalOverall_20; }
	inline int32_t* get_address_of_totalOverall_20() { return &___totalOverall_20; }
	inline void set_totalOverall_20(int32_t value)
	{
		___totalOverall_20 = value;
	}

	inline static int32_t get_offset_of_totalOverallProgress_21() { return static_cast<int32_t>(offsetof(MySquadView_t1225996665, ___totalOverallProgress_21)); }
	inline int32_t get_totalOverallProgress_21() const { return ___totalOverallProgress_21; }
	inline int32_t* get_address_of_totalOverallProgress_21() { return &___totalOverallProgress_21; }
	inline void set_totalOverallProgress_21(int32_t value)
	{
		___totalOverallProgress_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
