﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ScoutCellView2404854735.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// ScoutCell
struct ScoutCell_t2879086938;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellWideView
struct  ScoutCellWideView_t573861516  : public ScoutCellView_t2404854735
{
public:
	// UnityEngine.UI.Button ScoutCellWideView::Button
	Button_t2872111280 * ___Button_2;
	// UnityEngine.UI.Text ScoutCellWideView::SubjectText
	Text_t356221433 * ___SubjectText_3;
	// UnityEngine.UI.Text ScoutCellWideView::SubtitleText
	Text_t356221433 * ___SubtitleText_4;
	// UnityEngine.UI.Text ScoutCellWideView::CostText
	Text_t356221433 * ___CostText_5;
	// UnityEngine.UI.Image ScoutCellWideView::ScoutIcon
	Image_t2042527209 * ___ScoutIcon_6;
	// UnityEngine.UI.Image ScoutCellWideView::GemIcon
	Image_t2042527209 * ___GemIcon_7;
	// UnityEngine.UI.Image ScoutCellWideView::CostGemIcon
	Image_t2042527209 * ___CostGemIcon_8;
	// UnityEngine.UI.Image ScoutCellWideView::CostCoinIcon
	Image_t2042527209 * ___CostCoinIcon_9;
	// UnityEngine.UI.Image ScoutCellWideView::BadgeIcon
	Image_t2042527209 * ___BadgeIcon_10;
	// ScoutCell ScoutCellWideView::_parentCell
	ScoutCell_t2879086938 * ____parentCell_11;

public:
	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___Button_2)); }
	inline Button_t2872111280 * get_Button_2() const { return ___Button_2; }
	inline Button_t2872111280 ** get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(Button_t2872111280 * value)
	{
		___Button_2 = value;
		Il2CppCodeGenWriteBarrier(&___Button_2, value);
	}

	inline static int32_t get_offset_of_SubjectText_3() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___SubjectText_3)); }
	inline Text_t356221433 * get_SubjectText_3() const { return ___SubjectText_3; }
	inline Text_t356221433 ** get_address_of_SubjectText_3() { return &___SubjectText_3; }
	inline void set_SubjectText_3(Text_t356221433 * value)
	{
		___SubjectText_3 = value;
		Il2CppCodeGenWriteBarrier(&___SubjectText_3, value);
	}

	inline static int32_t get_offset_of_SubtitleText_4() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___SubtitleText_4)); }
	inline Text_t356221433 * get_SubtitleText_4() const { return ___SubtitleText_4; }
	inline Text_t356221433 ** get_address_of_SubtitleText_4() { return &___SubtitleText_4; }
	inline void set_SubtitleText_4(Text_t356221433 * value)
	{
		___SubtitleText_4 = value;
		Il2CppCodeGenWriteBarrier(&___SubtitleText_4, value);
	}

	inline static int32_t get_offset_of_CostText_5() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___CostText_5)); }
	inline Text_t356221433 * get_CostText_5() const { return ___CostText_5; }
	inline Text_t356221433 ** get_address_of_CostText_5() { return &___CostText_5; }
	inline void set_CostText_5(Text_t356221433 * value)
	{
		___CostText_5 = value;
		Il2CppCodeGenWriteBarrier(&___CostText_5, value);
	}

	inline static int32_t get_offset_of_ScoutIcon_6() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___ScoutIcon_6)); }
	inline Image_t2042527209 * get_ScoutIcon_6() const { return ___ScoutIcon_6; }
	inline Image_t2042527209 ** get_address_of_ScoutIcon_6() { return &___ScoutIcon_6; }
	inline void set_ScoutIcon_6(Image_t2042527209 * value)
	{
		___ScoutIcon_6 = value;
		Il2CppCodeGenWriteBarrier(&___ScoutIcon_6, value);
	}

	inline static int32_t get_offset_of_GemIcon_7() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___GemIcon_7)); }
	inline Image_t2042527209 * get_GemIcon_7() const { return ___GemIcon_7; }
	inline Image_t2042527209 ** get_address_of_GemIcon_7() { return &___GemIcon_7; }
	inline void set_GemIcon_7(Image_t2042527209 * value)
	{
		___GemIcon_7 = value;
		Il2CppCodeGenWriteBarrier(&___GemIcon_7, value);
	}

	inline static int32_t get_offset_of_CostGemIcon_8() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___CostGemIcon_8)); }
	inline Image_t2042527209 * get_CostGemIcon_8() const { return ___CostGemIcon_8; }
	inline Image_t2042527209 ** get_address_of_CostGemIcon_8() { return &___CostGemIcon_8; }
	inline void set_CostGemIcon_8(Image_t2042527209 * value)
	{
		___CostGemIcon_8 = value;
		Il2CppCodeGenWriteBarrier(&___CostGemIcon_8, value);
	}

	inline static int32_t get_offset_of_CostCoinIcon_9() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___CostCoinIcon_9)); }
	inline Image_t2042527209 * get_CostCoinIcon_9() const { return ___CostCoinIcon_9; }
	inline Image_t2042527209 ** get_address_of_CostCoinIcon_9() { return &___CostCoinIcon_9; }
	inline void set_CostCoinIcon_9(Image_t2042527209 * value)
	{
		___CostCoinIcon_9 = value;
		Il2CppCodeGenWriteBarrier(&___CostCoinIcon_9, value);
	}

	inline static int32_t get_offset_of_BadgeIcon_10() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ___BadgeIcon_10)); }
	inline Image_t2042527209 * get_BadgeIcon_10() const { return ___BadgeIcon_10; }
	inline Image_t2042527209 ** get_address_of_BadgeIcon_10() { return &___BadgeIcon_10; }
	inline void set_BadgeIcon_10(Image_t2042527209 * value)
	{
		___BadgeIcon_10 = value;
		Il2CppCodeGenWriteBarrier(&___BadgeIcon_10, value);
	}

	inline static int32_t get_offset_of__parentCell_11() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516, ____parentCell_11)); }
	inline ScoutCell_t2879086938 * get__parentCell_11() const { return ____parentCell_11; }
	inline ScoutCell_t2879086938 ** get_address_of__parentCell_11() { return &____parentCell_11; }
	inline void set__parentCell_11(ScoutCell_t2879086938 * value)
	{
		____parentCell_11 = value;
		Il2CppCodeGenWriteBarrier(&____parentCell_11, value);
	}
};

struct ScoutCellWideView_t573861516_StaticFields
{
public:
	// System.Action`1<System.Int32> ScoutCellWideView::<>f__am$cache0
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache0_12;
	// System.Action`1<System.Int32> ScoutCellWideView::<>f__am$cache1
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache1_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(ScoutCellWideView_t573861516_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
