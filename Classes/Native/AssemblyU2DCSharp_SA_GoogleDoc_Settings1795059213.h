﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<SA.GoogleDoc.DocTemplate>
struct List_1_t3873837002;
// SA.GoogleDoc.Settings
struct Settings_t1795059213;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.Settings
struct  Settings_t1795059213  : public ScriptableObject_t1975622470
{
public:
	// System.String SA.GoogleDoc.Settings::CachePath
	String_t* ___CachePath_17;
	// System.Collections.Generic.List`1<SA.GoogleDoc.DocTemplate> SA.GoogleDoc.Settings::docs
	List_1_t3873837002 * ___docs_18;

public:
	inline static int32_t get_offset_of_CachePath_17() { return static_cast<int32_t>(offsetof(Settings_t1795059213, ___CachePath_17)); }
	inline String_t* get_CachePath_17() const { return ___CachePath_17; }
	inline String_t** get_address_of_CachePath_17() { return &___CachePath_17; }
	inline void set_CachePath_17(String_t* value)
	{
		___CachePath_17 = value;
		Il2CppCodeGenWriteBarrier(&___CachePath_17, value);
	}

	inline static int32_t get_offset_of_docs_18() { return static_cast<int32_t>(offsetof(Settings_t1795059213, ___docs_18)); }
	inline List_1_t3873837002 * get_docs_18() const { return ___docs_18; }
	inline List_1_t3873837002 ** get_address_of_docs_18() { return &___docs_18; }
	inline void set_docs_18(List_1_t3873837002 * value)
	{
		___docs_18 = value;
		Il2CppCodeGenWriteBarrier(&___docs_18, value);
	}
};

struct Settings_t1795059213_StaticFields
{
public:
	// System.String SA.GoogleDoc.Settings::SERVICE_NAME
	String_t* ___SERVICE_NAME_5;
	// System.String SA.GoogleDoc.Settings::CLIENT_ID
	String_t* ___CLIENT_ID_6;
	// System.String SA.GoogleDoc.Settings::CLIENT_SECRET
	String_t* ___CLIENT_SECRET_7;
	// System.String SA.GoogleDoc.Settings::SCOPE
	String_t* ___SCOPE_8;
	// System.String SA.GoogleDoc.Settings::REDIRECT_URI
	String_t* ___REDIRECT_URI_9;
	// System.String SA.GoogleDoc.Settings::GOOGLEDOC_URL_START
	String_t* ___GOOGLEDOC_URL_START_10;
	// System.String SA.GoogleDoc.Settings::GOOGLEDOC_URL_END
	String_t* ___GOOGLEDOC_URL_END_11;
	// System.String SA.GoogleDoc.Settings::SPREADSHEET_URL_START
	String_t* ___SPREADSHEET_URL_START_12;
	// System.String SA.GoogleDoc.Settings::SPREADSHEET_URL_END
	String_t* ___SPREADSHEET_URL_END_13;
	// System.String SA.GoogleDoc.Settings::SCRIPT_URL_START
	String_t* ___SCRIPT_URL_START_14;
	// System.String SA.GoogleDoc.Settings::SCRIPT_URL_PARAM
	String_t* ___SCRIPT_URL_PARAM_15;
	// System.String SA.GoogleDoc.Settings::PATH_ADDING
	String_t* ___PATH_ADDING_16;
	// SA.GoogleDoc.Settings SA.GoogleDoc.Settings::instance
	Settings_t1795059213 * ___instance_19;

public:
	inline static int32_t get_offset_of_SERVICE_NAME_5() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___SERVICE_NAME_5)); }
	inline String_t* get_SERVICE_NAME_5() const { return ___SERVICE_NAME_5; }
	inline String_t** get_address_of_SERVICE_NAME_5() { return &___SERVICE_NAME_5; }
	inline void set_SERVICE_NAME_5(String_t* value)
	{
		___SERVICE_NAME_5 = value;
		Il2CppCodeGenWriteBarrier(&___SERVICE_NAME_5, value);
	}

	inline static int32_t get_offset_of_CLIENT_ID_6() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___CLIENT_ID_6)); }
	inline String_t* get_CLIENT_ID_6() const { return ___CLIENT_ID_6; }
	inline String_t** get_address_of_CLIENT_ID_6() { return &___CLIENT_ID_6; }
	inline void set_CLIENT_ID_6(String_t* value)
	{
		___CLIENT_ID_6 = value;
		Il2CppCodeGenWriteBarrier(&___CLIENT_ID_6, value);
	}

	inline static int32_t get_offset_of_CLIENT_SECRET_7() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___CLIENT_SECRET_7)); }
	inline String_t* get_CLIENT_SECRET_7() const { return ___CLIENT_SECRET_7; }
	inline String_t** get_address_of_CLIENT_SECRET_7() { return &___CLIENT_SECRET_7; }
	inline void set_CLIENT_SECRET_7(String_t* value)
	{
		___CLIENT_SECRET_7 = value;
		Il2CppCodeGenWriteBarrier(&___CLIENT_SECRET_7, value);
	}

	inline static int32_t get_offset_of_SCOPE_8() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___SCOPE_8)); }
	inline String_t* get_SCOPE_8() const { return ___SCOPE_8; }
	inline String_t** get_address_of_SCOPE_8() { return &___SCOPE_8; }
	inline void set_SCOPE_8(String_t* value)
	{
		___SCOPE_8 = value;
		Il2CppCodeGenWriteBarrier(&___SCOPE_8, value);
	}

	inline static int32_t get_offset_of_REDIRECT_URI_9() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___REDIRECT_URI_9)); }
	inline String_t* get_REDIRECT_URI_9() const { return ___REDIRECT_URI_9; }
	inline String_t** get_address_of_REDIRECT_URI_9() { return &___REDIRECT_URI_9; }
	inline void set_REDIRECT_URI_9(String_t* value)
	{
		___REDIRECT_URI_9 = value;
		Il2CppCodeGenWriteBarrier(&___REDIRECT_URI_9, value);
	}

	inline static int32_t get_offset_of_GOOGLEDOC_URL_START_10() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___GOOGLEDOC_URL_START_10)); }
	inline String_t* get_GOOGLEDOC_URL_START_10() const { return ___GOOGLEDOC_URL_START_10; }
	inline String_t** get_address_of_GOOGLEDOC_URL_START_10() { return &___GOOGLEDOC_URL_START_10; }
	inline void set_GOOGLEDOC_URL_START_10(String_t* value)
	{
		___GOOGLEDOC_URL_START_10 = value;
		Il2CppCodeGenWriteBarrier(&___GOOGLEDOC_URL_START_10, value);
	}

	inline static int32_t get_offset_of_GOOGLEDOC_URL_END_11() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___GOOGLEDOC_URL_END_11)); }
	inline String_t* get_GOOGLEDOC_URL_END_11() const { return ___GOOGLEDOC_URL_END_11; }
	inline String_t** get_address_of_GOOGLEDOC_URL_END_11() { return &___GOOGLEDOC_URL_END_11; }
	inline void set_GOOGLEDOC_URL_END_11(String_t* value)
	{
		___GOOGLEDOC_URL_END_11 = value;
		Il2CppCodeGenWriteBarrier(&___GOOGLEDOC_URL_END_11, value);
	}

	inline static int32_t get_offset_of_SPREADSHEET_URL_START_12() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___SPREADSHEET_URL_START_12)); }
	inline String_t* get_SPREADSHEET_URL_START_12() const { return ___SPREADSHEET_URL_START_12; }
	inline String_t** get_address_of_SPREADSHEET_URL_START_12() { return &___SPREADSHEET_URL_START_12; }
	inline void set_SPREADSHEET_URL_START_12(String_t* value)
	{
		___SPREADSHEET_URL_START_12 = value;
		Il2CppCodeGenWriteBarrier(&___SPREADSHEET_URL_START_12, value);
	}

	inline static int32_t get_offset_of_SPREADSHEET_URL_END_13() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___SPREADSHEET_URL_END_13)); }
	inline String_t* get_SPREADSHEET_URL_END_13() const { return ___SPREADSHEET_URL_END_13; }
	inline String_t** get_address_of_SPREADSHEET_URL_END_13() { return &___SPREADSHEET_URL_END_13; }
	inline void set_SPREADSHEET_URL_END_13(String_t* value)
	{
		___SPREADSHEET_URL_END_13 = value;
		Il2CppCodeGenWriteBarrier(&___SPREADSHEET_URL_END_13, value);
	}

	inline static int32_t get_offset_of_SCRIPT_URL_START_14() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___SCRIPT_URL_START_14)); }
	inline String_t* get_SCRIPT_URL_START_14() const { return ___SCRIPT_URL_START_14; }
	inline String_t** get_address_of_SCRIPT_URL_START_14() { return &___SCRIPT_URL_START_14; }
	inline void set_SCRIPT_URL_START_14(String_t* value)
	{
		___SCRIPT_URL_START_14 = value;
		Il2CppCodeGenWriteBarrier(&___SCRIPT_URL_START_14, value);
	}

	inline static int32_t get_offset_of_SCRIPT_URL_PARAM_15() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___SCRIPT_URL_PARAM_15)); }
	inline String_t* get_SCRIPT_URL_PARAM_15() const { return ___SCRIPT_URL_PARAM_15; }
	inline String_t** get_address_of_SCRIPT_URL_PARAM_15() { return &___SCRIPT_URL_PARAM_15; }
	inline void set_SCRIPT_URL_PARAM_15(String_t* value)
	{
		___SCRIPT_URL_PARAM_15 = value;
		Il2CppCodeGenWriteBarrier(&___SCRIPT_URL_PARAM_15, value);
	}

	inline static int32_t get_offset_of_PATH_ADDING_16() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___PATH_ADDING_16)); }
	inline String_t* get_PATH_ADDING_16() const { return ___PATH_ADDING_16; }
	inline String_t** get_address_of_PATH_ADDING_16() { return &___PATH_ADDING_16; }
	inline void set_PATH_ADDING_16(String_t* value)
	{
		___PATH_ADDING_16 = value;
		Il2CppCodeGenWriteBarrier(&___PATH_ADDING_16, value);
	}

	inline static int32_t get_offset_of_instance_19() { return static_cast<int32_t>(offsetof(Settings_t1795059213_StaticFields, ___instance_19)); }
	inline Settings_t1795059213 * get_instance_19() const { return ___instance_19; }
	inline Settings_t1795059213 ** get_address_of_instance_19() { return &___instance_19; }
	inline void set_instance_19(Settings_t1795059213 * value)
	{
		___instance_19 = value;
		Il2CppCodeGenWriteBarrier(&___instance_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
