﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnitDataHolder
struct UnitDataHolder_t2129886096;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TrainingView
struct TrainingView_t2704080403;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/<OnClickStar>c__AnonStorey4
struct  U3COnClickStarU3Ec__AnonStorey4_t1350708256  : public Il2CppObject
{
public:
	// UnitDataHolder TrainingView/<OnClickStar>c__AnonStorey4::unitDataHolder
	UnitDataHolder_t2129886096 * ___unitDataHolder_0;
	// UnityEngine.GameObject TrainingView/<OnClickStar>c__AnonStorey4::trainee
	GameObject_t1756533147 * ___trainee_1;
	// TrainingView TrainingView/<OnClickStar>c__AnonStorey4::$this
	TrainingView_t2704080403 * ___U24this_2;

public:
	inline static int32_t get_offset_of_unitDataHolder_0() { return static_cast<int32_t>(offsetof(U3COnClickStarU3Ec__AnonStorey4_t1350708256, ___unitDataHolder_0)); }
	inline UnitDataHolder_t2129886096 * get_unitDataHolder_0() const { return ___unitDataHolder_0; }
	inline UnitDataHolder_t2129886096 ** get_address_of_unitDataHolder_0() { return &___unitDataHolder_0; }
	inline void set_unitDataHolder_0(UnitDataHolder_t2129886096 * value)
	{
		___unitDataHolder_0 = value;
		Il2CppCodeGenWriteBarrier(&___unitDataHolder_0, value);
	}

	inline static int32_t get_offset_of_trainee_1() { return static_cast<int32_t>(offsetof(U3COnClickStarU3Ec__AnonStorey4_t1350708256, ___trainee_1)); }
	inline GameObject_t1756533147 * get_trainee_1() const { return ___trainee_1; }
	inline GameObject_t1756533147 ** get_address_of_trainee_1() { return &___trainee_1; }
	inline void set_trainee_1(GameObject_t1756533147 * value)
	{
		___trainee_1 = value;
		Il2CppCodeGenWriteBarrier(&___trainee_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnClickStarU3Ec__AnonStorey4_t1350708256, ___U24this_2)); }
	inline TrainingView_t2704080403 * get_U24this_2() const { return ___U24this_2; }
	inline TrainingView_t2704080403 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TrainingView_t2704080403 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
