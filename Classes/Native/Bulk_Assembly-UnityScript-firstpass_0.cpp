﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_GoogleDocConnec3892943177.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Exception1927440687.h"

// GoogleDocConnectorEvalScript
struct GoogleDocConnectorEvalScript_t3892943177;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Object
struct Object_t1021602117;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const uint32_t GoogleDocConnectorEvalScript_EvalAssignValue_m1547421645_MetadataUsageId;




// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
extern "C"  Component_t3819376471 * Component_GetComponent_m2473832642 (Component_t3819376471 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleDocConnectorEvalScript::.ctor()
extern "C"  void GoogleDocConnectorEvalScript__ctor_m473936279 (GoogleDocConnectorEvalScript_t3892943177 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GoogleDocConnectorEvalScript::EvalAssignValue(System.String,System.String,System.Object)
extern "C"  bool GoogleDocConnectorEvalScript_EvalAssignValue_m1547421645 (GoogleDocConnectorEvalScript_t3892943177 * __this, String_t* ___targetScript0, String_t* ___targetVariable1, Il2CppObject * ___targetValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GoogleDocConnectorEvalScript_EvalAssignValue_m1547421645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Component_t3819376471 * V_0 = NULL;
	Exception_t1927440687 * V_1 = NULL;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B7_0 = 0;
	{
		String_t* L_0 = ___targetScript0;
		Component_t3819376471 * L_1 = Component_GetComponent_m2473832642(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Component_t3819376471 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			V_2 = (bool)1;
			goto IL_0030;
		}

IL_001b:
		{
			; // IL_001b: leave IL_002d
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0020;
		throw e;
	}

CATCH_0020:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1927440687 *)__exception_local);
			V_2 = (bool)0;
			goto IL_0030;
		}

IL_0028:
		{
			; // IL_0028: leave IL_002d
		}
	} // end catch (depth: 1)

IL_002d:
	{
		G_B7_0 = 0;
		goto IL_0031;
	}

IL_0030:
	{
		bool L_4 = V_2;
		G_B7_0 = ((int32_t)(L_4));
	}

IL_0031:
	{
		return (bool)G_B7_0;
	}
}
// System.Void GoogleDocConnectorEvalScript::Main()
extern "C"  void GoogleDocConnectorEvalScript_Main_m2180687724 (GoogleDocConnectorEvalScript_t3892943177 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
