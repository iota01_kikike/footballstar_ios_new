﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "Stores_UnityEngine_Purchasing_EventDestType4138688180.h"

// System.String
struct String_t;
// UnityEngine.Purchasing.EventQueue
struct EventQueue_t560163637;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t850767941  : public Il2CppObject
{
public:
	// System.Nullable`1<System.Int32> UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0::delayInSeconds
	Nullable_1_t334943763  ___delayInSeconds_0;
	// UnityEngine.Purchasing.EventDestType UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0::dest
	int32_t ___dest_1;
	// System.String UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0::json
	String_t* ___json_2;
	// System.String UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0::target
	String_t* ___target_3;
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0::<>4__this
	EventQueue_t560163637 * ___U3CU3E4__this_4;
	// System.Action UnityEngine.Purchasing.EventQueue/<>c__DisplayClass11_0::<>9__2
	Action_t3226471752 * ___U3CU3E9__2_5;

public:
	inline static int32_t get_offset_of_delayInSeconds_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t850767941, ___delayInSeconds_0)); }
	inline Nullable_1_t334943763  get_delayInSeconds_0() const { return ___delayInSeconds_0; }
	inline Nullable_1_t334943763 * get_address_of_delayInSeconds_0() { return &___delayInSeconds_0; }
	inline void set_delayInSeconds_0(Nullable_1_t334943763  value)
	{
		___delayInSeconds_0 = value;
	}

	inline static int32_t get_offset_of_dest_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t850767941, ___dest_1)); }
	inline int32_t get_dest_1() const { return ___dest_1; }
	inline int32_t* get_address_of_dest_1() { return &___dest_1; }
	inline void set_dest_1(int32_t value)
	{
		___dest_1 = value;
	}

	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t850767941, ___json_2)); }
	inline String_t* get_json_2() const { return ___json_2; }
	inline String_t** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(String_t* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier(&___json_2, value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t850767941, ___target_3)); }
	inline String_t* get_target_3() const { return ___target_3; }
	inline String_t** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(String_t* value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier(&___target_3, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t850767941, ___U3CU3E4__this_4)); }
	inline EventQueue_t560163637 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline EventQueue_t560163637 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(EventQueue_t560163637 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_4, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t850767941, ___U3CU3E9__2_5)); }
	inline Action_t3226471752 * get_U3CU3E9__2_5() const { return ___U3CU3E9__2_5; }
	inline Action_t3226471752 ** get_address_of_U3CU3E9__2_5() { return &___U3CU3E9__2_5; }
	inline void set_U3CU3E9__2_5(Action_t3226471752 * value)
	{
		___U3CU3E9__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__2_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
