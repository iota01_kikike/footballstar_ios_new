﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Parser
struct  Asn1Parser_t914015216  : public Il2CppObject
{
public:
	// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Parser::rawData
	ByteU5BU5D_t3397334013* ___rawData_0;
	// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Parser::rootNode
	Asn1Node_t1770761751 * ___rootNode_1;

public:
	inline static int32_t get_offset_of_rawData_0() { return static_cast<int32_t>(offsetof(Asn1Parser_t914015216, ___rawData_0)); }
	inline ByteU5BU5D_t3397334013* get_rawData_0() const { return ___rawData_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_rawData_0() { return &___rawData_0; }
	inline void set_rawData_0(ByteU5BU5D_t3397334013* value)
	{
		___rawData_0 = value;
		Il2CppCodeGenWriteBarrier(&___rawData_0, value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(Asn1Parser_t914015216, ___rootNode_1)); }
	inline Asn1Node_t1770761751 * get_rootNode_1() const { return ___rootNode_1; }
	inline Asn1Node_t1770761751 ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(Asn1Node_t1770761751 * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___rootNode_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
