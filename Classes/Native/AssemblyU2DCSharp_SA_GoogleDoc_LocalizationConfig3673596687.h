﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

// System.String
struct String_t;
// SA.GoogleDoc.LocalizationConfig
struct LocalizationConfig_t3673596687;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.LocalizationConfig
struct  LocalizationConfig_t3673596687  : public ScriptableObject_t1975622470
{
public:
	// System.String SA.GoogleDoc.LocalizationConfig::LocalizationDocKey
	String_t* ___LocalizationDocKey_2;
	// System.Boolean SA.GoogleDoc.LocalizationConfig::isLanguagesInfoOpen
	bool ___isLanguagesInfoOpen_3;
	// System.Boolean SA.GoogleDoc.LocalizationConfig::isWorksheetInfoOpen
	bool ___isWorksheetInfoOpen_4;

public:
	inline static int32_t get_offset_of_LocalizationDocKey_2() { return static_cast<int32_t>(offsetof(LocalizationConfig_t3673596687, ___LocalizationDocKey_2)); }
	inline String_t* get_LocalizationDocKey_2() const { return ___LocalizationDocKey_2; }
	inline String_t** get_address_of_LocalizationDocKey_2() { return &___LocalizationDocKey_2; }
	inline void set_LocalizationDocKey_2(String_t* value)
	{
		___LocalizationDocKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___LocalizationDocKey_2, value);
	}

	inline static int32_t get_offset_of_isLanguagesInfoOpen_3() { return static_cast<int32_t>(offsetof(LocalizationConfig_t3673596687, ___isLanguagesInfoOpen_3)); }
	inline bool get_isLanguagesInfoOpen_3() const { return ___isLanguagesInfoOpen_3; }
	inline bool* get_address_of_isLanguagesInfoOpen_3() { return &___isLanguagesInfoOpen_3; }
	inline void set_isLanguagesInfoOpen_3(bool value)
	{
		___isLanguagesInfoOpen_3 = value;
	}

	inline static int32_t get_offset_of_isWorksheetInfoOpen_4() { return static_cast<int32_t>(offsetof(LocalizationConfig_t3673596687, ___isWorksheetInfoOpen_4)); }
	inline bool get_isWorksheetInfoOpen_4() const { return ___isWorksheetInfoOpen_4; }
	inline bool* get_address_of_isWorksheetInfoOpen_4() { return &___isWorksheetInfoOpen_4; }
	inline void set_isWorksheetInfoOpen_4(bool value)
	{
		___isWorksheetInfoOpen_4 = value;
	}
};

struct LocalizationConfig_t3673596687_StaticFields
{
public:
	// SA.GoogleDoc.LocalizationConfig SA.GoogleDoc.LocalizationConfig::instance
	LocalizationConfig_t3673596687 * ___instance_9;

public:
	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(LocalizationConfig_t3673596687_StaticFields, ___instance_9)); }
	inline LocalizationConfig_t3673596687 * get_instance_9() const { return ___instance_9; }
	inline LocalizationConfig_t3673596687 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(LocalizationConfig_t3673596687 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier(&___instance_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
