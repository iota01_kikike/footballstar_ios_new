﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t1563811461;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizationFromDocExample
struct  LocalizationFromDocExample_t277634483  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUIStyle LocalizationFromDocExample::GUITextStyle
	GUIStyle_t1799908754 * ___GUITextStyle_2;
	// System.Collections.Generic.List`1<System.String> LocalizationFromDocExample::LangCodes
	List_1_t1398341365 * ___LangCodes_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> LocalizationFromDocExample::languages
	Dictionary_2_t1563811461 * ___languages_6;
	// System.Collections.Generic.List`1<System.String> LocalizationFromDocExample::tokens
	List_1_t1398341365 * ___tokens_7;
	// System.String LocalizationFromDocExample::currentLangCode
	String_t* ___currentLangCode_8;

public:
	inline static int32_t get_offset_of_GUITextStyle_2() { return static_cast<int32_t>(offsetof(LocalizationFromDocExample_t277634483, ___GUITextStyle_2)); }
	inline GUIStyle_t1799908754 * get_GUITextStyle_2() const { return ___GUITextStyle_2; }
	inline GUIStyle_t1799908754 ** get_address_of_GUITextStyle_2() { return &___GUITextStyle_2; }
	inline void set_GUITextStyle_2(GUIStyle_t1799908754 * value)
	{
		___GUITextStyle_2 = value;
		Il2CppCodeGenWriteBarrier(&___GUITextStyle_2, value);
	}

	inline static int32_t get_offset_of_LangCodes_5() { return static_cast<int32_t>(offsetof(LocalizationFromDocExample_t277634483, ___LangCodes_5)); }
	inline List_1_t1398341365 * get_LangCodes_5() const { return ___LangCodes_5; }
	inline List_1_t1398341365 ** get_address_of_LangCodes_5() { return &___LangCodes_5; }
	inline void set_LangCodes_5(List_1_t1398341365 * value)
	{
		___LangCodes_5 = value;
		Il2CppCodeGenWriteBarrier(&___LangCodes_5, value);
	}

	inline static int32_t get_offset_of_languages_6() { return static_cast<int32_t>(offsetof(LocalizationFromDocExample_t277634483, ___languages_6)); }
	inline Dictionary_2_t1563811461 * get_languages_6() const { return ___languages_6; }
	inline Dictionary_2_t1563811461 ** get_address_of_languages_6() { return &___languages_6; }
	inline void set_languages_6(Dictionary_2_t1563811461 * value)
	{
		___languages_6 = value;
		Il2CppCodeGenWriteBarrier(&___languages_6, value);
	}

	inline static int32_t get_offset_of_tokens_7() { return static_cast<int32_t>(offsetof(LocalizationFromDocExample_t277634483, ___tokens_7)); }
	inline List_1_t1398341365 * get_tokens_7() const { return ___tokens_7; }
	inline List_1_t1398341365 ** get_address_of_tokens_7() { return &___tokens_7; }
	inline void set_tokens_7(List_1_t1398341365 * value)
	{
		___tokens_7 = value;
		Il2CppCodeGenWriteBarrier(&___tokens_7, value);
	}

	inline static int32_t get_offset_of_currentLangCode_8() { return static_cast<int32_t>(offsetof(LocalizationFromDocExample_t277634483, ___currentLangCode_8)); }
	inline String_t* get_currentLangCode_8() const { return ___currentLangCode_8; }
	inline String_t** get_address_of_currentLangCode_8() { return &___currentLangCode_8; }
	inline void set_currentLangCode_8(String_t* value)
	{
		___currentLangCode_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentLangCode_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
