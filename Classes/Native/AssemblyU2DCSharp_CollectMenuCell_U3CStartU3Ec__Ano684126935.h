﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// CollectMenuCell
struct CollectMenuCell_t2927503929;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectMenuCell/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t684126935  : public Il2CppObject
{
public:
	// System.Int32 CollectMenuCell/<Start>c__AnonStorey0::index
	int32_t ___index_0;
	// CollectMenuCell CollectMenuCell/<Start>c__AnonStorey0::$this
	CollectMenuCell_t2927503929 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t684126935, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t684126935, ___U24this_1)); }
	inline CollectMenuCell_t2927503929 * get_U24this_1() const { return ___U24this_1; }
	inline CollectMenuCell_t2927503929 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CollectMenuCell_t2927503929 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
