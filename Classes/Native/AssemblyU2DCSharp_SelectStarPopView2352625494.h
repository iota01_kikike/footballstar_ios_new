﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TraineeUnit
struct TraineeUnit_t2929148814;
// SelectStarPopView/SelectStarPopViewDelegate
struct SelectStarPopViewDelegate_t2309401728;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectStarPopView
struct  SelectStarPopView_t2352625494  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SelectStarPopView::MessageText
	Text_t356221433 * ___MessageText_2;
	// UnityEngine.UI.Button SelectStarPopView::CloseButton
	Button_t2872111280 * ___CloseButton_3;
	// UnityEngine.UI.Button SelectStarPopView::Button1
	Button_t2872111280 * ___Button1_4;
	// UnityEngine.UI.Button SelectStarPopView::Button2
	Button_t2872111280 * ___Button2_5;
	// UnityEngine.UI.Button SelectStarPopView::Button3
	Button_t2872111280 * ___Button3_6;
	// UnityEngine.UI.Text SelectStarPopView::GemCntText
	Text_t356221433 * ___GemCntText_7;
	// UnityEngine.UI.Text SelectStarPopView::UnitNameText
	Text_t356221433 * ___UnitNameText_8;
	// UnityEngine.UI.Image SelectStarPopView::StarImage1
	Image_t2042527209 * ___StarImage1_9;
	// UnityEngine.UI.Image SelectStarPopView::StarImage2
	Image_t2042527209 * ___StarImage2_10;
	// UnityEngine.GameObject SelectStarPopView::UnitObject
	GameObject_t1756533147 * ___UnitObject_11;
	// TraineeUnit SelectStarPopView::traineeUnit
	TraineeUnit_t2929148814 * ___traineeUnit_12;
	// SelectStarPopView/SelectStarPopViewDelegate SelectStarPopView::Delegate
	SelectStarPopViewDelegate_t2309401728 * ___Delegate_13;

public:
	inline static int32_t get_offset_of_MessageText_2() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___MessageText_2)); }
	inline Text_t356221433 * get_MessageText_2() const { return ___MessageText_2; }
	inline Text_t356221433 ** get_address_of_MessageText_2() { return &___MessageText_2; }
	inline void set_MessageText_2(Text_t356221433 * value)
	{
		___MessageText_2 = value;
		Il2CppCodeGenWriteBarrier(&___MessageText_2, value);
	}

	inline static int32_t get_offset_of_CloseButton_3() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___CloseButton_3)); }
	inline Button_t2872111280 * get_CloseButton_3() const { return ___CloseButton_3; }
	inline Button_t2872111280 ** get_address_of_CloseButton_3() { return &___CloseButton_3; }
	inline void set_CloseButton_3(Button_t2872111280 * value)
	{
		___CloseButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_3, value);
	}

	inline static int32_t get_offset_of_Button1_4() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___Button1_4)); }
	inline Button_t2872111280 * get_Button1_4() const { return ___Button1_4; }
	inline Button_t2872111280 ** get_address_of_Button1_4() { return &___Button1_4; }
	inline void set_Button1_4(Button_t2872111280 * value)
	{
		___Button1_4 = value;
		Il2CppCodeGenWriteBarrier(&___Button1_4, value);
	}

	inline static int32_t get_offset_of_Button2_5() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___Button2_5)); }
	inline Button_t2872111280 * get_Button2_5() const { return ___Button2_5; }
	inline Button_t2872111280 ** get_address_of_Button2_5() { return &___Button2_5; }
	inline void set_Button2_5(Button_t2872111280 * value)
	{
		___Button2_5 = value;
		Il2CppCodeGenWriteBarrier(&___Button2_5, value);
	}

	inline static int32_t get_offset_of_Button3_6() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___Button3_6)); }
	inline Button_t2872111280 * get_Button3_6() const { return ___Button3_6; }
	inline Button_t2872111280 ** get_address_of_Button3_6() { return &___Button3_6; }
	inline void set_Button3_6(Button_t2872111280 * value)
	{
		___Button3_6 = value;
		Il2CppCodeGenWriteBarrier(&___Button3_6, value);
	}

	inline static int32_t get_offset_of_GemCntText_7() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___GemCntText_7)); }
	inline Text_t356221433 * get_GemCntText_7() const { return ___GemCntText_7; }
	inline Text_t356221433 ** get_address_of_GemCntText_7() { return &___GemCntText_7; }
	inline void set_GemCntText_7(Text_t356221433 * value)
	{
		___GemCntText_7 = value;
		Il2CppCodeGenWriteBarrier(&___GemCntText_7, value);
	}

	inline static int32_t get_offset_of_UnitNameText_8() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___UnitNameText_8)); }
	inline Text_t356221433 * get_UnitNameText_8() const { return ___UnitNameText_8; }
	inline Text_t356221433 ** get_address_of_UnitNameText_8() { return &___UnitNameText_8; }
	inline void set_UnitNameText_8(Text_t356221433 * value)
	{
		___UnitNameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___UnitNameText_8, value);
	}

	inline static int32_t get_offset_of_StarImage1_9() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___StarImage1_9)); }
	inline Image_t2042527209 * get_StarImage1_9() const { return ___StarImage1_9; }
	inline Image_t2042527209 ** get_address_of_StarImage1_9() { return &___StarImage1_9; }
	inline void set_StarImage1_9(Image_t2042527209 * value)
	{
		___StarImage1_9 = value;
		Il2CppCodeGenWriteBarrier(&___StarImage1_9, value);
	}

	inline static int32_t get_offset_of_StarImage2_10() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___StarImage2_10)); }
	inline Image_t2042527209 * get_StarImage2_10() const { return ___StarImage2_10; }
	inline Image_t2042527209 ** get_address_of_StarImage2_10() { return &___StarImage2_10; }
	inline void set_StarImage2_10(Image_t2042527209 * value)
	{
		___StarImage2_10 = value;
		Il2CppCodeGenWriteBarrier(&___StarImage2_10, value);
	}

	inline static int32_t get_offset_of_UnitObject_11() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___UnitObject_11)); }
	inline GameObject_t1756533147 * get_UnitObject_11() const { return ___UnitObject_11; }
	inline GameObject_t1756533147 ** get_address_of_UnitObject_11() { return &___UnitObject_11; }
	inline void set_UnitObject_11(GameObject_t1756533147 * value)
	{
		___UnitObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___UnitObject_11, value);
	}

	inline static int32_t get_offset_of_traineeUnit_12() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___traineeUnit_12)); }
	inline TraineeUnit_t2929148814 * get_traineeUnit_12() const { return ___traineeUnit_12; }
	inline TraineeUnit_t2929148814 ** get_address_of_traineeUnit_12() { return &___traineeUnit_12; }
	inline void set_traineeUnit_12(TraineeUnit_t2929148814 * value)
	{
		___traineeUnit_12 = value;
		Il2CppCodeGenWriteBarrier(&___traineeUnit_12, value);
	}

	inline static int32_t get_offset_of_Delegate_13() { return static_cast<int32_t>(offsetof(SelectStarPopView_t2352625494, ___Delegate_13)); }
	inline SelectStarPopViewDelegate_t2309401728 * get_Delegate_13() const { return ___Delegate_13; }
	inline SelectStarPopViewDelegate_t2309401728 ** get_address_of_Delegate_13() { return &___Delegate_13; }
	inline void set_Delegate_13(SelectStarPopViewDelegate_t2309401728 * value)
	{
		___Delegate_13 = value;
		Il2CppCodeGenWriteBarrier(&___Delegate_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
