﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Stores_UnityEngine_Purchasing_Result3399123711.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SubscriptionInfo
struct  SubscriptionInfo_t2237741923  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_subscribed
	int32_t ___is_subscribed_0;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_expired
	int32_t ___is_expired_1;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_cancelled
	int32_t ___is_cancelled_2;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_free_trial
	int32_t ___is_free_trial_3;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_auto_renewing
	int32_t ___is_auto_renewing_4;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_introductory_price_period
	int32_t ___is_introductory_price_period_5;
	// System.String UnityEngine.Purchasing.SubscriptionInfo::productId
	String_t* ___productId_6;
	// System.DateTime UnityEngine.Purchasing.SubscriptionInfo::purchaseDate
	DateTime_t693205669  ___purchaseDate_7;
	// System.DateTime UnityEngine.Purchasing.SubscriptionInfo::subscriptionExpireDate
	DateTime_t693205669  ___subscriptionExpireDate_8;
	// System.DateTime UnityEngine.Purchasing.SubscriptionInfo::subscriptionCancelDate
	DateTime_t693205669  ___subscriptionCancelDate_9;
	// System.TimeSpan UnityEngine.Purchasing.SubscriptionInfo::remainedTime
	TimeSpan_t3430258949  ___remainedTime_10;
	// System.String UnityEngine.Purchasing.SubscriptionInfo::introductory_price
	String_t* ___introductory_price_11;
	// System.TimeSpan UnityEngine.Purchasing.SubscriptionInfo::introductory_price_period
	TimeSpan_t3430258949  ___introductory_price_period_12;
	// System.Int64 UnityEngine.Purchasing.SubscriptionInfo::introductory_price_cycles
	int64_t ___introductory_price_cycles_13;
	// System.TimeSpan UnityEngine.Purchasing.SubscriptionInfo::freeTrialPeriod
	TimeSpan_t3430258949  ___freeTrialPeriod_14;
	// System.TimeSpan UnityEngine.Purchasing.SubscriptionInfo::subscriptionPeriod
	TimeSpan_t3430258949  ___subscriptionPeriod_15;
	// System.String UnityEngine.Purchasing.SubscriptionInfo::free_trial_period_string
	String_t* ___free_trial_period_string_16;
	// System.String UnityEngine.Purchasing.SubscriptionInfo::sku_details
	String_t* ___sku_details_17;

public:
	inline static int32_t get_offset_of_is_subscribed_0() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___is_subscribed_0)); }
	inline int32_t get_is_subscribed_0() const { return ___is_subscribed_0; }
	inline int32_t* get_address_of_is_subscribed_0() { return &___is_subscribed_0; }
	inline void set_is_subscribed_0(int32_t value)
	{
		___is_subscribed_0 = value;
	}

	inline static int32_t get_offset_of_is_expired_1() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___is_expired_1)); }
	inline int32_t get_is_expired_1() const { return ___is_expired_1; }
	inline int32_t* get_address_of_is_expired_1() { return &___is_expired_1; }
	inline void set_is_expired_1(int32_t value)
	{
		___is_expired_1 = value;
	}

	inline static int32_t get_offset_of_is_cancelled_2() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___is_cancelled_2)); }
	inline int32_t get_is_cancelled_2() const { return ___is_cancelled_2; }
	inline int32_t* get_address_of_is_cancelled_2() { return &___is_cancelled_2; }
	inline void set_is_cancelled_2(int32_t value)
	{
		___is_cancelled_2 = value;
	}

	inline static int32_t get_offset_of_is_free_trial_3() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___is_free_trial_3)); }
	inline int32_t get_is_free_trial_3() const { return ___is_free_trial_3; }
	inline int32_t* get_address_of_is_free_trial_3() { return &___is_free_trial_3; }
	inline void set_is_free_trial_3(int32_t value)
	{
		___is_free_trial_3 = value;
	}

	inline static int32_t get_offset_of_is_auto_renewing_4() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___is_auto_renewing_4)); }
	inline int32_t get_is_auto_renewing_4() const { return ___is_auto_renewing_4; }
	inline int32_t* get_address_of_is_auto_renewing_4() { return &___is_auto_renewing_4; }
	inline void set_is_auto_renewing_4(int32_t value)
	{
		___is_auto_renewing_4 = value;
	}

	inline static int32_t get_offset_of_is_introductory_price_period_5() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___is_introductory_price_period_5)); }
	inline int32_t get_is_introductory_price_period_5() const { return ___is_introductory_price_period_5; }
	inline int32_t* get_address_of_is_introductory_price_period_5() { return &___is_introductory_price_period_5; }
	inline void set_is_introductory_price_period_5(int32_t value)
	{
		___is_introductory_price_period_5 = value;
	}

	inline static int32_t get_offset_of_productId_6() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___productId_6)); }
	inline String_t* get_productId_6() const { return ___productId_6; }
	inline String_t** get_address_of_productId_6() { return &___productId_6; }
	inline void set_productId_6(String_t* value)
	{
		___productId_6 = value;
		Il2CppCodeGenWriteBarrier(&___productId_6, value);
	}

	inline static int32_t get_offset_of_purchaseDate_7() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___purchaseDate_7)); }
	inline DateTime_t693205669  get_purchaseDate_7() const { return ___purchaseDate_7; }
	inline DateTime_t693205669 * get_address_of_purchaseDate_7() { return &___purchaseDate_7; }
	inline void set_purchaseDate_7(DateTime_t693205669  value)
	{
		___purchaseDate_7 = value;
	}

	inline static int32_t get_offset_of_subscriptionExpireDate_8() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___subscriptionExpireDate_8)); }
	inline DateTime_t693205669  get_subscriptionExpireDate_8() const { return ___subscriptionExpireDate_8; }
	inline DateTime_t693205669 * get_address_of_subscriptionExpireDate_8() { return &___subscriptionExpireDate_8; }
	inline void set_subscriptionExpireDate_8(DateTime_t693205669  value)
	{
		___subscriptionExpireDate_8 = value;
	}

	inline static int32_t get_offset_of_subscriptionCancelDate_9() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___subscriptionCancelDate_9)); }
	inline DateTime_t693205669  get_subscriptionCancelDate_9() const { return ___subscriptionCancelDate_9; }
	inline DateTime_t693205669 * get_address_of_subscriptionCancelDate_9() { return &___subscriptionCancelDate_9; }
	inline void set_subscriptionCancelDate_9(DateTime_t693205669  value)
	{
		___subscriptionCancelDate_9 = value;
	}

	inline static int32_t get_offset_of_remainedTime_10() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___remainedTime_10)); }
	inline TimeSpan_t3430258949  get_remainedTime_10() const { return ___remainedTime_10; }
	inline TimeSpan_t3430258949 * get_address_of_remainedTime_10() { return &___remainedTime_10; }
	inline void set_remainedTime_10(TimeSpan_t3430258949  value)
	{
		___remainedTime_10 = value;
	}

	inline static int32_t get_offset_of_introductory_price_11() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___introductory_price_11)); }
	inline String_t* get_introductory_price_11() const { return ___introductory_price_11; }
	inline String_t** get_address_of_introductory_price_11() { return &___introductory_price_11; }
	inline void set_introductory_price_11(String_t* value)
	{
		___introductory_price_11 = value;
		Il2CppCodeGenWriteBarrier(&___introductory_price_11, value);
	}

	inline static int32_t get_offset_of_introductory_price_period_12() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___introductory_price_period_12)); }
	inline TimeSpan_t3430258949  get_introductory_price_period_12() const { return ___introductory_price_period_12; }
	inline TimeSpan_t3430258949 * get_address_of_introductory_price_period_12() { return &___introductory_price_period_12; }
	inline void set_introductory_price_period_12(TimeSpan_t3430258949  value)
	{
		___introductory_price_period_12 = value;
	}

	inline static int32_t get_offset_of_introductory_price_cycles_13() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___introductory_price_cycles_13)); }
	inline int64_t get_introductory_price_cycles_13() const { return ___introductory_price_cycles_13; }
	inline int64_t* get_address_of_introductory_price_cycles_13() { return &___introductory_price_cycles_13; }
	inline void set_introductory_price_cycles_13(int64_t value)
	{
		___introductory_price_cycles_13 = value;
	}

	inline static int32_t get_offset_of_freeTrialPeriod_14() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___freeTrialPeriod_14)); }
	inline TimeSpan_t3430258949  get_freeTrialPeriod_14() const { return ___freeTrialPeriod_14; }
	inline TimeSpan_t3430258949 * get_address_of_freeTrialPeriod_14() { return &___freeTrialPeriod_14; }
	inline void set_freeTrialPeriod_14(TimeSpan_t3430258949  value)
	{
		___freeTrialPeriod_14 = value;
	}

	inline static int32_t get_offset_of_subscriptionPeriod_15() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___subscriptionPeriod_15)); }
	inline TimeSpan_t3430258949  get_subscriptionPeriod_15() const { return ___subscriptionPeriod_15; }
	inline TimeSpan_t3430258949 * get_address_of_subscriptionPeriod_15() { return &___subscriptionPeriod_15; }
	inline void set_subscriptionPeriod_15(TimeSpan_t3430258949  value)
	{
		___subscriptionPeriod_15 = value;
	}

	inline static int32_t get_offset_of_free_trial_period_string_16() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___free_trial_period_string_16)); }
	inline String_t* get_free_trial_period_string_16() const { return ___free_trial_period_string_16; }
	inline String_t** get_address_of_free_trial_period_string_16() { return &___free_trial_period_string_16; }
	inline void set_free_trial_period_string_16(String_t* value)
	{
		___free_trial_period_string_16 = value;
		Il2CppCodeGenWriteBarrier(&___free_trial_period_string_16, value);
	}

	inline static int32_t get_offset_of_sku_details_17() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t2237741923, ___sku_details_17)); }
	inline String_t* get_sku_details_17() const { return ___sku_details_17; }
	inline String_t** get_address_of_sku_details_17() { return &___sku_details_17; }
	inline void set_sku_details_17(String_t* value)
	{
		___sku_details_17 = value;
		Il2CppCodeGenWriteBarrier(&___sku_details_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
