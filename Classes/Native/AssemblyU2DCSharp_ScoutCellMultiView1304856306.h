﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ScoutCellView2404854735.h"

// ScoutCell
struct ScoutCell_t2879086938;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellMultiView
struct  ScoutCellMultiView_t1304856306  : public ScoutCellView_t2404854735
{
public:
	// ScoutCell ScoutCellMultiView::_parentCell
	ScoutCell_t2879086938 * ____parentCell_2;
	// UnityEngine.UI.Button[] ScoutCellMultiView::Button
	ButtonU5BU5D_t3071100561* ___Button_3;
	// UnityEngine.UI.Text[] ScoutCellMultiView::NameText
	TextU5BU5D_t4216439300* ___NameText_4;
	// UnityEngine.UI.Text[] ScoutCellMultiView::AmountText
	TextU5BU5D_t4216439300* ___AmountText_5;
	// UnityEngine.UI.Text[] ScoutCellMultiView::PriceText
	TextU5BU5D_t4216439300* ___PriceText_6;
	// UnityEngine.UI.Text[] ScoutCellMultiView::GradeText
	TextU5BU5D_t4216439300* ___GradeText_7;
	// UnityEngine.UI.Image[] ScoutCellMultiView::FlagImage
	ImageU5BU5D_t590162004* ___FlagImage_8;
	// UnityEngine.GameObject[] ScoutCellMultiView::PlayerObject
	GameObjectU5BU5D_t3057952154* ___PlayerObject_9;
	// System.Boolean ScoutCellMultiView::IsReload
	bool ___IsReload_10;
	// System.Single[] ScoutCellMultiView::originPosX
	SingleU5BU5D_t577127397* ___originPosX_11;

public:
	inline static int32_t get_offset_of__parentCell_2() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ____parentCell_2)); }
	inline ScoutCell_t2879086938 * get__parentCell_2() const { return ____parentCell_2; }
	inline ScoutCell_t2879086938 ** get_address_of__parentCell_2() { return &____parentCell_2; }
	inline void set__parentCell_2(ScoutCell_t2879086938 * value)
	{
		____parentCell_2 = value;
		Il2CppCodeGenWriteBarrier(&____parentCell_2, value);
	}

	inline static int32_t get_offset_of_Button_3() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___Button_3)); }
	inline ButtonU5BU5D_t3071100561* get_Button_3() const { return ___Button_3; }
	inline ButtonU5BU5D_t3071100561** get_address_of_Button_3() { return &___Button_3; }
	inline void set_Button_3(ButtonU5BU5D_t3071100561* value)
	{
		___Button_3 = value;
		Il2CppCodeGenWriteBarrier(&___Button_3, value);
	}

	inline static int32_t get_offset_of_NameText_4() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___NameText_4)); }
	inline TextU5BU5D_t4216439300* get_NameText_4() const { return ___NameText_4; }
	inline TextU5BU5D_t4216439300** get_address_of_NameText_4() { return &___NameText_4; }
	inline void set_NameText_4(TextU5BU5D_t4216439300* value)
	{
		___NameText_4 = value;
		Il2CppCodeGenWriteBarrier(&___NameText_4, value);
	}

	inline static int32_t get_offset_of_AmountText_5() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___AmountText_5)); }
	inline TextU5BU5D_t4216439300* get_AmountText_5() const { return ___AmountText_5; }
	inline TextU5BU5D_t4216439300** get_address_of_AmountText_5() { return &___AmountText_5; }
	inline void set_AmountText_5(TextU5BU5D_t4216439300* value)
	{
		___AmountText_5 = value;
		Il2CppCodeGenWriteBarrier(&___AmountText_5, value);
	}

	inline static int32_t get_offset_of_PriceText_6() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___PriceText_6)); }
	inline TextU5BU5D_t4216439300* get_PriceText_6() const { return ___PriceText_6; }
	inline TextU5BU5D_t4216439300** get_address_of_PriceText_6() { return &___PriceText_6; }
	inline void set_PriceText_6(TextU5BU5D_t4216439300* value)
	{
		___PriceText_6 = value;
		Il2CppCodeGenWriteBarrier(&___PriceText_6, value);
	}

	inline static int32_t get_offset_of_GradeText_7() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___GradeText_7)); }
	inline TextU5BU5D_t4216439300* get_GradeText_7() const { return ___GradeText_7; }
	inline TextU5BU5D_t4216439300** get_address_of_GradeText_7() { return &___GradeText_7; }
	inline void set_GradeText_7(TextU5BU5D_t4216439300* value)
	{
		___GradeText_7 = value;
		Il2CppCodeGenWriteBarrier(&___GradeText_7, value);
	}

	inline static int32_t get_offset_of_FlagImage_8() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___FlagImage_8)); }
	inline ImageU5BU5D_t590162004* get_FlagImage_8() const { return ___FlagImage_8; }
	inline ImageU5BU5D_t590162004** get_address_of_FlagImage_8() { return &___FlagImage_8; }
	inline void set_FlagImage_8(ImageU5BU5D_t590162004* value)
	{
		___FlagImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___FlagImage_8, value);
	}

	inline static int32_t get_offset_of_PlayerObject_9() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___PlayerObject_9)); }
	inline GameObjectU5BU5D_t3057952154* get_PlayerObject_9() const { return ___PlayerObject_9; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PlayerObject_9() { return &___PlayerObject_9; }
	inline void set_PlayerObject_9(GameObjectU5BU5D_t3057952154* value)
	{
		___PlayerObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerObject_9, value);
	}

	inline static int32_t get_offset_of_IsReload_10() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___IsReload_10)); }
	inline bool get_IsReload_10() const { return ___IsReload_10; }
	inline bool* get_address_of_IsReload_10() { return &___IsReload_10; }
	inline void set_IsReload_10(bool value)
	{
		___IsReload_10 = value;
	}

	inline static int32_t get_offset_of_originPosX_11() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306, ___originPosX_11)); }
	inline SingleU5BU5D_t577127397* get_originPosX_11() const { return ___originPosX_11; }
	inline SingleU5BU5D_t577127397** get_address_of_originPosX_11() { return &___originPosX_11; }
	inline void set_originPosX_11(SingleU5BU5D_t577127397* value)
	{
		___originPosX_11 = value;
		Il2CppCodeGenWriteBarrier(&___originPosX_11, value);
	}
};

struct ScoutCellMultiView_t1304856306_StaticFields
{
public:
	// System.Action`1<System.Int32> ScoutCellMultiView::<>f__am$cache0
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(ScoutCellMultiView_t1304856306_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
