﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_Tacticsoft_TableView1276614623.h"
#include "AssemblyU2DCSharp_NationCellType3781181491.h"

// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// NationSimpleInfo[]
struct NationSimpleInfoU5BU5D_t321925280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectMenuCell
struct  CollectMenuCell_t2927503929  : public TableViewCell_t1276614623
{
public:
	// UnityEngine.UI.Button[] CollectMenuCell::Button
	ButtonU5BU5D_t3071100561* ___Button_2;
	// UnityEngine.UI.Text[] CollectMenuCell::NationText
	TextU5BU5D_t4216439300* ___NationText_3;
	// UnityEngine.UI.Text[] CollectMenuCell::PercentText
	TextU5BU5D_t4216439300* ___PercentText_4;
	// UnityEngine.UI.Image[] CollectMenuCell::BlindPanel
	ImageU5BU5D_t590162004* ___BlindPanel_5;
	// UnityEngine.UI.Image[] CollectMenuCell::FlagImage
	ImageU5BU5D_t590162004* ___FlagImage_6;
	// UnityEngine.UI.Image[] CollectMenuCell::BadgeImage
	ImageU5BU5D_t590162004* ___BadgeImage_7;
	// UnityEngine.UI.Image[] CollectMenuCell::GaugeBgImage
	ImageU5BU5D_t590162004* ___GaugeBgImage_8;
	// UnityEngine.UI.Image[] CollectMenuCell::GaugeImage
	ImageU5BU5D_t590162004* ___GaugeImage_9;
	// UnityEngine.UI.Text[] CollectMenuCell::ComingSoonText
	TextU5BU5D_t4216439300* ___ComingSoonText_10;
	// UnityEngine.Material CollectMenuCell::GrayscaleMat
	Material_t193706927 * ___GrayscaleMat_11;
	// UnityEngine.RectTransform CollectMenuCell::gaugeRect
	RectTransform_t3349966182 * ___gaugeRect_12;
	// NationSimpleInfo[] CollectMenuCell::_nationInfo
	NationSimpleInfoU5BU5D_t321925280* ____nationInfo_13;
	// System.Int32 CollectMenuCell::_rowNum
	int32_t ____rowNum_14;
	// NationCellType CollectMenuCell::_cellType
	int32_t ____cellType_15;

public:
	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___Button_2)); }
	inline ButtonU5BU5D_t3071100561* get_Button_2() const { return ___Button_2; }
	inline ButtonU5BU5D_t3071100561** get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(ButtonU5BU5D_t3071100561* value)
	{
		___Button_2 = value;
		Il2CppCodeGenWriteBarrier(&___Button_2, value);
	}

	inline static int32_t get_offset_of_NationText_3() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___NationText_3)); }
	inline TextU5BU5D_t4216439300* get_NationText_3() const { return ___NationText_3; }
	inline TextU5BU5D_t4216439300** get_address_of_NationText_3() { return &___NationText_3; }
	inline void set_NationText_3(TextU5BU5D_t4216439300* value)
	{
		___NationText_3 = value;
		Il2CppCodeGenWriteBarrier(&___NationText_3, value);
	}

	inline static int32_t get_offset_of_PercentText_4() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___PercentText_4)); }
	inline TextU5BU5D_t4216439300* get_PercentText_4() const { return ___PercentText_4; }
	inline TextU5BU5D_t4216439300** get_address_of_PercentText_4() { return &___PercentText_4; }
	inline void set_PercentText_4(TextU5BU5D_t4216439300* value)
	{
		___PercentText_4 = value;
		Il2CppCodeGenWriteBarrier(&___PercentText_4, value);
	}

	inline static int32_t get_offset_of_BlindPanel_5() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___BlindPanel_5)); }
	inline ImageU5BU5D_t590162004* get_BlindPanel_5() const { return ___BlindPanel_5; }
	inline ImageU5BU5D_t590162004** get_address_of_BlindPanel_5() { return &___BlindPanel_5; }
	inline void set_BlindPanel_5(ImageU5BU5D_t590162004* value)
	{
		___BlindPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___BlindPanel_5, value);
	}

	inline static int32_t get_offset_of_FlagImage_6() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___FlagImage_6)); }
	inline ImageU5BU5D_t590162004* get_FlagImage_6() const { return ___FlagImage_6; }
	inline ImageU5BU5D_t590162004** get_address_of_FlagImage_6() { return &___FlagImage_6; }
	inline void set_FlagImage_6(ImageU5BU5D_t590162004* value)
	{
		___FlagImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___FlagImage_6, value);
	}

	inline static int32_t get_offset_of_BadgeImage_7() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___BadgeImage_7)); }
	inline ImageU5BU5D_t590162004* get_BadgeImage_7() const { return ___BadgeImage_7; }
	inline ImageU5BU5D_t590162004** get_address_of_BadgeImage_7() { return &___BadgeImage_7; }
	inline void set_BadgeImage_7(ImageU5BU5D_t590162004* value)
	{
		___BadgeImage_7 = value;
		Il2CppCodeGenWriteBarrier(&___BadgeImage_7, value);
	}

	inline static int32_t get_offset_of_GaugeBgImage_8() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___GaugeBgImage_8)); }
	inline ImageU5BU5D_t590162004* get_GaugeBgImage_8() const { return ___GaugeBgImage_8; }
	inline ImageU5BU5D_t590162004** get_address_of_GaugeBgImage_8() { return &___GaugeBgImage_8; }
	inline void set_GaugeBgImage_8(ImageU5BU5D_t590162004* value)
	{
		___GaugeBgImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___GaugeBgImage_8, value);
	}

	inline static int32_t get_offset_of_GaugeImage_9() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___GaugeImage_9)); }
	inline ImageU5BU5D_t590162004* get_GaugeImage_9() const { return ___GaugeImage_9; }
	inline ImageU5BU5D_t590162004** get_address_of_GaugeImage_9() { return &___GaugeImage_9; }
	inline void set_GaugeImage_9(ImageU5BU5D_t590162004* value)
	{
		___GaugeImage_9 = value;
		Il2CppCodeGenWriteBarrier(&___GaugeImage_9, value);
	}

	inline static int32_t get_offset_of_ComingSoonText_10() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___ComingSoonText_10)); }
	inline TextU5BU5D_t4216439300* get_ComingSoonText_10() const { return ___ComingSoonText_10; }
	inline TextU5BU5D_t4216439300** get_address_of_ComingSoonText_10() { return &___ComingSoonText_10; }
	inline void set_ComingSoonText_10(TextU5BU5D_t4216439300* value)
	{
		___ComingSoonText_10 = value;
		Il2CppCodeGenWriteBarrier(&___ComingSoonText_10, value);
	}

	inline static int32_t get_offset_of_GrayscaleMat_11() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___GrayscaleMat_11)); }
	inline Material_t193706927 * get_GrayscaleMat_11() const { return ___GrayscaleMat_11; }
	inline Material_t193706927 ** get_address_of_GrayscaleMat_11() { return &___GrayscaleMat_11; }
	inline void set_GrayscaleMat_11(Material_t193706927 * value)
	{
		___GrayscaleMat_11 = value;
		Il2CppCodeGenWriteBarrier(&___GrayscaleMat_11, value);
	}

	inline static int32_t get_offset_of_gaugeRect_12() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ___gaugeRect_12)); }
	inline RectTransform_t3349966182 * get_gaugeRect_12() const { return ___gaugeRect_12; }
	inline RectTransform_t3349966182 ** get_address_of_gaugeRect_12() { return &___gaugeRect_12; }
	inline void set_gaugeRect_12(RectTransform_t3349966182 * value)
	{
		___gaugeRect_12 = value;
		Il2CppCodeGenWriteBarrier(&___gaugeRect_12, value);
	}

	inline static int32_t get_offset_of__nationInfo_13() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ____nationInfo_13)); }
	inline NationSimpleInfoU5BU5D_t321925280* get__nationInfo_13() const { return ____nationInfo_13; }
	inline NationSimpleInfoU5BU5D_t321925280** get_address_of__nationInfo_13() { return &____nationInfo_13; }
	inline void set__nationInfo_13(NationSimpleInfoU5BU5D_t321925280* value)
	{
		____nationInfo_13 = value;
		Il2CppCodeGenWriteBarrier(&____nationInfo_13, value);
	}

	inline static int32_t get_offset_of__rowNum_14() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ____rowNum_14)); }
	inline int32_t get__rowNum_14() const { return ____rowNum_14; }
	inline int32_t* get_address_of__rowNum_14() { return &____rowNum_14; }
	inline void set__rowNum_14(int32_t value)
	{
		____rowNum_14 = value;
	}

	inline static int32_t get_offset_of__cellType_15() { return static_cast<int32_t>(offsetof(CollectMenuCell_t2927503929, ____cellType_15)); }
	inline int32_t get__cellType_15() const { return ____cellType_15; }
	inline int32_t* get_address_of__cellType_15() { return &____cellType_15; }
	inline void set__cellType_15(int32_t value)
	{
		____cellType_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
