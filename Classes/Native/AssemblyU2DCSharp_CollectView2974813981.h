﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectView
struct  CollectView_t2974813981  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject CollectView::CollectMenuPanel
	GameObject_t1756533147 * ___CollectMenuPanel_2;
	// UnityEngine.GameObject CollectView::CollectLineupPanel
	GameObject_t1756533147 * ___CollectLineupPanel_3;
	// UnityEngine.UI.Button CollectView::AchievementButton
	Button_t2872111280 * ___AchievementButton_4;

public:
	inline static int32_t get_offset_of_CollectMenuPanel_2() { return static_cast<int32_t>(offsetof(CollectView_t2974813981, ___CollectMenuPanel_2)); }
	inline GameObject_t1756533147 * get_CollectMenuPanel_2() const { return ___CollectMenuPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_CollectMenuPanel_2() { return &___CollectMenuPanel_2; }
	inline void set_CollectMenuPanel_2(GameObject_t1756533147 * value)
	{
		___CollectMenuPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___CollectMenuPanel_2, value);
	}

	inline static int32_t get_offset_of_CollectLineupPanel_3() { return static_cast<int32_t>(offsetof(CollectView_t2974813981, ___CollectLineupPanel_3)); }
	inline GameObject_t1756533147 * get_CollectLineupPanel_3() const { return ___CollectLineupPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_CollectLineupPanel_3() { return &___CollectLineupPanel_3; }
	inline void set_CollectLineupPanel_3(GameObject_t1756533147 * value)
	{
		___CollectLineupPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___CollectLineupPanel_3, value);
	}

	inline static int32_t get_offset_of_AchievementButton_4() { return static_cast<int32_t>(offsetof(CollectView_t2974813981, ___AchievementButton_4)); }
	inline Button_t2872111280 * get_AchievementButton_4() const { return ___AchievementButton_4; }
	inline Button_t2872111280 ** get_address_of_AchievementButton_4() { return &___AchievementButton_4; }
	inline void set_AchievementButton_4(Button_t2872111280 * value)
	{
		___AchievementButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___AchievementButton_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
