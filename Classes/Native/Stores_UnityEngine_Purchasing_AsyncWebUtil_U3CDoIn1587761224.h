﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3226471752;
// UnityEngine.Purchasing.AsyncWebUtil
struct AsyncWebUtil_t1370427196;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__3
struct  U3CDoInvokeU3Ed__3_t1587761224  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__3::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// System.Action UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__3::a
	Action_t3226471752 * ___a_2;
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__3::delayInSeconds
	int32_t ___delayInSeconds_3;
	// UnityEngine.Purchasing.AsyncWebUtil UnityEngine.Purchasing.AsyncWebUtil/<DoInvoke>d__3::<>4__this
	AsyncWebUtil_t1370427196 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t1587761224, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t1587761224, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_a_2() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t1587761224, ___a_2)); }
	inline Action_t3226471752 * get_a_2() const { return ___a_2; }
	inline Action_t3226471752 ** get_address_of_a_2() { return &___a_2; }
	inline void set_a_2(Action_t3226471752 * value)
	{
		___a_2 = value;
		Il2CppCodeGenWriteBarrier(&___a_2, value);
	}

	inline static int32_t get_offset_of_delayInSeconds_3() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t1587761224, ___delayInSeconds_3)); }
	inline int32_t get_delayInSeconds_3() const { return ___delayInSeconds_3; }
	inline int32_t* get_address_of_delayInSeconds_3() { return &___delayInSeconds_3; }
	inline void set_delayInSeconds_3(int32_t value)
	{
		___delayInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t1587761224, ___U3CU3E4__this_4)); }
	inline AsyncWebUtil_t1370427196 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline AsyncWebUtil_t1370427196 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(AsyncWebUtil_t1370427196 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
