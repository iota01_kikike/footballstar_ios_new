﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2523981134.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct  ObscuredDecimal_t3299336390 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::currentCryptoKey
	int64_t ___currentCryptoKey_1;
	// System.Byte[] CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::hiddenValueOld
	ByteU5BU5D_t3397334013* ___hiddenValueOld_2;
	// CodeStage.AntiCheat.Common.ACTkByte16 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::hiddenValue
	ACTkByte16_t2523981134  ___hiddenValue_3;
	// System.Decimal CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::fakeValue
	Decimal_t724701077  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::inited
	bool ___inited_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t3299336390, ___currentCryptoKey_1)); }
	inline int64_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int64_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int64_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValueOld_2() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t3299336390, ___hiddenValueOld_2)); }
	inline ByteU5BU5D_t3397334013* get_hiddenValueOld_2() const { return ___hiddenValueOld_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_hiddenValueOld_2() { return &___hiddenValueOld_2; }
	inline void set_hiddenValueOld_2(ByteU5BU5D_t3397334013* value)
	{
		___hiddenValueOld_2 = value;
		Il2CppCodeGenWriteBarrier(&___hiddenValueOld_2, value);
	}

	inline static int32_t get_offset_of_hiddenValue_3() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t3299336390, ___hiddenValue_3)); }
	inline ACTkByte16_t2523981134  get_hiddenValue_3() const { return ___hiddenValue_3; }
	inline ACTkByte16_t2523981134 * get_address_of_hiddenValue_3() { return &___hiddenValue_3; }
	inline void set_hiddenValue_3(ACTkByte16_t2523981134  value)
	{
		___hiddenValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t3299336390, ___fakeValue_4)); }
	inline Decimal_t724701077  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Decimal_t724701077 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Decimal_t724701077  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_inited_5() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t3299336390, ___inited_5)); }
	inline bool get_inited_5() const { return ___inited_5; }
	inline bool* get_address_of_inited_5() { return &___inited_5; }
	inline void set_inited_5(bool value)
	{
		___inited_5 = value;
	}
};

struct ObscuredDecimal_t3299336390_StaticFields
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::cryptoKey
	int64_t ___cryptoKey_0;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t3299336390_StaticFields, ___cryptoKey_0)); }
	inline int64_t get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline int64_t* get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(int64_t value)
	{
		___cryptoKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct ObscuredDecimal_t3299336390_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_1;
	uint8_t* ___hiddenValueOld_2;
	ACTkByte16_t2523981134  ___hiddenValue_3;
	Decimal_t724701077  ___fakeValue_4;
	int32_t ___inited_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct ObscuredDecimal_t3299336390_marshaled_com
{
	int64_t ___currentCryptoKey_1;
	uint8_t* ___hiddenValueOld_2;
	ACTkByte16_t2523981134  ___hiddenValue_3;
	Decimal_t724701077  ___fakeValue_4;
	int32_t ___inited_5;
};
