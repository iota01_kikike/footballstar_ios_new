﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameOverPop2955216129.h"
#include "AssemblyU2DCSharp_SelectNationPop2763424430.h"
#include "AssemblyU2DCSharp_SelectStarPopView2352625494.h"
#include "AssemblyU2DCSharp_SelectStarPopView_SelectStarPopV2309401728.h"
#include "AssemblyU2DCSharp_TrainingView2704080403.h"
#include "AssemblyU2DCSharp_TrainingView_Dir1137582919.h"
#include "AssemblyU2DCSharp_TrainingView_TileData2249013992.h"
#include "AssemblyU2DCSharp_TrainingView_U3CCreateOneUnitU3E1421011047.h"
#include "AssemblyU2DCSharp_TrainingView_U3CMoveOneTileU3Ec_2934086062.h"
#include "AssemblyU2DCSharp_TrainingView_U3CMoveOneTileU3Ec_1368002121.h"
#include "AssemblyU2DCSharp_TrainingView_U3CShuffleBlockU3Ec1755741014.h"
#include "AssemblyU2DCSharp_TrainingView_U3COnClickStarU3Ec_1350708256.h"
#include "AssemblyU2DCSharp_TrainingView_U3COnClickStarU3Ec_4079591611.h"
#include "AssemblyU2DCSharp_TrainingView_U3CGainGemU3Ec__Ano2228177261.h"
#include "AssemblyU2DCSharp_TrainingView_U3CGainCoinU3Ec__An1534706638.h"
#include "AssemblyU2DCSharp_UnitInfoPop2203256789.h"
#include "AssemblyU2DCSharp_InfoType242837520.h"
#include "AssemblyU2DCSharp_MySquadView1225996665.h"
#include "AssemblyU2DCSharp_MySquadView_U3CAwakeU3Ec__AnonSt2394710539.h"
#include "AssemblyU2DCSharp_MySquadView_U3CAwakeU3Ec__AnonSt2394710536.h"
#include "AssemblyU2DCSharp_MySquadView_U3CTakeSSAndShareU3E2292767412.h"
#include "AssemblyU2DCSharp_SelectPositionCell908287989.h"
#include "AssemblyU2DCSharp_SelectPositionCell_U3CStartU3Ec_1765731431.h"
#include "AssemblyU2DCSharp_SelectPositionPopView2293217285.h"
#include "AssemblyU2DCSharp_SelectPositionPopView_U3CAwakeU32123783708.h"
#include "AssemblyU2DCSharp_SelectPositionPopView_U3CLoadPla2607580775.h"
#include "AssemblyU2DCSharp_Coupon2313665098.h"
#include "AssemblyU2DCSharp_CouponPop2786823949.h"
#include "AssemblyU2DCSharp_MyRecordView1262634078.h"
#include "AssemblyU2DCSharp_OptionView81925000.h"
#include "AssemblyU2DCSharp_AdvertisePop3889815228.h"
#include "AssemblyU2DCSharp_CostType3548590213.h"
#include "AssemblyU2DCSharp_AlertView4232360573.h"
#include "AssemblyU2DCSharp_BuyGemPop3502837550.h"
#include "AssemblyU2DCSharp_CheckPlayerView3953299720.h"
#include "AssemblyU2DCSharp_GuidePop1959359454.h"
#include "AssemblyU2DCSharp_GuidePop1_U3CStartAnimationU3Ec_3548337856.h"
#include "AssemblyU2DCSharp_GuidePop2959359457.h"
#include "AssemblyU2DCSharp_PlayerInfoPopView4205922565.h"
#include "AssemblyU2DCSharp_PlayerInfoPopView_U3CCoAddCardCo1807627623.h"
#include "AssemblyU2DCSharp_AdMobController3743396963.h"
#include "AssemblyU2DCSharp_DBManager2841981931.h"
#include "AssemblyU2DCSharp_FormulaManager3329446299.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_GameManager_U3CCoPlayTimerU3Ec__I895334160.h"
#include "AssemblyU2DCSharp_WebData1635253674.h"
#include "AssemblyU2DCSharp_IAPManager2850027101.h"
#include "AssemblyU2DCSharp_IAPManager_U3CWaitForRequestU3Ec1300943864.h"
#include "AssemblyU2DCSharp_ResourceManager4136494783.h"
#include "AssemblyU2DCSharp_SoundManager654432262.h"
#include "AssemblyU2DCSharp_UIManager2519183485.h"
#include "AssemblyU2DCSharp_Nation3431670537.h"
#include "AssemblyU2DCSharp_GameData450274222.h"
#include "AssemblyU2DCSharp_Person3241917763.h"
#include "AssemblyU2DCSharp_Player1147783557.h"
#include "AssemblyU2DCSharp_PlayerUserData4090529802.h"
#include "AssemblyU2DCSharp_SquadUserData3007047065.h"
#include "AssemblyU2DCSharp_TraineeUnit2929148814.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_NewGameView3231952493.h"
#include "AssemblyU2DCSharp_UIPopAction3469027355.h"
#include "AssemblyU2DCSharp_FormEmptyUIView2064892716.h"
#include "AssemblyU2DCSharp_FormHelper3562480506.h"
#include "AssemblyU2DCSharp_FormPlayer4046102703.h"
#include "AssemblyU2DCSharp_FormUIType1820353380.h"
#include "AssemblyU2DCSharp_FormPlayerUIView1488813792.h"
#include "AssemblyU2DCSharp_FormPlayerUIView_DelegateOnChange798682617.h"
#include "AssemblyU2DCSharp_UnitDataHolder2129886096.h"
#include "AssemblyU2DCSharp_UnitUIView3306525905.h"
#include "AssemblyU2DCSharp_Debug154383703.h"
#include "AssemblyU2DCSharp_Serializer3184141204.h"
#include "AssemblyU2DCSharp_Util4006552276.h"
#include "AssemblyU2DCSharp_EZObjectPools_EZObjectPool3968219684.h"
#include "AssemblyU2DCSharp_ParticleSystemDisable496244635.h"
#include "AssemblyU2DCSharp_EZObjectPools_PooledObject2079175126.h"
#include "AssemblyU2DCSharp_TimedDisable2667936835.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_API2390511294.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_Reader1391219339.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_Settings1795059213.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_SpreadsheetData567722816.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_DataType848016694.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_RanageDirection1288290907.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_VariableType3941595272.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_Cell3987632660.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_CellDictionaryRange1484450799.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_CellRange4079182527.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_DataComponentConnect366829713.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_DataDelegate1852602171.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_DocTemplate209748574.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_GoogleData1520579079.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_WorksheetTemplate3784200896.h"
#include "AssemblyU2DCSharp_GDMiniJSON_Json1582238276.h"
#include "AssemblyU2DCSharp_GDMiniJSON_Json_Parser3557338248.h"
#include "AssemblyU2DCSharp_GDMiniJSON_Json_Parser_TOKEN2343128322.h"
#include "AssemblyU2DCSharp_GDMiniJSON_Json_Serializer3424133211.h"
#include "AssemblyU2DCSharp_OnDocLoaded4025950952.h"
#include "AssemblyU2DCSharp_WWWDocLoader336628936.h"
#include "AssemblyU2DCSharp_WWWDocLoader_U3CWaitForRequestU33800969493.h"
#include "AssemblyU2DCSharp_DocDataRertivalExamle2328889893.h"
#include "AssemblyU2DCSharp_ExampleSpawner1272242923.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (GameOverPop_t2955216129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[4] = 
{
	GameOverPop_t2955216129::get_offset_of_CloseButton_2(),
	GameOverPop_t2955216129::get_offset_of_Button1_3(),
	GameOverPop_t2955216129::get_offset_of_Button2_4(),
	GameOverPop_t2955216129::get_offset_of_Callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (SelectNationPop_t2763424430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[7] = 
{
	SelectNationPop_t2763424430::get_offset_of_CloseButton_2(),
	SelectNationPop_t2763424430::get_offset_of_m_cellPrefab_3(),
	SelectNationPop_t2763424430::get_offset_of_m_tableView_4(),
	SelectNationPop_t2763424430::get_offset_of_m_numInstancesCreated_5(),
	SelectNationPop_t2763424430::get_offset_of_nationDict_6(),
	SelectNationPop_t2763424430::get_offset_of_nationInfoList_7(),
	SelectNationPop_t2763424430::get_offset_of_Callback_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (SelectStarPopView_t2352625494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[12] = 
{
	SelectStarPopView_t2352625494::get_offset_of_MessageText_2(),
	SelectStarPopView_t2352625494::get_offset_of_CloseButton_3(),
	SelectStarPopView_t2352625494::get_offset_of_Button1_4(),
	SelectStarPopView_t2352625494::get_offset_of_Button2_5(),
	SelectStarPopView_t2352625494::get_offset_of_Button3_6(),
	SelectStarPopView_t2352625494::get_offset_of_GemCntText_7(),
	SelectStarPopView_t2352625494::get_offset_of_UnitNameText_8(),
	SelectStarPopView_t2352625494::get_offset_of_StarImage1_9(),
	SelectStarPopView_t2352625494::get_offset_of_StarImage2_10(),
	SelectStarPopView_t2352625494::get_offset_of_UnitObject_11(),
	SelectStarPopView_t2352625494::get_offset_of_traineeUnit_12(),
	SelectStarPopView_t2352625494::get_offset_of_Delegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (SelectStarPopViewDelegate_t2309401728), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (TrainingView_t2704080403), -1, sizeof(TrainingView_t2704080403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2504[34] = 
{
	TrainingView_t2704080403::get_offset_of_TileBase_2(),
	TrainingView_t2704080403::get_offset_of_ScorePanel_3(),
	TrainingView_t2704080403::get_offset_of_HighScoreText_4(),
	TrainingView_t2704080403::get_offset_of_ScoreText_5(),
	TrainingView_t2704080403::get_offset_of_DoubleButton_6(),
	TrainingView_t2704080403::get_offset_of_ChanceText_7(),
	TrainingView_t2704080403::get_offset_of_Double2Text_8(),
	TrainingView_t2704080403::get_offset_of_DoubleRemainText_9(),
	TrainingView_t2704080403::get_offset_of_LeaderboardButton_10(),
	TrainingView_t2704080403::get_offset_of_InfoButton_11(),
	TrainingView_t2704080403::get_offset_of_ResetButton_12(),
	TrainingView_t2704080403::get_offset_of_GrayscaleMat_13(),
	TrainingView_t2704080403::get_offset_of_TileOffMat_14(),
	TrainingView_t2704080403::get_offset_of_TileOnMat_15(),
	TrainingView_t2704080403::get_offset_of_tileList_16(),
	TrainingView_t2704080403::get_offset_of_traineeDict_17(),
	TrainingView_t2704080403::get_offset_of_turn_18(),
	TrainingView_t2704080403::get_offset_of_unitOriginScale_19(),
	TrainingView_t2704080403::get_offset_of_unitZoomScale_20(),
	TrainingView_t2704080403::get_offset_of_beginPos_21(),
	TrainingView_t2704080403::get_offset_of_isMoving_22(),
	TrainingView_t2704080403::get_offset_of_finDragging_23(),
	TrainingView_t2704080403::get_offset_of_unitCenterPos_24(),
	0,
	0,
	TrainingView_t2704080403::get_offset_of_coinIcon_27(),
	TrainingView_t2704080403::get_offset_of_gemIcon_28(),
	TrainingView_t2704080403::get_offset_of_coinIconOriginScale_29(),
	TrainingView_t2704080403::get_offset_of_gemIconOriginScale_30(),
	TrainingView_t2704080403::get_offset_of_doubleSeq_31(),
	TrainingView_t2704080403_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
	TrainingView_t2704080403_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_33(),
	TrainingView_t2704080403_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_34(),
	TrainingView_t2704080403_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (Dir_t1137582919)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[6] = 
{
	Dir_t1137582919::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (TileData_t2249013992)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	TileData_t2249013992::get_offset_of_Trainee_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (U3CCreateOneUnitU3Ec__AnonStorey0_t1421011047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[3] = 
{
	U3CCreateOneUnitU3Ec__AnonStorey0_t1421011047::get_offset_of_trainee_0(),
	U3CCreateOneUnitU3Ec__AnonStorey0_t1421011047::get_offset_of_unitUIObj_1(),
	U3CCreateOneUnitU3Ec__AnonStorey0_t1421011047::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (U3CMoveOneTileU3Ec__AnonStorey2_t2934086062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[2] = 
{
	U3CMoveOneTileU3Ec__AnonStorey2_t2934086062::get_offset_of_trainee_0(),
	U3CMoveOneTileU3Ec__AnonStorey2_t2934086062::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (U3CMoveOneTileU3Ec__AnonStorey1_t1368002121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[2] = 
{
	U3CMoveOneTileU3Ec__AnonStorey1_t1368002121::get_offset_of_removeTrainee_0(),
	U3CMoveOneTileU3Ec__AnonStorey1_t1368002121::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (U3CShuffleBlockU3Ec__AnonStorey3_t1755741014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	U3CShuffleBlockU3Ec__AnonStorey3_t1755741014::get_offset_of_r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (U3COnClickStarU3Ec__AnonStorey4_t1350708256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[3] = 
{
	U3COnClickStarU3Ec__AnonStorey4_t1350708256::get_offset_of_unitDataHolder_0(),
	U3COnClickStarU3Ec__AnonStorey4_t1350708256::get_offset_of_trainee_1(),
	U3COnClickStarU3Ec__AnonStorey4_t1350708256::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (U3COnClickStarU3Ec__AnonStorey5_t4079591611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[2] = 
{
	U3COnClickStarU3Ec__AnonStorey5_t4079591611::get_offset_of_gemPrice_0(),
	U3COnClickStarU3Ec__AnonStorey5_t4079591611::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (U3CGainGemU3Ec__AnonStorey6_t2228177261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[3] = 
{
	U3CGainGemU3Ec__AnonStorey6_t2228177261::get_offset_of_obj_0(),
	U3CGainGemU3Ec__AnonStorey6_t2228177261::get_offset_of_gemCnt_1(),
	U3CGainGemU3Ec__AnonStorey6_t2228177261::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (U3CGainCoinU3Ec__AnonStorey7_t1534706638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[3] = 
{
	U3CGainCoinU3Ec__AnonStorey7_t1534706638::get_offset_of_obj_0(),
	U3CGainCoinU3Ec__AnonStorey7_t1534706638::get_offset_of_count_1(),
	U3CGainCoinU3Ec__AnonStorey7_t1534706638::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (UnitInfoPop_t2203256789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[4] = 
{
	UnitInfoPop_t2203256789::get_offset_of_CloseButton_2(),
	UnitInfoPop_t2203256789::get_offset_of_CountText_3(),
	UnitInfoPop_t2203256789::get_offset_of_UnitObject_4(),
	UnitInfoPop_t2203256789::get_offset_of_UnitNameText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (InfoType_t242837520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	InfoType_t242837520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (MySquadView_t1225996665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[20] = 
{
	MySquadView_t1225996665::get_offset_of_InfoTypeButton_2(),
	MySquadView_t1225996665::get_offset_of_FormationButton_3(),
	MySquadView_t1225996665::get_offset_of_LeaderboardButton_4(),
	MySquadView_t1225996665::get_offset_of_ShareButton1_5(),
	MySquadView_t1225996665::get_offset_of_ShareButton2_6(),
	MySquadView_t1225996665::get_offset_of_ShareButton3_7(),
	MySquadView_t1225996665::get_offset_of_ShareButton4_8(),
	MySquadView_t1225996665::get_offset_of_DeckButtonList_9(),
	MySquadView_t1225996665::get_offset_of_FormButton_10(),
	MySquadView_t1225996665::get_offset_of_Best11PlayersObj_11(),
	MySquadView_t1225996665::get_offset_of_FormationText_12(),
	MySquadView_t1225996665::get_offset_of_TotalOverallText_13(),
	MySquadView_t1225996665::get_offset_of_SelectFormPanel_14(),
	MySquadView_t1225996665::get_offset_of_curInfoType_15(),
	MySquadView_t1225996665::get_offset_of_curSquadIndex_16(),
	MySquadView_t1225996665::get_offset_of_formObj_17(),
	MySquadView_t1225996665::get_offset_of_formUIDict_18(),
	MySquadView_t1225996665::get_offset_of_selectedFormUIIndex_19(),
	MySquadView_t1225996665::get_offset_of_totalOverall_20(),
	MySquadView_t1225996665::get_offset_of_totalOverallProgress_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (U3CAwakeU3Ec__AnonStorey1_t2394710539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[2] = 
{
	U3CAwakeU3Ec__AnonStorey1_t2394710539::get_offset_of_index_0(),
	U3CAwakeU3Ec__AnonStorey1_t2394710539::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (U3CAwakeU3Ec__AnonStorey2_t2394710536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[2] = 
{
	U3CAwakeU3Ec__AnonStorey2_t2394710536::get_offset_of_index_0(),
	U3CAwakeU3Ec__AnonStorey2_t2394710536::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (U3CTakeSSAndShareU3Ec__Iterator0_t2292767412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[5] = 
{
	U3CTakeSSAndShareU3Ec__Iterator0_t2292767412::get_offset_of_U3CssU3E__0_0(),
	U3CTakeSSAndShareU3Ec__Iterator0_t2292767412::get_offset_of_buttonNo_1(),
	U3CTakeSSAndShareU3Ec__Iterator0_t2292767412::get_offset_of_U24current_2(),
	U3CTakeSSAndShareU3Ec__Iterator0_t2292767412::get_offset_of_U24disposing_3(),
	U3CTakeSSAndShareU3Ec__Iterator0_t2292767412::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (SelectPositionCell_t908287989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[8] = 
{
	SelectPositionCell_t908287989::get_offset_of__row_2(),
	SelectPositionCell_t908287989::get_offset_of_Button_3(),
	SelectPositionCell_t908287989::get_offset_of_NameText_4(),
	SelectPositionCell_t908287989::get_offset_of_OverallText_5(),
	SelectPositionCell_t908287989::get_offset_of_FlagImage_6(),
	SelectPositionCell_t908287989::get_offset_of_OriginHead_7(),
	SelectPositionCell_t908287989::get_offset_of_BlindImage_8(),
	SelectPositionCell_t908287989::get_offset_of_Callback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (U3CStartU3Ec__AnonStorey0_t1765731431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[2] = 
{
	U3CStartU3Ec__AnonStorey0_t1765731431::get_offset_of_rowIndex_0(),
	U3CStartU3Ec__AnonStorey0_t1765731431::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (SelectPositionPopView_t2293217285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[10] = 
{
	SelectPositionPopView_t2293217285::get_offset_of_CellPrefab_2(),
	SelectPositionPopView_t2293217285::get_offset_of_TableView_3(),
	SelectPositionPopView_t2293217285::get_offset_of_CloseButton_4(),
	SelectPositionPopView_t2293217285::get_offset_of_TabButton_5(),
	SelectPositionPopView_t2293217285::get_offset_of_NoPlayerText_6(),
	SelectPositionPopView_t2293217285::get_offset_of_curTabIndex_7(),
	SelectPositionPopView_t2293217285::get_offset_of_numInstancesCreated_8(),
	SelectPositionPopView_t2293217285::get_offset_of_position_9(),
	SelectPositionPopView_t2293217285::get_offset_of_formationNo_10(),
	SelectPositionPopView_t2293217285::get_offset_of_playerListDict_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t2123783708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[2] = 
{
	U3CAwakeU3Ec__AnonStorey0_t2123783708::get_offset_of_index_0(),
	U3CAwakeU3Ec__AnonStorey0_t2123783708::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (U3CLoadPlayerU3Ec__AnonStorey1_t2607580775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[1] = 
{
	U3CLoadPlayerU3Ec__AnonStorey1_t2607580775::get_offset_of_user_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Coupon_t2313665098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[2] = 
{
	Coupon_t2313665098::get_offset_of_type_0(),
	Coupon_t2313665098::get_offset_of_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (CouponPop_t2786823949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[3] = 
{
	CouponPop_t2786823949::get_offset_of_CloseButton_2(),
	CouponPop_t2786823949::get_offset_of_OkayButton_3(),
	CouponPop_t2786823949::get_offset_of_InputField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (MyRecordView_t1262634078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[7] = 
{
	MyRecordView_t1262634078::get_offset_of_Prevbutton_2(),
	MyRecordView_t1262634078::get_offset_of_PlayTimeText_3(),
	MyRecordView_t1262634078::get_offset_of_HighScoreText_4(),
	MyRecordView_t1262634078::get_offset_of_HighOverallText_5(),
	MyRecordView_t1262634078::get_offset_of_PlayerCountText_6(),
	MyRecordView_t1262634078::get_offset_of_GaugeImage_7(),
	MyRecordView_t1262634078::get_offset_of_GaugeBgTransform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (OptionView_t81925000), -1, sizeof(OptionView_t81925000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2529[20] = 
{
	OptionView_t81925000::get_offset_of_MyRecordViewPanel_2(),
	OptionView_t81925000::get_offset_of_MyRecordButton_3(),
	OptionView_t81925000::get_offset_of_FacebookButton_4(),
	OptionView_t81925000::get_offset_of_InstagramButton_5(),
	OptionView_t81925000::get_offset_of_GuideButton_6(),
	OptionView_t81925000::get_offset_of_RateUsButton_7(),
	OptionView_t81925000::get_offset_of_CouponButton_8(),
	OptionView_t81925000::get_offset_of_SoundButton_9(),
	OptionView_t81925000::get_offset_of_LeaderboardButton_10(),
	OptionView_t81925000::get_offset_of_AchievementButton_11(),
	OptionView_t81925000::get_offset_of_CloudSaveButton_12(),
	OptionView_t81925000::get_offset_of_CloudLoadButton_13(),
	OptionView_t81925000::get_offset_of_CheatButton_14(),
	OptionView_t81925000::get_offset_of_AllPlayerButton_15(),
	OptionView_t81925000::get_offset_of_UserResetButton_16(),
	OptionView_t81925000::get_offset_of_DebugText_17(),
	OptionView_t81925000::get_offset_of_LikeBox_18(),
	OptionView_t81925000::get_offset_of_HeartBox_19(),
	OptionView_t81925000_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
	OptionView_t81925000_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (AdvertisePop_t3889815228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[2] = 
{
	AdvertisePop_t3889815228::get_offset_of_CloseButton_2(),
	AdvertisePop_t3889815228::get_offset_of_linkURL_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (CostType_t3548590213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	CostType_t3548590213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (AlertView_t4232360573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[8] = 
{
	AlertView_t4232360573::get_offset_of_TitleText_2(),
	AlertView_t4232360573::get_offset_of_MessageText_3(),
	AlertView_t4232360573::get_offset_of_YesText_4(),
	AlertView_t4232360573::get_offset_of_YesButton_5(),
	AlertView_t4232360573::get_offset_of_CloseButton_6(),
	AlertView_t4232360573::get_offset_of_GemIcon_7(),
	AlertView_t4232360573::get_offset_of_CoinIcon_8(),
	AlertView_t4232360573::get_offset_of_Callback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (BuyGemPop_t3502837550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[3] = 
{
	BuyGemPop_t3502837550::get_offset_of_YesButton_2(),
	BuyGemPop_t3502837550::get_offset_of_CloseButton_3(),
	BuyGemPop_t3502837550::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (CheckPlayerView_t3953299720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[1] = 
{
	CheckPlayerView_t3953299720::get_offset_of_PlayerBoxObj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (GuidePop1_t959359454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[9] = 
{
	GuidePop1_t959359454::get_offset_of_NextButton_2(),
	GuidePop1_t959359454::get_offset_of_Unit1_3(),
	GuidePop1_t959359454::get_offset_of_Unit2_4(),
	GuidePop1_t959359454::get_offset_of_Unit3_5(),
	GuidePop1_t959359454::get_offset_of_Unit4_6(),
	GuidePop1_t959359454::get_offset_of_ArrowLeft_7(),
	GuidePop1_t959359454::get_offset_of_HandLeft_8(),
	GuidePop1_t959359454::get_offset_of_ArrowRight_9(),
	GuidePop1_t959359454::get_offset_of_HandRight_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (U3CStartAnimationU3Ec__AnonStorey0_t3548337856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[5] = 
{
	U3CStartAnimationU3Ec__AnonStorey0_t3548337856::get_offset_of_originScale_0(),
	U3CStartAnimationU3Ec__AnonStorey0_t3548337856::get_offset_of_originPos2_1(),
	U3CStartAnimationU3Ec__AnonStorey0_t3548337856::get_offset_of_originPos3_2(),
	U3CStartAnimationU3Ec__AnonStorey0_t3548337856::get_offset_of_originPos1_3(),
	U3CStartAnimationU3Ec__AnonStorey0_t3548337856::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (GuidePop2_t959359457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[3] = 
{
	GuidePop2_t959359457::get_offset_of_CloseButton_2(),
	GuidePop2_t959359457::get_offset_of_StarIcon_3(),
	GuidePop2_t959359457::get_offset_of_TouchImage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (PlayerInfoPopView_t4205922565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[20] = 
{
	PlayerInfoPopView_t4205922565::get_offset_of_CurPlayer_2(),
	PlayerInfoPopView_t4205922565::get_offset_of_PlayerUserData_3(),
	PlayerInfoPopView_t4205922565::get_offset_of_NewPlayerText_4(),
	PlayerInfoPopView_t4205922565::get_offset_of_NationText_5(),
	PlayerInfoPopView_t4205922565::get_offset_of_NameText_6(),
	PlayerInfoPopView_t4205922565::get_offset_of_GradeText_7(),
	PlayerInfoPopView_t4205922565::get_offset_of_BackNumberText_8(),
	PlayerInfoPopView_t4205922565::get_offset_of_PositionText_9(),
	PlayerInfoPopView_t4205922565::get_offset_of_LevelText_10(),
	PlayerInfoPopView_t4205922565::get_offset_of_OverallText_11(),
	PlayerInfoPopView_t4205922565::get_offset_of_CardCountText_12(),
	PlayerInfoPopView_t4205922565::get_offset_of_CostText_13(),
	PlayerInfoPopView_t4205922565::get_offset_of_PlayerBoxObj_14(),
	PlayerInfoPopView_t4205922565::get_offset_of_GaugeBg_15(),
	PlayerInfoPopView_t4205922565::get_offset_of_Gauge_16(),
	PlayerInfoPopView_t4205922565::get_offset_of_StarImage_17(),
	PlayerInfoPopView_t4205922565::get_offset_of_FlagImage_18(),
	PlayerInfoPopView_t4205922565::get_offset_of_LevelUpButton_19(),
	PlayerInfoPopView_t4205922565::get_offset_of_BackButton_20(),
	PlayerInfoPopView_t4205922565::get_offset_of__addCardCount_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (U3CCoAddCardCountU3Ec__Iterator0_t1807627623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	U3CCoAddCardCountU3Ec__Iterator0_t1807627623::get_offset_of_U24this_0(),
	U3CCoAddCardCountU3Ec__Iterator0_t1807627623::get_offset_of_U24current_1(),
	U3CCoAddCardCountU3Ec__Iterator0_t1807627623::get_offset_of_U24disposing_2(),
	U3CCoAddCardCountU3Ec__Iterator0_t1807627623::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (AdMobController_t3743396963), -1, sizeof(AdMobController_t3743396963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2540[7] = 
{
	AdMobController_t3743396963::get_offset_of_bannerView_2(),
	AdMobController_t3743396963::get_offset_of_interstitial_3(),
	AdMobController_t3743396963::get_offset_of_rewardVideo_4(),
	AdMobController_t3743396963::get_offset_of_currentVideoType_5(),
	AdMobController_t3743396963_StaticFields::get_offset_of_IOS_BANNER_ID_6(),
	AdMobController_t3743396963_StaticFields::get_offset_of_IOS_INTERSTITIAL_ID_7(),
	AdMobController_t3743396963_StaticFields::get_offset_of_IOS_REWARD_VIDEO_ID_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (DBManager_t2841981931), -1, sizeof(DBManager_t2841981931_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2541[4] = 
{
	DBManager_t2841981931::get_offset_of_DB_NAME_2(),
	DBManager_t2841981931::get_offset_of_DB_SECRET_KEY_3(),
	DBManager_t2841981931_StaticFields::get_offset_of__instance_4(),
	DBManager_t2841981931::get_offset_of__db_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (FormulaManager_t3329446299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (GameManager_t2252321495), -1, sizeof(GameManager_t2252321495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2543[8] = 
{
	GameManager_t2252321495::get_offset_of_SAVE_KEY_2(),
	GameManager_t2252321495_StaticFields::get_offset_of__instance_3(),
	GameManager_t2252321495::get_offset_of_IS_DEV_4(),
	GameManager_t2252321495::get_offset_of_NetworkTime_5(),
	GameManager_t2252321495::get_offset_of__adMobController_6(),
	GameManager_t2252321495::get_offset_of__gameData_7(),
	GameManager_t2252321495::get_offset_of__user_8(),
	GameManager_t2252321495_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (U3CCoPlayTimerU3Ec__Iterator0_t895334160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[4] = 
{
	U3CCoPlayTimerU3Ec__Iterator0_t895334160::get_offset_of_U24this_0(),
	U3CCoPlayTimerU3Ec__Iterator0_t895334160::get_offset_of_U24current_1(),
	U3CCoPlayTimerU3Ec__Iterator0_t895334160::get_offset_of_U24disposing_2(),
	U3CCoPlayTimerU3Ec__Iterator0_t895334160::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (WebData_t1635253674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[2] = 
{
	WebData_t1635253674::get_offset_of_rs_0(),
	WebData_t1635253674::get_offset_of_msg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (IAPManager_t2850027101), -1, sizeof(IAPManager_t2850027101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2546[7] = 
{
	IAPManager_t2850027101_StaticFields::get_offset_of_storeController_2(),
	IAPManager_t2850027101_StaticFields::get_offset_of_extensionProvider_3(),
	0,
	0,
	0,
	IAPManager_t2850027101::get_offset_of_currentProductId_7(),
	IAPManager_t2850027101_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (U3CWaitForRequestU3Ec__Iterator0_t1300943864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[5] = 
{
	U3CWaitForRequestU3Ec__Iterator0_t1300943864::get_offset_of_www_0(),
	U3CWaitForRequestU3Ec__Iterator0_t1300943864::get_offset_of_U24this_1(),
	U3CWaitForRequestU3Ec__Iterator0_t1300943864::get_offset_of_U24current_2(),
	U3CWaitForRequestU3Ec__Iterator0_t1300943864::get_offset_of_U24disposing_3(),
	U3CWaitForRequestU3Ec__Iterator0_t1300943864::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (ResourceManager_t4136494783), -1, sizeof(ResourceManager_t4136494783_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2548[8] = 
{
	ResourceManager_t4136494783_StaticFields::get_offset_of__instance_2(),
	ResourceManager_t4136494783_StaticFields::get_offset_of_resourceCache_3(),
	ResourceManager_t4136494783_StaticFields::get_offset_of_vertextLit_4(),
	ResourceManager_t4136494783::get_offset_of_CoinIconPool_5(),
	ResourceManager_t4136494783::get_offset_of_GemIconPool_6(),
	ResourceManager_t4136494783_StaticFields::get_offset_of_ORANGE_COLOR_7(),
	ResourceManager_t4136494783_StaticFields::get_offset_of_BEIGE_COLOR_8(),
	ResourceManager_t4136494783::get_offset_of_GrayScaleMat_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (SoundManager_t654432262), -1, sizeof(SoundManager_t654432262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[6] = 
{
	SoundManager_t654432262_StaticFields::get_offset_of__instance_2(),
	SoundManager_t654432262::get_offset_of_audioBGM_3(),
	SoundManager_t654432262::get_offset_of_audioFx_4(),
	SoundManager_t654432262::get_offset_of_bgm_5(),
	SoundManager_t654432262::get_offset_of_isBGMEnable_6(),
	SoundManager_t654432262::get_offset_of_isEffectEnable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (UIManager_t2519183485), -1, sizeof(UIManager_t2519183485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2550[12] = 
{
	UIManager_t2519183485_StaticFields::get_offset_of__instance_2(),
	UIManager_t2519183485::get_offset_of_UICamera_3(),
	UIManager_t2519183485::get_offset_of_BaseCanvas_4(),
	UIManager_t2519183485::get_offset_of_ScoutView_5(),
	UIManager_t2519183485::get_offset_of_CollectView_6(),
	UIManager_t2519183485::get_offset_of_TrainingView_7(),
	UIManager_t2519183485::get_offset_of_MySquadView_8(),
	UIManager_t2519183485::get_offset_of_SettingView_9(),
	UIManager_t2519183485::get_offset_of_UserCashPanel_10(),
	UIManager_t2519183485::get_offset_of_PopUp_11(),
	UIManager_t2519183485::get_offset_of_DepthView_12(),
	UIManager_t2519183485_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (Nation_t3431670537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[4] = 
{
	Nation_t3431670537::get_offset_of_No_0(),
	Nation_t3431670537::get_offset_of_Name_1(),
	Nation_t3431670537::get_offset_of_Form_2(),
	Nation_t3431670537::get_offset_of_IsOpen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (GameData_t450274222), -1, sizeof(GameData_t450274222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2552[11] = 
{
	GameData_t450274222_StaticFields::get_offset_of_UNIT_MAX_LEVEL_0(),
	GameData_t450274222_StaticFields::get_offset_of_LEVELUP_CARD_COUNT_1(),
	GameData_t450274222_StaticFields::get_offset_of_LEVELUP_COST_2(),
	GameData_t450274222_StaticFields::get_offset_of_SEARCH_FREE_TIME_3(),
	GameData_t450274222_StaticFields::get_offset_of_DIRECT_REFRESH_TIME_4(),
	GameData_t450274222_StaticFields::get_offset_of_DOUBLE_CHANCE_TIME_5(),
	GameData_t450274222_StaticFields::get_offset_of_FREE_GEM_TIME_6(),
	GameData_t450274222_StaticFields::get_offset_of_SEARCH_PLAYER_COIN_7(),
	GameData_t450274222_StaticFields::get_offset_of_SEARCH_PLAYER_GEM_8(),
	GameData_t450274222_StaticFields::get_offset_of_SUFFLE_GEM_9(),
	GameData_t450274222_StaticFields::get_offset_of_FORMATION_STRING_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (Person_t3241917763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (Player_t1147783557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[10] = 
{
	Player_t1147783557::get_offset_of_No_0(),
	Player_t1147783557::get_offset_of_ModelNo_1(),
	Player_t1147783557::get_offset_of_NationNo_2(),
	Player_t1147783557::get_offset_of_Name_3(),
	Player_t1147783557::get_offset_of_ShortName_4(),
	Player_t1147783557::get_offset_of_Skin_5(),
	Player_t1147783557::get_offset_of_BackNumber_6(),
	Player_t1147783557::get_offset_of_Position_7(),
	Player_t1147783557::get_offset_of_Grade_8(),
	Player_t1147783557::get_offset_of_Overall_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (PlayerUserData_t4090529802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[5] = 
{
	PlayerUserData_t4090529802::get_offset_of_No_0(),
	PlayerUserData_t4090529802::get_offset_of_NationNo_1(),
	PlayerUserData_t4090529802::get_offset_of_Lv_2(),
	PlayerUserData_t4090529802::get_offset_of_CardCount_3(),
	PlayerUserData_t4090529802::get_offset_of_IsNew_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (SquadUserData_t3007047065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[3] = 
{
	SquadUserData_t3007047065::get_offset_of_SlotNo_0(),
	SquadUserData_t3007047065::get_offset_of_Form_1(),
	SquadUserData_t3007047065::get_offset_of_PlayerFormDict_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (TraineeUnit_t2929148814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[6] = 
{
	TraineeUnit_t2929148814::get_offset_of_Index_0(),
	TraineeUnit_t2929148814::get_offset_of_Level_1(),
	TraineeUnit_t2929148814::get_offset_of_Value_2(),
	TraineeUnit_t2929148814::get_offset_of_HaveCoin_3(),
	TraineeUnit_t2929148814::get_offset_of_IsStar_4(),
	TraineeUnit_t2929148814::get_offset_of_IsUpgraded_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (User_t719925459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[20] = 
{
	User_t719925459::get_offset_of_coin_0(),
	User_t719925459::get_offset_of_gem_1(),
	User_t719925459::get_offset_of_highScore_2(),
	User_t719925459::get_offset_of_totalOverall_3(),
	User_t719925459::get_offset_of_gameScore_4(),
	User_t719925459::get_offset_of_gameGem_5(),
	User_t719925459::get_offset_of_UnlockUnitLevel_6(),
	User_t719925459::get_offset_of_PlayTime_7(),
	User_t719925459::get_offset_of_FreeScoutTime_8(),
	User_t719925459::get_offset_of_DirectBuyRefreshTime_9(),
	User_t719925459::get_offset_of_DoubleChanceTime_10(),
	User_t719925459::get_offset_of_FreeGemTime_11(),
	User_t719925459::get_offset_of_DirectPlayerDict_12(),
	User_t719925459::get_offset_of__unitDataDict_13(),
	User_t719925459::get_offset_of__playerDataDict_14(),
	User_t719925459::get_offset_of__squadDataDict_15(),
	User_t719925459::get_offset_of_IsGuideFin_16(),
	User_t719925459::get_offset_of_CouponDict_17(),
	User_t719925459::get_offset_of_FacebookLiked_18(),
	User_t719925459::get_offset_of_InstagramFollowed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (NewGameView_t3231952493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[7] = 
{
	NewGameView_t3231952493::get_offset_of_genderText_2(),
	NewGameView_t3231952493::get_offset_of_MaleButton_3(),
	NewGameView_t3231952493::get_offset_of_FemaleButton_4(),
	NewGameView_t3231952493::get_offset_of_AgeSlider_5(),
	NewGameView_t3231952493::get_offset_of_AgeText_6(),
	NewGameView_t3231952493::get_offset_of_StartButton_7(),
	NewGameView_t3231952493::get_offset_of__gender_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (UIPopAction_t3469027355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[2] = 
{
	UIPopAction_t3469027355::get_offset_of_PopupFrame_2(),
	UIPopAction_t3469027355::get_offset_of_originPos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (FormEmptyUIView_t2064892716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[4] = 
{
	FormEmptyUIView_t2064892716::get_offset_of_index_2(),
	FormEmptyUIView_t2064892716::get_offset_of_positionName_3(),
	FormEmptyUIView_t2064892716::get_offset_of_BaseButton_4(),
	FormEmptyUIView_t2064892716::get_offset_of_PositionText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (FormHelper_t3562480506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[2] = 
{
	FormHelper_t3562480506::get_offset_of_PositionList_2(),
	FormHelper_t3562480506::get_offset_of_posNameList_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (FormPlayer_t4046102703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[3] = 
{
	FormPlayer_t4046102703::get_offset_of_PlayerData_2(),
	FormPlayer_t4046102703::get_offset_of_PlayerUserData_3(),
	FormPlayer_t4046102703::get_offset_of__head_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (FormUIType_t1820353380)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2564[3] = 
{
	FormUIType_t1820353380::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (FormPlayerUIView_t1488813792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[23] = 
{
	FormPlayerUIView_t1488813792::get_offset_of_index_2(),
	FormPlayerUIView_t1488813792::get_offset_of_positionName_3(),
	FormPlayerUIView_t1488813792::get_offset_of_formUIType_4(),
	FormPlayerUIView_t1488813792::get_offset_of_infoType_5(),
	FormPlayerUIView_t1488813792::get_offset_of_PlayerData_6(),
	FormPlayerUIView_t1488813792::get_offset_of_PlayerUserData_7(),
	FormPlayerUIView_t1488813792::get_offset_of_NameText_8(),
	FormPlayerUIView_t1488813792::get_offset_of_Button_9(),
	FormPlayerUIView_t1488813792::get_offset_of_InfoButton_10(),
	FormPlayerUIView_t1488813792::get_offset_of_ChangeButton_11(),
	FormPlayerUIView_t1488813792::get_offset_of_RemoveButton_12(),
	FormPlayerUIView_t1488813792::get_offset_of_LevelUpIcon_13(),
	FormPlayerUIView_t1488813792::get_offset_of_StarIcon_14(),
	FormPlayerUIView_t1488813792::get_offset_of_FlagImage_15(),
	FormPlayerUIView_t1488813792::get_offset_of_OverallBox_16(),
	FormPlayerUIView_t1488813792::get_offset_of_OverallText_17(),
	FormPlayerUIView_t1488813792::get_offset_of_PositionText_18(),
	FormPlayerUIView_t1488813792::get_offset_of_GradeText_19(),
	FormPlayerUIView_t1488813792::get_offset_of_LevelText_20(),
	FormPlayerUIView_t1488813792::get_offset_of_ExpendPanel_21(),
	FormPlayerUIView_t1488813792::get_offset_of_Badge_22(),
	FormPlayerUIView_t1488813792::get_offset_of_OnExpendOff_23(),
	FormPlayerUIView_t1488813792::get_offset_of_OnExpendOn_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (DelegateOnChanged_t798682617), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (UnitDataHolder_t2129886096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	UnitDataHolder_t2129886096::get_offset_of_UnitData_2(),
	UnitDataHolder_t2129886096::get_offset_of_UnitUIView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (UnitUIView_t3306525905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[8] = 
{
	UnitUIView_t3306525905::get_offset_of_TargetTransform_2(),
	UnitUIView_t3306525905::get_offset_of_ValueText_3(),
	UnitUIView_t3306525905::get_offset_of_CoinImage_4(),
	UnitUIView_t3306525905::get_offset_of_StarImage_5(),
	UnitUIView_t3306525905::get_offset_of_Button_6(),
	UnitUIView_t3306525905::get_offset_of_originScaleX_7(),
	UnitUIView_t3306525905::get_offset_of_originY_8(),
	UnitUIView_t3306525905::get_offset_of_gradeColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (Debug_t154383703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (Serializer_t3184141204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (Util_t4006552276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (EZObjectPool_t3968219684), -1, sizeof(EZObjectPool_t3968219684_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[10] = 
{
	EZObjectPool_t3968219684_StaticFields::get_offset_of_SharedPools_2(),
	EZObjectPool_t3968219684_StaticFields::get_offset_of_Marker_3(),
	EZObjectPool_t3968219684::get_offset_of_Template_4(),
	EZObjectPool_t3968219684::get_offset_of_PoolName_5(),
	EZObjectPool_t3968219684::get_offset_of_ObjectList_6(),
	EZObjectPool_t3968219684::get_offset_of_AutoResize_7(),
	EZObjectPool_t3968219684::get_offset_of_PoolSize_8(),
	EZObjectPool_t3968219684::get_offset_of_InstantiateOnAwake_9(),
	EZObjectPool_t3968219684::get_offset_of_Shared_10(),
	EZObjectPool_t3968219684::get_offset_of_AvailableObjects_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (ParticleSystemDisable_t496244635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[1] = 
{
	ParticleSystemDisable_t496244635::get_offset_of_Particles_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (PooledObject_t2079175126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[1] = 
{
	PooledObject_t2079175126::get_offset_of_ParentPool_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (TimedDisable_t2667936835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[2] = 
{
	TimedDisable_t2667936835::get_offset_of_timer_3(),
	TimedDisable_t2667936835::get_offset_of_DisableTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (API_t2390511294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Reader_t1391219339), -1, sizeof(Reader_t1391219339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2577[2] = 
{
	Reader_t1391219339_StaticFields::get_offset_of_sheets_0(),
	Reader_t1391219339_StaticFields::get_offset_of_DocsCacheData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (Settings_t1795059213), -1, sizeof(Settings_t1795059213_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2578[18] = 
{
	0,
	0,
	0,
	Settings_t1795059213_StaticFields::get_offset_of_SERVICE_NAME_5(),
	Settings_t1795059213_StaticFields::get_offset_of_CLIENT_ID_6(),
	Settings_t1795059213_StaticFields::get_offset_of_CLIENT_SECRET_7(),
	Settings_t1795059213_StaticFields::get_offset_of_SCOPE_8(),
	Settings_t1795059213_StaticFields::get_offset_of_REDIRECT_URI_9(),
	Settings_t1795059213_StaticFields::get_offset_of_GOOGLEDOC_URL_START_10(),
	Settings_t1795059213_StaticFields::get_offset_of_GOOGLEDOC_URL_END_11(),
	Settings_t1795059213_StaticFields::get_offset_of_SPREADSHEET_URL_START_12(),
	Settings_t1795059213_StaticFields::get_offset_of_SPREADSHEET_URL_END_13(),
	Settings_t1795059213_StaticFields::get_offset_of_SCRIPT_URL_START_14(),
	Settings_t1795059213_StaticFields::get_offset_of_SCRIPT_URL_PARAM_15(),
	Settings_t1795059213_StaticFields::get_offset_of_PATH_ADDING_16(),
	Settings_t1795059213::get_offset_of_CachePath_17(),
	Settings_t1795059213::get_offset_of_docs_18(),
	Settings_t1795059213_StaticFields::get_offset_of_instance_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (SpreadsheetData_t567722816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[1] = 
{
	SpreadsheetData_t567722816::get_offset_of__worksheets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (DataType_t848016694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2580[5] = 
{
	DataType_t848016694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (RanageDirection_t1288290907)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2581[3] = 
{
	RanageDirection_t1288290907::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (VariableType_t3941595272)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[5] = 
{
	VariableType_t3941595272::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Cell_t3987632660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[3] = 
{
	Cell_t3987632660::get_offset_of__col_0(),
	Cell_t3987632660::get_offset_of__row_1(),
	Cell_t3987632660::get_offset_of__key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (CellDictionaryRange_t1484450799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[3] = 
{
	CellDictionaryRange_t1484450799::get_offset_of_cellRange_0(),
	CellDictionaryRange_t1484450799::get_offset_of_rowShift_1(),
	CellDictionaryRange_t1484450799::get_offset_of_columnShift_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (CellRange_t4079182527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[4] = 
{
	CellRange_t4079182527::get_offset_of_direction_0(),
	CellRange_t4079182527::get_offset_of_StartCell_1(),
	CellRange_t4079182527::get_offset_of_useLinebreak_2(),
	CellRange_t4079182527::get_offset_of_LineLength_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (DataComponentConnection_t366829713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[8] = 
{
	DataComponentConnection_t366829713::get_offset_of_IsOpened_0(),
	DataComponentConnection_t366829713::get_offset_of_IsMarkForDel_1(),
	DataComponentConnection_t366829713::get_offset_of_component_2(),
	DataComponentConnection_t366829713::get_offset_of_owner_3(),
	DataComponentConnection_t366829713::get_offset_of_componentName_4(),
	DataComponentConnection_t366829713::get_offset_of_delegates_5(),
	DataComponentConnection_t366829713::get_offset_of_fieldsNames_6(),
	DataComponentConnection_t366829713::get_offset_of_fieldConnectable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (DataDelegate_t1852602171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[11] = 
{
	DataDelegate_t1852602171::get_offset_of_FiledName_0(),
	DataDelegate_t1852602171::get_offset_of_IsOpened_1(),
	DataDelegate_t1852602171::get_offset_of_cell_2(),
	DataDelegate_t1852602171::get_offset_of_range_3(),
	DataDelegate_t1852602171::get_offset_of_hashMap_4(),
	DataDelegate_t1852602171::get_offset_of_dataType_5(),
	DataDelegate_t1852602171::get_offset_of_variableType_6(),
	DataDelegate_t1852602171::get_offset_of_bConnectableField_7(),
	DataDelegate_t1852602171::get_offset_of_SourceDocName_8(),
	DataDelegate_t1852602171::get_offset_of_owner_9(),
	DataDelegate_t1852602171::get_offset_of__googleData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (DocTemplate_t209748574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[9] = 
{
	DocTemplate_t209748574::get_offset_of_key_0(),
	DocTemplate_t209748574::get_offset_of__worksheets_1(),
	DocTemplate_t209748574::get_offset_of__creationTime_2(),
	DocTemplate_t209748574::get_offset_of_name_3(),
	DocTemplate_t209748574::get_offset_of_IsTableListOpen_4(),
	DocTemplate_t209748574::get_offset_of_IsOpen_5(),
	DocTemplate_t209748574::get_offset_of_docName_6(),
	DocTemplate_t209748574::get_offset_of_docIndex_7(),
	DocTemplate_t209748574::get_offset_of_selectedWorksheet_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (GoogleData_t1520579079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[1] = 
{
	GoogleData_t1520579079::get_offset_of_connections_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (WorksheetTemplate_t3784200896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[2] = 
{
	WorksheetTemplate_t3784200896::get_offset_of_listName_0(),
	WorksheetTemplate_t3784200896::get_offset_of_listId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (Json_t1582238276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (Parser_t3557338248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	0,
	0,
	Parser_t3557338248::get_offset_of_json_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (TOKEN_t2343128322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2593[13] = 
{
	TOKEN_t2343128322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (Serializer_t3424133211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	Serializer_t3424133211::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (OnDocLoaded_t4025950952), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (WWWDocLoader_t336628936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[1] = 
{
	WWWDocLoader_t336628936::get_offset_of_LoadEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (U3CWaitForRequestU3Ec__Iterator0_t3800969493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[5] = 
{
	U3CWaitForRequestU3Ec__Iterator0_t3800969493::get_offset_of_www_0(),
	U3CWaitForRequestU3Ec__Iterator0_t3800969493::get_offset_of_U24this_1(),
	U3CWaitForRequestU3Ec__Iterator0_t3800969493::get_offset_of_U24current_2(),
	U3CWaitForRequestU3Ec__Iterator0_t3800969493::get_offset_of_U24disposing_3(),
	U3CWaitForRequestU3Ec__Iterator0_t3800969493::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (DocDataRertivalExamle_t2328889893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (ExampleSpawner1_t272242923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[4] = 
{
	ExampleSpawner1_t272242923::get_offset_of_SpawnRange_2(),
	ExampleSpawner1_t272242923::get_offset_of_SpawnDelay_3(),
	ExampleSpawner1_t272242923::get_offset_of_spawnPattern_4(),
	ExampleSpawner1_t272242923::get_offset_of_IsStarted_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
