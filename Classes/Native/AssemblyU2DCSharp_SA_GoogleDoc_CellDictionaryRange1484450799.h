﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SA.GoogleDoc.CellRange
struct CellRange_t4079182527;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.CellDictionaryRange
struct  CellDictionaryRange_t1484450799  : public Il2CppObject
{
public:
	// SA.GoogleDoc.CellRange SA.GoogleDoc.CellDictionaryRange::cellRange
	CellRange_t4079182527 * ___cellRange_0;
	// System.Int32 SA.GoogleDoc.CellDictionaryRange::rowShift
	int32_t ___rowShift_1;
	// System.Int32 SA.GoogleDoc.CellDictionaryRange::columnShift
	int32_t ___columnShift_2;

public:
	inline static int32_t get_offset_of_cellRange_0() { return static_cast<int32_t>(offsetof(CellDictionaryRange_t1484450799, ___cellRange_0)); }
	inline CellRange_t4079182527 * get_cellRange_0() const { return ___cellRange_0; }
	inline CellRange_t4079182527 ** get_address_of_cellRange_0() { return &___cellRange_0; }
	inline void set_cellRange_0(CellRange_t4079182527 * value)
	{
		___cellRange_0 = value;
		Il2CppCodeGenWriteBarrier(&___cellRange_0, value);
	}

	inline static int32_t get_offset_of_rowShift_1() { return static_cast<int32_t>(offsetof(CellDictionaryRange_t1484450799, ___rowShift_1)); }
	inline int32_t get_rowShift_1() const { return ___rowShift_1; }
	inline int32_t* get_address_of_rowShift_1() { return &___rowShift_1; }
	inline void set_rowShift_1(int32_t value)
	{
		___rowShift_1 = value;
	}

	inline static int32_t get_offset_of_columnShift_2() { return static_cast<int32_t>(offsetof(CellDictionaryRange_t1484450799, ___columnShift_2)); }
	inline int32_t get_columnShift_2() const { return ___columnShift_2; }
	inline int32_t* get_address_of_columnShift_2() { return &___columnShift_2; }
	inline void set_columnShift_2(int32_t value)
	{
		___columnShift_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
