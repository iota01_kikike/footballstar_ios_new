﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData
struct FPSCounterData_t432421409;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0
struct  U3CUpdateCounterU3Ec__Iterator0_t2432920949  : public Il2CppObject
{
public:
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::<previousUpdateTime>__1
	float ___U3CpreviousUpdateTimeU3E__1_0;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::<previousUpdateFrames>__1
	int32_t ___U3CpreviousUpdateFramesU3E__1_1;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::<timeElapsed>__1
	float ___U3CtimeElapsedU3E__1_2;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::<framesChanged>__1
	int32_t ___U3CframesChangedU3E__1_3;
	// CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::$this
	FPSCounterData_t432421409 * ___U24this_4;
	// System.Object CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData/<UpdateCounter>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CpreviousUpdateTimeU3E__1_0() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U3CpreviousUpdateTimeU3E__1_0)); }
	inline float get_U3CpreviousUpdateTimeU3E__1_0() const { return ___U3CpreviousUpdateTimeU3E__1_0; }
	inline float* get_address_of_U3CpreviousUpdateTimeU3E__1_0() { return &___U3CpreviousUpdateTimeU3E__1_0; }
	inline void set_U3CpreviousUpdateTimeU3E__1_0(float value)
	{
		___U3CpreviousUpdateTimeU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CpreviousUpdateFramesU3E__1_1() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U3CpreviousUpdateFramesU3E__1_1)); }
	inline int32_t get_U3CpreviousUpdateFramesU3E__1_1() const { return ___U3CpreviousUpdateFramesU3E__1_1; }
	inline int32_t* get_address_of_U3CpreviousUpdateFramesU3E__1_1() { return &___U3CpreviousUpdateFramesU3E__1_1; }
	inline void set_U3CpreviousUpdateFramesU3E__1_1(int32_t value)
	{
		___U3CpreviousUpdateFramesU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtimeElapsedU3E__1_2() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U3CtimeElapsedU3E__1_2)); }
	inline float get_U3CtimeElapsedU3E__1_2() const { return ___U3CtimeElapsedU3E__1_2; }
	inline float* get_address_of_U3CtimeElapsedU3E__1_2() { return &___U3CtimeElapsedU3E__1_2; }
	inline void set_U3CtimeElapsedU3E__1_2(float value)
	{
		___U3CtimeElapsedU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CframesChangedU3E__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U3CframesChangedU3E__1_3)); }
	inline int32_t get_U3CframesChangedU3E__1_3() const { return ___U3CframesChangedU3E__1_3; }
	inline int32_t* get_address_of_U3CframesChangedU3E__1_3() { return &___U3CframesChangedU3E__1_3; }
	inline void set_U3CframesChangedU3E__1_3(int32_t value)
	{
		___U3CframesChangedU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U24this_4)); }
	inline FPSCounterData_t432421409 * get_U24this_4() const { return ___U24this_4; }
	inline FPSCounterData_t432421409 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(FPSCounterData_t432421409 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CUpdateCounterU3Ec__Iterator0_t2432920949, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
