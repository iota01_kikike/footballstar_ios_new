﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t364059421;
// UnityEngine.Purchasing.EventQueue
struct EventQueue_t560163637;
// UnityEngine.Purchasing.ProfileData
struct ProfileData_t3353328249;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventQueue
struct  EventQueue_t560163637  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.EventQueue::m_AsyncUtil
	Il2CppObject * ___m_AsyncUtil_0;
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.EventQueue::Profile
	ProfileData_t3353328249 * ___Profile_2;
	// System.String UnityEngine.Purchasing.EventQueue::TrackingUrl
	String_t* ___TrackingUrl_3;
	// System.String UnityEngine.Purchasing.EventQueue::EventUrl
	String_t* ___EventUrl_4;
	// System.Object UnityEngine.Purchasing.EventQueue::ProfileDict
	Il2CppObject * ___ProfileDict_5;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(EventQueue_t560163637, ___m_AsyncUtil_0)); }
	inline Il2CppObject * get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline Il2CppObject ** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(Il2CppObject * value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_AsyncUtil_0, value);
	}

	inline static int32_t get_offset_of_Profile_2() { return static_cast<int32_t>(offsetof(EventQueue_t560163637, ___Profile_2)); }
	inline ProfileData_t3353328249 * get_Profile_2() const { return ___Profile_2; }
	inline ProfileData_t3353328249 ** get_address_of_Profile_2() { return &___Profile_2; }
	inline void set_Profile_2(ProfileData_t3353328249 * value)
	{
		___Profile_2 = value;
		Il2CppCodeGenWriteBarrier(&___Profile_2, value);
	}

	inline static int32_t get_offset_of_TrackingUrl_3() { return static_cast<int32_t>(offsetof(EventQueue_t560163637, ___TrackingUrl_3)); }
	inline String_t* get_TrackingUrl_3() const { return ___TrackingUrl_3; }
	inline String_t** get_address_of_TrackingUrl_3() { return &___TrackingUrl_3; }
	inline void set_TrackingUrl_3(String_t* value)
	{
		___TrackingUrl_3 = value;
		Il2CppCodeGenWriteBarrier(&___TrackingUrl_3, value);
	}

	inline static int32_t get_offset_of_EventUrl_4() { return static_cast<int32_t>(offsetof(EventQueue_t560163637, ___EventUrl_4)); }
	inline String_t* get_EventUrl_4() const { return ___EventUrl_4; }
	inline String_t** get_address_of_EventUrl_4() { return &___EventUrl_4; }
	inline void set_EventUrl_4(String_t* value)
	{
		___EventUrl_4 = value;
		Il2CppCodeGenWriteBarrier(&___EventUrl_4, value);
	}

	inline static int32_t get_offset_of_ProfileDict_5() { return static_cast<int32_t>(offsetof(EventQueue_t560163637, ___ProfileDict_5)); }
	inline Il2CppObject * get_ProfileDict_5() const { return ___ProfileDict_5; }
	inline Il2CppObject ** get_address_of_ProfileDict_5() { return &___ProfileDict_5; }
	inline void set_ProfileDict_5(Il2CppObject * value)
	{
		___ProfileDict_5 = value;
		Il2CppCodeGenWriteBarrier(&___ProfileDict_5, value);
	}
};

struct EventQueue_t560163637_StaticFields
{
public:
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.EventQueue::QueueInstance
	EventQueue_t560163637 * ___QueueInstance_1;

public:
	inline static int32_t get_offset_of_QueueInstance_1() { return static_cast<int32_t>(offsetof(EventQueue_t560163637_StaticFields, ___QueueInstance_1)); }
	inline EventQueue_t560163637 * get_QueueInstance_1() const { return ___QueueInstance_1; }
	inline EventQueue_t560163637 ** get_address_of_QueueInstance_1() { return &___QueueInstance_1; }
	inline void set_QueueInstance_1(EventQueue_t560163637 * value)
	{
		___QueueInstance_1 = value;
		Il2CppCodeGenWriteBarrier(&___QueueInstance_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
