﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Random
struct Random_t1044426839;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/<ShuffleBlock>c__AnonStorey3
struct  U3CShuffleBlockU3Ec__AnonStorey3_t1755741014  : public Il2CppObject
{
public:
	// System.Random TrainingView/<ShuffleBlock>c__AnonStorey3::r
	Random_t1044426839 * ___r_0;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(U3CShuffleBlockU3Ec__AnonStorey3_t1755741014, ___r_0)); }
	inline Random_t1044426839 * get_r_0() const { return ___r_0; }
	inline Random_t1044426839 ** get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(Random_t1044426839 * value)
	{
		___r_0 = value;
		Il2CppCodeGenWriteBarrier(&___r_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
