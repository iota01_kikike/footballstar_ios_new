﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LangSection1360763029.h"

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.LabelLocalizer
struct  LabelLocalizer_t2982418349  : public MonoBehaviour_t1158329972
{
public:
	// System.String SA.GoogleDoc.LabelLocalizer::Token
	String_t* ___Token_2;
	// SA.GoogleDoc.LangSection SA.GoogleDoc.LabelLocalizer::Section
	int32_t ___Section_3;
	// UnityEngine.UI.Text SA.GoogleDoc.LabelLocalizer::LocalizableLabel
	Text_t356221433 * ___LocalizableLabel_4;

public:
	inline static int32_t get_offset_of_Token_2() { return static_cast<int32_t>(offsetof(LabelLocalizer_t2982418349, ___Token_2)); }
	inline String_t* get_Token_2() const { return ___Token_2; }
	inline String_t** get_address_of_Token_2() { return &___Token_2; }
	inline void set_Token_2(String_t* value)
	{
		___Token_2 = value;
		Il2CppCodeGenWriteBarrier(&___Token_2, value);
	}

	inline static int32_t get_offset_of_Section_3() { return static_cast<int32_t>(offsetof(LabelLocalizer_t2982418349, ___Section_3)); }
	inline int32_t get_Section_3() const { return ___Section_3; }
	inline int32_t* get_address_of_Section_3() { return &___Section_3; }
	inline void set_Section_3(int32_t value)
	{
		___Section_3 = value;
	}

	inline static int32_t get_offset_of_LocalizableLabel_4() { return static_cast<int32_t>(offsetof(LabelLocalizer_t2982418349, ___LocalizableLabel_4)); }
	inline Text_t356221433 * get_LocalizableLabel_4() const { return ___LocalizableLabel_4; }
	inline Text_t356221433 ** get_address_of_LocalizableLabel_4() { return &___LocalizableLabel_4; }
	inline void set_LocalizableLabel_4(Text_t356221433 * value)
	{
		___LocalizableLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___LocalizableLabel_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
