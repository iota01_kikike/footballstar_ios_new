﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GoogleMobileAds.Api.BannerView
struct BannerView_t1745853549;
// GoogleMobileAds.Api.InterstitialAd
struct InterstitialAd_t3805611425;
// GoogleMobileAds.Api.RewardBasedVideoAd
struct RewardBasedVideoAd_t2581948736;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdMobController
struct  AdMobController_t3743396963  : public MonoBehaviour_t1158329972
{
public:
	// GoogleMobileAds.Api.BannerView AdMobController::bannerView
	BannerView_t1745853549 * ___bannerView_2;
	// GoogleMobileAds.Api.InterstitialAd AdMobController::interstitial
	InterstitialAd_t3805611425 * ___interstitial_3;
	// GoogleMobileAds.Api.RewardBasedVideoAd AdMobController::rewardVideo
	RewardBasedVideoAd_t2581948736 * ___rewardVideo_4;
	// System.String AdMobController::currentVideoType
	String_t* ___currentVideoType_5;

public:
	inline static int32_t get_offset_of_bannerView_2() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963, ___bannerView_2)); }
	inline BannerView_t1745853549 * get_bannerView_2() const { return ___bannerView_2; }
	inline BannerView_t1745853549 ** get_address_of_bannerView_2() { return &___bannerView_2; }
	inline void set_bannerView_2(BannerView_t1745853549 * value)
	{
		___bannerView_2 = value;
		Il2CppCodeGenWriteBarrier(&___bannerView_2, value);
	}

	inline static int32_t get_offset_of_interstitial_3() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963, ___interstitial_3)); }
	inline InterstitialAd_t3805611425 * get_interstitial_3() const { return ___interstitial_3; }
	inline InterstitialAd_t3805611425 ** get_address_of_interstitial_3() { return &___interstitial_3; }
	inline void set_interstitial_3(InterstitialAd_t3805611425 * value)
	{
		___interstitial_3 = value;
		Il2CppCodeGenWriteBarrier(&___interstitial_3, value);
	}

	inline static int32_t get_offset_of_rewardVideo_4() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963, ___rewardVideo_4)); }
	inline RewardBasedVideoAd_t2581948736 * get_rewardVideo_4() const { return ___rewardVideo_4; }
	inline RewardBasedVideoAd_t2581948736 ** get_address_of_rewardVideo_4() { return &___rewardVideo_4; }
	inline void set_rewardVideo_4(RewardBasedVideoAd_t2581948736 * value)
	{
		___rewardVideo_4 = value;
		Il2CppCodeGenWriteBarrier(&___rewardVideo_4, value);
	}

	inline static int32_t get_offset_of_currentVideoType_5() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963, ___currentVideoType_5)); }
	inline String_t* get_currentVideoType_5() const { return ___currentVideoType_5; }
	inline String_t** get_address_of_currentVideoType_5() { return &___currentVideoType_5; }
	inline void set_currentVideoType_5(String_t* value)
	{
		___currentVideoType_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentVideoType_5, value);
	}
};

struct AdMobController_t3743396963_StaticFields
{
public:
	// System.String AdMobController::IOS_BANNER_ID
	String_t* ___IOS_BANNER_ID_6;
	// System.String AdMobController::IOS_INTERSTITIAL_ID
	String_t* ___IOS_INTERSTITIAL_ID_7;
	// System.String AdMobController::IOS_REWARD_VIDEO_ID
	String_t* ___IOS_REWARD_VIDEO_ID_8;

public:
	inline static int32_t get_offset_of_IOS_BANNER_ID_6() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963_StaticFields, ___IOS_BANNER_ID_6)); }
	inline String_t* get_IOS_BANNER_ID_6() const { return ___IOS_BANNER_ID_6; }
	inline String_t** get_address_of_IOS_BANNER_ID_6() { return &___IOS_BANNER_ID_6; }
	inline void set_IOS_BANNER_ID_6(String_t* value)
	{
		___IOS_BANNER_ID_6 = value;
		Il2CppCodeGenWriteBarrier(&___IOS_BANNER_ID_6, value);
	}

	inline static int32_t get_offset_of_IOS_INTERSTITIAL_ID_7() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963_StaticFields, ___IOS_INTERSTITIAL_ID_7)); }
	inline String_t* get_IOS_INTERSTITIAL_ID_7() const { return ___IOS_INTERSTITIAL_ID_7; }
	inline String_t** get_address_of_IOS_INTERSTITIAL_ID_7() { return &___IOS_INTERSTITIAL_ID_7; }
	inline void set_IOS_INTERSTITIAL_ID_7(String_t* value)
	{
		___IOS_INTERSTITIAL_ID_7 = value;
		Il2CppCodeGenWriteBarrier(&___IOS_INTERSTITIAL_ID_7, value);
	}

	inline static int32_t get_offset_of_IOS_REWARD_VIDEO_ID_8() { return static_cast<int32_t>(offsetof(AdMobController_t3743396963_StaticFields, ___IOS_REWARD_VIDEO_ID_8)); }
	inline String_t* get_IOS_REWARD_VIDEO_ID_8() const { return ___IOS_REWARD_VIDEO_ID_8; }
	inline String_t** get_address_of_IOS_REWARD_VIDEO_ID_8() { return &___IOS_REWARD_VIDEO_ID_8; }
	inline void set_IOS_REWARD_VIDEO_ID_8(String_t* value)
	{
		___IOS_REWARD_VIDEO_ID_8 = value;
		Il2CppCodeGenWriteBarrier(&___IOS_REWARD_VIDEO_ID_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
