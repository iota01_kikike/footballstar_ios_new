﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.SubscriptionManager/<>c
struct U3CU3Ec_t3750601015;
// System.Comparison`1<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>
struct Comparison_1_t238470304;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SubscriptionManager/<>c
struct  U3CU3Ec_t3750601015  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t3750601015_StaticFields
{
public:
	// UnityEngine.Purchasing.SubscriptionManager/<>c UnityEngine.Purchasing.SubscriptionManager/<>c::<>9
	U3CU3Ec_t3750601015 * ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt> UnityEngine.Purchasing.SubscriptionManager/<>c::<>9__7_0
	Comparison_1_t238470304 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3750601015_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3750601015 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3750601015 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3750601015 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3750601015_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Comparison_1_t238470304 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Comparison_1_t238470304 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Comparison_1_t238470304 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__7_0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
