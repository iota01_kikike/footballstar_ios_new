﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.IO.FileInfo
struct FileInfo_t3153503742;
// UnityEngine.WWW
struct WWW_t2919945039;
// NativeShare
struct NativeShare_t1150945090;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/<LoadFiles>c__Iterator1
struct  U3CLoadFilesU3Ec__Iterator1_t4154548522  : public Il2CppObject
{
public:
	// System.String[] NativeShare/<LoadFiles>c__Iterator1::filesPath
	StringU5BU5D_t1642385972* ___filesPath_0;
	// System.String[] NativeShare/<LoadFiles>c__Iterator1::<filesList>__0
	StringU5BU5D_t1642385972* ___U3CfilesListU3E__0_1;
	// System.String NativeShare/<LoadFiles>c__Iterator1::<baseFloder>__0
	String_t* ___U3CbaseFloderU3E__0_2;
	// System.Single NativeShare/<LoadFiles>c__Iterator1::<progres>__0
	float ___U3CprogresU3E__0_3;
	// System.Boolean NativeShare/<LoadFiles>c__Iterator1::<loadFile>__0
	bool ___U3CloadFileU3E__0_4;
	// System.Int32 NativeShare/<LoadFiles>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_5;
	// System.String NativeShare/<LoadFiles>c__Iterator1::<fileURL>__2
	String_t* ___U3CfileURLU3E__2_6;
	// System.IO.FileInfo NativeShare/<LoadFiles>c__Iterator1::<fileInfo>__2
	FileInfo_t3153503742 * ___U3CfileInfoU3E__2_7;
	// System.String NativeShare/<LoadFiles>c__Iterator1::<pathSave>__2
	String_t* ___U3CpathSaveU3E__2_8;
	// UnityEngine.WWW NativeShare/<LoadFiles>c__Iterator1::<www>__2
	WWW_t2919945039 * ___U3CwwwU3E__2_9;
	// System.String NativeShare/<LoadFiles>c__Iterator1::subject
	String_t* ___subject_10;
	// System.String NativeShare/<LoadFiles>c__Iterator1::msg
	String_t* ___msg_11;
	// System.String[] NativeShare/<LoadFiles>c__Iterator1::emails
	StringU5BU5D_t1642385972* ___emails_12;
	// System.String NativeShare/<LoadFiles>c__Iterator1::shareApp
	String_t* ___shareApp_13;
	// NativeShare NativeShare/<LoadFiles>c__Iterator1::$this
	NativeShare_t1150945090 * ___U24this_14;
	// System.Object NativeShare/<LoadFiles>c__Iterator1::$current
	Il2CppObject * ___U24current_15;
	// System.Boolean NativeShare/<LoadFiles>c__Iterator1::$disposing
	bool ___U24disposing_16;
	// System.Int32 NativeShare/<LoadFiles>c__Iterator1::$PC
	int32_t ___U24PC_17;

public:
	inline static int32_t get_offset_of_filesPath_0() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___filesPath_0)); }
	inline StringU5BU5D_t1642385972* get_filesPath_0() const { return ___filesPath_0; }
	inline StringU5BU5D_t1642385972** get_address_of_filesPath_0() { return &___filesPath_0; }
	inline void set_filesPath_0(StringU5BU5D_t1642385972* value)
	{
		___filesPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___filesPath_0, value);
	}

	inline static int32_t get_offset_of_U3CfilesListU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CfilesListU3E__0_1)); }
	inline StringU5BU5D_t1642385972* get_U3CfilesListU3E__0_1() const { return ___U3CfilesListU3E__0_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CfilesListU3E__0_1() { return &___U3CfilesListU3E__0_1; }
	inline void set_U3CfilesListU3E__0_1(StringU5BU5D_t1642385972* value)
	{
		___U3CfilesListU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilesListU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CbaseFloderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CbaseFloderU3E__0_2)); }
	inline String_t* get_U3CbaseFloderU3E__0_2() const { return ___U3CbaseFloderU3E__0_2; }
	inline String_t** get_address_of_U3CbaseFloderU3E__0_2() { return &___U3CbaseFloderU3E__0_2; }
	inline void set_U3CbaseFloderU3E__0_2(String_t* value)
	{
		___U3CbaseFloderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbaseFloderU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CprogresU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CprogresU3E__0_3)); }
	inline float get_U3CprogresU3E__0_3() const { return ___U3CprogresU3E__0_3; }
	inline float* get_address_of_U3CprogresU3E__0_3() { return &___U3CprogresU3E__0_3; }
	inline void set_U3CprogresU3E__0_3(float value)
	{
		___U3CprogresU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CloadFileU3E__0_4() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CloadFileU3E__0_4)); }
	inline bool get_U3CloadFileU3E__0_4() const { return ___U3CloadFileU3E__0_4; }
	inline bool* get_address_of_U3CloadFileU3E__0_4() { return &___U3CloadFileU3E__0_4; }
	inline void set_U3CloadFileU3E__0_4(bool value)
	{
		___U3CloadFileU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_5() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CiU3E__1_5)); }
	inline int32_t get_U3CiU3E__1_5() const { return ___U3CiU3E__1_5; }
	inline int32_t* get_address_of_U3CiU3E__1_5() { return &___U3CiU3E__1_5; }
	inline void set_U3CiU3E__1_5(int32_t value)
	{
		___U3CiU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CfileURLU3E__2_6() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CfileURLU3E__2_6)); }
	inline String_t* get_U3CfileURLU3E__2_6() const { return ___U3CfileURLU3E__2_6; }
	inline String_t** get_address_of_U3CfileURLU3E__2_6() { return &___U3CfileURLU3E__2_6; }
	inline void set_U3CfileURLU3E__2_6(String_t* value)
	{
		___U3CfileURLU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileURLU3E__2_6, value);
	}

	inline static int32_t get_offset_of_U3CfileInfoU3E__2_7() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CfileInfoU3E__2_7)); }
	inline FileInfo_t3153503742 * get_U3CfileInfoU3E__2_7() const { return ___U3CfileInfoU3E__2_7; }
	inline FileInfo_t3153503742 ** get_address_of_U3CfileInfoU3E__2_7() { return &___U3CfileInfoU3E__2_7; }
	inline void set_U3CfileInfoU3E__2_7(FileInfo_t3153503742 * value)
	{
		___U3CfileInfoU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfileInfoU3E__2_7, value);
	}

	inline static int32_t get_offset_of_U3CpathSaveU3E__2_8() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CpathSaveU3E__2_8)); }
	inline String_t* get_U3CpathSaveU3E__2_8() const { return ___U3CpathSaveU3E__2_8; }
	inline String_t** get_address_of_U3CpathSaveU3E__2_8() { return &___U3CpathSaveU3E__2_8; }
	inline void set_U3CpathSaveU3E__2_8(String_t* value)
	{
		___U3CpathSaveU3E__2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathSaveU3E__2_8, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_9() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U3CwwwU3E__2_9)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__2_9() const { return ___U3CwwwU3E__2_9; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__2_9() { return &___U3CwwwU3E__2_9; }
	inline void set_U3CwwwU3E__2_9(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__2_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__2_9, value);
	}

	inline static int32_t get_offset_of_subject_10() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___subject_10)); }
	inline String_t* get_subject_10() const { return ___subject_10; }
	inline String_t** get_address_of_subject_10() { return &___subject_10; }
	inline void set_subject_10(String_t* value)
	{
		___subject_10 = value;
		Il2CppCodeGenWriteBarrier(&___subject_10, value);
	}

	inline static int32_t get_offset_of_msg_11() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___msg_11)); }
	inline String_t* get_msg_11() const { return ___msg_11; }
	inline String_t** get_address_of_msg_11() { return &___msg_11; }
	inline void set_msg_11(String_t* value)
	{
		___msg_11 = value;
		Il2CppCodeGenWriteBarrier(&___msg_11, value);
	}

	inline static int32_t get_offset_of_emails_12() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___emails_12)); }
	inline StringU5BU5D_t1642385972* get_emails_12() const { return ___emails_12; }
	inline StringU5BU5D_t1642385972** get_address_of_emails_12() { return &___emails_12; }
	inline void set_emails_12(StringU5BU5D_t1642385972* value)
	{
		___emails_12 = value;
		Il2CppCodeGenWriteBarrier(&___emails_12, value);
	}

	inline static int32_t get_offset_of_shareApp_13() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___shareApp_13)); }
	inline String_t* get_shareApp_13() const { return ___shareApp_13; }
	inline String_t** get_address_of_shareApp_13() { return &___shareApp_13; }
	inline void set_shareApp_13(String_t* value)
	{
		___shareApp_13 = value;
		Il2CppCodeGenWriteBarrier(&___shareApp_13, value);
	}

	inline static int32_t get_offset_of_U24this_14() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U24this_14)); }
	inline NativeShare_t1150945090 * get_U24this_14() const { return ___U24this_14; }
	inline NativeShare_t1150945090 ** get_address_of_U24this_14() { return &___U24this_14; }
	inline void set_U24this_14(NativeShare_t1150945090 * value)
	{
		___U24this_14 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_14, value);
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U24current_15)); }
	inline Il2CppObject * get_U24current_15() const { return ___U24current_15; }
	inline Il2CppObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(Il2CppObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_15, value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator1_t4154548522, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
