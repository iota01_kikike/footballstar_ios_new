﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1905710121.h"

// System.String
struct String_t;
// CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector
struct ObscuredCheatingDetector_t2708612136;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector
struct  ObscuredCheatingDetector_t2708612136  : public ActDetectorBase_t1905710121
{
public:
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::floatEpsilon
	float ___floatEpsilon_17;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::vector2Epsilon
	float ___vector2Epsilon_18;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::vector3Epsilon
	float ___vector3Epsilon_19;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::quaternionEpsilon
	float ___quaternionEpsilon_20;

public:
	inline static int32_t get_offset_of_floatEpsilon_17() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t2708612136, ___floatEpsilon_17)); }
	inline float get_floatEpsilon_17() const { return ___floatEpsilon_17; }
	inline float* get_address_of_floatEpsilon_17() { return &___floatEpsilon_17; }
	inline void set_floatEpsilon_17(float value)
	{
		___floatEpsilon_17 = value;
	}

	inline static int32_t get_offset_of_vector2Epsilon_18() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t2708612136, ___vector2Epsilon_18)); }
	inline float get_vector2Epsilon_18() const { return ___vector2Epsilon_18; }
	inline float* get_address_of_vector2Epsilon_18() { return &___vector2Epsilon_18; }
	inline void set_vector2Epsilon_18(float value)
	{
		___vector2Epsilon_18 = value;
	}

	inline static int32_t get_offset_of_vector3Epsilon_19() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t2708612136, ___vector3Epsilon_19)); }
	inline float get_vector3Epsilon_19() const { return ___vector3Epsilon_19; }
	inline float* get_address_of_vector3Epsilon_19() { return &___vector3Epsilon_19; }
	inline void set_vector3Epsilon_19(float value)
	{
		___vector3Epsilon_19 = value;
	}

	inline static int32_t get_offset_of_quaternionEpsilon_20() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t2708612136, ___quaternionEpsilon_20)); }
	inline float get_quaternionEpsilon_20() const { return ___quaternionEpsilon_20; }
	inline float* get_address_of_quaternionEpsilon_20() { return &___quaternionEpsilon_20; }
	inline void set_quaternionEpsilon_20(float value)
	{
		___quaternionEpsilon_20 = value;
	}
};

struct ObscuredCheatingDetector_t2708612136_StaticFields
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::instancesInScene
	int32_t ___instancesInScene_16;
	// CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::<Instance>k__BackingField
	ObscuredCheatingDetector_t2708612136 * ___U3CInstanceU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_instancesInScene_16() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t2708612136_StaticFields, ___instancesInScene_16)); }
	inline int32_t get_instancesInScene_16() const { return ___instancesInScene_16; }
	inline int32_t* get_address_of_instancesInScene_16() { return &___instancesInScene_16; }
	inline void set_instancesInScene_16(int32_t value)
	{
		___instancesInScene_16 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t2708612136_StaticFields, ___U3CInstanceU3Ek__BackingField_21)); }
	inline ObscuredCheatingDetector_t2708612136 * get_U3CInstanceU3Ek__BackingField_21() const { return ___U3CInstanceU3Ek__BackingField_21; }
	inline ObscuredCheatingDetector_t2708612136 ** get_address_of_U3CInstanceU3Ek__BackingField_21() { return &___U3CInstanceU3Ek__BackingField_21; }
	inline void set_U3CInstanceU3Ek__BackingField_21(ObscuredCheatingDetector_t2708612136 * value)
	{
		___U3CInstanceU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
