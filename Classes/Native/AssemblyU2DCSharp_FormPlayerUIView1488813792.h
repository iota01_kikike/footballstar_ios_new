﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_FormUIType1820353380.h"
#include "AssemblyU2DCSharp_InfoType242837520.h"

// System.String
struct String_t;
// Player
struct Player_t1147783557;
// PlayerUserData
struct PlayerUserData_t4090529802;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// FormPlayerUIView/DelegateOnChanged
struct DelegateOnChanged_t798682617;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FormPlayerUIView
struct  FormPlayerUIView_t1488813792  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FormPlayerUIView::index
	int32_t ___index_2;
	// System.String FormPlayerUIView::positionName
	String_t* ___positionName_3;
	// FormUIType FormPlayerUIView::formUIType
	int32_t ___formUIType_4;
	// InfoType FormPlayerUIView::infoType
	int32_t ___infoType_5;
	// Player FormPlayerUIView::PlayerData
	Player_t1147783557 * ___PlayerData_6;
	// PlayerUserData FormPlayerUIView::PlayerUserData
	PlayerUserData_t4090529802 * ___PlayerUserData_7;
	// UnityEngine.UI.Text FormPlayerUIView::NameText
	Text_t356221433 * ___NameText_8;
	// UnityEngine.UI.Button FormPlayerUIView::Button
	Button_t2872111280 * ___Button_9;
	// UnityEngine.UI.Button FormPlayerUIView::InfoButton
	Button_t2872111280 * ___InfoButton_10;
	// UnityEngine.UI.Button FormPlayerUIView::ChangeButton
	Button_t2872111280 * ___ChangeButton_11;
	// UnityEngine.UI.Button FormPlayerUIView::RemoveButton
	Button_t2872111280 * ___RemoveButton_12;
	// UnityEngine.UI.Image FormPlayerUIView::LevelUpIcon
	Image_t2042527209 * ___LevelUpIcon_13;
	// UnityEngine.UI.Image FormPlayerUIView::StarIcon
	Image_t2042527209 * ___StarIcon_14;
	// UnityEngine.UI.Image FormPlayerUIView::FlagImage
	Image_t2042527209 * ___FlagImage_15;
	// UnityEngine.UI.Image FormPlayerUIView::OverallBox
	Image_t2042527209 * ___OverallBox_16;
	// UnityEngine.UI.Text FormPlayerUIView::OverallText
	Text_t356221433 * ___OverallText_17;
	// UnityEngine.UI.Text FormPlayerUIView::PositionText
	Text_t356221433 * ___PositionText_18;
	// UnityEngine.UI.Text FormPlayerUIView::GradeText
	Text_t356221433 * ___GradeText_19;
	// UnityEngine.UI.Text FormPlayerUIView::LevelText
	Text_t356221433 * ___LevelText_20;
	// UnityEngine.GameObject FormPlayerUIView::ExpendPanel
	GameObject_t1756533147 * ___ExpendPanel_21;
	// UnityEngine.UI.Image FormPlayerUIView::Badge
	Image_t2042527209 * ___Badge_22;
	// FormPlayerUIView/DelegateOnChanged FormPlayerUIView::OnExpendOff
	DelegateOnChanged_t798682617 * ___OnExpendOff_23;
	// FormPlayerUIView/DelegateOnChanged FormPlayerUIView::OnExpendOn
	DelegateOnChanged_t798682617 * ___OnExpendOn_24;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_positionName_3() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___positionName_3)); }
	inline String_t* get_positionName_3() const { return ___positionName_3; }
	inline String_t** get_address_of_positionName_3() { return &___positionName_3; }
	inline void set_positionName_3(String_t* value)
	{
		___positionName_3 = value;
		Il2CppCodeGenWriteBarrier(&___positionName_3, value);
	}

	inline static int32_t get_offset_of_formUIType_4() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___formUIType_4)); }
	inline int32_t get_formUIType_4() const { return ___formUIType_4; }
	inline int32_t* get_address_of_formUIType_4() { return &___formUIType_4; }
	inline void set_formUIType_4(int32_t value)
	{
		___formUIType_4 = value;
	}

	inline static int32_t get_offset_of_infoType_5() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___infoType_5)); }
	inline int32_t get_infoType_5() const { return ___infoType_5; }
	inline int32_t* get_address_of_infoType_5() { return &___infoType_5; }
	inline void set_infoType_5(int32_t value)
	{
		___infoType_5 = value;
	}

	inline static int32_t get_offset_of_PlayerData_6() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___PlayerData_6)); }
	inline Player_t1147783557 * get_PlayerData_6() const { return ___PlayerData_6; }
	inline Player_t1147783557 ** get_address_of_PlayerData_6() { return &___PlayerData_6; }
	inline void set_PlayerData_6(Player_t1147783557 * value)
	{
		___PlayerData_6 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerData_6, value);
	}

	inline static int32_t get_offset_of_PlayerUserData_7() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___PlayerUserData_7)); }
	inline PlayerUserData_t4090529802 * get_PlayerUserData_7() const { return ___PlayerUserData_7; }
	inline PlayerUserData_t4090529802 ** get_address_of_PlayerUserData_7() { return &___PlayerUserData_7; }
	inline void set_PlayerUserData_7(PlayerUserData_t4090529802 * value)
	{
		___PlayerUserData_7 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerUserData_7, value);
	}

	inline static int32_t get_offset_of_NameText_8() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___NameText_8)); }
	inline Text_t356221433 * get_NameText_8() const { return ___NameText_8; }
	inline Text_t356221433 ** get_address_of_NameText_8() { return &___NameText_8; }
	inline void set_NameText_8(Text_t356221433 * value)
	{
		___NameText_8 = value;
		Il2CppCodeGenWriteBarrier(&___NameText_8, value);
	}

	inline static int32_t get_offset_of_Button_9() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___Button_9)); }
	inline Button_t2872111280 * get_Button_9() const { return ___Button_9; }
	inline Button_t2872111280 ** get_address_of_Button_9() { return &___Button_9; }
	inline void set_Button_9(Button_t2872111280 * value)
	{
		___Button_9 = value;
		Il2CppCodeGenWriteBarrier(&___Button_9, value);
	}

	inline static int32_t get_offset_of_InfoButton_10() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___InfoButton_10)); }
	inline Button_t2872111280 * get_InfoButton_10() const { return ___InfoButton_10; }
	inline Button_t2872111280 ** get_address_of_InfoButton_10() { return &___InfoButton_10; }
	inline void set_InfoButton_10(Button_t2872111280 * value)
	{
		___InfoButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___InfoButton_10, value);
	}

	inline static int32_t get_offset_of_ChangeButton_11() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___ChangeButton_11)); }
	inline Button_t2872111280 * get_ChangeButton_11() const { return ___ChangeButton_11; }
	inline Button_t2872111280 ** get_address_of_ChangeButton_11() { return &___ChangeButton_11; }
	inline void set_ChangeButton_11(Button_t2872111280 * value)
	{
		___ChangeButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___ChangeButton_11, value);
	}

	inline static int32_t get_offset_of_RemoveButton_12() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___RemoveButton_12)); }
	inline Button_t2872111280 * get_RemoveButton_12() const { return ___RemoveButton_12; }
	inline Button_t2872111280 ** get_address_of_RemoveButton_12() { return &___RemoveButton_12; }
	inline void set_RemoveButton_12(Button_t2872111280 * value)
	{
		___RemoveButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___RemoveButton_12, value);
	}

	inline static int32_t get_offset_of_LevelUpIcon_13() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___LevelUpIcon_13)); }
	inline Image_t2042527209 * get_LevelUpIcon_13() const { return ___LevelUpIcon_13; }
	inline Image_t2042527209 ** get_address_of_LevelUpIcon_13() { return &___LevelUpIcon_13; }
	inline void set_LevelUpIcon_13(Image_t2042527209 * value)
	{
		___LevelUpIcon_13 = value;
		Il2CppCodeGenWriteBarrier(&___LevelUpIcon_13, value);
	}

	inline static int32_t get_offset_of_StarIcon_14() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___StarIcon_14)); }
	inline Image_t2042527209 * get_StarIcon_14() const { return ___StarIcon_14; }
	inline Image_t2042527209 ** get_address_of_StarIcon_14() { return &___StarIcon_14; }
	inline void set_StarIcon_14(Image_t2042527209 * value)
	{
		___StarIcon_14 = value;
		Il2CppCodeGenWriteBarrier(&___StarIcon_14, value);
	}

	inline static int32_t get_offset_of_FlagImage_15() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___FlagImage_15)); }
	inline Image_t2042527209 * get_FlagImage_15() const { return ___FlagImage_15; }
	inline Image_t2042527209 ** get_address_of_FlagImage_15() { return &___FlagImage_15; }
	inline void set_FlagImage_15(Image_t2042527209 * value)
	{
		___FlagImage_15 = value;
		Il2CppCodeGenWriteBarrier(&___FlagImage_15, value);
	}

	inline static int32_t get_offset_of_OverallBox_16() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___OverallBox_16)); }
	inline Image_t2042527209 * get_OverallBox_16() const { return ___OverallBox_16; }
	inline Image_t2042527209 ** get_address_of_OverallBox_16() { return &___OverallBox_16; }
	inline void set_OverallBox_16(Image_t2042527209 * value)
	{
		___OverallBox_16 = value;
		Il2CppCodeGenWriteBarrier(&___OverallBox_16, value);
	}

	inline static int32_t get_offset_of_OverallText_17() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___OverallText_17)); }
	inline Text_t356221433 * get_OverallText_17() const { return ___OverallText_17; }
	inline Text_t356221433 ** get_address_of_OverallText_17() { return &___OverallText_17; }
	inline void set_OverallText_17(Text_t356221433 * value)
	{
		___OverallText_17 = value;
		Il2CppCodeGenWriteBarrier(&___OverallText_17, value);
	}

	inline static int32_t get_offset_of_PositionText_18() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___PositionText_18)); }
	inline Text_t356221433 * get_PositionText_18() const { return ___PositionText_18; }
	inline Text_t356221433 ** get_address_of_PositionText_18() { return &___PositionText_18; }
	inline void set_PositionText_18(Text_t356221433 * value)
	{
		___PositionText_18 = value;
		Il2CppCodeGenWriteBarrier(&___PositionText_18, value);
	}

	inline static int32_t get_offset_of_GradeText_19() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___GradeText_19)); }
	inline Text_t356221433 * get_GradeText_19() const { return ___GradeText_19; }
	inline Text_t356221433 ** get_address_of_GradeText_19() { return &___GradeText_19; }
	inline void set_GradeText_19(Text_t356221433 * value)
	{
		___GradeText_19 = value;
		Il2CppCodeGenWriteBarrier(&___GradeText_19, value);
	}

	inline static int32_t get_offset_of_LevelText_20() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___LevelText_20)); }
	inline Text_t356221433 * get_LevelText_20() const { return ___LevelText_20; }
	inline Text_t356221433 ** get_address_of_LevelText_20() { return &___LevelText_20; }
	inline void set_LevelText_20(Text_t356221433 * value)
	{
		___LevelText_20 = value;
		Il2CppCodeGenWriteBarrier(&___LevelText_20, value);
	}

	inline static int32_t get_offset_of_ExpendPanel_21() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___ExpendPanel_21)); }
	inline GameObject_t1756533147 * get_ExpendPanel_21() const { return ___ExpendPanel_21; }
	inline GameObject_t1756533147 ** get_address_of_ExpendPanel_21() { return &___ExpendPanel_21; }
	inline void set_ExpendPanel_21(GameObject_t1756533147 * value)
	{
		___ExpendPanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___ExpendPanel_21, value);
	}

	inline static int32_t get_offset_of_Badge_22() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___Badge_22)); }
	inline Image_t2042527209 * get_Badge_22() const { return ___Badge_22; }
	inline Image_t2042527209 ** get_address_of_Badge_22() { return &___Badge_22; }
	inline void set_Badge_22(Image_t2042527209 * value)
	{
		___Badge_22 = value;
		Il2CppCodeGenWriteBarrier(&___Badge_22, value);
	}

	inline static int32_t get_offset_of_OnExpendOff_23() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___OnExpendOff_23)); }
	inline DelegateOnChanged_t798682617 * get_OnExpendOff_23() const { return ___OnExpendOff_23; }
	inline DelegateOnChanged_t798682617 ** get_address_of_OnExpendOff_23() { return &___OnExpendOff_23; }
	inline void set_OnExpendOff_23(DelegateOnChanged_t798682617 * value)
	{
		___OnExpendOff_23 = value;
		Il2CppCodeGenWriteBarrier(&___OnExpendOff_23, value);
	}

	inline static int32_t get_offset_of_OnExpendOn_24() { return static_cast<int32_t>(offsetof(FormPlayerUIView_t1488813792, ___OnExpendOn_24)); }
	inline DelegateOnChanged_t798682617 * get_OnExpendOn_24() const { return ___OnExpendOn_24; }
	inline DelegateOnChanged_t798682617 ** get_address_of_OnExpendOn_24() { return &___OnExpendOn_24; }
	inline void set_OnExpendOn_24(DelegateOnChanged_t798682617 * value)
	{
		___OnExpendOn_24 = value;
		Il2CppCodeGenWriteBarrier(&___OnExpendOn_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
