﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdvertisePop
struct  AdvertisePop_t3889815228  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button AdvertisePop::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// System.String AdvertisePop::linkURL
	String_t* ___linkURL_3;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(AdvertisePop_t3889815228, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_linkURL_3() { return static_cast<int32_t>(offsetof(AdvertisePop_t3889815228, ___linkURL_3)); }
	inline String_t* get_linkURL_3() const { return ___linkURL_3; }
	inline String_t** get_address_of_linkURL_3() { return &___linkURL_3; }
	inline void set_linkURL_3(String_t* value)
	{
		___linkURL_3 = value;
		Il2CppCodeGenWriteBarrier(&___linkURL_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
