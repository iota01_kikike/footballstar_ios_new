﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ScoutCellView2404854735.h"

// ScoutCell
struct ScoutCell_t2879086938;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellImageView
struct  ScoutCellImageView_t3741486866  : public ScoutCellView_t2404854735
{
public:
	// ScoutCell ScoutCellImageView::_parentCell
	ScoutCell_t2879086938 * ____parentCell_2;

public:
	inline static int32_t get_offset_of__parentCell_2() { return static_cast<int32_t>(offsetof(ScoutCellImageView_t3741486866, ____parentCell_2)); }
	inline ScoutCell_t2879086938 * get__parentCell_2() const { return ____parentCell_2; }
	inline ScoutCell_t2879086938 ** get_address_of__parentCell_2() { return &____parentCell_2; }
	inline void set__parentCell_2(ScoutCell_t2879086938 * value)
	{
		____parentCell_2 = value;
		Il2CppCodeGenWriteBarrier(&____parentCell_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
