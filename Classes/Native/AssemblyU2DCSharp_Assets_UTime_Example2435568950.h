﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.UTime.Example
struct  Example_t2435568950  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Assets.UTime.Example::Time
	Text_t356221433 * ___Time_2;

public:
	inline static int32_t get_offset_of_Time_2() { return static_cast<int32_t>(offsetof(Example_t2435568950, ___Time_2)); }
	inline Text_t356221433 * get_Time_2() const { return ___Time_2; }
	inline Text_t356221433 ** get_address_of_Time_2() { return &___Time_2; }
	inline void set_Time_2(Text_t356221433 * value)
	{
		___Time_2 = value;
		Il2CppCodeGenWriteBarrier(&___Time_2, value);
	}
};

struct Example_t2435568950_StaticFields
{
public:
	// System.Action`1<System.Boolean> Assets.UTime.Example::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(Example_t2435568950_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
