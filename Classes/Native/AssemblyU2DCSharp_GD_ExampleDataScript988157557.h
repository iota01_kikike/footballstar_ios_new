﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GD_ExampleDataScript
struct  GD_ExampleDataScript_t988157557  : public MonoBehaviour_t1158329972
{
public:
	// System.String GD_ExampleDataScript::MyString
	String_t* ___MyString_2;
	// System.Single[] GD_ExampleDataScript::MyFloatArray
	SingleU5BU5D_t577127397* ___MyFloatArray_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GD_ExampleDataScript::MyDictionary
	Dictionary_2_t3943999495 * ___MyDictionary_4;

public:
	inline static int32_t get_offset_of_MyString_2() { return static_cast<int32_t>(offsetof(GD_ExampleDataScript_t988157557, ___MyString_2)); }
	inline String_t* get_MyString_2() const { return ___MyString_2; }
	inline String_t** get_address_of_MyString_2() { return &___MyString_2; }
	inline void set_MyString_2(String_t* value)
	{
		___MyString_2 = value;
		Il2CppCodeGenWriteBarrier(&___MyString_2, value);
	}

	inline static int32_t get_offset_of_MyFloatArray_3() { return static_cast<int32_t>(offsetof(GD_ExampleDataScript_t988157557, ___MyFloatArray_3)); }
	inline SingleU5BU5D_t577127397* get_MyFloatArray_3() const { return ___MyFloatArray_3; }
	inline SingleU5BU5D_t577127397** get_address_of_MyFloatArray_3() { return &___MyFloatArray_3; }
	inline void set_MyFloatArray_3(SingleU5BU5D_t577127397* value)
	{
		___MyFloatArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___MyFloatArray_3, value);
	}

	inline static int32_t get_offset_of_MyDictionary_4() { return static_cast<int32_t>(offsetof(GD_ExampleDataScript_t988157557, ___MyDictionary_4)); }
	inline Dictionary_2_t3943999495 * get_MyDictionary_4() const { return ___MyDictionary_4; }
	inline Dictionary_2_t3943999495 ** get_address_of_MyDictionary_4() { return &___MyDictionary_4; }
	inline void set_MyDictionary_4(Dictionary_2_t3943999495 * value)
	{
		___MyDictionary_4 = value;
		Il2CppCodeGenWriteBarrier(&___MyDictionary_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
