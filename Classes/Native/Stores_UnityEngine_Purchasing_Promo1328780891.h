﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"

// UnityEngine.Purchasing.JSONStore
struct JSONStore_t1890359403;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// System.String
struct String_t;
// Uniject.IUtil
struct IUtil_t2188430191;
// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t364059421;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Promo
struct  Promo_t1328780891  : public Il2CppObject
{
public:

public:
};

struct Promo_t1328780891_StaticFields
{
public:
	// UnityEngine.Purchasing.JSONStore UnityEngine.Purchasing.Promo::s_PromoPurchaser
	JSONStore_t1890359403 * ___s_PromoPurchaser_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.Promo::s_Unity
	Il2CppObject * ___s_Unity_1;
	// UnityEngine.RuntimePlatform UnityEngine.Purchasing.Promo::s_RuntimePlatform
	int32_t ___s_RuntimePlatform_2;
	// UnityEngine.ILogger UnityEngine.Purchasing.Promo::s_Logger
	Il2CppObject * ___s_Logger_3;
	// System.String UnityEngine.Purchasing.Promo::s_Version
	String_t* ___s_Version_4;
	// Uniject.IUtil UnityEngine.Purchasing.Promo::s_Util
	Il2CppObject * ___s_Util_5;
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.Promo::s_WebUtil
	Il2CppObject * ___s_WebUtil_6;
	// System.Boolean UnityEngine.Purchasing.Promo::s_IsReady
	bool ___s_IsReady_7;
	// System.String UnityEngine.Purchasing.Promo::s_ProductJSON
	String_t* ___s_ProductJSON_8;

public:
	inline static int32_t get_offset_of_s_PromoPurchaser_0() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_PromoPurchaser_0)); }
	inline JSONStore_t1890359403 * get_s_PromoPurchaser_0() const { return ___s_PromoPurchaser_0; }
	inline JSONStore_t1890359403 ** get_address_of_s_PromoPurchaser_0() { return &___s_PromoPurchaser_0; }
	inline void set_s_PromoPurchaser_0(JSONStore_t1890359403 * value)
	{
		___s_PromoPurchaser_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_PromoPurchaser_0, value);
	}

	inline static int32_t get_offset_of_s_Unity_1() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_Unity_1)); }
	inline Il2CppObject * get_s_Unity_1() const { return ___s_Unity_1; }
	inline Il2CppObject ** get_address_of_s_Unity_1() { return &___s_Unity_1; }
	inline void set_s_Unity_1(Il2CppObject * value)
	{
		___s_Unity_1 = value;
		Il2CppCodeGenWriteBarrier(&___s_Unity_1, value);
	}

	inline static int32_t get_offset_of_s_RuntimePlatform_2() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_RuntimePlatform_2)); }
	inline int32_t get_s_RuntimePlatform_2() const { return ___s_RuntimePlatform_2; }
	inline int32_t* get_address_of_s_RuntimePlatform_2() { return &___s_RuntimePlatform_2; }
	inline void set_s_RuntimePlatform_2(int32_t value)
	{
		___s_RuntimePlatform_2 = value;
	}

	inline static int32_t get_offset_of_s_Logger_3() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_Logger_3)); }
	inline Il2CppObject * get_s_Logger_3() const { return ___s_Logger_3; }
	inline Il2CppObject ** get_address_of_s_Logger_3() { return &___s_Logger_3; }
	inline void set_s_Logger_3(Il2CppObject * value)
	{
		___s_Logger_3 = value;
		Il2CppCodeGenWriteBarrier(&___s_Logger_3, value);
	}

	inline static int32_t get_offset_of_s_Version_4() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_Version_4)); }
	inline String_t* get_s_Version_4() const { return ___s_Version_4; }
	inline String_t** get_address_of_s_Version_4() { return &___s_Version_4; }
	inline void set_s_Version_4(String_t* value)
	{
		___s_Version_4 = value;
		Il2CppCodeGenWriteBarrier(&___s_Version_4, value);
	}

	inline static int32_t get_offset_of_s_Util_5() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_Util_5)); }
	inline Il2CppObject * get_s_Util_5() const { return ___s_Util_5; }
	inline Il2CppObject ** get_address_of_s_Util_5() { return &___s_Util_5; }
	inline void set_s_Util_5(Il2CppObject * value)
	{
		___s_Util_5 = value;
		Il2CppCodeGenWriteBarrier(&___s_Util_5, value);
	}

	inline static int32_t get_offset_of_s_WebUtil_6() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_WebUtil_6)); }
	inline Il2CppObject * get_s_WebUtil_6() const { return ___s_WebUtil_6; }
	inline Il2CppObject ** get_address_of_s_WebUtil_6() { return &___s_WebUtil_6; }
	inline void set_s_WebUtil_6(Il2CppObject * value)
	{
		___s_WebUtil_6 = value;
		Il2CppCodeGenWriteBarrier(&___s_WebUtil_6, value);
	}

	inline static int32_t get_offset_of_s_IsReady_7() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_IsReady_7)); }
	inline bool get_s_IsReady_7() const { return ___s_IsReady_7; }
	inline bool* get_address_of_s_IsReady_7() { return &___s_IsReady_7; }
	inline void set_s_IsReady_7(bool value)
	{
		___s_IsReady_7 = value;
	}

	inline static int32_t get_offset_of_s_ProductJSON_8() { return static_cast<int32_t>(offsetof(Promo_t1328780891_StaticFields, ___s_ProductJSON_8)); }
	inline String_t* get_s_ProductJSON_8() const { return ___s_ProductJSON_8; }
	inline String_t** get_address_of_s_ProductJSON_8() { return &___s_ProductJSON_8; }
	inline void set_s_ProductJSON_8(String_t* value)
	{
		___s_ProductJSON_8 = value;
		Il2CppCodeGenWriteBarrier(&___s_ProductJSON_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
