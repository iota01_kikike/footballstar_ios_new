﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SubscriptionManager
struct  SubscriptionManager_t1070011402  : public Il2CppObject
{
public:
	// System.String UnityEngine.Purchasing.SubscriptionManager::receipt
	String_t* ___receipt_0;
	// System.String UnityEngine.Purchasing.SubscriptionManager::productId
	String_t* ___productId_1;
	// System.String UnityEngine.Purchasing.SubscriptionManager::intro_json
	String_t* ___intro_json_2;

public:
	inline static int32_t get_offset_of_receipt_0() { return static_cast<int32_t>(offsetof(SubscriptionManager_t1070011402, ___receipt_0)); }
	inline String_t* get_receipt_0() const { return ___receipt_0; }
	inline String_t** get_address_of_receipt_0() { return &___receipt_0; }
	inline void set_receipt_0(String_t* value)
	{
		___receipt_0 = value;
		Il2CppCodeGenWriteBarrier(&___receipt_0, value);
	}

	inline static int32_t get_offset_of_productId_1() { return static_cast<int32_t>(offsetof(SubscriptionManager_t1070011402, ___productId_1)); }
	inline String_t* get_productId_1() const { return ___productId_1; }
	inline String_t** get_address_of_productId_1() { return &___productId_1; }
	inline void set_productId_1(String_t* value)
	{
		___productId_1 = value;
		Il2CppCodeGenWriteBarrier(&___productId_1, value);
	}

	inline static int32_t get_offset_of_intro_json_2() { return static_cast<int32_t>(offsetof(SubscriptionManager_t1070011402, ___intro_json_2)); }
	inline String_t* get_intro_json_2() const { return ___intro_json_2; }
	inline String_t** get_address_of_intro_json_2() { return &___intro_json_2; }
	inline void set_intro_json_2(String_t* value)
	{
		___intro_json_2 = value;
		Il2CppCodeGenWriteBarrier(&___intro_json_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
