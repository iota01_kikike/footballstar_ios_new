﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$8 <PrivateImplementationDetails>::$$field-26
	U24ArrayTypeU248_t1957337328  ___U24U24fieldU2D26_0;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-27
	U24ArrayTypeU24256_t2038352957  ___U24U24fieldU2D27_1;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-28
	U24ArrayTypeU24256_t2038352957  ___U24U24fieldU2D28_2;
	// <PrivateImplementationDetails>/$ArrayType$1280 <PrivateImplementationDetails>::$$field-29
	U24ArrayTypeU241280_t628910058  ___U24U24fieldU2D29_3;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D26_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24U24fieldU2D26_0)); }
	inline U24ArrayTypeU248_t1957337328  get_U24U24fieldU2D26_0() const { return ___U24U24fieldU2D26_0; }
	inline U24ArrayTypeU248_t1957337328 * get_address_of_U24U24fieldU2D26_0() { return &___U24U24fieldU2D26_0; }
	inline void set_U24U24fieldU2D26_0(U24ArrayTypeU248_t1957337328  value)
	{
		___U24U24fieldU2D26_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D27_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24U24fieldU2D27_1)); }
	inline U24ArrayTypeU24256_t2038352957  get_U24U24fieldU2D27_1() const { return ___U24U24fieldU2D27_1; }
	inline U24ArrayTypeU24256_t2038352957 * get_address_of_U24U24fieldU2D27_1() { return &___U24U24fieldU2D27_1; }
	inline void set_U24U24fieldU2D27_1(U24ArrayTypeU24256_t2038352957  value)
	{
		___U24U24fieldU2D27_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D28_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24U24fieldU2D28_2)); }
	inline U24ArrayTypeU24256_t2038352957  get_U24U24fieldU2D28_2() const { return ___U24U24fieldU2D28_2; }
	inline U24ArrayTypeU24256_t2038352957 * get_address_of_U24U24fieldU2D28_2() { return &___U24U24fieldU2D28_2; }
	inline void set_U24U24fieldU2D28_2(U24ArrayTypeU24256_t2038352957  value)
	{
		___U24U24fieldU2D28_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D29_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24U24fieldU2D29_3)); }
	inline U24ArrayTypeU241280_t628910058  get_U24U24fieldU2D29_3() const { return ___U24U24fieldU2D29_3; }
	inline U24ArrayTypeU241280_t628910058 * get_address_of_U24U24fieldU2D29_3() { return &___U24U24fieldU2D29_3; }
	inline void set_U24U24fieldU2D29_3(U24ArrayTypeU241280_t628910058  value)
	{
		___U24U24fieldU2D29_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
