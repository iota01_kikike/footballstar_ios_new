﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// ScoutCellWideView
struct ScoutCellWideView_t573861516;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellWideView/<OnClickButton>c__AnonStorey1
struct  U3COnClickButtonU3Ec__AnonStorey1_t2143885460  : public Il2CppObject
{
public:
	// System.String ScoutCellWideView/<OnClickButton>c__AnonStorey1::key
	String_t* ___key_0;
	// ScoutCellWideView ScoutCellWideView/<OnClickButton>c__AnonStorey1::$this
	ScoutCellWideView_t573861516 * ___U24this_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey1_t2143885460, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey1_t2143885460, ___U24this_1)); }
	inline ScoutCellWideView_t573861516 * get_U24this_1() const { return ___U24this_1; }
	inline ScoutCellWideView_t573861516 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ScoutCellWideView_t573861516 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
