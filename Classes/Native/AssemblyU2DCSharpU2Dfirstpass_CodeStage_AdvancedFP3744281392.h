﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2350381201.h"

// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// CodeStage.AdvancedFPSCounter.WaitForSecondsUnscaled
struct WaitForSecondsUnscaled_t1880715370;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.CountersData.UpdatebleCounterData
struct  UpdatebleCounterData_t3744281392  : public BaseCounterData_t2350381201
{
public:
	// UnityEngine.Coroutine CodeStage.AdvancedFPSCounter.CountersData.UpdatebleCounterData::updateCoroutine
	Coroutine_t2299508840 * ___updateCoroutine_14;
	// CodeStage.AdvancedFPSCounter.WaitForSecondsUnscaled CodeStage.AdvancedFPSCounter.CountersData.UpdatebleCounterData::cachedWaitForSecondsUnscaled
	WaitForSecondsUnscaled_t1880715370 * ___cachedWaitForSecondsUnscaled_15;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.UpdatebleCounterData::updateInterval
	float ___updateInterval_16;

public:
	inline static int32_t get_offset_of_updateCoroutine_14() { return static_cast<int32_t>(offsetof(UpdatebleCounterData_t3744281392, ___updateCoroutine_14)); }
	inline Coroutine_t2299508840 * get_updateCoroutine_14() const { return ___updateCoroutine_14; }
	inline Coroutine_t2299508840 ** get_address_of_updateCoroutine_14() { return &___updateCoroutine_14; }
	inline void set_updateCoroutine_14(Coroutine_t2299508840 * value)
	{
		___updateCoroutine_14 = value;
		Il2CppCodeGenWriteBarrier(&___updateCoroutine_14, value);
	}

	inline static int32_t get_offset_of_cachedWaitForSecondsUnscaled_15() { return static_cast<int32_t>(offsetof(UpdatebleCounterData_t3744281392, ___cachedWaitForSecondsUnscaled_15)); }
	inline WaitForSecondsUnscaled_t1880715370 * get_cachedWaitForSecondsUnscaled_15() const { return ___cachedWaitForSecondsUnscaled_15; }
	inline WaitForSecondsUnscaled_t1880715370 ** get_address_of_cachedWaitForSecondsUnscaled_15() { return &___cachedWaitForSecondsUnscaled_15; }
	inline void set_cachedWaitForSecondsUnscaled_15(WaitForSecondsUnscaled_t1880715370 * value)
	{
		___cachedWaitForSecondsUnscaled_15 = value;
		Il2CppCodeGenWriteBarrier(&___cachedWaitForSecondsUnscaled_15, value);
	}

	inline static int32_t get_offset_of_updateInterval_16() { return static_cast<int32_t>(offsetof(UpdatebleCounterData_t3744281392, ___updateInterval_16)); }
	inline float get_updateInterval_16() const { return ___updateInterval_16; }
	inline float* get_address_of_updateInterval_16() { return &___updateInterval_16; }
	inline void set_updateInterval_16(float value)
	{
		___updateInterval_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
