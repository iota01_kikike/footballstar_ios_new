﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.ProductCatalog/<>c
struct U3CU3Ec_t651058539;
// System.Func`2<UnityEngine.Purchasing.ProductCatalogItem,System.Boolean>
struct Func_2_t1725700936;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalog/<>c
struct  U3CU3Ec_t651058539  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t651058539_StaticFields
{
public:
	// UnityEngine.Purchasing.ProductCatalog/<>c UnityEngine.Purchasing.ProductCatalog/<>c::<>9
	U3CU3Ec_t651058539 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Purchasing.ProductCatalogItem,System.Boolean> UnityEngine.Purchasing.ProductCatalog/<>c::<>9__8_0
	Func_2_t1725700936 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t651058539_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t651058539 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t651058539 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t651058539 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t651058539_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t1725700936 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t1725700936 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t1725700936 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__8_0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
