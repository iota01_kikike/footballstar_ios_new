﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Video_V3522318241.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Video_V1296310244.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger2779394790.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_2290843011.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_U792128005.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Logger_2696409916.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_Misc1418674630.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGam3863181352.h"
#include "AssemblyU2DCSharp_GooglePlayGames_OurUtils_PlayGam1370729796.h"
#include "AssemblyU2DCSharp_GooglePlayGames_PluginVersion4200204159.h"
#include "AssemblyU2DCSharp_NativeShare_Example211902429.h"
#include "AssemblyU2DCSharp_Assets_SimpleAndroidNotification3894720273.h"
#include "AssemblyU2DCSharp_Assets_SimpleAndroidNotification3050020448.h"
#include "AssemblyU2DCSharp_Assets_SimpleAndroidNotification2869813160.h"
#include "AssemblyU2DCSharp_Assets_SimpleAndroidNotification1024736401.h"
#include "AssemblyU2DCSharp_SA_Config3505216209.h"
#include "AssemblyU2DCSharp_SA_Ad_EditorUIController4080214878.h"
#include "AssemblyU2DCSharp_SA_EditorAd1410159287.h"
#include "AssemblyU2DCSharp_SA_EditorInApps4145855819.h"
#include "AssemblyU2DCSharp_SA_InApps_EditorUIController628772056.h"
#include "AssemblyU2DCSharp_SA_EditorNotificationType2256397131.h"
#include "AssemblyU2DCSharp_SA_EditorNotifications2173147004.h"
#include "AssemblyU2DCSharp_SA_Notifications_EditorUIControll749267495.h"
#include "AssemblyU2DCSharp_SA_EditorTesting2941573636.h"
#include "AssemblyU2DCSharp_SA_UIHightDependence2303649696.h"
#include "AssemblyU2DCSharp_SA_UIWidthDependence1527301732.h"
#include "AssemblyU2DCSharp_SA_EditorTestingSceneController2020162592.h"
#include "AssemblyU2DCSharp_SA_EaseType2212523945.h"
#include "AssemblyU2DCSharp_SA_TweenExtensions1623901338.h"
#include "AssemblyU2DCSharp_SA_TweenedGameobject3799170596.h"
#include "AssemblyU2DCSharp_SA_ValuesTween24537018.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween2821477864.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_Eas3881226392.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_Appl105260918.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_Ease613022473.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_Loo2225176159.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_Nam3185264378.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_Def4291532283.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_CRS2776801819.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_U3C1156727502.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_U3C3965683511.h"
#include "AssemblyU2DCSharp_StansAssets_Animation_iTween_U3C1634077204.h"
#include "AssemblyU2DCSharp_SA_ScreenShotMaker2081619307.h"
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_U3CSaveScreen1936109914.h"
#include "AssemblyU2DCSharp_SA_DataConverter2919071859.h"
#include "AssemblyU2DCSharp_SA_Gameobject361085552.h"
#include "AssemblyU2DCSharp_SA_PrefabAsyncLoader16537326.h"
#include "AssemblyU2DCSharp_SA_PrefabAsyncLoader_U3CLoadU3Ec2011498114.h"
#include "AssemblyU2DCSharp_SA_WWWTextureLoader129006864.h"
#include "AssemblyU2DCSharp_SA_WWWTextureLoader_U3CLoadCorou3413334227.h"
#include "AssemblyU2DCSharp_SA_ModulesInfo3047057538.h"
#include "AssemblyU2DCSharp_SA_IdFactory647695146.h"
#include "AssemblyU2DCSharp_SA_IdFactory_U3CU3Ec__AnonStorey3241106578.h"
#include "AssemblyU2DCSharp_Assets_UTime_Example2435568950.h"
#include "AssemblyU2DCSharp_Assets_UTime_UTime2203768558.h"
#include "AssemblyU2DCSharp_Assets_UTime_UTime_U3CGetUtcTime2452908945.h"
#include "AssemblyU2DCSharp_Assets_UTime_UTime_U3CHasConnect2139057901.h"
#include "AssemblyU2DCSharp_Assets_UTime_UTime_U3CDownloadU32940414941.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206766.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437122.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (VideoCapabilities_t3522318241), -1, sizeof(VideoCapabilities_t3522318241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2701[7] = 
{
	VideoCapabilities_t3522318241::get_offset_of_mIsCameraSupported_0(),
	VideoCapabilities_t3522318241::get_offset_of_mIsMicSupported_1(),
	VideoCapabilities_t3522318241::get_offset_of_mIsWriteStorageSupported_2(),
	VideoCapabilities_t3522318241::get_offset_of_mCaptureModesSupported_3(),
	VideoCapabilities_t3522318241::get_offset_of_mQualityLevelsSupported_4(),
	VideoCapabilities_t3522318241_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	VideoCapabilities_t3522318241_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (VideoCaptureState_t1296310244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[5] = 
{
	VideoCaptureState_t1296310244::get_offset_of_mIsCapturing_0(),
	VideoCaptureState_t1296310244::get_offset_of_mCaptureMode_1(),
	VideoCaptureState_t1296310244::get_offset_of_mQualityLevel_2(),
	VideoCaptureState_t1296310244::get_offset_of_mIsOverlayVisible_3(),
	VideoCaptureState_t1296310244::get_offset_of_mIsPaused_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (Logger_t2779394790), -1, sizeof(Logger_t2779394790_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2704[2] = 
{
	Logger_t2779394790_StaticFields::get_offset_of_debugLogEnabled_0(),
	Logger_t2779394790_StaticFields::get_offset_of_warningLogEnabled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (U3CdU3Ec__AnonStorey0_t2290843011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[1] = 
{
	U3CdU3Ec__AnonStorey0_t2290843011::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (U3CwU3Ec__AnonStorey1_t792128005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[1] = 
{
	U3CwU3Ec__AnonStorey1_t792128005::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (U3CeU3Ec__AnonStorey2_t2696409916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[1] = 
{
	U3CeU3Ec__AnonStorey2_t2696409916::get_offset_of_msg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (Misc_t1418674630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (PlayGamesHelperObject_t3863181352), -1, sizeof(PlayGamesHelperObject_t3863181352_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2709[7] = 
{
	PlayGamesHelperObject_t3863181352_StaticFields::get_offset_of_instance_2(),
	PlayGamesHelperObject_t3863181352_StaticFields::get_offset_of_sIsDummy_3(),
	PlayGamesHelperObject_t3863181352_StaticFields::get_offset_of_sQueue_4(),
	PlayGamesHelperObject_t3863181352::get_offset_of_localQueue_5(),
	PlayGamesHelperObject_t3863181352_StaticFields::get_offset_of_sQueueEmpty_6(),
	PlayGamesHelperObject_t3863181352_StaticFields::get_offset_of_sPauseCallbackList_7(),
	PlayGamesHelperObject_t3863181352_StaticFields::get_offset_of_sFocusCallbackList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (U3CRunCoroutineU3Ec__AnonStorey0_t1370729796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[1] = 
{
	U3CRunCoroutineU3Ec__AnonStorey0_t1370729796::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (PluginVersion_t4200204159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (NativeShare_Example_t211902429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	NativeShare_Example_t211902429::get_offset_of_m_texture_2(),
	NativeShare_Example_t211902429::get_offset_of_m_slider_3(),
	NativeShare_Example_t211902429::get_offset_of_eventSystem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (NotificationExample_t3894720273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (NotificationIcon_t3050020448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2714[7] = 
{
	NotificationIcon_t3050020448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (NotificationManager_t2869813160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (NotificationParams_t1024736401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[11] = 
{
	NotificationParams_t1024736401::get_offset_of_Id_0(),
	NotificationParams_t1024736401::get_offset_of_Delay_1(),
	NotificationParams_t1024736401::get_offset_of_Title_2(),
	NotificationParams_t1024736401::get_offset_of_Message_3(),
	NotificationParams_t1024736401::get_offset_of_Ticker_4(),
	NotificationParams_t1024736401::get_offset_of_Sound_5(),
	NotificationParams_t1024736401::get_offset_of_Vibrate_6(),
	NotificationParams_t1024736401::get_offset_of_Light_7(),
	NotificationParams_t1024736401::get_offset_of_SmallIcon_8(),
	NotificationParams_t1024736401::get_offset_of_SmallIconColor_9(),
	NotificationParams_t1024736401::get_offset_of_LargeIcon_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (SA_Config_t3505216209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (SA_Ad_EditorUIController_t4080214878), -1, sizeof(SA_Ad_EditorUIController_t4080214878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2718[12] = 
{
	SA_Ad_EditorUIController_t4080214878::get_offset_of_VideoPanel_2(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_InterstitialPanel_3(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_AppIcons_4(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_AppNames_5(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_OnCloseVideo_6(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_OnVideoLeftApplication_7(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_OnCloseInterstitial_8(),
	SA_Ad_EditorUIController_t4080214878::get_offset_of_OnInterstitialLeftApplication_9(),
	SA_Ad_EditorUIController_t4080214878_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	SA_Ad_EditorUIController_t4080214878_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
	SA_Ad_EditorUIController_t4080214878_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_12(),
	SA_Ad_EditorUIController_t4080214878_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (SA_EditorAd_t1410159287), -1, sizeof(SA_EditorAd_t1410159287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2719[14] = 
{
	0,
	0,
	SA_EditorAd_t1410159287::get_offset_of__IsInterstitialLoading_6(),
	SA_EditorAd_t1410159287::get_offset_of__IsVideoLoading_7(),
	SA_EditorAd_t1410159287::get_offset_of__IsInterstitialReady_8(),
	SA_EditorAd_t1410159287::get_offset_of__IsVideoReady_9(),
	SA_EditorAd_t1410159287::get_offset_of__FillRate_10(),
	SA_EditorAd_t1410159287_StaticFields::get_offset_of_OnInterstitialFinished_11(),
	SA_EditorAd_t1410159287_StaticFields::get_offset_of_OnInterstitialLoadComplete_12(),
	SA_EditorAd_t1410159287_StaticFields::get_offset_of_OnInterstitialLeftApplication_13(),
	SA_EditorAd_t1410159287_StaticFields::get_offset_of_OnVideoFinished_14(),
	SA_EditorAd_t1410159287_StaticFields::get_offset_of_OnVideoLoadComplete_15(),
	SA_EditorAd_t1410159287_StaticFields::get_offset_of_OnVideoLeftApplication_16(),
	SA_EditorAd_t1410159287::get_offset_of__EditorUI_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (SA_EditorInApps_t4145855819), -1, sizeof(SA_EditorInApps_t4145855819_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2720[1] = 
{
	SA_EditorInApps_t4145855819_StaticFields::get_offset_of__EditorUI_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (SA_InApps_EditorUIController_t628772056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[9] = 
{
	SA_InApps_EditorUIController_t628772056::get_offset_of_Title_2(),
	SA_InApps_EditorUIController_t628772056::get_offset_of_Describtion_3(),
	SA_InApps_EditorUIController_t628772056::get_offset_of_Price_4(),
	SA_InApps_EditorUIController_t628772056::get_offset_of_IsSuccsesPurchase_5(),
	SA_InApps_EditorUIController_t628772056::get_offset_of_Fader_6(),
	SA_InApps_EditorUIController_t628772056::get_offset_of_HightDependence_7(),
	SA_InApps_EditorUIController_t628772056::get_offset_of__CurrentTween_8(),
	SA_InApps_EditorUIController_t628772056::get_offset_of__FaderTween_9(),
	SA_InApps_EditorUIController_t628772056::get_offset_of__OnComplete_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (SA_EditorNotificationType_t2256397131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2722[5] = 
{
	SA_EditorNotificationType_t2256397131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (SA_EditorNotifications_t2173147004), -1, sizeof(SA_EditorNotifications_t2173147004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2723[1] = 
{
	SA_EditorNotifications_t2173147004_StaticFields::get_offset_of__EditorUI_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (SA_Notifications_EditorUIController_t749267495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[5] = 
{
	SA_Notifications_EditorUIController_t749267495::get_offset_of_Title_2(),
	SA_Notifications_EditorUIController_t749267495::get_offset_of_Message_3(),
	SA_Notifications_EditorUIController_t749267495::get_offset_of_Icons_4(),
	SA_Notifications_EditorUIController_t749267495::get_offset_of_HightDependence_5(),
	SA_Notifications_EditorUIController_t749267495::get_offset_of__CurrentTween_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (SA_EditorTesting_t2941573636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (SA_UIHightDependence_t2303649696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[5] = 
{
	SA_UIHightDependence_t2303649696::get_offset_of__rect_2(),
	SA_UIHightDependence_t2303649696::get_offset_of_KeepRatioInEdiotr_3(),
	SA_UIHightDependence_t2303649696::get_offset_of_CaclulcateOnlyOntStart_4(),
	SA_UIHightDependence_t2303649696::get_offset_of_InitialRect_5(),
	SA_UIHightDependence_t2303649696::get_offset_of_InitialScreen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (SA_UIWidthDependence_t1527301732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[5] = 
{
	SA_UIWidthDependence_t1527301732::get_offset_of__rect_2(),
	SA_UIWidthDependence_t1527301732::get_offset_of_KeepRatioInEdiotr_3(),
	SA_UIWidthDependence_t1527301732::get_offset_of_CaclulcateOnlyOntStart_4(),
	SA_UIWidthDependence_t1527301732::get_offset_of_InitialRect_5(),
	SA_UIWidthDependence_t1527301732::get_offset_of_InitialScreen_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (SA_EditorTestingSceneController_t2020162592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[2] = 
{
	SA_EditorTestingSceneController_t2020162592::get_offset_of_ShowInterstitial_Button_2(),
	SA_EditorTestingSceneController_t2020162592::get_offset_of_ShowInterstitial_Video_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (SA_EaseType_t2212523945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2729[34] = 
{
	SA_EaseType_t2212523945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (SA_TweenExtensions_t1623901338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (SA_TweenedGameobject_t3799170596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (SA_ValuesTween_t24537018), -1, sizeof(SA_ValuesTween_t24537018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2732[9] = 
{
	SA_ValuesTween_t24537018::get_offset_of_OnComplete_2(),
	SA_ValuesTween_t24537018::get_offset_of_OnValueChanged_3(),
	SA_ValuesTween_t24537018::get_offset_of_OnVectorValueChanged_4(),
	SA_ValuesTween_t24537018::get_offset_of_DestoryGameObjectOnComplete_5(),
	SA_ValuesTween_t24537018::get_offset_of_FinalFloatValue_6(),
	SA_ValuesTween_t24537018::get_offset_of_FinalVectorValue_7(),
	SA_ValuesTween_t24537018_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
	SA_ValuesTween_t24537018_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_9(),
	SA_ValuesTween_t24537018_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (iTween_t2821477864), -1, sizeof(iTween_t2821477864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2733[39] = 
{
	iTween_t2821477864_StaticFields::get_offset_of_tweens_2(),
	iTween_t2821477864_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t2821477864::get_offset_of_id_4(),
	iTween_t2821477864::get_offset_of_type_5(),
	iTween_t2821477864::get_offset_of_method_6(),
	iTween_t2821477864::get_offset_of_easeType_7(),
	iTween_t2821477864::get_offset_of_time_8(),
	iTween_t2821477864::get_offset_of_delay_9(),
	iTween_t2821477864::get_offset_of_loopType_10(),
	iTween_t2821477864::get_offset_of_isRunning_11(),
	iTween_t2821477864::get_offset_of_isPaused_12(),
	iTween_t2821477864::get_offset_of__name_13(),
	iTween_t2821477864::get_offset_of_runningTime_14(),
	iTween_t2821477864::get_offset_of_percentage_15(),
	iTween_t2821477864::get_offset_of_delayStarted_16(),
	iTween_t2821477864::get_offset_of_kinematic_17(),
	iTween_t2821477864::get_offset_of_isLocal_18(),
	iTween_t2821477864::get_offset_of_loop_19(),
	iTween_t2821477864::get_offset_of_reverse_20(),
	iTween_t2821477864::get_offset_of_wasPaused_21(),
	iTween_t2821477864::get_offset_of_physics_22(),
	iTween_t2821477864::get_offset_of_tweenArguments_23(),
	iTween_t2821477864::get_offset_of_space_24(),
	iTween_t2821477864::get_offset_of_ease_25(),
	iTween_t2821477864::get_offset_of_apply_26(),
	iTween_t2821477864::get_offset_of_audioSource_27(),
	iTween_t2821477864::get_offset_of_vector3s_28(),
	iTween_t2821477864::get_offset_of_vector2s_29(),
	iTween_t2821477864::get_offset_of_colors_30(),
	iTween_t2821477864::get_offset_of_floats_31(),
	iTween_t2821477864::get_offset_of_rects_32(),
	iTween_t2821477864::get_offset_of_path_33(),
	iTween_t2821477864::get_offset_of_preUpdate_34(),
	iTween_t2821477864::get_offset_of_postUpdate_35(),
	iTween_t2821477864::get_offset_of_namedcolorvalue_36(),
	iTween_t2821477864::get_offset_of_lastRealTime_37(),
	iTween_t2821477864::get_offset_of_useRealTime_38(),
	iTween_t2821477864::get_offset_of_thisTransform_39(),
	iTween_t2821477864_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (EasingFunction_t3881226392), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (ApplyTween_t105260918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (EaseType_t613022473)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[34] = 
{
	EaseType_t613022473::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (LoopType_t2225176159)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2737[4] = 
{
	LoopType_t2225176159::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (NamedValueColor_t3185264378)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	NamedValueColor_t3185264378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (Defaults_t4291532283), -1, sizeof(Defaults_t4291532283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2739[16] = 
{
	Defaults_t4291532283_StaticFields::get_offset_of_time_0(),
	Defaults_t4291532283_StaticFields::get_offset_of_delay_1(),
	Defaults_t4291532283_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4291532283_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4291532283_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4291532283_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4291532283_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4291532283_StaticFields::get_offset_of_space_7(),
	Defaults_t4291532283_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4291532283_StaticFields::get_offset_of_color_9(),
	Defaults_t4291532283_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4291532283_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4291532283_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4291532283_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4291532283_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4291532283_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (CRSpline_t2776801819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[1] = 
{
	CRSpline_t2776801819::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t1156727502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t1156727502::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t1156727502::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t1156727502::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t1156727502::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t3965683511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t3965683511::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t3965683511::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t3965683511::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t3965683511::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (U3CStartU3Ec__Iterator2_t1634077204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[4] = 
{
	U3CStartU3Ec__Iterator2_t1634077204::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t1634077204::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t1634077204::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t1634077204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (SA_ScreenShotMaker_t2081619307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[1] = 
{
	SA_ScreenShotMaker_t2081619307::get_offset_of_OnScreenshotReady_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U3CSaveScreenshotU3Ec__Iterator0_t1936109914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[7] = 
{
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U3CwidthU3E__0_0(),
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U3CheightU3E__0_1(),
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U3CtexU3E__0_2(),
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U24this_3(),
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U24current_4(),
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U24disposing_5(),
	U3CSaveScreenshotU3Ec__Iterator0_t1936109914::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (SA_DataConverter_t2919071859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (SA_Gameobject_t361085552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (SA_PrefabAsyncLoader_t16537326), -1, sizeof(SA_PrefabAsyncLoader_t16537326_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2748[3] = 
{
	SA_PrefabAsyncLoader_t16537326::get_offset_of_PrefabPath_2(),
	SA_PrefabAsyncLoader_t16537326::get_offset_of_ObjectLoadedAction_3(),
	SA_PrefabAsyncLoader_t16537326_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (U3CLoadU3Ec__Iterator0_t2011498114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[6] = 
{
	U3CLoadU3Ec__Iterator0_t2011498114::get_offset_of_U3CrequestU3E__0_0(),
	U3CLoadU3Ec__Iterator0_t2011498114::get_offset_of_U3CloadedObjectU3E__0_1(),
	U3CLoadU3Ec__Iterator0_t2011498114::get_offset_of_U24this_2(),
	U3CLoadU3Ec__Iterator0_t2011498114::get_offset_of_U24current_3(),
	U3CLoadU3Ec__Iterator0_t2011498114::get_offset_of_U24disposing_4(),
	U3CLoadU3Ec__Iterator0_t2011498114::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (SA_WWWTextureLoader_t129006864), -1, sizeof(SA_WWWTextureLoader_t129006864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2750[4] = 
{
	SA_WWWTextureLoader_t129006864_StaticFields::get_offset_of_LocalCache_2(),
	SA_WWWTextureLoader_t129006864::get_offset_of__url_3(),
	SA_WWWTextureLoader_t129006864::get_offset_of_OnLoad_4(),
	SA_WWWTextureLoader_t129006864_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (U3CLoadCoroutinU3Ec__Iterator0_t3413334227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[5] = 
{
	U3CLoadCoroutinU3Ec__Iterator0_t3413334227::get_offset_of_U3CwwwU3E__0_0(),
	U3CLoadCoroutinU3Ec__Iterator0_t3413334227::get_offset_of_U24this_1(),
	U3CLoadCoroutinU3Ec__Iterator0_t3413334227::get_offset_of_U24current_2(),
	U3CLoadCoroutinU3Ec__Iterator0_t3413334227::get_offset_of_U24disposing_3(),
	U3CLoadCoroutinU3Ec__Iterator0_t3413334227::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (SA_ModulesInfo_t3047057538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (SA_IdFactory_t647695146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (U3CU3Ec__AnonStorey0_t3241106578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[1] = 
{
	U3CU3Ec__AnonStorey0_t3241106578::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (Example_t2435568950), -1, sizeof(Example_t2435568950_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2757[2] = 
{
	Example_t2435568950::get_offset_of_Time_2(),
	Example_t2435568950_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (UTime_t2203768558), -1, sizeof(UTime_t2203768558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2758[2] = 
{
	0,
	UTime_t2203768558_StaticFields::get_offset_of__instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (U3CGetUtcTimeAsyncU3Ec__AnonStorey1_t2452908945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	U3CGetUtcTimeAsyncU3Ec__AnonStorey1_t2452908945::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (U3CHasConnectionU3Ec__AnonStorey2_t2139057901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[1] = 
{
	U3CHasConnectionU3Ec__AnonStorey2_t2139057901::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (U3CDownloadU3Ec__Iterator0_t2940414941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[6] = 
{
	U3CDownloadU3Ec__Iterator0_t2940414941::get_offset_of_url_0(),
	U3CDownloadU3Ec__Iterator0_t2940414941::get_offset_of_U3CwwwU3E__0_1(),
	U3CDownloadU3Ec__Iterator0_t2940414941::get_offset_of_callback_2(),
	U3CDownloadU3Ec__Iterator0_t2940414941::get_offset_of_U24current_3(),
	U3CDownloadU3Ec__Iterator0_t2940414941::get_offset_of_U24disposing_4(),
	U3CDownloadU3Ec__Iterator0_t2940414941::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305147), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2762[10] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D03A360BA72DAB105AEDD6431251E3FD4C4F56D1B_0(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2DF46C9E8AA24FD4329A31F3F7935A3171DC93AE9B_1(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D47BB8FD394E24F402AEB3EDED86A13EFC400E08D_2(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D836DCF337B9E5BB1BBE7165E3BE05A7F291525BC_3(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D8459FE258058867DFF3EEB589888DC869A211048_4(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2DE7ECEE139F2F7FD5B5A531E3A928FD283C6186D6_5(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D0BE6EBDD00CE178BCBDA44BAEDB0C0FDFFF8AEC6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2DE64538585CC2B3A3457E2443F76AF538A5EA69FC_7(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D3EF483E00E87F725D5CB4FA26FC09476379D4E8F_8(),
	U3CPrivateImplementationDetailsU3E_t1486305147_StaticFields::get_offset_of_U24fieldU2D5FADCD534E1A38F6F0314587149C904967B24AFE_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (U24ArrayTypeU3D48_t2375206766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U24ArrayTypeU3D80_t2731437122)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D80_t2731437122 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
