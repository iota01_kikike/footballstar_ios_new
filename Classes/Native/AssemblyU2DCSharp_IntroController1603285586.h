﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntroController
struct  IntroController_t1603285586  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject IntroController::TitleView
	GameObject_t1756533147 * ___TitleView_2;
	// UnityEngine.GameObject IntroController::TempAlert
	GameObject_t1756533147 * ___TempAlert_3;
	// System.String IntroController::LAST_VER
	String_t* ___LAST_VER_4;

public:
	inline static int32_t get_offset_of_TitleView_2() { return static_cast<int32_t>(offsetof(IntroController_t1603285586, ___TitleView_2)); }
	inline GameObject_t1756533147 * get_TitleView_2() const { return ___TitleView_2; }
	inline GameObject_t1756533147 ** get_address_of_TitleView_2() { return &___TitleView_2; }
	inline void set_TitleView_2(GameObject_t1756533147 * value)
	{
		___TitleView_2 = value;
		Il2CppCodeGenWriteBarrier(&___TitleView_2, value);
	}

	inline static int32_t get_offset_of_TempAlert_3() { return static_cast<int32_t>(offsetof(IntroController_t1603285586, ___TempAlert_3)); }
	inline GameObject_t1756533147 * get_TempAlert_3() const { return ___TempAlert_3; }
	inline GameObject_t1756533147 ** get_address_of_TempAlert_3() { return &___TempAlert_3; }
	inline void set_TempAlert_3(GameObject_t1756533147 * value)
	{
		___TempAlert_3 = value;
		Il2CppCodeGenWriteBarrier(&___TempAlert_3, value);
	}

	inline static int32_t get_offset_of_LAST_VER_4() { return static_cast<int32_t>(offsetof(IntroController_t1603285586, ___LAST_VER_4)); }
	inline String_t* get_LAST_VER_4() const { return ___LAST_VER_4; }
	inline String_t** get_address_of_LAST_VER_4() { return &___LAST_VER_4; }
	inline void set_LAST_VER_4(String_t* value)
	{
		___LAST_VER_4 = value;
		Il2CppCodeGenWriteBarrier(&___LAST_VER_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
