﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,SA.GoogleDoc.SpreadsheetData>
struct Dictionary_2_t2482502078;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.Reader
struct  Reader_t1391219339  : public Il2CppObject
{
public:

public:
};

struct Reader_t1391219339_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SA.GoogleDoc.SpreadsheetData> SA.GoogleDoc.Reader::sheets
	Dictionary_2_t2482502078 * ___sheets_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> SA.GoogleDoc.Reader::DocsCacheData
	Dictionary_2_t3943999495 * ___DocsCacheData_1;

public:
	inline static int32_t get_offset_of_sheets_0() { return static_cast<int32_t>(offsetof(Reader_t1391219339_StaticFields, ___sheets_0)); }
	inline Dictionary_2_t2482502078 * get_sheets_0() const { return ___sheets_0; }
	inline Dictionary_2_t2482502078 ** get_address_of_sheets_0() { return &___sheets_0; }
	inline void set_sheets_0(Dictionary_2_t2482502078 * value)
	{
		___sheets_0 = value;
		Il2CppCodeGenWriteBarrier(&___sheets_0, value);
	}

	inline static int32_t get_offset_of_DocsCacheData_1() { return static_cast<int32_t>(offsetof(Reader_t1391219339_StaticFields, ___DocsCacheData_1)); }
	inline Dictionary_2_t3943999495 * get_DocsCacheData_1() const { return ___DocsCacheData_1; }
	inline Dictionary_2_t3943999495 ** get_address_of_DocsCacheData_1() { return &___DocsCacheData_1; }
	inline void set_DocsCacheData_1(Dictionary_2_t3943999495 * value)
	{
		___DocsCacheData_1 = value;
		Il2CppCodeGenWriteBarrier(&___DocsCacheData_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
