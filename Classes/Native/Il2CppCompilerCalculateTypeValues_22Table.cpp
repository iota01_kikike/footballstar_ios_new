﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_AmazonAppStoreStoreE1518886395.h"
#include "Stores_UnityEngine_Purchasing_FakeAmazonExtensions2261777661.h"
#include "Stores_UnityEngine_Purchasing_FakeMoolahConfigurat1681533527.h"
#include "Stores_UnityEngine_Purchasing_FakeMoolahExtensions1432169715.h"
#include "Stores_UnityEngine_Purchasing_CloudMoolahMode206291964.h"
#include "Stores_UnityEngine_Purchasing_ValidateReceiptState4359597.h"
#include "Stores_UnityEngine_Purchasing_RestoreTransactionID2487303652.h"
#include "Stores_UnityEngine_Purchasing_TradeSeqState1177537792.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl4206626141.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CVa631815662.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CU3872126098.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CR1220668507.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CS2291090002.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CRe536115202.h"
#include "Stores_UnityEngine_Purchasing_MoolahStoreImpl_U3CV1085247229.h"
#include "Stores_UnityEngine_Purchasing_PayMethod1558319955.h"
#include "Stores_UnityEngine_Purchasing_GooglePlayAndroidJav4038061283.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266.h"
#include "Stores_UnityEngine_Purchasing_FakeSamsungAppsExten1522853249.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsMode2214306743.h"
#include "Stores_UnityEngine_Purchasing_SamsungAppsStoreExte3441062041.h"
#include "Stores_UnityEngine_Purchasing_FakeUnityChannelConf4230617165.h"
#include "Stores_UnityEngine_Purchasing_FakeUnityChannelExte2132025117.h"
#include "Stores_UnityEngine_Purchasing_XiaomiPriceTiers2494450221.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelBindings2880355556.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelBindings1107960729.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl1327714682.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl_U3CU3Ec505099.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl_U3C2729388454.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelPurchase1964142665.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CU33474430932.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CU3E468385856.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CU31888143949.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CU31888144345.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "Stores_UnityEngine_Purchasing_FakeMicrosoftExtensi3351923809.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore_U3CU3Ec4236842726.h"
#include "Stores_UnityEngine_Purchasing_TizenStoreImpl274247241.h"
#include "Stores_UnityEngine_Purchasing_FakeTizenStoreConfig3055550456.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl1362794587.h"
#include "Stores_UnityEngine_Purchasing_FacebookStoreImpl_U3C294810501.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CU3Ec__D2968753776.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CU3Ec__D3251078778.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_U3CU3Ec3471345421.h"
#include "Stores_UnityEngine_Purchasing_AsyncWebUtil1370427196.h"
#include "Stores_UnityEngine_Purchasing_AsyncWebUtil_U3CDoIn1587761224.h"
#include "Stores_UnityEngine_Purchasing_AsyncWebUtil_U3CProc1496112689.h"
#include "Stores_UnityEngine_Purchasing_CloudCatalogImpl569898932.h"
#include "Stores_UnityEngine_Purchasing_CloudCatalogImpl_U3C2096495139.h"
#include "Stores_UnityEngine_Purchasing_CloudCatalogImpl_U3C4238608033.h"
#include "Stores_UnityEngine_Purchasing_EventDestType4138688180.h"
#include "Stores_UnityEngine_Purchasing_EventQueue560163637.h"
#include "Stores_UnityEngine_Purchasing_EventQueue_U3CU3Ec__D850767941.h"
#include "Stores_UnityEngine_Purchasing_EventQueue_U3CU3Ec1387771964.h"
#include "Stores_UnityEngine_Purchasing_FakeManagedStoreConfi797709047.h"
#include "Stores_UnityEngine_Purchasing_FakeManagedStoreExte3092625943.h"
#include "Stores_UnityEngine_Purchasing_FakeTransactionHisto2213685253.h"
#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"
#include "Stores_UnityEngine_Purchasing_NativeStoreProvider4107132677.h"
#include "Stores_UnityEngine_Purchasing_Price1853024949.h"
#include "Stores_UnityEngine_Purchasing_StoreID471452324.h"
#include "Stores_UnityEngine_Purchasing_TranslationLocale3543162129.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc1525635964.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc3617267505.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogPayout96590568.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogPayout3881497528.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (AmazonAppStoreStoreExtensions_t1518886395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[1] = 
{
	AmazonAppStoreStoreExtensions_t1518886395::get_offset_of_android_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (FakeAmazonExtensions_t2261777661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (FakeMoolahConfiguration_t1681533527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[2] = 
{
	FakeMoolahConfiguration_t1681533527::get_offset_of_m_appKey_0(),
	FakeMoolahConfiguration_t1681533527::get_offset_of_m_hashKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (FakeMoolahExtensions_t1432169715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (CloudMoolahMode_t206291964)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2206[4] = 
{
	CloudMoolahMode_t206291964::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (ValidateReceiptState_t4359597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2208[4] = 
{
	ValidateReceiptState_t4359597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (RestoreTransactionIDState_t2487303652)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[5] = 
{
	RestoreTransactionIDState_t2487303652::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (TradeSeqState_t1177537792)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[7] = 
{
	TradeSeqState_t1177537792::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (MoolahStoreImpl_t4206626141), -1, sizeof(MoolahStoreImpl_t4206626141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2212[14] = 
{
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_pollingPath_2(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestAuthCodePath_3(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestRestoreTransactionUrl_4(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestValidateReceiptUrl_5(),
	MoolahStoreImpl_t4206626141_StaticFields::get_offset_of_requestProductValidateUrl_6(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_callback_7(),
	MoolahStoreImpl_t4206626141::get_offset_of_isNeedPolling_8(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_CurrentStoreProductID_9(),
	MoolahStoreImpl_t4206626141::get_offset_of_isRequestAuthCodeing_10(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_appKey_11(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_hashKey_12(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_notificationURL_13(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_mode_14(),
	MoolahStoreImpl_t4206626141::get_offset_of_m_CustomerID_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (U3CVaildateProductU3Ed__13_t631815662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[9] = 
{
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_U3CU3E1__state_0(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_U3CU3E2__current_1(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_appkey_2(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_productInfo_3(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_result_4(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_U3CU3E4__this_5(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_U3CsignU3E5__1_6(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_U3CwfU3E5__2_7(),
	U3CVaildateProductU3Ed__13_t631815662::get_offset_of_U3CwU3E5__3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (U3CU3Ec__DisplayClass18_0_t3872126098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[3] = 
{
	U3CU3Ec__DisplayClass18_0_t3872126098::get_offset_of_purchaseSucceed_0(),
	U3CU3Ec__DisplayClass18_0_t3872126098::get_offset_of_purchaseFailed_1(),
	U3CU3Ec__DisplayClass18_0_t3872126098::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (U3CRequestAuthCodeU3Ed__22_t1220668507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[13] = 
{
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CU3E1__state_0(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CU3E2__current_1(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_wf_2(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_productID_3(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_transactionId_4(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_succeed_5(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_failed_6(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CU3E4__this_7(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CwU3E5__1_8(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CauthCodeResultU3E5__2_9(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CauthCodeValuesU3E5__3_10(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CauthCodeU3E5__4_11(),
	U3CRequestAuthCodeU3Ed__22_t1220668507::get_offset_of_U3CpaymentURLU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (U3CStartPurchasePollingU3Ed__23_t2291090002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[21] = 
{
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CU3E1__state_0(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CU3E2__current_1(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_authGlobal_2(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_transactionId_3(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_purchaseSucceed_4(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_purchaseFailed_5(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CU3E4__this_6(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CorderSuccessU3E5__1_7(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CsignstrU3E5__2_8(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CsignU3E5__3_9(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CparamU3E5__4_10(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CurlU3E5__5_11(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CpollingstrU3E5__6_12(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CjsonPollingObjectsU3E5__7_13(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CcodeU3E5__8_14(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CpollingValuesU3E5__9_15(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CtradeSeqU3E5__10_16(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CtradeStateU3E5__11_17(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CproductIdU3E5__12_18(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CMsgU3E5__13_19(),
	U3CStartPurchasePollingU3Ed__23_t2291090002::get_offset_of_U3CreceiptU3E5__14_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (U3CRestoreTransactionIDProcessU3Ed__45_t536115202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[20] = 
{
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3E1__state_0(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3E2__current_1(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_result_2(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3E4__this_3(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CcustomIDU3E5__1_4(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CwfU3E5__2_5(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CnowU3E5__3_6(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CendDateU3E5__4_7(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CupperWeekU3E5__5_8(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CstartDateU3E5__6_9(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CsignU3E5__7_10(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CwU3E5__8_11(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CrestoreObjectsU3E5__9_12(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CcodeU3E5__10_13(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CrestoreValuesU3E5__11_14(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CU3Es__12_15(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CrestoreObjectElemU3E5__13_16(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CproductIdU3E5__14_17(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CtradeSeqU3E5__15_18(),
	U3CRestoreTransactionIDProcessU3Ed__45_t536115202::get_offset_of_U3CreceiptU3E5__16_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CValidateReceiptProcessU3Ed__47_t1085247229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[13] = 
{
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CU3E1__state_0(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CU3E2__current_1(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_transactionId_2(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_receipt_3(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_result_4(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CU3E4__this_5(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CtempJsonU3E5__1_6(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CwfU3E5__2_7(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CsignU3E5__3_8(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CwU3E5__4_9(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CjsonObjectsU3E5__5_10(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CcodeU3E5__6_11(),
	U3CValidateReceiptProcessU3Ed__47_t1085247229::get_offset_of_U3CmsgU3E5__7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (PayMethod_t1558319955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (GooglePlayAndroidJavaStore_t4038061283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	GooglePlayAndroidJavaStore_t4038061283::get_offset_of_m_Util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (FakeGooglePlayConfiguration_t737012266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (FakeSamsungAppsExtensions_t1522853249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (SamsungAppsMode_t2214306743)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	SamsungAppsMode_t2214306743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (SamsungAppsStoreExtensions_t3441062041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[2] = 
{
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_RestoreCallback_1(),
	SamsungAppsStoreExtensions_t3441062041::get_offset_of_m_Java_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (FakeUnityChannelConfiguration_t4230617165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	FakeUnityChannelConfiguration_t4230617165::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (FakeUnityChannelExtensions_t2132025117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (XiaomiPriceTiers_t2494450221), -1, sizeof(XiaomiPriceTiers_t2494450221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	XiaomiPriceTiers_t2494450221_StaticFields::get_offset_of_XiaomiPriceTierPrices_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (UnityChannelBindings_t2880355556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	UnityChannelBindings_t2880355556::get_offset_of_m_PurchaseCallback_0(),
	UnityChannelBindings_t2880355556::get_offset_of_m_PurchaseGuid_1(),
	UnityChannelBindings_t2880355556::get_offset_of_m_ValidateCallbacks_2(),
	UnityChannelBindings_t2880355556::get_offset_of_m_PurchaseConfirmCallbacks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3CU3Ec__DisplayClass16_0_t1107960729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[2] = 
{
	U3CU3Ec__DisplayClass16_0_t1107960729::get_offset_of_transactionId_0(),
	U3CU3Ec__DisplayClass16_0_t1107960729::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (UnityChannelImpl_t1327714682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[3] = 
{
	UnityChannelImpl_t1327714682::get_offset_of_m_Bindings_22(),
	UnityChannelImpl_t1327714682::get_offset_of_m_LastPurchaseError_23(),
	UnityChannelImpl_t1327714682::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (U3CU3Ec__DisplayClass7_0_t505099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[2] = 
{
	U3CU3Ec__DisplayClass7_0_t505099::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass7_0_t505099::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (U3CU3Ec__DisplayClass7_1_t2729388454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[3] = 
{
	U3CU3Ec__DisplayClass7_1_t2729388454::get_offset_of_dic_0(),
	U3CU3Ec__DisplayClass7_1_t2729388454::get_offset_of_transactionId_1(),
	U3CU3Ec__DisplayClass7_1_t2729388454::get_offset_of_CSU24U3CU3E8__locals1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (UnityChannelPurchaseReceipt_t1964142665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[3] = 
{
	UnityChannelPurchaseReceipt_t1964142665::get_offset_of_storeSpecificId_0(),
	UnityChannelPurchaseReceipt_t1964142665::get_offset_of_transactionId_1(),
	UnityChannelPurchaseReceipt_t1964142665::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (AppleStoreImpl_t1301617341), -1, sizeof(AppleStoreImpl_t1301617341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2241[9] = 
{
	AppleStoreImpl_t1301617341::get_offset_of_m_DeferredCallback_22(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptError_23(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptSuccess_24(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RestoreCallback_25(),
	AppleStoreImpl_t1301617341::get_offset_of_m_PromotionalPurchaseCallback_26(),
	AppleStoreImpl_t1301617341::get_offset_of_m_Native_27(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_util_28(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_instance_29(),
	AppleStoreImpl_t1301617341::get_offset_of_products_json_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (U3CU3Ec__DisplayClass23_0_t3474430932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[1] = 
{
	U3CU3Ec__DisplayClass23_0_t3474430932::get_offset_of_productDescription_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (U3CU3Ec_t468385856), -1, sizeof(U3CU3Ec_t468385856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	U3CU3Ec_t468385856_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t468385856_StaticFields::get_offset_of_U3CU3E9__23_1_1(),
	U3CU3Ec_t468385856_StaticFields::get_offset_of_U3CU3E9__39_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (U3CU3Ec__DisplayClass35_0_t1888143949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[4] = 
{
	U3CU3Ec__DisplayClass35_0_t1888143949::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass35_0_t1888143949::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass35_0_t1888143949::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass35_0_t1888143949::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CU3Ec__DisplayClass39_0_t1888144345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[1] = 
{
	U3CU3Ec__DisplayClass39_0_t1888144345::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (FakeAppleConfiguation_t4052738437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (FakeAppleExtensions_t4039399289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (AppStore_t379104228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2250[13] = 
{
	AppStore_t379104228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (FakeMicrosoftExtensions_t3351923809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (WinRTStore_t36043095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[5] = 
{
	WinRTStore_t36043095::get_offset_of_win8_0(),
	WinRTStore_t36043095::get_offset_of_callback_1(),
	WinRTStore_t36043095::get_offset_of_util_2(),
	WinRTStore_t36043095::get_offset_of_logger_3(),
	WinRTStore_t36043095::get_offset_of_m_CanReceivePurchases_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (U3CU3Ec_t4236842726), -1, sizeof(U3CU3Ec_t4236842726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2255[3] = 
{
	U3CU3Ec_t4236842726_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4236842726_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t4236842726_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (TizenStoreImpl_t274247241), -1, sizeof(TizenStoreImpl_t274247241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[2] = 
{
	TizenStoreImpl_t274247241_StaticFields::get_offset_of_instance_22(),
	TizenStoreImpl_t274247241::get_offset_of_m_Native_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (FakeTizenStoreConfiguration_t3055550456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (FacebookStoreImpl_t1362794587), -1, sizeof(FacebookStoreImpl_t1362794587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2259[3] = 
{
	FacebookStoreImpl_t1362794587::get_offset_of_m_Native_22(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_util_23(),
	FacebookStoreImpl_t1362794587_StaticFields::get_offset_of_instance_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CU3Ec__DisplayClass6_0_t294810501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[4] = 
{
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass6_0_t294810501::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (FakeStoreUIMode_t2321492887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[4] = 
{
	FakeStoreUIMode_t2321492887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (FakeStore_t3882981564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[5] = 
{
	FakeStore_t3882981564::get_offset_of_m_Biller_22(),
	FakeStore_t3882981564::get_offset_of_m_PurchasedProducts_23(),
	FakeStore_t3882981564::get_offset_of_purchaseCalled_24(),
	FakeStore_t3882981564::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_25(),
	FakeStore_t3882981564::get_offset_of_UIMode_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (DialogType_t1733969544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	DialogType_t1733969544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (U3CU3Ec__DisplayClass13_0_t2968753776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[2] = 
{
	U3CU3Ec__DisplayClass13_0_t2968753776::get_offset_of_products_0(),
	U3CU3Ec__DisplayClass13_0_t2968753776::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (U3CU3Ec__DisplayClass15_0_t3251078778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[2] = 
{
	U3CU3Ec__DisplayClass15_0_t3251078778::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass15_0_t3251078778::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (UIFakeStore_t3684252124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[6] = 
{
	UIFakeStore_t3684252124::get_offset_of_m_CurrentDialog_27(),
	UIFakeStore_t3684252124::get_offset_of_m_LastSelectedDropdownIndex_28(),
	UIFakeStore_t3684252124::get_offset_of_UIFakeStoreCanvasPrefab_29(),
	UIFakeStore_t3684252124::get_offset_of_m_Canvas_30(),
	UIFakeStore_t3684252124::get_offset_of_m_EventSystem_31(),
	UIFakeStore_t3684252124::get_offset_of_m_ParentGameObjectPath_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (DialogRequest_t2092195449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[5] = 
{
	DialogRequest_t2092195449::get_offset_of_QueryText_0(),
	DialogRequest_t2092195449::get_offset_of_OkayButtonText_1(),
	DialogRequest_t2092195449::get_offset_of_CancelButtonText_2(),
	DialogRequest_t2092195449::get_offset_of_Options_3(),
	DialogRequest_t2092195449::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (LifecycleNotifier_t1057582876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[1] = 
{
	LifecycleNotifier_t1057582876::get_offset_of_OnDestroyCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (U3CU3Ec_t3471345421), -1, sizeof(U3CU3Ec_t3471345421_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2271[2] = 
{
	U3CU3Ec_t3471345421_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3471345421_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (AsyncWebUtil_t1370427196), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (U3CDoInvokeU3Ed__3_t1587761224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[5] = 
{
	U3CDoInvokeU3Ed__3_t1587761224::get_offset_of_U3CU3E1__state_0(),
	U3CDoInvokeU3Ed__3_t1587761224::get_offset_of_U3CU3E2__current_1(),
	U3CDoInvokeU3Ed__3_t1587761224::get_offset_of_a_2(),
	U3CDoInvokeU3Ed__3_t1587761224::get_offset_of_delayInSeconds_3(),
	U3CDoInvokeU3Ed__3_t1587761224::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (U3CProcessU3Ed__4_t1496112689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[9] = 
{
	U3CProcessU3Ed__4_t1496112689::get_offset_of_U3CU3E1__state_0(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_U3CU3E2__current_1(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_request_2(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_responseHandler_3(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_errorHandler_4(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_maxTimeoutInSeconds_5(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_U3CU3E4__this_6(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_U3CtimerU3E5__1_7(),
	U3CProcessU3Ed__4_t1496112689::get_offset_of_U3ChasTimedOutU3E5__2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (CloudCatalogImpl_t569898932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[7] = 
{
	CloudCatalogImpl_t569898932::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogImpl_t569898932::get_offset_of_m_CacheFileName_1(),
	CloudCatalogImpl_t569898932::get_offset_of_m_Logger_2(),
	CloudCatalogImpl_t569898932::get_offset_of_m_CatalogURL_3(),
	CloudCatalogImpl_t569898932::get_offset_of_m_StoreName_4(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (U3CU3Ec__DisplayClass10_0_t2096495139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[4] = 
{
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_delayInSeconds_1(),
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_U3CU3E4__this_2(),
	U3CU3Ec__DisplayClass10_0_t2096495139::get_offset_of_U3CU3E9__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CU3Ec_t4238608033), -1, sizeof(U3CU3Ec_t4238608033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2277[3] = 
{
	U3CU3Ec_t4238608033_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4238608033_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_t4238608033_StaticFields::get_offset_of_U3CU3E9__12_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (EventDestType_t4138688180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2278[6] = 
{
	EventDestType_t4138688180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (EventQueue_t560163637), -1, sizeof(EventQueue_t560163637_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2279[6] = 
{
	EventQueue_t560163637::get_offset_of_m_AsyncUtil_0(),
	EventQueue_t560163637_StaticFields::get_offset_of_QueueInstance_1(),
	EventQueue_t560163637::get_offset_of_Profile_2(),
	EventQueue_t560163637::get_offset_of_TrackingUrl_3(),
	EventQueue_t560163637::get_offset_of_EventUrl_4(),
	EventQueue_t560163637::get_offset_of_ProfileDict_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CU3Ec__DisplayClass11_0_t850767941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[6] = 
{
	U3CU3Ec__DisplayClass11_0_t850767941::get_offset_of_delayInSeconds_0(),
	U3CU3Ec__DisplayClass11_0_t850767941::get_offset_of_dest_1(),
	U3CU3Ec__DisplayClass11_0_t850767941::get_offset_of_json_2(),
	U3CU3Ec__DisplayClass11_0_t850767941::get_offset_of_target_3(),
	U3CU3Ec__DisplayClass11_0_t850767941::get_offset_of_U3CU3E4__this_4(),
	U3CU3Ec__DisplayClass11_0_t850767941::get_offset_of_U3CU3E9__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CU3Ec_t1387771964), -1, sizeof(U3CU3Ec_t1387771964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2281[4] = 
{
	U3CU3Ec_t1387771964_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1387771964_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_t1387771964_StaticFields::get_offset_of_U3CU3E9__11_3_2(),
	U3CU3Ec_t1387771964_StaticFields::get_offset_of_U3CU3E9__11_4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (FakeManagedStoreConfig_t797709047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[4] = 
{
	FakeManagedStoreConfig_t797709047::get_offset_of_catalogDisabled_0(),
	FakeManagedStoreConfig_t797709047::get_offset_of_testStore_1(),
	FakeManagedStoreConfig_t797709047::get_offset_of_iapBaseUrl_2(),
	FakeManagedStoreConfig_t797709047::get_offset_of_eventBaseUrl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (FakeManagedStoreExtensions_t3092625943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (FakeTransactionHistoryExtensions_t2213685253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (JSONStore_t1890359403), -1, sizeof(JSONStore_t1890359403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2291[22] = 
{
	JSONStore_t1890359403::get_offset_of_m_managedStore_0(),
	JSONStore_t1890359403::get_offset_of_unity_1(),
	JSONStore_t1890359403::get_offset_of_store_2(),
	JSONStore_t1890359403::get_offset_of_m_storeCatalog_3(),
	JSONStore_t1890359403::get_offset_of_isManagedStoreEnabled_4(),
	JSONStore_t1890359403::get_offset_of_m_profileData_5(),
	JSONStore_t1890359403::get_offset_of_isRefreshing_6(),
	JSONStore_t1890359403::get_offset_of_isFirstTimeRetrievingProducts_7(),
	JSONStore_t1890359403::get_offset_of_refreshCallback_8(),
	JSONStore_t1890359403_StaticFields::get_offset_of_analyticsCustomEventMethodInfo_9(),
	JSONStore_t1890359403::get_offset_of_m_Module_10(),
	JSONStore_t1890359403::get_offset_of_m_BuilderProducts_11(),
	JSONStore_t1890359403::get_offset_of_m_Logger_12(),
	JSONStore_t1890359403::get_offset_of_m_EventQueue_13(),
	JSONStore_t1890359403::get_offset_of_promoPayload_14(),
	JSONStore_t1890359403::get_offset_of_catalogDisabled_15(),
	JSONStore_t1890359403::get_offset_of_testStore_16(),
	JSONStore_t1890359403::get_offset_of_iapBaseUrl_17(),
	JSONStore_t1890359403::get_offset_of_eventBaseUrl_18(),
	JSONStore_t1890359403::get_offset_of_lastPurchaseFailureDescription_19(),
	JSONStore_t1890359403::get_offset_of__lastPurchaseErrorCode_20(),
	JSONStore_t1890359403::get_offset_of_kStoreSpecificErrorCodeKey_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (NativeStoreProvider_t4107132677), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (Price_t1853024949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[3] = 
{
	Price_t1853024949::get_offset_of_value_0(),
	Price_t1853024949::get_offset_of_data_1(),
	Price_t1853024949::get_offset_of_num_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (StoreID_t471452324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[2] = 
{
	StoreID_t471452324::get_offset_of_store_0(),
	StoreID_t471452324::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (TranslationLocale_t3543162129)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2295[33] = 
{
	TranslationLocale_t3543162129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (LocalizedProductDescription_t1525635964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[3] = 
{
	LocalizedProductDescription_t1525635964::get_offset_of_googleLocale_0(),
	LocalizedProductDescription_t1525635964::get_offset_of_title_1(),
	LocalizedProductDescription_t1525635964::get_offset_of_description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (U3CU3Ec_t3617267505), -1, sizeof(U3CU3Ec_t3617267505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2297[2] = 
{
	U3CU3Ec_t3617267505_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3617267505_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (ProductCatalogPayout_t96590568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[3] = 
{
	ProductCatalogPayout_t96590568::get_offset_of_t_0(),
	ProductCatalogPayout_t96590568::get_offset_of_st_1(),
	ProductCatalogPayout_t96590568::get_offset_of_d_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (ProductCatalogPayoutType_t3881497528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2299[5] = 
{
	ProductCatalogPayoutType_t3881497528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
