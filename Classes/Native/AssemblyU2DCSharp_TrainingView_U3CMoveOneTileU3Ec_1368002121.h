﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TrainingView/<MoveOneTile>c__AnonStorey2
struct U3CMoveOneTileU3Ec__AnonStorey2_t2934086062;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/<MoveOneTile>c__AnonStorey1
struct  U3CMoveOneTileU3Ec__AnonStorey1_t1368002121  : public Il2CppObject
{
public:
	// UnityEngine.GameObject TrainingView/<MoveOneTile>c__AnonStorey1::removeTrainee
	GameObject_t1756533147 * ___removeTrainee_0;
	// TrainingView/<MoveOneTile>c__AnonStorey2 TrainingView/<MoveOneTile>c__AnonStorey1::<>f__ref$2
	U3CMoveOneTileU3Ec__AnonStorey2_t2934086062 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_removeTrainee_0() { return static_cast<int32_t>(offsetof(U3CMoveOneTileU3Ec__AnonStorey1_t1368002121, ___removeTrainee_0)); }
	inline GameObject_t1756533147 * get_removeTrainee_0() const { return ___removeTrainee_0; }
	inline GameObject_t1756533147 ** get_address_of_removeTrainee_0() { return &___removeTrainee_0; }
	inline void set_removeTrainee_0(GameObject_t1756533147 * value)
	{
		___removeTrainee_0 = value;
		Il2CppCodeGenWriteBarrier(&___removeTrainee_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CMoveOneTileU3Ec__AnonStorey1_t1368002121, ___U3CU3Ef__refU242_1)); }
	inline U3CMoveOneTileU3Ec__AnonStorey2_t2934086062 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CMoveOneTileU3Ec__AnonStorey2_t2934086062 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CMoveOneTileU3Ec__AnonStorey2_t2934086062 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU242_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
