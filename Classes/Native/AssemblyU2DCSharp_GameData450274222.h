﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"

// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt[]
struct ObscuredIntU5BU5D_t111592097;
// CodeStage.AntiCheat.ObscuredTypes.ObscuredString[]
struct ObscuredStringU5BU5D_t3702386811;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameData
struct  GameData_t450274222  : public Il2CppObject
{
public:

public:
};

struct GameData_t450274222_StaticFields
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::UNIT_MAX_LEVEL
	ObscuredInt_t796441056  ___UNIT_MAX_LEVEL_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt[] GameData::LEVELUP_CARD_COUNT
	ObscuredIntU5BU5D_t111592097* ___LEVELUP_CARD_COUNT_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt[] GameData::LEVELUP_COST
	ObscuredIntU5BU5D_t111592097* ___LEVELUP_COST_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::SEARCH_FREE_TIME
	ObscuredInt_t796441056  ___SEARCH_FREE_TIME_3;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::DIRECT_REFRESH_TIME
	ObscuredInt_t796441056  ___DIRECT_REFRESH_TIME_4;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::DOUBLE_CHANCE_TIME
	ObscuredInt_t796441056  ___DOUBLE_CHANCE_TIME_5;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::FREE_GEM_TIME
	ObscuredInt_t796441056  ___FREE_GEM_TIME_6;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::SEARCH_PLAYER_COIN
	ObscuredInt_t796441056  ___SEARCH_PLAYER_COIN_7;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::SEARCH_PLAYER_GEM
	ObscuredInt_t796441056  ___SEARCH_PLAYER_GEM_8;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt GameData::SUFFLE_GEM
	ObscuredInt_t796441056  ___SUFFLE_GEM_9;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString[] GameData::FORMATION_STRING
	ObscuredStringU5BU5D_t3702386811* ___FORMATION_STRING_10;

public:
	inline static int32_t get_offset_of_UNIT_MAX_LEVEL_0() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___UNIT_MAX_LEVEL_0)); }
	inline ObscuredInt_t796441056  get_UNIT_MAX_LEVEL_0() const { return ___UNIT_MAX_LEVEL_0; }
	inline ObscuredInt_t796441056 * get_address_of_UNIT_MAX_LEVEL_0() { return &___UNIT_MAX_LEVEL_0; }
	inline void set_UNIT_MAX_LEVEL_0(ObscuredInt_t796441056  value)
	{
		___UNIT_MAX_LEVEL_0 = value;
	}

	inline static int32_t get_offset_of_LEVELUP_CARD_COUNT_1() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___LEVELUP_CARD_COUNT_1)); }
	inline ObscuredIntU5BU5D_t111592097* get_LEVELUP_CARD_COUNT_1() const { return ___LEVELUP_CARD_COUNT_1; }
	inline ObscuredIntU5BU5D_t111592097** get_address_of_LEVELUP_CARD_COUNT_1() { return &___LEVELUP_CARD_COUNT_1; }
	inline void set_LEVELUP_CARD_COUNT_1(ObscuredIntU5BU5D_t111592097* value)
	{
		___LEVELUP_CARD_COUNT_1 = value;
		Il2CppCodeGenWriteBarrier(&___LEVELUP_CARD_COUNT_1, value);
	}

	inline static int32_t get_offset_of_LEVELUP_COST_2() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___LEVELUP_COST_2)); }
	inline ObscuredIntU5BU5D_t111592097* get_LEVELUP_COST_2() const { return ___LEVELUP_COST_2; }
	inline ObscuredIntU5BU5D_t111592097** get_address_of_LEVELUP_COST_2() { return &___LEVELUP_COST_2; }
	inline void set_LEVELUP_COST_2(ObscuredIntU5BU5D_t111592097* value)
	{
		___LEVELUP_COST_2 = value;
		Il2CppCodeGenWriteBarrier(&___LEVELUP_COST_2, value);
	}

	inline static int32_t get_offset_of_SEARCH_FREE_TIME_3() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___SEARCH_FREE_TIME_3)); }
	inline ObscuredInt_t796441056  get_SEARCH_FREE_TIME_3() const { return ___SEARCH_FREE_TIME_3; }
	inline ObscuredInt_t796441056 * get_address_of_SEARCH_FREE_TIME_3() { return &___SEARCH_FREE_TIME_3; }
	inline void set_SEARCH_FREE_TIME_3(ObscuredInt_t796441056  value)
	{
		___SEARCH_FREE_TIME_3 = value;
	}

	inline static int32_t get_offset_of_DIRECT_REFRESH_TIME_4() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___DIRECT_REFRESH_TIME_4)); }
	inline ObscuredInt_t796441056  get_DIRECT_REFRESH_TIME_4() const { return ___DIRECT_REFRESH_TIME_4; }
	inline ObscuredInt_t796441056 * get_address_of_DIRECT_REFRESH_TIME_4() { return &___DIRECT_REFRESH_TIME_4; }
	inline void set_DIRECT_REFRESH_TIME_4(ObscuredInt_t796441056  value)
	{
		___DIRECT_REFRESH_TIME_4 = value;
	}

	inline static int32_t get_offset_of_DOUBLE_CHANCE_TIME_5() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___DOUBLE_CHANCE_TIME_5)); }
	inline ObscuredInt_t796441056  get_DOUBLE_CHANCE_TIME_5() const { return ___DOUBLE_CHANCE_TIME_5; }
	inline ObscuredInt_t796441056 * get_address_of_DOUBLE_CHANCE_TIME_5() { return &___DOUBLE_CHANCE_TIME_5; }
	inline void set_DOUBLE_CHANCE_TIME_5(ObscuredInt_t796441056  value)
	{
		___DOUBLE_CHANCE_TIME_5 = value;
	}

	inline static int32_t get_offset_of_FREE_GEM_TIME_6() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___FREE_GEM_TIME_6)); }
	inline ObscuredInt_t796441056  get_FREE_GEM_TIME_6() const { return ___FREE_GEM_TIME_6; }
	inline ObscuredInt_t796441056 * get_address_of_FREE_GEM_TIME_6() { return &___FREE_GEM_TIME_6; }
	inline void set_FREE_GEM_TIME_6(ObscuredInt_t796441056  value)
	{
		___FREE_GEM_TIME_6 = value;
	}

	inline static int32_t get_offset_of_SEARCH_PLAYER_COIN_7() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___SEARCH_PLAYER_COIN_7)); }
	inline ObscuredInt_t796441056  get_SEARCH_PLAYER_COIN_7() const { return ___SEARCH_PLAYER_COIN_7; }
	inline ObscuredInt_t796441056 * get_address_of_SEARCH_PLAYER_COIN_7() { return &___SEARCH_PLAYER_COIN_7; }
	inline void set_SEARCH_PLAYER_COIN_7(ObscuredInt_t796441056  value)
	{
		___SEARCH_PLAYER_COIN_7 = value;
	}

	inline static int32_t get_offset_of_SEARCH_PLAYER_GEM_8() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___SEARCH_PLAYER_GEM_8)); }
	inline ObscuredInt_t796441056  get_SEARCH_PLAYER_GEM_8() const { return ___SEARCH_PLAYER_GEM_8; }
	inline ObscuredInt_t796441056 * get_address_of_SEARCH_PLAYER_GEM_8() { return &___SEARCH_PLAYER_GEM_8; }
	inline void set_SEARCH_PLAYER_GEM_8(ObscuredInt_t796441056  value)
	{
		___SEARCH_PLAYER_GEM_8 = value;
	}

	inline static int32_t get_offset_of_SUFFLE_GEM_9() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___SUFFLE_GEM_9)); }
	inline ObscuredInt_t796441056  get_SUFFLE_GEM_9() const { return ___SUFFLE_GEM_9; }
	inline ObscuredInt_t796441056 * get_address_of_SUFFLE_GEM_9() { return &___SUFFLE_GEM_9; }
	inline void set_SUFFLE_GEM_9(ObscuredInt_t796441056  value)
	{
		___SUFFLE_GEM_9 = value;
	}

	inline static int32_t get_offset_of_FORMATION_STRING_10() { return static_cast<int32_t>(offsetof(GameData_t450274222_StaticFields, ___FORMATION_STRING_10)); }
	inline ObscuredStringU5BU5D_t3702386811* get_FORMATION_STRING_10() const { return ___FORMATION_STRING_10; }
	inline ObscuredStringU5BU5D_t3702386811** get_address_of_FORMATION_STRING_10() { return &___FORMATION_STRING_10; }
	inline void set_FORMATION_STRING_10(ObscuredStringU5BU5D_t3702386811* value)
	{
		___FORMATION_STRING_10 = value;
		Il2CppCodeGenWriteBarrier(&___FORMATION_STRING_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
