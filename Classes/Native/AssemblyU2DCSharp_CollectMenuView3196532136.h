﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// CollectMenuCell
struct CollectMenuCell_t2927503929;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.Dictionary`2<System.Int32,Nation>
struct Dictionary_2_t2439496172;
// NationSimpleInfo[]
struct NationSimpleInfoU5BU5D_t321925280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectMenuView
struct  CollectMenuView_t3196532136  : public MonoBehaviour_t1158329972
{
public:
	// CollectMenuCell CollectMenuView::m_cellPrefab
	CollectMenuCell_t2927503929 * ___m_cellPrefab_2;
	// Tacticsoft.TableView CollectMenuView::m_tableView
	TableView_t3179510217 * ___m_tableView_3;
	// System.Int32 CollectMenuView::m_numInstancesCreated
	int32_t ___m_numInstancesCreated_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,Nation> CollectMenuView::nationDict
	Dictionary_2_t2439496172 * ___nationDict_5;
	// NationSimpleInfo[] CollectMenuView::nationInfoList
	NationSimpleInfoU5BU5D_t321925280* ___nationInfoList_6;

public:
	inline static int32_t get_offset_of_m_cellPrefab_2() { return static_cast<int32_t>(offsetof(CollectMenuView_t3196532136, ___m_cellPrefab_2)); }
	inline CollectMenuCell_t2927503929 * get_m_cellPrefab_2() const { return ___m_cellPrefab_2; }
	inline CollectMenuCell_t2927503929 ** get_address_of_m_cellPrefab_2() { return &___m_cellPrefab_2; }
	inline void set_m_cellPrefab_2(CollectMenuCell_t2927503929 * value)
	{
		___m_cellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_cellPrefab_2, value);
	}

	inline static int32_t get_offset_of_m_tableView_3() { return static_cast<int32_t>(offsetof(CollectMenuView_t3196532136, ___m_tableView_3)); }
	inline TableView_t3179510217 * get_m_tableView_3() const { return ___m_tableView_3; }
	inline TableView_t3179510217 ** get_address_of_m_tableView_3() { return &___m_tableView_3; }
	inline void set_m_tableView_3(TableView_t3179510217 * value)
	{
		___m_tableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tableView_3, value);
	}

	inline static int32_t get_offset_of_m_numInstancesCreated_4() { return static_cast<int32_t>(offsetof(CollectMenuView_t3196532136, ___m_numInstancesCreated_4)); }
	inline int32_t get_m_numInstancesCreated_4() const { return ___m_numInstancesCreated_4; }
	inline int32_t* get_address_of_m_numInstancesCreated_4() { return &___m_numInstancesCreated_4; }
	inline void set_m_numInstancesCreated_4(int32_t value)
	{
		___m_numInstancesCreated_4 = value;
	}

	inline static int32_t get_offset_of_nationDict_5() { return static_cast<int32_t>(offsetof(CollectMenuView_t3196532136, ___nationDict_5)); }
	inline Dictionary_2_t2439496172 * get_nationDict_5() const { return ___nationDict_5; }
	inline Dictionary_2_t2439496172 ** get_address_of_nationDict_5() { return &___nationDict_5; }
	inline void set_nationDict_5(Dictionary_2_t2439496172 * value)
	{
		___nationDict_5 = value;
		Il2CppCodeGenWriteBarrier(&___nationDict_5, value);
	}

	inline static int32_t get_offset_of_nationInfoList_6() { return static_cast<int32_t>(offsetof(CollectMenuView_t3196532136, ___nationInfoList_6)); }
	inline NationSimpleInfoU5BU5D_t321925280* get_nationInfoList_6() const { return ___nationInfoList_6; }
	inline NationSimpleInfoU5BU5D_t321925280** get_address_of_nationInfoList_6() { return &___nationInfoList_6; }
	inline void set_nationInfoList_6(NationSimpleInfoU5BU5D_t321925280* value)
	{
		___nationInfoList_6 = value;
		Il2CppCodeGenWriteBarrier(&___nationInfoList_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
