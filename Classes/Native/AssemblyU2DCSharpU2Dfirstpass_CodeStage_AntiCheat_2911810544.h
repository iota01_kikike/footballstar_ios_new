﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// CodeStage.AntiCheat.Detectors.WallHackDetector
struct WallHackDetector_t1804770419;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1
struct  U3CCaptureFrameU3Ec__Iterator1_t2911810544  : public Il2CppObject
{
public:
	// UnityEngine.RenderTexture CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1::<previousActive>__0
	RenderTexture_t2666733923 * ___U3CpreviousActiveU3E__0_0;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1::<detected>__0
	bool ___U3CdetectedU3E__0_1;
	// CodeStage.AntiCheat.Detectors.WallHackDetector CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1::$this
	WallHackDetector_t1804770419 * ___U24this_2;
	// System.Object CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector/<CaptureFrame>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CpreviousActiveU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ec__Iterator1_t2911810544, ___U3CpreviousActiveU3E__0_0)); }
	inline RenderTexture_t2666733923 * get_U3CpreviousActiveU3E__0_0() const { return ___U3CpreviousActiveU3E__0_0; }
	inline RenderTexture_t2666733923 ** get_address_of_U3CpreviousActiveU3E__0_0() { return &___U3CpreviousActiveU3E__0_0; }
	inline void set_U3CpreviousActiveU3E__0_0(RenderTexture_t2666733923 * value)
	{
		___U3CpreviousActiveU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpreviousActiveU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CdetectedU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ec__Iterator1_t2911810544, ___U3CdetectedU3E__0_1)); }
	inline bool get_U3CdetectedU3E__0_1() const { return ___U3CdetectedU3E__0_1; }
	inline bool* get_address_of_U3CdetectedU3E__0_1() { return &___U3CdetectedU3E__0_1; }
	inline void set_U3CdetectedU3E__0_1(bool value)
	{
		___U3CdetectedU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ec__Iterator1_t2911810544, ___U24this_2)); }
	inline WallHackDetector_t1804770419 * get_U24this_2() const { return ___U24this_2; }
	inline WallHackDetector_t1804770419 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WallHackDetector_t1804770419 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ec__Iterator1_t2911810544, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ec__Iterator1_t2911810544, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ec__Iterator1_t2911810544, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
