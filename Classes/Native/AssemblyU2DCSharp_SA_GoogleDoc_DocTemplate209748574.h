﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<SA.GoogleDoc.WorksheetTemplate>
struct List_1_t3153322028;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.DocTemplate
struct  DocTemplate_t209748574  : public Il2CppObject
{
public:
	// System.String SA.GoogleDoc.DocTemplate::key
	String_t* ___key_0;
	// System.Collections.Generic.List`1<SA.GoogleDoc.WorksheetTemplate> SA.GoogleDoc.DocTemplate::_worksheets
	List_1_t3153322028 * ____worksheets_1;
	// System.String SA.GoogleDoc.DocTemplate::_creationTime
	String_t* ____creationTime_2;
	// System.String SA.GoogleDoc.DocTemplate::name
	String_t* ___name_3;
	// System.Boolean SA.GoogleDoc.DocTemplate::IsTableListOpen
	bool ___IsTableListOpen_4;
	// System.Boolean SA.GoogleDoc.DocTemplate::IsOpen
	bool ___IsOpen_5;
	// System.String SA.GoogleDoc.DocTemplate::docName
	String_t* ___docName_6;
	// System.Int32 SA.GoogleDoc.DocTemplate::docIndex
	int32_t ___docIndex_7;
	// System.Int32 SA.GoogleDoc.DocTemplate::selectedWorksheet
	int32_t ___selectedWorksheet_8;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of__worksheets_1() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ____worksheets_1)); }
	inline List_1_t3153322028 * get__worksheets_1() const { return ____worksheets_1; }
	inline List_1_t3153322028 ** get_address_of__worksheets_1() { return &____worksheets_1; }
	inline void set__worksheets_1(List_1_t3153322028 * value)
	{
		____worksheets_1 = value;
		Il2CppCodeGenWriteBarrier(&____worksheets_1, value);
	}

	inline static int32_t get_offset_of__creationTime_2() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ____creationTime_2)); }
	inline String_t* get__creationTime_2() const { return ____creationTime_2; }
	inline String_t** get_address_of__creationTime_2() { return &____creationTime_2; }
	inline void set__creationTime_2(String_t* value)
	{
		____creationTime_2 = value;
		Il2CppCodeGenWriteBarrier(&____creationTime_2, value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier(&___name_3, value);
	}

	inline static int32_t get_offset_of_IsTableListOpen_4() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___IsTableListOpen_4)); }
	inline bool get_IsTableListOpen_4() const { return ___IsTableListOpen_4; }
	inline bool* get_address_of_IsTableListOpen_4() { return &___IsTableListOpen_4; }
	inline void set_IsTableListOpen_4(bool value)
	{
		___IsTableListOpen_4 = value;
	}

	inline static int32_t get_offset_of_IsOpen_5() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___IsOpen_5)); }
	inline bool get_IsOpen_5() const { return ___IsOpen_5; }
	inline bool* get_address_of_IsOpen_5() { return &___IsOpen_5; }
	inline void set_IsOpen_5(bool value)
	{
		___IsOpen_5 = value;
	}

	inline static int32_t get_offset_of_docName_6() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___docName_6)); }
	inline String_t* get_docName_6() const { return ___docName_6; }
	inline String_t** get_address_of_docName_6() { return &___docName_6; }
	inline void set_docName_6(String_t* value)
	{
		___docName_6 = value;
		Il2CppCodeGenWriteBarrier(&___docName_6, value);
	}

	inline static int32_t get_offset_of_docIndex_7() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___docIndex_7)); }
	inline int32_t get_docIndex_7() const { return ___docIndex_7; }
	inline int32_t* get_address_of_docIndex_7() { return &___docIndex_7; }
	inline void set_docIndex_7(int32_t value)
	{
		___docIndex_7 = value;
	}

	inline static int32_t get_offset_of_selectedWorksheet_8() { return static_cast<int32_t>(offsetof(DocTemplate_t209748574, ___selectedWorksheet_8)); }
	inline int32_t get_selectedWorksheet_8() const { return ___selectedWorksheet_8; }
	inline int32_t* get_address_of_selectedWorksheet_8() { return &___selectedWorksheet_8; }
	inline void set_selectedWorksheet_8(int32_t value)
	{
		___selectedWorksheet_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
