﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte2787096497.h"
#include "Stores_UnityEngine_Purchasing_StoreSpecificPurchas1224749471.h"

// UnityEngine.Purchasing.StoreCatalogImpl
struct StoreCatalogImpl_t946505162;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t3203646079;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>
struct List_1_t1311596400;
// UnityEngine.Purchasing.ProfileData
struct ProfileData_t3353328249;
// System.Action
struct Action_t3226471752;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_t4003664591;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_t275936122;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// UnityEngine.Purchasing.EventQueue
struct EventQueue_t560163637;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.String
struct String_t;
// UnityEngine.Purchasing.Extension.PurchaseFailureDescription
struct PurchaseFailureDescription_t1607114611;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JSONStore
struct  JSONStore_t1890359403  : public AbstractStore_t2787096497
{
public:
	// UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.JSONStore::m_managedStore
	StoreCatalogImpl_t946505162 * ___m_managedStore_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.JSONStore::unity
	Il2CppObject * ___unity_1;
	// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.JSONStore::store
	Il2CppObject * ___store_2;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.JSONStore::m_storeCatalog
	List_1_t1311596400 * ___m_storeCatalog_3;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isManagedStoreEnabled
	bool ___isManagedStoreEnabled_4;
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.JSONStore::m_profileData
	ProfileData_t3353328249 * ___m_profileData_5;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isRefreshing
	bool ___isRefreshing_6;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isFirstTimeRetrievingProducts
	bool ___isFirstTimeRetrievingProducts_7;
	// System.Action UnityEngine.Purchasing.JSONStore::refreshCallback
	Action_t3226471752 * ___refreshCallback_8;
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.JSONStore::m_Module
	StandardPurchasingModule_t4003664591 * ___m_Module_10;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.JSONStore::m_BuilderProducts
	HashSet_1_t275936122 * ___m_BuilderProducts_11;
	// UnityEngine.ILogger UnityEngine.Purchasing.JSONStore::m_Logger
	Il2CppObject * ___m_Logger_12;
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.JSONStore::m_EventQueue
	EventQueue_t560163637 * ___m_EventQueue_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONStore::promoPayload
	Dictionary_2_t309261261 * ___promoPayload_14;
	// System.Boolean UnityEngine.Purchasing.JSONStore::catalogDisabled
	bool ___catalogDisabled_15;
	// System.Boolean UnityEngine.Purchasing.JSONStore::testStore
	bool ___testStore_16;
	// System.String UnityEngine.Purchasing.JSONStore::iapBaseUrl
	String_t* ___iapBaseUrl_17;
	// System.String UnityEngine.Purchasing.JSONStore::eventBaseUrl
	String_t* ___eventBaseUrl_18;
	// UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONStore::lastPurchaseFailureDescription
	PurchaseFailureDescription_t1607114611 * ___lastPurchaseFailureDescription_19;
	// UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::_lastPurchaseErrorCode
	int32_t ____lastPurchaseErrorCode_20;
	// System.String UnityEngine.Purchasing.JSONStore::kStoreSpecificErrorCodeKey
	String_t* ___kStoreSpecificErrorCodeKey_21;

public:
	inline static int32_t get_offset_of_m_managedStore_0() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_managedStore_0)); }
	inline StoreCatalogImpl_t946505162 * get_m_managedStore_0() const { return ___m_managedStore_0; }
	inline StoreCatalogImpl_t946505162 ** get_address_of_m_managedStore_0() { return &___m_managedStore_0; }
	inline void set_m_managedStore_0(StoreCatalogImpl_t946505162 * value)
	{
		___m_managedStore_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_managedStore_0, value);
	}

	inline static int32_t get_offset_of_unity_1() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___unity_1)); }
	inline Il2CppObject * get_unity_1() const { return ___unity_1; }
	inline Il2CppObject ** get_address_of_unity_1() { return &___unity_1; }
	inline void set_unity_1(Il2CppObject * value)
	{
		___unity_1 = value;
		Il2CppCodeGenWriteBarrier(&___unity_1, value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___store_2)); }
	inline Il2CppObject * get_store_2() const { return ___store_2; }
	inline Il2CppObject ** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(Il2CppObject * value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier(&___store_2, value);
	}

	inline static int32_t get_offset_of_m_storeCatalog_3() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_storeCatalog_3)); }
	inline List_1_t1311596400 * get_m_storeCatalog_3() const { return ___m_storeCatalog_3; }
	inline List_1_t1311596400 ** get_address_of_m_storeCatalog_3() { return &___m_storeCatalog_3; }
	inline void set_m_storeCatalog_3(List_1_t1311596400 * value)
	{
		___m_storeCatalog_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_storeCatalog_3, value);
	}

	inline static int32_t get_offset_of_isManagedStoreEnabled_4() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___isManagedStoreEnabled_4)); }
	inline bool get_isManagedStoreEnabled_4() const { return ___isManagedStoreEnabled_4; }
	inline bool* get_address_of_isManagedStoreEnabled_4() { return &___isManagedStoreEnabled_4; }
	inline void set_isManagedStoreEnabled_4(bool value)
	{
		___isManagedStoreEnabled_4 = value;
	}

	inline static int32_t get_offset_of_m_profileData_5() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_profileData_5)); }
	inline ProfileData_t3353328249 * get_m_profileData_5() const { return ___m_profileData_5; }
	inline ProfileData_t3353328249 ** get_address_of_m_profileData_5() { return &___m_profileData_5; }
	inline void set_m_profileData_5(ProfileData_t3353328249 * value)
	{
		___m_profileData_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_profileData_5, value);
	}

	inline static int32_t get_offset_of_isRefreshing_6() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___isRefreshing_6)); }
	inline bool get_isRefreshing_6() const { return ___isRefreshing_6; }
	inline bool* get_address_of_isRefreshing_6() { return &___isRefreshing_6; }
	inline void set_isRefreshing_6(bool value)
	{
		___isRefreshing_6 = value;
	}

	inline static int32_t get_offset_of_isFirstTimeRetrievingProducts_7() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___isFirstTimeRetrievingProducts_7)); }
	inline bool get_isFirstTimeRetrievingProducts_7() const { return ___isFirstTimeRetrievingProducts_7; }
	inline bool* get_address_of_isFirstTimeRetrievingProducts_7() { return &___isFirstTimeRetrievingProducts_7; }
	inline void set_isFirstTimeRetrievingProducts_7(bool value)
	{
		___isFirstTimeRetrievingProducts_7 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_8() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___refreshCallback_8)); }
	inline Action_t3226471752 * get_refreshCallback_8() const { return ___refreshCallback_8; }
	inline Action_t3226471752 ** get_address_of_refreshCallback_8() { return &___refreshCallback_8; }
	inline void set_refreshCallback_8(Action_t3226471752 * value)
	{
		___refreshCallback_8 = value;
		Il2CppCodeGenWriteBarrier(&___refreshCallback_8, value);
	}

	inline static int32_t get_offset_of_m_Module_10() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_Module_10)); }
	inline StandardPurchasingModule_t4003664591 * get_m_Module_10() const { return ___m_Module_10; }
	inline StandardPurchasingModule_t4003664591 ** get_address_of_m_Module_10() { return &___m_Module_10; }
	inline void set_m_Module_10(StandardPurchasingModule_t4003664591 * value)
	{
		___m_Module_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Module_10, value);
	}

	inline static int32_t get_offset_of_m_BuilderProducts_11() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_BuilderProducts_11)); }
	inline HashSet_1_t275936122 * get_m_BuilderProducts_11() const { return ___m_BuilderProducts_11; }
	inline HashSet_1_t275936122 ** get_address_of_m_BuilderProducts_11() { return &___m_BuilderProducts_11; }
	inline void set_m_BuilderProducts_11(HashSet_1_t275936122 * value)
	{
		___m_BuilderProducts_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_BuilderProducts_11, value);
	}

	inline static int32_t get_offset_of_m_Logger_12() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_Logger_12)); }
	inline Il2CppObject * get_m_Logger_12() const { return ___m_Logger_12; }
	inline Il2CppObject ** get_address_of_m_Logger_12() { return &___m_Logger_12; }
	inline void set_m_Logger_12(Il2CppObject * value)
	{
		___m_Logger_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_Logger_12, value);
	}

	inline static int32_t get_offset_of_m_EventQueue_13() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___m_EventQueue_13)); }
	inline EventQueue_t560163637 * get_m_EventQueue_13() const { return ___m_EventQueue_13; }
	inline EventQueue_t560163637 ** get_address_of_m_EventQueue_13() { return &___m_EventQueue_13; }
	inline void set_m_EventQueue_13(EventQueue_t560163637 * value)
	{
		___m_EventQueue_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_EventQueue_13, value);
	}

	inline static int32_t get_offset_of_promoPayload_14() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___promoPayload_14)); }
	inline Dictionary_2_t309261261 * get_promoPayload_14() const { return ___promoPayload_14; }
	inline Dictionary_2_t309261261 ** get_address_of_promoPayload_14() { return &___promoPayload_14; }
	inline void set_promoPayload_14(Dictionary_2_t309261261 * value)
	{
		___promoPayload_14 = value;
		Il2CppCodeGenWriteBarrier(&___promoPayload_14, value);
	}

	inline static int32_t get_offset_of_catalogDisabled_15() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___catalogDisabled_15)); }
	inline bool get_catalogDisabled_15() const { return ___catalogDisabled_15; }
	inline bool* get_address_of_catalogDisabled_15() { return &___catalogDisabled_15; }
	inline void set_catalogDisabled_15(bool value)
	{
		___catalogDisabled_15 = value;
	}

	inline static int32_t get_offset_of_testStore_16() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___testStore_16)); }
	inline bool get_testStore_16() const { return ___testStore_16; }
	inline bool* get_address_of_testStore_16() { return &___testStore_16; }
	inline void set_testStore_16(bool value)
	{
		___testStore_16 = value;
	}

	inline static int32_t get_offset_of_iapBaseUrl_17() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___iapBaseUrl_17)); }
	inline String_t* get_iapBaseUrl_17() const { return ___iapBaseUrl_17; }
	inline String_t** get_address_of_iapBaseUrl_17() { return &___iapBaseUrl_17; }
	inline void set_iapBaseUrl_17(String_t* value)
	{
		___iapBaseUrl_17 = value;
		Il2CppCodeGenWriteBarrier(&___iapBaseUrl_17, value);
	}

	inline static int32_t get_offset_of_eventBaseUrl_18() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___eventBaseUrl_18)); }
	inline String_t* get_eventBaseUrl_18() const { return ___eventBaseUrl_18; }
	inline String_t** get_address_of_eventBaseUrl_18() { return &___eventBaseUrl_18; }
	inline void set_eventBaseUrl_18(String_t* value)
	{
		___eventBaseUrl_18 = value;
		Il2CppCodeGenWriteBarrier(&___eventBaseUrl_18, value);
	}

	inline static int32_t get_offset_of_lastPurchaseFailureDescription_19() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___lastPurchaseFailureDescription_19)); }
	inline PurchaseFailureDescription_t1607114611 * get_lastPurchaseFailureDescription_19() const { return ___lastPurchaseFailureDescription_19; }
	inline PurchaseFailureDescription_t1607114611 ** get_address_of_lastPurchaseFailureDescription_19() { return &___lastPurchaseFailureDescription_19; }
	inline void set_lastPurchaseFailureDescription_19(PurchaseFailureDescription_t1607114611 * value)
	{
		___lastPurchaseFailureDescription_19 = value;
		Il2CppCodeGenWriteBarrier(&___lastPurchaseFailureDescription_19, value);
	}

	inline static int32_t get_offset_of__lastPurchaseErrorCode_20() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ____lastPurchaseErrorCode_20)); }
	inline int32_t get__lastPurchaseErrorCode_20() const { return ____lastPurchaseErrorCode_20; }
	inline int32_t* get_address_of__lastPurchaseErrorCode_20() { return &____lastPurchaseErrorCode_20; }
	inline void set__lastPurchaseErrorCode_20(int32_t value)
	{
		____lastPurchaseErrorCode_20 = value;
	}

	inline static int32_t get_offset_of_kStoreSpecificErrorCodeKey_21() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403, ___kStoreSpecificErrorCodeKey_21)); }
	inline String_t* get_kStoreSpecificErrorCodeKey_21() const { return ___kStoreSpecificErrorCodeKey_21; }
	inline String_t** get_address_of_kStoreSpecificErrorCodeKey_21() { return &___kStoreSpecificErrorCodeKey_21; }
	inline void set_kStoreSpecificErrorCodeKey_21(String_t* value)
	{
		___kStoreSpecificErrorCodeKey_21 = value;
		Il2CppCodeGenWriteBarrier(&___kStoreSpecificErrorCodeKey_21, value);
	}
};

struct JSONStore_t1890359403_StaticFields
{
public:
	// System.Reflection.MethodInfo UnityEngine.Purchasing.JSONStore::analyticsCustomEventMethodInfo
	MethodInfo_t * ___analyticsCustomEventMethodInfo_9;

public:
	inline static int32_t get_offset_of_analyticsCustomEventMethodInfo_9() { return static_cast<int32_t>(offsetof(JSONStore_t1890359403_StaticFields, ___analyticsCustomEventMethodInfo_9)); }
	inline MethodInfo_t * get_analyticsCustomEventMethodInfo_9() const { return ___analyticsCustomEventMethodInfo_9; }
	inline MethodInfo_t ** get_address_of_analyticsCustomEventMethodInfo_9() { return &___analyticsCustomEventMethodInfo_9; }
	inline void set_analyticsCustomEventMethodInfo_9(MethodInfo_t * value)
	{
		___analyticsCustomEventMethodInfo_9 = value;
		Il2CppCodeGenWriteBarrier(&___analyticsCustomEventMethodInfo_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
