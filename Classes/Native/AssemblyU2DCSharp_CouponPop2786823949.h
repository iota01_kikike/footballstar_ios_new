﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.InputField
struct InputField_t1631627530;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CouponPop
struct  CouponPop_t2786823949  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button CouponPop::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// UnityEngine.UI.Button CouponPop::OkayButton
	Button_t2872111280 * ___OkayButton_3;
	// UnityEngine.UI.InputField CouponPop::InputField
	InputField_t1631627530 * ___InputField_4;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(CouponPop_t2786823949, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_OkayButton_3() { return static_cast<int32_t>(offsetof(CouponPop_t2786823949, ___OkayButton_3)); }
	inline Button_t2872111280 * get_OkayButton_3() const { return ___OkayButton_3; }
	inline Button_t2872111280 ** get_address_of_OkayButton_3() { return &___OkayButton_3; }
	inline void set_OkayButton_3(Button_t2872111280 * value)
	{
		___OkayButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___OkayButton_3, value);
	}

	inline static int32_t get_offset_of_InputField_4() { return static_cast<int32_t>(offsetof(CouponPop_t2786823949, ___InputField_4)); }
	inline InputField_t1631627530 * get_InputField_4() const { return ___InputField_4; }
	inline InputField_t1631627530 ** get_address_of_InputField_4() { return &___InputField_4; }
	inline void set_InputField_4(InputField_t1631627530 * value)
	{
		___InputField_4 = value;
		Il2CppCodeGenWriteBarrier(&___InputField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
