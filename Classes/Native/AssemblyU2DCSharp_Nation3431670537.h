﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"

// CodeStage.AntiCheat.ObscuredTypes.ObscuredString
struct ObscuredString_t692732302;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Nation
struct  Nation_t3431670537  : public Il2CppObject
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Nation::No
	ObscuredInt_t796441056  ___No_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Nation::Name
	ObscuredString_t692732302 * ___Name_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Nation::Form
	ObscuredString_t692732302 * ___Form_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Nation::IsOpen
	ObscuredInt_t796441056  ___IsOpen_3;

public:
	inline static int32_t get_offset_of_No_0() { return static_cast<int32_t>(offsetof(Nation_t3431670537, ___No_0)); }
	inline ObscuredInt_t796441056  get_No_0() const { return ___No_0; }
	inline ObscuredInt_t796441056 * get_address_of_No_0() { return &___No_0; }
	inline void set_No_0(ObscuredInt_t796441056  value)
	{
		___No_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(Nation_t3431670537, ___Name_1)); }
	inline ObscuredString_t692732302 * get_Name_1() const { return ___Name_1; }
	inline ObscuredString_t692732302 ** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(ObscuredString_t692732302 * value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier(&___Name_1, value);
	}

	inline static int32_t get_offset_of_Form_2() { return static_cast<int32_t>(offsetof(Nation_t3431670537, ___Form_2)); }
	inline ObscuredString_t692732302 * get_Form_2() const { return ___Form_2; }
	inline ObscuredString_t692732302 ** get_address_of_Form_2() { return &___Form_2; }
	inline void set_Form_2(ObscuredString_t692732302 * value)
	{
		___Form_2 = value;
		Il2CppCodeGenWriteBarrier(&___Form_2, value);
	}

	inline static int32_t get_offset_of_IsOpen_3() { return static_cast<int32_t>(offsetof(Nation_t3431670537, ___IsOpen_3)); }
	inline ObscuredInt_t796441056  get_IsOpen_3() const { return ___IsOpen_3; }
	inline ObscuredInt_t796441056 * get_address_of_IsOpen_3() { return &___IsOpen_3; }
	inline void set_IsOpen_3(ObscuredInt_t796441056  value)
	{
		___IsOpen_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
