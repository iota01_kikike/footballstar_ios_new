﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.Cell
struct  Cell_t3987632660  : public Il2CppObject
{
public:
	// System.Int32 SA.GoogleDoc.Cell::_col
	int32_t ____col_0;
	// System.Int32 SA.GoogleDoc.Cell::_row
	int32_t ____row_1;
	// System.String SA.GoogleDoc.Cell::_key
	String_t* ____key_2;

public:
	inline static int32_t get_offset_of__col_0() { return static_cast<int32_t>(offsetof(Cell_t3987632660, ____col_0)); }
	inline int32_t get__col_0() const { return ____col_0; }
	inline int32_t* get_address_of__col_0() { return &____col_0; }
	inline void set__col_0(int32_t value)
	{
		____col_0 = value;
	}

	inline static int32_t get_offset_of__row_1() { return static_cast<int32_t>(offsetof(Cell_t3987632660, ____row_1)); }
	inline int32_t get__row_1() const { return ____row_1; }
	inline int32_t* get_address_of__row_1() { return &____row_1; }
	inline void set__row_1(int32_t value)
	{
		____row_1 = value;
	}

	inline static int32_t get_offset_of__key_2() { return static_cast<int32_t>(offsetof(Cell_t3987632660, ____key_2)); }
	inline String_t* get__key_2() const { return ____key_2; }
	inline String_t** get_address_of__key_2() { return &____key_2; }
	inline void set__key_2(String_t* value)
	{
		____key_2 = value;
		Il2CppCodeGenWriteBarrier(&____key_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
