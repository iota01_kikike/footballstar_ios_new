﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// User
struct User_t719925459;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectPositionPopView/<LoadPlayer>c__AnonStorey1
struct  U3CLoadPlayerU3Ec__AnonStorey1_t2607580775  : public Il2CppObject
{
public:
	// User SelectPositionPopView/<LoadPlayer>c__AnonStorey1::user
	User_t719925459 * ___user_0;

public:
	inline static int32_t get_offset_of_user_0() { return static_cast<int32_t>(offsetof(U3CLoadPlayerU3Ec__AnonStorey1_t2607580775, ___user_0)); }
	inline User_t719925459 * get_user_0() const { return ___user_0; }
	inline User_t719925459 ** get_address_of_user_0() { return &___user_0; }
	inline void set_user_0(User_t719925459 * value)
	{
		___user_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
