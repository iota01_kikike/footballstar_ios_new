﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3265587138.h"

// System.String
struct String_t;
// GameManager
struct GameManager_t2252321495;
// AdMobController
struct AdMobController_t3743396963;
// GameData
struct GameData_t450274222;
// User
struct User_t719925459;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// System.String GameManager::SAVE_KEY
	String_t* ___SAVE_KEY_2;
	// System.Boolean GameManager::IS_DEV
	bool ___IS_DEV_4;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble GameManager::NetworkTime
	ObscuredDouble_t3265587138  ___NetworkTime_5;
	// AdMobController GameManager::_adMobController
	AdMobController_t3743396963 * ____adMobController_6;
	// GameData GameManager::_gameData
	GameData_t450274222 * ____gameData_7;
	// User GameManager::_user
	User_t719925459 * ____user_8;

public:
	inline static int32_t get_offset_of_SAVE_KEY_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___SAVE_KEY_2)); }
	inline String_t* get_SAVE_KEY_2() const { return ___SAVE_KEY_2; }
	inline String_t** get_address_of_SAVE_KEY_2() { return &___SAVE_KEY_2; }
	inline void set_SAVE_KEY_2(String_t* value)
	{
		___SAVE_KEY_2 = value;
		Il2CppCodeGenWriteBarrier(&___SAVE_KEY_2, value);
	}

	inline static int32_t get_offset_of_IS_DEV_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___IS_DEV_4)); }
	inline bool get_IS_DEV_4() const { return ___IS_DEV_4; }
	inline bool* get_address_of_IS_DEV_4() { return &___IS_DEV_4; }
	inline void set_IS_DEV_4(bool value)
	{
		___IS_DEV_4 = value;
	}

	inline static int32_t get_offset_of_NetworkTime_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___NetworkTime_5)); }
	inline ObscuredDouble_t3265587138  get_NetworkTime_5() const { return ___NetworkTime_5; }
	inline ObscuredDouble_t3265587138 * get_address_of_NetworkTime_5() { return &___NetworkTime_5; }
	inline void set_NetworkTime_5(ObscuredDouble_t3265587138  value)
	{
		___NetworkTime_5 = value;
	}

	inline static int32_t get_offset_of__adMobController_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ____adMobController_6)); }
	inline AdMobController_t3743396963 * get__adMobController_6() const { return ____adMobController_6; }
	inline AdMobController_t3743396963 ** get_address_of__adMobController_6() { return &____adMobController_6; }
	inline void set__adMobController_6(AdMobController_t3743396963 * value)
	{
		____adMobController_6 = value;
		Il2CppCodeGenWriteBarrier(&____adMobController_6, value);
	}

	inline static int32_t get_offset_of__gameData_7() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ____gameData_7)); }
	inline GameData_t450274222 * get__gameData_7() const { return ____gameData_7; }
	inline GameData_t450274222 ** get_address_of__gameData_7() { return &____gameData_7; }
	inline void set__gameData_7(GameData_t450274222 * value)
	{
		____gameData_7 = value;
		Il2CppCodeGenWriteBarrier(&____gameData_7, value);
	}

	inline static int32_t get_offset_of__user_8() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ____user_8)); }
	inline User_t719925459 * get__user_8() const { return ____user_8; }
	inline User_t719925459 ** get_address_of__user_8() { return &____user_8; }
	inline void set__user_8(User_t719925459 * value)
	{
		____user_8 = value;
		Il2CppCodeGenWriteBarrier(&____user_8, value);
	}
};

struct GameManager_t2252321495_StaticFields
{
public:
	// GameManager GameManager::_instance
	GameManager_t2252321495 * ____instance_3;
	// System.Action`1<System.Int32> GameManager::<>f__am$cache0
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495_StaticFields, ____instance_3)); }
	inline GameManager_t2252321495 * get__instance_3() const { return ____instance_3; }
	inline GameManager_t2252321495 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(GameManager_t2252321495 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(GameManager_t2252321495_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
