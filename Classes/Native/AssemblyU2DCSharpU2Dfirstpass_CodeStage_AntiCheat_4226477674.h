﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion/RawEncryptedQuaternion
struct  RawEncryptedQuaternion_t4226477674 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion/RawEncryptedQuaternion::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion/RawEncryptedQuaternion::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion/RawEncryptedQuaternion::z
	int32_t ___z_2;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion/RawEncryptedQuaternion::w
	int32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t4226477674, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t4226477674, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t4226477674, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t4226477674, ___w_3)); }
	inline int32_t get_w_3() const { return ___w_3; }
	inline int32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(int32_t value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
