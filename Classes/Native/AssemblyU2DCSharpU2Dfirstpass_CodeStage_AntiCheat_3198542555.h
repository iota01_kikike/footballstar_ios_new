﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3474909705.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct  ObscuredFloat_t3198542555 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.Common.ACTkByte4 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::hiddenValue
	ACTkByte4_t3474909705  ___hiddenValue_2;
	// System.Byte[] CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::hiddenValueOld
	ByteU5BU5D_t3397334013* ___hiddenValueOld_3;
	// System.Single CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::fakeValue
	float ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::inited
	bool ___inited_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredFloat_t3198542555, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredFloat_t3198542555, ___hiddenValue_2)); }
	inline ACTkByte4_t3474909705  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline ACTkByte4_t3474909705 * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(ACTkByte4_t3474909705  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_hiddenValueOld_3() { return static_cast<int32_t>(offsetof(ObscuredFloat_t3198542555, ___hiddenValueOld_3)); }
	inline ByteU5BU5D_t3397334013* get_hiddenValueOld_3() const { return ___hiddenValueOld_3; }
	inline ByteU5BU5D_t3397334013** get_address_of_hiddenValueOld_3() { return &___hiddenValueOld_3; }
	inline void set_hiddenValueOld_3(ByteU5BU5D_t3397334013* value)
	{
		___hiddenValueOld_3 = value;
		Il2CppCodeGenWriteBarrier(&___hiddenValueOld_3, value);
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredFloat_t3198542555, ___fakeValue_4)); }
	inline float get_fakeValue_4() const { return ___fakeValue_4; }
	inline float* get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(float value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_inited_5() { return static_cast<int32_t>(offsetof(ObscuredFloat_t3198542555, ___inited_5)); }
	inline bool get_inited_5() const { return ___inited_5; }
	inline bool* get_address_of_inited_5() { return &___inited_5; }
	inline void set_inited_5(bool value)
	{
		___inited_5 = value;
	}
};

struct ObscuredFloat_t3198542555_StaticFields
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::cryptoKey
	int32_t ___cryptoKey_0;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredFloat_t3198542555_StaticFields, ___cryptoKey_0)); }
	inline int32_t get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline int32_t* get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(int32_t value)
	{
		___cryptoKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct ObscuredFloat_t3198542555_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	ACTkByte4_t3474909705  ___hiddenValue_2;
	uint8_t* ___hiddenValueOld_3;
	float ___fakeValue_4;
	int32_t ___inited_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct ObscuredFloat_t3198542555_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	ACTkByte4_t3474909705  ___hiddenValue_2;
	uint8_t* ___hiddenValueOld_3;
	float ___fakeValue_4;
	int32_t ___inited_5;
};
