﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct  ObscuredBool_t337339225 
{
public:
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::currentCryptoKey
	uint8_t ___currentCryptoKey_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::hiddenValue
	int32_t ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::fakeValue
	bool ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::fakeValueChanged
	bool ___fakeValueChanged_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::inited
	bool ___inited_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredBool_t337339225, ___currentCryptoKey_1)); }
	inline uint8_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline uint8_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(uint8_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredBool_t337339225, ___hiddenValue_2)); }
	inline int32_t get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline int32_t* get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(int32_t value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredBool_t337339225, ___fakeValue_3)); }
	inline bool get_fakeValue_3() const { return ___fakeValue_3; }
	inline bool* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(bool value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueChanged_4() { return static_cast<int32_t>(offsetof(ObscuredBool_t337339225, ___fakeValueChanged_4)); }
	inline bool get_fakeValueChanged_4() const { return ___fakeValueChanged_4; }
	inline bool* get_address_of_fakeValueChanged_4() { return &___fakeValueChanged_4; }
	inline void set_fakeValueChanged_4(bool value)
	{
		___fakeValueChanged_4 = value;
	}

	inline static int32_t get_offset_of_inited_5() { return static_cast<int32_t>(offsetof(ObscuredBool_t337339225, ___inited_5)); }
	inline bool get_inited_5() const { return ___inited_5; }
	inline bool* get_address_of_inited_5() { return &___inited_5; }
	inline void set_inited_5(bool value)
	{
		___inited_5 = value;
	}
};

struct ObscuredBool_t337339225_StaticFields
{
public:
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::cryptoKey
	uint8_t ___cryptoKey_0;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredBool_t337339225_StaticFields, ___cryptoKey_0)); }
	inline uint8_t get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline uint8_t* get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(uint8_t value)
	{
		___cryptoKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct ObscuredBool_t337339225_marshaled_pinvoke
{
	uint8_t ___currentCryptoKey_1;
	int32_t ___hiddenValue_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueChanged_4;
	int32_t ___inited_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct ObscuredBool_t337339225_marshaled_com
{
	uint8_t ___currentCryptoKey_1;
	int32_t ___hiddenValue_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueChanged_4;
	int32_t ___inited_5;
};
