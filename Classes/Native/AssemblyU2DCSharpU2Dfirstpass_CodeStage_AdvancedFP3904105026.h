﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.Utils.AFPSRenderRecorder
struct  AFPSRenderRecorder_t3904105026  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean CodeStage.AdvancedFPSCounter.Utils.AFPSRenderRecorder::recording
	bool ___recording_2;
	// System.Single CodeStage.AdvancedFPSCounter.Utils.AFPSRenderRecorder::renderTime
	float ___renderTime_3;

public:
	inline static int32_t get_offset_of_recording_2() { return static_cast<int32_t>(offsetof(AFPSRenderRecorder_t3904105026, ___recording_2)); }
	inline bool get_recording_2() const { return ___recording_2; }
	inline bool* get_address_of_recording_2() { return &___recording_2; }
	inline void set_recording_2(bool value)
	{
		___recording_2 = value;
	}

	inline static int32_t get_offset_of_renderTime_3() { return static_cast<int32_t>(offsetof(AFPSRenderRecorder_t3904105026, ___renderTime_3)); }
	inline float get_renderTime_3() const { return ___renderTime_3; }
	inline float* get_address_of_renderTime_3() { return &___renderTime_3; }
	inline void set_renderTime_3(float value)
	{
		___renderTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
