﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte
struct  ObscuredSByte_t4105365306 
{
public:
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::currentCryptoKey
	int8_t ___currentCryptoKey_1;
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::hiddenValue
	int8_t ___hiddenValue_2;
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::fakeValue
	int8_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::inited
	bool ___inited_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredSByte_t4105365306, ___currentCryptoKey_1)); }
	inline int8_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int8_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int8_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredSByte_t4105365306, ___hiddenValue_2)); }
	inline int8_t get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline int8_t* get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(int8_t value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredSByte_t4105365306, ___fakeValue_3)); }
	inline int8_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int8_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int8_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_inited_4() { return static_cast<int32_t>(offsetof(ObscuredSByte_t4105365306, ___inited_4)); }
	inline bool get_inited_4() const { return ___inited_4; }
	inline bool* get_address_of_inited_4() { return &___inited_4; }
	inline void set_inited_4(bool value)
	{
		___inited_4 = value;
	}
};

struct ObscuredSByte_t4105365306_StaticFields
{
public:
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::cryptoKey
	int8_t ___cryptoKey_0;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredSByte_t4105365306_StaticFields, ___cryptoKey_0)); }
	inline int8_t get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline int8_t* get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(int8_t value)
	{
		___cryptoKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte
struct ObscuredSByte_t4105365306_marshaled_pinvoke
{
	int8_t ___currentCryptoKey_1;
	int8_t ___hiddenValue_2;
	int8_t ___fakeValue_3;
	int32_t ___inited_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte
struct ObscuredSByte_t4105365306_marshaled_com
{
	int8_t ___currentCryptoKey_1;
	int8_t ___hiddenValue_2;
	int8_t ___fakeValue_3;
	int32_t ___inited_4;
};
