﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LabelEffect
struct  LabelEffect_t1942779263  : public Il2CppObject
{
public:
	// System.Boolean LabelEffect::enabled
	bool ___enabled_0;
	// UnityEngine.Color LabelEffect::color
	Color_t2020392075  ___color_1;
	// UnityEngine.Vector2 LabelEffect::distance
	Vector2_t2243707579  ___distance_2;
	// System.Int32 LabelEffect::padding
	int32_t ___padding_3;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(LabelEffect_t1942779263, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(LabelEffect_t1942779263, ___color_1)); }
	inline Color_t2020392075  get_color_1() const { return ___color_1; }
	inline Color_t2020392075 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2020392075  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(LabelEffect_t1942779263, ___distance_2)); }
	inline Vector2_t2243707579  get_distance_2() const { return ___distance_2; }
	inline Vector2_t2243707579 * get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(Vector2_t2243707579  value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_padding_3() { return static_cast<int32_t>(offsetof(LabelEffect_t1942779263, ___padding_3)); }
	inline int32_t get_padding_3() const { return ___padding_3; }
	inline int32_t* get_address_of_padding_3() { return &___padding_3; }
	inline void set_padding_3(int32_t value)
	{
		___padding_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
