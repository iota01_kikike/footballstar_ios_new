﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.ILogger
struct ILogger_t1425954571;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FileReference
struct  FileReference_t4250167507  : public Il2CppObject
{
public:
	// System.String UnityEngine.Purchasing.FileReference::m_FilePath
	String_t* ___m_FilePath_0;
	// UnityEngine.ILogger UnityEngine.Purchasing.FileReference::m_Logger
	Il2CppObject * ___m_Logger_1;

public:
	inline static int32_t get_offset_of_m_FilePath_0() { return static_cast<int32_t>(offsetof(FileReference_t4250167507, ___m_FilePath_0)); }
	inline String_t* get_m_FilePath_0() const { return ___m_FilePath_0; }
	inline String_t** get_address_of_m_FilePath_0() { return &___m_FilePath_0; }
	inline void set_m_FilePath_0(String_t* value)
	{
		___m_FilePath_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_FilePath_0, value);
	}

	inline static int32_t get_offset_of_m_Logger_1() { return static_cast<int32_t>(offsetof(FileReference_t4250167507, ___m_Logger_1)); }
	inline Il2CppObject * get_m_Logger_1() const { return ___m_Logger_1; }
	inline Il2CppObject ** get_address_of_m_Logger_1() { return &___m_Logger_1; }
	inline void set_m_Logger_1(Il2CppObject * value)
	{
		___m_Logger_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Logger_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
