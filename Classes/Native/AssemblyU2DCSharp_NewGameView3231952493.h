﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Analytics_Gender14695945.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NewGameView
struct  NewGameView_t3231952493  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text NewGameView::genderText
	Text_t356221433 * ___genderText_2;
	// UnityEngine.UI.Button NewGameView::MaleButton
	Button_t2872111280 * ___MaleButton_3;
	// UnityEngine.UI.Button NewGameView::FemaleButton
	Button_t2872111280 * ___FemaleButton_4;
	// UnityEngine.UI.Slider NewGameView::AgeSlider
	Slider_t297367283 * ___AgeSlider_5;
	// UnityEngine.UI.Text NewGameView::AgeText
	Text_t356221433 * ___AgeText_6;
	// UnityEngine.UI.Button NewGameView::StartButton
	Button_t2872111280 * ___StartButton_7;
	// UnityEngine.Analytics.Gender NewGameView::_gender
	int32_t ____gender_8;

public:
	inline static int32_t get_offset_of_genderText_2() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ___genderText_2)); }
	inline Text_t356221433 * get_genderText_2() const { return ___genderText_2; }
	inline Text_t356221433 ** get_address_of_genderText_2() { return &___genderText_2; }
	inline void set_genderText_2(Text_t356221433 * value)
	{
		___genderText_2 = value;
		Il2CppCodeGenWriteBarrier(&___genderText_2, value);
	}

	inline static int32_t get_offset_of_MaleButton_3() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ___MaleButton_3)); }
	inline Button_t2872111280 * get_MaleButton_3() const { return ___MaleButton_3; }
	inline Button_t2872111280 ** get_address_of_MaleButton_3() { return &___MaleButton_3; }
	inline void set_MaleButton_3(Button_t2872111280 * value)
	{
		___MaleButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___MaleButton_3, value);
	}

	inline static int32_t get_offset_of_FemaleButton_4() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ___FemaleButton_4)); }
	inline Button_t2872111280 * get_FemaleButton_4() const { return ___FemaleButton_4; }
	inline Button_t2872111280 ** get_address_of_FemaleButton_4() { return &___FemaleButton_4; }
	inline void set_FemaleButton_4(Button_t2872111280 * value)
	{
		___FemaleButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___FemaleButton_4, value);
	}

	inline static int32_t get_offset_of_AgeSlider_5() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ___AgeSlider_5)); }
	inline Slider_t297367283 * get_AgeSlider_5() const { return ___AgeSlider_5; }
	inline Slider_t297367283 ** get_address_of_AgeSlider_5() { return &___AgeSlider_5; }
	inline void set_AgeSlider_5(Slider_t297367283 * value)
	{
		___AgeSlider_5 = value;
		Il2CppCodeGenWriteBarrier(&___AgeSlider_5, value);
	}

	inline static int32_t get_offset_of_AgeText_6() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ___AgeText_6)); }
	inline Text_t356221433 * get_AgeText_6() const { return ___AgeText_6; }
	inline Text_t356221433 ** get_address_of_AgeText_6() { return &___AgeText_6; }
	inline void set_AgeText_6(Text_t356221433 * value)
	{
		___AgeText_6 = value;
		Il2CppCodeGenWriteBarrier(&___AgeText_6, value);
	}

	inline static int32_t get_offset_of_StartButton_7() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ___StartButton_7)); }
	inline Button_t2872111280 * get_StartButton_7() const { return ___StartButton_7; }
	inline Button_t2872111280 ** get_address_of_StartButton_7() { return &___StartButton_7; }
	inline void set_StartButton_7(Button_t2872111280 * value)
	{
		___StartButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___StartButton_7, value);
	}

	inline static int32_t get_offset_of__gender_8() { return static_cast<int32_t>(offsetof(NewGameView_t3231952493, ____gender_8)); }
	inline int32_t get__gender_8() const { return ____gender_8; }
	inline int32_t* get_address_of__gender_8() { return &____gender_8; }
	inline void set__gender_8(int32_t value)
	{
		____gender_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
