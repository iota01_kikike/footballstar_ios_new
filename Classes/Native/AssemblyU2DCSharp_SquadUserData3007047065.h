﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"

// CodeStage.AntiCheat.ObscuredTypes.ObscuredString
struct ObscuredString_t692732302;
// System.Collections.Generic.Dictionary`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>
struct Dictionary_2_t4099233987;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SquadUserData
struct  SquadUserData_t3007047065  : public Il2CppObject
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt SquadUserData::SlotNo
	ObscuredInt_t796441056  ___SlotNo_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString SquadUserData::Form
	ObscuredString_t692732302 * ___Form_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt> SquadUserData::PlayerFormDict
	Dictionary_2_t4099233987 * ___PlayerFormDict_2;

public:
	inline static int32_t get_offset_of_SlotNo_0() { return static_cast<int32_t>(offsetof(SquadUserData_t3007047065, ___SlotNo_0)); }
	inline ObscuredInt_t796441056  get_SlotNo_0() const { return ___SlotNo_0; }
	inline ObscuredInt_t796441056 * get_address_of_SlotNo_0() { return &___SlotNo_0; }
	inline void set_SlotNo_0(ObscuredInt_t796441056  value)
	{
		___SlotNo_0 = value;
	}

	inline static int32_t get_offset_of_Form_1() { return static_cast<int32_t>(offsetof(SquadUserData_t3007047065, ___Form_1)); }
	inline ObscuredString_t692732302 * get_Form_1() const { return ___Form_1; }
	inline ObscuredString_t692732302 ** get_address_of_Form_1() { return &___Form_1; }
	inline void set_Form_1(ObscuredString_t692732302 * value)
	{
		___Form_1 = value;
		Il2CppCodeGenWriteBarrier(&___Form_1, value);
	}

	inline static int32_t get_offset_of_PlayerFormDict_2() { return static_cast<int32_t>(offsetof(SquadUserData_t3007047065, ___PlayerFormDict_2)); }
	inline Dictionary_2_t4099233987 * get_PlayerFormDict_2() const { return ___PlayerFormDict_2; }
	inline Dictionary_2_t4099233987 ** get_address_of_PlayerFormDict_2() { return &___PlayerFormDict_2; }
	inline void set_PlayerFormDict_2(Dictionary_2_t4099233987 * value)
	{
		___PlayerFormDict_2 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerFormDict_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
