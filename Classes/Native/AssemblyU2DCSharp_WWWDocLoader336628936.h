﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// OnDocLoaded
struct OnDocLoaded_t4025950952;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WWWDocLoader
struct  WWWDocLoader_t336628936  : public MonoBehaviour_t1158329972
{
public:
	// OnDocLoaded WWWDocLoader::LoadEvent
	OnDocLoaded_t4025950952 * ___LoadEvent_2;

public:
	inline static int32_t get_offset_of_LoadEvent_2() { return static_cast<int32_t>(offsetof(WWWDocLoader_t336628936, ___LoadEvent_2)); }
	inline OnDocLoaded_t4025950952 * get_LoadEvent_2() const { return ___LoadEvent_2; }
	inline OnDocLoaded_t4025950952 ** get_address_of_LoadEvent_2() { return &___LoadEvent_2; }
	inline void set_LoadEvent_2(OnDocLoaded_t4025950952 * value)
	{
		___LoadEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___LoadEvent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
