﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TabController
struct  TabController_t40264029  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TabController::_tabNo
	int32_t ____tabNo_2;
	// UnityEngine.GameObject[] TabController::PanelList
	GameObjectU5BU5D_t3057952154* ___PanelList_3;
	// UnityEngine.GameObject[] TabController::ButtonList
	GameObjectU5BU5D_t3057952154* ___ButtonList_4;
	// UnityEngine.GameObject[] TabController::BadgeList
	GameObjectU5BU5D_t3057952154* ___BadgeList_5;

public:
	inline static int32_t get_offset_of__tabNo_2() { return static_cast<int32_t>(offsetof(TabController_t40264029, ____tabNo_2)); }
	inline int32_t get__tabNo_2() const { return ____tabNo_2; }
	inline int32_t* get_address_of__tabNo_2() { return &____tabNo_2; }
	inline void set__tabNo_2(int32_t value)
	{
		____tabNo_2 = value;
	}

	inline static int32_t get_offset_of_PanelList_3() { return static_cast<int32_t>(offsetof(TabController_t40264029, ___PanelList_3)); }
	inline GameObjectU5BU5D_t3057952154* get_PanelList_3() const { return ___PanelList_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PanelList_3() { return &___PanelList_3; }
	inline void set_PanelList_3(GameObjectU5BU5D_t3057952154* value)
	{
		___PanelList_3 = value;
		Il2CppCodeGenWriteBarrier(&___PanelList_3, value);
	}

	inline static int32_t get_offset_of_ButtonList_4() { return static_cast<int32_t>(offsetof(TabController_t40264029, ___ButtonList_4)); }
	inline GameObjectU5BU5D_t3057952154* get_ButtonList_4() const { return ___ButtonList_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_ButtonList_4() { return &___ButtonList_4; }
	inline void set_ButtonList_4(GameObjectU5BU5D_t3057952154* value)
	{
		___ButtonList_4 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonList_4, value);
	}

	inline static int32_t get_offset_of_BadgeList_5() { return static_cast<int32_t>(offsetof(TabController_t40264029, ___BadgeList_5)); }
	inline GameObjectU5BU5D_t3057952154* get_BadgeList_5() const { return ___BadgeList_5; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_BadgeList_5() { return &___BadgeList_5; }
	inline void set_BadgeList_5(GameObjectU5BU5D_t3057952154* value)
	{
		___BadgeList_5 = value;
		Il2CppCodeGenWriteBarrier(&___BadgeList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
