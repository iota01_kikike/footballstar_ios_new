﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UIManager
struct UIManager_t2519183485;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t2519183485  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera UIManager::UICamera
	Camera_t189460977 * ___UICamera_3;
	// UnityEngine.GameObject UIManager::BaseCanvas
	GameObject_t1756533147 * ___BaseCanvas_4;
	// UnityEngine.GameObject UIManager::ScoutView
	GameObject_t1756533147 * ___ScoutView_5;
	// UnityEngine.GameObject UIManager::CollectView
	GameObject_t1756533147 * ___CollectView_6;
	// UnityEngine.GameObject UIManager::TrainingView
	GameObject_t1756533147 * ___TrainingView_7;
	// UnityEngine.GameObject UIManager::MySquadView
	GameObject_t1756533147 * ___MySquadView_8;
	// UnityEngine.GameObject UIManager::SettingView
	GameObject_t1756533147 * ___SettingView_9;
	// UnityEngine.GameObject UIManager::UserCashPanel
	GameObject_t1756533147 * ___UserCashPanel_10;
	// UnityEngine.GameObject UIManager::PopUp
	GameObject_t1756533147 * ___PopUp_11;
	// UnityEngine.GameObject UIManager::DepthView
	GameObject_t1756533147 * ___DepthView_12;

public:
	inline static int32_t get_offset_of_UICamera_3() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___UICamera_3)); }
	inline Camera_t189460977 * get_UICamera_3() const { return ___UICamera_3; }
	inline Camera_t189460977 ** get_address_of_UICamera_3() { return &___UICamera_3; }
	inline void set_UICamera_3(Camera_t189460977 * value)
	{
		___UICamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___UICamera_3, value);
	}

	inline static int32_t get_offset_of_BaseCanvas_4() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___BaseCanvas_4)); }
	inline GameObject_t1756533147 * get_BaseCanvas_4() const { return ___BaseCanvas_4; }
	inline GameObject_t1756533147 ** get_address_of_BaseCanvas_4() { return &___BaseCanvas_4; }
	inline void set_BaseCanvas_4(GameObject_t1756533147 * value)
	{
		___BaseCanvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___BaseCanvas_4, value);
	}

	inline static int32_t get_offset_of_ScoutView_5() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___ScoutView_5)); }
	inline GameObject_t1756533147 * get_ScoutView_5() const { return ___ScoutView_5; }
	inline GameObject_t1756533147 ** get_address_of_ScoutView_5() { return &___ScoutView_5; }
	inline void set_ScoutView_5(GameObject_t1756533147 * value)
	{
		___ScoutView_5 = value;
		Il2CppCodeGenWriteBarrier(&___ScoutView_5, value);
	}

	inline static int32_t get_offset_of_CollectView_6() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___CollectView_6)); }
	inline GameObject_t1756533147 * get_CollectView_6() const { return ___CollectView_6; }
	inline GameObject_t1756533147 ** get_address_of_CollectView_6() { return &___CollectView_6; }
	inline void set_CollectView_6(GameObject_t1756533147 * value)
	{
		___CollectView_6 = value;
		Il2CppCodeGenWriteBarrier(&___CollectView_6, value);
	}

	inline static int32_t get_offset_of_TrainingView_7() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___TrainingView_7)); }
	inline GameObject_t1756533147 * get_TrainingView_7() const { return ___TrainingView_7; }
	inline GameObject_t1756533147 ** get_address_of_TrainingView_7() { return &___TrainingView_7; }
	inline void set_TrainingView_7(GameObject_t1756533147 * value)
	{
		___TrainingView_7 = value;
		Il2CppCodeGenWriteBarrier(&___TrainingView_7, value);
	}

	inline static int32_t get_offset_of_MySquadView_8() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___MySquadView_8)); }
	inline GameObject_t1756533147 * get_MySquadView_8() const { return ___MySquadView_8; }
	inline GameObject_t1756533147 ** get_address_of_MySquadView_8() { return &___MySquadView_8; }
	inline void set_MySquadView_8(GameObject_t1756533147 * value)
	{
		___MySquadView_8 = value;
		Il2CppCodeGenWriteBarrier(&___MySquadView_8, value);
	}

	inline static int32_t get_offset_of_SettingView_9() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___SettingView_9)); }
	inline GameObject_t1756533147 * get_SettingView_9() const { return ___SettingView_9; }
	inline GameObject_t1756533147 ** get_address_of_SettingView_9() { return &___SettingView_9; }
	inline void set_SettingView_9(GameObject_t1756533147 * value)
	{
		___SettingView_9 = value;
		Il2CppCodeGenWriteBarrier(&___SettingView_9, value);
	}

	inline static int32_t get_offset_of_UserCashPanel_10() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___UserCashPanel_10)); }
	inline GameObject_t1756533147 * get_UserCashPanel_10() const { return ___UserCashPanel_10; }
	inline GameObject_t1756533147 ** get_address_of_UserCashPanel_10() { return &___UserCashPanel_10; }
	inline void set_UserCashPanel_10(GameObject_t1756533147 * value)
	{
		___UserCashPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___UserCashPanel_10, value);
	}

	inline static int32_t get_offset_of_PopUp_11() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___PopUp_11)); }
	inline GameObject_t1756533147 * get_PopUp_11() const { return ___PopUp_11; }
	inline GameObject_t1756533147 ** get_address_of_PopUp_11() { return &___PopUp_11; }
	inline void set_PopUp_11(GameObject_t1756533147 * value)
	{
		___PopUp_11 = value;
		Il2CppCodeGenWriteBarrier(&___PopUp_11, value);
	}

	inline static int32_t get_offset_of_DepthView_12() { return static_cast<int32_t>(offsetof(UIManager_t2519183485, ___DepthView_12)); }
	inline GameObject_t1756533147 * get_DepthView_12() const { return ___DepthView_12; }
	inline GameObject_t1756533147 ** get_address_of_DepthView_12() { return &___DepthView_12; }
	inline void set_DepthView_12(GameObject_t1756533147 * value)
	{
		___DepthView_12 = value;
		Il2CppCodeGenWriteBarrier(&___DepthView_12, value);
	}
};

struct UIManager_t2519183485_StaticFields
{
public:
	// UIManager UIManager::_instance
	UIManager_t2519183485 * ____instance_2;
	// System.Action`1<System.Int32> UIManager::<>f__am$cache0
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(UIManager_t2519183485_StaticFields, ____instance_2)); }
	inline UIManager_t2519183485 * get__instance_2() const { return ____instance_2; }
	inline UIManager_t2519183485 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(UIManager_t2519183485 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(UIManager_t2519183485_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
