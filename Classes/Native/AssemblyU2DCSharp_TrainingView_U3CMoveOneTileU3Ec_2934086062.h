﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TrainingView
struct TrainingView_t2704080403;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/<MoveOneTile>c__AnonStorey2
struct  U3CMoveOneTileU3Ec__AnonStorey2_t2934086062  : public Il2CppObject
{
public:
	// UnityEngine.GameObject TrainingView/<MoveOneTile>c__AnonStorey2::trainee
	GameObject_t1756533147 * ___trainee_0;
	// TrainingView TrainingView/<MoveOneTile>c__AnonStorey2::$this
	TrainingView_t2704080403 * ___U24this_1;

public:
	inline static int32_t get_offset_of_trainee_0() { return static_cast<int32_t>(offsetof(U3CMoveOneTileU3Ec__AnonStorey2_t2934086062, ___trainee_0)); }
	inline GameObject_t1756533147 * get_trainee_0() const { return ___trainee_0; }
	inline GameObject_t1756533147 ** get_address_of_trainee_0() { return &___trainee_0; }
	inline void set_trainee_0(GameObject_t1756533147 * value)
	{
		___trainee_0 = value;
		Il2CppCodeGenWriteBarrier(&___trainee_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CMoveOneTileU3Ec__AnonStorey2_t2934086062, ___U24this_1)); }
	inline TrainingView_t2704080403 * get_U24this_1() const { return ___U24this_1; }
	inline TrainingView_t2704080403 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TrainingView_t2704080403 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
