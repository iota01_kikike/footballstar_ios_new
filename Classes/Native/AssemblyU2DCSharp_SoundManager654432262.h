﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// SoundManager
struct SoundManager_t654432262;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t654432262  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource SoundManager::audioBGM
	AudioSource_t1135106623 * ___audioBGM_3;
	// UnityEngine.AudioSource SoundManager::audioFx
	AudioSource_t1135106623 * ___audioFx_4;
	// UnityEngine.AudioClip SoundManager::bgm
	AudioClip_t1932558630 * ___bgm_5;
	// System.Boolean SoundManager::isBGMEnable
	bool ___isBGMEnable_6;
	// System.Boolean SoundManager::isEffectEnable
	bool ___isEffectEnable_7;

public:
	inline static int32_t get_offset_of_audioBGM_3() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___audioBGM_3)); }
	inline AudioSource_t1135106623 * get_audioBGM_3() const { return ___audioBGM_3; }
	inline AudioSource_t1135106623 ** get_address_of_audioBGM_3() { return &___audioBGM_3; }
	inline void set_audioBGM_3(AudioSource_t1135106623 * value)
	{
		___audioBGM_3 = value;
		Il2CppCodeGenWriteBarrier(&___audioBGM_3, value);
	}

	inline static int32_t get_offset_of_audioFx_4() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___audioFx_4)); }
	inline AudioSource_t1135106623 * get_audioFx_4() const { return ___audioFx_4; }
	inline AudioSource_t1135106623 ** get_address_of_audioFx_4() { return &___audioFx_4; }
	inline void set_audioFx_4(AudioSource_t1135106623 * value)
	{
		___audioFx_4 = value;
		Il2CppCodeGenWriteBarrier(&___audioFx_4, value);
	}

	inline static int32_t get_offset_of_bgm_5() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___bgm_5)); }
	inline AudioClip_t1932558630 * get_bgm_5() const { return ___bgm_5; }
	inline AudioClip_t1932558630 ** get_address_of_bgm_5() { return &___bgm_5; }
	inline void set_bgm_5(AudioClip_t1932558630 * value)
	{
		___bgm_5 = value;
		Il2CppCodeGenWriteBarrier(&___bgm_5, value);
	}

	inline static int32_t get_offset_of_isBGMEnable_6() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___isBGMEnable_6)); }
	inline bool get_isBGMEnable_6() const { return ___isBGMEnable_6; }
	inline bool* get_address_of_isBGMEnable_6() { return &___isBGMEnable_6; }
	inline void set_isBGMEnable_6(bool value)
	{
		___isBGMEnable_6 = value;
	}

	inline static int32_t get_offset_of_isEffectEnable_7() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___isEffectEnable_7)); }
	inline bool get_isEffectEnable_7() const { return ___isEffectEnable_7; }
	inline bool* get_address_of_isEffectEnable_7() { return &___isEffectEnable_7; }
	inline void set_isEffectEnable_7(bool value)
	{
		___isEffectEnable_7 = value;
	}
};

struct SoundManager_t654432262_StaticFields
{
public:
	// SoundManager SoundManager::_instance
	SoundManager_t654432262 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(SoundManager_t654432262_StaticFields, ____instance_2)); }
	inline SoundManager_t654432262 * get__instance_2() const { return ____instance_2; }
	inline SoundManager_t654432262 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SoundManager_t654432262 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
