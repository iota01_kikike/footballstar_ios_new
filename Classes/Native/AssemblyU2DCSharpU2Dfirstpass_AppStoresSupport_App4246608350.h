﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSetting
struct  AppStoreSetting_t4246608350  : public Il2CppObject
{
public:
	// System.String AppStoresSupport.AppStoreSetting::AppID
	String_t* ___AppID_0;
	// System.String AppStoresSupport.AppStoreSetting::AppKey
	String_t* ___AppKey_1;
	// System.Boolean AppStoresSupport.AppStoreSetting::IsTestMode
	bool ___IsTestMode_2;

public:
	inline static int32_t get_offset_of_AppID_0() { return static_cast<int32_t>(offsetof(AppStoreSetting_t4246608350, ___AppID_0)); }
	inline String_t* get_AppID_0() const { return ___AppID_0; }
	inline String_t** get_address_of_AppID_0() { return &___AppID_0; }
	inline void set_AppID_0(String_t* value)
	{
		___AppID_0 = value;
		Il2CppCodeGenWriteBarrier(&___AppID_0, value);
	}

	inline static int32_t get_offset_of_AppKey_1() { return static_cast<int32_t>(offsetof(AppStoreSetting_t4246608350, ___AppKey_1)); }
	inline String_t* get_AppKey_1() const { return ___AppKey_1; }
	inline String_t** get_address_of_AppKey_1() { return &___AppKey_1; }
	inline void set_AppKey_1(String_t* value)
	{
		___AppKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___AppKey_1, value);
	}

	inline static int32_t get_offset_of_IsTestMode_2() { return static_cast<int32_t>(offsetof(AppStoreSetting_t4246608350, ___IsTestMode_2)); }
	inline bool get_IsTestMode_2() const { return ___IsTestMode_2; }
	inline bool* get_address_of_IsTestMode_2() { return &___IsTestMode_2; }
	inline void set_IsTestMode_2(bool value)
	{
		___IsTestMode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
