﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"

// System.Action`1<UnityEngine.Purchasing.Product>
struct Action_1_t1005487353;
// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// UnityEngine.Purchasing.INativeAppleStore
struct INativeAppleStore_t2240226449;
// Uniject.IUtil
struct IUtil_t2188430191;
// UnityEngine.Purchasing.AppleStoreImpl
struct AppleStoreImpl_t1301617341;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl
struct  AppleStoreImpl_t1301617341  : public JSONStore_t1890359403
{
public:
	// System.Action`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.AppleStoreImpl::m_DeferredCallback
	Action_1_t1005487353 * ___m_DeferredCallback_22;
	// System.Action UnityEngine.Purchasing.AppleStoreImpl::m_RefreshReceiptError
	Action_t3226471752 * ___m_RefreshReceiptError_23;
	// System.Action`1<System.String> UnityEngine.Purchasing.AppleStoreImpl::m_RefreshReceiptSuccess
	Action_1_t1831019615 * ___m_RefreshReceiptSuccess_24;
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.AppleStoreImpl::m_RestoreCallback
	Action_1_t3627374100 * ___m_RestoreCallback_25;
	// System.Action`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.AppleStoreImpl::m_PromotionalPurchaseCallback
	Action_1_t1005487353 * ___m_PromotionalPurchaseCallback_26;
	// UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.AppleStoreImpl::m_Native
	Il2CppObject * ___m_Native_27;
	// System.String UnityEngine.Purchasing.AppleStoreImpl::products_json
	String_t* ___products_json_30;

public:
	inline static int32_t get_offset_of_m_DeferredCallback_22() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___m_DeferredCallback_22)); }
	inline Action_1_t1005487353 * get_m_DeferredCallback_22() const { return ___m_DeferredCallback_22; }
	inline Action_1_t1005487353 ** get_address_of_m_DeferredCallback_22() { return &___m_DeferredCallback_22; }
	inline void set_m_DeferredCallback_22(Action_1_t1005487353 * value)
	{
		___m_DeferredCallback_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_DeferredCallback_22, value);
	}

	inline static int32_t get_offset_of_m_RefreshReceiptError_23() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___m_RefreshReceiptError_23)); }
	inline Action_t3226471752 * get_m_RefreshReceiptError_23() const { return ___m_RefreshReceiptError_23; }
	inline Action_t3226471752 ** get_address_of_m_RefreshReceiptError_23() { return &___m_RefreshReceiptError_23; }
	inline void set_m_RefreshReceiptError_23(Action_t3226471752 * value)
	{
		___m_RefreshReceiptError_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_RefreshReceiptError_23, value);
	}

	inline static int32_t get_offset_of_m_RefreshReceiptSuccess_24() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___m_RefreshReceiptSuccess_24)); }
	inline Action_1_t1831019615 * get_m_RefreshReceiptSuccess_24() const { return ___m_RefreshReceiptSuccess_24; }
	inline Action_1_t1831019615 ** get_address_of_m_RefreshReceiptSuccess_24() { return &___m_RefreshReceiptSuccess_24; }
	inline void set_m_RefreshReceiptSuccess_24(Action_1_t1831019615 * value)
	{
		___m_RefreshReceiptSuccess_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_RefreshReceiptSuccess_24, value);
	}

	inline static int32_t get_offset_of_m_RestoreCallback_25() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___m_RestoreCallback_25)); }
	inline Action_1_t3627374100 * get_m_RestoreCallback_25() const { return ___m_RestoreCallback_25; }
	inline Action_1_t3627374100 ** get_address_of_m_RestoreCallback_25() { return &___m_RestoreCallback_25; }
	inline void set_m_RestoreCallback_25(Action_1_t3627374100 * value)
	{
		___m_RestoreCallback_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_RestoreCallback_25, value);
	}

	inline static int32_t get_offset_of_m_PromotionalPurchaseCallback_26() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___m_PromotionalPurchaseCallback_26)); }
	inline Action_1_t1005487353 * get_m_PromotionalPurchaseCallback_26() const { return ___m_PromotionalPurchaseCallback_26; }
	inline Action_1_t1005487353 ** get_address_of_m_PromotionalPurchaseCallback_26() { return &___m_PromotionalPurchaseCallback_26; }
	inline void set_m_PromotionalPurchaseCallback_26(Action_1_t1005487353 * value)
	{
		___m_PromotionalPurchaseCallback_26 = value;
		Il2CppCodeGenWriteBarrier(&___m_PromotionalPurchaseCallback_26, value);
	}

	inline static int32_t get_offset_of_m_Native_27() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___m_Native_27)); }
	inline Il2CppObject * get_m_Native_27() const { return ___m_Native_27; }
	inline Il2CppObject ** get_address_of_m_Native_27() { return &___m_Native_27; }
	inline void set_m_Native_27(Il2CppObject * value)
	{
		___m_Native_27 = value;
		Il2CppCodeGenWriteBarrier(&___m_Native_27, value);
	}

	inline static int32_t get_offset_of_products_json_30() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341, ___products_json_30)); }
	inline String_t* get_products_json_30() const { return ___products_json_30; }
	inline String_t** get_address_of_products_json_30() { return &___products_json_30; }
	inline void set_products_json_30(String_t* value)
	{
		___products_json_30 = value;
		Il2CppCodeGenWriteBarrier(&___products_json_30, value);
	}
};

struct AppleStoreImpl_t1301617341_StaticFields
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.AppleStoreImpl::util
	Il2CppObject * ___util_28;
	// UnityEngine.Purchasing.AppleStoreImpl UnityEngine.Purchasing.AppleStoreImpl::instance
	AppleStoreImpl_t1301617341 * ___instance_29;

public:
	inline static int32_t get_offset_of_util_28() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341_StaticFields, ___util_28)); }
	inline Il2CppObject * get_util_28() const { return ___util_28; }
	inline Il2CppObject ** get_address_of_util_28() { return &___util_28; }
	inline void set_util_28(Il2CppObject * value)
	{
		___util_28 = value;
		Il2CppCodeGenWriteBarrier(&___util_28, value);
	}

	inline static int32_t get_offset_of_instance_29() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t1301617341_StaticFields, ___instance_29)); }
	inline AppleStoreImpl_t1301617341 * get_instance_29() const { return ___instance_29; }
	inline AppleStoreImpl_t1301617341 ** get_address_of_instance_29() { return &___instance_29; }
	inline void set_instance_29(AppleStoreImpl_t1301617341 * value)
	{
		___instance_29 = value;
		Il2CppCodeGenWriteBarrier(&___instance_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
