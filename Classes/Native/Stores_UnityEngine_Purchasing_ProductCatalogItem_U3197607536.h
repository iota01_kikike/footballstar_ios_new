﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Stores_UnityEngine_Purchasing_TranslationLocale3543162129.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t3197607536  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.TranslationLocale UnityEngine.Purchasing.ProductCatalogItem/<>c__DisplayClass21_0::locale
	int32_t ___locale_0;

public:
	inline static int32_t get_offset_of_locale_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t3197607536, ___locale_0)); }
	inline int32_t get_locale_0() const { return ___locale_0; }
	inline int32_t* get_address_of_locale_0() { return &___locale_0; }
	inline void set_locale_0(int32_t value)
	{
		___locale_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
