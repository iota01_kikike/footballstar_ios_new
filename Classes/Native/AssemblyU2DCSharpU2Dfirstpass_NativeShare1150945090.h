﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t2973420583;
// NativeShare/DownloadingHanderler
struct DownloadingHanderler_t535197402;
// NativeShare/DownloadCompletedHanderler
struct DownloadCompletedHanderler_t2341399827;
// NativeShare/ShareCompletedHanderler
struct ShareCompletedHanderler_t523036664;
// NativeShare
struct NativeShare_t1150945090;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare
struct  NativeShare_t1150945090  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct NativeShare_t1150945090_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass NativeShare::share
	AndroidJavaClass_t2973420583 * ___share_2;
	// NativeShare/DownloadingHanderler NativeShare::OnDownloading
	DownloadingHanderler_t535197402 * ___OnDownloading_3;
	// NativeShare/DownloadCompletedHanderler NativeShare::OnDownloadCompleted
	DownloadCompletedHanderler_t2341399827 * ___OnDownloadCompleted_4;
	// NativeShare/ShareCompletedHanderler NativeShare::OnShareCompleted
	ShareCompletedHanderler_t523036664 * ___OnShareCompleted_5;
	// NativeShare NativeShare::instnace
	NativeShare_t1150945090 * ___instnace_6;
	// UnityEngine.GameObject NativeShare::shareObject
	GameObject_t1756533147 * ___shareObject_7;
	// UnityEngine.GameObject NativeShare::listenner
	GameObject_t1756533147 * ___listenner_8;

public:
	inline static int32_t get_offset_of_share_2() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___share_2)); }
	inline AndroidJavaClass_t2973420583 * get_share_2() const { return ___share_2; }
	inline AndroidJavaClass_t2973420583 ** get_address_of_share_2() { return &___share_2; }
	inline void set_share_2(AndroidJavaClass_t2973420583 * value)
	{
		___share_2 = value;
		Il2CppCodeGenWriteBarrier(&___share_2, value);
	}

	inline static int32_t get_offset_of_OnDownloading_3() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___OnDownloading_3)); }
	inline DownloadingHanderler_t535197402 * get_OnDownloading_3() const { return ___OnDownloading_3; }
	inline DownloadingHanderler_t535197402 ** get_address_of_OnDownloading_3() { return &___OnDownloading_3; }
	inline void set_OnDownloading_3(DownloadingHanderler_t535197402 * value)
	{
		___OnDownloading_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownloading_3, value);
	}

	inline static int32_t get_offset_of_OnDownloadCompleted_4() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___OnDownloadCompleted_4)); }
	inline DownloadCompletedHanderler_t2341399827 * get_OnDownloadCompleted_4() const { return ___OnDownloadCompleted_4; }
	inline DownloadCompletedHanderler_t2341399827 ** get_address_of_OnDownloadCompleted_4() { return &___OnDownloadCompleted_4; }
	inline void set_OnDownloadCompleted_4(DownloadCompletedHanderler_t2341399827 * value)
	{
		___OnDownloadCompleted_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownloadCompleted_4, value);
	}

	inline static int32_t get_offset_of_OnShareCompleted_5() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___OnShareCompleted_5)); }
	inline ShareCompletedHanderler_t523036664 * get_OnShareCompleted_5() const { return ___OnShareCompleted_5; }
	inline ShareCompletedHanderler_t523036664 ** get_address_of_OnShareCompleted_5() { return &___OnShareCompleted_5; }
	inline void set_OnShareCompleted_5(ShareCompletedHanderler_t523036664 * value)
	{
		___OnShareCompleted_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnShareCompleted_5, value);
	}

	inline static int32_t get_offset_of_instnace_6() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___instnace_6)); }
	inline NativeShare_t1150945090 * get_instnace_6() const { return ___instnace_6; }
	inline NativeShare_t1150945090 ** get_address_of_instnace_6() { return &___instnace_6; }
	inline void set_instnace_6(NativeShare_t1150945090 * value)
	{
		___instnace_6 = value;
		Il2CppCodeGenWriteBarrier(&___instnace_6, value);
	}

	inline static int32_t get_offset_of_shareObject_7() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___shareObject_7)); }
	inline GameObject_t1756533147 * get_shareObject_7() const { return ___shareObject_7; }
	inline GameObject_t1756533147 ** get_address_of_shareObject_7() { return &___shareObject_7; }
	inline void set_shareObject_7(GameObject_t1756533147 * value)
	{
		___shareObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___shareObject_7, value);
	}

	inline static int32_t get_offset_of_listenner_8() { return static_cast<int32_t>(offsetof(NativeShare_t1150945090_StaticFields, ___listenner_8)); }
	inline GameObject_t1756533147 * get_listenner_8() const { return ___listenner_8; }
	inline GameObject_t1756533147 ** get_address_of_listenner_8() { return &___listenner_8; }
	inline void set_listenner_8(GameObject_t1756533147 * value)
	{
		___listenner_8 = value;
		Il2CppCodeGenWriteBarrier(&___listenner_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
