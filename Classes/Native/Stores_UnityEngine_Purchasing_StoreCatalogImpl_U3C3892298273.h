﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action`1<System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_t1113395782;
// UnityEngine.Purchasing.StoreCatalogImpl
struct StoreCatalogImpl_t946505162;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreCatalogImpl/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t3892298273  : public Il2CppObject
{
public:
	// System.Action`1<System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>> UnityEngine.Purchasing.StoreCatalogImpl/<>c__DisplayClass10_0::callback
	Action_1_t1113395782 * ___callback_0;
	// UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.StoreCatalogImpl/<>c__DisplayClass10_0::<>4__this
	StoreCatalogImpl_t946505162 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t3892298273, ___callback_0)); }
	inline Action_1_t1113395782 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t1113395782 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t1113395782 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t3892298273, ___U3CU3E4__this_1)); }
	inline StoreCatalogImpl_t946505162 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline StoreCatalogImpl_t946505162 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(StoreCatalogImpl_t946505162 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
