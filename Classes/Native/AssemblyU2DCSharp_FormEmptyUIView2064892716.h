﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FormEmptyUIView
struct  FormEmptyUIView_t2064892716  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 FormEmptyUIView::index
	int32_t ___index_2;
	// System.String FormEmptyUIView::positionName
	String_t* ___positionName_3;
	// UnityEngine.UI.Button FormEmptyUIView::BaseButton
	Button_t2872111280 * ___BaseButton_4;
	// UnityEngine.UI.Text FormEmptyUIView::PositionText
	Text_t356221433 * ___PositionText_5;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(FormEmptyUIView_t2064892716, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_positionName_3() { return static_cast<int32_t>(offsetof(FormEmptyUIView_t2064892716, ___positionName_3)); }
	inline String_t* get_positionName_3() const { return ___positionName_3; }
	inline String_t** get_address_of_positionName_3() { return &___positionName_3; }
	inline void set_positionName_3(String_t* value)
	{
		___positionName_3 = value;
		Il2CppCodeGenWriteBarrier(&___positionName_3, value);
	}

	inline static int32_t get_offset_of_BaseButton_4() { return static_cast<int32_t>(offsetof(FormEmptyUIView_t2064892716, ___BaseButton_4)); }
	inline Button_t2872111280 * get_BaseButton_4() const { return ___BaseButton_4; }
	inline Button_t2872111280 ** get_address_of_BaseButton_4() { return &___BaseButton_4; }
	inline void set_BaseButton_4(Button_t2872111280 * value)
	{
		___BaseButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___BaseButton_4, value);
	}

	inline static int32_t get_offset_of_PositionText_5() { return static_cast<int32_t>(offsetof(FormEmptyUIView_t2064892716, ___PositionText_5)); }
	inline Text_t356221433 * get_PositionText_5() const { return ___PositionText_5; }
	inline Text_t356221433 ** get_address_of_PositionText_5() { return &___PositionText_5; }
	inline void set_PositionText_5(Text_t356221433 * value)
	{
		___PositionText_5 = value;
		Il2CppCodeGenWriteBarrier(&___PositionText_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
