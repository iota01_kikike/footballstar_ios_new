﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_Tacticsoft_TableView1276614623.h"

// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t1370245513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectPositionCell
struct  SelectPositionCell_t908287989  : public TableViewCell_t1276614623
{
public:
	// System.Int32 SelectPositionCell::_row
	int32_t ____row_2;
	// UnityEngine.UI.Button[] SelectPositionCell::Button
	ButtonU5BU5D_t3071100561* ___Button_3;
	// UnityEngine.UI.Text[] SelectPositionCell::NameText
	TextU5BU5D_t4216439300* ___NameText_4;
	// UnityEngine.UI.Text[] SelectPositionCell::OverallText
	TextU5BU5D_t4216439300* ___OverallText_5;
	// UnityEngine.UI.Image[] SelectPositionCell::FlagImage
	ImageU5BU5D_t590162004* ___FlagImage_6;
	// UnityEngine.GameObject[] SelectPositionCell::OriginHead
	GameObjectU5BU5D_t3057952154* ___OriginHead_7;
	// UnityEngine.UI.Image[] SelectPositionCell::BlindImage
	ImageU5BU5D_t590162004* ___BlindImage_8;
	// System.Action`2<System.Int32,System.Int32> SelectPositionCell::Callback
	Action_2_t1370245513 * ___Callback_9;

public:
	inline static int32_t get_offset_of__row_2() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ____row_2)); }
	inline int32_t get__row_2() const { return ____row_2; }
	inline int32_t* get_address_of__row_2() { return &____row_2; }
	inline void set__row_2(int32_t value)
	{
		____row_2 = value;
	}

	inline static int32_t get_offset_of_Button_3() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___Button_3)); }
	inline ButtonU5BU5D_t3071100561* get_Button_3() const { return ___Button_3; }
	inline ButtonU5BU5D_t3071100561** get_address_of_Button_3() { return &___Button_3; }
	inline void set_Button_3(ButtonU5BU5D_t3071100561* value)
	{
		___Button_3 = value;
		Il2CppCodeGenWriteBarrier(&___Button_3, value);
	}

	inline static int32_t get_offset_of_NameText_4() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___NameText_4)); }
	inline TextU5BU5D_t4216439300* get_NameText_4() const { return ___NameText_4; }
	inline TextU5BU5D_t4216439300** get_address_of_NameText_4() { return &___NameText_4; }
	inline void set_NameText_4(TextU5BU5D_t4216439300* value)
	{
		___NameText_4 = value;
		Il2CppCodeGenWriteBarrier(&___NameText_4, value);
	}

	inline static int32_t get_offset_of_OverallText_5() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___OverallText_5)); }
	inline TextU5BU5D_t4216439300* get_OverallText_5() const { return ___OverallText_5; }
	inline TextU5BU5D_t4216439300** get_address_of_OverallText_5() { return &___OverallText_5; }
	inline void set_OverallText_5(TextU5BU5D_t4216439300* value)
	{
		___OverallText_5 = value;
		Il2CppCodeGenWriteBarrier(&___OverallText_5, value);
	}

	inline static int32_t get_offset_of_FlagImage_6() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___FlagImage_6)); }
	inline ImageU5BU5D_t590162004* get_FlagImage_6() const { return ___FlagImage_6; }
	inline ImageU5BU5D_t590162004** get_address_of_FlagImage_6() { return &___FlagImage_6; }
	inline void set_FlagImage_6(ImageU5BU5D_t590162004* value)
	{
		___FlagImage_6 = value;
		Il2CppCodeGenWriteBarrier(&___FlagImage_6, value);
	}

	inline static int32_t get_offset_of_OriginHead_7() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___OriginHead_7)); }
	inline GameObjectU5BU5D_t3057952154* get_OriginHead_7() const { return ___OriginHead_7; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_OriginHead_7() { return &___OriginHead_7; }
	inline void set_OriginHead_7(GameObjectU5BU5D_t3057952154* value)
	{
		___OriginHead_7 = value;
		Il2CppCodeGenWriteBarrier(&___OriginHead_7, value);
	}

	inline static int32_t get_offset_of_BlindImage_8() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___BlindImage_8)); }
	inline ImageU5BU5D_t590162004* get_BlindImage_8() const { return ___BlindImage_8; }
	inline ImageU5BU5D_t590162004** get_address_of_BlindImage_8() { return &___BlindImage_8; }
	inline void set_BlindImage_8(ImageU5BU5D_t590162004* value)
	{
		___BlindImage_8 = value;
		Il2CppCodeGenWriteBarrier(&___BlindImage_8, value);
	}

	inline static int32_t get_offset_of_Callback_9() { return static_cast<int32_t>(offsetof(SelectPositionCell_t908287989, ___Callback_9)); }
	inline Action_2_t1370245513 * get_Callback_9() const { return ___Callback_9; }
	inline Action_2_t1370245513 ** get_address_of_Callback_9() { return &___Callback_9; }
	inline void set_Callback_9(Action_2_t1370245513 * value)
	{
		___Callback_9 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
