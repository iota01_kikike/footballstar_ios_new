﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// EZObjectPools.EZObjectPool
struct EZObjectPool_t3968219684;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EZObjectPools.PooledObject
struct  PooledObject_t2079175126  : public MonoBehaviour_t1158329972
{
public:
	// EZObjectPools.EZObjectPool EZObjectPools.PooledObject::ParentPool
	EZObjectPool_t3968219684 * ___ParentPool_2;

public:
	inline static int32_t get_offset_of_ParentPool_2() { return static_cast<int32_t>(offsetof(PooledObject_t2079175126, ___ParentPool_2)); }
	inline EZObjectPool_t3968219684 * get_ParentPool_2() const { return ___ParentPool_2; }
	inline EZObjectPool_t3968219684 ** get_address_of_ParentPool_2() { return &___ParentPool_2; }
	inline void set_ParentPool_2(EZObjectPool_t3968219684 * value)
	{
		___ParentPool_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParentPool_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
