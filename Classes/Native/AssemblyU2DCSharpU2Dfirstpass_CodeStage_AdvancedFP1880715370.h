﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_CustomYieldInstruction1786092740.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.WaitForSecondsUnscaled
struct  WaitForSecondsUnscaled_t1880715370  : public CustomYieldInstruction_t1786092740
{
public:
	// System.Single CodeStage.AdvancedFPSCounter.WaitForSecondsUnscaled::waitTime
	float ___waitTime_0;
	// System.Single CodeStage.AdvancedFPSCounter.WaitForSecondsUnscaled::runUntil
	float ___runUntil_1;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(WaitForSecondsUnscaled_t1880715370, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}

	inline static int32_t get_offset_of_runUntil_1() { return static_cast<int32_t>(offsetof(WaitForSecondsUnscaled_t1880715370, ___runUntil_1)); }
	inline float get_runUntil_1() const { return ___runUntil_1; }
	inline float* get_address_of_runUntil_1() { return &___runUntil_1; }
	inline void set_runUntil_1(float value)
	{
		___runUntil_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
