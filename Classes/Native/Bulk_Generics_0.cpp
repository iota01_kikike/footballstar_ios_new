﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2014832765.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1565154527.h"
#include "mscorlib_System_Double4078015681.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3853983590.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2691184179.h"
#include "mscorlib_System_Int64909078037.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen176588141.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "mscorlib_System_Single2076509932.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3931788163.h"
#include "mscorlib_System_UInt322149682021.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen396335760.h"
#include "mscorlib_System_UInt642909196914.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1517212764.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1168894472.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813723.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1890955609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1441277371.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3730106434.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2567307023.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen52710985.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3807911007.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen272458604.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1393335608.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1045017316.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936567.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3925803634.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "mscorlib_System_Type1303803226.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "mscorlib_System_String2029220233.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2833266637.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen456598886.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3659029185.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3160108754.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2067571757.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3199111798.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2106574801.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1705766462.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g613229465.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2082658550.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g990121553.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1186869890.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1648829745.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g556292748.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2937657178.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1845120181.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1905502397.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen102288586.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3304718885.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1672798003.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g580261006.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3065187631.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1972650634.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2158331857.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1622518059.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g529981062.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1635203449.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g542666452.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_ge16126469.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3261425374.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2168888377.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3418705418.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4036277265.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3423337902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ListPool_1_gen2027455341.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ObjectPool_1_gen3678846538.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3425156178.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ListPool_1_gen542264864.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ObjectPool_1_gen2193656061.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1939965701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ObjectPool_1_gen14758110.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449.h"
#include "AssemblyU2DCSharp_SA_NonMonoSingleton_1_gen2015726709.h"
#include "AssemblyU2DCSharp_SA_Singleton_1_gen556325241.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1463927430.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T958855109.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Action_1_gen2800324759.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2998525377.h"
#include "mscorlib_System_Action_1_gen1815011612.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_2013212230.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen1873676830.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen1878309314.h"
#include "mscorlib_System_Action_1_gen2755832024.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "mscorlib_System_Action_1_gen2289103034.h"
#include "Stores_UnityEngine_Purchasing_RestoreTransactionID2487303652.h"
#include "mscorlib_System_Action_1_gen2045506962.h"
#include "System_Core_System_Action_2_gen2506932584.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response167331027.h"
#include "System_Core_System_Action_2_gen1370807161.h"
#include "System_Core_System_Action_2_gen2272063304.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2671736816.h"
#include "System_Core_System_Action_2_gen3649729058.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2446036638.h"
#include "System_Core_System_Action_2_gen1907880187.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2790035381.h"
#include "System_Core_System_Action_2_gen1158962578.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "System_Core_System_Action_2_gen1370245513.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_3_gen206039628.h"
#include "mscorlib_System_DateTime693205669.h"
#include "System_Core_System_Action_3_gen2202283254.h"
#include "System_Core_System_Action_3_gen1115657183.h"
#include "System_Core_System_Action_3_gen4226502927.h"
#include "System_Core_System_Action_3_gen863926345.h"
#include "Stores_UnityEngine_Purchasing_ValidateReceiptState4359597.h"

// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t2014832765;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t1565154527;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t3853983590;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t2691184179;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t176588141;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3858616074;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t3931788163;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t396335760;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t3802498217;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t1517212764;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1168894472;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t4025813721;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t4025813722;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t4025813723;
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t1890955609;
// DG.Tweening.Core.DOSetter`1<System.Double>
struct DOSetter_1_t1441277371;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t3730106434;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t2567307023;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t52710985;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t3734738918;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t3807911007;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t272458604;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t3678621061;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1393335608;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1045017316;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3901936565;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t3901936566;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t3901936567;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t3925803634;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// DG.Tweening.Tween
struct Tween_t278478013;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t456598886;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3160108754;
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3199111798;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1705766462;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t2082658550;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct TweenerCore_3_t1648829745;
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t2937657178;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t102288586;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t1672798003;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t3065187631;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3250868854;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t1622518059;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1635203449;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1108663466;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3261425374;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t2833266637;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3659029185;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2067571757;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2106574801;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t613229465;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t990121553;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t1186869890;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct ABSTweenPlugin_3_t556292748;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t1845120181;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t1905502397;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3304718885;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct ABSTweenPlugin_3_t580261006;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct ABSTweenPlugin_3_t1972650634;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2158331857;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t529981062;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_t542666452;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t16126469;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2168888377;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3418705418;
// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t4036277265;
// DG.Tweening.TweenCallback`1<System.Single>
struct TweenCallback_1_t3423337902;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// ObjectPool`1<System.Object>
struct ObjectPool_1_t14758110;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// SA_NonMonoSingleton`1<System.Object>
struct SA_NonMonoSingleton_1_t2015726709;
// SA_Singleton`1<System.Object>
struct SA_Singleton_1_t556325241;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Component
struct Component_t3819376471;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t1463927430;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t958855109;
// System.NotImplementedException
struct NotImplementedException_t2785117854;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>
struct Action_1_t2800324759;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t1815011612;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Single>
struct Action_1_t1878309314;
// System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>
struct Action_1_t2755832024;
// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>
struct Action_1_t2289103034;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t2045506962;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>
struct Action_2_t2506932584;
// System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>
struct Action_2_t1370807161;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>
struct Action_2_t2272063304;
// System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>
struct Action_2_t3649729058;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t1907880187;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>
struct Action_2_t2790035381;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>
struct Action_2_t1158962578;
// System.Action`2<System.Int32,System.Int32>
struct Action_2_t1370245513;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Action`3<System.Boolean,System.Object,System.DateTime>
struct Action_3_t206039628;
// System.Action`3<System.Boolean,System.Object,System.Object>
struct Action_3_t2202283254;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t1115657183;
// System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>
struct Action_3_t4226502927;
// System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>
struct Action_3_t863926345;
extern Il2CppClass* Color2_t232726623_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m736671164_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId;
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId;
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m681046145_MetadataUsageId;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1803465319_MetadataUsageId;
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral552937255;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3004544632_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1422288232;
extern const uint32_t TweenerCore_3_ChangeValues_m266369415_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m3163669337_MetadataUsageId;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1180323095_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1112518902_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2531960211_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2094047477_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2533780907_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3831419980_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2571270975_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3448887169_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1918761711_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2139817550_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m1168812923_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m1266531737_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1198377687_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3792476854_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m4189518099_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m1363945525_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m749792945_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3971101479_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m536230576_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m3838998779_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2015486485_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1849842838_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3084865231_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m114545046_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2802707620_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m4074697550_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m806842373_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m4204159460_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m621478568_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m418004407_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1875838422_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m570000243_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m1883068437_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2978185163_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3818406250_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m592431499_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m44760597_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3146716579_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m65087268_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2618008744_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2005239399_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3999115877_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2916579489_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m2776308754_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m839318221_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2925494578_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3629558540_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m231619286_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1025968471_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m1594244980_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3855338832_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m3640631583_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m508084142_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2621469739_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2299353629_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1012438200_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2708020849_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m3787576666_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3391320434_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1432615606_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3270859719_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m2725288820_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m841334684_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m4171196319_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m217970222_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m624671403_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m2965636509_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId;
extern const uint32_t TweenerCore_3__ctor_m1407812639_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1286718126_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId;
extern const uint32_t TweenerCore_3_ChangeValues_m868439851_MetadataUsageId;
extern const uint32_t TweenerCore_3_Validate_m3552936477_MetadataUsageId;
extern const uint32_t TweenerCore_3_ApplyTween_m140499649_MetadataUsageId;
extern const uint32_t TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId;
extern const uint32_t TweenCallback_1_BeginInvoke_m3843094324_MetadataUsageId;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral273729679;
extern const uint32_t ObjectPool_1_Release_m727363400_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const uint32_t SA_Singleton_1_get_Instance_m1640053663_MetadataUsageId;
extern const uint32_t SA_Singleton_1_get_IsDestroyed_m2800413832_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2__ctor_m2422343709_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m2282389750_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_set_Item_m2513745515_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_Add_m1306324747_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_Clear_m3155495606_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_Contains_m1533726473_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_CopyTo_m1781093487_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_MetadataUsageId;
extern const uint32_t ThreadSafeDictionary_2_Remove_m1651487354_MetadataUsageId;
extern Il2CppClass* FPSLevel_t2998525377_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m726771024_MetadataUsageId;
extern Il2CppClass* InitializationStatus_t2013212230_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3701353426_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m1720726178_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m1617223338_MetadataUsageId;
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1319991882_MetadataUsageId;
extern Il2CppClass* RestoreTransactionIDState_t2487303652_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1248823956_MetadataUsageId;
extern const uint32_t Action_1_BeginInvoke_m402802490_MetadataUsageId;
extern Il2CppClass* ResponseStatus_t167331027_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1999404200_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m2670173333_MetadataUsageId;
extern Il2CppClass* SavedGameRequestStatus_t2671736816_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3524880053_MetadataUsageId;
extern Il2CppClass* SelectUIStatus_t2446036638_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m753119779_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m1925898598_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m410485158_MetadataUsageId;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m772046713_MetadataUsageId;
extern const uint32_t Action_2_BeginInvoke_m2329746402_MetadataUsageId;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1436240799_MetadataUsageId;
extern const uint32_t Action_3_BeginInvoke_m4234685015_MetadataUsageId;
extern const uint32_t Action_3_BeginInvoke_m4030220740_MetadataUsageId;
extern Il2CppClass* ValidateReceiptState_t4359597_il2cpp_TypeInfo_var;
extern const uint32_t Action_3_BeginInvoke_m1518280454_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t38854645  m_Items[1];

public:
	inline KeyValuePair_2_t38854645  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t38854645 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t38854645  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t38854645  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t38854645 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t38854645  value)
	{
		m_Items[index] = value;
	}
};


// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method);
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method);
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3090779465_gshared (TweenCallback_1_t3423337902 * __this, float ___value0, const MethodInfo* method);
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, const MethodInfo* method);
// System.Void System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>::Invoke(T)
extern "C"  void Action_1_Invoke_m3334985867_gshared (Action_1_t2800324759 * __this, uint8_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m3402415237_gshared (Action_1_t1815011612 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3352874125_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m4180501989_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
// System.Void System.Action`1<System.Single>::Invoke(T)
extern "C"  void Action_1_Invoke_m3661933365_gshared (Action_1_t1878309314 * __this, float ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T)
extern "C"  void Action_1_Invoke_m2983653028_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>::Invoke(T)
extern "C"  void Action_1_Invoke_m188738021_gshared (Action_1_t2289103034 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void Action_1_Invoke_m1071418069_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m4107579927_gshared (Action_2_t2506932584 * __this, int32_t ___arg10, bool ___arg21, const MethodInfo* method);
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m65990118_gshared (Action_2_t1370807161 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1770798410_gshared (Action_2_t2272063304 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2875204762_gshared (Action_2_t3649729058 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2365036873_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m352317182_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3143687639_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1462475090_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Int32,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3770950199_gshared (Action_2_t1370245513 * __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2406183663_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
// System.Void System.Action`3<System.Boolean,System.Object,System.DateTime>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m694522710_gshared (Action_3_t206039628 * __this, bool ___arg10, Il2CppObject * ___arg21, DateTime_t693205669  ___arg32, const MethodInfo* method);
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m138287784_gshared (Action_3_t2202283254 * __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3309590603_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m2533654301_gshared (Action_3_t4226502927 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3171976315_gshared (Action_3_t863926345 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);

// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
#define DOGetter_1_Invoke_m1748194493(__this, method) ((  Color2_t232726623  (*) (DOGetter_1_t2014832765 *, const MethodInfo*))DOGetter_1_Invoke_m1748194493_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
#define DOGetter_1_Invoke_m4107339380(__this, method) ((  double (*) (DOGetter_1_t1565154527 *, const MethodInfo*))DOGetter_1_Invoke_m4107339380_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
#define DOGetter_1_Invoke_m2109831897(__this, method) ((  int32_t (*) (DOGetter_1_t3853983590 *, const MethodInfo*))DOGetter_1_Invoke_m2109831897_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
#define DOGetter_1_Invoke_m111587642(__this, method) ((  int64_t (*) (DOGetter_1_t2691184179 *, const MethodInfo*))DOGetter_1_Invoke_m111587642_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
#define DOGetter_1_Invoke_m882816534(__this, method) ((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))DOGetter_1_Invoke_m882816534_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
#define DOGetter_1_Invoke_m3647275797(__this, method) ((  float (*) (DOGetter_1_t3858616074 *, const MethodInfo*))DOGetter_1_Invoke_m3647275797_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
#define DOGetter_1_Invoke_m370264108(__this, method) ((  uint32_t (*) (DOGetter_1_t3931788163 *, const MethodInfo*))DOGetter_1_Invoke_m370264108_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
#define DOGetter_1_Invoke_m2202291235(__this, method) ((  uint64_t (*) (DOGetter_1_t396335760 *, const MethodInfo*))DOGetter_1_Invoke_m2202291235_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
#define DOGetter_1_Invoke_m222071538(__this, method) ((  Color_t2020392075  (*) (DOGetter_1_t3802498217 *, const MethodInfo*))DOGetter_1_Invoke_m222071538_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
#define DOGetter_1_Invoke_m1844179403(__this, method) ((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))DOGetter_1_Invoke_m1844179403_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
#define DOGetter_1_Invoke_m205270839(__this, method) ((  Rect_t3681755626  (*) (DOGetter_1_t1168894472 *, const MethodInfo*))DOGetter_1_Invoke_m205270839_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
#define DOGetter_1_Invoke_m3450181142(__this, method) ((  Vector2_t2243707579  (*) (DOGetter_1_t4025813721 *, const MethodInfo*))DOGetter_1_Invoke_m3450181142_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
#define DOGetter_1_Invoke_m3486946741(__this, method) ((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))DOGetter_1_Invoke_m3486946741_gshared)(__this, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
#define DOGetter_1_Invoke_m3680105936(__this, method) ((  Vector4_t2243707581  (*) (DOGetter_1_t4025813723 *, const MethodInfo*))DOGetter_1_Invoke_m3680105936_gshared)(__this, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
#define DOSetter_1_Invoke_m3163525136(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1890955609 *, Color2_t232726623 , const MethodInfo*))DOSetter_1_Invoke_m3163525136_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
#define DOSetter_1_Invoke_m1726747861(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1441277371 *, double, const MethodInfo*))DOSetter_1_Invoke_m1726747861_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
#define DOSetter_1_Invoke_m748616666(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3730106434 *, int32_t, const MethodInfo*))DOSetter_1_Invoke_m748616666_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
#define DOSetter_1_Invoke_m2393688041(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t2567307023 *, int64_t, const MethodInfo*))DOSetter_1_Invoke_m2393688041_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
#define DOSetter_1_Invoke_m1999305269(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t52710985 *, Il2CppObject *, const MethodInfo*))DOSetter_1_Invoke_m1999305269_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
#define DOSetter_1_Invoke_m184768384(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3734738918 *, float, const MethodInfo*))DOSetter_1_Invoke_m184768384_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
#define DOSetter_1_Invoke_m236045679(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3807911007 *, uint32_t, const MethodInfo*))DOSetter_1_Invoke_m236045679_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
#define DOSetter_1_Invoke_m4121538054(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t272458604 *, uint64_t, const MethodInfo*))DOSetter_1_Invoke_m4121538054_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
#define DOSetter_1_Invoke_m3892954541(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3678621061 *, Color_t2020392075 , const MethodInfo*))DOSetter_1_Invoke_m3892954541_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
#define DOSetter_1_Invoke_m903072570(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1393335608 *, Quaternion_t4030073918 , const MethodInfo*))DOSetter_1_Invoke_m903072570_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
#define DOSetter_1_Invoke_m1386366628(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1045017316 *, Rect_t3681755626 , const MethodInfo*))DOSetter_1_Invoke_m1386366628_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
#define DOSetter_1_Invoke_m2956451219(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936565 *, Vector2_t2243707579 , const MethodInfo*))DOSetter_1_Invoke_m2956451219_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
#define DOSetter_1_Invoke_m4169715988(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936566 *, Vector3_t2243707580 , const MethodInfo*))DOSetter_1_Invoke_m4169715988_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
#define DOSetter_1_Invoke_m212800661(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3901936567 *, Vector4_t2243707581 , const MethodInfo*))DOSetter_1_Invoke_m212800661_gshared)(__this, ___pNewValue0, method)
// System.Void DG.Tweening.Tweener::.ctor()
extern "C"  void Tweener__ctor_m102043033 (Tweener_t760404022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern "C"  void Debugger_LogWarning_m1294243619 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Tween::Reset()
extern "C"  void Tween_Reset_m2158413979 (Tween_t278478013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.ColorOptions::Reset()
extern "C"  void ColorOptions_Reset_m2624799597 (ColorOptions_t2213017305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.NoOptions::Reset()
extern "C"  void NoOptions_Reset_m532649001 (NoOptions_t2508431845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.StringOptions::Reset()
extern "C"  void StringOptions_Reset_m530675817 (StringOptions_t2885323933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.FloatOptions::Reset()
extern "C"  void FloatOptions_Reset_m1378398748 (FloatOptions_t1421548266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.UintOptions::Reset()
extern "C"  void UintOptions_Reset_m3831863362 (UintOptions_t2267095136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.QuaternionOptions::Reset()
extern "C"  void QuaternionOptions_Reset_m4116534850 (QuaternionOptions_t466049668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.RectOptions::Reset()
extern "C"  void RectOptions_Reset_m297368492 (RectOptions_t3393635162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.VectorOptions::Reset()
extern "C"  void VectorOptions_Reset_m245768289 (VectorOptions_t293385261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.PathOptions::Reset()
extern "C"  void PathOptions_Reset_m4016313393 (PathOptions_t2659884781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Options.Vector3ArrayOptions::Reset()
extern "C"  void Vector3ArrayOptions_Reset_m277565119 (Vector3ArrayOptions_t2672570171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
#define TweenCallback_1_Invoke_m3615052821(__this, ___value0, method) ((  void (*) (TweenCallback_1_t3418705418 *, int32_t, const MethodInfo*))TweenCallback_1_Invoke_m3615052821_gshared)(__this, ___value0, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
#define TweenCallback_1_Invoke_m872757678(__this, ___value0, method) ((  void (*) (TweenCallback_1_t4036277265 *, Il2CppObject *, const MethodInfo*))TweenCallback_1_Invoke_m872757678_gshared)(__this, ___value0, method)
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
#define TweenCallback_1_Invoke_m3090779465(__this, ___value0, method) ((  void (*) (TweenCallback_1_t3423337902 *, float, const MethodInfo*))TweenCallback_1_Invoke_m3090779465_gshared)(__this, ___value0, method)
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m3900584722 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t1021602117 * Object_FindObjectOfType_m2330404063 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m498247354 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2136705809 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m808189835 (NotImplementedException_t2785117854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
#define ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t958855109 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419_gshared)(__this, ___key0, method)
// System.Void System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>::Invoke(T)
#define Action_1_Invoke_m3334985867(__this, ___obj0, method) ((  void (*) (Action_1_t2800324759 *, uint8_t, const MethodInfo*))Action_1_Invoke_m3334985867_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::Invoke(T)
#define Action_1_Invoke_m3402415237(__this, ___obj0, method) ((  void (*) (Action_1_t1815011612 *, int32_t, const MethodInfo*))Action_1_Invoke_m3402415237_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Boolean>::Invoke(T)
#define Action_1_Invoke_m3662000152(__this, ___obj0, method) ((  void (*) (Action_1_t3627374100 *, bool, const MethodInfo*))Action_1_Invoke_m3662000152_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Int32>::Invoke(T)
#define Action_1_Invoke_m3352874125(__this, ___obj0, method) ((  void (*) (Action_1_t1873676830 *, int32_t, const MethodInfo*))Action_1_Invoke_m3352874125_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Object>::Invoke(T)
#define Action_1_Invoke_m4180501989(__this, ___obj0, method) ((  void (*) (Action_1_t2491248677 *, Il2CppObject *, const MethodInfo*))Action_1_Invoke_m4180501989_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<System.Single>::Invoke(T)
#define Action_1_Invoke_m3661933365(__this, ___obj0, method) ((  void (*) (Action_1_t1878309314 *, float, const MethodInfo*))Action_1_Invoke_m3661933365_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T)
#define Action_1_Invoke_m2983653028(__this, ___obj0, method) ((  void (*) (Action_1_t2755832024 *, int32_t, const MethodInfo*))Action_1_Invoke_m2983653028_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>::Invoke(T)
#define Action_1_Invoke_m188738021(__this, ___obj0, method) ((  void (*) (Action_1_t2289103034 *, int32_t, const MethodInfo*))Action_1_Invoke_m188738021_gshared)(__this, ___obj0, method)
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
#define Action_1_Invoke_m1071418069(__this, ___obj0, method) ((  void (*) (Action_1_t2045506962 *, Vector3_t2243707580 , const MethodInfo*))Action_1_Invoke_m1071418069_gshared)(__this, ___obj0, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>::Invoke(T1,T2)
#define Action_2_Invoke_m4107579927(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2506932584 *, int32_t, bool, const MethodInfo*))Action_2_Invoke_m4107579927_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m65990118(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1370807161 *, int32_t, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m65990118_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m1770798410(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2272063304 *, int32_t, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m1770798410_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m2875204762(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3649729058 *, int32_t, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m2875204762_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(T1,T2)
#define Action_2_Invoke_m2365036873(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1907880187 *, bool, int32_t, const MethodInfo*))Action_2_Invoke_m2365036873_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m352317182(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2525452034 *, bool, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m352317182_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T1,T2)
#define Action_2_Invoke_m3143687639(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2790035381 *, bool, int32_t, const MethodInfo*))Action_2_Invoke_m3143687639_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::Invoke(T1,T2)
#define Action_2_Invoke_m1462475090(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1158962578 *, bool, int32_t, const MethodInfo*))Action_2_Invoke_m1462475090_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Int32,System.Int32>::Invoke(T1,T2)
#define Action_2_Invoke_m3770950199(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1370245513 *, int32_t, int32_t, const MethodInfo*))Action_2_Invoke_m3770950199_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
#define Action_2_Invoke_m2406183663(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2572051853 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_2_Invoke_m2406183663_gshared)(__this, ___arg10, ___arg21, method)
// System.Void System.Action`3<System.Boolean,System.Object,System.DateTime>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m694522710(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t206039628 *, bool, Il2CppObject *, DateTime_t693205669 , const MethodInfo*))Action_3_Invoke_m694522710_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m138287784(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2202283254 *, bool, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m138287784_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m3309590603(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t1115657183 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m3309590603_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2533654301(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t4226502927 *, Il2CppObject *, int32_t, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m2533654301_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m3171976315(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t863926345 *, Il2CppObject *, int32_t, Il2CppObject *, const MethodInfo*))Action_3_Invoke_m3171976315_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2103134696_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1748194493((DOGetter_1_t2014832765 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color2_t232726623  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color2_t232726623  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3134053505_gshared (DOGetter_1_t2014832765 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  Color2_t232726623  DOGetter_1_EndInvoke_m3467158547_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color2_t232726623 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m188978433_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m4107339380((DOGetter_1_t1565154527 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Double>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2666210620_gshared (DOGetter_1_t1565154527 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double DOGetter_1_EndInvoke_m4233629864_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2793538546_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2109831897((DOGetter_1_t3853983590 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1887709557_gshared (DOGetter_1_t3853983590 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t DOGetter_1_EndInvoke_m912235863_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1068054853_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m111587642((DOGetter_1_t2691184179 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2803366776_gshared (DOGetter_1_t2691184179 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t DOGetter_1_EndInvoke_m1345976682_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2292700265_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m882816534((DOGetter_1_t176588141 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3769142830_gshared (DOGetter_1_t176588141 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * DOGetter_1_EndInvoke_m4273483522_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3288120768_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3647275797((DOGetter_1_t3858616074 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1690324125_gshared (DOGetter_1_t3858616074 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float DOGetter_1_EndInvoke_m2074726563_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1362595967_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m370264108((DOGetter_1_t3931788163 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3143608020_gshared (DOGetter_1_t3931788163 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t DOGetter_1_EndInvoke_m859660492_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3368190962_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2202291235((DOGetter_1_t396335760 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1007973739_gshared (DOGetter_1_t396335760 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t DOGetter_1_EndInvoke_m3446459513_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m4239784889_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m222071538((DOGetter_1_t3802498217 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color_t2020392075  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t2020392075  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3894549130_gshared (DOGetter_1_t3802498217 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t2020392075  DOGetter_1_EndInvoke_m2918527242_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t2020392075 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3923641982_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1844179403((DOGetter_1_t1517212764 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2685409055_gshared (DOGetter_1_t1517212764 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  Quaternion_t4030073918  DOGetter_1_EndInvoke_m4117334265_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Quaternion_t4030073918 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2167974952_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m205270839((DOGetter_1_t1168894472 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2542487891_gshared (DOGetter_1_t1168894472 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  Rect_t3681755626  DOGetter_1_EndInvoke_m47398349_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Rect_t3681755626 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2108428307_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3450181142((DOGetter_1_t4025813721 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2030350446_gshared (DOGetter_1_t4025813721 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t2243707579  DOGetter_1_EndInvoke_m1102230086_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t2243707579 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1323234132_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3486946741((DOGetter_1_t4025813722 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2677033165_gshared (DOGetter_1_t4025813722 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t2243707580  DOGetter_1_EndInvoke_m3095315399_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector3_t2243707580 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m664413081_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3680105936((DOGetter_1_t4025813723 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1764844456_gshared (DOGetter_1_t4025813723 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  Vector4_t2243707581  DOGetter_1_EndInvoke_m2636742600_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector4_t2243707581 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1733732172_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3163525136((DOSetter_1_t1890955609 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3915298133_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color2_t232726623_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2796185966_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3181129925_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1726747861((DOSetter_1_t1441277371 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1311537432_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t4078015681_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2118077943_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2404517806_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m748616666((DOSetter_1_t3730106434 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3860168801_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3034368096_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1460312617_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2393688041((DOSetter_1_t2567307023 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m736671164_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m736671164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m977414887_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m4187492813_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1999305269((DOSetter_1_t52710985 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m4262435930_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pNewValue0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m936279787_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2613085532_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m184768384((DOSetter_1_t3734738918 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3656965161_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1222853090_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2057781435_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m236045679((DOSetter_1_t3807911007 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2436040648_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2552133209_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1650381910_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4121538054((DOSetter_1_t272458604 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2966874951_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m480288392_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3548377173_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3892954541((DOSetter_1_t3678621061 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2663926534_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2243481199_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2116426170_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m903072570((DOSetter_1_t1393335608 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1885597315_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2987470368_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2570159628_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1386366628((DOSetter_1_t1045017316 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3580342655_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t3681755626_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1624612738_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1453722671_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2956451219((DOSetter_1_t3901936565 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2720752834_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3251197393_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m668528496_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4169715988((DOSetter_1_t3901936566 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m681046145_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m681046145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m949315410_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m9707445_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m212800661((DOSetter_1_t3901936567 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2980016964_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1324394315_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1803465319_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1803465319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3004544632_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3004544632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m157243687_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3909355140_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m266369415_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m266369415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , Color2_t232726623 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1733021291_gshared (TweenerCore_3_t3925803634 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3925803634 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2833266637 *)L_0, (TweenerCore_3_t3925803634 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m731417792_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2833266637 * L_1 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3925803634 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2833266637 *)L_1, (TweenerCore_3_t3925803634 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305 * L_2 = (ColorOptions_t2213017305 *)__this->get_address_of_plugOptions_56();
		ColorOptions_Reset_m2624799597((ColorOptions_t2213017305 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t2014832765 *)NULL);
		__this->set_setter_58((DOSetter_1_t1890955609 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3163669337_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3163669337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2014832765 * L_0 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2014832765 *)L_0);
		((  Color2_t232726623  (*) (DOGetter_1_t2014832765 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t2014832765 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2044147238_gshared (TweenerCore_3_t3925803634 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m4133666392_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3682676813_gshared (TweenerCore_3_t3925803634 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2833266637 * L_5 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_8 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_9 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_10 = V_0;
		Color2_t232726623  L_11 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_12 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2014832765 *)L_8, (DOSetter_1_t1890955609 *)L_9, (float)L_10, (Color2_t232726623 )L_11, (Color2_t232726623 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2833266637 * L_16 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_19 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_20 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_21 = V_0;
		Color2_t232726623  L_22 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_23 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2014832765 *)L_19, (DOSetter_1_t1890955609 *)L_20, (float)L_21, (Color2_t232726623 )L_22, (Color2_t232726623 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1180323095_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1180323095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1112518902_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1112518902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m647570427_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m912926810_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2531960211_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2531960211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, double, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (double)((*(double*)((double*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3460574231_gshared (TweenerCore_3_t456598886 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t456598886 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3659029185 *)L_0, (TweenerCore_3_t456598886 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3599961708_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3659029185 * L_1 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t456598886 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3659029185 *)L_1, (TweenerCore_3_t456598886 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1565154527 *)NULL);
		__this->set_setter_58((DOSetter_1_t1441277371 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2094047477_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2094047477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1565154527 * L_0 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1565154527 *)L_0);
		((  double (*) (DOGetter_1_t1565154527 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1565154527 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3059104922_gshared (TweenerCore_3_t456598886 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m514828332_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3426149681_gshared (TweenerCore_3_t456598886 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3659029185 * L_5 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_8 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_9 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_10 = V_0;
		double L_11 = (double)__this->get_startValue_53();
		double L_12 = (double)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1565154527 *)L_8, (DOSetter_1_t1441277371 *)L_9, (float)L_10, (double)L_11, (double)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3659029185 * L_16 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_19 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_20 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_21 = V_0;
		double L_22 = (double)__this->get_startValue_53();
		double L_23 = (double)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1565154527 *)L_19, (DOSetter_1_t1441277371 *)L_20, (float)L_21, (double)L_22, (double)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2533780907_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2533780907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3831419980_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3831419980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3691364999_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3330381752_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2571270975_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2571270975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (int32_t)((*(int32_t*)((int32_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m230978731_gshared (TweenerCore_3_t3160108754 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3160108754 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2067571757 *)L_0, (TweenerCore_3_t3160108754 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m387790262_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2067571757 * L_1 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3160108754 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2067571757 *)L_1, (TweenerCore_3_t3160108754 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3853983590 *)NULL);
		__this->set_setter_58((DOSetter_1_t3730106434 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3448887169_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3448887169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3853983590 * L_0 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3853983590 *)L_0);
		((  int32_t (*) (DOGetter_1_t3853983590 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3853983590 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3455091692_gshared (TweenerCore_3_t3160108754 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3282992694_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2587998509_gshared (TweenerCore_3_t3160108754 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2067571757 * L_5 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_8 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_9 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_startValue_53();
		int32_t L_12 = (int32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3853983590 *)L_8, (DOSetter_1_t3730106434 *)L_9, (float)L_10, (int32_t)L_11, (int32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2067571757 * L_16 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_19 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_20 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_21 = V_0;
		int32_t L_22 = (int32_t)__this->get_startValue_53();
		int32_t L_23 = (int32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3853983590 *)L_19, (DOSetter_1_t3730106434 *)L_20, (float)L_21, (int32_t)L_22, (int32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1918761711_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1918761711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m2139817550_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2139817550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3842480115_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2582633306_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m1168812923_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1168812923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, int64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (int64_t)((*(int64_t*)((int64_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m4270264951_gshared (TweenerCore_3_t3199111798 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3199111798 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2106574801 *)L_0, (TweenerCore_3_t3199111798 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1606553284_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2106574801 * L_1 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3199111798 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2106574801 *)L_1, (TweenerCore_3_t3199111798 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t2691184179 *)NULL);
		__this->set_setter_58((DOSetter_1_t2567307023 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m1266531737_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1266531737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2691184179 * L_0 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2691184179 *)L_0);
		((  int64_t (*) (DOGetter_1_t2691184179 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t2691184179 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m690694406_gshared (TweenerCore_3_t3199111798 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1097327556_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m1754088405_gshared (TweenerCore_3_t3199111798 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2106574801 * L_5 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_8 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_9 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_10 = V_0;
		int64_t L_11 = (int64_t)__this->get_startValue_53();
		int64_t L_12 = (int64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2691184179 *)L_8, (DOSetter_1_t2567307023 *)L_9, (float)L_10, (int64_t)L_11, (int64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2106574801 * L_16 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_19 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_20 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_21 = V_0;
		int64_t L_22 = (int64_t)__this->get_startValue_53();
		int64_t L_23 = (int64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2691184179 *)L_19, (DOSetter_1_t2567307023 *)L_20, (float)L_21, (int64_t)L_22, (int64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1198377687_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1198377687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3792476854_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3792476854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1306328571_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m188691034_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m4189518099_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m4189518099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2965628695_gshared (TweenerCore_3_t1705766462 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1705766462 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t613229465 *)L_0, (TweenerCore_3_t1705766462 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2741938284_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t613229465 * L_1 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1705766462 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t613229465 *)L_1, (TweenerCore_3_t1705766462 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m1363945525_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1363945525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t176588141 * L_0 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t176588141 *)L_0);
		((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t176588141 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2609812250_gshared (TweenerCore_3_t1705766462 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3258896428_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m749792945_gshared (TweenerCore_3_t1705766462 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m749792945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t613229465 * L_5 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t613229465 * L_16 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3971101479_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3971101479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m536230576_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m536230576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2107526987_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2269915508_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m3838998779_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3838998779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m528864199_gshared (TweenerCore_3_t2082658550 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2082658550 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t990121553 *)L_0, (TweenerCore_3_t2082658550 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m492887674_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t990121553 * L_1 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2082658550 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t990121553 *)L_1, (TweenerCore_3_t2082658550 *)__this);
	}

IL_001a:
	{
		StringOptions_t2885323933 * L_2 = (StringOptions_t2885323933 *)__this->get_address_of_plugOptions_56();
		StringOptions_Reset_m530675817((StringOptions_t2885323933 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2015486485_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2015486485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t176588141 * L_0 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t176588141 *)L_0);
		((  Il2CppObject * (*) (DOGetter_1_t176588141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t176588141 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3197074312_gshared (TweenerCore_3_t2082658550 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2775364066_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m4039624089_gshared (TweenerCore_3_t2082658550 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t990121553 * L_5 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_6 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_5);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_5, (StringOptions_t2885323933 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t990121553 * L_16 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_17 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_16);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_16, (StringOptions_t2885323933 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1849842838_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1849842838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3084865231_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3084865231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m407697134_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2067864171_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m114545046_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m114545046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)((*(float*)((float*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3054976738_gshared (TweenerCore_3_t2279406887 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2279406887 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1186869890 *)L_0, (TweenerCore_3_t2279406887 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3808154951_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1186869890 * L_1 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2279406887 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1186869890 *)L_1, (TweenerCore_3_t2279406887 *)__this);
	}

IL_001a:
	{
		FloatOptions_t1421548266 * L_2 = (FloatOptions_t1421548266 *)__this->get_address_of_plugOptions_56();
		FloatOptions_Reset_m1378398748((FloatOptions_t1421548266 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3858616074 *)NULL);
		__this->set_setter_58((DOSetter_1_t3734738918 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2802707620_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2802707620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3858616074 * L_0 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3858616074 *)L_0);
		((  float (*) (DOGetter_1_t3858616074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3858616074 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2385689477_gshared (TweenerCore_3_t2279406887 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3559394027_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3135202618_gshared (TweenerCore_3_t2279406887 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1186869890 * L_5 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_6 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_8 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_9 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_10 = V_0;
		float L_11 = (float)__this->get_startValue_53();
		float L_12 = (float)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_5);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_5, (FloatOptions_t1421548266 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3858616074 *)L_8, (DOSetter_1_t3734738918 *)L_9, (float)L_10, (float)L_11, (float)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1186869890 * L_16 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_17 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_19 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_20 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_21 = V_0;
		float L_22 = (float)__this->get_startValue_53();
		float L_23 = (float)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_16);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_16, (FloatOptions_t1421548266 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3858616074 *)L_19, (DOSetter_1_t3734738918 *)L_20, (float)L_21, (float)L_22, (float)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m4074697550_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4074697550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m806842373_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m806842373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2418838464_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m949598801_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m4204159460_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m4204159460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, uint32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (uint32_t)((*(uint32_t*)((uint32_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1000169988_gshared (TweenerCore_3_t1648829745 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1648829745 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t556292748 *)L_0, (TweenerCore_3_t1648829745 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2146775889_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t556292748 * L_1 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1648829745 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t556292748 *)L_1, (TweenerCore_3_t1648829745 *)__this);
	}

IL_001a:
	{
		UintOptions_t2267095136 * L_2 = (UintOptions_t2267095136 *)__this->get_address_of_plugOptions_56();
		UintOptions_Reset_m3831863362((UintOptions_t2267095136 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3931788163 *)NULL);
		__this->set_setter_58((DOSetter_1_t3807911007 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m621478568_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m621478568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3931788163 * L_0 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3931788163 *)L_0);
		((  uint32_t (*) (DOGetter_1_t3931788163 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3931788163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3527224199_gshared (TweenerCore_3_t1648829745 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3369460217_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3033691518_gshared (TweenerCore_3_t1648829745 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t556292748 * L_5 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_6 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_8 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_9 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_10 = V_0;
		uint32_t L_11 = (uint32_t)__this->get_startValue_53();
		uint32_t L_12 = (uint32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_5);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_5, (UintOptions_t2267095136 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3931788163 *)L_8, (DOSetter_1_t3807911007 *)L_9, (float)L_10, (uint32_t)L_11, (uint32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t556292748 * L_16 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_17 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_19 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_20 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_21 = V_0;
		uint32_t L_22 = (uint32_t)__this->get_startValue_53();
		uint32_t L_23 = (uint32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_16);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_16, (UintOptions_t2267095136 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3931788163 *)L_19, (DOSetter_1_t3807911007 *)L_20, (float)L_21, (uint32_t)L_22, (uint32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m418004407_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m418004407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1875838422_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1875838422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2663196443_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1802430330_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m570000243_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m570000243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, uint64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (uint64_t)((*(uint64_t*)((uint64_t*)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m175513655_gshared (TweenerCore_3_t2937657178 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2937657178 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1845120181 *)L_0, (TweenerCore_3_t2937657178 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2800505548_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1845120181 * L_1 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2937657178 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1845120181 *)L_1, (TweenerCore_3_t2937657178 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t396335760 *)NULL);
		__this->set_setter_58((DOSetter_1_t272458604 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m1883068437_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1883068437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t396335760 * L_0 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t396335760 *)L_0);
		((  uint64_t (*) (DOGetter_1_t396335760 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t396335760 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1164357562_gshared (TweenerCore_3_t2937657178 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m501231052_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2278209809_gshared (TweenerCore_3_t2937657178 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1845120181 * L_5 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_8 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_9 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_10 = V_0;
		uint64_t L_11 = (uint64_t)__this->get_startValue_53();
		uint64_t L_12 = (uint64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t396335760 *)L_8, (DOSetter_1_t272458604 *)L_9, (float)L_10, (uint64_t)L_11, (uint64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1845120181 * L_16 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_19 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_20 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_21 = V_0;
		uint64_t L_22 = (uint64_t)__this->get_startValue_53();
		uint64_t L_23 = (uint64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t396335760 *)L_19, (DOSetter_1_t272458604 *)L_20, (float)L_21, (uint64_t)L_22, (uint64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2978185163_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2978185163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3818406250_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3818406250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m437196795_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3105054398_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m592431499_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m592431499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , Color_t2020392075 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1414404375_gshared (TweenerCore_3_t2998039394 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2998039394 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1905502397 *)L_0, (TweenerCore_3_t2998039394 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3158058770_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1905502397 * L_1 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2998039394 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1905502397 *)L_1, (TweenerCore_3_t2998039394 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305 * L_2 = (ColorOptions_t2213017305 *)__this->get_address_of_plugOptions_56();
		ColorOptions_Reset_m2624799597((ColorOptions_t2213017305 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t3802498217 *)NULL);
		__this->set_setter_58((DOSetter_1_t3678621061 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m44760597_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m44760597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3802498217 * L_0 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3802498217 *)L_0);
		((  Color_t2020392075  (*) (DOGetter_1_t3802498217 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t3802498217 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1284389728_gshared (TweenerCore_3_t2998039394 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1966872506_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2248740889_gshared (TweenerCore_3_t2998039394 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1905502397 * L_5 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_8 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_9 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_10 = V_0;
		Color_t2020392075  L_11 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_12 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3802498217 *)L_8, (DOSetter_1_t3678621061 *)L_9, (float)L_10, (Color_t2020392075 )L_11, (Color_t2020392075 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1905502397 * L_16 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_19 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_20 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_21 = V_0;
		Color_t2020392075  L_22 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_23 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3802498217 *)L_19, (DOSetter_1_t3678621061 *)L_20, (float)L_21, (Color_t2020392075 )L_22, (Color_t2020392075 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3146716579_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3146716579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m65087268_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m65087268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, Quaternion_t4030073918 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3873261087_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2618008744_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2618008744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, Quaternion_t4030073918 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2005239399_gshared (TweenerCore_3_t102288586 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2005239399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, Quaternion_t4030073918 , Quaternion_t4030073918 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m856670555_gshared (TweenerCore_3_t102288586 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3304718885 * L_0 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t102288586 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3304718885 *)L_0, (TweenerCore_3_t102288586 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1862249806_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3304718885 * L_0 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3304718885 * L_1 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t102288586 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3304718885 *)L_1, (TweenerCore_3_t102288586 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845 * L_2 = (NoOptions_t2508431845 *)__this->get_address_of_plugOptions_56();
		NoOptions_Reset_m532649001((NoOptions_t2508431845 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1517212764 *)NULL);
		__this->set_setter_58((DOSetter_1_t1393335608 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3999115877_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3999115877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1517212764 * L_0 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1517212764 *)L_0);
		((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1517212764 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m116258440_gshared (TweenerCore_3_t102288586 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1801576590_gshared (TweenerCore_3_t102288586 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t102288586 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t102288586 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2916579489_gshared (TweenerCore_3_t102288586 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2916579489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3304718885 * L_5 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_8 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_9 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_10 = V_0;
		Quaternion_t4030073918  L_11 = (Quaternion_t4030073918 )__this->get_startValue_53();
		Quaternion_t4030073918  L_12 = (Quaternion_t4030073918 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Quaternion_t4030073918 , Quaternion_t4030073918 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3304718885 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1517212764 *)L_8, (DOSetter_1_t1393335608 *)L_9, (float)L_10, (Quaternion_t4030073918 )L_11, (Quaternion_t4030073918 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3304718885 * L_16 = (ABSTweenPlugin_3_t3304718885 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_19 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_20 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_21 = V_0;
		Quaternion_t4030073918  L_22 = (Quaternion_t4030073918 )__this->get_startValue_53();
		Quaternion_t4030073918  L_23 = (Quaternion_t4030073918 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3304718885 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Quaternion_t4030073918 , Quaternion_t4030073918 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3304718885 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1517212764 *)L_19, (DOSetter_1_t1393335608 *)L_20, (float)L_21, (Quaternion_t4030073918 )L_22, (Quaternion_t4030073918 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m2776308754_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2776308754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m839318221_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m839318221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m398039294_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3657890433_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2925494578_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2925494578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2275858234_gshared (TweenerCore_3_t1672798003 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1672798003 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t580261006 *)L_0, (TweenerCore_3_t1672798003 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1088438225_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t580261006 * L_1 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1672798003 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t580261006 *)L_1, (TweenerCore_3_t1672798003 *)__this);
	}

IL_001a:
	{
		QuaternionOptions_t466049668 * L_2 = (QuaternionOptions_t466049668 *)__this->get_address_of_plugOptions_56();
		QuaternionOptions_Reset_m4116534850((QuaternionOptions_t466049668 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1517212764 *)NULL);
		__this->set_setter_58((DOSetter_1_t1393335608 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3629558540_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3629558540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1517212764 * L_0 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1517212764 *)L_0);
		((  Quaternion_t4030073918  (*) (DOGetter_1_t1517212764 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1517212764 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1578326919_gshared (TweenerCore_3_t1672798003 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3129492921_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m2434462778_gshared (TweenerCore_3_t1672798003 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t580261006 * L_5 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_6 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_8 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_9 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_5);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_5, (QuaternionOptions_t466049668 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1517212764 *)L_8, (DOSetter_1_t1393335608 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t580261006 * L_16 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_17 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_19 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_20 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_16);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_16, (QuaternionOptions_t466049668 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1517212764 *)L_19, (DOSetter_1_t1393335608 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m231619286_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m231619286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1025968471_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1025968471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3831761276_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2524932243_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m1594244980_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1594244980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , Rect_t3681755626 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1170036736_gshared (TweenerCore_3_t3065187631 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3065187631 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1972650634 *)L_0, (TweenerCore_3_t3065187631 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m48799595_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1972650634 * L_1 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3065187631 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1972650634 *)L_1, (TweenerCore_3_t3065187631 *)__this);
	}

IL_001a:
	{
		RectOptions_t3393635162 * L_2 = (RectOptions_t3393635162 *)__this->get_address_of_plugOptions_56();
		RectOptions_Reset_m297368492((RectOptions_t3393635162 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t1168894472 *)NULL);
		__this->set_setter_58((DOSetter_1_t1045017316 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3855338832_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3855338832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1168894472 * L_0 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1168894472 *)L_0);
		((  Rect_t3681755626  (*) (DOGetter_1_t1168894472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t1168894472 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3599180177_gshared (TweenerCore_3_t3065187631 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2071201127_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m4231943806_gshared (TweenerCore_3_t3065187631 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1972650634 * L_5 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_6 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_8 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_9 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_10 = V_0;
		Rect_t3681755626  L_11 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_12 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_5);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_5, (RectOptions_t3393635162 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1168894472 *)L_8, (DOSetter_1_t1045017316 *)L_9, (float)L_10, (Rect_t3681755626 )L_11, (Rect_t3681755626 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1972650634 * L_16 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_17 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_19 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_20 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_21 = V_0;
		Rect_t3681755626  L_22 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_23 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_16);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_16, (RectOptions_t3393635162 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1168894472 *)L_19, (DOSetter_1_t1045017316 *)L_20, (float)L_21, (Rect_t3681755626 )L_22, (Rect_t3681755626 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m3640631583_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3640631583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m508084142_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m508084142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4014959587_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m863616202_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2621469739_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2621469739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , Vector2_t2243707579 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2801490071_gshared (TweenerCore_3_t3250868854 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3250868854 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2158331857 *)L_0, (TweenerCore_3_t3250868854 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3315574948_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2158331857 * L_1 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3250868854 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2158331857 *)L_1, (TweenerCore_3_t3250868854 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261 * L_2 = (VectorOptions_t293385261 *)__this->get_address_of_plugOptions_56();
		VectorOptions_Reset_m245768289((VectorOptions_t293385261 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813721 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936565 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2299353629_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2299353629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813721 * L_0 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813721 *)L_0);
		((  Vector2_t2243707579  (*) (DOGetter_1_t4025813721 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813721 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1266215114_gshared (TweenerCore_3_t3250868854 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2050368132_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3268195009_gshared (TweenerCore_3_t3250868854 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2158331857 * L_5 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_8 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector2_t2243707579  L_11 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_12 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813721 *)L_8, (DOSetter_1_t3901936565 *)L_9, (float)L_10, (Vector2_t2243707579 )L_11, (Vector2_t2243707579 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2158331857 * L_16 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_19 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_20 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector2_t2243707579  L_22 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_23 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813721 *)L_19, (DOSetter_1_t3901936565 *)L_20, (float)L_21, (Vector2_t2243707579 )L_22, (Vector2_t2243707579 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1012438200_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1012438200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m2708020849_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2708020849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2479530614_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2654203693_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m3787576666_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3787576666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2363190842_gshared (TweenerCore_3_t1622518059 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1622518059 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t529981062 *)L_0, (TweenerCore_3_t1622518059 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2389620197_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t529981062 * L_1 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1622518059 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t529981062 *)L_1, (TweenerCore_3_t1622518059 *)__this);
	}

IL_001a:
	{
		PathOptions_t2659884781 * L_2 = (PathOptions_t2659884781 *)__this->get_address_of_plugOptions_56();
		PathOptions_Reset_m4016313393((PathOptions_t2659884781 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3391320434_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3391320434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813722 * L_0 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813722 *)L_0);
		((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813722 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2529672523_gshared (TweenerCore_3_t1622518059 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2279545925_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m1795781652_gshared (TweenerCore_3_t1622518059 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t529981062 * L_5 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_6 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_5);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_5, (PathOptions_t2659884781 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t529981062 * L_16 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_17 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_16);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_16, (PathOptions_t2659884781 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1432615606_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1432615606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m3270859719_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3270859719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4171376220_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2062234395_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m2725288820_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2725288820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2915691848_gshared (TweenerCore_3_t1635203449 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1635203449 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t542666452 *)L_0, (TweenerCore_3_t1635203449 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3865103419_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t542666452 * L_1 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1635203449 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t542666452 *)L_1, (TweenerCore_3_t1635203449 *)__this);
	}

IL_001a:
	{
		Vector3ArrayOptions_t2672570171 * L_2 = (Vector3ArrayOptions_t2672570171 *)__this->get_address_of_plugOptions_56();
		Vector3ArrayOptions_Reset_m277565119((Vector3ArrayOptions_t2672570171 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m841334684_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m841334684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813722 * L_0 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813722 *)L_0);
		((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813722 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1806502181_gshared (TweenerCore_3_t1635203449 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2282744407_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3384733786_gshared (TweenerCore_3_t1635203449 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t542666452 * L_5 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_6 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_5);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_5, (Vector3ArrayOptions_t2672570171 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t542666452 * L_16 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_17 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_16);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_16, (Vector3ArrayOptions_t2672570171 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m4171196319_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4171196319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m217970222_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m217970222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2834813539_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3688747850_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m624671403_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m624671403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , Vector3_t2243707580 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1933156119_gshared (TweenerCore_3_t1108663466 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1108663466 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t16126469 *)L_0, (TweenerCore_3_t1108663466 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3846139684_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t16126469 * L_1 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1108663466 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t16126469 *)L_1, (TweenerCore_3_t1108663466 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261 * L_2 = (VectorOptions_t293385261 *)__this->get_address_of_plugOptions_56();
		VectorOptions_Reset_m245768289((VectorOptions_t293385261 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m2965636509_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2965636509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813722 * L_0 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813722 *)L_0);
		((  Vector3_t2243707580  (*) (DOGetter_1_t4025813722 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813722 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m283403338_gshared (TweenerCore_3_t1108663466 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2716651012_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m3434606145_gshared (TweenerCore_3_t1108663466 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t16126469 * L_5 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t16126469 * L_16 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void TweenerCore_3__ctor_m1407812639_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1407812639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeStartValue_m1286718126_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1286718126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral552937255);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral552937255);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t760404022 * L_17 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2737049315_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3143172042_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeValues_m868439851_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m868439851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m191970594((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral1422288232);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = (ObjectU5BU5D_t3614634134*)L_10;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3736592114);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)L_13;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral372029317);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1422288232);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1422288232);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3736592114);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_24;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral372029317);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t760404022 * L_31 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , Vector4_t2243707581 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox(L_29, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1394097047_gshared (TweenerCore_3_t3261425374 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3261425374 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2168888377 *)L_0, (TweenerCore_3_t3261425374 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1082756004_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2168888377 * L_1 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3261425374 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2168888377 *)L_1, (TweenerCore_3_t3261425374 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261 * L_2 = (VectorOptions_t293385261 *)__this->get_address_of_plugOptions_56();
		VectorOptions_Reset_m245768289((VectorOptions_t293385261 *)L_2, /*hidden argument*/NULL);
		__this->set_getter_57((DOGetter_1_t4025813723 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936567 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C"  bool TweenerCore_3_Validate_m3552936477_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3552936477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t4025813723 * L_0 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t4025813723 *)L_0);
		((  Vector4_t2243707581  (*) (DOGetter_1_t4025813723 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((DOGetter_1_t4025813723 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m465544650_gshared (TweenerCore_3_t3261425374 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3303950980_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C"  bool TweenerCore_3_ApplyTween_m140499649_gshared (TweenerCore_3_t3261425374 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m140499649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2168888377 * L_5 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_8 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_9 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector4_t2243707581  L_11 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_12 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813723 *)L_8, (DOSetter_1_t3901936567 *)L_9, (float)L_10, (Vector4_t2243707581 )L_11, (Vector4_t2243707581 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2168888377 * L_16 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_19 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_20 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector4_t2243707581  L_22 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_23 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813723 *)L_19, (DOSetter_1_t3901936567 *)L_20, (float)L_21, (Vector4_t2243707581 )L_22, (Vector4_t2243707581 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3622767690_gshared (ABSTweenPlugin_3_t2833266637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1578194524_gshared (ABSTweenPlugin_3_t3659029185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m36425734_gshared (ABSTweenPlugin_3_t2067571757 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m223063124_gshared (ABSTweenPlugin_3_t2106574801 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2153224412_gshared (ABSTweenPlugin_3_t613229465 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m912798746_gshared (ABSTweenPlugin_3_t990121553 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4275081527_gshared (ABSTweenPlugin_3_t1186869890 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4159300161_gshared (ABSTweenPlugin_3_t556292748 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1254825468_gshared (ABSTweenPlugin_3_t1845120181 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1367930952_gshared (ABSTweenPlugin_3_t1905502397 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3467964222_gshared (ABSTweenPlugin_3_t3304718885 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m862077949_gshared (ABSTweenPlugin_3_t580261006 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1871528759_gshared (ABSTweenPlugin_3_t1972650634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1914255460_gshared (ABSTweenPlugin_3_t2158331857 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2241027021_gshared (ABSTweenPlugin_3_t529981062 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3497636823_gshared (ABSTweenPlugin_3_t542666452 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3289379812_gshared (ABSTweenPlugin_3_t16126469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3385119076_gshared (ABSTweenPlugin_3_t2168888377 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3015172213_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m3615052821((TweenCallback_1_t3418705418 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m711035500_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1922991223_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3888348002_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m872757678((TweenCallback_1_t4036277265 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m138693653_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m3980063300_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m4000929921_gshared (TweenCallback_1_t3423337902 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3090779465_gshared (TweenCallback_1_t3423337902 * __this, float ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m3090779465((TweenCallback_1_t3423337902 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m3843094324_gshared (TweenCallback_1_t3423337902 * __this, float ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m3843094324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m801412299_gshared (TweenCallback_1_t3423337902 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Collections.Generic.List`1<T> ListPool`1<System.Object>::Get()
extern "C"  List_1_t2058570427 * ListPool_1_Get_m3704175045_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3678846538 * L_0 = ((ListPool_1_t2027455341_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3678846538 *)L_0);
		List_1_t2058570427 * L_1 = ((  List_1_t2058570427 * (*) (ObjectPool_1_t3678846538 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3678846538 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3744934293_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3678846538 * L_0 = ((ListPool_1_t2027455341_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t2058570427 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3678846538 *)L_0);
		((  void (*) (ObjectPool_1_t3678846538 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3678846538 *)L_0, (List_1_t2058570427 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m690153489_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t3425156178 * L_1 = (UnityAction_1_t3425156178 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t3425156178 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3678846538 * L_2 = (ObjectPool_1_t3678846538 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3678846538 *, UnityAction_1_t3425156178 *, UnityAction_1_t3425156178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t3425156178 *)NULL, (UnityAction_1_t3425156178 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t2027455341_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void ListPool`1<System.Object>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m1632613917_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t573379950 * ListPool_1_Get_m2942590482_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2193656061 * L_0 = ((ListPool_1_t542264864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t2193656061 *)L_0);
		List_1_t573379950 * L_1 = ((  List_1_t573379950 * (*) (ObjectPool_1_t2193656061 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t2193656061 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m4069980162_gshared (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t2193656061 * L_0 = ((ListPool_1_t542264864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t573379950 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t2193656061 *)L_0);
		((  void (*) (ObjectPool_1_t2193656061 *, List_1_t573379950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t2193656061 *)L_0, (List_1_t573379950 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m2796251254_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1939965701 * L_1 = (UnityAction_1_t1939965701 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1939965701 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t2193656061 * L_2 = (ObjectPool_1_t2193656061 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t2193656061 *, UnityAction_1_t1939965701 *, UnityAction_1_t1939965701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1939965701 *)NULL, (UnityAction_1_t1939965701 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t542264864_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m4147748170_gshared (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___l0;
		NullCheck((List_1_t573379950 *)L_0);
		((  void (*) (List_1_t573379950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t573379950 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C"  void ObjectPool_1__ctor_m2091227681_gshared (ObjectPool_1_t14758110 * __this, UnityAction_1_t4056035046 * ___actionOnGet0, UnityAction_1_t4056035046 * ___actionOnRelease1, const MethodInfo* method)
{
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Stack_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		UnityAction_1_t4056035046 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		return;
	}
}
// System.Int32 ObjectPool`1<System.Object>::get_countAll()
extern "C"  int32_t ObjectPool_1_get_countAll_m3367016110_gshared (ObjectPool_1_t14758110 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C"  void ObjectPool_1_set_countAll_m2589455551_gshared (ObjectPool_1_t14758110 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 ObjectPool`1<System.Object>::get_countActive()
extern "C"  int32_t ObjectPool_1_get_countActive_m3458232801_gshared (ObjectPool_1_t14758110 * __this, const MethodInfo* method)
{
	{
		NullCheck((ObjectPool_1_t14758110 *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t14758110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t14758110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t14758110 *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t14758110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((ObjectPool_1_t14758110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return ((int32_t)((int32_t)L_0-(int32_t)L_1));
	}
}
// System.Int32 ObjectPool`1<System.Object>::get_countInactive()
extern "C"  int32_t ObjectPool_1_get_countInactive_m1864035260_gshared (ObjectPool_1_t14758110 * __this, const MethodInfo* method)
{
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3777177449 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3777177449 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_1;
	}
}
// T ObjectPool`1<System.Object>::Get()
extern "C"  Il2CppObject * ObjectPool_1_Get_m4213286530_gshared (ObjectPool_1_t14758110 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3777177449 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3777177449 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (Il2CppObject *)L_2;
		NullCheck((ObjectPool_1_t14758110 *)__this);
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t14758110 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ObjectPool_1_t14758110 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		NullCheck((ObjectPool_1_t14758110 *)__this);
		((  void (*) (ObjectPool_1_t14758110 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((ObjectPool_1_t14758110 *)__this, (int32_t)((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		goto IL_0035;
	}

IL_0029:
	{
		Stack_1_t3777177449 * L_4 = (Stack_1_t3777177449 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3777177449 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Stack_1_t3777177449 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (Il2CppObject *)L_5;
	}

IL_0035:
	{
		UnityAction_1_t4056035046 * L_6 = (UnityAction_1_t4056035046 *)__this->get_m_ActionOnGet_1();
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		UnityAction_1_t4056035046 * L_7 = (UnityAction_1_t4056035046 *)__this->get_m_ActionOnGet_1();
		Il2CppObject * L_8 = V_0;
		NullCheck((UnityAction_1_t4056035046 *)L_7);
		((  void (*) (UnityAction_1_t4056035046 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_1_t4056035046 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_004c:
	{
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// System.Void ObjectPool`1<System.Object>::Release(T)
extern "C"  void ObjectPool_1_Release_m727363400_gshared (ObjectPool_1_t14758110 * __this, Il2CppObject * ___element0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m727363400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t3777177449 * L_0 = (Stack_1_t3777177449 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3777177449 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Stack_1_t3777177449 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		Stack_1_t3777177449 * L_2 = (Stack_1_t3777177449 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t3777177449 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Stack_1_t3777177449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Stack_1_t3777177449 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		Il2CppObject * L_4 = ___element0;
		bool L_5 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral273729679, /*hidden argument*/NULL);
	}

IL_003b:
	{
		UnityAction_1_t4056035046 * L_6 = (UnityAction_1_t4056035046 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		UnityAction_1_t4056035046 * L_7 = (UnityAction_1_t4056035046 *)__this->get_m_ActionOnRelease_2();
		Il2CppObject * L_8 = ___element0;
		NullCheck((UnityAction_1_t4056035046 *)L_7);
		((  void (*) (UnityAction_1_t4056035046 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_1_t4056035046 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0052:
	{
		Stack_1_t3777177449 * L_9 = (Stack_1_t3777177449 *)__this->get_m_Stack_0();
		Il2CppObject * L_10 = ___element0;
		NullCheck((Stack_1_t3777177449 *)L_9);
		((  void (*) (Stack_1_t3777177449 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Stack_1_t3777177449 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return;
	}
}
// System.Void SA_NonMonoSingleton`1<System.Object>::.ctor()
extern "C"  void SA_NonMonoSingleton_1__ctor_m1232978372_gshared (SA_NonMonoSingleton_1_t2015726709 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T SA_NonMonoSingleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SA_NonMonoSingleton_1_get_Instance_m2787801223_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_0 = ((SA_NonMonoSingleton_1_t2015726709_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__Instance_0();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((SA_NonMonoSingleton_1_t2015726709_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set__Instance_0(L_1);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_2 = ((SA_NonMonoSingleton_1_t2015726709_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get__Instance_0();
		return L_2;
	}
}
// System.Void SA_NonMonoSingleton`1<System.Object>::.cctor()
extern "C"  void SA_NonMonoSingleton_1__cctor_m2852181783_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SA_Singleton`1<System.Object>::.ctor()
extern "C"  void SA_Singleton_1__ctor_m2045516282_gshared (SA_Singleton_1_t556325241 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t1158329972 *)__this);
		MonoBehaviour__ctor_m2464341955((MonoBehaviour_t1158329972 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T SA_Singleton`1<System.Object>::get_instance()
extern "C"  Il2CppObject * SA_Singleton_1_get_instance_m3583389439_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// T SA_Singleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SA_Singleton_1_get_Instance_m1640053663_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SA_Singleton_1_get_Instance_m1640053663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_applicationIsQuitting_3();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_1 = ((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_4 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		Il2CppObject * L_5 = ((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0092;
		}
	}
	{
		GameObject_t1756533147 * L_7 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m498247354(L_7, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_7);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)((GameObject_t1756533147 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set__instance_2(L_8);
		NullCheck((Component_t3819376471 *)(*(((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())));
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835((Component_t3819376471 *)(*(((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)(*(((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())));
		Type_t * L_10 = Object_GetType_m191970594((Il2CppObject *)(*(((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_address_of__instance_2())), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_10);
		NullCheck((Object_t1021602117 *)L_9);
		Object_set_name_m4157836998((Object_t1021602117 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_12 = ((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		return L_12;
	}
}
// System.Boolean SA_Singleton`1<System.Object>::get_HasInstance()
extern "C"  bool SA_Singleton_1_get_HasInstance_m2663607460_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean SA_Singleton`1<System.Object>::get_IsDestroyed()
extern "C"  bool SA_Singleton_1_get_IsDestroyed_m2800413832_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SA_Singleton_1_get_IsDestroyed_m2800413832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_0 = ((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get__instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void SA_Singleton`1<System.Object>::OnDestroy()
extern "C"  void SA_Singleton_1_OnDestroy_m1536238735_gshared (SA_Singleton_1_t556325241 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set_applicationIsQuitting_3((bool)1);
		return;
	}
}
// System.Void SA_Singleton`1<System.Object>::OnApplicationQuit()
extern "C"  void SA_Singleton_1_OnApplicationQuit_m452895004_gshared (SA_Singleton_1_t556325241 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set__instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		((SA_Singleton_1_t556325241_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)->static_fields)->set_applicationIsQuitting_3((bool)1);
		return;
	}
}
// System.Void SA_Singleton`1<System.Object>::.cctor()
extern "C"  void SA_Singleton_1__cctor_m2387091787_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C"  void ThreadSafeDictionary_2__ctor_m2422343709_gshared (ThreadSafeDictionary_2_t1463927430 * __this, ThreadSafeDictionaryValueFactory_2_t958855109 * ___valueFactory0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2__ctor_m2422343709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ThreadSafeDictionaryValueFactory_2_t958855109 * L_1 = ___valueFactory0;
		__this->set__valueFactory_1(L_1);
		return;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_Get_m1259355240_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_2;
		goto IL_0040;
	}

IL_0019:
	{
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		bool L_5 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (Il2CppObject *)L_4, (Il2CppObject **)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		if (L_5)
		{
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_6 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_7;
		goto IL_0040;
	}

IL_0039:
	{
		Il2CppObject * L_8 = V_1;
		V_0 = (Il2CppObject *)L_8;
		goto IL_0040;
	}

IL_0040:
	{
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_AddValue_m2068149316_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Dictionary_2_t2281509423 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ThreadSafeDictionaryValueFactory_2_t958855109 * L_0 = (ThreadSafeDictionaryValueFactory_2_t958855109 *)__this->get__valueFactory_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionaryValueFactory_2_t958855109 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t958855109 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ThreadSafeDictionaryValueFactory_2_t958855109 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			if (L_5)
			{
				goto IL_0046;
			}
		}

IL_0027:
		{
			Dictionary_2_t2281509423 * L_6 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			__this->set__dictionary_2(L_6);
			Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_7);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			goto IL_0080;
		}

IL_0046:
		{
			Dictionary_2_t2281509423 * L_10 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((Dictionary_2_t2281509423 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
			if (!L_12)
			{
				goto IL_0061;
			}
		}

IL_005a:
		{
			Il2CppObject * L_13 = V_2;
			V_3 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x94, FINALLY_0086);
		}

IL_0061:
		{
			Dictionary_2_t2281509423 * L_14 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Dictionary_2_t2281509423 * L_15 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			V_4 = (Dictionary_2_t2281509423 *)L_15;
			Dictionary_2_t2281509423 * L_16 = V_4;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_16);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			Dictionary_2_t2281509423 * L_19 = V_4;
			__this->set__dictionary_2(L_19);
		}

IL_0080:
		{
			IL2CPP_LEAVE(0x8D, FINALLY_0086);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0086;
	}

FINALLY_0086:
	{ // begin finally (depth: 1)
		Il2CppObject * L_20 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(134)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(134)
	{
		IL2CPP_JUMP_TBL(0x94, IL_0094)
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008d:
	{
		Il2CppObject * L_21 = V_0;
		V_3 = (Il2CppObject *)L_21;
		goto IL_0094;
	}

IL_0094:
	{
		Il2CppObject * L_22 = V_3;
		return L_22;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ThreadSafeDictionary_2_Add_m2282389750_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m2282389750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Keys_m2226270717_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		KeyCollection_t470039898 * L_1 = ((  KeyCollection_t470039898 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (Il2CppObject*)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Il2CppObject* L_2 = V_0;
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Values_m2761852509_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		ValueCollection_t984569266 * L_1 = ((  ValueCollection_t984569266 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (Il2CppObject*)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Il2CppObject* L_2 = V_0;
		return L_2;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_get_Item_m2261720138_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (Il2CppObject *)L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ThreadSafeDictionary_2_set_Item_m2513745515_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_set_Item_m2513745515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ThreadSafeDictionary_2_Add_m1306324747_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m1306324747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ThreadSafeDictionary_2_Clear_m3155495606_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Clear_m3155495606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ThreadSafeDictionary_2_Contains_m1533726473_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Contains_m1533726473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ThreadSafeDictionary_2_CopyTo_m1781093487_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_CopyTo_m1781093487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ThreadSafeDictionary_2_get_Count_m3265115815_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ThreadSafeDictionary_2_Remove_m1651487354_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m1651487354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_GetEnumerator_m527450770_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Enumerator_t3601534125  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_2);
		V_0 = (Il2CppObject*)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject* L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1443063812_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Enumerator_t3601534125  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_2);
		V_0 = (Il2CppObject *)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ThreadSafeDictionaryValueFactory_2__ctor_m3821979982_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419((ThreadSafeDictionaryValueFactory_2_t958855109 *)__this->get_prev_9(),___key0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_BeginInvoke_m2175992264_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___key0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_EndInvoke_m235081874_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m547340188_gshared (Action_1_t2800324759 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>::Invoke(T)
extern "C"  void Action_1_Invoke_m3334985867_gshared (Action_1_t2800324759 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3334985867((Action_1_t2800324759 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m726771024_gshared (Action_1_t2800324759 * __this, uint8_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m726771024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(FPSLevel_t2998525377_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3385125581_gshared (Action_1_t2800324759 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2625719213_gshared (Action_1_t1815011612 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m3402415237_gshared (Action_1_t1815011612 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3402415237((Action_1_t1815011612 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3701353426_gshared (Action_1_t1815011612 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3701353426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializationStatus_t2013212230_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1938580315_gshared (Action_1_t1815011612 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m309821356_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m345545414_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m3352874125_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3352874125((Action_1_t1873676830 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1720726178_gshared (Action_1_t1873676830 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1720726178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3128406917_gshared (Action_1_t1873676830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m4180501989_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4180501989((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m928763336_gshared (Action_1_t1878309314 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Single>::Invoke(T)
extern "C"  void Action_1_Invoke_m3661933365_gshared (Action_1_t1878309314 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3661933365((Action_1_t1878309314 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1617223338_gshared (Action_1_t1878309314 * __this, float ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1617223338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m249557173_gshared (Action_1_t1878309314 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2249515781_gshared (Action_1_t2755832024 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T)
extern "C"  void Action_1_Invoke_m2983653028_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2983653028((Action_1_t2755832024 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1319991882_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1319991882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1485676899_gshared (Action_1_t2755832024 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3424424418_gshared (Action_1_t2289103034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>::Invoke(T)
extern "C"  void Action_1_Invoke_m188738021_gshared (Action_1_t2289103034 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m188738021((Action_1_t2289103034 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1248823956_gshared (Action_1_t2289103034 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1248823956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RestoreTransactionIDState_t2487303652_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m736865145_gshared (Action_1_t2289103034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3887251552_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void Action_1_Invoke_m1071418069_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1071418069((Action_1_t2045506962 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m402802490_gshared (Action_1_t2045506962 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m402802490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4206793277_gshared (Action_1_t2045506962 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1936909466_gshared (Action_2_t2506932584 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m4107579927_gshared (Action_2_t2506932584 * __this, int32_t ___arg10, bool ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m4107579927((Action_2_t2506932584 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, bool ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1999404200_gshared (Action_2_t2506932584 * __this, int32_t ___arg10, bool ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1999404200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ResponseStatus_t167331027_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3476503760_gshared (Action_2_t2506932584 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3231696065_gshared (Action_2_t1370807161 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m65990118_gshared (Action_2_t1370807161 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m65990118((Action_2_t1370807161 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2670173333_gshared (Action_2_t1370807161 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2670173333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ResponseStatus_t167331027_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.ResponseStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2108019219_gshared (Action_2_t1370807161 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m18421417_gshared (Action_2_t2272063304 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1770798410_gshared (Action_2_t2272063304 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1770798410((Action_2_t2272063304 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3524880053_gshared (Action_2_t2272063304 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3524880053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SavedGameRequestStatus_t2671736816_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2249251167_gshared (Action_2_t2272063304 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m4085539543_gshared (Action_2_t3649729058 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2875204762_gshared (Action_2_t3649729058 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2875204762((Action_2_t3649729058 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m753119779_gshared (Action_2_t3649729058 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m753119779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(SelectUIStatus_t2446036638_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<GooglePlayGames.BasicApi.SavedGame.SelectUIStatus,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3491302245_gshared (Action_2_t3649729058 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1582347788_gshared (Action_2_t1907880187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2365036873_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2365036873((Action_2_t1907880187 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1925898598_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1925898598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m343868110_gshared (Action_2_t1907880187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m352317182_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m352317182((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3463170984_gshared (Action_2_t2790035381 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3143687639_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3143687639((Action_2_t2790035381 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m410485158_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m410485158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m99462074_gshared (Action_2_t2790035381 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1121744629_gshared (Action_2_t1158962578 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1462475090_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1462475090((Action_2_t1158962578 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m772046713_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m772046713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m430603547_gshared (Action_2_t1158962578 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m382481810_gshared (Action_2_t1370245513 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Int32,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3770950199_gshared (Action_2_t1370245513 * __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3770950199((Action_2_t1370245513 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Int32,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m2329746402_gshared (Action_2_t1370245513 * __this, int32_t ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m2329746402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3308276602_gshared (Action_2_t1370245513 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2406183663_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2406183663((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m1132625820_gshared (Action_3_t206039628 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.DateTime>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m694522710_gshared (Action_3_t206039628 * __this, bool ___arg10, Il2CppObject * ___arg21, DateTime_t693205669  ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m694522710((Action_3_t206039628 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, DateTime_t693205669  ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, DateTime_t693205669  ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Boolean,System.Object,System.DateTime>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1436240799_gshared (Action_3_t206039628 * __this, bool ___arg10, Il2CppObject * ___arg21, DateTime_t693205669  ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1436240799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &___arg32);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3176226574_gshared (Action_3_t206039628 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2414365210_gshared (Action_3_t2202283254 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m138287784_gshared (Action_3_t2202283254 * __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m138287784((Action_3_t2202283254 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Boolean,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m4234685015_gshared (Action_3_t2202283254 * __this, bool ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m4234685015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m695059408_gshared (Action_3_t2202283254 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2182397233_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3309590603_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3309590603((Action_3_t1115657183 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m275697784_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m4265906515_gshared (Action_3_t1115657183 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m130518359_gshared (Action_3_t4226502927 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m2533654301_gshared (Action_3_t4226502927 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m2533654301((Action_3_t4226502927 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m4030220740_gshared (Action_3_t4226502927 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m4030220740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.PurchaseFailureReason,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m559785561_gshared (Action_3_t4226502927 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m1512926489_gshared (Action_3_t863926345 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>::Invoke(T1,T2,T3)
extern "C"  void Action_3_Invoke_m3171976315_gshared (Action_3_t863926345 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_3_Invoke_m3171976315((Action_3_t863926345 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_3_BeginInvoke_m1518280454_gshared (Action_3_t863926345 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_3_BeginInvoke_m1518280454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(ValidateReceiptState_t4359597_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void System.Action`3<System.Object,UnityEngine.Purchasing.ValidateReceiptState,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_3_EndInvoke_m3011069123_gshared (Action_3_t863926345 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
