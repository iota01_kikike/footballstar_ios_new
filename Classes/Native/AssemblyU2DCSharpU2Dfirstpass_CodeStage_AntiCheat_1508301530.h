﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2523981134.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal/DecimalLongBytesUnion
struct  DecimalLongBytesUnion_t1508301530 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Decimal CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal/DecimalLongBytesUnion::d
			Decimal_t724701077  ___d_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Decimal_t724701077  ___d_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal/DecimalLongBytesUnion::l1
			int64_t ___l1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___l1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___l2_2_OffsetPadding[8];
			// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal/DecimalLongBytesUnion::l2
			int64_t ___l2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___l2_2_OffsetPadding_forAlignmentOnly[8];
			int64_t ___l2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// CodeStage.AntiCheat.Common.ACTkByte16 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal/DecimalLongBytesUnion::b16
			ACTkByte16_t2523981134  ___b16_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			ACTkByte16_t2523981134  ___b16_3_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t1508301530, ___d_0)); }
	inline Decimal_t724701077  get_d_0() const { return ___d_0; }
	inline Decimal_t724701077 * get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(Decimal_t724701077  value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_l1_1() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t1508301530, ___l1_1)); }
	inline int64_t get_l1_1() const { return ___l1_1; }
	inline int64_t* get_address_of_l1_1() { return &___l1_1; }
	inline void set_l1_1(int64_t value)
	{
		___l1_1 = value;
	}

	inline static int32_t get_offset_of_l2_2() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t1508301530, ___l2_2)); }
	inline int64_t get_l2_2() const { return ___l2_2; }
	inline int64_t* get_address_of_l2_2() { return &___l2_2; }
	inline void set_l2_2(int64_t value)
	{
		___l2_2 = value;
	}

	inline static int32_t get_offset_of_b16_3() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t1508301530, ___b16_3)); }
	inline ACTkByte16_t2523981134  get_b16_3() const { return ___b16_3; }
	inline ACTkByte16_t2523981134 * get_address_of_b16_3() { return &___b16_3; }
	inline void set_b16_3(ACTkByte16_t2523981134  value)
	{
		___b16_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
