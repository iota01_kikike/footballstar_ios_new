﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuidePop2
struct  GuidePop2_t959359457  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button GuidePop2::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// UnityEngine.GameObject GuidePop2::StarIcon
	GameObject_t1756533147 * ___StarIcon_3;
	// UnityEngine.UI.Image GuidePop2::TouchImage
	Image_t2042527209 * ___TouchImage_4;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(GuidePop2_t959359457, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_StarIcon_3() { return static_cast<int32_t>(offsetof(GuidePop2_t959359457, ___StarIcon_3)); }
	inline GameObject_t1756533147 * get_StarIcon_3() const { return ___StarIcon_3; }
	inline GameObject_t1756533147 ** get_address_of_StarIcon_3() { return &___StarIcon_3; }
	inline void set_StarIcon_3(GameObject_t1756533147 * value)
	{
		___StarIcon_3 = value;
		Il2CppCodeGenWriteBarrier(&___StarIcon_3, value);
	}

	inline static int32_t get_offset_of_TouchImage_4() { return static_cast<int32_t>(offsetof(GuidePop2_t959359457, ___TouchImage_4)); }
	inline Image_t2042527209 * get_TouchImage_4() const { return ___TouchImage_4; }
	inline Image_t2042527209 ** get_address_of_TouchImage_4() { return &___TouchImage_4; }
	inline void set_TouchImage_4(Image_t2042527209 * value)
	{
		___TouchImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___TouchImage_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
