﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.WorksheetTemplate
struct  WorksheetTemplate_t3784200896  : public Il2CppObject
{
public:
	// System.String SA.GoogleDoc.WorksheetTemplate::listName
	String_t* ___listName_0;
	// System.Int32 SA.GoogleDoc.WorksheetTemplate::listId
	int32_t ___listId_1;

public:
	inline static int32_t get_offset_of_listName_0() { return static_cast<int32_t>(offsetof(WorksheetTemplate_t3784200896, ___listName_0)); }
	inline String_t* get_listName_0() const { return ___listName_0; }
	inline String_t** get_address_of_listName_0() { return &___listName_0; }
	inline void set_listName_0(String_t* value)
	{
		___listName_0 = value;
		Il2CppCodeGenWriteBarrier(&___listName_0, value);
	}

	inline static int32_t get_offset_of_listId_1() { return static_cast<int32_t>(offsetof(WorksheetTemplate_t3784200896, ___listId_1)); }
	inline int32_t get_listId_1() const { return ___listId_1; }
	inline int32_t* get_address_of_listId_1() { return &___listId_1; }
	inline void set_listId_1(int32_t value)
	{
		___listId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
