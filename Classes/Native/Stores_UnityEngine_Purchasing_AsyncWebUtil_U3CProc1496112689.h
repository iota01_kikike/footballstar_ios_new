﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// UnityEngine.Purchasing.AsyncWebUtil
struct AsyncWebUtil_t1370427196;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4
struct  U3CProcessU3Ed__4_t1496112689  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// UnityEngine.WWW UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::request
	WWW_t2919945039 * ___request_2;
	// System.Action`1<System.String> UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::responseHandler
	Action_1_t1831019615 * ___responseHandler_3;
	// System.Action`1<System.String> UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::errorHandler
	Action_1_t1831019615 * ___errorHandler_4;
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::maxTimeoutInSeconds
	int32_t ___maxTimeoutInSeconds_5;
	// UnityEngine.Purchasing.AsyncWebUtil UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::<>4__this
	AsyncWebUtil_t1370427196 * ___U3CU3E4__this_6;
	// System.Single UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::<timer>5__1
	float ___U3CtimerU3E5__1_7;
	// System.Boolean UnityEngine.Purchasing.AsyncWebUtil/<Process>d__4::<hasTimedOut>5__2
	bool ___U3ChasTimedOutU3E5__2_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___request_2)); }
	inline WWW_t2919945039 * get_request_2() const { return ___request_2; }
	inline WWW_t2919945039 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(WWW_t2919945039 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}

	inline static int32_t get_offset_of_responseHandler_3() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___responseHandler_3)); }
	inline Action_1_t1831019615 * get_responseHandler_3() const { return ___responseHandler_3; }
	inline Action_1_t1831019615 ** get_address_of_responseHandler_3() { return &___responseHandler_3; }
	inline void set_responseHandler_3(Action_1_t1831019615 * value)
	{
		___responseHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___responseHandler_3, value);
	}

	inline static int32_t get_offset_of_errorHandler_4() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___errorHandler_4)); }
	inline Action_1_t1831019615 * get_errorHandler_4() const { return ___errorHandler_4; }
	inline Action_1_t1831019615 ** get_address_of_errorHandler_4() { return &___errorHandler_4; }
	inline void set_errorHandler_4(Action_1_t1831019615 * value)
	{
		___errorHandler_4 = value;
		Il2CppCodeGenWriteBarrier(&___errorHandler_4, value);
	}

	inline static int32_t get_offset_of_maxTimeoutInSeconds_5() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___maxTimeoutInSeconds_5)); }
	inline int32_t get_maxTimeoutInSeconds_5() const { return ___maxTimeoutInSeconds_5; }
	inline int32_t* get_address_of_maxTimeoutInSeconds_5() { return &___maxTimeoutInSeconds_5; }
	inline void set_maxTimeoutInSeconds_5(int32_t value)
	{
		___maxTimeoutInSeconds_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___U3CU3E4__this_6)); }
	inline AsyncWebUtil_t1370427196 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline AsyncWebUtil_t1370427196 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(AsyncWebUtil_t1370427196 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_6, value);
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___U3CtimerU3E5__1_7)); }
	inline float get_U3CtimerU3E5__1_7() const { return ___U3CtimerU3E5__1_7; }
	inline float* get_address_of_U3CtimerU3E5__1_7() { return &___U3CtimerU3E5__1_7; }
	inline void set_U3CtimerU3E5__1_7(float value)
	{
		___U3CtimerU3E5__1_7 = value;
	}

	inline static int32_t get_offset_of_U3ChasTimedOutU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1496112689, ___U3ChasTimedOutU3E5__2_8)); }
	inline bool get_U3ChasTimedOutU3E5__2_8() const { return ___U3ChasTimedOutU3E5__2_8; }
	inline bool* get_address_of_U3ChasTimedOutU3E5__2_8() { return &___U3ChasTimedOutU3E5__2_8; }
	inline void set_U3ChasTimedOutU3E5__2_8(bool value)
	{
		___U3ChasTimedOutU3E5__2_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
