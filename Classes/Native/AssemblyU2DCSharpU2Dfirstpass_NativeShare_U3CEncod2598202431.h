﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// NativeShare
struct NativeShare_t1150945090;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/<EncodeImageData>c__Iterator0
struct  U3CEncodeImageDataU3Ec__Iterator0_t2598202431  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D[] NativeShare/<EncodeImageData>c__Iterator0::textures
	Texture2DU5BU5D_t2724090252* ___textures_0;
	// System.String[] NativeShare/<EncodeImageData>c__Iterator0::<filesList>__0
	StringU5BU5D_t1642385972* ___U3CfilesListU3E__0_1;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::<baseFloder>__0
	String_t* ___U3CbaseFloderU3E__0_2;
	// System.Int32 NativeShare/<EncodeImageData>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_3;
	// System.Byte[] NativeShare/<EncodeImageData>c__Iterator0::<bytes>__2
	ByteU5BU5D_t3397334013* ___U3CbytesU3E__2_4;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::<pathSave>__2
	String_t* ___U3CpathSaveU3E__2_5;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::<filesJsonString>__0
	String_t* ___U3CfilesJsonStringU3E__0_6;
	// System.String[] NativeShare/<EncodeImageData>c__Iterator0::emails
	StringU5BU5D_t1642385972* ___emails_7;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::<emailJsonString>__0
	String_t* ___U3CemailJsonStringU3E__0_8;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::subject
	String_t* ___subject_9;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::msg
	String_t* ___msg_10;
	// System.String NativeShare/<EncodeImageData>c__Iterator0::shareApp
	String_t* ___shareApp_11;
	// NativeShare NativeShare/<EncodeImageData>c__Iterator0::$this
	NativeShare_t1150945090 * ___U24this_12;
	// System.Object NativeShare/<EncodeImageData>c__Iterator0::$current
	Il2CppObject * ___U24current_13;
	// System.Boolean NativeShare/<EncodeImageData>c__Iterator0::$disposing
	bool ___U24disposing_14;
	// System.Int32 NativeShare/<EncodeImageData>c__Iterator0::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_textures_0() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___textures_0)); }
	inline Texture2DU5BU5D_t2724090252* get_textures_0() const { return ___textures_0; }
	inline Texture2DU5BU5D_t2724090252** get_address_of_textures_0() { return &___textures_0; }
	inline void set_textures_0(Texture2DU5BU5D_t2724090252* value)
	{
		___textures_0 = value;
		Il2CppCodeGenWriteBarrier(&___textures_0, value);
	}

	inline static int32_t get_offset_of_U3CfilesListU3E__0_1() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CfilesListU3E__0_1)); }
	inline StringU5BU5D_t1642385972* get_U3CfilesListU3E__0_1() const { return ___U3CfilesListU3E__0_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CfilesListU3E__0_1() { return &___U3CfilesListU3E__0_1; }
	inline void set_U3CfilesListU3E__0_1(StringU5BU5D_t1642385972* value)
	{
		___U3CfilesListU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilesListU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CbaseFloderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CbaseFloderU3E__0_2)); }
	inline String_t* get_U3CbaseFloderU3E__0_2() const { return ___U3CbaseFloderU3E__0_2; }
	inline String_t** get_address_of_U3CbaseFloderU3E__0_2() { return &___U3CbaseFloderU3E__0_2; }
	inline void set_U3CbaseFloderU3E__0_2(String_t* value)
	{
		___U3CbaseFloderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbaseFloderU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_3() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CiU3E__1_3)); }
	inline int32_t get_U3CiU3E__1_3() const { return ___U3CiU3E__1_3; }
	inline int32_t* get_address_of_U3CiU3E__1_3() { return &___U3CiU3E__1_3; }
	inline void set_U3CiU3E__1_3(int32_t value)
	{
		___U3CiU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CbytesU3E__2_4() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CbytesU3E__2_4)); }
	inline ByteU5BU5D_t3397334013* get_U3CbytesU3E__2_4() const { return ___U3CbytesU3E__2_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CbytesU3E__2_4() { return &___U3CbytesU3E__2_4; }
	inline void set_U3CbytesU3E__2_4(ByteU5BU5D_t3397334013* value)
	{
		___U3CbytesU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytesU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CpathSaveU3E__2_5() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CpathSaveU3E__2_5)); }
	inline String_t* get_U3CpathSaveU3E__2_5() const { return ___U3CpathSaveU3E__2_5; }
	inline String_t** get_address_of_U3CpathSaveU3E__2_5() { return &___U3CpathSaveU3E__2_5; }
	inline void set_U3CpathSaveU3E__2_5(String_t* value)
	{
		___U3CpathSaveU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathSaveU3E__2_5, value);
	}

	inline static int32_t get_offset_of_U3CfilesJsonStringU3E__0_6() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CfilesJsonStringU3E__0_6)); }
	inline String_t* get_U3CfilesJsonStringU3E__0_6() const { return ___U3CfilesJsonStringU3E__0_6; }
	inline String_t** get_address_of_U3CfilesJsonStringU3E__0_6() { return &___U3CfilesJsonStringU3E__0_6; }
	inline void set_U3CfilesJsonStringU3E__0_6(String_t* value)
	{
		___U3CfilesJsonStringU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfilesJsonStringU3E__0_6, value);
	}

	inline static int32_t get_offset_of_emails_7() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___emails_7)); }
	inline StringU5BU5D_t1642385972* get_emails_7() const { return ___emails_7; }
	inline StringU5BU5D_t1642385972** get_address_of_emails_7() { return &___emails_7; }
	inline void set_emails_7(StringU5BU5D_t1642385972* value)
	{
		___emails_7 = value;
		Il2CppCodeGenWriteBarrier(&___emails_7, value);
	}

	inline static int32_t get_offset_of_U3CemailJsonStringU3E__0_8() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U3CemailJsonStringU3E__0_8)); }
	inline String_t* get_U3CemailJsonStringU3E__0_8() const { return ___U3CemailJsonStringU3E__0_8; }
	inline String_t** get_address_of_U3CemailJsonStringU3E__0_8() { return &___U3CemailJsonStringU3E__0_8; }
	inline void set_U3CemailJsonStringU3E__0_8(String_t* value)
	{
		___U3CemailJsonStringU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CemailJsonStringU3E__0_8, value);
	}

	inline static int32_t get_offset_of_subject_9() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___subject_9)); }
	inline String_t* get_subject_9() const { return ___subject_9; }
	inline String_t** get_address_of_subject_9() { return &___subject_9; }
	inline void set_subject_9(String_t* value)
	{
		___subject_9 = value;
		Il2CppCodeGenWriteBarrier(&___subject_9, value);
	}

	inline static int32_t get_offset_of_msg_10() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___msg_10)); }
	inline String_t* get_msg_10() const { return ___msg_10; }
	inline String_t** get_address_of_msg_10() { return &___msg_10; }
	inline void set_msg_10(String_t* value)
	{
		___msg_10 = value;
		Il2CppCodeGenWriteBarrier(&___msg_10, value);
	}

	inline static int32_t get_offset_of_shareApp_11() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___shareApp_11)); }
	inline String_t* get_shareApp_11() const { return ___shareApp_11; }
	inline String_t** get_address_of_shareApp_11() { return &___shareApp_11; }
	inline void set_shareApp_11(String_t* value)
	{
		___shareApp_11 = value;
		Il2CppCodeGenWriteBarrier(&___shareApp_11, value);
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U24this_12)); }
	inline NativeShare_t1150945090 * get_U24this_12() const { return ___U24this_12; }
	inline NativeShare_t1150945090 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(NativeShare_t1150945090 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_12, value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CEncodeImageDataU3Ec__Iterator0_t2598202431, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
