﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t364059421;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// System.String
struct String_t;
// UnityEngine.Purchasing.FileReference
struct FileReference_t4250167507;
// UnityEngine.Purchasing.ProfileData
struct ProfileData_t3353328249;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreCatalogImpl
struct  StoreCatalogImpl_t946505162  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StoreCatalogImpl::m_AsyncUtil
	Il2CppObject * ___m_AsyncUtil_0;
	// UnityEngine.ILogger UnityEngine.Purchasing.StoreCatalogImpl::m_Logger
	Il2CppObject * ___m_Logger_1;
	// System.String UnityEngine.Purchasing.StoreCatalogImpl::m_CatalogURL
	String_t* ___m_CatalogURL_2;
	// System.String UnityEngine.Purchasing.StoreCatalogImpl::m_StoreName
	String_t* ___m_StoreName_3;
	// UnityEngine.Purchasing.FileReference UnityEngine.Purchasing.StoreCatalogImpl::m_cachedStoreCatalogReference
	FileReference_t4250167507 * ___m_cachedStoreCatalogReference_4;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_t946505162, ___m_AsyncUtil_0)); }
	inline Il2CppObject * get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline Il2CppObject ** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(Il2CppObject * value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_AsyncUtil_0, value);
	}

	inline static int32_t get_offset_of_m_Logger_1() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_t946505162, ___m_Logger_1)); }
	inline Il2CppObject * get_m_Logger_1() const { return ___m_Logger_1; }
	inline Il2CppObject ** get_address_of_m_Logger_1() { return &___m_Logger_1; }
	inline void set_m_Logger_1(Il2CppObject * value)
	{
		___m_Logger_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Logger_1, value);
	}

	inline static int32_t get_offset_of_m_CatalogURL_2() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_t946505162, ___m_CatalogURL_2)); }
	inline String_t* get_m_CatalogURL_2() const { return ___m_CatalogURL_2; }
	inline String_t** get_address_of_m_CatalogURL_2() { return &___m_CatalogURL_2; }
	inline void set_m_CatalogURL_2(String_t* value)
	{
		___m_CatalogURL_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_CatalogURL_2, value);
	}

	inline static int32_t get_offset_of_m_StoreName_3() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_t946505162, ___m_StoreName_3)); }
	inline String_t* get_m_StoreName_3() const { return ___m_StoreName_3; }
	inline String_t** get_address_of_m_StoreName_3() { return &___m_StoreName_3; }
	inline void set_m_StoreName_3(String_t* value)
	{
		___m_StoreName_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreName_3, value);
	}

	inline static int32_t get_offset_of_m_cachedStoreCatalogReference_4() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_t946505162, ___m_cachedStoreCatalogReference_4)); }
	inline FileReference_t4250167507 * get_m_cachedStoreCatalogReference_4() const { return ___m_cachedStoreCatalogReference_4; }
	inline FileReference_t4250167507 ** get_address_of_m_cachedStoreCatalogReference_4() { return &___m_cachedStoreCatalogReference_4; }
	inline void set_m_cachedStoreCatalogReference_4(FileReference_t4250167507 * value)
	{
		___m_cachedStoreCatalogReference_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_cachedStoreCatalogReference_4, value);
	}
};

struct StoreCatalogImpl_t946505162_StaticFields
{
public:
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.StoreCatalogImpl::profile
	ProfileData_t3353328249 * ___profile_5;

public:
	inline static int32_t get_offset_of_profile_5() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_t946505162_StaticFields, ___profile_5)); }
	inline ProfileData_t3353328249 * get_profile_5() const { return ___profile_5; }
	inline ProfileData_t3353328249 ** get_address_of_profile_5() { return &___profile_5; }
	inline void set_profile_5(ProfileData_t3353328249 * value)
	{
		___profile_5 = value;
		Il2CppCodeGenWriteBarrier(&___profile_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
