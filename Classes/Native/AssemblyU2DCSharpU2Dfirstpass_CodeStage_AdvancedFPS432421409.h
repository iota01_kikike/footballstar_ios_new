﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3744281392.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2998525377.h"

// System.String
struct String_t;
// System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel>
struct Action_1_t2800324759;
// System.Single[]
struct SingleU5BU5D_t577127397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData
struct  FPSCounterData_t432421409  : public UpdatebleCounterData_t3744281392
{
public:
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::warningLevelValue
	int32_t ___warningLevelValue_26;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::criticalLevelValue
	int32_t ___criticalLevelValue_27;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::resetAverageOnNewScene
	bool ___resetAverageOnNewScene_28;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::resetMinMaxOnNewScene
	bool ___resetMinMaxOnNewScene_29;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::minMaxIntervalsToSkip
	int32_t ___minMaxIntervalsToSkip_30;
	// System.Action`1<CodeStage.AdvancedFPSCounter.FPSLevel> CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::OnFPSLevelChange
	Action_1_t2800324759 * ___OnFPSLevelChange_31;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::newValue
	float ___newValue_32;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCachedMs
	String_t* ___colorCachedMs_33;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCachedMin
	String_t* ___colorCachedMin_34;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCachedMax
	String_t* ___colorCachedMax_35;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCachedAvg
	String_t* ___colorCachedAvg_36;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCachedRender
	String_t* ___colorCachedRender_37;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorWarningCached
	String_t* ___colorWarningCached_38;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorWarningCachedMs
	String_t* ___colorWarningCachedMs_39;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorWarningCachedMin
	String_t* ___colorWarningCachedMin_40;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorWarningCachedMax
	String_t* ___colorWarningCachedMax_41;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorWarningCachedAvg
	String_t* ___colorWarningCachedAvg_42;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCriticalCached
	String_t* ___colorCriticalCached_43;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCriticalCachedMs
	String_t* ___colorCriticalCachedMs_44;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCriticalCachedMin
	String_t* ___colorCriticalCachedMin_45;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCriticalCachedMax
	String_t* ___colorCriticalCachedMax_46;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCriticalCachedAvg
	String_t* ___colorCriticalCachedAvg_47;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::currentAverageSamples
	int32_t ___currentAverageSamples_48;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::currentAverageRaw
	float ___currentAverageRaw_49;
	// System.Single[] CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::accumulatedAverageSamples
	SingleU5BU5D_t577127397* ___accumulatedAverageSamples_50;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::minMaxIntervalsSkipped
	int32_t ___minMaxIntervalsSkipped_51;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::renderTimeBank
	float ___renderTimeBank_52;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::previousFrameCount
	int32_t ___previousFrameCount_53;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::milliseconds
	bool ___milliseconds_54;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::average
	bool ___average_55;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::averageMilliseconds
	bool ___averageMilliseconds_56;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::averageNewLine
	bool ___averageNewLine_57;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::averageSamples
	int32_t ___averageSamples_58;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::minMax
	bool ___minMax_59;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::minMaxMilliseconds
	bool ___minMaxMilliseconds_60;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::minMaxNewLine
	bool ___minMaxNewLine_61;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::minMaxTwoLines
	bool ___minMaxTwoLines_62;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::render
	bool ___render_63;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::renderNewLine
	bool ___renderNewLine_64;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::renderAutoAdd
	bool ___renderAutoAdd_65;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorWarning
	Color_t2020392075  ___colorWarning_66;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorCritical
	Color_t2020392075  ___colorCritical_67;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::colorRender
	Color_t2020392075  ___colorRender_68;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastValue>k__BackingField
	int32_t ___U3CLastValueU3Ek__BackingField_69;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastMillisecondsValue>k__BackingField
	float ___U3CLastMillisecondsValueU3Ek__BackingField_70;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastRenderValue>k__BackingField
	float ___U3CLastRenderValueU3Ek__BackingField_71;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastAverageValue>k__BackingField
	int32_t ___U3CLastAverageValueU3Ek__BackingField_72;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastAverageMillisecondsValue>k__BackingField
	float ___U3CLastAverageMillisecondsValueU3Ek__BackingField_73;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastMinimumValue>k__BackingField
	int32_t ___U3CLastMinimumValueU3Ek__BackingField_74;
	// System.Int32 CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastMaximumValue>k__BackingField
	int32_t ___U3CLastMaximumValueU3Ek__BackingField_75;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastMinMillisecondsValue>k__BackingField
	float ___U3CLastMinMillisecondsValueU3Ek__BackingField_76;
	// System.Single CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<LastMaxMillisecondsValue>k__BackingField
	float ___U3CLastMaxMillisecondsValueU3Ek__BackingField_77;
	// CodeStage.AdvancedFPSCounter.FPSLevel CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData::<CurrentFpsLevel>k__BackingField
	uint8_t ___U3CCurrentFpsLevelU3Ek__BackingField_78;

public:
	inline static int32_t get_offset_of_warningLevelValue_26() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___warningLevelValue_26)); }
	inline int32_t get_warningLevelValue_26() const { return ___warningLevelValue_26; }
	inline int32_t* get_address_of_warningLevelValue_26() { return &___warningLevelValue_26; }
	inline void set_warningLevelValue_26(int32_t value)
	{
		___warningLevelValue_26 = value;
	}

	inline static int32_t get_offset_of_criticalLevelValue_27() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___criticalLevelValue_27)); }
	inline int32_t get_criticalLevelValue_27() const { return ___criticalLevelValue_27; }
	inline int32_t* get_address_of_criticalLevelValue_27() { return &___criticalLevelValue_27; }
	inline void set_criticalLevelValue_27(int32_t value)
	{
		___criticalLevelValue_27 = value;
	}

	inline static int32_t get_offset_of_resetAverageOnNewScene_28() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___resetAverageOnNewScene_28)); }
	inline bool get_resetAverageOnNewScene_28() const { return ___resetAverageOnNewScene_28; }
	inline bool* get_address_of_resetAverageOnNewScene_28() { return &___resetAverageOnNewScene_28; }
	inline void set_resetAverageOnNewScene_28(bool value)
	{
		___resetAverageOnNewScene_28 = value;
	}

	inline static int32_t get_offset_of_resetMinMaxOnNewScene_29() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___resetMinMaxOnNewScene_29)); }
	inline bool get_resetMinMaxOnNewScene_29() const { return ___resetMinMaxOnNewScene_29; }
	inline bool* get_address_of_resetMinMaxOnNewScene_29() { return &___resetMinMaxOnNewScene_29; }
	inline void set_resetMinMaxOnNewScene_29(bool value)
	{
		___resetMinMaxOnNewScene_29 = value;
	}

	inline static int32_t get_offset_of_minMaxIntervalsToSkip_30() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___minMaxIntervalsToSkip_30)); }
	inline int32_t get_minMaxIntervalsToSkip_30() const { return ___minMaxIntervalsToSkip_30; }
	inline int32_t* get_address_of_minMaxIntervalsToSkip_30() { return &___minMaxIntervalsToSkip_30; }
	inline void set_minMaxIntervalsToSkip_30(int32_t value)
	{
		___minMaxIntervalsToSkip_30 = value;
	}

	inline static int32_t get_offset_of_OnFPSLevelChange_31() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___OnFPSLevelChange_31)); }
	inline Action_1_t2800324759 * get_OnFPSLevelChange_31() const { return ___OnFPSLevelChange_31; }
	inline Action_1_t2800324759 ** get_address_of_OnFPSLevelChange_31() { return &___OnFPSLevelChange_31; }
	inline void set_OnFPSLevelChange_31(Action_1_t2800324759 * value)
	{
		___OnFPSLevelChange_31 = value;
		Il2CppCodeGenWriteBarrier(&___OnFPSLevelChange_31, value);
	}

	inline static int32_t get_offset_of_newValue_32() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___newValue_32)); }
	inline float get_newValue_32() const { return ___newValue_32; }
	inline float* get_address_of_newValue_32() { return &___newValue_32; }
	inline void set_newValue_32(float value)
	{
		___newValue_32 = value;
	}

	inline static int32_t get_offset_of_colorCachedMs_33() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCachedMs_33)); }
	inline String_t* get_colorCachedMs_33() const { return ___colorCachedMs_33; }
	inline String_t** get_address_of_colorCachedMs_33() { return &___colorCachedMs_33; }
	inline void set_colorCachedMs_33(String_t* value)
	{
		___colorCachedMs_33 = value;
		Il2CppCodeGenWriteBarrier(&___colorCachedMs_33, value);
	}

	inline static int32_t get_offset_of_colorCachedMin_34() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCachedMin_34)); }
	inline String_t* get_colorCachedMin_34() const { return ___colorCachedMin_34; }
	inline String_t** get_address_of_colorCachedMin_34() { return &___colorCachedMin_34; }
	inline void set_colorCachedMin_34(String_t* value)
	{
		___colorCachedMin_34 = value;
		Il2CppCodeGenWriteBarrier(&___colorCachedMin_34, value);
	}

	inline static int32_t get_offset_of_colorCachedMax_35() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCachedMax_35)); }
	inline String_t* get_colorCachedMax_35() const { return ___colorCachedMax_35; }
	inline String_t** get_address_of_colorCachedMax_35() { return &___colorCachedMax_35; }
	inline void set_colorCachedMax_35(String_t* value)
	{
		___colorCachedMax_35 = value;
		Il2CppCodeGenWriteBarrier(&___colorCachedMax_35, value);
	}

	inline static int32_t get_offset_of_colorCachedAvg_36() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCachedAvg_36)); }
	inline String_t* get_colorCachedAvg_36() const { return ___colorCachedAvg_36; }
	inline String_t** get_address_of_colorCachedAvg_36() { return &___colorCachedAvg_36; }
	inline void set_colorCachedAvg_36(String_t* value)
	{
		___colorCachedAvg_36 = value;
		Il2CppCodeGenWriteBarrier(&___colorCachedAvg_36, value);
	}

	inline static int32_t get_offset_of_colorCachedRender_37() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCachedRender_37)); }
	inline String_t* get_colorCachedRender_37() const { return ___colorCachedRender_37; }
	inline String_t** get_address_of_colorCachedRender_37() { return &___colorCachedRender_37; }
	inline void set_colorCachedRender_37(String_t* value)
	{
		___colorCachedRender_37 = value;
		Il2CppCodeGenWriteBarrier(&___colorCachedRender_37, value);
	}

	inline static int32_t get_offset_of_colorWarningCached_38() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorWarningCached_38)); }
	inline String_t* get_colorWarningCached_38() const { return ___colorWarningCached_38; }
	inline String_t** get_address_of_colorWarningCached_38() { return &___colorWarningCached_38; }
	inline void set_colorWarningCached_38(String_t* value)
	{
		___colorWarningCached_38 = value;
		Il2CppCodeGenWriteBarrier(&___colorWarningCached_38, value);
	}

	inline static int32_t get_offset_of_colorWarningCachedMs_39() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorWarningCachedMs_39)); }
	inline String_t* get_colorWarningCachedMs_39() const { return ___colorWarningCachedMs_39; }
	inline String_t** get_address_of_colorWarningCachedMs_39() { return &___colorWarningCachedMs_39; }
	inline void set_colorWarningCachedMs_39(String_t* value)
	{
		___colorWarningCachedMs_39 = value;
		Il2CppCodeGenWriteBarrier(&___colorWarningCachedMs_39, value);
	}

	inline static int32_t get_offset_of_colorWarningCachedMin_40() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorWarningCachedMin_40)); }
	inline String_t* get_colorWarningCachedMin_40() const { return ___colorWarningCachedMin_40; }
	inline String_t** get_address_of_colorWarningCachedMin_40() { return &___colorWarningCachedMin_40; }
	inline void set_colorWarningCachedMin_40(String_t* value)
	{
		___colorWarningCachedMin_40 = value;
		Il2CppCodeGenWriteBarrier(&___colorWarningCachedMin_40, value);
	}

	inline static int32_t get_offset_of_colorWarningCachedMax_41() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorWarningCachedMax_41)); }
	inline String_t* get_colorWarningCachedMax_41() const { return ___colorWarningCachedMax_41; }
	inline String_t** get_address_of_colorWarningCachedMax_41() { return &___colorWarningCachedMax_41; }
	inline void set_colorWarningCachedMax_41(String_t* value)
	{
		___colorWarningCachedMax_41 = value;
		Il2CppCodeGenWriteBarrier(&___colorWarningCachedMax_41, value);
	}

	inline static int32_t get_offset_of_colorWarningCachedAvg_42() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorWarningCachedAvg_42)); }
	inline String_t* get_colorWarningCachedAvg_42() const { return ___colorWarningCachedAvg_42; }
	inline String_t** get_address_of_colorWarningCachedAvg_42() { return &___colorWarningCachedAvg_42; }
	inline void set_colorWarningCachedAvg_42(String_t* value)
	{
		___colorWarningCachedAvg_42 = value;
		Il2CppCodeGenWriteBarrier(&___colorWarningCachedAvg_42, value);
	}

	inline static int32_t get_offset_of_colorCriticalCached_43() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCriticalCached_43)); }
	inline String_t* get_colorCriticalCached_43() const { return ___colorCriticalCached_43; }
	inline String_t** get_address_of_colorCriticalCached_43() { return &___colorCriticalCached_43; }
	inline void set_colorCriticalCached_43(String_t* value)
	{
		___colorCriticalCached_43 = value;
		Il2CppCodeGenWriteBarrier(&___colorCriticalCached_43, value);
	}

	inline static int32_t get_offset_of_colorCriticalCachedMs_44() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCriticalCachedMs_44)); }
	inline String_t* get_colorCriticalCachedMs_44() const { return ___colorCriticalCachedMs_44; }
	inline String_t** get_address_of_colorCriticalCachedMs_44() { return &___colorCriticalCachedMs_44; }
	inline void set_colorCriticalCachedMs_44(String_t* value)
	{
		___colorCriticalCachedMs_44 = value;
		Il2CppCodeGenWriteBarrier(&___colorCriticalCachedMs_44, value);
	}

	inline static int32_t get_offset_of_colorCriticalCachedMin_45() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCriticalCachedMin_45)); }
	inline String_t* get_colorCriticalCachedMin_45() const { return ___colorCriticalCachedMin_45; }
	inline String_t** get_address_of_colorCriticalCachedMin_45() { return &___colorCriticalCachedMin_45; }
	inline void set_colorCriticalCachedMin_45(String_t* value)
	{
		___colorCriticalCachedMin_45 = value;
		Il2CppCodeGenWriteBarrier(&___colorCriticalCachedMin_45, value);
	}

	inline static int32_t get_offset_of_colorCriticalCachedMax_46() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCriticalCachedMax_46)); }
	inline String_t* get_colorCriticalCachedMax_46() const { return ___colorCriticalCachedMax_46; }
	inline String_t** get_address_of_colorCriticalCachedMax_46() { return &___colorCriticalCachedMax_46; }
	inline void set_colorCriticalCachedMax_46(String_t* value)
	{
		___colorCriticalCachedMax_46 = value;
		Il2CppCodeGenWriteBarrier(&___colorCriticalCachedMax_46, value);
	}

	inline static int32_t get_offset_of_colorCriticalCachedAvg_47() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCriticalCachedAvg_47)); }
	inline String_t* get_colorCriticalCachedAvg_47() const { return ___colorCriticalCachedAvg_47; }
	inline String_t** get_address_of_colorCriticalCachedAvg_47() { return &___colorCriticalCachedAvg_47; }
	inline void set_colorCriticalCachedAvg_47(String_t* value)
	{
		___colorCriticalCachedAvg_47 = value;
		Il2CppCodeGenWriteBarrier(&___colorCriticalCachedAvg_47, value);
	}

	inline static int32_t get_offset_of_currentAverageSamples_48() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___currentAverageSamples_48)); }
	inline int32_t get_currentAverageSamples_48() const { return ___currentAverageSamples_48; }
	inline int32_t* get_address_of_currentAverageSamples_48() { return &___currentAverageSamples_48; }
	inline void set_currentAverageSamples_48(int32_t value)
	{
		___currentAverageSamples_48 = value;
	}

	inline static int32_t get_offset_of_currentAverageRaw_49() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___currentAverageRaw_49)); }
	inline float get_currentAverageRaw_49() const { return ___currentAverageRaw_49; }
	inline float* get_address_of_currentAverageRaw_49() { return &___currentAverageRaw_49; }
	inline void set_currentAverageRaw_49(float value)
	{
		___currentAverageRaw_49 = value;
	}

	inline static int32_t get_offset_of_accumulatedAverageSamples_50() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___accumulatedAverageSamples_50)); }
	inline SingleU5BU5D_t577127397* get_accumulatedAverageSamples_50() const { return ___accumulatedAverageSamples_50; }
	inline SingleU5BU5D_t577127397** get_address_of_accumulatedAverageSamples_50() { return &___accumulatedAverageSamples_50; }
	inline void set_accumulatedAverageSamples_50(SingleU5BU5D_t577127397* value)
	{
		___accumulatedAverageSamples_50 = value;
		Il2CppCodeGenWriteBarrier(&___accumulatedAverageSamples_50, value);
	}

	inline static int32_t get_offset_of_minMaxIntervalsSkipped_51() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___minMaxIntervalsSkipped_51)); }
	inline int32_t get_minMaxIntervalsSkipped_51() const { return ___minMaxIntervalsSkipped_51; }
	inline int32_t* get_address_of_minMaxIntervalsSkipped_51() { return &___minMaxIntervalsSkipped_51; }
	inline void set_minMaxIntervalsSkipped_51(int32_t value)
	{
		___minMaxIntervalsSkipped_51 = value;
	}

	inline static int32_t get_offset_of_renderTimeBank_52() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___renderTimeBank_52)); }
	inline float get_renderTimeBank_52() const { return ___renderTimeBank_52; }
	inline float* get_address_of_renderTimeBank_52() { return &___renderTimeBank_52; }
	inline void set_renderTimeBank_52(float value)
	{
		___renderTimeBank_52 = value;
	}

	inline static int32_t get_offset_of_previousFrameCount_53() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___previousFrameCount_53)); }
	inline int32_t get_previousFrameCount_53() const { return ___previousFrameCount_53; }
	inline int32_t* get_address_of_previousFrameCount_53() { return &___previousFrameCount_53; }
	inline void set_previousFrameCount_53(int32_t value)
	{
		___previousFrameCount_53 = value;
	}

	inline static int32_t get_offset_of_milliseconds_54() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___milliseconds_54)); }
	inline bool get_milliseconds_54() const { return ___milliseconds_54; }
	inline bool* get_address_of_milliseconds_54() { return &___milliseconds_54; }
	inline void set_milliseconds_54(bool value)
	{
		___milliseconds_54 = value;
	}

	inline static int32_t get_offset_of_average_55() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___average_55)); }
	inline bool get_average_55() const { return ___average_55; }
	inline bool* get_address_of_average_55() { return &___average_55; }
	inline void set_average_55(bool value)
	{
		___average_55 = value;
	}

	inline static int32_t get_offset_of_averageMilliseconds_56() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___averageMilliseconds_56)); }
	inline bool get_averageMilliseconds_56() const { return ___averageMilliseconds_56; }
	inline bool* get_address_of_averageMilliseconds_56() { return &___averageMilliseconds_56; }
	inline void set_averageMilliseconds_56(bool value)
	{
		___averageMilliseconds_56 = value;
	}

	inline static int32_t get_offset_of_averageNewLine_57() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___averageNewLine_57)); }
	inline bool get_averageNewLine_57() const { return ___averageNewLine_57; }
	inline bool* get_address_of_averageNewLine_57() { return &___averageNewLine_57; }
	inline void set_averageNewLine_57(bool value)
	{
		___averageNewLine_57 = value;
	}

	inline static int32_t get_offset_of_averageSamples_58() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___averageSamples_58)); }
	inline int32_t get_averageSamples_58() const { return ___averageSamples_58; }
	inline int32_t* get_address_of_averageSamples_58() { return &___averageSamples_58; }
	inline void set_averageSamples_58(int32_t value)
	{
		___averageSamples_58 = value;
	}

	inline static int32_t get_offset_of_minMax_59() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___minMax_59)); }
	inline bool get_minMax_59() const { return ___minMax_59; }
	inline bool* get_address_of_minMax_59() { return &___minMax_59; }
	inline void set_minMax_59(bool value)
	{
		___minMax_59 = value;
	}

	inline static int32_t get_offset_of_minMaxMilliseconds_60() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___minMaxMilliseconds_60)); }
	inline bool get_minMaxMilliseconds_60() const { return ___minMaxMilliseconds_60; }
	inline bool* get_address_of_minMaxMilliseconds_60() { return &___minMaxMilliseconds_60; }
	inline void set_minMaxMilliseconds_60(bool value)
	{
		___minMaxMilliseconds_60 = value;
	}

	inline static int32_t get_offset_of_minMaxNewLine_61() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___minMaxNewLine_61)); }
	inline bool get_minMaxNewLine_61() const { return ___minMaxNewLine_61; }
	inline bool* get_address_of_minMaxNewLine_61() { return &___minMaxNewLine_61; }
	inline void set_minMaxNewLine_61(bool value)
	{
		___minMaxNewLine_61 = value;
	}

	inline static int32_t get_offset_of_minMaxTwoLines_62() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___minMaxTwoLines_62)); }
	inline bool get_minMaxTwoLines_62() const { return ___minMaxTwoLines_62; }
	inline bool* get_address_of_minMaxTwoLines_62() { return &___minMaxTwoLines_62; }
	inline void set_minMaxTwoLines_62(bool value)
	{
		___minMaxTwoLines_62 = value;
	}

	inline static int32_t get_offset_of_render_63() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___render_63)); }
	inline bool get_render_63() const { return ___render_63; }
	inline bool* get_address_of_render_63() { return &___render_63; }
	inline void set_render_63(bool value)
	{
		___render_63 = value;
	}

	inline static int32_t get_offset_of_renderNewLine_64() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___renderNewLine_64)); }
	inline bool get_renderNewLine_64() const { return ___renderNewLine_64; }
	inline bool* get_address_of_renderNewLine_64() { return &___renderNewLine_64; }
	inline void set_renderNewLine_64(bool value)
	{
		___renderNewLine_64 = value;
	}

	inline static int32_t get_offset_of_renderAutoAdd_65() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___renderAutoAdd_65)); }
	inline bool get_renderAutoAdd_65() const { return ___renderAutoAdd_65; }
	inline bool* get_address_of_renderAutoAdd_65() { return &___renderAutoAdd_65; }
	inline void set_renderAutoAdd_65(bool value)
	{
		___renderAutoAdd_65 = value;
	}

	inline static int32_t get_offset_of_colorWarning_66() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorWarning_66)); }
	inline Color_t2020392075  get_colorWarning_66() const { return ___colorWarning_66; }
	inline Color_t2020392075 * get_address_of_colorWarning_66() { return &___colorWarning_66; }
	inline void set_colorWarning_66(Color_t2020392075  value)
	{
		___colorWarning_66 = value;
	}

	inline static int32_t get_offset_of_colorCritical_67() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorCritical_67)); }
	inline Color_t2020392075  get_colorCritical_67() const { return ___colorCritical_67; }
	inline Color_t2020392075 * get_address_of_colorCritical_67() { return &___colorCritical_67; }
	inline void set_colorCritical_67(Color_t2020392075  value)
	{
		___colorCritical_67 = value;
	}

	inline static int32_t get_offset_of_colorRender_68() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___colorRender_68)); }
	inline Color_t2020392075  get_colorRender_68() const { return ___colorRender_68; }
	inline Color_t2020392075 * get_address_of_colorRender_68() { return &___colorRender_68; }
	inline void set_colorRender_68(Color_t2020392075  value)
	{
		___colorRender_68 = value;
	}

	inline static int32_t get_offset_of_U3CLastValueU3Ek__BackingField_69() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastValueU3Ek__BackingField_69)); }
	inline int32_t get_U3CLastValueU3Ek__BackingField_69() const { return ___U3CLastValueU3Ek__BackingField_69; }
	inline int32_t* get_address_of_U3CLastValueU3Ek__BackingField_69() { return &___U3CLastValueU3Ek__BackingField_69; }
	inline void set_U3CLastValueU3Ek__BackingField_69(int32_t value)
	{
		___U3CLastValueU3Ek__BackingField_69 = value;
	}

	inline static int32_t get_offset_of_U3CLastMillisecondsValueU3Ek__BackingField_70() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastMillisecondsValueU3Ek__BackingField_70)); }
	inline float get_U3CLastMillisecondsValueU3Ek__BackingField_70() const { return ___U3CLastMillisecondsValueU3Ek__BackingField_70; }
	inline float* get_address_of_U3CLastMillisecondsValueU3Ek__BackingField_70() { return &___U3CLastMillisecondsValueU3Ek__BackingField_70; }
	inline void set_U3CLastMillisecondsValueU3Ek__BackingField_70(float value)
	{
		___U3CLastMillisecondsValueU3Ek__BackingField_70 = value;
	}

	inline static int32_t get_offset_of_U3CLastRenderValueU3Ek__BackingField_71() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastRenderValueU3Ek__BackingField_71)); }
	inline float get_U3CLastRenderValueU3Ek__BackingField_71() const { return ___U3CLastRenderValueU3Ek__BackingField_71; }
	inline float* get_address_of_U3CLastRenderValueU3Ek__BackingField_71() { return &___U3CLastRenderValueU3Ek__BackingField_71; }
	inline void set_U3CLastRenderValueU3Ek__BackingField_71(float value)
	{
		___U3CLastRenderValueU3Ek__BackingField_71 = value;
	}

	inline static int32_t get_offset_of_U3CLastAverageValueU3Ek__BackingField_72() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastAverageValueU3Ek__BackingField_72)); }
	inline int32_t get_U3CLastAverageValueU3Ek__BackingField_72() const { return ___U3CLastAverageValueU3Ek__BackingField_72; }
	inline int32_t* get_address_of_U3CLastAverageValueU3Ek__BackingField_72() { return &___U3CLastAverageValueU3Ek__BackingField_72; }
	inline void set_U3CLastAverageValueU3Ek__BackingField_72(int32_t value)
	{
		___U3CLastAverageValueU3Ek__BackingField_72 = value;
	}

	inline static int32_t get_offset_of_U3CLastAverageMillisecondsValueU3Ek__BackingField_73() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastAverageMillisecondsValueU3Ek__BackingField_73)); }
	inline float get_U3CLastAverageMillisecondsValueU3Ek__BackingField_73() const { return ___U3CLastAverageMillisecondsValueU3Ek__BackingField_73; }
	inline float* get_address_of_U3CLastAverageMillisecondsValueU3Ek__BackingField_73() { return &___U3CLastAverageMillisecondsValueU3Ek__BackingField_73; }
	inline void set_U3CLastAverageMillisecondsValueU3Ek__BackingField_73(float value)
	{
		___U3CLastAverageMillisecondsValueU3Ek__BackingField_73 = value;
	}

	inline static int32_t get_offset_of_U3CLastMinimumValueU3Ek__BackingField_74() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastMinimumValueU3Ek__BackingField_74)); }
	inline int32_t get_U3CLastMinimumValueU3Ek__BackingField_74() const { return ___U3CLastMinimumValueU3Ek__BackingField_74; }
	inline int32_t* get_address_of_U3CLastMinimumValueU3Ek__BackingField_74() { return &___U3CLastMinimumValueU3Ek__BackingField_74; }
	inline void set_U3CLastMinimumValueU3Ek__BackingField_74(int32_t value)
	{
		___U3CLastMinimumValueU3Ek__BackingField_74 = value;
	}

	inline static int32_t get_offset_of_U3CLastMaximumValueU3Ek__BackingField_75() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastMaximumValueU3Ek__BackingField_75)); }
	inline int32_t get_U3CLastMaximumValueU3Ek__BackingField_75() const { return ___U3CLastMaximumValueU3Ek__BackingField_75; }
	inline int32_t* get_address_of_U3CLastMaximumValueU3Ek__BackingField_75() { return &___U3CLastMaximumValueU3Ek__BackingField_75; }
	inline void set_U3CLastMaximumValueU3Ek__BackingField_75(int32_t value)
	{
		___U3CLastMaximumValueU3Ek__BackingField_75 = value;
	}

	inline static int32_t get_offset_of_U3CLastMinMillisecondsValueU3Ek__BackingField_76() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastMinMillisecondsValueU3Ek__BackingField_76)); }
	inline float get_U3CLastMinMillisecondsValueU3Ek__BackingField_76() const { return ___U3CLastMinMillisecondsValueU3Ek__BackingField_76; }
	inline float* get_address_of_U3CLastMinMillisecondsValueU3Ek__BackingField_76() { return &___U3CLastMinMillisecondsValueU3Ek__BackingField_76; }
	inline void set_U3CLastMinMillisecondsValueU3Ek__BackingField_76(float value)
	{
		___U3CLastMinMillisecondsValueU3Ek__BackingField_76 = value;
	}

	inline static int32_t get_offset_of_U3CLastMaxMillisecondsValueU3Ek__BackingField_77() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CLastMaxMillisecondsValueU3Ek__BackingField_77)); }
	inline float get_U3CLastMaxMillisecondsValueU3Ek__BackingField_77() const { return ___U3CLastMaxMillisecondsValueU3Ek__BackingField_77; }
	inline float* get_address_of_U3CLastMaxMillisecondsValueU3Ek__BackingField_77() { return &___U3CLastMaxMillisecondsValueU3Ek__BackingField_77; }
	inline void set_U3CLastMaxMillisecondsValueU3Ek__BackingField_77(float value)
	{
		___U3CLastMaxMillisecondsValueU3Ek__BackingField_77 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentFpsLevelU3Ek__BackingField_78() { return static_cast<int32_t>(offsetof(FPSCounterData_t432421409, ___U3CCurrentFpsLevelU3Ek__BackingField_78)); }
	inline uint8_t get_U3CCurrentFpsLevelU3Ek__BackingField_78() const { return ___U3CCurrentFpsLevelU3Ek__BackingField_78; }
	inline uint8_t* get_address_of_U3CCurrentFpsLevelU3Ek__BackingField_78() { return &___U3CCurrentFpsLevelU3Ek__BackingField_78; }
	inline void set_U3CCurrentFpsLevelU3Ek__BackingField_78(uint8_t value)
	{
		___U3CCurrentFpsLevelU3Ek__BackingField_78 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
