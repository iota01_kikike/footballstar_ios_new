﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Tacticsoft.Examples.ScrollingEventsHandler
struct ScrollingEventsHandler_t401430696;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0
struct  U3CAnimateToScrollYU3Ec__Iterator0_t3108120209  : public Il2CppObject
{
public:
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<startScrollY>__0
	float ___U3CstartScrollYU3E__0_1;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::time
	float ___time_2;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<endTime>__0
	float ___U3CendTimeU3E__0_3;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::<relativeProgress>__1
	float ___U3CrelativeProgressU3E__1_4;
	// System.Single Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::scrollY
	float ___scrollY_5;
	// Tacticsoft.Examples.ScrollingEventsHandler Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$this
	ScrollingEventsHandler_t401430696 * ___U24this_6;
	// System.Object Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 Tacticsoft.Examples.ScrollingEventsHandler/<AnimateToScrollY>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartScrollYU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U3CstartScrollYU3E__0_1)); }
	inline float get_U3CstartScrollYU3E__0_1() const { return ___U3CstartScrollYU3E__0_1; }
	inline float* get_address_of_U3CstartScrollYU3E__0_1() { return &___U3CstartScrollYU3E__0_1; }
	inline void set_U3CstartScrollYU3E__0_1(float value)
	{
		___U3CstartScrollYU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_U3CendTimeU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U3CendTimeU3E__0_3)); }
	inline float get_U3CendTimeU3E__0_3() const { return ___U3CendTimeU3E__0_3; }
	inline float* get_address_of_U3CendTimeU3E__0_3() { return &___U3CendTimeU3E__0_3; }
	inline void set_U3CendTimeU3E__0_3(float value)
	{
		___U3CendTimeU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CrelativeProgressU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U3CrelativeProgressU3E__1_4)); }
	inline float get_U3CrelativeProgressU3E__1_4() const { return ___U3CrelativeProgressU3E__1_4; }
	inline float* get_address_of_U3CrelativeProgressU3E__1_4() { return &___U3CrelativeProgressU3E__1_4; }
	inline void set_U3CrelativeProgressU3E__1_4(float value)
	{
		___U3CrelativeProgressU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_scrollY_5() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___scrollY_5)); }
	inline float get_scrollY_5() const { return ___scrollY_5; }
	inline float* get_address_of_scrollY_5() { return &___scrollY_5; }
	inline void set_scrollY_5(float value)
	{
		___scrollY_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U24this_6)); }
	inline ScrollingEventsHandler_t401430696 * get_U24this_6() const { return ___U24this_6; }
	inline ScrollingEventsHandler_t401430696 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ScrollingEventsHandler_t401430696 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAnimateToScrollYU3Ec__Iterator0_t3108120209, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
