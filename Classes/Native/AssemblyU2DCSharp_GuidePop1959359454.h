﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuidePop1
struct  GuidePop1_t959359454  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button GuidePop1::NextButton
	Button_t2872111280 * ___NextButton_2;
	// UnityEngine.GameObject GuidePop1::Unit1
	GameObject_t1756533147 * ___Unit1_3;
	// UnityEngine.GameObject GuidePop1::Unit2
	GameObject_t1756533147 * ___Unit2_4;
	// UnityEngine.GameObject GuidePop1::Unit3
	GameObject_t1756533147 * ___Unit3_5;
	// UnityEngine.GameObject GuidePop1::Unit4
	GameObject_t1756533147 * ___Unit4_6;
	// UnityEngine.GameObject GuidePop1::ArrowLeft
	GameObject_t1756533147 * ___ArrowLeft_7;
	// UnityEngine.GameObject GuidePop1::HandLeft
	GameObject_t1756533147 * ___HandLeft_8;
	// UnityEngine.GameObject GuidePop1::ArrowRight
	GameObject_t1756533147 * ___ArrowRight_9;
	// UnityEngine.GameObject GuidePop1::HandRight
	GameObject_t1756533147 * ___HandRight_10;

public:
	inline static int32_t get_offset_of_NextButton_2() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___NextButton_2)); }
	inline Button_t2872111280 * get_NextButton_2() const { return ___NextButton_2; }
	inline Button_t2872111280 ** get_address_of_NextButton_2() { return &___NextButton_2; }
	inline void set_NextButton_2(Button_t2872111280 * value)
	{
		___NextButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___NextButton_2, value);
	}

	inline static int32_t get_offset_of_Unit1_3() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___Unit1_3)); }
	inline GameObject_t1756533147 * get_Unit1_3() const { return ___Unit1_3; }
	inline GameObject_t1756533147 ** get_address_of_Unit1_3() { return &___Unit1_3; }
	inline void set_Unit1_3(GameObject_t1756533147 * value)
	{
		___Unit1_3 = value;
		Il2CppCodeGenWriteBarrier(&___Unit1_3, value);
	}

	inline static int32_t get_offset_of_Unit2_4() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___Unit2_4)); }
	inline GameObject_t1756533147 * get_Unit2_4() const { return ___Unit2_4; }
	inline GameObject_t1756533147 ** get_address_of_Unit2_4() { return &___Unit2_4; }
	inline void set_Unit2_4(GameObject_t1756533147 * value)
	{
		___Unit2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Unit2_4, value);
	}

	inline static int32_t get_offset_of_Unit3_5() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___Unit3_5)); }
	inline GameObject_t1756533147 * get_Unit3_5() const { return ___Unit3_5; }
	inline GameObject_t1756533147 ** get_address_of_Unit3_5() { return &___Unit3_5; }
	inline void set_Unit3_5(GameObject_t1756533147 * value)
	{
		___Unit3_5 = value;
		Il2CppCodeGenWriteBarrier(&___Unit3_5, value);
	}

	inline static int32_t get_offset_of_Unit4_6() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___Unit4_6)); }
	inline GameObject_t1756533147 * get_Unit4_6() const { return ___Unit4_6; }
	inline GameObject_t1756533147 ** get_address_of_Unit4_6() { return &___Unit4_6; }
	inline void set_Unit4_6(GameObject_t1756533147 * value)
	{
		___Unit4_6 = value;
		Il2CppCodeGenWriteBarrier(&___Unit4_6, value);
	}

	inline static int32_t get_offset_of_ArrowLeft_7() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___ArrowLeft_7)); }
	inline GameObject_t1756533147 * get_ArrowLeft_7() const { return ___ArrowLeft_7; }
	inline GameObject_t1756533147 ** get_address_of_ArrowLeft_7() { return &___ArrowLeft_7; }
	inline void set_ArrowLeft_7(GameObject_t1756533147 * value)
	{
		___ArrowLeft_7 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowLeft_7, value);
	}

	inline static int32_t get_offset_of_HandLeft_8() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___HandLeft_8)); }
	inline GameObject_t1756533147 * get_HandLeft_8() const { return ___HandLeft_8; }
	inline GameObject_t1756533147 ** get_address_of_HandLeft_8() { return &___HandLeft_8; }
	inline void set_HandLeft_8(GameObject_t1756533147 * value)
	{
		___HandLeft_8 = value;
		Il2CppCodeGenWriteBarrier(&___HandLeft_8, value);
	}

	inline static int32_t get_offset_of_ArrowRight_9() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___ArrowRight_9)); }
	inline GameObject_t1756533147 * get_ArrowRight_9() const { return ___ArrowRight_9; }
	inline GameObject_t1756533147 ** get_address_of_ArrowRight_9() { return &___ArrowRight_9; }
	inline void set_ArrowRight_9(GameObject_t1756533147 * value)
	{
		___ArrowRight_9 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowRight_9, value);
	}

	inline static int32_t get_offset_of_HandRight_10() { return static_cast<int32_t>(offsetof(GuidePop1_t959359454, ___HandRight_10)); }
	inline GameObject_t1756533147 * get_HandRight_10() const { return ___HandRight_10; }
	inline GameObject_t1756533147 ** get_address_of_HandRight_10() { return &___HandRight_10; }
	inline void set_HandRight_10(GameObject_t1756533147 * value)
	{
		___HandRight_10 = value;
		Il2CppCodeGenWriteBarrier(&___HandRight_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
