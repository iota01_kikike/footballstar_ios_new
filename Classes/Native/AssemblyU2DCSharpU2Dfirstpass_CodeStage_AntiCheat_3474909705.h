﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte4
struct  ACTkByte4_t3474909705 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b4
	uint8_t ___b4_3;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte4_t3474909705, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte4_t3474909705, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte4_t3474909705, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte4_t3474909705, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
