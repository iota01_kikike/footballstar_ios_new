﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitInfoPop
struct  UnitInfoPop_t2203256789  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button UnitInfoPop::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// UnityEngine.UI.Text UnitInfoPop::CountText
	Text_t356221433 * ___CountText_3;
	// UnityEngine.GameObject[] UnitInfoPop::UnitObject
	GameObjectU5BU5D_t3057952154* ___UnitObject_4;
	// UnityEngine.UI.Text[] UnitInfoPop::UnitNameText
	TextU5BU5D_t4216439300* ___UnitNameText_5;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(UnitInfoPop_t2203256789, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_CountText_3() { return static_cast<int32_t>(offsetof(UnitInfoPop_t2203256789, ___CountText_3)); }
	inline Text_t356221433 * get_CountText_3() const { return ___CountText_3; }
	inline Text_t356221433 ** get_address_of_CountText_3() { return &___CountText_3; }
	inline void set_CountText_3(Text_t356221433 * value)
	{
		___CountText_3 = value;
		Il2CppCodeGenWriteBarrier(&___CountText_3, value);
	}

	inline static int32_t get_offset_of_UnitObject_4() { return static_cast<int32_t>(offsetof(UnitInfoPop_t2203256789, ___UnitObject_4)); }
	inline GameObjectU5BU5D_t3057952154* get_UnitObject_4() const { return ___UnitObject_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_UnitObject_4() { return &___UnitObject_4; }
	inline void set_UnitObject_4(GameObjectU5BU5D_t3057952154* value)
	{
		___UnitObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___UnitObject_4, value);
	}

	inline static int32_t get_offset_of_UnitNameText_5() { return static_cast<int32_t>(offsetof(UnitInfoPop_t2203256789, ___UnitNameText_5)); }
	inline TextU5BU5D_t4216439300* get_UnitNameText_5() const { return ___UnitNameText_5; }
	inline TextU5BU5D_t4216439300** get_address_of_UnitNameText_5() { return &___UnitNameText_5; }
	inline void set_UnitNameText_5(TextU5BU5D_t4216439300* value)
	{
		___UnitNameText_5 = value;
		Il2CppCodeGenWriteBarrier(&___UnitNameText_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
