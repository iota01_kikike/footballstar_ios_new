﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_DataType848016694.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_VariableType3941595272.h"

// System.String
struct String_t;
// SA.GoogleDoc.Cell
struct Cell_t3987632660;
// SA.GoogleDoc.CellRange
struct CellRange_t4079182527;
// SA.GoogleDoc.CellDictionaryRange
struct CellDictionaryRange_t1484450799;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SA.GoogleDoc.GoogleData
struct GoogleData_t1520579079;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.DataDelegate
struct  DataDelegate_t1852602171  : public Il2CppObject
{
public:
	// System.String SA.GoogleDoc.DataDelegate::FiledName
	String_t* ___FiledName_0;
	// System.Boolean SA.GoogleDoc.DataDelegate::IsOpened
	bool ___IsOpened_1;
	// SA.GoogleDoc.Cell SA.GoogleDoc.DataDelegate::cell
	Cell_t3987632660 * ___cell_2;
	// SA.GoogleDoc.CellRange SA.GoogleDoc.DataDelegate::range
	CellRange_t4079182527 * ___range_3;
	// SA.GoogleDoc.CellDictionaryRange SA.GoogleDoc.DataDelegate::hashMap
	CellDictionaryRange_t1484450799 * ___hashMap_4;
	// SA.GoogleDoc.DataType SA.GoogleDoc.DataDelegate::dataType
	int32_t ___dataType_5;
	// SA.GoogleDoc.VariableType SA.GoogleDoc.DataDelegate::variableType
	int32_t ___variableType_6;
	// System.Boolean SA.GoogleDoc.DataDelegate::bConnectableField
	bool ___bConnectableField_7;
	// System.String SA.GoogleDoc.DataDelegate::SourceDocName
	String_t* ___SourceDocName_8;
	// UnityEngine.GameObject SA.GoogleDoc.DataDelegate::owner
	GameObject_t1756533147 * ___owner_9;
	// SA.GoogleDoc.GoogleData SA.GoogleDoc.DataDelegate::_googleData
	GoogleData_t1520579079 * ____googleData_10;

public:
	inline static int32_t get_offset_of_FiledName_0() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___FiledName_0)); }
	inline String_t* get_FiledName_0() const { return ___FiledName_0; }
	inline String_t** get_address_of_FiledName_0() { return &___FiledName_0; }
	inline void set_FiledName_0(String_t* value)
	{
		___FiledName_0 = value;
		Il2CppCodeGenWriteBarrier(&___FiledName_0, value);
	}

	inline static int32_t get_offset_of_IsOpened_1() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___IsOpened_1)); }
	inline bool get_IsOpened_1() const { return ___IsOpened_1; }
	inline bool* get_address_of_IsOpened_1() { return &___IsOpened_1; }
	inline void set_IsOpened_1(bool value)
	{
		___IsOpened_1 = value;
	}

	inline static int32_t get_offset_of_cell_2() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___cell_2)); }
	inline Cell_t3987632660 * get_cell_2() const { return ___cell_2; }
	inline Cell_t3987632660 ** get_address_of_cell_2() { return &___cell_2; }
	inline void set_cell_2(Cell_t3987632660 * value)
	{
		___cell_2 = value;
		Il2CppCodeGenWriteBarrier(&___cell_2, value);
	}

	inline static int32_t get_offset_of_range_3() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___range_3)); }
	inline CellRange_t4079182527 * get_range_3() const { return ___range_3; }
	inline CellRange_t4079182527 ** get_address_of_range_3() { return &___range_3; }
	inline void set_range_3(CellRange_t4079182527 * value)
	{
		___range_3 = value;
		Il2CppCodeGenWriteBarrier(&___range_3, value);
	}

	inline static int32_t get_offset_of_hashMap_4() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___hashMap_4)); }
	inline CellDictionaryRange_t1484450799 * get_hashMap_4() const { return ___hashMap_4; }
	inline CellDictionaryRange_t1484450799 ** get_address_of_hashMap_4() { return &___hashMap_4; }
	inline void set_hashMap_4(CellDictionaryRange_t1484450799 * value)
	{
		___hashMap_4 = value;
		Il2CppCodeGenWriteBarrier(&___hashMap_4, value);
	}

	inline static int32_t get_offset_of_dataType_5() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___dataType_5)); }
	inline int32_t get_dataType_5() const { return ___dataType_5; }
	inline int32_t* get_address_of_dataType_5() { return &___dataType_5; }
	inline void set_dataType_5(int32_t value)
	{
		___dataType_5 = value;
	}

	inline static int32_t get_offset_of_variableType_6() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___variableType_6)); }
	inline int32_t get_variableType_6() const { return ___variableType_6; }
	inline int32_t* get_address_of_variableType_6() { return &___variableType_6; }
	inline void set_variableType_6(int32_t value)
	{
		___variableType_6 = value;
	}

	inline static int32_t get_offset_of_bConnectableField_7() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___bConnectableField_7)); }
	inline bool get_bConnectableField_7() const { return ___bConnectableField_7; }
	inline bool* get_address_of_bConnectableField_7() { return &___bConnectableField_7; }
	inline void set_bConnectableField_7(bool value)
	{
		___bConnectableField_7 = value;
	}

	inline static int32_t get_offset_of_SourceDocName_8() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___SourceDocName_8)); }
	inline String_t* get_SourceDocName_8() const { return ___SourceDocName_8; }
	inline String_t** get_address_of_SourceDocName_8() { return &___SourceDocName_8; }
	inline void set_SourceDocName_8(String_t* value)
	{
		___SourceDocName_8 = value;
		Il2CppCodeGenWriteBarrier(&___SourceDocName_8, value);
	}

	inline static int32_t get_offset_of_owner_9() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ___owner_9)); }
	inline GameObject_t1756533147 * get_owner_9() const { return ___owner_9; }
	inline GameObject_t1756533147 ** get_address_of_owner_9() { return &___owner_9; }
	inline void set_owner_9(GameObject_t1756533147 * value)
	{
		___owner_9 = value;
		Il2CppCodeGenWriteBarrier(&___owner_9, value);
	}

	inline static int32_t get_offset_of__googleData_10() { return static_cast<int32_t>(offsetof(DataDelegate_t1852602171, ____googleData_10)); }
	inline GoogleData_t1520579079 * get__googleData_10() const { return ____googleData_10; }
	inline GoogleData_t1520579079 ** get_address_of__googleData_10() { return &____googleData_10; }
	inline void set__googleData_10(GoogleData_t1520579079 * value)
	{
		____googleData_10 = value;
		Il2CppCodeGenWriteBarrier(&____googleData_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
