﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.Dictionary`2<System.Int32,Nation>
struct Dictionary_2_t2439496172;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectLineupView
struct  CollectLineupView_t2793734312  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text CollectLineupView::NationText
	Text_t356221433 * ___NationText_2;
	// UnityEngine.UI.Image CollectLineupView::FlagImage
	Image_t2042527209 * ___FlagImage_3;
	// UnityEngine.UI.Button CollectLineupView::PrevBtn
	Button_t2872111280 * ___PrevBtn_4;
	// UnityEngine.GameObject CollectLineupView::Best11PlayersObj
	GameObject_t1756533147 * ___Best11PlayersObj_5;
	// UnityEngine.UI.Text CollectLineupView::PercentText
	Text_t356221433 * ___PercentText_6;
	// UnityEngine.UI.Text CollectLineupView::FormationText
	Text_t356221433 * ___FormationText_7;
	// UnityEngine.GameObject CollectLineupView::SubPlayersObj
	GameObject_t1756533147 * ___SubPlayersObj_8;
	// UnityEngine.Material CollectLineupView::GrayscaleMat
	Material_t193706927 * ___GrayscaleMat_9;
	// System.Int32 CollectLineupView::_currentNationNo
	int32_t ____currentNationNo_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,Nation> CollectLineupView::_nationDict
	Dictionary_2_t2439496172 * ____nationDict_11;
	// UnityEngine.GameObject CollectLineupView::_formObj
	GameObject_t1756533147 * ____formObj_12;
	// System.Int32 CollectLineupView::_bestPlayerCount
	int32_t ____bestPlayerCount_13;
	// System.Int32 CollectLineupView::_subPlayerCount
	int32_t ____subPlayerCount_14;
	// System.Int32 CollectLineupView::_haveBestPlayerCount
	int32_t ____haveBestPlayerCount_15;
	// System.Int32 CollectLineupView::_haveSubPlayerCount
	int32_t ____haveSubPlayerCount_16;

public:
	inline static int32_t get_offset_of_NationText_2() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___NationText_2)); }
	inline Text_t356221433 * get_NationText_2() const { return ___NationText_2; }
	inline Text_t356221433 ** get_address_of_NationText_2() { return &___NationText_2; }
	inline void set_NationText_2(Text_t356221433 * value)
	{
		___NationText_2 = value;
		Il2CppCodeGenWriteBarrier(&___NationText_2, value);
	}

	inline static int32_t get_offset_of_FlagImage_3() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___FlagImage_3)); }
	inline Image_t2042527209 * get_FlagImage_3() const { return ___FlagImage_3; }
	inline Image_t2042527209 ** get_address_of_FlagImage_3() { return &___FlagImage_3; }
	inline void set_FlagImage_3(Image_t2042527209 * value)
	{
		___FlagImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___FlagImage_3, value);
	}

	inline static int32_t get_offset_of_PrevBtn_4() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___PrevBtn_4)); }
	inline Button_t2872111280 * get_PrevBtn_4() const { return ___PrevBtn_4; }
	inline Button_t2872111280 ** get_address_of_PrevBtn_4() { return &___PrevBtn_4; }
	inline void set_PrevBtn_4(Button_t2872111280 * value)
	{
		___PrevBtn_4 = value;
		Il2CppCodeGenWriteBarrier(&___PrevBtn_4, value);
	}

	inline static int32_t get_offset_of_Best11PlayersObj_5() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___Best11PlayersObj_5)); }
	inline GameObject_t1756533147 * get_Best11PlayersObj_5() const { return ___Best11PlayersObj_5; }
	inline GameObject_t1756533147 ** get_address_of_Best11PlayersObj_5() { return &___Best11PlayersObj_5; }
	inline void set_Best11PlayersObj_5(GameObject_t1756533147 * value)
	{
		___Best11PlayersObj_5 = value;
		Il2CppCodeGenWriteBarrier(&___Best11PlayersObj_5, value);
	}

	inline static int32_t get_offset_of_PercentText_6() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___PercentText_6)); }
	inline Text_t356221433 * get_PercentText_6() const { return ___PercentText_6; }
	inline Text_t356221433 ** get_address_of_PercentText_6() { return &___PercentText_6; }
	inline void set_PercentText_6(Text_t356221433 * value)
	{
		___PercentText_6 = value;
		Il2CppCodeGenWriteBarrier(&___PercentText_6, value);
	}

	inline static int32_t get_offset_of_FormationText_7() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___FormationText_7)); }
	inline Text_t356221433 * get_FormationText_7() const { return ___FormationText_7; }
	inline Text_t356221433 ** get_address_of_FormationText_7() { return &___FormationText_7; }
	inline void set_FormationText_7(Text_t356221433 * value)
	{
		___FormationText_7 = value;
		Il2CppCodeGenWriteBarrier(&___FormationText_7, value);
	}

	inline static int32_t get_offset_of_SubPlayersObj_8() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___SubPlayersObj_8)); }
	inline GameObject_t1756533147 * get_SubPlayersObj_8() const { return ___SubPlayersObj_8; }
	inline GameObject_t1756533147 ** get_address_of_SubPlayersObj_8() { return &___SubPlayersObj_8; }
	inline void set_SubPlayersObj_8(GameObject_t1756533147 * value)
	{
		___SubPlayersObj_8 = value;
		Il2CppCodeGenWriteBarrier(&___SubPlayersObj_8, value);
	}

	inline static int32_t get_offset_of_GrayscaleMat_9() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ___GrayscaleMat_9)); }
	inline Material_t193706927 * get_GrayscaleMat_9() const { return ___GrayscaleMat_9; }
	inline Material_t193706927 ** get_address_of_GrayscaleMat_9() { return &___GrayscaleMat_9; }
	inline void set_GrayscaleMat_9(Material_t193706927 * value)
	{
		___GrayscaleMat_9 = value;
		Il2CppCodeGenWriteBarrier(&___GrayscaleMat_9, value);
	}

	inline static int32_t get_offset_of__currentNationNo_10() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____currentNationNo_10)); }
	inline int32_t get__currentNationNo_10() const { return ____currentNationNo_10; }
	inline int32_t* get_address_of__currentNationNo_10() { return &____currentNationNo_10; }
	inline void set__currentNationNo_10(int32_t value)
	{
		____currentNationNo_10 = value;
	}

	inline static int32_t get_offset_of__nationDict_11() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____nationDict_11)); }
	inline Dictionary_2_t2439496172 * get__nationDict_11() const { return ____nationDict_11; }
	inline Dictionary_2_t2439496172 ** get_address_of__nationDict_11() { return &____nationDict_11; }
	inline void set__nationDict_11(Dictionary_2_t2439496172 * value)
	{
		____nationDict_11 = value;
		Il2CppCodeGenWriteBarrier(&____nationDict_11, value);
	}

	inline static int32_t get_offset_of__formObj_12() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____formObj_12)); }
	inline GameObject_t1756533147 * get__formObj_12() const { return ____formObj_12; }
	inline GameObject_t1756533147 ** get_address_of__formObj_12() { return &____formObj_12; }
	inline void set__formObj_12(GameObject_t1756533147 * value)
	{
		____formObj_12 = value;
		Il2CppCodeGenWriteBarrier(&____formObj_12, value);
	}

	inline static int32_t get_offset_of__bestPlayerCount_13() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____bestPlayerCount_13)); }
	inline int32_t get__bestPlayerCount_13() const { return ____bestPlayerCount_13; }
	inline int32_t* get_address_of__bestPlayerCount_13() { return &____bestPlayerCount_13; }
	inline void set__bestPlayerCount_13(int32_t value)
	{
		____bestPlayerCount_13 = value;
	}

	inline static int32_t get_offset_of__subPlayerCount_14() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____subPlayerCount_14)); }
	inline int32_t get__subPlayerCount_14() const { return ____subPlayerCount_14; }
	inline int32_t* get_address_of__subPlayerCount_14() { return &____subPlayerCount_14; }
	inline void set__subPlayerCount_14(int32_t value)
	{
		____subPlayerCount_14 = value;
	}

	inline static int32_t get_offset_of__haveBestPlayerCount_15() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____haveBestPlayerCount_15)); }
	inline int32_t get__haveBestPlayerCount_15() const { return ____haveBestPlayerCount_15; }
	inline int32_t* get_address_of__haveBestPlayerCount_15() { return &____haveBestPlayerCount_15; }
	inline void set__haveBestPlayerCount_15(int32_t value)
	{
		____haveBestPlayerCount_15 = value;
	}

	inline static int32_t get_offset_of__haveSubPlayerCount_16() { return static_cast<int32_t>(offsetof(CollectLineupView_t2793734312, ____haveSubPlayerCount_16)); }
	inline int32_t get__haveSubPlayerCount_16() const { return ____haveSubPlayerCount_16; }
	inline int32_t* get_address_of__haveSubPlayerCount_16() { return &____haveSubPlayerCount_16; }
	inline void set__haveSubPlayerCount_16(int32_t value)
	{
		____haveSubPlayerCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
