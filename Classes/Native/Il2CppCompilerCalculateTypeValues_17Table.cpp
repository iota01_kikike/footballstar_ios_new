﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Purchasing_Common_U3CModuleU3E3783534214.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJson838727235.h"
#include "Purchasing_Common_UnityEngine_Purchasing_UnityPurc2635187846.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_J581258312.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_2551496676.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_3093089510.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_J645677759.h"
#include "Purchasing_Common_UnityEngine_Purchasing_MiniJSON_Mi53552535.h"
#include "UnityStore_U3CModuleU3E3783534214.h"
#include "UnityStore_UnityEngine_Store_MainThreadDispatcher513574034.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435.h"
#include "UnityStore_UnityEngine_Store_StoreService3339196318.h"
#include "UnityStore_UnityEngine_Store_LoginForwardCallback1105422505.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747.h"
#include "Apple_U3CModuleU3E3783534214.h"
#include "Apple_UnityEngine_Purchasing_iOSStoreBindings2633471826.h"
#include "Apple_UnityEngine_Purchasing_OSXStoreBindings116576999.h"
#include "ChannelPurchase_U3CModuleU3E3783534214.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas659182236.h"
#include "ChannelPurchase_UnityEngine_ChannelPurchase_Purchas144617755.h"
#include "DOTween_U3CModuleU3E3783534214.h"
#include "DOTween_DG_Tweening_AutoPlay2503223703.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "DOTween_DG_Tweening_EaseFunction3306356708.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_DOTween_U3CU3Ec__DisplayClass53467700553.h"
#include "DOTween_DG_Tweening_DOVirtual1722510550.h"
#include "DOTween_DG_Tweening_DOVirtual_U3CU3Ec__DisplayClas3724530167.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_EaseFactory4203735666.h"
#include "DOTween_DG_Tweening_EaseFactory_U3CU3Ec__DisplayCl2066423765.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "DOTween_DG_Tweening_ShortcutExtensions3524050470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842963.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842866.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843029.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842932.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842831.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842734.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842897.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842800.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843227.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843130.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978348.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691431.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653346.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366429.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628352.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341435.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303350.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678340.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391423.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978441.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691524.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653439.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366522.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628445.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341528.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303443.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016526.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391516.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978538.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691621.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653536.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366619.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628542.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341625.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303540.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016623.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678530.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391613.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978383.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691466.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653381.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366464.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628387.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303385.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016468.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678375.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391458.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978224.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (MiniJson_t838727235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (UnityPurchasingCallback_t2635187846), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (Json_t581258312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (Parser_t2551496676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[1] = 
{
	Parser_t2551496676::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (TOKEN_t3093089510)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1708[13] = 
{
	TOKEN_t3093089510::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (Serializer_t645677759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[1] = 
{
	Serializer_t645677759::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (MiniJsonExtensions_t53552535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (MainThreadDispatcher_t513574034), -1, sizeof(MainThreadDispatcher_t513574034_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[3] = 
{
	MainThreadDispatcher_t513574034_StaticFields::get_offset_of_OBJECT_NAME_2(),
	MainThreadDispatcher_t513574034_StaticFields::get_offset_of_s_Callbacks_3(),
	MainThreadDispatcher_t513574034_StaticFields::get_offset_of_s_CallbacksPending_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (AppInfo_t2080248435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[5] = 
{
	AppInfo_t2080248435::get_offset_of_U3CappIdU3Ek__BackingField_0(),
	AppInfo_t2080248435::get_offset_of_U3CappKeyU3Ek__BackingField_1(),
	AppInfo_t2080248435::get_offset_of_U3CclientIdU3Ek__BackingField_2(),
	AppInfo_t2080248435::get_offset_of_U3CclientKeyU3Ek__BackingField_3(),
	AppInfo_t2080248435::get_offset_of_U3CdebugU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (StoreService_t3339196318), -1, sizeof(StoreService_t3339196318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1714[1] = 
{
	StoreService_t3339196318_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (LoginForwardCallback_t1105422505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[1] = 
{
	LoginForwardCallback_t1105422505::get_offset_of_loginListener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (UserInfo_t741955747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[3] = 
{
	UserInfo_t741955747::get_offset_of_U3CchannelU3Ek__BackingField_0(),
	UserInfo_t741955747::get_offset_of_U3CuserIdU3Ek__BackingField_1(),
	UserInfo_t741955747::get_offset_of_U3CuserLoginTokenU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (iOSStoreBindings_t2633471826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (OSXStoreBindings_t116576999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (PurchaseService_t659182236), -1, sizeof(PurchaseService_t659182236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	PurchaseService_t659182236_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (PurchaseForwardCallback_t144617755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[1] = 
{
	PurchaseForwardCallback_t144617755::get_offset_of_purchaseListener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (AutoPlay_t2503223703)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[5] = 
{
	AutoPlay_t2503223703::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (AxisConstraint_t1244566668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1728[6] = 
{
	AxisConstraint_t1244566668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (Color2_t232726623)+ sizeof (Il2CppObject), sizeof(Color2_t232726623 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[2] = 
{
	Color2_t232726623::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color2_t232726623::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (TweenCallback_t3697142134), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (EaseFunction_t3306356708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (DOTween_t2276353038), -1, sizeof(DOTween_t2276353038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1733[25] = 
{
	DOTween_t2276353038_StaticFields::get_offset_of_Version_0(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t2276353038_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t2276353038_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxSmoothUnscaledTime_5(),
	DOTween_t2276353038_StaticFields::get_offset_of__logBehaviour_6(),
	DOTween_t2276353038_StaticFields::get_offset_of_drawGizmos_7(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultUpdateType_8(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultTimeScaleIndependent_9(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoPlay_10(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoKill_11(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultLoopType_12(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultRecyclable_13(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseType_14(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_15(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEasePeriod_16(),
	DOTween_t2276353038_StaticFields::get_offset_of_instance_17(),
	DOTween_t2276353038_StaticFields::get_offset_of_isUnityEditor_18(),
	DOTween_t2276353038_StaticFields::get_offset_of_isDebugBuild_19(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveTweenersReached_20(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveSequencesReached_21(),
	DOTween_t2276353038_StaticFields::get_offset_of_GizmosDelegates_22(),
	DOTween_t2276353038_StaticFields::get_offset_of_initialized_23(),
	DOTween_t2276353038_StaticFields::get_offset_of_isQuitting_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (U3CU3Ec__DisplayClass53_0_t3467700553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[2] = 
{
	U3CU3Ec__DisplayClass53_0_t3467700553::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass53_0_t3467700553::get_offset_of_setter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (DOVirtual_t1722510550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (U3CU3Ec__DisplayClass0_0_t3724530167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_val_0(),
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_onVirtualUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (Ease_t2502520296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1737[39] = 
{
	Ease_t2502520296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (EaseFactory_t4203735666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (U3CU3Ec__DisplayClass2_0_t2066423765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[2] = 
{
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_motionDelay_0(),
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_customEase_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (PathMode_t1545785466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[5] = 
{
	PathMode_t1545785466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (PathType_t2815988833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[3] = 
{
	PathType_t2815988833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (RotateMode_t1177727514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[5] = 
{
	RotateMode_t1177727514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (ScrambleMode_t385206138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1744[7] = 
{
	ScrambleMode_t385206138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (TweenExtensions_t405253783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (LoopType_t2249218064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[4] = 
{
	LoopType_t2249218064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (Sequence_t110643099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[3] = 
{
	Sequence_t110643099::get_offset_of_sequencedTweens_51(),
	Sequence_t110643099::get_offset_of__sequencedObjs_52(),
	Sequence_t110643099::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (ShortcutExtensions_t3524050470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U3CU3Ec__DisplayClass0_0_t964842963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[1] = 
{
	U3CU3Ec__DisplayClass0_0_t964842963::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (U3CU3Ec__DisplayClass1_0_t964842866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[1] = 
{
	U3CU3Ec__DisplayClass1_0_t964842866::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (U3CU3Ec__DisplayClass2_0_t964843029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[1] = 
{
	U3CU3Ec__DisplayClass2_0_t964843029::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (U3CU3Ec__DisplayClass3_0_t964842932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[1] = 
{
	U3CU3Ec__DisplayClass3_0_t964842932::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (U3CU3Ec__DisplayClass4_0_t964842831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[1] = 
{
	U3CU3Ec__DisplayClass4_0_t964842831::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (U3CU3Ec__DisplayClass5_0_t964842734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[1] = 
{
	U3CU3Ec__DisplayClass5_0_t964842734::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (U3CU3Ec__DisplayClass6_0_t964842897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[1] = 
{
	U3CU3Ec__DisplayClass6_0_t964842897::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (U3CU3Ec__DisplayClass7_0_t964842800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[1] = 
{
	U3CU3Ec__DisplayClass7_0_t964842800::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (U3CU3Ec__DisplayClass8_0_t964843227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[1] = 
{
	U3CU3Ec__DisplayClass8_0_t964843227::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (U3CU3Ec__DisplayClass9_0_t964843130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[1] = 
{
	U3CU3Ec__DisplayClass9_0_t964843130::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (U3CU3Ec__DisplayClass10_0_t3010978348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[1] = 
{
	U3CU3Ec__DisplayClass10_0_t3010978348::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (U3CU3Ec__DisplayClass11_0_t1424691431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	U3CU3Ec__DisplayClass11_0_t1424691431::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (U3CU3Ec__DisplayClass12_0_t2728653346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2728653346::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (U3CU3Ec__DisplayClass13_0_t1142366429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[1] = 
{
	U3CU3Ec__DisplayClass13_0_t1142366429::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (U3CU3Ec__DisplayClass14_0_t3575628352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[1] = 
{
	U3CU3Ec__DisplayClass14_0_t3575628352::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (U3CU3Ec__DisplayClass15_0_t1989341435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[1] = 
{
	U3CU3Ec__DisplayClass15_0_t1989341435::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (U3CU3Ec__DisplayClass16_0_t3293303350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3293303350::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (U3CU3Ec__DisplayClass17_0_t1707016433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[2] = 
{
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_startValue_0(),
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (U3CU3Ec__DisplayClass18_0_t1881678340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[1] = 
{
	U3CU3Ec__DisplayClass18_0_t1881678340::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (U3CU3Ec__DisplayClass19_0_t295391423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[2] = 
{
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (U3CU3Ec__DisplayClass20_0_t3010978441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[1] = 
{
	U3CU3Ec__DisplayClass20_0_t3010978441::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (U3CU3Ec__DisplayClass21_0_t1424691524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[2] = 
{
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (U3CU3Ec__DisplayClass22_0_t2728653439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[2] = 
{
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (U3CU3Ec__DisplayClass23_0_t1142366522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[1] = 
{
	U3CU3Ec__DisplayClass23_0_t1142366522::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (U3CU3Ec__DisplayClass24_0_t3575628445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1773[2] = 
{
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (U3CU3Ec__DisplayClass25_0_t1989341528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[1] = 
{
	U3CU3Ec__DisplayClass25_0_t1989341528::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (U3CU3Ec__DisplayClass26_0_t3293303443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[2] = 
{
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (U3CU3Ec__DisplayClass27_0_t1707016526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[2] = 
{
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (U3CU3Ec__DisplayClass28_0_t1881678433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1881678433::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (U3CU3Ec__DisplayClass29_0_t295391516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1778[1] = 
{
	U3CU3Ec__DisplayClass29_0_t295391516::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (U3CU3Ec__DisplayClass30_0_t3010978538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[1] = 
{
	U3CU3Ec__DisplayClass30_0_t3010978538::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (U3CU3Ec__DisplayClass31_0_t1424691621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	U3CU3Ec__DisplayClass31_0_t1424691621::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (U3CU3Ec__DisplayClass32_0_t2728653536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[1] = 
{
	U3CU3Ec__DisplayClass32_0_t2728653536::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (U3CU3Ec__DisplayClass33_0_t1142366619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[1] = 
{
	U3CU3Ec__DisplayClass33_0_t1142366619::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (U3CU3Ec__DisplayClass34_0_t3575628542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[6] = 
{
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (U3CU3Ec__DisplayClass35_0_t1989341625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[1] = 
{
	U3CU3Ec__DisplayClass35_0_t1989341625::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (U3CU3Ec__DisplayClass36_0_t3293303540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	U3CU3Ec__DisplayClass36_0_t3293303540::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass36_0_t3293303540::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (U3CU3Ec__DisplayClass37_0_t1707016623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[1] = 
{
	U3CU3Ec__DisplayClass37_0_t1707016623::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (U3CU3Ec__DisplayClass38_0_t1881678530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[2] = 
{
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (U3CU3Ec__DisplayClass39_0_t295391613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[1] = 
{
	U3CU3Ec__DisplayClass39_0_t295391613::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (U3CU3Ec__DisplayClass40_0_t3010978383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[1] = 
{
	U3CU3Ec__DisplayClass40_0_t3010978383::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (U3CU3Ec__DisplayClass41_0_t1424691466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[1] = 
{
	U3CU3Ec__DisplayClass41_0_t1424691466::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (U3CU3Ec__DisplayClass42_0_t2728653381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2728653381::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (U3CU3Ec__DisplayClass43_0_t1142366464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[1] = 
{
	U3CU3Ec__DisplayClass43_0_t1142366464::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (U3CU3Ec__DisplayClass44_0_t3575628387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[1] = 
{
	U3CU3Ec__DisplayClass44_0_t3575628387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (U3CU3Ec__DisplayClass45_0_t1989341470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[1] = 
{
	U3CU3Ec__DisplayClass45_0_t1989341470::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (U3CU3Ec__DisplayClass46_0_t3293303385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[1] = 
{
	U3CU3Ec__DisplayClass46_0_t3293303385::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (U3CU3Ec__DisplayClass47_0_t1707016468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	U3CU3Ec__DisplayClass47_0_t1707016468::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (U3CU3Ec__DisplayClass48_0_t1881678375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	U3CU3Ec__DisplayClass48_0_t1881678375::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (U3CU3Ec__DisplayClass49_0_t295391458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[1] = 
{
	U3CU3Ec__DisplayClass49_0_t295391458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (U3CU3Ec__DisplayClass50_0_t3010978224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[1] = 
{
	U3CU3Ec__DisplayClass50_0_t3010978224::get_offset_of_target_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
