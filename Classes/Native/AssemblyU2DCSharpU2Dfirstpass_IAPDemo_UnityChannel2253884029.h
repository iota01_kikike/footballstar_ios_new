﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action
struct Action_t3226471752;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<UnityEngine.Store.UserInfo>
struct Action_1_t543755129;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelLoginHandler
struct  UnityChannelLoginHandler_t2253884029  : public Il2CppObject
{
public:
	// System.Action IAPDemo/UnityChannelLoginHandler::initializeSucceededAction
	Action_t3226471752 * ___initializeSucceededAction_0;
	// System.Action`1<System.String> IAPDemo/UnityChannelLoginHandler::initializeFailedAction
	Action_1_t1831019615 * ___initializeFailedAction_1;
	// System.Action`1<UnityEngine.Store.UserInfo> IAPDemo/UnityChannelLoginHandler::loginSucceededAction
	Action_1_t543755129 * ___loginSucceededAction_2;
	// System.Action`1<System.String> IAPDemo/UnityChannelLoginHandler::loginFailedAction
	Action_1_t1831019615 * ___loginFailedAction_3;

public:
	inline static int32_t get_offset_of_initializeSucceededAction_0() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2253884029, ___initializeSucceededAction_0)); }
	inline Action_t3226471752 * get_initializeSucceededAction_0() const { return ___initializeSucceededAction_0; }
	inline Action_t3226471752 ** get_address_of_initializeSucceededAction_0() { return &___initializeSucceededAction_0; }
	inline void set_initializeSucceededAction_0(Action_t3226471752 * value)
	{
		___initializeSucceededAction_0 = value;
		Il2CppCodeGenWriteBarrier(&___initializeSucceededAction_0, value);
	}

	inline static int32_t get_offset_of_initializeFailedAction_1() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2253884029, ___initializeFailedAction_1)); }
	inline Action_1_t1831019615 * get_initializeFailedAction_1() const { return ___initializeFailedAction_1; }
	inline Action_1_t1831019615 ** get_address_of_initializeFailedAction_1() { return &___initializeFailedAction_1; }
	inline void set_initializeFailedAction_1(Action_1_t1831019615 * value)
	{
		___initializeFailedAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___initializeFailedAction_1, value);
	}

	inline static int32_t get_offset_of_loginSucceededAction_2() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2253884029, ___loginSucceededAction_2)); }
	inline Action_1_t543755129 * get_loginSucceededAction_2() const { return ___loginSucceededAction_2; }
	inline Action_1_t543755129 ** get_address_of_loginSucceededAction_2() { return &___loginSucceededAction_2; }
	inline void set_loginSucceededAction_2(Action_1_t543755129 * value)
	{
		___loginSucceededAction_2 = value;
		Il2CppCodeGenWriteBarrier(&___loginSucceededAction_2, value);
	}

	inline static int32_t get_offset_of_loginFailedAction_3() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t2253884029, ___loginFailedAction_3)); }
	inline Action_1_t1831019615 * get_loginFailedAction_3() const { return ___loginFailedAction_3; }
	inline Action_1_t1831019615 ** get_address_of_loginFailedAction_3() { return &___loginFailedAction_3; }
	inline void set_loginFailedAction_3(Action_1_t1831019615 * value)
	{
		___loginFailedAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___loginFailedAction_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
