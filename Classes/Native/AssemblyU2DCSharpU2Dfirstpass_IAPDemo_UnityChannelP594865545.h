﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// IAPDemo/UnityChannelPurchaseInfo
struct UnityChannelPurchaseInfo_t855507751;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/UnityChannelPurchaseError
struct  UnityChannelPurchaseError_t594865545  : public Il2CppObject
{
public:
	// System.String IAPDemo/UnityChannelPurchaseError::error
	String_t* ___error_0;
	// IAPDemo/UnityChannelPurchaseInfo IAPDemo/UnityChannelPurchaseError::purchaseInfo
	UnityChannelPurchaseInfo_t855507751 * ___purchaseInfo_1;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_t594865545, ___error_0)); }
	inline String_t* get_error_0() const { return ___error_0; }
	inline String_t** get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(String_t* value)
	{
		___error_0 = value;
		Il2CppCodeGenWriteBarrier(&___error_0, value);
	}

	inline static int32_t get_offset_of_purchaseInfo_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_t594865545, ___purchaseInfo_1)); }
	inline UnityChannelPurchaseInfo_t855507751 * get_purchaseInfo_1() const { return ___purchaseInfo_1; }
	inline UnityChannelPurchaseInfo_t855507751 ** get_address_of_purchaseInfo_1() { return &___purchaseInfo_1; }
	inline void set_purchaseInfo_1(UnityChannelPurchaseInfo_t855507751 * value)
	{
		___purchaseInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseInfo_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
