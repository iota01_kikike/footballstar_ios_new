﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOverPop
struct  GameOverPop_t2955216129  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button GameOverPop::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// UnityEngine.UI.Button GameOverPop::Button1
	Button_t2872111280 * ___Button1_3;
	// UnityEngine.UI.Button GameOverPop::Button2
	Button_t2872111280 * ___Button2_4;
	// System.Action`1<System.Int32> GameOverPop::Callback
	Action_1_t1873676830 * ___Callback_5;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(GameOverPop_t2955216129, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_Button1_3() { return static_cast<int32_t>(offsetof(GameOverPop_t2955216129, ___Button1_3)); }
	inline Button_t2872111280 * get_Button1_3() const { return ___Button1_3; }
	inline Button_t2872111280 ** get_address_of_Button1_3() { return &___Button1_3; }
	inline void set_Button1_3(Button_t2872111280 * value)
	{
		___Button1_3 = value;
		Il2CppCodeGenWriteBarrier(&___Button1_3, value);
	}

	inline static int32_t get_offset_of_Button2_4() { return static_cast<int32_t>(offsetof(GameOverPop_t2955216129, ___Button2_4)); }
	inline Button_t2872111280 * get_Button2_4() const { return ___Button2_4; }
	inline Button_t2872111280 ** get_address_of_Button2_4() { return &___Button2_4; }
	inline void set_Button2_4(Button_t2872111280 * value)
	{
		___Button2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Button2_4, value);
	}

	inline static int32_t get_offset_of_Callback_5() { return static_cast<int32_t>(offsetof(GameOverPop_t2955216129, ___Callback_5)); }
	inline Action_1_t1873676830 * get_Callback_5() const { return ___Callback_5; }
	inline Action_1_t1873676830 ** get_address_of_Callback_5() { return &___Callback_5; }
	inline void set_Callback_5(Action_1_t1873676830 * value)
	{
		___Callback_5 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
