﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TrainingView
struct TrainingView_t2704080403;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/<GainGem>c__AnonStorey6
struct  U3CGainGemU3Ec__AnonStorey6_t2228177261  : public Il2CppObject
{
public:
	// UnityEngine.GameObject TrainingView/<GainGem>c__AnonStorey6::obj
	GameObject_t1756533147 * ___obj_0;
	// System.Int32 TrainingView/<GainGem>c__AnonStorey6::gemCnt
	int32_t ___gemCnt_1;
	// TrainingView TrainingView/<GainGem>c__AnonStorey6::$this
	TrainingView_t2704080403 * ___U24this_2;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CGainGemU3Ec__AnonStorey6_t2228177261, ___obj_0)); }
	inline GameObject_t1756533147 * get_obj_0() const { return ___obj_0; }
	inline GameObject_t1756533147 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_t1756533147 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}

	inline static int32_t get_offset_of_gemCnt_1() { return static_cast<int32_t>(offsetof(U3CGainGemU3Ec__AnonStorey6_t2228177261, ___gemCnt_1)); }
	inline int32_t get_gemCnt_1() const { return ___gemCnt_1; }
	inline int32_t* get_address_of_gemCnt_1() { return &___gemCnt_1; }
	inline void set_gemCnt_1(int32_t value)
	{
		___gemCnt_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGainGemU3Ec__AnonStorey6_t2228177261, ___U24this_2)); }
	inline TrainingView_t2704080403 * get_U24this_2() const { return ___U24this_2; }
	inline TrainingView_t2704080403 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TrainingView_t2704080403 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
