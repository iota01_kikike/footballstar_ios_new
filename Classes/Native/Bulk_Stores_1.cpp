﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Stores_UnityEngine_Purchasing_TradeSeqState1177537792.h"
#include "Stores_UnityEngine_Purchasing_TranslationLocale3543162129.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124.h"
#include "mscorlib_System_Void1841601450.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "System_Core_System_Action_2_gen1907880187.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "System_Core_System_Action3226471752.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1789388632.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1942475268.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2128260960.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_U3CU3Ec3471345421.h"
#include "System_Core_System_Func_2_gen2958801606.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "Stores_UnityEngine_Purchasing_UnifiedReceipt2654419430.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelBindings2880355556.h"
#include "System_Core_System_Action_2_gen1865222972.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Decimal724701077.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1573242544.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte3318267523.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1651728377.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995.h"
#include "Stores_UnityEngine_Purchasing_LocalizedProductDesc1525635964.h"
#include "Stores_UnityEngine_Purchasing_XiaomiPriceTiers2494450221.h"
#include "System_Core_System_Action_3_gen3864773326.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelBindings1107960729.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge853706424.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3233894458.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl1327714682.h"
#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl_U3CU3Ec505099.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelImpl_U3C2729388454.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte1607114611.h"
#include "Stores_UnityEngine_Purchasing_UnityChannelPurchase1964142665.h"
#include "Stores_UnityEngine_Purchasing_ValidateReceiptState4359597.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Exte2787096497.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore_U3CU3Ec4236842726.h"
#include "System_Core_System_Func_2_gen460188795.h"
#include "System_Core_System_Func_2_gen2004692778.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405.h"
#include "mscorlib_System_Collections_Generic_List_1_gen444232537.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod2754455291.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "Stores_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Stores_U3CPrivateImplementationDetailsU3E___Static1542897494.h"

// UnityEngine.Purchasing.UIFakeStore
struct UIFakeStore_t3684252124;
// UnityEngine.Purchasing.FakeStore
struct FakeStore_t3882981564;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t1907880187;
// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct DialogRequest_t2092195449;
// System.Object
struct Il2CppObject;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier
struct LifecycleNotifier_t1057582876;
// System.Action
struct Action_t3226471752;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityEngine.EventSystems.StandaloneInputModule
struct StandaloneInputModule_t70867863;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Dropdown
struct Dropdown_t1985816271;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t1789388632;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t2420267500;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t2455055323;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t2203087800;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3438463199;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2110227463;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t828812576;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t1942475268;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>
struct ReadOnlyCollection_1_t2128260960;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductDefinition>
struct IEnumerable_1_t2234602313;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String>
struct Func_2_t2958801606;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2875234987;
// UnityEngine.Purchasing.UIFakeStore/<>c
struct U3CU3Ec_t3471345421;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Purchasing.UnifiedReceipt
struct UnifiedReceipt_t2654419430;
// UnityEngine.Purchasing.UnityChannelBindings
struct UnityChannelBindings_t2880355556;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// UnityEngine.ChannelPurchase.IPurchaseListener
struct IPurchaseListener_t2367674166;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>
struct HashSet_1_t1651728377;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1022910149;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t2667590766;
// System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.ProductCatalogItem>
struct ICollection_1_t1929787300;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductDefinition>
struct IEnumerator_1_t3712966391;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// UnityEngine.Purchasing.ProductCatalogItem
struct ProductCatalogItem_t977711995;
// UnityEngine.Purchasing.LocalizedProductDescription
struct LocalizedProductDescription_t1525635964;
// UnityEngine.Purchasing.ProductMetadata
struct ProductMetadata_t1573242544;
// UnityEngine.Purchasing.Extension.ProductDescription
struct ProductDescription_t3318267523;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Extension.ProductDescription>
struct IEnumerable_1_t3610394568;
// System.Action`3<System.Boolean,System.String,System.String>
struct Action_3_t3864773326;
// UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t1107960729;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>
struct Dictionary_2_t853706424;
// System.Action`3<System.Boolean,System.Object,System.Object>
struct Action_3_t2202283254;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>
struct List_1_t3233894458;
// System.NotImplementedException
struct NotImplementedException_t2785117854;
// UnityEngine.Purchasing.UnityChannelImpl
struct UnityChannelImpl_t1327714682;
// UnityEngine.Purchasing.JSONStore
struct JSONStore_t1890359403;
// UnityEngine.Purchasing.INativeUnityChannelStore
struct INativeUnityChannelStore_t670691399;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t3203646079;
// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t505099;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1
struct U3CU3Ec__DisplayClass7_1_t2729388454;
// UnityEngine.Purchasing.Extension.PurchaseFailureDescription
struct PurchaseFailureDescription_t1607114611;
// UnityEngine.Purchasing.UnityChannelPurchaseReceipt
struct UnityChannelPurchaseReceipt_t1964142665;
// UnityEngine.Purchasing.WinRTStore
struct WinRTStore_t36043095;
// UnityEngine.Purchasing.Default.IWindowsIAP
struct IWindowsIAP_t818184396;
// Uniject.IUtil
struct IUtil_t2188430191;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// UnityEngine.Purchasing.Extension.AbstractStore
struct AbstractStore_t2787096497;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean>
struct Func_2_t460188795;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>
struct Func_2_t2004692778;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Default.WinProductDescription>
struct IEnumerable_1_t1367238450;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.Default.WinProductDescription>
struct List_1_t444232537;
// UnityEngine.Purchasing.WinRTStore/<>c
struct U3CU3Ec_t4236842726;
// UnityEngine.Purchasing.Default.WinProductDescription
struct WinProductDescription_t1075111405;
// System.Array
struct Il2CppArray;
extern Il2CppClass* DialogRequest_t2092195449_il2cpp_TypeInfo_var;
extern const uint32_t UIFakeStore_StartUI_m2569939907_MetadataUsageId;
extern const Il2CppType* EventSystem_t3466835263_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* OptionData_t2420267500_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3438463199_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisCanvas_t209405766_m805779209_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Eb__16_0_m16729514_MethodInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1204779269_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1412753878_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2837468952_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3486116920_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1105633690_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Eb__16_1_m16729609_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Eb__16_2_m16729448_MethodInfo_var;
extern const MethodInfo* UIFakeStore_U3CInstantiateDialogU3Eb__16_3_m4148963786_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m4197675336_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m404084168_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4204482283;
extern Il2CppCodeGenString* _stringLiteral1832009962;
extern Il2CppCodeGenString* _stringLiteral3655311540;
extern Il2CppCodeGenString* _stringLiteral3628703177;
extern Il2CppCodeGenString* _stringLiteral4267440200;
extern const uint32_t UIFakeStore_InstantiateDialog_m26685090_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3368079914;
extern Il2CppCodeGenString* _stringLiteral3820628614;
extern const uint32_t UIFakeStore_CreatePurchaseQuestion_m2287137335_MetadataUsageId;
extern Il2CppClass* U3CU3Ec_t3471345421_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2958801606_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Take_TisProductDefinition_t1942475268_m2540339846_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_m980068044_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m585608501_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Count_m2134061681_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1923700425;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral3422253728;
extern Il2CppCodeGenString* _stringLiteral3741546081;
extern const uint32_t UIFakeStore_CreateRetrieveProductsQuestion_m4106290161_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3889149951;
extern const uint32_t UIFakeStore_GetOkayButton_m2121832312_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3889149952;
extern const uint32_t UIFakeStore_GetCancelButton_m1851079754_MetadataUsageId;
extern const uint32_t UIFakeStore_GetCancelButtonGameObject_m4002152618_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1636160581;
extern const uint32_t UIFakeStore_GetOkayButtonText_m6562956_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1636166022;
extern const uint32_t UIFakeStore_GetCancelButtonText_m1587731078_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4076336408;
extern const uint32_t UIFakeStore_GetDropdown_m651796348_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral516648086;
extern const uint32_t UIFakeStore_GetDropdownContainerGameObject_m2190404260_MetadataUsageId;
extern const MethodInfo* Action_2_Invoke_m2365036873_MethodInfo_var;
extern const uint32_t UIFakeStore_OkayButtonClicked_m1579250743_MetadataUsageId;
extern const uint32_t UIFakeStore_CancelButtonClicked_m1822980185_MetadataUsageId;
extern const uint32_t UIFakeStore_CloseDialog_m4132523490_MetadataUsageId;
extern const uint32_t U3CU3Ec__cctor_m70001523_MetadataUsageId;
extern Il2CppClass* Guid_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseService_t659182236_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m547817495_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral367725214;
extern const uint32_t UnityChannelBindings_Purchase_m1136380361_MetadataUsageId;
extern Il2CppClass* HashSet_1_t1651728377_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1269839040_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2748203118_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3712966391_il2cpp_TypeInfo_var;
extern Il2CppClass* XiaomiPriceTiers_t2494450221_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductMetadata_t1573242544_il2cpp_TypeInfo_var;
extern Il2CppClass* ProductDescription_t3318267523_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m738744506_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_GetEnumerator_m178725564_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m1190086556_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2309149295;
extern Il2CppCodeGenString* _stringLiteral1643762102;
extern const uint32_t UnityChannelBindings_RetrieveProducts_m1095305944_MetadataUsageId;
extern Il2CppClass* U3CU3Ec__DisplayClass16_0_t1107960729_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass16_0_U3CValidateReceiptU3Eb__0_m573116822_MethodInfo_var;
extern const uint32_t UnityChannelBindings_ValidateReceipt_m3411439973_MetadataUsageId;
extern Il2CppClass* List_1_t3233894458_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_3_Invoke_m304290708_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m661064896_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1828621836_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2269440416_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1256657483_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m598032533_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3528679085;
extern const uint32_t UnityChannelBindings_RequestUniquely_m1439297133_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t UnityChannelBindings_RetrieveProducts_m2334981462_MetadataUsageId;
extern const uint32_t UnityChannelBindings_Purchase_m2162196425_MetadataUsageId;
extern const uint32_t UnityChannelBindings_FinishTransaction_m1363685441_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t853706424_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2524193507_MethodInfo_var;
extern const uint32_t UnityChannelBindings__ctor_m3449949178_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass16_0_U3CValidateReceiptU3Eb__0_m573116822_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t UnityChannelImpl__ctor_m3372672074_MetadataUsageId;
extern Il2CppClass* Action_2_t1865222972_il2cpp_TypeInfo_var;
extern Il2CppClass* INativeUnityChannelStore_t670691399_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityChannelImpl_U3CRetrieveProductsU3Eb__6_0_m3443241054_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m759102168_MethodInfo_var;
extern const uint32_t UnityChannelImpl_RetrieveProducts_m86248098_MetadataUsageId;
extern Il2CppClass* U3CU3Ec__DisplayClass7_0_t505099_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass7_0_U3CPurchaseU3Eb__0_m2251081206_MethodInfo_var;
extern const uint32_t UnityChannelImpl_Purchase_m1906667446_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1533770720_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4132139590_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2595553265;
extern Il2CppCodeGenString* _stringLiteral3595395586;
extern Il2CppCodeGenString* _stringLiteral4271656310;
extern const uint32_t UnityChannelImpl_extractDeveloperPayload_m1881438288_MetadataUsageId;
extern const uint32_t UnityChannelImpl_ValidateReceipt_m2670145539_MetadataUsageId;
extern const Il2CppType* PurchaseFailureReason_t1322959839_0_0_0_var;
extern Il2CppClass* U3CU3Ec__DisplayClass7_1_t2729388454_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t3864773326_il2cpp_TypeInfo_var;
extern Il2CppClass* IStoreCallback_t2691517565_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureDescription_t1607114611_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass7_1_U3CPurchaseU3Eb__1_m1821532482_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3578948342_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3188644741_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3112617933;
extern Il2CppCodeGenString* _stringLiteral2651994212;
extern Il2CppCodeGenString* _stringLiteral1038890517;
extern Il2CppCodeGenString* _stringLiteral667782618;
extern Il2CppCodeGenString* _stringLiteral3887667925;
extern Il2CppCodeGenString* _stringLiteral2846264340;
extern Il2CppCodeGenString* _stringLiteral702192217;
extern Il2CppCodeGenString* _stringLiteral466964775;
extern Il2CppCodeGenString* _stringLiteral22409400;
extern Il2CppCodeGenString* _stringLiteral2397167425;
extern const uint32_t U3CU3Ec__DisplayClass7_0_U3CPurchaseU3Eb__0_m2251081206_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2704910744;
extern Il2CppCodeGenString* _stringLiteral3867530100;
extern Il2CppCodeGenString* _stringLiteral2442387418;
extern Il2CppCodeGenString* _stringLiteral1155927968;
extern const uint32_t U3CU3Ec__DisplayClass7_1_U3CPurchaseU3Eb__1_m1821532482_MetadataUsageId;
extern Il2CppClass* U3CU3Ec_t4236842726_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t460188795_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2004692778_il2cpp_TypeInfo_var;
extern Il2CppClass* IWindowsIAP_t818184396_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_m3748846711_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4202709936_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisProductDefinition_t1942475268_m96461137_MethodInfo_var;
extern const MethodInfo* U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m823723958_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m432649268_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046_MethodInfo_var;
extern const uint32_t WinRTStore_RetrieveProducts_m1073973345_MetadataUsageId;
extern const uint32_t WinRTStore_FinishTransaction_m1157468965_MetadataUsageId;
extern const uint32_t WinRTStore_init_m420138384_MetadataUsageId;
extern const uint32_t WinRTStore_Purchase_m27999093_MetadataUsageId;
extern const uint32_t WinRTStore_restoreTransactions_m1421431749_MetadataUsageId;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral383104206;
extern const uint32_t WinRTStore_RestoreTransactions_m1768243916_MetadataUsageId;
extern const uint32_t U3CU3Ec__cctor_m2748256470_MetadataUsageId;
extern Il2CppClass* WinProductDescription_t1075111405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1291769943;
extern Il2CppCodeGenString* _stringLiteral956924922;
extern Il2CppCodeGenString* _stringLiteral530053810;
extern Il2CppCodeGenString* _stringLiteral1690816450;
extern const uint32_t U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m823723958_MetadataUsageId;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305145____8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0_FieldInfo_var;
extern const uint32_t XiaomiPriceTiers__cctor_m3016051160_MetadataUsageId;

// System.Type[]
struct TypeU5BU5D_t1664964607  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m1798760551_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m4197675336_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
extern "C"  void UnityEvent_1_AddListener_m404084168_gshared (UnityEvent_1_t2110227463 * __this, UnityAction_1_t3438463199 * p0, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject* Enumerable_Take_TisIl2CppObject_m2048499799_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1684831714_gshared (Func_2_t2825504181 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2825504181 * p1, const MethodInfo* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2562379905_gshared (ReadOnlyCollection_1_t2875234987 * __this, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(!0,!1)
extern "C"  void Action_2_Invoke_m2365036873_gshared (Action_2_t1907880187 * __this, bool p0, int32_t p1, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(!0,!1)
extern "C"  void Action_2_Invoke_m352317182_gshared (Action_2_t2525452034 * __this, bool p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C"  void HashSet_1__ctor_m2858247305_gshared (HashSet_1_t1022910149 * __this, const MethodInfo* method);
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2569117622_gshared (ReadOnlyCollection_1_t2875234987 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C"  bool HashSet_1_Add_m199171953_gshared (HashSet_1_t1022910149 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::Invoke(!0,!1,!2)
extern "C"  void Action_3_Invoke_m138287784_gshared (Action_3_t2202283254 * __this, bool p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3321918434_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4209421183_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m4062719145_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void System.Action`3<System.Boolean,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m2414365210_gshared (Action_3_t2202283254 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1354888807_gshared (Func_2_t3961629604 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m1516493223_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m3361462832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);

// System.Void UnityEngine.Purchasing.FakeStore::.ctor()
extern "C"  void FakeStore__ctor_m3239256688 (FakeStore_t3882981564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.UIFakeStore::IsShowingDialog()
extern "C"  bool UIFakeStore_IsShowingDialog_m1285492143 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore/DialogRequest::.ctor()
extern "C"  void DialogRequest__ctor_m3883507536 (DialogRequest_t2092195449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore::InstantiateDialog()
extern "C"  void UIFakeStore_InstantiateDialog_m26685090 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t1021602117 * Resources_Load_m2041782325 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Canvas>(!!0)
#define Object_Instantiate_TisCanvas_t209405766_m805779209(__this /* static, unused */, p0, method) ((  Canvas_t209405766 * (*) (Il2CppObject * /* static, unused */, Canvas_t209405766 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier>()
#define GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664(__this, method) ((  LifecycleNotifier_t1057582876 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared)(__this, method)
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2606471964 (Action_t3226471752 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
#define Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623(__this /* static, unused */, method) ((  EventSystem_t3466835263 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m1798760551_gshared)(__this /* static, unused */, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m1633632305 (GameObject_t1756533147 * __this, String_t* p0, TypeU5BU5D_t1664964607* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.StandaloneInputModule>()
#define GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674(__this, method) ((  StandaloneInputModule_t70867863 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m3281327839 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m4280536079(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetOkayButtonText()
extern "C"  Text_t356221433 * UIFakeStore_GetOkayButtonText_m6562956 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetCancelButtonText()
extern "C"  Text_t356221433 * UIFakeStore_GetCancelButtonText_m1587731078 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Dropdown UnityEngine.Purchasing.UIFakeStore::GetDropdown()
extern "C"  Dropdown_t1985816271 * UIFakeStore_GetDropdown_m651796348 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown::get_options()
extern "C"  List_1_t1789388632 * Dropdown_get_options_m2669836220 (Dropdown_t1985816271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Clear()
#define List_1_Clear_m1204779269(__this, method) ((  void (*) (List_1_t1789388632 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m1412753878(__this, method) ((  Enumerator_t933071039  (*) (List_1_t1398341365 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m2837468952(__this, method) ((  String_t* (*) (Enumerator_t933071039 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(System.String)
extern "C"  void OptionData__ctor_m743450704 (OptionData_t2420267500 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Add(!0)
#define List_1_Add_m3486116920(__this, p0, method) ((  void (*) (List_1_t1789388632 *, OptionData_t2420267500 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m1105633690(__this, method) ((  bool (*) (Enumerator_t933071039 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m2205157096(__this, method) ((  void (*) (Enumerator_t933071039 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m780127360(__this, method) ((  int32_t (*) (List_1_t1398341365 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void UnityEngine.UI.Dropdown::RefreshShownValue()
extern "C"  void Dropdown_RefreshShownValue_m3113581237 (Dropdown_t1985816271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetOkayButton()
extern "C"  Button_t2872111280 * UIFakeStore_GetOkayButton_m2121832312 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C"  ButtonClickedEvent_t2455055323 * Button_get_onClick_m1595880935 (Button_t2872111280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2649891629 (UnityAction_t4025899511 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m1596810379 (UnityEvent_t408735097 * __this, UnityAction_t4025899511 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetCancelButton()
extern "C"  Button_t2872111280 * UIFakeStore_GetCancelButton_m1851079754 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::get_onValueChanged()
extern "C"  DropdownEvent_t2203087800 * Dropdown_get_onValueChanged_m3334401942 (Dropdown_t1985816271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m4197675336(__this, p0, p1, method) ((  void (*) (UnityAction_1_t3438463199 *, Il2CppObject *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m4197675336_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
#define UnityEvent_1_AddListener_m404084168(__this, p0, method) ((  void (*) (UnityEvent_1_t2110227463 *, UnityAction_1_t3438463199 *, const MethodInfo*))UnityEvent_1_AddListener_m404084168_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityEventBase::RemoveAllListeners()
extern "C"  void UnityEventBase_RemoveAllListeners_m230188638 (UnityEventBase_t828812576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetDropdownContainerGameObject()
extern "C"  GameObject_t1756533147 * UIFakeStore_GetDropdownContainerGameObject_m2190404260 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetCancelButtonGameObject()
extern "C"  GameObject_t1756533147 * UIFakeStore_GetCancelButtonGameObject_m4002152618 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.ProductDefinition::get_id()
extern "C"  String_t* ProductDefinition_get_id_m264072292 (ProductDefinition_t1942475268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Purchasing.ProductDefinition>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisProductDefinition_t1942475268_m2540339846(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_Take_TisIl2CppObject_m2048499799_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m585608501(__this, p0, p1, method) ((  void (*) (Func_2_t2958801606 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Purchasing.ProductDefinition,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2958801606 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m1953054010(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// System.String System.String::Join(System.String,System.String[])
extern "C"  String_t* String_Join_m1966872927 (Il2CppObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1642385972* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2134061681(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2128260960 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2562379905_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m3862106414(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
#define GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(__this, method) ((  Dropdown_t1985816271 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m2671311541 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(!0,!1)
#define Action_2_Invoke_m2365036873(__this, p0, p1, method) ((  void (*) (Action_2_t1907880187 *, bool, int32_t, const MethodInfo*))Action_2_Invoke_m2365036873_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Purchasing.UIFakeStore::CloseDialog()
extern "C"  void UIFakeStore_CloseDialog_m4132523490 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore::OkayButtonClicked()
extern "C"  void UIFakeStore_OkayButtonClicked_m1579250743 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore::CancelButtonClicked()
extern "C"  void UIFakeStore_CancelButtonClicked_m1822980185 (UIFakeStore_t3684252124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore::DropdownValueChanged(System.Int32)
extern "C"  void UIFakeStore_DropdownValueChanged_m3219846161 (UIFakeStore_t3684252124 * __this, int32_t ___selectedItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UIFakeStore/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2060140322 (U3CU3Ec_t3471345421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m3801112262 (Action_t3226471752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`2<System.Boolean,System.String>::Invoke(!0,!1)
#define Action_2_Invoke_m547817495(__this, p0, p1, method) ((  void (*) (Action_2_t1865222972 *, bool, String_t*, const MethodInfo*))Action_2_Invoke_m352317182_gshared)(__this, p0, p1, method)
// System.Guid System.Guid::NewGuid()
extern "C"  Guid_t  Guid_NewGuid_m3493657620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Guid::ToString()
extern "C"  String_t* Guid_ToString_m3927110175 (Guid_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ChannelPurchase.PurchaseService::Purchase(System.String,System.String,UnityEngine.ChannelPurchase.IPurchaseListener,System.String)
extern "C"  void PurchaseService_Purchase_m3636124256 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, Il2CppObject * p2, String_t* p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::.ctor()
#define HashSet_1__ctor_m738744506(__this, method) ((  void (*) (HashSet_1_t1651728377 *, const MethodInfo*))HashSet_1__ctor_m2858247305_gshared)(__this, method)
// UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.ProductCatalog::LoadDefaultCatalog()
extern "C"  ProductCatalog_t2667590766 * ProductCatalog_LoadDefaultCatalog_m589345558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::get_allValidProducts()
extern "C"  Il2CppObject* ProductCatalog_get_allValidProducts_m1766704888 (ProductCatalog_t2667590766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<!0> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m178725564(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2128260960 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2569117622_gshared)(__this, method)
// System.Boolean System.String::Equals(System.String,System.String)
extern "C"  bool String_Equals_m3568148125 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.LocalizedProductDescription UnityEngine.Purchasing.ProductCatalogItem::GetDescription(UnityEngine.Purchasing.TranslationLocale)
extern "C"  LocalizedProductDescription_t1525635964 * ProductCatalogItem_GetDescription_m2643762087 (ProductCatalogItem_t977711995 * __this, int32_t ___locale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Title()
extern "C"  String_t* LocalizedProductDescription_get_Title_m3689615546 (LocalizedProductDescription_t1525635964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.LocalizedProductDescription::get_Description()
extern "C"  String_t* LocalizedProductDescription_get_Description_m943727562 (LocalizedProductDescription_t1525635964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Decimal::op_Implicit(System.Int32)
extern "C"  Decimal_t724701077  Decimal_op_Implicit_m3312726949 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.ProductMetadata::.ctor(System.String,System.String,System.String,System.String,System.Decimal)
extern "C"  void ProductMetadata__ctor_m3648829555 (ProductMetadata_t1573242544 * __this, String_t* p0, String_t* p1, String_t* p2, String_t* p3, Decimal_t724701077  p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.ProductDefinition::get_storeSpecificId()
extern "C"  String_t* ProductDefinition_get_storeSpecificId_m2251287741 (ProductDefinition_t1942475268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Extension.ProductDescription::.ctor(System.String,UnityEngine.Purchasing.ProductMetadata)
extern "C"  void ProductDescription__ctor_m2055358744 (ProductDescription_t3318267523 * __this, String_t* p0, ProductMetadata_t1573242544 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Extension.ProductDescription>::Add(!0)
#define HashSet_1_Add_m1190086556(__this, p0, method) ((  bool (*) (HashSet_1_t1651728377 *, ProductDescription_t3318267523 *, const MethodInfo*))HashSet_1_Add_m199171953_gshared)(__this, p0, method)
// System.String UnityEngine.Purchasing.JSONSerializer::SerializeProductDescs(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Extension.ProductDescription>)
extern "C"  String_t* JSONSerializer_SerializeProductDescs_m582436362 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___products0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m1806464238 (U3CU3Ec__DisplayClass16_0_t1107960729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RequestUniquely(System.String,System.Action`3<System.Boolean,System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>,System.Action)
extern "C"  void UnityChannelBindings_RequestUniquely_m1439297133 (UnityChannelBindings_t2880355556 * __this, String_t* ___transactionId0, Action_3_t3864773326 * ___callback1, Dictionary_2_t853706424 * ___callbackDictionary2, Action_t3226471752 * ___requestAction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2802126737 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`3<System.Boolean,System.String,System.String>::Invoke(!0,!1,!2)
#define Action_3_Invoke_m304290708(__this, p0, p1, p2, method) ((  void (*) (Action_3_t3864773326 *, bool, String_t*, String_t*, const MethodInfo*))Action_3_Invoke_m138287784_gshared)(__this, p0, p1, p2, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m661064896(__this, p0, method) ((  bool (*) (Dictionary_2_t853706424 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>::.ctor()
#define List_1__ctor_m1828621836(__this, method) ((  void (*) (List_1_t3233894458 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>::Add(!0)
#define List_1_Add_m2269440416(__this, p0, method) ((  void (*) (List_1_t3233894458 *, Action_3_t3864773326 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>::Add(!0,!1)
#define Dictionary_2_Add_m1256657483(__this, p0, p1, method) ((  void (*) (Dictionary_2_t853706424 *, String_t*, List_1_t3233894458 *, const MethodInfo*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>::get_Item(!0)
#define Dictionary_2_get_Item_m598032533(__this, p0, method) ((  List_1_t3233894458 * (*) (Dictionary_2_t853706424 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m808189835 (NotImplementedException_t2785117854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>::.ctor()
#define Dictionary_2__ctor_m2524193507(__this, method) ((  void (*) (Dictionary_2_t853706424 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void UnityEngine.ChannelPurchase.PurchaseService::ValidateReceipt(System.String,UnityEngine.ChannelPurchase.IPurchaseListener)
extern "C"  void PurchaseService_ValidateReceipt_m2412817733 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::.ctor()
extern "C"  void JSONStore__ctor_m1861746843 (JSONStore_t1890359403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.JSONStore::SetNativeStore(UnityEngine.Purchasing.INativeStore)
extern "C"  void JSONStore_SetNativeStore_m574687663 (JSONStore_t1890359403 * __this, Il2CppObject * ___native0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`2<System.Boolean,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m759102168(__this, p0, p1, method) ((  void (*) (Action_2_t1865222972 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m946854823_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m2464233274 (U3CU3Ec__DisplayClass7_0_t505099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Purchasing.MiniJson::JsonDecode(System.String)
extern "C"  Il2CppObject * MiniJson_JsonDecode_m947751129 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1533770720(__this, p0, method) ((  bool (*) (Dictionary_2_t309261261 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0)
#define Dictionary_2_get_Item_m464793699(__this, p0, method) ((  Il2CppObject * (*) (Dictionary_2_t309261261 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m4132139590(__this, p0, p1, method) ((  void (*) (Dictionary_2_t309261261 *, String_t*, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_1__ctor_m2463119227 (U3CU3Ec__DisplayClass7_1_t2729388454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::HashtableFromJson(System.String)
extern "C"  Dictionary_2_t309261261 * MiniJsonExtensions_HashtableFromJson_m374830805 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::GetString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,System.String)
extern "C"  String_t* MiniJsonExtensions_GetString_m2316350864 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String)
extern "C"  bool String_Equals_m2633592423 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarningFormat(System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m2130157695 (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::get_fetchReceiptPayloadOnPurchase()
extern "C"  bool UnityChannelImpl_get_fetchReceiptPayloadOnPurchase_m154957409 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`3<System.Boolean,System.String,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m3578948342(__this, p0, p1, method) ((  void (*) (Action_3_t3864773326 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2414365210_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Purchasing.UnityChannelImpl::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void UnityChannelImpl_ValidateReceipt_m2670145539 (UnityChannelImpl_t1327714682 * __this, String_t* ___transactionIdentifier0, Action_3_t3864773326 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.MiniJSON.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  String_t* MiniJsonExtensions_toJson_m2405339524 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Enum::Parse(System.Type,System.String)
extern "C"  Il2CppObject * Enum_Parse_m2561000069 (Il2CppObject * __this /* static, unused */, Type_t * p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Boolean::TryParse(System.String,System.Boolean&)
extern "C"  bool Boolean_TryParse_m3918169608 (Il2CppObject * __this /* static, unused */, String_t* p0, bool* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Enum::IsDefined(System.Type,System.Object)
extern "C"  bool Enum_IsDefined_m92789062 (Il2CppObject * __this /* static, unused */, Type_t * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
#define Dictionary_2__ctor_m3188644741(__this, method) ((  void (*) (Dictionary_2_t309261261 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void UnityEngine.Purchasing.Extension.PurchaseFailureDescription::.ctor(System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String)
extern "C"  void PurchaseFailureDescription__ctor_m381019375 (PurchaseFailureDescription_t1607114611 * __this, String_t* p0, int32_t p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.UnityChannelImpl::extractDeveloperPayload(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  void UnityChannelImpl_extractDeveloperPayload_m1881438288 (UnityChannelImpl_t1327714682 * __this, Dictionary_2_t309261261 * ___dic0, String_t* ___signData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Extension.AbstractStore::.ctor()
extern "C"  void AbstractStore__ctor_m212291193 (AbstractStore_t2787096497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Purchasing.ProductDefinition,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m4202709936(__this, p0, p1, method) ((  void (*) (Func_2_t460188795 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1354888807_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Purchasing.ProductDefinition>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisProductDefinition_t1942475268_m96461137(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t460188795 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m1516493223_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m432649268(__this, p0, p1, method) ((  void (*) (Func_2_t2004692778 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Default.WinProductDescription>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2004692778 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.Purchasing.Default.WinProductDescription>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046(__this /* static, unused */, p0, method) ((  List_1_t444232537 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Purchasing.WinRTStore::init(System.Int32)
extern "C"  void WinRTStore_init_m420138384 (WinRTStore_t36043095 * __this, int32_t ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.WinRTStore/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m161161559 (U3CU3Ec_t4236842726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.ProductDefinition::get_type()
extern "C"  int32_t ProductDefinition_get_type_m590914665 (ProductDefinition_t1942475268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Decimal::.ctor(System.Int32,System.Int32,System.Int32,System.Boolean,System.Byte)
extern "C"  void Decimal__ctor_m1770144563 (Decimal_t724701077 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, uint8_t p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Default.WinProductDescription::.ctor(System.String,System.String,System.String,System.String,System.String,System.Decimal,System.String,System.String,System.Boolean)
extern "C"  void WinProductDescription__ctor_m3122598843 (WinProductDescription_t1075111405 * __this, String_t* p0, String_t* p1, String_t* p2, String_t* p3, String_t* p4, Decimal_t724701077  p5, String_t* p6, String_t* p7, bool p8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, RuntimeFieldHandle_t2331729674  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Purchasing.UIFakeStore::.ctor()
extern "C"  void UIFakeStore__ctor_m4281629614 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		FakeStore__ctor_m3239256688(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.UIFakeStore::StartUI(System.String,System.String,System.String,System.Collections.Generic.List`1<System.String>,System.Action`2<System.Boolean,System.Int32>)
extern "C"  bool UIFakeStore_StartUI_m2569939907 (UIFakeStore_t3684252124 * __this, String_t* ___queryText0, String_t* ___okayButtonText1, String_t* ___cancelButtonText2, List_1_t1398341365 * ___options3, Action_2_t1907880187 * ___callback4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_StartUI_m2569939907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DialogRequest_t2092195449 * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		bool L_0 = UIFakeStore_IsShowingDialog_m1285492143(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_004d;
	}

IL_0010:
	{
		DialogRequest_t2092195449 * L_2 = (DialogRequest_t2092195449 *)il2cpp_codegen_object_new(DialogRequest_t2092195449_il2cpp_TypeInfo_var);
		DialogRequest__ctor_m3883507536(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		DialogRequest_t2092195449 * L_3 = V_0;
		String_t* L_4 = ___queryText0;
		NullCheck(L_3);
		L_3->set_QueryText_0(L_4);
		DialogRequest_t2092195449 * L_5 = V_0;
		String_t* L_6 = ___okayButtonText1;
		NullCheck(L_5);
		L_5->set_OkayButtonText_1(L_6);
		DialogRequest_t2092195449 * L_7 = V_0;
		String_t* L_8 = ___cancelButtonText2;
		NullCheck(L_7);
		L_7->set_CancelButtonText_2(L_8);
		DialogRequest_t2092195449 * L_9 = V_0;
		List_1_t1398341365 * L_10 = ___options3;
		NullCheck(L_9);
		L_9->set_Options_3(L_10);
		DialogRequest_t2092195449 * L_11 = V_0;
		Action_2_t1907880187 * L_12 = ___callback4;
		NullCheck(L_11);
		L_11->set_Callback_4(L_12);
		DialogRequest_t2092195449 * L_13 = V_0;
		__this->set_m_CurrentDialog_27(L_13);
		UIFakeStore_InstantiateDialog_m26685090(__this, /*hidden argument*/NULL);
		V_2 = (bool)1;
		goto IL_004d;
	}

IL_004d:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::InstantiateDialog()
extern "C"  void UIFakeStore_InstantiateDialog_m26685090 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_InstantiateDialog_m26685090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Canvas_t209405766 * V_0 = NULL;
	LifecycleNotifier_t1057582876 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Text_t356221433 * V_3 = NULL;
	Text_t356221433 * V_4 = NULL;
	Text_t356221433 * V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	Enumerator_t933071039  V_9;
	memset(&V_9, 0, sizeof(V_9));
	String_t* V_10 = NULL;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DialogRequest_t2092195449 * L_0 = __this->get_m_CurrentDialog_27();
		V_6 = (bool)((((Il2CppObject*)(DialogRequest_t2092195449 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_6;
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m56707527(NULL /*static, unused*/, __this, _stringLiteral4204482283, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		goto IL_02af;
	}

IL_0027:
	{
		GameObject_t1756533147 * L_3 = __this->get_UIFakeStoreCanvasPrefab_29();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_7 = L_4;
		bool L_5 = V_7;
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		Object_t1021602117 * L_6 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral1832009962, /*hidden argument*/NULL);
		__this->set_UIFakeStoreCanvasPrefab_29(((GameObject_t1756533147 *)IsInstSealed(L_6, GameObject_t1756533147_il2cpp_TypeInfo_var)));
	}

IL_0050:
	{
		GameObject_t1756533147 * L_7 = __this->get_UIFakeStoreCanvasPrefab_29();
		NullCheck(L_7);
		Canvas_t209405766 * L_8 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_7, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var);
		V_0 = L_8;
		Canvas_t209405766 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Canvas_t209405766 * L_10 = Object_Instantiate_TisCanvas_t209405766_m805779209(NULL /*static, unused*/, L_9, /*hidden argument*/Object_Instantiate_TisCanvas_t209405766_m805779209_MethodInfo_var);
		__this->set_m_Canvas_30(L_10);
		Canvas_t209405766 * L_11 = __this->get_m_Canvas_30();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		LifecycleNotifier_t1057582876 * L_13 = GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664(L_12, /*hidden argument*/GameObject_AddComponent_TisLifecycleNotifier_t1057582876_m1564113664_MethodInfo_var);
		V_1 = L_13;
		LifecycleNotifier_t1057582876 * L_14 = V_1;
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Eb__16_0_m16729514_MethodInfo_var);
		Action_t3226471752 * L_16 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_16, __this, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_OnDestroyCallback_2(L_16);
		Canvas_t209405766 * L_17 = __this->get_m_Canvas_30();
		NullCheck(L_17);
		String_t* L_18 = Object_get_name_m2079638459(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, _stringLiteral3655311540, /*hidden argument*/NULL);
		__this->set_m_ParentGameObjectPath_32(L_19);
		EventSystem_t3466835263 * L_20 = Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEventSystem_t3466835263_m929139623_MethodInfo_var);
		bool L_21 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_20, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_8 = L_21;
		bool L_22 = V_8;
		if (!L_22)
		{
			goto IL_0104;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_23 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(EventSystem_t3466835263_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_24);
		GameObject_t1756533147 * L_25 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m1633632305(L_25, _stringLiteral3628703177, L_23, /*hidden argument*/NULL);
		__this->set_m_EventSystem_31(L_25);
		GameObject_t1756533147 * L_26 = __this->get_m_EventSystem_31();
		NullCheck(L_26);
		GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674(L_26, /*hidden argument*/GameObject_AddComponent_TisStandaloneInputModule_t70867863_m4180824674_MethodInfo_var);
		GameObject_t1756533147 * L_27 = __this->get_m_EventSystem_31();
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = GameObject_get_transform_m909382139(L_27, /*hidden argument*/NULL);
		Canvas_t209405766 * L_29 = __this->get_m_Canvas_30();
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_parent_m3281327839(L_28, L_30, /*hidden argument*/NULL);
	}

IL_0104:
	{
		String_t* L_31 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, L_31, _stringLiteral4267440200, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_33 = GameObject_Find_m836511350(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		GameObject_t1756533147 * L_34 = V_2;
		NullCheck(L_34);
		Text_t356221433 * L_35 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_34, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_3 = L_35;
		Text_t356221433 * L_36 = V_3;
		DialogRequest_t2092195449 * L_37 = __this->get_m_CurrentDialog_27();
		NullCheck(L_37);
		String_t* L_38 = L_37->get_QueryText_0();
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_36, L_38);
		Text_t356221433 * L_39 = UIFakeStore_GetOkayButtonText_m6562956(__this, /*hidden argument*/NULL);
		V_4 = L_39;
		Text_t356221433 * L_40 = V_4;
		DialogRequest_t2092195449 * L_41 = __this->get_m_CurrentDialog_27();
		NullCheck(L_41);
		String_t* L_42 = L_41->get_OkayButtonText_1();
		NullCheck(L_40);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_40, L_42);
		Text_t356221433 * L_43 = UIFakeStore_GetCancelButtonText_m1587731078(__this, /*hidden argument*/NULL);
		V_5 = L_43;
		Text_t356221433 * L_44 = V_5;
		DialogRequest_t2092195449 * L_45 = __this->get_m_CurrentDialog_27();
		NullCheck(L_45);
		String_t* L_46 = L_45->get_CancelButtonText_2();
		NullCheck(L_44);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_44, L_46);
		Dropdown_t1985816271 * L_47 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		List_1_t1789388632 * L_48 = Dropdown_get_options_m2669836220(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		List_1_Clear_m1204779269(L_48, /*hidden argument*/List_1_Clear_m1204779269_MethodInfo_var);
		DialogRequest_t2092195449 * L_49 = __this->get_m_CurrentDialog_27();
		NullCheck(L_49);
		List_1_t1398341365 * L_50 = L_49->get_Options_3();
		NullCheck(L_50);
		Enumerator_t933071039  L_51 = List_1_GetEnumerator_m1412753878(L_50, /*hidden argument*/List_1_GetEnumerator_m1412753878_MethodInfo_var);
		V_9 = L_51;
	}

IL_018d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01b2;
		}

IL_018f:
		{
			String_t* L_52 = Enumerator_get_Current_m2837468952((&V_9), /*hidden argument*/Enumerator_get_Current_m2837468952_MethodInfo_var);
			V_10 = L_52;
			Dropdown_t1985816271 * L_53 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
			NullCheck(L_53);
			List_1_t1789388632 * L_54 = Dropdown_get_options_m2669836220(L_53, /*hidden argument*/NULL);
			String_t* L_55 = V_10;
			OptionData_t2420267500 * L_56 = (OptionData_t2420267500 *)il2cpp_codegen_object_new(OptionData_t2420267500_il2cpp_TypeInfo_var);
			OptionData__ctor_m743450704(L_56, L_55, /*hidden argument*/NULL);
			NullCheck(L_54);
			List_1_Add_m3486116920(L_54, L_56, /*hidden argument*/List_1_Add_m3486116920_MethodInfo_var);
		}

IL_01b2:
		{
			bool L_57 = Enumerator_MoveNext_m1105633690((&V_9), /*hidden argument*/Enumerator_MoveNext_m1105633690_MethodInfo_var);
			if (L_57)
			{
				goto IL_018f;
			}
		}

IL_01bb:
		{
			IL2CPP_LEAVE(0x1CC, FINALLY_01bd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01bd;
	}

FINALLY_01bd:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_9), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(445)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(445)
	{
		IL2CPP_JUMP_TBL(0x1CC, IL_01cc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01cc:
	{
		DialogRequest_t2092195449 * L_58 = __this->get_m_CurrentDialog_27();
		NullCheck(L_58);
		List_1_t1398341365 * L_59 = L_58->get_Options_3();
		NullCheck(L_59);
		int32_t L_60 = List_1_get_Count_m780127360(L_59, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		V_11 = (bool)((((int32_t)L_60) > ((int32_t)0))? 1 : 0);
		bool L_61 = V_11;
		if (!L_61)
		{
			goto IL_01ee;
		}
	}
	{
		__this->set_m_LastSelectedDropdownIndex_28(0);
	}

IL_01ee:
	{
		Dropdown_t1985816271 * L_62 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_62);
		Dropdown_RefreshShownValue_m3113581237(L_62, /*hidden argument*/NULL);
		Button_t2872111280 * L_63 = UIFakeStore_GetOkayButton_m2121832312(__this, /*hidden argument*/NULL);
		NullCheck(L_63);
		ButtonClickedEvent_t2455055323 * L_64 = Button_get_onClick_m1595880935(L_63, /*hidden argument*/NULL);
		IntPtr_t L_65;
		L_65.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Eb__16_1_m16729609_MethodInfo_var);
		UnityAction_t4025899511 * L_66 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_66, __this, L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		UnityEvent_AddListener_m1596810379(L_64, L_66, /*hidden argument*/NULL);
		Button_t2872111280 * L_67 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		ButtonClickedEvent_t2455055323 * L_68 = Button_get_onClick_m1595880935(L_67, /*hidden argument*/NULL);
		IntPtr_t L_69;
		L_69.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Eb__16_2_m16729448_MethodInfo_var);
		UnityAction_t4025899511 * L_70 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_70, __this, L_69, /*hidden argument*/NULL);
		NullCheck(L_68);
		UnityEvent_AddListener_m1596810379(L_68, L_70, /*hidden argument*/NULL);
		Dropdown_t1985816271 * L_71 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		DropdownEvent_t2203087800 * L_72 = Dropdown_get_onValueChanged_m3334401942(L_71, /*hidden argument*/NULL);
		IntPtr_t L_73;
		L_73.set_m_value_0((void*)(void*)UIFakeStore_U3CInstantiateDialogU3Eb__16_3_m4148963786_MethodInfo_var);
		UnityAction_1_t3438463199 * L_74 = (UnityAction_1_t3438463199 *)il2cpp_codegen_object_new(UnityAction_1_t3438463199_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m4197675336(L_74, __this, L_73, /*hidden argument*/UnityAction_1__ctor_m4197675336_MethodInfo_var);
		NullCheck(L_72);
		UnityEvent_1_AddListener_m404084168(L_72, L_74, /*hidden argument*/UnityEvent_1_AddListener_m404084168_MethodInfo_var);
		int32_t L_75 = ((FakeStore_t3882981564 *)__this)->get_UIMode_26();
		V_12 = (bool)((((int32_t)L_75) == ((int32_t)1))? 1 : 0);
		bool L_76 = V_12;
		if (!L_76)
		{
			goto IL_0281;
		}
	}
	{
		Dropdown_t1985816271 * L_77 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_77);
		DropdownEvent_t2203087800 * L_78 = Dropdown_get_onValueChanged_m3334401942(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		UnityEventBase_RemoveAllListeners_m230188638(L_78, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_79 = UIFakeStore_GetDropdownContainerGameObject_m2190404260(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		goto IL_02af;
	}

IL_0281:
	{
		int32_t L_80 = ((FakeStore_t3882981564 *)__this)->get_UIMode_26();
		V_13 = (bool)((((int32_t)L_80) == ((int32_t)2))? 1 : 0);
		bool L_81 = V_13;
		if (!L_81)
		{
			goto IL_02af;
		}
	}
	{
		Button_t2872111280 * L_82 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		NullCheck(L_82);
		ButtonClickedEvent_t2455055323 * L_83 = Button_get_onClick_m1595880935(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		UnityEventBase_RemoveAllListeners_m230188638(L_83, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_84 = UIFakeStore_GetCancelButtonGameObject_m4002152618(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
	}

IL_02af:
	{
		return;
	}
}
// System.String UnityEngine.Purchasing.UIFakeStore::CreatePurchaseQuestion(UnityEngine.Purchasing.ProductDefinition)
extern "C"  String_t* UIFakeStore_CreatePurchaseQuestion_m2287137335 (UIFakeStore_t3684252124 * __this, ProductDefinition_t1942475268 * ___definition0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CreatePurchaseQuestion_m2287137335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ProductDefinition_t1942475268 * L_0 = ___definition0;
		NullCheck(L_0);
		String_t* L_1 = ProductDefinition_get_id_m264072292(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3368079914, L_1, _stringLiteral3820628614, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Purchasing.UIFakeStore::CreateRetrieveProductsQuestion(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  String_t* UIFakeStore_CreateRetrieveProductsQuestion_m4106290161 (UIFakeStore_t3684252124 * __this, ReadOnlyCollection_1_t2128260960 * ___definitions0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CreateRetrieveProductsQuestion_m4106290161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	Func_2_t2958801606 * G_B2_0 = NULL;
	Il2CppObject* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	Func_2_t2958801606 * G_B1_0 = NULL;
	Il2CppObject* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		V_0 = _stringLiteral1923700425;
		String_t* L_0 = V_0;
		ReadOnlyCollection_1_t2128260960 * L_1 = ___definitions0;
		Il2CppObject* L_2 = Enumerable_Take_TisProductDefinition_t1942475268_m2540339846(NULL /*static, unused*/, L_1, 2, /*hidden argument*/Enumerable_Take_TisProductDefinition_t1942475268_m2540339846_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t3471345421_il2cpp_TypeInfo_var);
		Func_2_t2958801606 * L_3 = ((U3CU3Ec_t3471345421_StaticFields*)U3CU3Ec_t3471345421_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__18_0_1();
		Func_2_t2958801606 * L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		G_B1_2 = _stringLiteral811305474;
		G_B1_3 = L_0;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			G_B2_2 = _stringLiteral811305474;
			G_B2_3 = L_0;
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t3471345421_il2cpp_TypeInfo_var);
		U3CU3Ec_t3471345421 * L_5 = ((U3CU3Ec_t3471345421_StaticFields*)U3CU3Ec_t3471345421_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_m980068044_MethodInfo_var);
		Func_2_t2958801606 * L_7 = (Func_2_t2958801606 *)il2cpp_codegen_object_new(Func_2_t2958801606_il2cpp_TypeInfo_var);
		Func_2__ctor_m585608501(L_7, L_5, L_6, /*hidden argument*/Func_2__ctor_m585608501_MethodInfo_var);
		Func_2_t2958801606 * L_8 = L_7;
		((U3CU3Ec_t3471345421_StaticFields*)U3CU3Ec_t3471345421_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__18_0_1(L_8);
		G_B2_0 = L_8;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0033:
	{
		Il2CppObject* L_9 = Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709(NULL /*static, unused*/, G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Select_TisProductDefinition_t1942475268_TisString_t_m2207542709_MethodInfo_var);
		StringU5BU5D_t1642385972* L_10 = Enumerable_ToArray_TisString_t_m1953054010(NULL /*static, unused*/, L_9, /*hidden argument*/Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Join_m1966872927(NULL /*static, unused*/, G_B2_2, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, G_B2_3, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		ReadOnlyCollection_1_t2128260960 * L_13 = ___definitions0;
		NullCheck(L_13);
		int32_t L_14 = ReadOnlyCollection_1_get_Count_m2134061681(L_13, /*hidden argument*/ReadOnlyCollection_1_get_Count_m2134061681_MethodInfo_var);
		V_1 = (bool)((((int32_t)L_14) > ((int32_t)2))? 1 : 0);
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, L_16, _stringLiteral3422253728, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_0063:
	{
		String_t* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, _stringLiteral3741546081, /*hidden argument*/NULL);
		V_0 = L_19;
		String_t* L_20 = V_0;
		V_2 = L_20;
		goto IL_0073;
	}

IL_0073:
	{
		String_t* L_21 = V_2;
		return L_21;
	}
}
// UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetOkayButton()
extern "C"  Button_t2872111280 * UIFakeStore_GetOkayButton_m2121832312 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetOkayButton_m2121832312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t2872111280 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3889149951, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Button_t2872111280 * L_3 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_2, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		Button_t2872111280 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.UI.Button UnityEngine.Purchasing.UIFakeStore::GetCancelButton()
extern "C"  Button_t2872111280 * UIFakeStore_GetCancelButton_m1851079754 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetCancelButton_m1851079754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	bool V_1 = false;
	Button_t2872111280 * V_2 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3889149952, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		Button_t2872111280 * L_7 = GameObject_GetComponent_TisButton_t2872111280_m3862106414(L_6, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m3862106414_MethodInfo_var);
		V_2 = L_7;
		goto IL_0031;
	}

IL_002c:
	{
		V_2 = (Button_t2872111280 *)NULL;
		goto IL_0031;
	}

IL_0031:
	{
		Button_t2872111280 * L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetCancelButtonGameObject()
extern "C"  GameObject_t1756533147 * UIFakeStore_GetCancelButtonGameObject_m4002152618 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetCancelButtonGameObject_m4002152618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral3889149952, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		GameObject_t1756533147 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetOkayButtonText()
extern "C"  Text_t356221433 * UIFakeStore_GetOkayButtonText_m6562956 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetOkayButtonText_m6562956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral1636160581, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		Text_t356221433 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.UI.Text UnityEngine.Purchasing.UIFakeStore::GetCancelButtonText()
extern "C"  Text_t356221433 * UIFakeStore_GetCancelButtonText_m1587731078 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetCancelButtonText_m1587731078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral1636166022, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t356221433 * L_3 = GameObject_GetComponent_TisText_t356221433_m4280536079(L_2, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m4280536079_MethodInfo_var);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		Text_t356221433 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.UI.Dropdown UnityEngine.Purchasing.UIFakeStore::GetDropdown()
extern "C"  Dropdown_t1985816271 * UIFakeStore_GetDropdown_m651796348 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetDropdown_m651796348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	bool V_1 = false;
	Dropdown_t1985816271 * V_2 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral4076336408, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		Dropdown_t1985816271 * L_7 = GameObject_GetComponent_TisDropdown_t1985816271_m1750975685(L_6, /*hidden argument*/GameObject_GetComponent_TisDropdown_t1985816271_m1750975685_MethodInfo_var);
		V_2 = L_7;
		goto IL_0031;
	}

IL_002c:
	{
		V_2 = (Dropdown_t1985816271 *)NULL;
		goto IL_0031;
	}

IL_0031:
	{
		Dropdown_t1985816271 * L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::GetDropdownContainerGameObject()
extern "C"  GameObject_t1756533147 * UIFakeStore_GetDropdownContainerGameObject_m2190404260 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_GetDropdownContainerGameObject_m2190404260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ParentGameObjectPath_32();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral516648086, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		GameObject_t1756533147 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::OkayButtonClicked()
extern "C"  void UIFakeStore_OkayButtonClicked_m1579250743 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_OkayButtonClicked_m1579250743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t G_B3_0 = 0;
	{
		V_0 = (bool)0;
		int32_t L_0 = __this->get_m_LastSelectedDropdownIndex_28();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = ((FakeStore_t3882981564 *)__this)->get_UIMode_26();
		G_B3_0 = ((((int32_t)((((int32_t)L_1) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 1;
	}

IL_001a:
	{
		V_2 = (bool)G_B3_0;
		bool L_2 = V_2;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0022:
	{
		int32_t L_3 = __this->get_m_LastSelectedDropdownIndex_28();
		int32_t L_4 = Math_Max_m2671311541(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_4;
		DialogRequest_t2092195449 * L_5 = __this->get_m_CurrentDialog_27();
		NullCheck(L_5);
		Action_2_t1907880187 * L_6 = L_5->get_Callback_4();
		bool L_7 = V_0;
		int32_t L_8 = V_1;
		NullCheck(L_6);
		Action_2_Invoke_m2365036873(L_6, L_7, L_8, /*hidden argument*/Action_2_Invoke_m2365036873_MethodInfo_var);
		UIFakeStore_CloseDialog_m4132523490(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::CancelButtonClicked()
extern "C"  void UIFakeStore_CancelButtonClicked_m1822980185 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CancelButtonClicked_m1822980185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_LastSelectedDropdownIndex_28();
		int32_t L_1 = Math_Max_m2671311541(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_0-(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_1;
		DialogRequest_t2092195449 * L_2 = __this->get_m_CurrentDialog_27();
		NullCheck(L_2);
		Action_2_t1907880187 * L_3 = L_2->get_Callback_4();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Action_2_Invoke_m2365036873(L_3, (bool)0, L_4, /*hidden argument*/Action_2_Invoke_m2365036873_MethodInfo_var);
		UIFakeStore_CloseDialog_m4132523490(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::DropdownValueChanged(System.Int32)
extern "C"  void UIFakeStore_DropdownValueChanged_m3219846161 (UIFakeStore_t3684252124 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___selectedItem0;
		__this->set_m_LastSelectedDropdownIndex_28(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::CloseDialog()
extern "C"  void UIFakeStore_CloseDialog_m4132523490 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIFakeStore_CloseDialog_m4132523490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		__this->set_m_CurrentDialog_27((DialogRequest_t2092195449 *)NULL);
		Button_t2872111280 * L_0 = UIFakeStore_GetOkayButton_m2121832312(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ButtonClickedEvent_t2455055323 * L_1 = Button_get_onClick_m1595880935(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		UnityEventBase_RemoveAllListeners_m230188638(L_1, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		Button_t2872111280 * L_5 = UIFakeStore_GetCancelButton_m1851079754(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		ButtonClickedEvent_t2455055323 * L_6 = Button_get_onClick_m1595880935(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEventBase_RemoveAllListeners_m230188638(L_6, /*hidden argument*/NULL);
	}

IL_003b:
	{
		Dropdown_t1985816271 * L_7 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_1 = L_8;
		bool L_9 = V_1;
		if (!L_9)
		{
			goto IL_005e;
		}
	}
	{
		Dropdown_t1985816271 * L_10 = UIFakeStore_GetDropdown_m651796348(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		DropdownEvent_t2203087800 * L_11 = Dropdown_get_onValueChanged_m3334401942(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		UnityEventBase_RemoveAllListeners_m230188638(L_11, /*hidden argument*/NULL);
	}

IL_005e:
	{
		Canvas_t209405766 * L_12 = __this->get_m_Canvas_30();
		NullCheck(L_12);
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.UIFakeStore::IsShowingDialog()
extern "C"  bool UIFakeStore_IsShowingDialog_m1285492143 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		DialogRequest_t2092195449 * L_0 = __this->get_m_CurrentDialog_27();
		V_0 = (bool)((!(((Il2CppObject*)(DialogRequest_t2092195449 *)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_0()
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Eb__16_0_m16729514 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		__this->set_m_CurrentDialog_27((DialogRequest_t2092195449 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_1()
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Eb__16_1_m16729609 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		UIFakeStore_OkayButtonClicked_m1579250743(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_2()
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Eb__16_2_m16729448 (UIFakeStore_t3684252124 * __this, const MethodInfo* method)
{
	{
		UIFakeStore_CancelButtonClicked_m1822980185(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore::<InstantiateDialog>b__16_3(System.Int32)
extern "C"  void UIFakeStore_U3CInstantiateDialogU3Eb__16_3_m4148963786 (UIFakeStore_t3684252124 * __this, int32_t ___selectedItem0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___selectedItem0;
		UIFakeStore_DropdownValueChanged_m3219846161(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m70001523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m70001523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t3471345421 * L_0 = (U3CU3Ec_t3471345421 *)il2cpp_codegen_object_new(U3CU3Ec_t3471345421_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m2060140322(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t3471345421_StaticFields*)U3CU3Ec_t3471345421_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m2060140322 (U3CU3Ec_t3471345421 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Purchasing.UIFakeStore/<>c::<CreateRetrieveProductsQuestion>b__18_0(UnityEngine.Purchasing.ProductDefinition)
extern "C"  String_t* U3CU3Ec_U3CCreateRetrieveProductsQuestionU3Eb__18_0_m980068044 (U3CU3Ec_t3471345421 * __this, ProductDefinition_t1942475268 * ___pid0, const MethodInfo* method)
{
	{
		ProductDefinition_t1942475268 * L_0 = ___pid0;
		NullCheck(L_0);
		String_t* L_1 = ProductDefinition_get_id_m264072292(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/DialogRequest::.ctor()
extern "C"  void DialogRequest__ctor_m3883507536 (DialogRequest_t2092195449 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier::OnDestroy()
extern "C"  void LifecycleNotifier_OnDestroy_m3855820152 (LifecycleNotifier_t1057582876 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Action_t3226471752 * L_0 = __this->get_OnDestroyCallback_2();
		V_0 = (bool)((!(((Il2CppObject*)(Action_t3226471752 *)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Action_t3226471752 * L_2 = __this->get_OnDestroyCallback_2();
		NullCheck(L_2);
		Action_Invoke_m3801112262(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UIFakeStore/LifecycleNotifier::.ctor()
extern "C"  void LifecycleNotifier__ctor_m1493596751 (LifecycleNotifier_t1057582876 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnifiedReceipt::.ctor()
extern "C"  void UnifiedReceipt__ctor_m2939574242 (UnifiedReceipt_t2654419430 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::Purchase(System.String,System.Action`2<System.Boolean,System.String>,System.String)
extern "C"  void UnityChannelBindings_Purchase_m1136380361 (UnityChannelBindings_t2880355556 * __this, String_t* ___productId0, Action_2_t1865222972 * ___callback1, String_t* ___developerPayload2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_Purchase_m1136380361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Guid_t  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Action_2_t1865222972 * L_0 = ___callback1;
		V_0 = (bool)((((Il2CppObject*)(Action_2_t1865222972 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		goto IL_0058;
	}

IL_000c:
	{
		Action_2_t1865222972 * L_2 = __this->get_m_PurchaseCallback_0();
		V_1 = (bool)((!(((Il2CppObject*)(Action_2_t1865222972 *)L_2) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		Action_2_t1865222972 * L_4 = ___callback1;
		NullCheck(L_4);
		Action_2_Invoke_m547817495(L_4, (bool)0, _stringLiteral367725214, /*hidden argument*/Action_2_Invoke_m547817495_MethodInfo_var);
		goto IL_0058;
	}

IL_0029:
	{
		Action_2_t1865222972 * L_5 = ___callback1;
		__this->set_m_PurchaseCallback_0(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_6 = Guid_NewGuid_m3493657620(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_6;
		String_t* L_7 = Guid_ToString_m3927110175((&V_2), /*hidden argument*/NULL);
		__this->set_m_PurchaseGuid_1(L_7);
		String_t* L_8 = ___productId0;
		String_t* L_9 = __this->get_m_PurchaseGuid_1();
		String_t* L_10 = ___developerPayload2;
		IL2CPP_RUNTIME_CLASS_INIT(PurchaseService_t659182236_il2cpp_TypeInfo_var);
		PurchaseService_Purchase_m3636124256(NULL /*static, unused*/, L_8, L_9, __this, L_10, /*hidden argument*/NULL);
	}

IL_0058:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>)
extern "C"  void UnityChannelBindings_RetrieveProducts_m1095305944 (UnityChannelBindings_t2880355556 * __this, ReadOnlyCollection_1_t2128260960 * ___products0, Action_2_t1865222972 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_RetrieveProducts_m1095305944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HashSet_1_t1651728377 * V_0 = NULL;
	ProductCatalog_t2667590766 * V_1 = NULL;
	String_t* V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	ProductCatalogItem_t977711995 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	ProductDefinition_t1942475268 * V_6 = NULL;
	bool V_7 = false;
	int32_t V_8 = 0;
	String_t* V_9 = NULL;
	LocalizedProductDescription_t1525635964 * V_10 = NULL;
	LocalizedProductDescription_t1525635964 * V_11 = NULL;
	ProductMetadata_t1573242544 * V_12 = NULL;
	ProductDescription_t3318267523 * V_13 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	LocalizedProductDescription_t1525635964 * G_B7_0 = NULL;
	LocalizedProductDescription_t1525635964 * G_B6_0 = NULL;
	{
		HashSet_1_t1651728377 * L_0 = (HashSet_1_t1651728377 *)il2cpp_codegen_object_new(HashSet_1_t1651728377_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m738744506(L_0, /*hidden argument*/HashSet_1__ctor_m738744506_MethodInfo_var);
		V_0 = L_0;
		ProductCatalog_t2667590766 * L_1 = ProductCatalog_LoadDefaultCatalog_m589345558(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		ProductCatalog_t2667590766 * L_2 = V_1;
		NullCheck(L_2);
		Il2CppObject* L_3 = ProductCatalog_get_allValidProducts_m1766704888(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.ProductCatalogItem>::GetEnumerator() */, IEnumerable_1_t1269839040_il2cpp_TypeInfo_var, L_3);
		V_3 = L_4;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f4;
		}

IL_001f:
		{
			Il2CppObject* L_5 = V_3;
			NullCheck(L_5);
			ProductCatalogItem_t977711995 * L_6 = InterfaceFuncInvoker0< ProductCatalogItem_t977711995 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductCatalogItem>::get_Current() */, IEnumerator_1_t2748203118_il2cpp_TypeInfo_var, L_5);
			V_4 = L_6;
			ReadOnlyCollection_1_t2128260960 * L_7 = ___products0;
			NullCheck(L_7);
			Il2CppObject* L_8 = ReadOnlyCollection_1_GetEnumerator_m178725564(L_7, /*hidden argument*/ReadOnlyCollection_1_GetEnumerator_m178725564_MethodInfo_var);
			V_5 = L_8;
		}

IL_0031:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00d8;
			}

IL_0036:
			{
				Il2CppObject* L_9 = V_5;
				NullCheck(L_9);
				ProductDefinition_t1942475268 * L_10 = InterfaceFuncInvoker0< ProductDefinition_t1942475268 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Purchasing.ProductDefinition>::get_Current() */, IEnumerator_1_t3712966391_il2cpp_TypeInfo_var, L_9);
				V_6 = L_10;
				ProductCatalogItem_t977711995 * L_11 = V_4;
				NullCheck(L_11);
				String_t* L_12 = L_11->get_id_0();
				ProductDefinition_t1942475268 * L_13 = V_6;
				NullCheck(L_13);
				String_t* L_14 = ProductDefinition_get_id_m264072292(L_13, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				bool L_15 = String_Equals_m3568148125(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
				V_7 = L_15;
				bool L_16 = V_7;
				if (!L_16)
				{
					goto IL_00d7;
				}
			}

IL_0059:
			{
				IL2CPP_RUNTIME_CLASS_INIT(XiaomiPriceTiers_t2494450221_il2cpp_TypeInfo_var);
				Int32U5BU5D_t3030399641* L_17 = ((XiaomiPriceTiers_t2494450221_StaticFields*)XiaomiPriceTiers_t2494450221_il2cpp_TypeInfo_var->static_fields)->get_XiaomiPriceTierPrices_0();
				ProductCatalogItem_t977711995 * L_18 = V_4;
				NullCheck(L_18);
				int32_t L_19 = L_18->get_xiaomiPriceTier_5();
				NullCheck(L_17);
				int32_t L_20 = L_19;
				int32_t L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
				V_8 = L_21;
				int32_t L_22 = V_8;
				int32_t L_23 = L_22;
				Il2CppObject * L_24 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_23);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_25 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2309149295, L_24, /*hidden argument*/NULL);
				V_9 = L_25;
				ProductCatalogItem_t977711995 * L_26 = V_4;
				NullCheck(L_26);
				LocalizedProductDescription_t1525635964 * L_27 = L_26->get_defaultDescription_3();
				V_10 = L_27;
				ProductCatalogItem_t977711995 * L_28 = V_4;
				NullCheck(L_28);
				LocalizedProductDescription_t1525635964 * L_29 = ProductCatalogItem_GetDescription_m2643762087(L_28, ((int32_t)19), /*hidden argument*/NULL);
				V_11 = L_29;
				LocalizedProductDescription_t1525635964 * L_30 = V_11;
				LocalizedProductDescription_t1525635964 * L_31 = L_30;
				G_B6_0 = L_31;
				if (L_31)
				{
					G_B7_0 = L_31;
					goto IL_0098;
				}
			}

IL_0095:
			{
				LocalizedProductDescription_t1525635964 * L_32 = V_10;
				G_B7_0 = L_32;
			}

IL_0098:
			{
				V_10 = G_B7_0;
				String_t* L_33 = V_9;
				LocalizedProductDescription_t1525635964 * L_34 = V_10;
				NullCheck(L_34);
				String_t* L_35 = LocalizedProductDescription_get_Title_m3689615546(L_34, /*hidden argument*/NULL);
				LocalizedProductDescription_t1525635964 * L_36 = V_10;
				NullCheck(L_36);
				String_t* L_37 = LocalizedProductDescription_get_Description_m943727562(L_36, /*hidden argument*/NULL);
				int32_t L_38 = V_8;
				IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
				Decimal_t724701077  L_39 = Decimal_op_Implicit_m3312726949(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
				ProductMetadata_t1573242544 * L_40 = (ProductMetadata_t1573242544 *)il2cpp_codegen_object_new(ProductMetadata_t1573242544_il2cpp_TypeInfo_var);
				ProductMetadata__ctor_m3648829555(L_40, L_33, L_35, L_37, _stringLiteral1643762102, L_39, /*hidden argument*/NULL);
				V_12 = L_40;
				ProductDefinition_t1942475268 * L_41 = V_6;
				NullCheck(L_41);
				String_t* L_42 = ProductDefinition_get_storeSpecificId_m2251287741(L_41, /*hidden argument*/NULL);
				ProductMetadata_t1573242544 * L_43 = V_12;
				ProductDescription_t3318267523 * L_44 = (ProductDescription_t3318267523 *)il2cpp_codegen_object_new(ProductDescription_t3318267523_il2cpp_TypeInfo_var);
				ProductDescription__ctor_m2055358744(L_44, L_42, L_43, /*hidden argument*/NULL);
				V_13 = L_44;
				HashSet_1_t1651728377 * L_45 = V_0;
				ProductDescription_t3318267523 * L_46 = V_13;
				NullCheck(L_45);
				HashSet_1_Add_m1190086556(L_45, L_46, /*hidden argument*/HashSet_1_Add_m1190086556_MethodInfo_var);
			}

IL_00d7:
			{
			}

IL_00d8:
			{
				Il2CppObject* L_47 = V_5;
				NullCheck(L_47);
				bool L_48 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_47);
				if (L_48)
				{
					goto IL_0036;
				}
			}

IL_00e4:
			{
				IL2CPP_LEAVE(0xF3, FINALLY_00e6);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_00e6;
		}

FINALLY_00e6:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_49 = V_5;
				if (!L_49)
				{
					goto IL_00f2;
				}
			}

IL_00ea:
			{
				Il2CppObject* L_50 = V_5;
				NullCheck(L_50);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_50);
			}

IL_00f2:
			{
				IL2CPP_END_FINALLY(230)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(230)
		{
			IL2CPP_JUMP_TBL(0xF3, IL_00f3)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_00f3:
		{
		}

IL_00f4:
		{
			Il2CppObject* L_51 = V_3;
			NullCheck(L_51);
			bool L_52 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_51);
			if (L_52)
			{
				goto IL_001f;
			}
		}

IL_00ff:
		{
			IL2CPP_LEAVE(0x10C, FINALLY_0101);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0101;
	}

FINALLY_0101:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_53 = V_3;
			if (!L_53)
			{
				goto IL_010b;
			}
		}

IL_0104:
		{
			Il2CppObject* L_54 = V_3;
			NullCheck(L_54);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_54);
		}

IL_010b:
		{
			IL2CPP_END_FINALLY(257)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(257)
	{
		IL2CPP_JUMP_TBL(0x10C, IL_010c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_010c:
	{
		HashSet_1_t1651728377 * L_55 = V_0;
		String_t* L_56 = JSONSerializer_SerializeProductDescs_m582436362(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		V_2 = L_56;
		Action_2_t1865222972 * L_57 = ___callback1;
		String_t* L_58 = V_2;
		NullCheck(L_57);
		Action_2_Invoke_m547817495(L_57, (bool)1, L_58, /*hidden argument*/Action_2_Invoke_m547817495_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void UnityChannelBindings_ValidateReceipt_m3411439973 (UnityChannelBindings_t2880355556 * __this, String_t* ___transactionId0, Action_3_t3864773326 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_ValidateReceipt_m3411439973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass16_0_t1107960729 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass16_0_t1107960729 * L_0 = (U3CU3Ec__DisplayClass16_0_t1107960729 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass16_0_t1107960729_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass16_0__ctor_m1806464238(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass16_0_t1107960729 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_1(__this);
		U3CU3Ec__DisplayClass16_0_t1107960729 * L_2 = V_0;
		String_t* L_3 = ___transactionId0;
		NullCheck(L_2);
		L_2->set_transactionId_0(L_3);
		U3CU3Ec__DisplayClass16_0_t1107960729 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_transactionId_0();
		Action_3_t3864773326 * L_6 = ___callback1;
		Dictionary_2_t853706424 * L_7 = __this->get_m_ValidateCallbacks_2();
		U3CU3Ec__DisplayClass16_0_t1107960729 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass16_0_U3CValidateReceiptU3Eb__0_m573116822_MethodInfo_var);
		Action_t3226471752 * L_10 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_10, L_8, L_9, /*hidden argument*/NULL);
		UnityChannelBindings_RequestUniquely_m1439297133(__this, L_5, L_6, L_7, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RequestUniquely(System.String,System.Action`3<System.Boolean,System.String,System.String>,System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>,System.Action)
extern "C"  void UnityChannelBindings_RequestUniquely_m1439297133 (UnityChannelBindings_t2880355556 * __this, String_t* ___transactionId0, Action_3_t3864773326 * ___callback1, Dictionary_2_t853706424 * ___callbackDictionary2, Action_t3226471752 * ___requestAction3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_RequestUniquely_m1439297133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Action_3_t3864773326 * L_0 = ___callback1;
		V_0 = (bool)((((Il2CppObject*)(Action_3_t3864773326 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		goto IL_0066;
	}

IL_000c:
	{
		String_t* L_2 = ___transactionId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		Action_3_t3864773326 * L_5 = ___callback1;
		NullCheck(L_5);
		Action_3_Invoke_m304290708(L_5, (bool)0, _stringLiteral3528679085, (String_t*)NULL, /*hidden argument*/Action_3_Invoke_m304290708_MethodInfo_var);
		goto IL_0066;
	}

IL_0027:
	{
		Dictionary_2_t853706424 * L_6 = ___callbackDictionary2;
		String_t* L_7 = ___transactionId0;
		NullCheck(L_6);
		bool L_8 = Dictionary_2_ContainsKey_m661064896(L_6, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m661064896_MethodInfo_var);
		V_2 = (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_2;
		if (!L_9)
		{
			goto IL_0056;
		}
	}
	{
		Dictionary_2_t853706424 * L_10 = ___callbackDictionary2;
		String_t* L_11 = ___transactionId0;
		List_1_t3233894458 * L_12 = (List_1_t3233894458 *)il2cpp_codegen_object_new(List_1_t3233894458_il2cpp_TypeInfo_var);
		List_1__ctor_m1828621836(L_12, /*hidden argument*/List_1__ctor_m1828621836_MethodInfo_var);
		List_1_t3233894458 * L_13 = L_12;
		Action_3_t3864773326 * L_14 = ___callback1;
		NullCheck(L_13);
		List_1_Add_m2269440416(L_13, L_14, /*hidden argument*/List_1_Add_m2269440416_MethodInfo_var);
		NullCheck(L_10);
		Dictionary_2_Add_m1256657483(L_10, L_11, L_13, /*hidden argument*/Dictionary_2_Add_m1256657483_MethodInfo_var);
		Action_t3226471752 * L_15 = ___requestAction3;
		NullCheck(L_15);
		Action_Invoke_m3801112262(L_15, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0056:
	{
		Dictionary_2_t853706424 * L_16 = ___callbackDictionary2;
		String_t* L_17 = ___transactionId0;
		NullCheck(L_16);
		List_1_t3233894458 * L_18 = Dictionary_2_get_Item_m598032533(L_16, L_17, /*hidden argument*/Dictionary_2_get_Item_m598032533_MethodInfo_var);
		Action_3_t3864773326 * L_19 = ___callback1;
		NullCheck(L_18);
		List_1_Add_m2269440416(L_18, L_19, /*hidden argument*/List_1_Add_m2269440416_MethodInfo_var);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::RetrieveProducts(System.String)
extern "C"  void UnityChannelBindings_RetrieveProducts_m2334981462 (UnityChannelBindings_t2880355556 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_RetrieveProducts_m2334981462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::Purchase(System.String,System.String)
extern "C"  void UnityChannelBindings_Purchase_m2162196425 (UnityChannelBindings_t2880355556 * __this, String_t* ___productJSON0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_Purchase_m2162196425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::FinishTransaction(System.String,System.String)
extern "C"  void UnityChannelBindings_FinishTransaction_m1363685441 (UnityChannelBindings_t2880355556 * __this, String_t* ___productJSON0, String_t* ___transactionID1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings_FinishTransaction_m1363685441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings::.ctor()
extern "C"  void UnityChannelBindings__ctor_m3449949178 (UnityChannelBindings_t2880355556 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelBindings__ctor_m3449949178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t853706424 * L_0 = (Dictionary_2_t853706424 *)il2cpp_codegen_object_new(Dictionary_2_t853706424_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2524193507(L_0, /*hidden argument*/Dictionary_2__ctor_m2524193507_MethodInfo_var);
		__this->set_m_ValidateCallbacks_2(L_0);
		Dictionary_2_t853706424 * L_1 = (Dictionary_2_t853706424 *)il2cpp_codegen_object_new(Dictionary_2_t853706424_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2524193507(L_1, /*hidden argument*/Dictionary_2__ctor_m2524193507_MethodInfo_var);
		__this->set_m_PurchaseConfirmCallbacks_3(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m1806464238 (U3CU3Ec__DisplayClass16_0_t1107960729 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelBindings/<>c__DisplayClass16_0::<ValidateReceipt>b__0()
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CValidateReceiptU3Eb__0_m573116822 (U3CU3Ec__DisplayClass16_0_t1107960729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass16_0_U3CValidateReceiptU3Eb__0_m573116822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_transactionId_0();
		UnityChannelBindings_t2880355556 * L_1 = __this->get_U3CU3E4__this_1();
		IL2CPP_RUNTIME_CLASS_INIT(PurchaseService_t659182236_il2cpp_TypeInfo_var);
		PurchaseService_ValidateReceipt_m2412817733(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::.ctor()
extern "C"  void UnityChannelImpl__ctor_m3372672074 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelImpl__ctor_m3372672074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_LastPurchaseError_23(_stringLiteral371857150);
		JSONStore__ctor_m1861746843(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::SetNativeStore(UnityEngine.Purchasing.INativeUnityChannelStore)
extern "C"  void UnityChannelImpl_SetNativeStore_m3720432 (UnityChannelImpl_t1327714682 * __this, Il2CppObject * ___unityChannelBindings0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___unityChannelBindings0;
		JSONStore_SetNativeStore_m574687663(__this, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___unityChannelBindings0;
		__this->set_m_Bindings_22(L_1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  void UnityChannelImpl_RetrieveProducts_m86248098 (UnityChannelImpl_t1327714682 * __this, ReadOnlyCollection_1_t2128260960 * ___products0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelImpl_RetrieveProducts_m86248098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_Bindings_22();
		ReadOnlyCollection_1_t2128260960 * L_1 = ___products0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UnityChannelImpl_U3CRetrieveProductsU3Eb__6_0_m3443241054_MethodInfo_var);
		Action_2_t1865222972 * L_3 = (Action_2_t1865222972 *)il2cpp_codegen_object_new(Action_2_t1865222972_il2cpp_TypeInfo_var);
		Action_2__ctor_m759102168(L_3, __this, L_2, /*hidden argument*/Action_2__ctor_m759102168_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker2< ReadOnlyCollection_1_t2128260960 *, Action_2_t1865222972 * >::Invoke(1 /* System.Void UnityEngine.Purchasing.INativeUnityChannelStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>,System.Action`2<System.Boolean,System.String>) */, INativeUnityChannelStore_t670691399_il2cpp_TypeInfo_var, L_0, L_1, L_3);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void UnityChannelImpl_Purchase_m1906667446 (UnityChannelImpl_t1327714682 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelImpl_Purchase_m1906667446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass7_0_t505099 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass7_0_t505099 * L_0 = (U3CU3Ec__DisplayClass7_0_t505099 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass7_0_t505099_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass7_0__ctor_m2464233274(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass7_0_t505099 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_1(__this);
		U3CU3Ec__DisplayClass7_0_t505099 * L_2 = V_0;
		ProductDefinition_t1942475268 * L_3 = ___product0;
		NullCheck(L_2);
		L_2->set_product_0(L_3);
		Il2CppObject * L_4 = __this->get_m_Bindings_22();
		U3CU3Ec__DisplayClass7_0_t505099 * L_5 = V_0;
		NullCheck(L_5);
		ProductDefinition_t1942475268 * L_6 = L_5->get_product_0();
		NullCheck(L_6);
		String_t* L_7 = ProductDefinition_get_storeSpecificId_m2251287741(L_6, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass7_0_t505099 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass7_0_U3CPurchaseU3Eb__0_m2251081206_MethodInfo_var);
		Action_2_t1865222972 * L_10 = (Action_2_t1865222972 *)il2cpp_codegen_object_new(Action_2_t1865222972_il2cpp_TypeInfo_var);
		Action_2__ctor_m759102168(L_10, L_8, L_9, /*hidden argument*/Action_2__ctor_m759102168_MethodInfo_var);
		String_t* L_11 = ___developerPayload1;
		NullCheck(L_4);
		InterfaceActionInvoker3< String_t*, Action_2_t1865222972 *, String_t* >::Invoke(0 /* System.Void UnityEngine.Purchasing.INativeUnityChannelStore::Purchase(System.String,System.Action`2<System.Boolean,System.String>,System.String) */, INativeUnityChannelStore_t670691399_il2cpp_TypeInfo_var, L_4, L_7, L_10, L_11);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::extractDeveloperPayload(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  void UnityChannelImpl_extractDeveloperPayload_m1881438288 (UnityChannelImpl_t1327714682 * __this, Dictionary_2_t309261261 * ___dic0, String_t* ___signData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelImpl_extractDeveloperPayload_m1881438288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	String_t* V_1 = NULL;
	Dictionary_2_t309261261 * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	{
		String_t* L_0 = ___signData1;
		Il2CppObject * L_1 = MiniJson_JsonDecode_m947751129(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t309261261 *)CastclassClass(L_1, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = Dictionary_2_ContainsKey_m1533770720(L_2, _stringLiteral2595553265, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		V_3 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_3;
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_007e;
	}

IL_0021:
	{
		Dictionary_2_t309261261 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = Dictionary_2_get_Item_m464793699(L_5, _stringLiteral2595553265, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		V_1 = ((String_t*)CastclassSealed(L_6, String_t_il2cpp_TypeInfo_var));
		String_t* L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_4 = L_8;
		bool L_9 = V_4;
		if (!L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_007e;
	}

IL_0040:
	{
		String_t* L_10 = V_1;
		Il2CppObject * L_11 = MiniJson_JsonDecode_m947751129(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_2 = ((Dictionary_2_t309261261 *)CastclassClass(L_11, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
		Dictionary_2_t309261261 * L_12 = V_2;
		NullCheck(L_12);
		bool L_13 = Dictionary_2_ContainsKey_m1533770720(L_12, _stringLiteral3595395586, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		V_5 = (bool)((((int32_t)L_13) == ((int32_t)0))? 1 : 0);
		bool L_14 = V_5;
		if (!L_14)
		{
			goto IL_0062;
		}
	}
	{
		goto IL_007e;
	}

IL_0062:
	{
		Dictionary_2_t309261261 * L_15 = ___dic0;
		Dictionary_2_t309261261 * L_16 = V_2;
		NullCheck(L_16);
		Il2CppObject * L_17 = Dictionary_2_get_Item_m464793699(L_16, _stringLiteral3595395586, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		NullCheck(L_15);
		Dictionary_2_set_Item_m4132139590(L_15, _stringLiteral4271656310, ((String_t*)CastclassSealed(L_17, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
	}

IL_007e:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void UnityChannelImpl_FinishTransaction_m1796617110 (UnityChannelImpl_t1327714682 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::get_fetchReceiptPayloadOnPurchase()
extern "C"  bool UnityChannelImpl_get_fetchReceiptPayloadOnPurchase_m154957409 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24();
		return L_0;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::set_fetchReceiptPayloadOnPurchase(System.Boolean)
extern "C"  void UnityChannelImpl_set_fetchReceiptPayloadOnPurchase_m826422872 (UnityChannelImpl_t1327714682 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void UnityChannelImpl_ValidateReceipt_m2670145539 (UnityChannelImpl_t1327714682 * __this, String_t* ___transactionIdentifier0, Action_3_t3864773326 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityChannelImpl_ValidateReceipt_m2670145539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_m_Bindings_22();
		String_t* L_1 = ___transactionIdentifier0;
		Action_3_t3864773326 * L_2 = ___callback1;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, Action_3_t3864773326 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.INativeUnityChannelStore::ValidateReceipt(System.String,System.Action`3<System.Boolean,System.String,System.String>) */, INativeUnityChannelStore_t670691399_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.String UnityEngine.Purchasing.UnityChannelImpl::GetLastPurchaseError()
extern "C"  String_t* UnityChannelImpl_GetLastPurchaseError_m3414529960 (UnityChannelImpl_t1327714682 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_LastPurchaseError_23();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl::<RetrieveProducts>b__6_0(System.Boolean,System.String)
extern "C"  void UnityChannelImpl_U3CRetrieveProductsU3Eb__6_0_m3443241054 (UnityChannelImpl_t1327714682 * __this, bool ___result0, String_t* ___json1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___json1;
		VirtActionInvoker1< String_t* >::Invoke(17 /* System.Void UnityEngine.Purchasing.JSONStore::OnProductsRetrieved(System.String) */, __this, L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m2464233274 (U3CU3Ec__DisplayClass7_0_t505099 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0::<Purchase>b__0(System.Boolean,System.String)
extern "C"  void U3CU3Ec__DisplayClass7_0_U3CPurchaseU3Eb__0_m2251081206 (U3CU3Ec__DisplayClass7_0_t505099 * __this, bool ___purchaseSuccess0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass7_0_U3CPurchaseU3Eb__0_m2251081206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	U3CU3Ec__DisplayClass7_1_t2729388454 * V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	String_t* V_7 = NULL;
	int32_t V_8 = 0;
	String_t* V_9 = NULL;
	Dictionary_2_t309261261 * V_10 = NULL;
	String_t* V_11 = NULL;
	bool V_12 = false;
	Dictionary_2_t309261261 * V_13 = NULL;
	String_t* V_14 = NULL;
	PurchaseFailureDescription_t1607114611 * V_15 = NULL;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	{
		UnityChannelImpl_t1327714682 * L_0 = __this->get_U3CU3E4__this_1();
		NullCheck(L_0);
		L_0->set_m_LastPurchaseError_23(_stringLiteral371857150);
		bool L_1 = ___purchaseSuccess0;
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_015d;
		}
	}
	{
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_3 = (U3CU3Ec__DisplayClass7_1_t2729388454 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass7_1_t2729388454_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass7_1__ctor_m2463119227(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_4 = V_1;
		NullCheck(L_4);
		L_4->set_CSU24U3CU3E8__locals1_2(__this);
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_5 = V_1;
		String_t* L_6 = ___message1;
		Dictionary_2_t309261261 * L_7 = MiniJsonExtensions_HashtableFromJson_m374830805(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_dic_0(L_7);
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_8 = V_1;
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_t309261261 * L_10 = L_9->get_dic_0();
		String_t* L_11 = MiniJsonExtensions_GetString_m2316350864(NULL /*static, unused*/, L_10, _stringLiteral3112617933, _stringLiteral371857150, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_transactionId_1(L_11);
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_12 = V_1;
		NullCheck(L_12);
		Dictionary_2_t309261261 * L_13 = L_12->get_dic_0();
		String_t* L_14 = MiniJsonExtensions_GetString_m2316350864(NULL /*static, unused*/, L_13, _stringLiteral2651994212, _stringLiteral371857150, /*hidden argument*/NULL);
		V_2 = L_14;
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_15 = V_1;
		NullCheck(L_15);
		String_t* L_16 = L_15->get_transactionId_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_3;
		if (!L_18)
		{
			goto IL_008f;
		}
	}
	{
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_19 = V_1;
		NullCheck(L_19);
		Dictionary_2_t309261261 * L_20 = L_19->get_dic_0();
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_21 = V_1;
		NullCheck(L_21);
		String_t* L_22 = L_21->get_transactionId_1();
		NullCheck(L_20);
		Dictionary_2_set_Item_m4132139590(L_20, _stringLiteral1038890517, L_22, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
	}

IL_008f:
	{
		String_t* L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_24) == ((int32_t)0))? 1 : 0);
		bool L_25 = V_4;
		if (!L_25)
		{
			goto IL_00b2;
		}
	}
	{
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_26 = V_1;
		NullCheck(L_26);
		Dictionary_2_t309261261 * L_27 = L_26->get_dic_0();
		String_t* L_28 = V_2;
		NullCheck(L_27);
		Dictionary_2_set_Item_m4132139590(L_27, _stringLiteral667782618, L_28, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
	}

IL_00b2:
	{
		ProductDefinition_t1942475268 * L_29 = __this->get_product_0();
		NullCheck(L_29);
		String_t* L_30 = ProductDefinition_get_storeSpecificId_m2251287741(L_29, /*hidden argument*/NULL);
		String_t* L_31 = V_2;
		NullCheck(L_30);
		bool L_32 = String_Equals_m2633592423(L_30, L_31, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_32) == ((int32_t)0))? 1 : 0);
		bool L_33 = V_5;
		if (!L_33)
		{
			goto IL_00f1;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_34 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		ProductDefinition_t1942475268 * L_35 = __this->get_product_0();
		NullCheck(L_35);
		String_t* L_36 = ProductDefinition_get_storeSpecificId_m2251287741(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, L_36);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_36);
		ObjectU5BU5D_t3614634134* L_37 = L_34;
		String_t* L_38 = V_2;
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_38);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_38);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarningFormat_m2130157695(NULL /*static, unused*/, _stringLiteral3887667925, L_37, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		UnityChannelImpl_t1327714682 * L_39 = __this->get_U3CU3E4__this_1();
		NullCheck(L_39);
		bool L_40 = UnityChannelImpl_get_fetchReceiptPayloadOnPurchase_m154957409(L_39, /*hidden argument*/NULL);
		V_6 = L_40;
		bool L_41 = V_6;
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		UnityChannelImpl_t1327714682 * L_42 = __this->get_U3CU3E4__this_1();
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_43 = V_1;
		NullCheck(L_43);
		String_t* L_44 = L_43->get_transactionId_1();
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_45 = V_1;
		IntPtr_t L_46;
		L_46.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass7_1_U3CPurchaseU3Eb__1_m1821532482_MethodInfo_var);
		Action_3_t3864773326 * L_47 = (Action_3_t3864773326 *)il2cpp_codegen_object_new(Action_3_t3864773326_il2cpp_TypeInfo_var);
		Action_3__ctor_m3578948342(L_47, L_45, L_46, /*hidden argument*/Action_3__ctor_m3578948342_MethodInfo_var);
		NullCheck(L_42);
		UnityChannelImpl_ValidateReceipt_m2670145539(L_42, L_44, L_47, /*hidden argument*/NULL);
		goto IL_0157;
	}

IL_0124:
	{
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_48 = V_1;
		NullCheck(L_48);
		Dictionary_2_t309261261 * L_49 = L_48->get_dic_0();
		String_t* L_50 = MiniJsonExtensions_toJson_m2405339524(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		V_7 = L_50;
		UnityChannelImpl_t1327714682 * L_51 = __this->get_U3CU3E4__this_1();
		NullCheck(L_51);
		Il2CppObject * L_52 = ((JSONStore_t1890359403 *)L_51)->get_unity_1();
		ProductDefinition_t1942475268 * L_53 = __this->get_product_0();
		NullCheck(L_53);
		String_t* L_54 = ProductDefinition_get_storeSpecificId_m2251287741(L_53, /*hidden argument*/NULL);
		String_t* L_55 = V_7;
		U3CU3Ec__DisplayClass7_1_t2729388454 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = L_56->get_transactionId_1();
		NullCheck(L_52);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseSucceeded(System.String,System.String,System.String) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_52, L_54, L_55, L_57);
	}

IL_0157:
	{
		goto IL_0277;
	}

IL_015d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_58 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PurchaseFailureReason_t1322959839_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_59 = Enum_Parse_m2561000069(NULL /*static, unused*/, L_58, _stringLiteral2846264340, /*hidden argument*/NULL);
		V_8 = ((*(int32_t*)((int32_t*)UnBox(L_59, PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var))));
		Il2CppObject * L_60 = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, (&V_8));
		NullCheck(L_60);
		String_t* L_61 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_9 = L_61;
		String_t* L_62 = ___message1;
		Dictionary_2_t309261261 * L_63 = MiniJsonExtensions_HashtableFromJson_m374830805(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		V_10 = L_63;
		Dictionary_2_t309261261 * L_64 = V_10;
		String_t* L_65 = MiniJsonExtensions_GetString_m2316350864(NULL /*static, unused*/, L_64, _stringLiteral702192217, _stringLiteral371857150, /*hidden argument*/NULL);
		V_11 = L_65;
		String_t* L_66 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t3825574718_il2cpp_TypeInfo_var);
		Boolean_TryParse_m3918169608(NULL /*static, unused*/, L_66, (&V_12), /*hidden argument*/NULL);
		bool L_67 = V_12;
		V_16 = L_67;
		bool L_68 = V_16;
		if (!L_68)
		{
			goto IL_01f5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_69 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PurchaseFailureReason_t1322959839_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		bool L_70 = Enum_IsDefined_m92789062(NULL /*static, unused*/, L_69, _stringLiteral466964775, /*hidden argument*/NULL);
		V_17 = L_70;
		bool L_71 = V_17;
		if (!L_71)
		{
			goto IL_01ed;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_72 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PurchaseFailureReason_t1322959839_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_73 = Enum_Parse_m2561000069(NULL /*static, unused*/, L_72, _stringLiteral466964775, /*hidden argument*/NULL);
		V_8 = ((*(int32_t*)((int32_t*)UnBox(L_73, PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var))));
	}

IL_01ed:
	{
		V_9 = _stringLiteral466964775;
	}

IL_01f5:
	{
		Dictionary_2_t309261261 * L_74 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3188644741(L_74, /*hidden argument*/Dictionary_2__ctor_m3188644741_MethodInfo_var);
		V_13 = L_74;
		Dictionary_2_t309261261 * L_75 = V_13;
		String_t* L_76 = V_9;
		NullCheck(L_75);
		Dictionary_2_set_Item_m4132139590(L_75, _stringLiteral22409400, L_76, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		Dictionary_2_t309261261 * L_77 = V_10;
		NullCheck(L_77);
		bool L_78 = Dictionary_2_ContainsKey_m1533770720(L_77, _stringLiteral2397167425, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		V_18 = L_78;
		bool L_79 = V_18;
		if (!L_79)
		{
			goto IL_0238;
		}
	}
	{
		Dictionary_2_t309261261 * L_80 = V_13;
		Dictionary_2_t309261261 * L_81 = V_10;
		NullCheck(L_81);
		Il2CppObject * L_82 = Dictionary_2_get_Item_m464793699(L_81, _stringLiteral2397167425, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		NullCheck(L_80);
		Dictionary_2_set_Item_m4132139590(L_80, _stringLiteral2397167425, L_82, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
	}

IL_0238:
	{
		Dictionary_2_t309261261 * L_83 = V_13;
		String_t* L_84 = MiniJsonExtensions_toJson_m2405339524(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		V_14 = L_84;
		UnityChannelImpl_t1327714682 * L_85 = __this->get_U3CU3E4__this_1();
		String_t* L_86 = V_14;
		NullCheck(L_85);
		L_85->set_m_LastPurchaseError_23(L_86);
		ProductDefinition_t1942475268 * L_87 = __this->get_product_0();
		NullCheck(L_87);
		String_t* L_88 = ProductDefinition_get_storeSpecificId_m2251287741(L_87, /*hidden argument*/NULL);
		int32_t L_89 = V_8;
		String_t* L_90 = ___message1;
		PurchaseFailureDescription_t1607114611 * L_91 = (PurchaseFailureDescription_t1607114611 *)il2cpp_codegen_object_new(PurchaseFailureDescription_t1607114611_il2cpp_TypeInfo_var);
		PurchaseFailureDescription__ctor_m381019375(L_91, L_88, L_89, L_90, /*hidden argument*/NULL);
		V_15 = L_91;
		UnityChannelImpl_t1327714682 * L_92 = __this->get_U3CU3E4__this_1();
		NullCheck(L_92);
		Il2CppObject * L_93 = ((JSONStore_t1890359403 *)L_92)->get_unity_1();
		PurchaseFailureDescription_t1607114611 * L_94 = V_15;
		NullCheck(L_93);
		InterfaceActionInvoker1< PurchaseFailureDescription_t1607114611 * >::Invoke(4 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseFailed(UnityEngine.Purchasing.Extension.PurchaseFailureDescription) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_93, L_94);
	}

IL_0277:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_1__ctor_m2463119227 (U3CU3Ec__DisplayClass7_1_t2729388454 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::<Purchase>b__1(System.Boolean,System.String,System.String)
extern "C"  void U3CU3Ec__DisplayClass7_1_U3CPurchaseU3Eb__1_m1821532482 (U3CU3Ec__DisplayClass7_1_t2729388454 * __this, bool ___success0, String_t* ___signData1, String_t* ___signature2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass7_1_U3CPurchaseU3Eb__1_m1821532482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	Dictionary_2_t309261261 * G_B4_2 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	Dictionary_2_t309261261 * G_B3_2 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	Dictionary_2_t309261261 * G_B6_2 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	Dictionary_2_t309261261 * G_B5_2 = NULL;
	{
		bool L_0 = ___success0;
		V_1 = L_0;
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0046;
		}
	}
	{
		Dictionary_2_t309261261 * L_2 = __this->get_dic_0();
		String_t* L_3 = ___signData1;
		NullCheck(L_2);
		Dictionary_2_set_Item_m4132139590(L_2, _stringLiteral2704910744, L_3, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		Dictionary_2_t309261261 * L_4 = __this->get_dic_0();
		String_t* L_5 = ___signature2;
		NullCheck(L_4);
		Dictionary_2_set_Item_m4132139590(L_4, _stringLiteral3867530100, L_5, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		U3CU3Ec__DisplayClass7_0_t505099 * L_6 = __this->get_CSU24U3CU3E8__locals1_2();
		NullCheck(L_6);
		UnityChannelImpl_t1327714682 * L_7 = L_6->get_U3CU3E4__this_1();
		Dictionary_2_t309261261 * L_8 = __this->get_dic_0();
		String_t* L_9 = ___signData1;
		NullCheck(L_7);
		UnityChannelImpl_extractDeveloperPayload_m1881438288(L_7, L_8, L_9, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0046:
	{
		Dictionary_2_t309261261 * L_10 = __this->get_dic_0();
		String_t* L_11 = ___signData1;
		String_t* L_12 = L_11;
		G_B3_0 = L_12;
		G_B3_1 = _stringLiteral2704910744;
		G_B3_2 = L_10;
		if (L_12)
		{
			G_B4_0 = L_12;
			G_B4_1 = _stringLiteral2704910744;
			G_B4_2 = L_10;
			goto IL_005c;
		}
	}
	{
		G_B4_0 = _stringLiteral2442387418;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_005c:
	{
		NullCheck(G_B4_2);
		Dictionary_2_set_Item_m4132139590(G_B4_2, G_B4_1, G_B4_0, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		Dictionary_2_t309261261 * L_13 = __this->get_dic_0();
		String_t* L_14 = ___signature2;
		String_t* L_15 = L_14;
		G_B5_0 = L_15;
		G_B5_1 = _stringLiteral3867530100;
		G_B5_2 = L_13;
		if (L_15)
		{
			G_B6_0 = L_15;
			G_B6_1 = _stringLiteral3867530100;
			G_B6_2 = L_13;
			goto IL_0077;
		}
	}
	{
		G_B6_0 = _stringLiteral2442387418;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		Dictionary_2_set_Item_m4132139590(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		Dictionary_2_t309261261 * L_16 = __this->get_dic_0();
		NullCheck(L_16);
		Dictionary_2_set_Item_m4132139590(L_16, _stringLiteral22409400, _stringLiteral1155927968, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
	}

IL_0094:
	{
		Dictionary_2_t309261261 * L_17 = __this->get_dic_0();
		String_t* L_18 = MiniJsonExtensions_toJson_m2405339524(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		U3CU3Ec__DisplayClass7_0_t505099 * L_19 = __this->get_CSU24U3CU3E8__locals1_2();
		NullCheck(L_19);
		UnityChannelImpl_t1327714682 * L_20 = L_19->get_U3CU3E4__this_1();
		NullCheck(L_20);
		Il2CppObject * L_21 = ((JSONStore_t1890359403 *)L_20)->get_unity_1();
		U3CU3Ec__DisplayClass7_0_t505099 * L_22 = __this->get_CSU24U3CU3E8__locals1_2();
		NullCheck(L_22);
		ProductDefinition_t1942475268 * L_23 = L_22->get_product_0();
		NullCheck(L_23);
		String_t* L_24 = ProductDefinition_get_storeSpecificId_m2251287741(L_23, /*hidden argument*/NULL);
		String_t* L_25 = V_0;
		String_t* L_26 = __this->get_transactionId_1();
		NullCheck(L_21);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.Extension.IStoreCallback::OnPurchaseSucceeded(System.String,System.String,System.String) */, IStoreCallback_t2691517565_il2cpp_TypeInfo_var, L_21, L_24, L_25, L_26);
		return;
	}
}
// System.Void UnityEngine.Purchasing.UnityChannelPurchaseReceipt::.ctor()
extern "C"  void UnityChannelPurchaseReceipt__ctor_m420745419 (UnityChannelPurchaseReceipt_t1964142665 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::.ctor(UnityEngine.Purchasing.Default.IWindowsIAP,Uniject.IUtil,UnityEngine.ILogger)
extern "C"  void WinRTStore__ctor_m3142178002 (WinRTStore_t36043095 * __this, Il2CppObject * ___win80, Il2CppObject * ___util1, Il2CppObject * ___logger2, const MethodInfo* method)
{
	{
		__this->set_m_CanReceivePurchases_4((bool)0);
		AbstractStore__ctor_m212291193(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___win80;
		__this->set_win8_0(L_0);
		Il2CppObject * L_1 = ___util1;
		__this->set_util_2(L_1);
		Il2CppObject * L_2 = ___logger2;
		__this->set_logger_3(L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::SetWindowsIAP(UnityEngine.Purchasing.Default.IWindowsIAP)
extern "C"  void WinRTStore_SetWindowsIAP_m1317379130 (WinRTStore_t36043095 * __this, Il2CppObject * ___iap0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___iap0;
		__this->set_win8_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::Initialize(UnityEngine.Purchasing.Extension.IStoreCallback)
extern "C"  void WinRTStore_Initialize_m2648058662 (WinRTStore_t36043095 * __this, Il2CppObject * ___biller0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___biller0;
		__this->set_callback_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::RetrieveProducts(System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>)
extern "C"  void WinRTStore_RetrieveProducts_m1073973345 (WinRTStore_t36043095 * __this, ReadOnlyCollection_1_t2128260960 * ___productDefs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_RetrieveProducts_m1073973345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Func_2_t460188795 * G_B2_0 = NULL;
	ReadOnlyCollection_1_t2128260960 * G_B2_1 = NULL;
	Func_2_t460188795 * G_B1_0 = NULL;
	ReadOnlyCollection_1_t2128260960 * G_B1_1 = NULL;
	Func_2_t2004692778 * G_B4_0 = NULL;
	Il2CppObject* G_B4_1 = NULL;
	Func_2_t2004692778 * G_B3_0 = NULL;
	Il2CppObject* G_B3_1 = NULL;
	{
		ReadOnlyCollection_1_t2128260960 * L_0 = ___productDefs0;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t4236842726_il2cpp_TypeInfo_var);
		Func_2_t460188795 * L_1 = ((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__8_0_1();
		Func_2_t460188795 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t4236842726_il2cpp_TypeInfo_var);
		U3CU3Ec_t4236842726 * L_3 = ((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_m3748846711_MethodInfo_var);
		Func_2_t460188795 * L_5 = (Func_2_t460188795 *)il2cpp_codegen_object_new(Func_2_t460188795_il2cpp_TypeInfo_var);
		Func_2__ctor_m4202709936(L_5, L_3, L_4, /*hidden argument*/Func_2__ctor_m4202709936_MethodInfo_var);
		Func_2_t460188795 * L_6 = L_5;
		((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__8_0_1(L_6);
		G_B2_0 = L_6;
		G_B2_1 = G_B1_1;
	}

IL_0021:
	{
		Il2CppObject* L_7 = Enumerable_Where_TisProductDefinition_t1942475268_m96461137(NULL /*static, unused*/, G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Where_TisProductDefinition_t1942475268_m96461137_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t4236842726_il2cpp_TypeInfo_var);
		Func_2_t2004692778 * L_8 = ((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9__8_1_2();
		Func_2_t2004692778 * L_9 = L_8;
		G_B3_0 = L_9;
		G_B3_1 = L_7;
		if (L_9)
		{
			G_B4_0 = L_9;
			G_B4_1 = L_7;
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t4236842726_il2cpp_TypeInfo_var);
		U3CU3Ec_t4236842726 * L_10 = ((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->get_U3CU3E9_0();
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m823723958_MethodInfo_var);
		Func_2_t2004692778 * L_12 = (Func_2_t2004692778 *)il2cpp_codegen_object_new(Func_2_t2004692778_il2cpp_TypeInfo_var);
		Func_2__ctor_m432649268(L_12, L_10, L_11, /*hidden argument*/Func_2__ctor_m432649268_MethodInfo_var);
		Func_2_t2004692778 * L_13 = L_12;
		((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9__8_1_2(L_13);
		G_B4_0 = L_13;
		G_B4_1 = G_B3_1;
	}

IL_0045:
	{
		Il2CppObject* L_14 = Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840(NULL /*static, unused*/, G_B4_1, G_B4_0, /*hidden argument*/Enumerable_Select_TisProductDefinition_t1942475268_TisWinProductDescription_t1075111405_m2891196840_MethodInfo_var);
		V_0 = L_14;
		Il2CppObject * L_15 = __this->get_win8_0();
		Il2CppObject* L_16 = V_0;
		List_1_t444232537 * L_17 = Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_ToList_TisWinProductDescription_t1075111405_m820579046_MethodInfo_var);
		NullCheck(L_15);
		InterfaceActionInvoker1< List_1_t444232537 * >::Invoke(0 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::BuildDummyProducts(System.Collections.Generic.List`1<UnityEngine.Purchasing.Default.WinProductDescription>) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_15, L_17);
		WinRTStore_init_m420138384(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::FinishTransaction(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void WinRTStore_FinishTransaction_m1157468965 (WinRTStore_t36043095 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___transactionId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_FinishTransaction_m1157468965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_win8_0();
		String_t* L_1 = ___transactionId1;
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(4 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::FinaliseTransaction(System.String) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::init(System.Int32)
extern "C"  void WinRTStore_init_m420138384 (WinRTStore_t36043095 * __this, int32_t ___delay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_init_m420138384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_win8_0();
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::Initialize(UnityEngine.Purchasing.Default.IWindowsIAPCallback) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_0, __this);
		Il2CppObject * L_1 = __this->get_win8_0();
		NullCheck(L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::RetrieveProducts(System.Boolean) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_1, (bool)1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::Purchase(UnityEngine.Purchasing.ProductDefinition,System.String)
extern "C"  void WinRTStore_Purchase_m27999093 (WinRTStore_t36043095 * __this, ProductDefinition_t1942475268 * ___product0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_Purchase_m27999093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_win8_0();
		ProductDefinition_t1942475268 * L_1 = ___product0;
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_storeSpecificId_m2251287741(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(3 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::Purchase(System.String) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_0, L_2);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::restoreTransactions(System.Boolean)
extern "C"  void WinRTStore_restoreTransactions_m1421431749 (WinRTStore_t36043095 * __this, bool ___pausing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_restoreTransactions_m1421431749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		bool L_0 = ___pausing0;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		bool L_2 = __this->get_m_CanReceivePurchases_4();
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject * L_4 = __this->get_win8_0();
		NullCheck(L_4);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::RetrieveProducts(System.Boolean) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_4, (bool)0);
	}

IL_0023:
	{
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore::RestoreTransactions()
extern "C"  void WinRTStore_RestoreTransactions_m1768243916 (WinRTStore_t36043095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinRTStore_RestoreTransactions_m1768243916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_logger_3();
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UnityEngine.ILogger::Log(System.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, _stringLiteral383104206);
		Il2CppObject * L_1 = __this->get_win8_0();
		NullCheck(L_1);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UnityEngine.Purchasing.Default.IWindowsIAP::RetrieveProducts(System.Boolean) */, IWindowsIAP_t818184396_il2cpp_TypeInfo_var, L_1, (bool)0);
		__this->set_m_CanReceivePurchases_4((bool)1);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore/<>c::.cctor()
extern "C"  void U3CU3Ec__cctor_m2748256470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m2748256470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t4236842726 * L_0 = (U3CU3Ec_t4236842726 *)il2cpp_codegen_object_new(U3CU3Ec_t4236842726_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m161161559(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t4236842726_StaticFields*)U3CU3Ec_t4236842726_il2cpp_TypeInfo_var->static_fields)->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Purchasing.WinRTStore/<>c::.ctor()
extern "C"  void U3CU3Ec__ctor_m161161559 (U3CU3Ec_t4236842726 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Purchasing.WinRTStore/<>c::<RetrieveProducts>b__8_0(UnityEngine.Purchasing.ProductDefinition)
extern "C"  bool U3CU3Ec_U3CRetrieveProductsU3Eb__8_0_m3748846711 (U3CU3Ec_t4236842726 * __this, ProductDefinition_t1942475268 * ___def0, const MethodInfo* method)
{
	{
		ProductDefinition_t1942475268 * L_0 = ___def0;
		NullCheck(L_0);
		int32_t L_1 = ProductDefinition_get_type_m590914665(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Purchasing.Default.WinProductDescription UnityEngine.Purchasing.WinRTStore/<>c::<RetrieveProducts>b__8_1(UnityEngine.Purchasing.ProductDefinition)
extern "C"  WinProductDescription_t1075111405 * U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m823723958 (U3CU3Ec_t4236842726 * __this, ProductDefinition_t1942475268 * ___def0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CRetrieveProductsU3Eb__8_1_m823723958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ProductDefinition_t1942475268 * L_0 = ___def0;
		NullCheck(L_0);
		String_t* L_1 = ProductDefinition_get_storeSpecificId_m2251287741(L_0, /*hidden argument*/NULL);
		ProductDefinition_t1942475268 * L_2 = ___def0;
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_storeSpecificId_m2251287741(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral956924922, L_3, /*hidden argument*/NULL);
		ProductDefinition_t1942475268 * L_5 = ___def0;
		NullCheck(L_5);
		String_t* L_6 = ProductDefinition_get_storeSpecificId_m2251287741(L_5, /*hidden argument*/NULL);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral530053810, L_6, /*hidden argument*/NULL);
		Decimal_t724701077  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Decimal__ctor_m1770144563(&L_8, 1, 0, 0, (bool)0, (uint8_t)2, /*hidden argument*/NULL);
		ProductDefinition_t1942475268 * L_9 = ___def0;
		NullCheck(L_9);
		int32_t L_10 = ProductDefinition_get_type_m590914665(L_9, /*hidden argument*/NULL);
		WinProductDescription_t1075111405 * L_11 = (WinProductDescription_t1075111405 *)il2cpp_codegen_object_new(WinProductDescription_t1075111405_il2cpp_TypeInfo_var);
		WinProductDescription__ctor_m3122598843(L_11, L_1, _stringLiteral1291769943, L_4, L_7, _stringLiteral1690816450, L_8, (String_t*)NULL, (String_t*)NULL, (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void UnityEngine.Purchasing.XiaomiPriceTiers::.cctor()
extern "C"  void XiaomiPriceTiers__cctor_m3016051160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XiaomiPriceTiers__cctor_m3016051160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t3030399641* L_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)92)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305145____8ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0_FieldInfo_var), /*hidden argument*/NULL);
		((XiaomiPriceTiers_t2494450221_StaticFields*)XiaomiPriceTiers_t2494450221_il2cpp_TypeInfo_var->static_fields)->set_XiaomiPriceTierPrices_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
