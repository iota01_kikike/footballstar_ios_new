﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte8
struct  ACTkByte8_t1505541237 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b8
	uint8_t ___b8_7;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte8_t1505541237, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
