﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_System_Xml_XmlChar1369421061.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_UInt322149682021.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Globalization_DateTimeStyles370343085.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"

// System.Array
struct Il2CppArray;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.FormatException
struct FormatException_t2948921286;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t1369421061_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D26_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D27_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D28_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D29_3_FieldInfo_var;
extern const uint32_t XmlChar__cctor_m2546143923_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlConvert_t1936105738_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4267997412;
extern Il2CppCodeGenString* _stringLiteral1558831900;
extern Il2CppCodeGenString* _stringLiteral1507611276;
extern Il2CppCodeGenString* _stringLiteral1200884884;
extern Il2CppCodeGenString* _stringLiteral1140863418;
extern Il2CppCodeGenString* _stringLiteral1685789682;
extern Il2CppCodeGenString* _stringLiteral1699032502;
extern Il2CppCodeGenString* _stringLiteral2296696910;
extern Il2CppCodeGenString* _stringLiteral1516699112;
extern Il2CppCodeGenString* _stringLiteral4175716736;
extern Il2CppCodeGenString* _stringLiteral4126045856;
extern Il2CppCodeGenString* _stringLiteral1863912648;
extern Il2CppCodeGenString* _stringLiteral4023469606;
extern Il2CppCodeGenString* _stringLiteral1159212584;
extern Il2CppCodeGenString* _stringLiteral1751525920;
extern Il2CppCodeGenString* _stringLiteral1978214733;
extern Il2CppCodeGenString* _stringLiteral2646635687;
extern Il2CppCodeGenString* _stringLiteral2673625471;
extern Il2CppCodeGenString* _stringLiteral164244414;
extern Il2CppCodeGenString* _stringLiteral2213484352;
extern Il2CppCodeGenString* _stringLiteral351817784;
extern Il2CppCodeGenString* _stringLiteral4057108113;
extern Il2CppCodeGenString* _stringLiteral106968827;
extern Il2CppCodeGenString* _stringLiteral1937792179;
extern Il2CppCodeGenString* _stringLiteral3603823165;
extern Il2CppCodeGenString* _stringLiteral341927191;
extern Il2CppCodeGenString* _stringLiteral2400745071;
extern Il2CppCodeGenString* _stringLiteral30623680;
extern const uint32_t XmlConvert__cctor_m990448552_MetadataUsageId;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3424083573;
extern const uint32_t XmlConvert_ToTimeSpan_m25786016_MetadataUsageId;

// System.Char[]
struct CharU5BU5D_t1328083999  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t59386216  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, RuntimeFieldHandle_t2331729674  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Trim(System.Char[])
extern "C"  String_t* String_Trim_m3982520224 (String_t* __this, CharU5BU5D_t1328083999* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m1606060069 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.FormatException::.ctor(System.String)
extern "C"  void FormatException__ctor_m1466217969 (FormatException_t2948921286 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m4230566705 (String_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m12482732 (String_t* __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C"  CultureInfo_t3500843524 * CultureInfo_get_InvariantCulture_m398972276 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::Parse(System.String,System.IFormatProvider)
extern "C"  int32_t Int32_Parse_m4134638052 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void TimeSpan__ctor_m423866652 (TimeSpan_t3430258949 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.TimeSpan::get_Ticks()
extern "C"  int64_t TimeSpan_get_Ticks_m2285358246 (TimeSpan_t3430258949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.TimeSpan::FromTicks(System.Int64)
extern "C"  TimeSpan_t3430258949  TimeSpan_FromTicks_m827965597 (Il2CppObject * __this /* static, unused */, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.XmlChar::.cctor()
extern "C"  void XmlChar__cctor_m2546143923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlChar__cctor_m2546143923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CharU5BU5D_t1328083999* L_0 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)4));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D26_0_FieldInfo_var), /*hidden argument*/NULL);
		((XmlChar_t1369421061_StaticFields*)XmlChar_t1369421061_il2cpp_TypeInfo_var->static_fields)->set_WhitespaceChars_0(L_0);
		ByteU5BU5D_t3397334013* L_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D27_1_FieldInfo_var), /*hidden argument*/NULL);
		((XmlChar_t1369421061_StaticFields*)XmlChar_t1369421061_il2cpp_TypeInfo_var->static_fields)->set_firstNamePages_1(L_1);
		ByteU5BU5D_t3397334013* L_2 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D28_2_FieldInfo_var), /*hidden argument*/NULL);
		((XmlChar_t1369421061_StaticFields*)XmlChar_t1369421061_il2cpp_TypeInfo_var->static_fields)->set_namePages_2(L_2);
		UInt32U5BU5D_t59386216* L_3 = ((UInt32U5BU5D_t59386216*)SZArrayNew(UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var, (uint32_t)((int32_t)320)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305143____U24U24fieldU2D29_3_FieldInfo_var), /*hidden argument*/NULL);
		((XmlChar_t1369421061_StaticFields*)XmlChar_t1369421061_il2cpp_TypeInfo_var->static_fields)->set_nameBitmap_3(L_3);
		return;
	}
}
// System.Void System.Xml.XmlConvert::.cctor()
extern "C"  void XmlConvert__cctor_m990448552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert__cctor_m990448552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)((int32_t)27)));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral4267997412);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4267997412);
		StringU5BU5D_t1642385972* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1558831900);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1558831900);
		StringU5BU5D_t1642385972* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral1507611276);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1507611276);
		StringU5BU5D_t1642385972* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral1200884884);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1200884884);
		StringU5BU5D_t1642385972* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral1140863418);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1140863418);
		StringU5BU5D_t1642385972* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1685789682);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral1685789682);
		StringU5BU5D_t1642385972* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1699032502);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral1699032502);
		StringU5BU5D_t1642385972* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2296696910);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral2296696910);
		StringU5BU5D_t1642385972* L_8 = L_7;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral1516699112);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral1516699112);
		StringU5BU5D_t1642385972* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral4175716736);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral4175716736);
		StringU5BU5D_t1642385972* L_10 = L_9;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral4126045856);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral4126045856);
		StringU5BU5D_t1642385972* L_11 = L_10;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral1863912648);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral1863912648);
		StringU5BU5D_t1642385972* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral4023469606);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral4023469606);
		StringU5BU5D_t1642385972* L_13 = L_12;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral1159212584);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral1159212584);
		StringU5BU5D_t1642385972* L_14 = L_13;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral1751525920);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral1751525920);
		StringU5BU5D_t1642385972* L_15 = L_14;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral1978214733);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral1978214733);
		StringU5BU5D_t1642385972* L_16 = L_15;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral2646635687);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral2646635687);
		StringU5BU5D_t1642385972* L_17 = L_16;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral2673625471);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral2673625471);
		StringU5BU5D_t1642385972* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral164244414);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral164244414);
		StringU5BU5D_t1642385972* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral2213484352);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral2213484352);
		StringU5BU5D_t1642385972* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral351817784);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (String_t*)_stringLiteral351817784);
		StringU5BU5D_t1642385972* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral4057108113);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (String_t*)_stringLiteral4057108113);
		StringU5BU5D_t1642385972* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral106968827);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (String_t*)_stringLiteral106968827);
		StringU5BU5D_t1642385972* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral1937792179);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (String_t*)_stringLiteral1937792179);
		StringU5BU5D_t1642385972* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral3603823165);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (String_t*)_stringLiteral3603823165);
		StringU5BU5D_t1642385972* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral341927191);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (String_t*)_stringLiteral341927191);
		StringU5BU5D_t1642385972* L_26 = L_25;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral2400745071);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (String_t*)_stringLiteral2400745071);
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set_datetimeFormats_0(L_26);
		StringU5BU5D_t1642385972* L_27 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, _stringLiteral1140863418);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1140863418);
		StringU5BU5D_t1642385972* L_28 = L_27;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral1685789682);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1685789682);
		StringU5BU5D_t1642385972* L_29 = L_28;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral4023469606);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral4023469606);
		StringU5BU5D_t1642385972* L_30 = L_29;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral1699032502);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1699032502);
		StringU5BU5D_t1642385972* L_31 = L_30;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral1978214733);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1978214733);
		StringU5BU5D_t1642385972* L_32 = L_31;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral164244414);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral164244414);
		StringU5BU5D_t1642385972* L_33 = L_32;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral4057108113);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral4057108113);
		StringU5BU5D_t1642385972* L_34 = L_33;
		NullCheck(L_34);
		ArrayElementTypeCheck (L_34, _stringLiteral3603823165);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral3603823165);
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set_defaultDateTimeFormats_1(L_34);
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set__defaultStyle_6(3);
		StringU5BU5D_t1642385972* L_35 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_defaultDateTimeFormats_1();
		NullCheck(L_35);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))));
		int32_t L_36 = V_0;
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set_roundtripDateTimeFormats_2(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_36)));
		int32_t L_37 = V_0;
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set_localDateTimeFormats_3(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_37)));
		int32_t L_38 = V_0;
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set_utcDateTimeFormats_4(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_38*(int32_t)3)))));
		int32_t L_39 = V_0;
		((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->set_unspecifiedDateTimeFormats_5(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_39*(int32_t)4)))));
		V_1 = 0;
		goto IL_0230;
	}

IL_0186:
	{
		StringU5BU5D_t1642385972* L_40 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_defaultDateTimeFormats_1();
		int32_t L_41 = V_1;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		String_t* L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		V_2 = L_43;
		StringU5BU5D_t1642385972* L_44 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_localDateTimeFormats_3();
		int32_t L_45 = V_1;
		String_t* L_46 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m2596409543(NULL /*static, unused*/, L_46, _stringLiteral30623680, /*hidden argument*/NULL);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(L_45), (String_t*)L_47);
		StringU5BU5D_t1642385972* L_48 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_roundtripDateTimeFormats_2();
		int32_t L_49 = V_1;
		String_t* L_50 = V_2;
		Il2CppChar L_51 = ((Il2CppChar)((int32_t)75));
		Il2CppObject * L_52 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_51);
		String_t* L_53 = String_Concat_m56707527(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_53);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(L_49), (String_t*)L_53);
		StringU5BU5D_t1642385972* L_54 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_utcDateTimeFormats_4();
		int32_t L_55 = V_1;
		String_t* L_56 = V_2;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_56);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_55*(int32_t)3))), (String_t*)L_56);
		StringU5BU5D_t1642385972* L_57 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_utcDateTimeFormats_4();
		int32_t L_58 = V_1;
		String_t* L_59 = V_2;
		Il2CppChar L_60 = ((Il2CppChar)((int32_t)90));
		Il2CppObject * L_61 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_60);
		String_t* L_62 = String_Concat_m56707527(NULL /*static, unused*/, L_59, L_61, /*hidden argument*/NULL);
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, L_62);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_58*(int32_t)3))+(int32_t)1))), (String_t*)L_62);
		StringU5BU5D_t1642385972* L_63 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_utcDateTimeFormats_4();
		int32_t L_64 = V_1;
		String_t* L_65 = V_2;
		String_t* L_66 = String_Concat_m2596409543(NULL /*static, unused*/, L_65, _stringLiteral30623680, /*hidden argument*/NULL);
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, L_66);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_64*(int32_t)3))+(int32_t)2))), (String_t*)L_66);
		StringU5BU5D_t1642385972* L_67 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_unspecifiedDateTimeFormats_5();
		int32_t L_68 = V_1;
		String_t* L_69 = V_2;
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, L_69);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_68*(int32_t)4))), (String_t*)L_69);
		StringU5BU5D_t1642385972* L_70 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_unspecifiedDateTimeFormats_5();
		int32_t L_71 = V_1;
		StringU5BU5D_t1642385972* L_72 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_localDateTimeFormats_3();
		int32_t L_73 = V_1;
		NullCheck(L_72);
		int32_t L_74 = L_73;
		String_t* L_75 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		NullCheck(L_70);
		ArrayElementTypeCheck (L_70, L_75);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_71*(int32_t)4))+(int32_t)1))), (String_t*)L_75);
		StringU5BU5D_t1642385972* L_76 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_unspecifiedDateTimeFormats_5();
		int32_t L_77 = V_1;
		StringU5BU5D_t1642385972* L_78 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_roundtripDateTimeFormats_2();
		int32_t L_79 = V_1;
		NullCheck(L_78);
		int32_t L_80 = L_79;
		String_t* L_81 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, L_81);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_77*(int32_t)4))+(int32_t)2))), (String_t*)L_81);
		StringU5BU5D_t1642385972* L_82 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_unspecifiedDateTimeFormats_5();
		int32_t L_83 = V_1;
		StringU5BU5D_t1642385972* L_84 = ((XmlConvert_t1936105738_StaticFields*)XmlConvert_t1936105738_il2cpp_TypeInfo_var->static_fields)->get_utcDateTimeFormats_4();
		int32_t L_85 = V_1;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		String_t* L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		NullCheck(L_82);
		ArrayElementTypeCheck (L_82, L_87);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_83*(int32_t)4))+(int32_t)3))), (String_t*)L_87);
		int32_t L_88 = V_1;
		V_1 = ((int32_t)((int32_t)L_88+(int32_t)1));
	}

IL_0230:
	{
		int32_t L_89 = V_1;
		int32_t L_90 = V_0;
		if ((((int32_t)L_89) < ((int32_t)L_90)))
		{
			goto IL_0186;
		}
	}
	{
		return;
	}
}
// System.TimeSpan System.Xml.XmlConvert::ToTimeSpan(System.String)
extern "C"  TimeSpan_t3430258949  XmlConvert_ToTimeSpan_m25786016 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XmlConvert_ToTimeSpan_m25786016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int64_t V_8 = 0;
	int32_t V_9 = 0;
	bool V_10 = false;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	TimeSpan_t3430258949  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Il2CppChar V_14 = 0x0;
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t1369421061_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_1 = ((XmlChar_t1369421061_StaticFields*)XmlChar_t1369421061_il2cpp_TypeInfo_var->static_fields)->get_WhitespaceChars_0();
		NullCheck(L_0);
		String_t* L_2 = String_Trim_m3982520224(L_0, L_1, /*hidden argument*/NULL);
		___s0 = L_2;
		String_t* L_3 = ___s0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		FormatException_t2948921286 * L_5 = (FormatException_t2948921286 *)il2cpp_codegen_object_new(FormatException_t2948921286_il2cpp_TypeInfo_var);
		FormatException__ctor_m1466217969(L_5, _stringLiteral3424083573, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0023:
	{
		V_0 = 0;
		String_t* L_6 = ___s0;
		NullCheck(L_6);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_6, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0035;
		}
	}
	{
		V_0 = 1;
	}

IL_0035:
	{
		int32_t L_8 = V_0;
		V_1 = (bool)((((int32_t)L_8) == ((int32_t)1))? 1 : 0);
		String_t* L_9 = ___s0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m4230566705(L_9, L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)((int32_t)80))))
		{
			goto IL_0053;
		}
	}
	{
		FormatException_t2948921286 * L_12 = (FormatException_t2948921286 *)il2cpp_codegen_object_new(FormatException_t2948921286_il2cpp_TypeInfo_var);
		FormatException__ctor_m1466217969(L_12, _stringLiteral3424083573, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0053:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		V_2 = 0;
		V_3 = 0;
		V_4 = (bool)0;
		V_5 = 0;
		V_6 = 0;
		V_7 = 0;
		V_8 = (((int64_t)((int64_t)0)));
		V_9 = 0;
		V_10 = (bool)0;
		int32_t L_14 = V_0;
		V_11 = L_14;
		goto IL_0281;
	}

IL_0079:
	{
		String_t* L_15 = ___s0;
		int32_t L_16 = V_11;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m4230566705(L_15, L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)84)))))
		{
			goto IL_009b;
		}
	}
	{
		V_4 = (bool)1;
		V_2 = 4;
		int32_t L_18 = V_11;
		V_11 = ((int32_t)((int32_t)L_18+(int32_t)1));
		int32_t L_19 = V_11;
		V_0 = L_19;
		goto IL_0281;
	}

IL_009b:
	{
		goto IL_00c9;
	}

IL_00a0:
	{
		String_t* L_20 = ___s0;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		Il2CppChar L_22 = String_get_Chars_m4230566705(L_20, L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)48))))
		{
			goto IL_00be;
		}
	}
	{
		String_t* L_23 = ___s0;
		int32_t L_24 = V_11;
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m4230566705(L_23, L_24, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)57)) >= ((int32_t)L_25)))
		{
			goto IL_00c3;
		}
	}

IL_00be:
	{
		goto IL_00d6;
	}

IL_00c3:
	{
		int32_t L_26 = V_11;
		V_11 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00c9:
	{
		int32_t L_27 = V_11;
		String_t* L_28 = ___s0;
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m1606060069(L_28, /*hidden argument*/NULL);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_00a0;
		}
	}

IL_00d6:
	{
		int32_t L_30 = V_2;
		if ((!(((uint32_t)L_30) == ((uint32_t)7))))
		{
			goto IL_00e3;
		}
	}
	{
		int32_t L_31 = V_11;
		int32_t L_32 = V_0;
		V_9 = ((int32_t)((int32_t)L_31-(int32_t)L_32));
	}

IL_00e3:
	{
		String_t* L_33 = ___s0;
		int32_t L_34 = V_0;
		int32_t L_35 = V_11;
		int32_t L_36 = V_0;
		NullCheck(L_33);
		String_t* L_37 = String_Substring_m12482732(L_33, L_34, ((int32_t)((int32_t)L_35-(int32_t)L_36)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_38 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_39 = Int32_Parse_m4134638052(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		V_12 = L_39;
		int32_t L_40 = V_2;
		if ((!(((uint32_t)L_40) == ((uint32_t)7))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_0113;
	}

IL_0106:
	{
		int32_t L_41 = V_12;
		V_12 = ((int32_t)((int32_t)L_41/(int32_t)((int32_t)10)));
		int32_t L_42 = V_9;
		V_9 = ((int32_t)((int32_t)L_42-(int32_t)1));
	}

IL_0113:
	{
		int32_t L_43 = V_9;
		if ((((int32_t)L_43) > ((int32_t)7)))
		{
			goto IL_0106;
		}
	}
	{
		goto IL_012d;
	}

IL_0120:
	{
		int32_t L_44 = V_12;
		V_12 = ((int32_t)((int32_t)L_44*(int32_t)((int32_t)10)));
		int32_t L_45 = V_9;
		V_9 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_012d:
	{
		int32_t L_46 = V_9;
		if ((((int32_t)L_46) < ((int32_t)7)))
		{
			goto IL_0120;
		}
	}

IL_0135:
	{
		String_t* L_47 = ___s0;
		int32_t L_48 = V_11;
		NullCheck(L_47);
		Il2CppChar L_49 = String_get_Chars_m4230566705(L_47, L_48, /*hidden argument*/NULL);
		V_14 = L_49;
		Il2CppChar L_50 = V_14;
		if ((((int32_t)L_50) == ((int32_t)((int32_t)46))))
		{
			goto IL_024f;
		}
	}
	{
		Il2CppChar L_51 = V_14;
		if ((((int32_t)L_51) == ((int32_t)((int32_t)68))))
		{
			goto IL_01e1;
		}
	}
	{
		Il2CppChar L_52 = V_14;
		if ((((int32_t)L_52) == ((int32_t)((int32_t)72))))
		{
			goto IL_01fc;
		}
	}
	{
		Il2CppChar L_53 = V_14;
		if ((((int32_t)L_53) == ((int32_t)((int32_t)77))))
		{
			goto IL_019b;
		}
	}
	{
		Il2CppChar L_54 = V_14;
		if ((((int32_t)L_54) == ((int32_t)((int32_t)83))))
		{
			goto IL_021d;
		}
	}
	{
		Il2CppChar L_55 = V_14;
		if ((((int32_t)L_55) == ((int32_t)((int32_t)89))))
		{
			goto IL_017a;
		}
	}
	{
		goto IL_0264;
	}

IL_017a:
	{
		int32_t L_56 = V_3;
		int32_t L_57 = V_12;
		V_3 = ((int32_t)((int32_t)L_56+(int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)365)))));
		int32_t L_58 = V_2;
		if ((((int32_t)L_58) <= ((int32_t)0)))
		{
			goto IL_0194;
		}
	}
	{
		V_10 = (bool)1;
		goto IL_0196;
	}

IL_0194:
	{
		V_2 = 1;
	}

IL_0196:
	{
		goto IL_026c;
	}

IL_019b:
	{
		int32_t L_59 = V_2;
		if ((((int32_t)L_59) >= ((int32_t)2)))
		{
			goto IL_01c0;
		}
	}
	{
		int32_t L_60 = V_3;
		int32_t L_61 = V_12;
		int32_t L_62 = V_12;
		V_3 = ((int32_t)((int32_t)L_60+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)365)*(int32_t)((int32_t)((int32_t)L_61/(int32_t)((int32_t)12)))))+(int32_t)((int32_t)((int32_t)((int32_t)30)*(int32_t)((int32_t)((int32_t)L_62%(int32_t)((int32_t)12)))))))));
		V_2 = 2;
		goto IL_01dc;
	}

IL_01c0:
	{
		bool L_63 = V_4;
		if (!L_63)
		{
			goto IL_01d9;
		}
	}
	{
		int32_t L_64 = V_2;
		if ((((int32_t)L_64) >= ((int32_t)6)))
		{
			goto IL_01d9;
		}
	}
	{
		int32_t L_65 = V_12;
		V_6 = L_65;
		V_2 = 6;
		goto IL_01dc;
	}

IL_01d9:
	{
		V_10 = (bool)1;
	}

IL_01dc:
	{
		goto IL_026c;
	}

IL_01e1:
	{
		int32_t L_66 = V_3;
		int32_t L_67 = V_12;
		V_3 = ((int32_t)((int32_t)L_66+(int32_t)L_67));
		int32_t L_68 = V_2;
		if ((((int32_t)L_68) <= ((int32_t)2)))
		{
			goto IL_01f5;
		}
	}
	{
		V_10 = (bool)1;
		goto IL_01f7;
	}

IL_01f5:
	{
		V_2 = 3;
	}

IL_01f7:
	{
		goto IL_026c;
	}

IL_01fc:
	{
		int32_t L_69 = V_12;
		V_5 = L_69;
		bool L_70 = V_4;
		if (!L_70)
		{
			goto IL_020e;
		}
	}
	{
		int32_t L_71 = V_2;
		if ((((int32_t)L_71) <= ((int32_t)4)))
		{
			goto IL_0216;
		}
	}

IL_020e:
	{
		V_10 = (bool)1;
		goto IL_0218;
	}

IL_0216:
	{
		V_2 = 5;
	}

IL_0218:
	{
		goto IL_026c;
	}

IL_021d:
	{
		int32_t L_72 = V_2;
		if ((!(((uint32_t)L_72) == ((uint32_t)7))))
		{
			goto IL_022e;
		}
	}
	{
		int32_t L_73 = V_12;
		V_8 = (((int64_t)((int64_t)L_73)));
		goto IL_0232;
	}

IL_022e:
	{
		int32_t L_74 = V_12;
		V_7 = L_74;
	}

IL_0232:
	{
		bool L_75 = V_4;
		if (!L_75)
		{
			goto IL_0240;
		}
	}
	{
		int32_t L_76 = V_2;
		if ((((int32_t)L_76) <= ((int32_t)7)))
		{
			goto IL_0248;
		}
	}

IL_0240:
	{
		V_10 = (bool)1;
		goto IL_024a;
	}

IL_0248:
	{
		V_2 = 8;
	}

IL_024a:
	{
		goto IL_026c;
	}

IL_024f:
	{
		int32_t L_77 = V_2;
		if ((((int32_t)L_77) <= ((int32_t)7)))
		{
			goto IL_0259;
		}
	}
	{
		V_10 = (bool)1;
	}

IL_0259:
	{
		int32_t L_78 = V_12;
		V_7 = L_78;
		V_2 = 7;
		goto IL_026c;
	}

IL_0264:
	{
		V_10 = (bool)1;
		goto IL_026c;
	}

IL_026c:
	{
		bool L_79 = V_10;
		if (!L_79)
		{
			goto IL_0278;
		}
	}
	{
		goto IL_028e;
	}

IL_0278:
	{
		int32_t L_80 = V_11;
		V_11 = ((int32_t)((int32_t)L_80+(int32_t)1));
		int32_t L_81 = V_11;
		V_0 = L_81;
	}

IL_0281:
	{
		int32_t L_82 = V_11;
		String_t* L_83 = ___s0;
		NullCheck(L_83);
		int32_t L_84 = String_get_Length_m1606060069(L_83, /*hidden argument*/NULL);
		if ((((int32_t)L_82) < ((int32_t)L_84)))
		{
			goto IL_0079;
		}
	}

IL_028e:
	{
		bool L_85 = V_10;
		if (!L_85)
		{
			goto IL_02a0;
		}
	}
	{
		FormatException_t2948921286 * L_86 = (FormatException_t2948921286 *)il2cpp_codegen_object_new(FormatException_t2948921286_il2cpp_TypeInfo_var);
		FormatException__ctor_m1466217969(L_86, _stringLiteral3424083573, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_86);
	}

IL_02a0:
	{
		int32_t L_87 = V_3;
		int32_t L_88 = V_5;
		int32_t L_89 = V_6;
		int32_t L_90 = V_7;
		TimeSpan__ctor_m423866652((&V_13), L_87, L_88, L_89, L_90, /*hidden argument*/NULL);
		bool L_91 = V_1;
		if (!L_91)
		{
			goto IL_02c5;
		}
	}
	{
		int64_t L_92 = TimeSpan_get_Ticks_m2285358246((&V_13), /*hidden argument*/NULL);
		int64_t L_93 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_94 = TimeSpan_FromTicks_m827965597(NULL /*static, unused*/, ((-((int64_t)((int64_t)L_92+(int64_t)L_93)))), /*hidden argument*/NULL);
		return L_94;
	}

IL_02c5:
	{
		int64_t L_95 = TimeSpan_get_Ticks_m2285358246((&V_13), /*hidden argument*/NULL);
		int64_t L_96 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t3430258949_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_97 = TimeSpan_FromTicks_m827965597(NULL /*static, unused*/, ((int64_t)((int64_t)L_95+(int64_t)L_96)), /*hidden argument*/NULL);
		return L_97;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
