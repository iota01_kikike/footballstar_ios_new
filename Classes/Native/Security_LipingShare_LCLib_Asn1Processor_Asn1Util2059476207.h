﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Util
struct  Asn1Util_t2059476207  : public Il2CppObject
{
public:

public:
};

struct Asn1Util_t2059476207_StaticFields
{
public:
	// System.Char[] LipingShare.LCLib.Asn1Processor.Asn1Util::hexDigits
	CharU5BU5D_t1328083999* ___hexDigits_0;

public:
	inline static int32_t get_offset_of_hexDigits_0() { return static_cast<int32_t>(offsetof(Asn1Util_t2059476207_StaticFields, ___hexDigits_0)); }
	inline CharU5BU5D_t1328083999* get_hexDigits_0() const { return ___hexDigits_0; }
	inline CharU5BU5D_t1328083999** get_address_of_hexDigits_0() { return &___hexDigits_0; }
	inline void set_hexDigits_0(CharU5BU5D_t1328083999* value)
	{
		___hexDigits_0 = value;
		Il2CppCodeGenWriteBarrier(&___hexDigits_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
