﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityStore_U3CModuleU3E3783534214.h"
#include "UnityStore_UnityEngine_Store_AppInfo2080248435.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityStore_UnityEngine_Store_LoginForwardCallback1105422505.h"
#include "UnityEngine_UnityEngine_AndroidJavaProxy4274989947.h"
#include "UnityStore_UnityEngine_Store_MainThreadDispatcher513574034.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2595592884.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityStore_UnityEngine_Store_StoreService3339196318.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass2973420583.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject4251328308.h"
#include "UnityStore_UnityEngine_Store_UserInfo741955747.h"

// UnityEngine.Store.AppInfo
struct AppInfo_t2080248435;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Store.LoginForwardCallback
struct LoginForwardCallback_t1105422505;
// UnityEngine.Store.ILoginListener
struct ILoginListener_t3164042774;
// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_t4274989947;
// System.Action
struct Action_t3226471752;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2595592884;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Store.MainThreadDispatcher
struct MainThreadDispatcher_t513574034;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// System.Action[]
struct ActionU5BU5D_t87223449;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t2973420583;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4251328308;
// UnityEngine.Store.UserInfo
struct UserInfo_t741955747;
extern Il2CppCodeGenString* _stringLiteral772087215;
extern const uint32_t LoginForwardCallback__ctor_m1842789250_MetadataUsageId;
extern Il2CppClass* MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m253778810_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_RunOnMainThread_m3425248423_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t MainThreadDispatcher_Start_m3725736351_MetadataUsageId;
extern Il2CppClass* ActionU5BU5D_t87223449_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1567401164_MethodInfo_var;
extern const MethodInfo* List_1_CopyTo_m3662704345_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m2016173969_MethodInfo_var;
extern const uint32_t MainThreadDispatcher_Update_m546764710_MetadataUsageId;
extern Il2CppClass* List_1_t2595592884_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3444459430_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral334013814;
extern const uint32_t MainThreadDispatcher__cctor_m2497885682_MetadataUsageId;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var;
extern Il2CppClass* LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var;
extern Il2CppClass* StoreService_t3339196318_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var;
extern const MethodInfo* AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3075639932;
extern Il2CppCodeGenString* _stringLiteral2584981958;
extern Il2CppCodeGenString* _stringLiteral766701006;
extern Il2CppCodeGenString* _stringLiteral526454908;
extern Il2CppCodeGenString* _stringLiteral1195878932;
extern Il2CppCodeGenString* _stringLiteral1643072148;
extern Il2CppCodeGenString* _stringLiteral1396753435;
extern Il2CppCodeGenString* _stringLiteral882420973;
extern Il2CppCodeGenString* _stringLiteral2126705824;
extern const uint32_t StoreService_Initialize_m2334922442_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4283626287;
extern const uint32_t StoreService_Login_m2535849060_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2225618755;
extern const uint32_t StoreService__cctor_m2623941302_MetadataUsageId;

// System.Action[]
struct ActionU5BU5D_t87223449  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3226471752 * m_Items[1];

public:
	inline Action_t3226471752 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Action_t3226471752 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Action_t3226471752 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Action_t3226471752 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(!0[])
extern "C"  void List_1_CopyTo_m2192729009_gshared (List_1_t2058570427 * __this, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
extern "C"  Il2CppObject * AndroidJavaObject_GetStatic_TisIl2CppObject_m569049653_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, const MethodInfo* method);
// System.Void UnityEngine.AndroidJavaObject::Set<System.Object>(System.String,!!0)
extern "C"  void AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, Il2CppObject * p1, const MethodInfo* method);
// System.Void UnityEngine.AndroidJavaObject::Set<System.Boolean>(System.String,!!0)
extern "C"  void AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_gshared (AndroidJavaObject_t4251328308 * __this, String_t* p0, bool p1, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaProxy::.ctor(System.String)
extern "C"  void AndroidJavaProxy__ctor_m4016180768 (AndroidJavaProxy_t4274989947 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2136705809 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Action>::Add(!0)
#define List_1_Add_m253778810(__this, p0, method) ((  void (*) (List_1_t2595592884 *, Action_t3226471752 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count()
#define List_1_get_Count_m1567401164(__this, method) ((  int32_t (*) (List_1_t2595592884 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action>::CopyTo(!0[])
#define List_1_CopyTo_m3662704345(__this, p0, method) ((  void (*) (List_1_t2595592884 *, ActionU5BU5D_t87223449*, const MethodInfo*))List_1_CopyTo_m2192729009_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Action>::Clear()
#define List_1_Clear_m2016173969(__this, method) ((  void (*) (List_1_t2595592884 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m3801112262 (Action_t3226471752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
#define List_1__ctor_m3444459430(__this, method) ((  void (*) (List_1_t2595592884 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m962601984 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2204253440 (Object_t1021602117 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Store.MainThreadDispatcher>()
#define GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902(__this, method) ((  MainThreadDispatcher_t513574034 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3535407496_gshared)(__this, method)
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
extern "C"  void AndroidJavaClass__ctor_m3221829804 (AndroidJavaClass_t2973420583 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
#define AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956(__this, p0, method) ((  AndroidJavaObject_t4251328308 * (*) (AndroidJavaObject_t4251328308 *, String_t*, const MethodInfo*))AndroidJavaObject_GetStatic_TisIl2CppObject_m569049653_gshared)(__this, p0, method)
// System.Void UnityEngine.Store.LoginForwardCallback::.ctor(UnityEngine.Store.ILoginListener)
extern "C"  void LoginForwardCallback__ctor_m1842789250 (LoginForwardCallback_t1105422505 * __this, Il2CppObject * ___loginListener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::.ctor(System.String,System.Object[])
extern "C"  void AndroidJavaObject__ctor_m1076535321 (AndroidJavaObject_t4251328308 * __this, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_appId()
extern "C"  String_t* AppInfo_get_appId_m3632567790 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::Set<System.String>(System.String,!!0)
#define AndroidJavaObject_Set_TisString_t_m551399930(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, String_t*, const MethodInfo*))AndroidJavaObject_Set_TisIl2CppObject_m1125093376_gshared)(__this, p0, p1, method)
// System.String UnityEngine.Store.AppInfo::get_appKey()
extern "C"  String_t* AppInfo_get_appKey_m3678981840 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_clientId()
extern "C"  String_t* AppInfo_get_clientId_m1686889884 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Store.AppInfo::get_clientKey()
extern "C"  String_t* AppInfo_get_clientKey_m385900370 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Store.AppInfo::get_debug()
extern "C"  bool AppInfo_get_debug_m3181976920 (AppInfo_t2080248435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::Set<System.Boolean>(System.String,!!0)
#define AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435(__this, p0, p1, method) ((  void (*) (AndroidJavaObject_t4251328308 *, String_t*, bool, const MethodInfo*))AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.AndroidJavaObject::CallStatic(System.String,System.Object[])
extern "C"  void AndroidJavaObject_CallStatic_m1227537731 (AndroidJavaObject_t4251328308 * __this, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.Store.AppInfo::get_appId()
extern "C"  String_t* AppInfo_get_appId_m3632567790 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CappIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_appId(System.String)
extern "C"  void AppInfo_set_appId_m1112756485 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CappIdU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_appKey()
extern "C"  String_t* AppInfo_get_appKey_m3678981840 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CappKeyU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_appKey(System.String)
extern "C"  void AppInfo_set_appKey_m3344021451 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CappKeyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_clientId()
extern "C"  String_t* AppInfo_get_clientId_m1686889884 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CclientIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_clientId(System.String)
extern "C"  void AppInfo_set_clientId_m1298558813 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CclientIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String UnityEngine.Store.AppInfo::get_clientKey()
extern "C"  String_t* AppInfo_get_clientKey_m385900370 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CclientKeyU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_clientKey(System.String)
extern "C"  void AppInfo_set_clientKey_m1161742527 (AppInfo_t2080248435 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CclientKeyU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Store.AppInfo::get_debug()
extern "C"  bool AppInfo_get_debug_m3181976920 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CdebugU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.Store.AppInfo::set_debug(System.Boolean)
extern "C"  void AppInfo_set_debug_m4042117453 (AppInfo_t2080248435 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdebugU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void UnityEngine.Store.AppInfo::.ctor()
extern "C"  void AppInfo__ctor_m2870035846 (AppInfo_t2080248435 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.LoginForwardCallback::.ctor(UnityEngine.Store.ILoginListener)
extern "C"  void LoginForwardCallback__ctor_m1842789250 (LoginForwardCallback_t1105422505 * __this, Il2CppObject * ___loginListener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoginForwardCallback__ctor_m1842789250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m4016180768(__this, _stringLiteral772087215, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___loginListener0;
		__this->set_loginListener_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::RunOnMainThread(System.Action)
extern "C"  void MainThreadDispatcher_RunOnMainThread_m3425248423 (Il2CppObject * __this /* static, unused */, Action_t3226471752 * ___runnable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_RunOnMainThread_m3425248423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2595592884 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_0 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
		V_0 = L_0;
		List_1_t2595592884 * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_2 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
		Action_t3226471752 * L_3 = ___runnable0;
		NullCheck(L_2);
		List_1_Add_m253778810(L_2, L_3, /*hidden argument*/List_1_Add_m253778810_MethodInfo_var);
		il2cpp_codegen_memory_barrier();
		((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_s_CallbacksPending_4(1);
		IL2CPP_LEAVE(0x2E, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		List_1_t2595592884 * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x2E, IL_002e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002e:
	{
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::Start()
extern "C"  void MainThreadDispatcher_Start_m3725736351 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Start_m3725736351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::Update()
extern "C"  void MainThreadDispatcher_Update_m546764710 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Update_m546764710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ActionU5BU5D_t87223449* V_0 = NULL;
	bool V_1 = false;
	List_1_t2595592884 * V_2 = NULL;
	bool V_3 = false;
	ActionU5BU5D_t87223449* V_4 = NULL;
	int32_t V_5 = 0;
	Action_t3226471752 * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		bool L_0 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_CallbacksPending_4();
		il2cpp_codegen_memory_barrier();
		V_1 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0097;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		List_1_t2595592884 * L_2 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
		V_2 = L_2;
		List_1_t2595592884 * L_3 = V_2;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
			List_1_t2595592884 * L_4 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			NullCheck(L_4);
			int32_t L_5 = List_1_get_Count_m1567401164(L_4, /*hidden argument*/List_1_get_Count_m1567401164_MethodInfo_var);
			V_3 = (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
			bool L_6 = V_3;
			if (!L_6)
			{
				goto IL_0035;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x97, FINALLY_0067);
		}

IL_0035:
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
			List_1_t2595592884 * L_7 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			NullCheck(L_7);
			int32_t L_8 = List_1_get_Count_m1567401164(L_7, /*hidden argument*/List_1_get_Count_m1567401164_MethodInfo_var);
			V_0 = ((ActionU5BU5D_t87223449*)SZArrayNew(ActionU5BU5D_t87223449_il2cpp_TypeInfo_var, (uint32_t)L_8));
			List_1_t2595592884 * L_9 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			ActionU5BU5D_t87223449* L_10 = V_0;
			NullCheck(L_9);
			List_1_CopyTo_m3662704345(L_9, L_10, /*hidden argument*/List_1_CopyTo_m3662704345_MethodInfo_var);
			List_1_t2595592884 * L_11 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_s_Callbacks_3();
			NullCheck(L_11);
			List_1_Clear_m2016173969(L_11, /*hidden argument*/List_1_Clear_m2016173969_MethodInfo_var);
			il2cpp_codegen_memory_barrier();
			((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_s_CallbacksPending_4(0);
			IL2CPP_LEAVE(0x6F, FINALLY_0067);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		List_1_t2595592884 * L_12 = V_2;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(103)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006f:
	{
		ActionU5BU5D_t87223449* L_13 = V_0;
		V_4 = L_13;
		V_5 = 0;
		goto IL_008f;
	}

IL_0078:
	{
		ActionU5BU5D_t87223449* L_14 = V_4;
		int32_t L_15 = V_5;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Action_t3226471752 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_6 = L_17;
		Action_t3226471752 * L_18 = V_6;
		NullCheck(L_18);
		Action_Invoke_m3801112262(L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_5;
		V_5 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_008f:
	{
		int32_t L_20 = V_5;
		ActionU5BU5D_t87223449* L_21 = V_4;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_0078;
		}
	}

IL_0097:
	{
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::.ctor()
extern "C"  void MainThreadDispatcher__ctor_m292601619 (MainThreadDispatcher_t513574034 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.MainThreadDispatcher::.cctor()
extern "C"  void MainThreadDispatcher__cctor_m2497885682 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher__cctor_m2497885682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_OBJECT_NAME_2(_stringLiteral334013814);
		List_1_t2595592884 * L_0 = (List_1_t2595592884 *)il2cpp_codegen_object_new(List_1_t2595592884_il2cpp_TypeInfo_var);
		List_1__ctor_m3444459430(L_0, /*hidden argument*/List_1__ctor_m3444459430_MethodInfo_var);
		((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->set_s_Callbacks_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Store.StoreService::Initialize(UnityEngine.Store.AppInfo,UnityEngine.Store.ILoginListener)
extern "C"  void StoreService_Initialize_m2334922442 (Il2CppObject * __this /* static, unused */, AppInfo_t2080248435 * ___appInfo0, Il2CppObject * ___listener1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_Initialize_m2334922442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t2973420583 * V_0 = NULL;
	AndroidJavaObject_t4251328308 * V_1 = NULL;
	LoginForwardCallback_t1105422505 * V_2 = NULL;
	AndroidJavaObject_t4251328308 * V_3 = NULL;
	bool V_4 = false;
	GameObject_t1756533147 * V_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		String_t* L_0 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_OBJECT_NAME_2();
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		V_4 = L_2;
		bool L_3 = V_4;
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var);
		String_t* L_4 = ((MainThreadDispatcher_t513574034_StaticFields*)MainThreadDispatcher_t513574034_il2cpp_TypeInfo_var->static_fields)->get_OBJECT_NAME_2();
		GameObject_t1756533147 * L_5 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_5, L_4, /*hidden argument*/NULL);
		V_5 = L_5;
		GameObject_t1756533147 * L_6 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_5;
		NullCheck(L_7);
		Object_set_hideFlags_m2204253440(L_7, 3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = V_5;
		NullCheck(L_8);
		GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902(L_8, /*hidden argument*/GameObject_AddComponent_TisMainThreadDispatcher_t513574034_m3133984902_MethodInfo_var);
	}

IL_003e:
	{
		AndroidJavaClass_t2973420583 * L_9 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_9, _stringLiteral3075639932, /*hidden argument*/NULL);
		V_0 = L_9;
		AndroidJavaClass_t2973420583 * L_10 = V_0;
		NullCheck(L_10);
		AndroidJavaObject_t4251328308 * L_11 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956(L_10, _stringLiteral2584981958, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4251328308_m974310956_MethodInfo_var);
		V_1 = L_11;
		Il2CppObject * L_12 = ___listener1;
		LoginForwardCallback_t1105422505 * L_13 = (LoginForwardCallback_t1105422505 *)il2cpp_codegen_object_new(LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var);
		LoginForwardCallback__ctor_m1842789250(L_13, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		AndroidJavaObject_t4251328308 * L_14 = (AndroidJavaObject_t4251328308 *)il2cpp_codegen_object_new(AndroidJavaObject_t4251328308_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m1076535321(L_14, _stringLiteral766701006, ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_3 = L_14;
		AndroidJavaObject_t4251328308 * L_15 = V_3;
		AppInfo_t2080248435 * L_16 = ___appInfo0;
		NullCheck(L_16);
		String_t* L_17 = AppInfo_get_appId_m3632567790(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		AndroidJavaObject_Set_TisString_t_m551399930(L_15, _stringLiteral526454908, L_17, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_18 = V_3;
		AppInfo_t2080248435 * L_19 = ___appInfo0;
		NullCheck(L_19);
		String_t* L_20 = AppInfo_get_appKey_m3678981840(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		AndroidJavaObject_Set_TisString_t_m551399930(L_18, _stringLiteral1195878932, L_20, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_21 = V_3;
		AppInfo_t2080248435 * L_22 = ___appInfo0;
		NullCheck(L_22);
		String_t* L_23 = AppInfo_get_clientId_m1686889884(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		AndroidJavaObject_Set_TisString_t_m551399930(L_21, _stringLiteral1643072148, L_23, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_24 = V_3;
		AppInfo_t2080248435 * L_25 = ___appInfo0;
		NullCheck(L_25);
		String_t* L_26 = AppInfo_get_clientKey_m385900370(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		AndroidJavaObject_Set_TisString_t_m551399930(L_24, _stringLiteral1396753435, L_26, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m551399930_MethodInfo_var);
		AndroidJavaObject_t4251328308 * L_27 = V_3;
		AppInfo_t2080248435 * L_28 = ___appInfo0;
		NullCheck(L_28);
		bool L_29 = AppInfo_get_debug_m3181976920(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435(L_27, _stringLiteral882420973, L_29, /*hidden argument*/AndroidJavaObject_Set_TisBoolean_t3825574718_m4012912435_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(StoreService_t3339196318_il2cpp_TypeInfo_var);
		AndroidJavaClass_t2973420583 * L_30 = ((StoreService_t3339196318_StaticFields*)StoreService_t3339196318_il2cpp_TypeInfo_var->static_fields)->get_serviceClass_0();
		ObjectU5BU5D_t3614634134* L_31 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		AndroidJavaObject_t4251328308 * L_32 = V_1;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_31;
		AndroidJavaObject_t4251328308 * L_34 = V_3;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_34);
		ObjectU5BU5D_t3614634134* L_35 = L_33;
		LoginForwardCallback_t1105422505 * L_36 = V_2;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_36);
		NullCheck(L_30);
		AndroidJavaObject_CallStatic_m1227537731(L_30, _stringLiteral2126705824, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.StoreService::Login(UnityEngine.Store.ILoginListener)
extern "C"  void StoreService_Login_m2535849060 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_Login_m2535849060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LoginForwardCallback_t1105422505 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___listener0;
		LoginForwardCallback_t1105422505 * L_1 = (LoginForwardCallback_t1105422505 *)il2cpp_codegen_object_new(LoginForwardCallback_t1105422505_il2cpp_TypeInfo_var);
		LoginForwardCallback__ctor_m1842789250(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(StoreService_t3339196318_il2cpp_TypeInfo_var);
		AndroidJavaClass_t2973420583 * L_2 = ((StoreService_t3339196318_StaticFields*)StoreService_t3339196318_il2cpp_TypeInfo_var->static_fields)->get_serviceClass_0();
		ObjectU5BU5D_t3614634134* L_3 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		LoginForwardCallback_t1105422505 * L_4 = V_0;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		NullCheck(L_2);
		AndroidJavaObject_CallStatic_m1227537731(L_2, _stringLiteral4283626287, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Store.StoreService::.cctor()
extern "C"  void StoreService__cctor_m2623941302 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService__cctor_m2623941302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaClass_t2973420583 * L_0 = (AndroidJavaClass_t2973420583 *)il2cpp_codegen_object_new(AndroidJavaClass_t2973420583_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m3221829804(L_0, _stringLiteral2225618755, /*hidden argument*/NULL);
		((StoreService_t3339196318_StaticFields*)StoreService_t3339196318_il2cpp_TypeInfo_var->static_fields)->set_serviceClass_0(L_0);
		return;
	}
}
// System.String UnityEngine.Store.UserInfo::get_channel()
extern "C"  String_t* UserInfo_get_channel_m1234361509 (UserInfo_t741955747 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CchannelU3Ek__BackingField_0();
		return L_0;
	}
}
// System.String UnityEngine.Store.UserInfo::get_userId()
extern "C"  String_t* UserInfo_get_userId_m3160010982 (UserInfo_t741955747 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CuserIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.String UnityEngine.Store.UserInfo::get_userLoginToken()
extern "C"  String_t* UserInfo_get_userLoginToken_m706751899 (UserInfo_t741955747 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CuserLoginTokenU3Ek__BackingField_2();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
