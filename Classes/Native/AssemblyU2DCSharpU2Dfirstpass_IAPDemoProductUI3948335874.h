﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemoProductUI
struct  IAPDemoProductUI_t3948335874  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button IAPDemoProductUI::purchaseButton
	Button_t2872111280 * ___purchaseButton_2;
	// UnityEngine.UI.Button IAPDemoProductUI::receiptButton
	Button_t2872111280 * ___receiptButton_3;
	// UnityEngine.UI.Text IAPDemoProductUI::titleText
	Text_t356221433 * ___titleText_4;
	// UnityEngine.UI.Text IAPDemoProductUI::descriptionText
	Text_t356221433 * ___descriptionText_5;
	// UnityEngine.UI.Text IAPDemoProductUI::priceText
	Text_t356221433 * ___priceText_6;
	// UnityEngine.UI.Text IAPDemoProductUI::statusText
	Text_t356221433 * ___statusText_7;
	// System.String IAPDemoProductUI::m_ProductID
	String_t* ___m_ProductID_8;
	// System.Action`1<System.String> IAPDemoProductUI::m_PurchaseCallback
	Action_1_t1831019615 * ___m_PurchaseCallback_9;
	// System.String IAPDemoProductUI::m_Receipt
	String_t* ___m_Receipt_10;

public:
	inline static int32_t get_offset_of_purchaseButton_2() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___purchaseButton_2)); }
	inline Button_t2872111280 * get_purchaseButton_2() const { return ___purchaseButton_2; }
	inline Button_t2872111280 ** get_address_of_purchaseButton_2() { return &___purchaseButton_2; }
	inline void set_purchaseButton_2(Button_t2872111280 * value)
	{
		___purchaseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseButton_2, value);
	}

	inline static int32_t get_offset_of_receiptButton_3() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___receiptButton_3)); }
	inline Button_t2872111280 * get_receiptButton_3() const { return ___receiptButton_3; }
	inline Button_t2872111280 ** get_address_of_receiptButton_3() { return &___receiptButton_3; }
	inline void set_receiptButton_3(Button_t2872111280 * value)
	{
		___receiptButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___receiptButton_3, value);
	}

	inline static int32_t get_offset_of_titleText_4() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___titleText_4)); }
	inline Text_t356221433 * get_titleText_4() const { return ___titleText_4; }
	inline Text_t356221433 ** get_address_of_titleText_4() { return &___titleText_4; }
	inline void set_titleText_4(Text_t356221433 * value)
	{
		___titleText_4 = value;
		Il2CppCodeGenWriteBarrier(&___titleText_4, value);
	}

	inline static int32_t get_offset_of_descriptionText_5() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___descriptionText_5)); }
	inline Text_t356221433 * get_descriptionText_5() const { return ___descriptionText_5; }
	inline Text_t356221433 ** get_address_of_descriptionText_5() { return &___descriptionText_5; }
	inline void set_descriptionText_5(Text_t356221433 * value)
	{
		___descriptionText_5 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionText_5, value);
	}

	inline static int32_t get_offset_of_priceText_6() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___priceText_6)); }
	inline Text_t356221433 * get_priceText_6() const { return ___priceText_6; }
	inline Text_t356221433 ** get_address_of_priceText_6() { return &___priceText_6; }
	inline void set_priceText_6(Text_t356221433 * value)
	{
		___priceText_6 = value;
		Il2CppCodeGenWriteBarrier(&___priceText_6, value);
	}

	inline static int32_t get_offset_of_statusText_7() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___statusText_7)); }
	inline Text_t356221433 * get_statusText_7() const { return ___statusText_7; }
	inline Text_t356221433 ** get_address_of_statusText_7() { return &___statusText_7; }
	inline void set_statusText_7(Text_t356221433 * value)
	{
		___statusText_7 = value;
		Il2CppCodeGenWriteBarrier(&___statusText_7, value);
	}

	inline static int32_t get_offset_of_m_ProductID_8() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___m_ProductID_8)); }
	inline String_t* get_m_ProductID_8() const { return ___m_ProductID_8; }
	inline String_t** get_address_of_m_ProductID_8() { return &___m_ProductID_8; }
	inline void set_m_ProductID_8(String_t* value)
	{
		___m_ProductID_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_ProductID_8, value);
	}

	inline static int32_t get_offset_of_m_PurchaseCallback_9() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___m_PurchaseCallback_9)); }
	inline Action_1_t1831019615 * get_m_PurchaseCallback_9() const { return ___m_PurchaseCallback_9; }
	inline Action_1_t1831019615 ** get_address_of_m_PurchaseCallback_9() { return &___m_PurchaseCallback_9; }
	inline void set_m_PurchaseCallback_9(Action_1_t1831019615 * value)
	{
		___m_PurchaseCallback_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_PurchaseCallback_9, value);
	}

	inline static int32_t get_offset_of_m_Receipt_10() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t3948335874, ___m_Receipt_10)); }
	inline String_t* get_m_Receipt_10() const { return ___m_Receipt_10; }
	inline String_t** get_address_of_m_Receipt_10() { return &___m_Receipt_10; }
	inline void set_m_Receipt_10(String_t* value)
	{
		___m_Receipt_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Receipt_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
