﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23232738961.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21856579209.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23136648085.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23309152145.h"
#include "AssemblyU2DCSharp_TrainingView_TileData2249013992.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22553450683.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23720882578.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23907470188.h"
#include "Stores_UnityEngine_Purchasing_AppStore379104228.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2994157524.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2994157524;
// System.ObjectDisposedException
struct ObjectDisposedException_t2695136451;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Array
struct Il2CppArray;
// System.ArgumentException
struct ArgumentException_t3259014390;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t1585555208;
// System.ArgumentNullException
struct ArgumentNullException_t628810857;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t279959794;
// System.Type
struct Type_t;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m3352580748_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m928087581_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1391611625_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1783216556_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2965823341_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1739958171_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4177387161_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1394661909_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2938889728_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2613351884_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m495261565_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m435779533_MetadataUsageId;
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m1061591080_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m3158498407_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m1957727328_MetadataUsageId;
extern const uint32_t Enumerator_Dispose_m3217456423_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m1337573311_MetadataUsageId;
extern const uint32_t LinkedList_1__ctor_m1570295574_MetadataUsageId;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1414245146;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m1333791742_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral374702583;
extern Il2CppCodeGenString* _stringLiteral2752284169;
extern const uint32_t LinkedList_1_CopyTo_m3700485924_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_GetObjectData_m2848049047_MetadataUsageId;
extern const uint32_t LinkedList_1_OnDeserialization_m264209791_MetadataUsageId;

// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m226940276_gshared (KeyValuePair_2_t3232738961 * __this, ObscuredInt_t796441056  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1247873036_gshared (KeyValuePair_2_t3232738961 * __this, ObscuredInt_t796441056  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m544020433_gshared (KeyValuePair_2_t3232738961 * __this, ObscuredInt_t796441056  ___key0, ObscuredInt_t796441056  ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Key()
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Key_m3151253958_gshared (KeyValuePair_2_t3232738961 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Value()
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Value_m2166034995_gshared (KeyValuePair_2_t3232738961 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3352580748_gshared (KeyValuePair_2_t3232738961 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1369240259_gshared (KeyValuePair_2_t1856579209 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3545888883_gshared (KeyValuePair_2_t1856579209 * __this, ObscuredInt_t796441056  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1773742016_gshared (KeyValuePair_2_t1856579209 * __this, int32_t ___key0, ObscuredInt_t796441056  ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1496298774_gshared (KeyValuePair_2_t1856579209 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Value()
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Value_m2107329620_gshared (KeyValuePair_2_t1856579209 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m928087581_gshared (KeyValuePair_2_t1856579209 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1545349962_gshared (KeyValuePair_2_t3136648085 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m743944002_gshared (KeyValuePair_2_t3136648085 * __this, float p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2602951305_gshared (KeyValuePair_2_t3136648085 * __this, int32_t ___key0, float ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m58753387_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m303967379_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1783216556_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m808786515_gshared (KeyValuePair_2_t3309152145 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m982241571_gshared (KeyValuePair_2_t3309152145 * __this, TileData_t2249013992  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3955115456_gshared (KeyValuePair_2_t3309152145 * __this, int32_t ___key0, TileData_t2249013992  ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m802503897_gshared (KeyValuePair_2_t3309152145 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::get_Value()
extern "C"  TileData_t2249013992  KeyValuePair_2_get_Value_m871374836_gshared (KeyValuePair_2_t3309152145 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2965823341_gshared (KeyValuePair_2_t3309152145 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3257200763_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m815956739_gshared (KeyValuePair_2_t1683227291 * __this, KeyValuePair_2_t38854645  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m400583398_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2592036086_gshared (KeyValuePair_2_t2553450683 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m99955926_gshared (KeyValuePair_2_t2553450683 * __this, int64_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2353880237_gshared (KeyValuePair_2_t2553450683 * __this, Il2CppObject * ___key0, int64_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m289678647_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m2459084999_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2938889728_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3464331946_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3897037655_gshared (KeyValuePair_2_t3720882578 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2368197927_gshared (KeyValuePair_2_t3720882578 * __this, float p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3796742776_gshared (KeyValuePair_2_t3720882578 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1314509062_gshared (KeyValuePair_2_t3720882578 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m253835334_gshared (KeyValuePair_2_t3720882578 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m495261565_gshared (KeyValuePair_2_t3720882578 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3630760627_gshared (KeyValuePair_2_t3907470188 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1791557123_gshared (KeyValuePair_2_t3907470188 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3254048546_gshared (KeyValuePair_2_t3907470188 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m158792468_gshared (KeyValuePair_2_t3907470188 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2242053076_gshared (KeyValuePair_2_t3907470188 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m435779533_gshared (KeyValuePair_2_t3907470188 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m1586864815_gshared (Enumerator_t1817063546 * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method);
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1957727328_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3217456423_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method);

// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m226940276(__this, p0, method) ((  void (*) (KeyValuePair_2_t3232738961 *, ObscuredInt_t796441056 , const MethodInfo*))KeyValuePair_2_set_Key_m226940276_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1247873036(__this, p0, method) ((  void (*) (KeyValuePair_2_t3232738961 *, ObscuredInt_t796441056 , const MethodInfo*))KeyValuePair_2_set_Value_m1247873036_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m544020433(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3232738961 *, ObscuredInt_t796441056 , ObscuredInt_t796441056 , const MethodInfo*))KeyValuePair_2__ctor_m544020433_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Key()
#define KeyValuePair_2_get_Key_m3151253958(__this, method) ((  ObscuredInt_t796441056  (*) (KeyValuePair_2_t3232738961 *, const MethodInfo*))KeyValuePair_2_get_Key_m3151253958_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Value()
#define KeyValuePair_2_get_Value_m2166034995(__this, method) ((  ObscuredInt_t796441056  (*) (KeyValuePair_2_t3232738961 *, const MethodInfo*))KeyValuePair_2_get_Value_m2166034995_gshared)(__this, method)
// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::ToString()
extern "C"  String_t* ObscuredInt_ToString_m1514839122 (ObscuredInt_t796441056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::ToString()
#define KeyValuePair_2_ToString_m3352580748(__this, method) ((  String_t* (*) (KeyValuePair_2_t3232738961 *, const MethodInfo*))KeyValuePair_2_ToString_m3352580748_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1369240259(__this, p0, method) ((  void (*) (KeyValuePair_2_t1856579209 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1369240259_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3545888883(__this, p0, method) ((  void (*) (KeyValuePair_2_t1856579209 *, ObscuredInt_t796441056 , const MethodInfo*))KeyValuePair_2_set_Value_m3545888883_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1773742016(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1856579209 *, int32_t, ObscuredInt_t796441056 , const MethodInfo*))KeyValuePair_2__ctor_m1773742016_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Key()
#define KeyValuePair_2_get_Key_m1496298774(__this, method) ((  int32_t (*) (KeyValuePair_2_t1856579209 *, const MethodInfo*))KeyValuePair_2_get_Key_m1496298774_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Value()
#define KeyValuePair_2_get_Value_m2107329620(__this, method) ((  ObscuredInt_t796441056  (*) (KeyValuePair_2_t1856579209 *, const MethodInfo*))KeyValuePair_2_get_Value_m2107329620_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::ToString()
#define KeyValuePair_2_ToString_m928087581(__this, method) ((  String_t* (*) (KeyValuePair_2_t1856579209 *, const MethodInfo*))KeyValuePair_2_ToString_m928087581_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1350990071(__this, p0, method) ((  void (*) (KeyValuePair_2_t3749587448 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1350990071_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2726037047(__this, p0, method) ((  void (*) (KeyValuePair_2_t3749587448 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2726037047_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3201181706(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3749587448 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3201181706_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1537018582(__this, method) ((  int32_t (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_get_Key_m1537018582_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2897691047(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_get_Value_m2897691047_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1391611625(__this, method) ((  String_t* (*) (KeyValuePair_2_t3749587448 *, const MethodInfo*))KeyValuePair_2_ToString_m1391611625_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1545349962(__this, p0, method) ((  void (*) (KeyValuePair_2_t3136648085 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1545349962_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m743944002(__this, p0, method) ((  void (*) (KeyValuePair_2_t3136648085 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m743944002_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2602951305(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3136648085 *, int32_t, float, const MethodInfo*))KeyValuePair_2__ctor_m2602951305_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m58753387(__this, method) ((  int32_t (*) (KeyValuePair_2_t3136648085 *, const MethodInfo*))KeyValuePair_2_get_Key_m58753387_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m303967379(__this, method) ((  float (*) (KeyValuePair_2_t3136648085 *, const MethodInfo*))KeyValuePair_2_get_Value_m303967379_gshared)(__this, method)
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m1813392066 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::ToString()
#define KeyValuePair_2_ToString_m1783216556(__this, method) ((  String_t* (*) (KeyValuePair_2_t3136648085 *, const MethodInfo*))KeyValuePair_2_ToString_m1783216556_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m808786515(__this, p0, method) ((  void (*) (KeyValuePair_2_t3309152145 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m808786515_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m982241571(__this, p0, method) ((  void (*) (KeyValuePair_2_t3309152145 *, TileData_t2249013992 , const MethodInfo*))KeyValuePair_2_set_Value_m982241571_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3955115456(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3309152145 *, int32_t, TileData_t2249013992 , const MethodInfo*))KeyValuePair_2__ctor_m3955115456_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::get_Key()
#define KeyValuePair_2_get_Key_m802503897(__this, method) ((  int32_t (*) (KeyValuePair_2_t3309152145 *, const MethodInfo*))KeyValuePair_2_get_Key_m802503897_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::get_Value()
#define KeyValuePair_2_get_Value_m871374836(__this, method) ((  TileData_t2249013992  (*) (KeyValuePair_2_t3309152145 *, const MethodInfo*))KeyValuePair_2_get_Value_m871374836_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::ToString()
#define KeyValuePair_2_ToString_m2965823341(__this, method) ((  String_t* (*) (KeyValuePair_2_t3309152145 *, const MethodInfo*))KeyValuePair_2_ToString_m2965823341_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1222844869(__this, p0, method) ((  void (*) (KeyValuePair_2_t1174980068 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1222844869_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m965533293(__this, p0, method) ((  void (*) (KeyValuePair_2_t1174980068 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m965533293_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4040336782(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1174980068 *, Il2CppObject *, bool, const MethodInfo*))KeyValuePair_2__ctor_m4040336782_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m2113318928(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_get_Key_m2113318928_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m1916631176(__this, method) ((  bool (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_get_Value_m1916631176_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m1253164328 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m1739958171(__this, method) ((  String_t* (*) (KeyValuePair_2_t1174980068 *, const MethodInfo*))KeyValuePair_2_ToString_m1739958171_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3257200763(__this, p0, method) ((  void (*) (KeyValuePair_2_t1683227291 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3257200763_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m815956739(__this, p0, method) ((  void (*) (KeyValuePair_2_t1683227291 *, KeyValuePair_2_t38854645 , const MethodInfo*))KeyValuePair_2_set_Value_m815956739_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m400583398(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1683227291 *, Il2CppObject *, KeyValuePair_2_t38854645 , const MethodInfo*))KeyValuePair_2__ctor_m400583398_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m4247498472(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1683227291 *, const MethodInfo*))KeyValuePair_2_get_Key_m4247498472_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m667219360(__this, method) ((  KeyValuePair_2_t38854645  (*) (KeyValuePair_2_t1683227291 *, const MethodInfo*))KeyValuePair_2_get_Value_m667219360_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
#define KeyValuePair_2_ToString_m4177387161(__this, method) ((  String_t* (*) (KeyValuePair_2_t1683227291 *, const MethodInfo*))KeyValuePair_2_ToString_m4177387161_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1307112735(__this, p0, method) ((  void (*) (KeyValuePair_2_t3716250094 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1307112735_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1921288671(__this, p0, method) ((  void (*) (KeyValuePair_2_t3716250094 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1921288671_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1877755778(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3716250094 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1877755778_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1454531804(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_get_Key_m1454531804_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3699669100(__this, method) ((  int32_t (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_get_Value_m3699669100_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m1394661909(__this, method) ((  String_t* (*) (KeyValuePair_2_t3716250094 *, const MethodInfo*))KeyValuePair_2_ToString_m1394661909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2592036086(__this, p0, method) ((  void (*) (KeyValuePair_2_t2553450683 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2592036086_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m99955926(__this, p0, method) ((  void (*) (KeyValuePair_2_t2553450683 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m99955926_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2353880237(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2553450683 *, Il2CppObject *, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m2353880237_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m289678647(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2553450683 *, const MethodInfo*))KeyValuePair_2_get_Key_m289678647_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m2459084999(__this, method) ((  int64_t (*) (KeyValuePair_2_t2553450683 *, const MethodInfo*))KeyValuePair_2_get_Value_m2459084999_gshared)(__this, method)
// System.String System.Int64::ToString()
extern "C"  String_t* Int64_ToString_m689375889 (int64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m2938889728(__this, method) ((  String_t* (*) (KeyValuePair_2_t2553450683 *, const MethodInfo*))KeyValuePair_2_ToString_m2938889728_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m744486900(__this, p0, method) ((  void (*) (KeyValuePair_2_t38854645 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1416408204(__this, p0, method) ((  void (*) (KeyValuePair_2_t38854645 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3464331946(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t38854645 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3464331946_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3385717033(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_get_Key_m3385717033_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m1251901674(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
#define KeyValuePair_2_ToString_m2613351884(__this, method) ((  String_t* (*) (KeyValuePair_2_t38854645 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3897037655(__this, p0, method) ((  void (*) (KeyValuePair_2_t3720882578 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3897037655_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2368197927(__this, p0, method) ((  void (*) (KeyValuePair_2_t3720882578 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m2368197927_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3796742776(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3720882578 *, Il2CppObject *, float, const MethodInfo*))KeyValuePair_2__ctor_m3796742776_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m1314509062(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3720882578 *, const MethodInfo*))KeyValuePair_2_get_Key_m1314509062_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m253835334(__this, method) ((  float (*) (KeyValuePair_2_t3720882578 *, const MethodInfo*))KeyValuePair_2_get_Value_m253835334_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
#define KeyValuePair_2_ToString_m495261565(__this, method) ((  String_t* (*) (KeyValuePair_2_t3720882578 *, const MethodInfo*))KeyValuePair_2_ToString_m495261565_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3630760627(__this, p0, method) ((  void (*) (KeyValuePair_2_t3907470188 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3630760627_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1791557123(__this, p0, method) ((  void (*) (KeyValuePair_2_t3907470188 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1791557123_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3254048546(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3907470188 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3254048546_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m158792468(__this, method) ((  int32_t (*) (KeyValuePair_2_t3907470188 *, const MethodInfo*))KeyValuePair_2_get_Key_m158792468_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m2242053076(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3907470188 *, const MethodInfo*))KeyValuePair_2_get_Value_m2242053076_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::ToString()
#define KeyValuePair_2_ToString_m435779533(__this, method) ((  String_t* (*) (KeyValuePair_2_t3907470188 *, const MethodInfo*))KeyValuePair_2_ToString_m435779533_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m1586864815(__this, ___parent0, method) ((  void (*) (Enumerator_t1817063546 *, LinkedList_1_t2994157524 *, const MethodInfo*))Enumerator__ctor_m1586864815_gshared)(__this, ___parent0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m3158498407(__this, method) ((  Il2CppObject * (*) (Enumerator_t1817063546 *, const MethodInfo*))Enumerator_get_Current_m3158498407_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3175039148(__this, method) ((  Il2CppObject * (*) (Enumerator_t1817063546 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared)(__this, method)
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m3156784572 (ObjectDisposedException_t2695136451 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2801133788 (InvalidOperationException_t721527559 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1061591080(__this, method) ((  void (*) (Enumerator_t1817063546 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor()
extern "C"  void InvalidOperationException__ctor_m102359810 (InvalidOperationException_t721527559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m1957727328(__this, method) ((  bool (*) (Enumerator_t1817063546 *, const MethodInfo*))Enumerator_MoveNext_m1957727328_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m3217456423(__this, method) ((  void (*) (Enumerator_t1817063546 *, const MethodInfo*))Enumerator_Dispose_m3217456423_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m3739475201 (ArgumentException_t3259014390 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m3380712306 (ArgumentNullException_t628810857 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLowerBound(System.Int32)
extern "C"  int32_t Array_GetLowerBound_m3733237204 (Il2CppArray * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m1595007065 (ArgumentOutOfRangeException_t279959794 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Rank()
extern "C"  int32_t Array_get_Rank_m3837250695 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m544251339 (ArgumentException_t3259014390 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object,System.Type)
extern "C"  void SerializationInfo_AddValue_m1781549036 (SerializationInfo_t228987430 * __this, String_t* p0, Il2CppObject * p1, Type_t * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt32)
extern "C"  void SerializationInfo_AddValue_m383491877 (SerializationInfo_t228987430 * __this, String_t* p0, uint32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
extern "C"  Il2CppObject * SerializationInfo_GetValue_m1127314592 (SerializationInfo_t228987430 * __this, String_t* p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Runtime.Serialization.SerializationInfo::GetUInt32(System.String)
extern "C"  uint32_t SerializationInfo_GetUInt32_m3393505633 (SerializationInfo_t228987430 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m544020433_gshared (KeyValuePair_2_t3232738961 * __this, ObscuredInt_t796441056  ___key0, ObscuredInt_t796441056  ___value1, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = ___key0;
		KeyValuePair_2_set_Key_m226940276((KeyValuePair_2_t3232738961 *)__this, (ObscuredInt_t796441056 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObscuredInt_t796441056  L_1 = ___value1;
		KeyValuePair_2_set_Value_m1247873036((KeyValuePair_2_t3232738961 *)__this, (ObscuredInt_t796441056 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m544020433_AdjustorThunk (Il2CppObject * __this, ObscuredInt_t796441056  ___key0, ObscuredInt_t796441056  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3232738961 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3232738961 *>(__this + 1);
	KeyValuePair_2__ctor_m544020433(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Key()
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Key_m3151253958_gshared (KeyValuePair_2_t3232738961 * __this, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = (ObscuredInt_t796441056 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Key_m3151253958_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3232738961 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3232738961 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3151253958(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m226940276_gshared (KeyValuePair_2_t3232738961 * __this, ObscuredInt_t796441056  ___value0, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m226940276_AdjustorThunk (Il2CppObject * __this, ObscuredInt_t796441056  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3232738961 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3232738961 *>(__this + 1);
	KeyValuePair_2_set_Key_m226940276(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Value()
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Value_m2166034995_gshared (KeyValuePair_2_t3232738961 * __this, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = (ObscuredInt_t796441056 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Value_m2166034995_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3232738961 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3232738961 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2166034995(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1247873036_gshared (KeyValuePair_2_t3232738961 * __this, ObscuredInt_t796441056  ___value0, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1247873036_AdjustorThunk (Il2CppObject * __this, ObscuredInt_t796441056  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3232738961 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3232738961 *>(__this + 1);
	KeyValuePair_2_set_Value_m1247873036(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<CodeStage.AntiCheat.ObscuredTypes.ObscuredInt,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3352580748_gshared (KeyValuePair_2_t3232738961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3352580748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObscuredInt_t796441056  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ObscuredInt_t796441056  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		ObscuredInt_t796441056  L_2 = KeyValuePair_2_get_Key_m3151253958((KeyValuePair_2_t3232738961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		ObscuredInt_t796441056  L_3 = KeyValuePair_2_get_Key_m3151253958((KeyValuePair_2_t3232738961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (ObscuredInt_t796441056 )L_3;
		String_t* L_4 = ObscuredInt_ToString_m1514839122((ObscuredInt_t796441056 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		ObscuredInt_t796441056  L_8 = KeyValuePair_2_get_Value_m2166034995((KeyValuePair_2_t3232738961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ObscuredInt_t796441056  L_9 = KeyValuePair_2_get_Value_m2166034995((KeyValuePair_2_t3232738961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ObscuredInt_t796441056 )L_9;
		String_t* L_10 = ObscuredInt_ToString_m1514839122((ObscuredInt_t796441056 *)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3352580748_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3232738961 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3232738961 *>(__this + 1);
	return KeyValuePair_2_ToString_m3352580748(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1773742016_gshared (KeyValuePair_2_t1856579209 * __this, int32_t ___key0, ObscuredInt_t796441056  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1369240259((KeyValuePair_2_t1856579209 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObscuredInt_t796441056  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3545888883((KeyValuePair_2_t1856579209 *)__this, (ObscuredInt_t796441056 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1773742016_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, ObscuredInt_t796441056  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1856579209 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1856579209 *>(__this + 1);
	KeyValuePair_2__ctor_m1773742016(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1496298774_gshared (KeyValuePair_2_t1856579209 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1496298774_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1856579209 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1856579209 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1496298774(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1369240259_gshared (KeyValuePair_2_t1856579209 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1369240259_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1856579209 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1856579209 *>(__this + 1);
	KeyValuePair_2_set_Key_m1369240259(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::get_Value()
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Value_m2107329620_gshared (KeyValuePair_2_t1856579209 * __this, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = (ObscuredInt_t796441056 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ObscuredInt_t796441056  KeyValuePair_2_get_Value_m2107329620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1856579209 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1856579209 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2107329620(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3545888883_gshared (KeyValuePair_2_t1856579209 * __this, ObscuredInt_t796441056  ___value0, const MethodInfo* method)
{
	{
		ObscuredInt_t796441056  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3545888883_AdjustorThunk (Il2CppObject * __this, ObscuredInt_t796441056  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1856579209 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1856579209 *>(__this + 1);
	KeyValuePair_2_set_Value_m3545888883(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,CodeStage.AntiCheat.ObscuredTypes.ObscuredInt>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m928087581_gshared (KeyValuePair_2_t1856579209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m928087581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ObscuredInt_t796441056  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1496298774((KeyValuePair_2_t1856579209 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1496298774((KeyValuePair_2_t1856579209 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		ObscuredInt_t796441056  L_8 = KeyValuePair_2_get_Value_m2107329620((KeyValuePair_2_t1856579209 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ObscuredInt_t796441056  L_9 = KeyValuePair_2_get_Value_m2107329620((KeyValuePair_2_t1856579209 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ObscuredInt_t796441056 )L_9;
		String_t* L_10 = ObscuredInt_ToString_m1514839122((ObscuredInt_t796441056 *)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m928087581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1856579209 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1856579209 *>(__this + 1);
	return KeyValuePair_2_ToString_m928087581(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1350990071((KeyValuePair_2_t3749587448 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2726037047((KeyValuePair_2_t3749587448 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3201181706_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2__ctor_m3201181706(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1537018582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1537018582(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1350990071_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Key_m1350990071(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2897691047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2897691047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2726037047_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Value_m2726037047(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1391611625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1537018582((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1537018582((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m2897691047((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m2897691047((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_ToString_m1391611625(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2602951305_gshared (KeyValuePair_2_t3136648085 * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1545349962((KeyValuePair_2_t3136648085 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value1;
		KeyValuePair_2_set_Value_m743944002((KeyValuePair_2_t3136648085 *)__this, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2602951305_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, float ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3136648085 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3136648085 *>(__this + 1);
	KeyValuePair_2__ctor_m2602951305(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m58753387_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m58753387_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3136648085 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3136648085 *>(__this + 1);
	return KeyValuePair_2_get_Key_m58753387(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1545349962_gshared (KeyValuePair_2_t3136648085 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1545349962_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3136648085 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3136648085 *>(__this + 1);
	KeyValuePair_2_set_Key_m1545349962(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m303967379_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_1();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Value_m303967379_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3136648085 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3136648085 *>(__this + 1);
	return KeyValuePair_2_get_Value_m303967379(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m743944002_gshared (KeyValuePair_2_t3136648085 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m743944002_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3136648085 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3136648085 *>(__this + 1);
	KeyValuePair_2_set_Value_m743944002(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1783216556_gshared (KeyValuePair_2_t3136648085 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1783216556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m58753387((KeyValuePair_2_t3136648085 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m58753387((KeyValuePair_2_t3136648085 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		float L_8 = KeyValuePair_2_get_Value_m303967379((KeyValuePair_2_t3136648085 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = KeyValuePair_2_get_Value_m303967379((KeyValuePair_2_t3136648085 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		String_t* L_10 = Single_ToString_m1813392066((float*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1783216556_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3136648085 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3136648085 *>(__this + 1);
	return KeyValuePair_2_ToString_m1783216556(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3955115456_gshared (KeyValuePair_2_t3309152145 * __this, int32_t ___key0, TileData_t2249013992  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m808786515((KeyValuePair_2_t3309152145 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TileData_t2249013992  L_1 = ___value1;
		KeyValuePair_2_set_Value_m982241571((KeyValuePair_2_t3309152145 *)__this, (TileData_t2249013992 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3955115456_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, TileData_t2249013992  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3309152145 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3309152145 *>(__this + 1);
	KeyValuePair_2__ctor_m3955115456(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m802503897_gshared (KeyValuePair_2_t3309152145 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m802503897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3309152145 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3309152145 *>(__this + 1);
	return KeyValuePair_2_get_Key_m802503897(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m808786515_gshared (KeyValuePair_2_t3309152145 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m808786515_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3309152145 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3309152145 *>(__this + 1);
	KeyValuePair_2_set_Key_m808786515(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::get_Value()
extern "C"  TileData_t2249013992  KeyValuePair_2_get_Value_m871374836_gshared (KeyValuePair_2_t3309152145 * __this, const MethodInfo* method)
{
	{
		TileData_t2249013992  L_0 = (TileData_t2249013992 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  TileData_t2249013992  KeyValuePair_2_get_Value_m871374836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3309152145 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3309152145 *>(__this + 1);
	return KeyValuePair_2_get_Value_m871374836(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m982241571_gshared (KeyValuePair_2_t3309152145 * __this, TileData_t2249013992  ___value0, const MethodInfo* method)
{
	{
		TileData_t2249013992  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m982241571_AdjustorThunk (Il2CppObject * __this, TileData_t2249013992  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3309152145 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3309152145 *>(__this + 1);
	KeyValuePair_2_set_Value_m982241571(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2965823341_gshared (KeyValuePair_2_t3309152145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2965823341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TileData_t2249013992  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m802503897((KeyValuePair_2_t3309152145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m802503897((KeyValuePair_2_t3309152145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		TileData_t2249013992  L_8 = KeyValuePair_2_get_Value_m871374836((KeyValuePair_2_t3309152145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		TileData_t2249013992  L_9 = KeyValuePair_2_get_Value_m871374836((KeyValuePair_2_t3309152145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (TileData_t2249013992 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2965823341_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3309152145 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3309152145 *>(__this + 1);
	return KeyValuePair_2_ToString_m2965823341(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1222844869((KeyValuePair_2_t1174980068 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m965533293((KeyValuePair_2_t1174980068 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4040336782_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2__ctor_m4040336782(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2113318928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1222844869_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Key_m1222844869(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1916631176(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m965533293_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Value_m965533293(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1739958171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m1253164328((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_ToString_m1739958171(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m400583398_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3257200763((KeyValuePair_2_t1683227291 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_1 = ___value1;
		KeyValuePair_2_set_Value_m815956739((KeyValuePair_2_t1683227291 *)__this, (KeyValuePair_2_t38854645 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m400583398_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2__ctor_m400583398(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4247498472(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3257200763_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3257200763_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2_set_Key_m3257200763(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_get_Value_m667219360(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m815956739_gshared (KeyValuePair_2_t1683227291 * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m815956739_AdjustorThunk (Il2CppObject * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2_set_Value_m815956739(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4177387161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	KeyValuePair_2_t38854645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		KeyValuePair_2_t38854645  L_8 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		KeyValuePair_2_t38854645  L_9 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (KeyValuePair_2_t38854645 )L_9;
		Il2CppFakeBox<KeyValuePair_2_t38854645 > L_10(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__104 = il2cpp_codegen_get_virtual_invoke_data(3, &L_10);
		String_t* L_11 = ((  String_t* (*) (Il2CppObject *, const MethodInfo*))il2cpp_virtual_invoke_data__104.methodPtr)((Il2CppObject *)&L_10, /*hidden argument*/il2cpp_virtual_invoke_data__104.method);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_ToString_m4177387161(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1307112735((KeyValuePair_2_t3716250094 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1921288671((KeyValuePair_2_t3716250094 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1877755778_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2__ctor_m1877755778(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1454531804(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1307112735_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Key_m1307112735(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3699669100(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1921288671_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Value_m1921288671(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1394661909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_ToString_m1394661909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2353880237_gshared (KeyValuePair_2_t2553450683 * __this, Il2CppObject * ___key0, int64_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2592036086((KeyValuePair_2_t2553450683 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m99955926((KeyValuePair_2_t2553450683 *)__this, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2353880237_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int64_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	KeyValuePair_2__ctor_m2353880237(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m289678647_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m289678647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	return KeyValuePair_2_get_Key_m289678647(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2592036086_gshared (KeyValuePair_2_t2553450683 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2592036086_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	KeyValuePair_2_set_Key_m2592036086(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m2459084999_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int64_t KeyValuePair_2_get_Value_m2459084999_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2459084999(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m99955926_gshared (KeyValuePair_2_t2553450683 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m99955926_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	KeyValuePair_2_set_Value_m99955926(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2938889728_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2938889728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int64_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int64_t L_8 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int64_t L_9 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int64_t)L_9;
		String_t* L_10 = Int64_ToString_m689375889((int64_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2938889728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	return KeyValuePair_2_ToString_m2938889728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3464331946_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m744486900((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1416408204((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3464331946_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2__ctor_m3464331946(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3385717033(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m744486900_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Key_m744486900(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1251901674(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1416408204_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Value_m1416408204(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2613351884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3385717033((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3385717033((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m1251901674((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m1251901674((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_ToString_m2613351884(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3796742776_gshared (KeyValuePair_2_t3720882578 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3897037655((KeyValuePair_2_t3720882578 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value1;
		KeyValuePair_2_set_Value_m2368197927((KeyValuePair_2_t3720882578 *)__this, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3796742776_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3720882578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3720882578 *>(__this + 1);
	KeyValuePair_2__ctor_m3796742776(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1314509062_gshared (KeyValuePair_2_t3720882578 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1314509062_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3720882578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3720882578 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1314509062(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3897037655_gshared (KeyValuePair_2_t3720882578 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3897037655_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3720882578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3720882578 *>(__this + 1);
	KeyValuePair_2_set_Key_m3897037655(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m253835334_gshared (KeyValuePair_2_t3720882578 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_1();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Value_m253835334_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3720882578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3720882578 *>(__this + 1);
	return KeyValuePair_2_get_Value_m253835334(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2368197927_gshared (KeyValuePair_2_t3720882578 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2368197927_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3720882578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3720882578 *>(__this + 1);
	KeyValuePair_2_set_Value_m2368197927(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m495261565_gshared (KeyValuePair_2_t3720882578 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m495261565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1314509062((KeyValuePair_2_t3720882578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1314509062((KeyValuePair_2_t3720882578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		float L_8 = KeyValuePair_2_get_Value_m253835334((KeyValuePair_2_t3720882578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = KeyValuePair_2_get_Value_m253835334((KeyValuePair_2_t3720882578 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		String_t* L_10 = Single_ToString_m1813392066((float*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m495261565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3720882578 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3720882578 *>(__this + 1);
	return KeyValuePair_2_ToString_m495261565(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3254048546_gshared (KeyValuePair_2_t3907470188 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3630760627((KeyValuePair_2_t3907470188 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1791557123((KeyValuePair_2_t3907470188 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3254048546_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3907470188 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3907470188 *>(__this + 1);
	KeyValuePair_2__ctor_m3254048546(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m158792468_gshared (KeyValuePair_2_t3907470188 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m158792468_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3907470188 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3907470188 *>(__this + 1);
	return KeyValuePair_2_get_Key_m158792468(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3630760627_gshared (KeyValuePair_2_t3907470188 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3630760627_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3907470188 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3907470188 *>(__this + 1);
	KeyValuePair_2_set_Key_m3630760627(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2242053076_gshared (KeyValuePair_2_t3907470188 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2242053076_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3907470188 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3907470188 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2242053076(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1791557123_gshared (KeyValuePair_2_t3907470188 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1791557123_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3907470188 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3907470188 *>(__this + 1);
	KeyValuePair_2_set_Value_m1791557123(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Purchasing.AppStore,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m435779533_gshared (KeyValuePair_2_t3907470188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m435779533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m158792468((KeyValuePair_2_t3907470188 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m158792468((KeyValuePair_2_t3907470188 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral811305474);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m2242053076((KeyValuePair_2_t3907470188 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m2242053076((KeyValuePair_2_t3907470188 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m435779533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3907470188 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3907470188 *>(__this + 1);
	return KeyValuePair_2_ToString_m435779533(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m1586864815_gshared (Enumerator_t1817063546 * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t2994157524 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2994157524 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_3();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1586864815_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator__ctor_m1586864815(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_Current_m3158498407((Enumerator_t1817063546 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3175039148(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m1061591080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_3();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1061591080(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m3158498407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t1585555208 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t1585555208 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_get_Current_m3158498407(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1957727328_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m1957727328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_3();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2994157524 * L_7 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)L_7->get_first_5();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t1585555208 * L_9 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t1585555208 * L_10 = (LinkedListNode_1_t1585555208 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t1585555208 * L_11 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		LinkedList_1_t2994157524 * L_12 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)L_12->get_first_5();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t1585555208 * L_14 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1957727328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_MoveNext_m1957727328(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3217456423_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m3217456423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_list_0((LinkedList_1_t2994157524 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3217456423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_Dispose_m3217456423(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C"  void LinkedList_1__ctor_m1337573311_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1337573311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_4(L_0);
		__this->set_first_5((LinkedListNode_1_t1585555208 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_3(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1__ctor_m1570295574_gshared (LinkedList_1_t2994157524 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1570295574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t228987430 * L_0 = ___info0;
		__this->set_si_6(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_4(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3874300024_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_gshared (LinkedList_1_t2994157524 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2994157524 *)__this, (ObjectU5BU5D_t3614634134*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4026686671_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		Enumerator_t1817063546  L_0 = ((  Enumerator_t1817063546  (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1817063546  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1585231526_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		Enumerator_t1817063546  L_0 = ((  Enumerator_t1817063546  (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1817063546  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m382194114_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2849171683_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m550341923_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_4();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_VerifyReferencedNode_m1333791742_gshared (LinkedList_1_t2994157524 * __this, LinkedListNode_1_t1585555208 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m1333791742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedListNode_1_t1585555208 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1414245146, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t1585555208 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_2);
		LinkedList_1_t2994157524 * L_3 = ((  LinkedList_1_t2994157524 * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2994157524 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2994157524 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_AddLast_m2912952080_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1585555208 *, LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t1585555208 *)L_2;
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		__this->set_first_5(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t1585555208 * L_5 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_back_3();
		LinkedListNode_1_t1585555208 * L_7 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1585555208 *, LinkedList_1_t2994157524 *, Il2CppObject *, LinkedListNode_1_t1585555208 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t1585555208 *)L_6, (LinkedListNode_1_t1585555208 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t1585555208 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_2();
		__this->set_count_2(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_3();
		__this->set_version_3(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t1585555208 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m3288132428_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3220831510_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		V_0 = (LinkedListNode_1_t1585555208 *)L_0;
		LinkedListNode_1_t1585555208 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t1585555208 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_6;
		LinkedListNode_1_t1585555208 * L_7 = V_0;
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void LinkedList_1_CopyTo_m3700485924_gshared (LinkedList_1_t2994157524 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3700485924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t3614634134* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_5, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t3614634134* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m3837250695((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, (String_t*)_stringLiteral1185213181, (String_t*)_stringLiteral374702583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t3614634134* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t3614634134* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_2();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t3259014390 * L_14 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_14, (String_t*)_stringLiteral2752284169, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t1585555208 * L_15 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		V_0 = (LinkedListNode_1_t1585555208 *)L_15;
		LinkedListNode_1_t1585555208 * L_16 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t3614634134* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t1585555208 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t1585555208 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t1585555208 * L_23 = (LinkedListNode_1_t1585555208 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_23;
		LinkedListNode_1_t1585555208 * L_24 = V_0;
		LinkedListNode_1_t1585555208 * L_25 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_Find_m1218372336_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		V_0 = (LinkedListNode_1_t1585555208 *)L_0;
		LinkedListNode_1_t1585555208 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1585555208 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t1585555208 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t1585555208 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t1585555208 * L_11 = (LinkedListNode_1_t1585555208 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_11;
		LinkedListNode_1_t1585555208 * L_12 = V_0;
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1585555208 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1817063546  LinkedList_1_GetEnumerator_m4181273888_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1817063546  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1586864815(&L_0, (LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1_GetObjectData_m2848049047_gshared (LinkedList_1_t2994157524 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m2848049047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_2();
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2994157524 *)__this, (ObjectU5BU5D_t3614634134*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t228987430 * L_2 = ___info0;
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m1781549036((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral1747804205, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_3();
		NullCheck((SerializationInfo_t228987430 *)L_5);
		SerializationInfo_AddValue_m383491877((SerializationInfo_t228987430 *)L_5, (String_t*)_stringLiteral3617362, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C"  void LinkedList_1_OnDeserialization_m264209791_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m264209791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t228987430 * L_0 = (SerializationInfo_t228987430 *)__this->get_si_6();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t228987430 * L_1 = (SerializationInfo_t228987430 *)__this->get_si_6();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_1, (String_t*)_stringLiteral1747804205, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3614634134* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t3614634134*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t3614634134* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t3614634134* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t228987430 * L_14 = (SerializationInfo_t228987430 *)__this->get_si_6();
		NullCheck((SerializationInfo_t228987430 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m3393505633((SerializationInfo_t228987430 *)L_14, (String_t*)_stringLiteral3617362, /*hidden argument*/NULL);
		__this->set_version_3(L_15);
		__this->set_si_6((SerializationInfo_t228987430 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m2925241645_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		LinkedListNode_1_t1585555208 * L_1 = ((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t1585555208 *)L_1;
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m18160224_gshared (LinkedList_1_t2994157524 * __this, LinkedListNode_1_t1585555208 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_2();
		__this->set_count_2(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_2();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_5((LinkedListNode_1_t1585555208 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t1585555208 * L_3 = ___node0;
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_5 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_forward_2();
		__this->set_first_5(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_3();
		__this->set_version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t1585555208 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_8);
		((  void (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t1585555208 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveFirst()
extern "C"  void LinkedList_1_RemoveFirst_m1001176547_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0017:
	{
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2904732739_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		NullCheck(L_1);
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m402269195_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_2();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_get_First_m3618453197_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_5();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
