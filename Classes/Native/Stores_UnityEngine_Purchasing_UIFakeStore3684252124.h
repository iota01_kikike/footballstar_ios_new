﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"

// UnityEngine.Purchasing.UIFakeStore/DialogRequest
struct DialogRequest_t2092195449;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UIFakeStore
struct  UIFakeStore_t3684252124  : public FakeStore_t3882981564
{
public:
	// UnityEngine.Purchasing.UIFakeStore/DialogRequest UnityEngine.Purchasing.UIFakeStore::m_CurrentDialog
	DialogRequest_t2092195449 * ___m_CurrentDialog_27;
	// System.Int32 UnityEngine.Purchasing.UIFakeStore::m_LastSelectedDropdownIndex
	int32_t ___m_LastSelectedDropdownIndex_28;
	// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::UIFakeStoreCanvasPrefab
	GameObject_t1756533147 * ___UIFakeStoreCanvasPrefab_29;
	// UnityEngine.Canvas UnityEngine.Purchasing.UIFakeStore::m_Canvas
	Canvas_t209405766 * ___m_Canvas_30;
	// UnityEngine.GameObject UnityEngine.Purchasing.UIFakeStore::m_EventSystem
	GameObject_t1756533147 * ___m_EventSystem_31;
	// System.String UnityEngine.Purchasing.UIFakeStore::m_ParentGameObjectPath
	String_t* ___m_ParentGameObjectPath_32;

public:
	inline static int32_t get_offset_of_m_CurrentDialog_27() { return static_cast<int32_t>(offsetof(UIFakeStore_t3684252124, ___m_CurrentDialog_27)); }
	inline DialogRequest_t2092195449 * get_m_CurrentDialog_27() const { return ___m_CurrentDialog_27; }
	inline DialogRequest_t2092195449 ** get_address_of_m_CurrentDialog_27() { return &___m_CurrentDialog_27; }
	inline void set_m_CurrentDialog_27(DialogRequest_t2092195449 * value)
	{
		___m_CurrentDialog_27 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentDialog_27, value);
	}

	inline static int32_t get_offset_of_m_LastSelectedDropdownIndex_28() { return static_cast<int32_t>(offsetof(UIFakeStore_t3684252124, ___m_LastSelectedDropdownIndex_28)); }
	inline int32_t get_m_LastSelectedDropdownIndex_28() const { return ___m_LastSelectedDropdownIndex_28; }
	inline int32_t* get_address_of_m_LastSelectedDropdownIndex_28() { return &___m_LastSelectedDropdownIndex_28; }
	inline void set_m_LastSelectedDropdownIndex_28(int32_t value)
	{
		___m_LastSelectedDropdownIndex_28 = value;
	}

	inline static int32_t get_offset_of_UIFakeStoreCanvasPrefab_29() { return static_cast<int32_t>(offsetof(UIFakeStore_t3684252124, ___UIFakeStoreCanvasPrefab_29)); }
	inline GameObject_t1756533147 * get_UIFakeStoreCanvasPrefab_29() const { return ___UIFakeStoreCanvasPrefab_29; }
	inline GameObject_t1756533147 ** get_address_of_UIFakeStoreCanvasPrefab_29() { return &___UIFakeStoreCanvasPrefab_29; }
	inline void set_UIFakeStoreCanvasPrefab_29(GameObject_t1756533147 * value)
	{
		___UIFakeStoreCanvasPrefab_29 = value;
		Il2CppCodeGenWriteBarrier(&___UIFakeStoreCanvasPrefab_29, value);
	}

	inline static int32_t get_offset_of_m_Canvas_30() { return static_cast<int32_t>(offsetof(UIFakeStore_t3684252124, ___m_Canvas_30)); }
	inline Canvas_t209405766 * get_m_Canvas_30() const { return ___m_Canvas_30; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_30() { return &___m_Canvas_30; }
	inline void set_m_Canvas_30(Canvas_t209405766 * value)
	{
		___m_Canvas_30 = value;
		Il2CppCodeGenWriteBarrier(&___m_Canvas_30, value);
	}

	inline static int32_t get_offset_of_m_EventSystem_31() { return static_cast<int32_t>(offsetof(UIFakeStore_t3684252124, ___m_EventSystem_31)); }
	inline GameObject_t1756533147 * get_m_EventSystem_31() const { return ___m_EventSystem_31; }
	inline GameObject_t1756533147 ** get_address_of_m_EventSystem_31() { return &___m_EventSystem_31; }
	inline void set_m_EventSystem_31(GameObject_t1756533147 * value)
	{
		___m_EventSystem_31 = value;
		Il2CppCodeGenWriteBarrier(&___m_EventSystem_31, value);
	}

	inline static int32_t get_offset_of_m_ParentGameObjectPath_32() { return static_cast<int32_t>(offsetof(UIFakeStore_t3684252124, ___m_ParentGameObjectPath_32)); }
	inline String_t* get_m_ParentGameObjectPath_32() const { return ___m_ParentGameObjectPath_32; }
	inline String_t** get_address_of_m_ParentGameObjectPath_32() { return &___m_ParentGameObjectPath_32; }
	inline void set_m_ParentGameObjectPath_32(String_t* value)
	{
		___m_ParentGameObjectPath_32 = value;
		Il2CppCodeGenWriteBarrier(&___m_ParentGameObjectPath_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
