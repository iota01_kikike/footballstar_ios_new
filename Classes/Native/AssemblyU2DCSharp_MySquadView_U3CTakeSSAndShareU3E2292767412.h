﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MySquadView/<TakeSSAndShare>c__Iterator0
struct  U3CTakeSSAndShareU3Ec__Iterator0_t2292767412  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D MySquadView/<TakeSSAndShare>c__Iterator0::<ss>__0
	Texture2D_t3542995729 * ___U3CssU3E__0_0;
	// System.Int32 MySquadView/<TakeSSAndShare>c__Iterator0::buttonNo
	int32_t ___buttonNo_1;
	// System.Object MySquadView/<TakeSSAndShare>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean MySquadView/<TakeSSAndShare>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 MySquadView/<TakeSSAndShare>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CssU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTakeSSAndShareU3Ec__Iterator0_t2292767412, ___U3CssU3E__0_0)); }
	inline Texture2D_t3542995729 * get_U3CssU3E__0_0() const { return ___U3CssU3E__0_0; }
	inline Texture2D_t3542995729 ** get_address_of_U3CssU3E__0_0() { return &___U3CssU3E__0_0; }
	inline void set_U3CssU3E__0_0(Texture2D_t3542995729 * value)
	{
		___U3CssU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CssU3E__0_0, value);
	}

	inline static int32_t get_offset_of_buttonNo_1() { return static_cast<int32_t>(offsetof(U3CTakeSSAndShareU3Ec__Iterator0_t2292767412, ___buttonNo_1)); }
	inline int32_t get_buttonNo_1() const { return ___buttonNo_1; }
	inline int32_t* get_address_of_buttonNo_1() { return &___buttonNo_1; }
	inline void set_buttonNo_1(int32_t value)
	{
		___buttonNo_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CTakeSSAndShareU3Ec__Iterator0_t2292767412, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CTakeSSAndShareU3Ec__Iterator0_t2292767412, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CTakeSSAndShareU3Ec__Iterator0_t2292767412, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
