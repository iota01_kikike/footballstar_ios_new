﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t1627764765;
// UnityEngine.Purchasing.IMoolahExtension
struct IMoolahExtension_t3195861654;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_t3429739537;
// UnityEngine.Purchasing.IMicrosoftExtensions
struct IMicrosoftExtensions_t1101930285;
// UnityEngine.Purchasing.IUnityChannelExtensions
struct IUnityChannelExtensions_t4012708657;
// UnityEngine.Purchasing.ITransactionHistoryExtensions
struct ITransactionHistoryExtensions_t600227109;
// System.String
struct String_t;
// IAPDemo/UnityChannelLoginHandler
struct UnityChannelLoginHandler_t2253884029;
// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI>
struct Dictionary_2_t1568147840;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action`1<System.String>
struct Action_1_t1831019615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo
struct  IAPDemo_t823941185  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Purchasing.IStoreController IAPDemo::m_Controller
	Il2CppObject * ___m_Controller_2;
	// UnityEngine.Purchasing.IAppleExtensions IAPDemo::m_AppleExtensions
	Il2CppObject * ___m_AppleExtensions_3;
	// UnityEngine.Purchasing.IMoolahExtension IAPDemo::m_MoolahExtensions
	Il2CppObject * ___m_MoolahExtensions_4;
	// UnityEngine.Purchasing.ISamsungAppsExtensions IAPDemo::m_SamsungExtensions
	Il2CppObject * ___m_SamsungExtensions_5;
	// UnityEngine.Purchasing.IMicrosoftExtensions IAPDemo::m_MicrosoftExtensions
	Il2CppObject * ___m_MicrosoftExtensions_6;
	// UnityEngine.Purchasing.IUnityChannelExtensions IAPDemo::m_UnityChannelExtensions
	Il2CppObject * ___m_UnityChannelExtensions_7;
	// UnityEngine.Purchasing.ITransactionHistoryExtensions IAPDemo::m_TransactionHistoryExtensions
	Il2CppObject * ___m_TransactionHistoryExtensions_8;
	// System.Boolean IAPDemo::m_IsGooglePlayStoreSelected
	bool ___m_IsGooglePlayStoreSelected_9;
	// System.Boolean IAPDemo::m_IsSamsungAppsStoreSelected
	bool ___m_IsSamsungAppsStoreSelected_10;
	// System.Boolean IAPDemo::m_IsCloudMoolahStoreSelected
	bool ___m_IsCloudMoolahStoreSelected_11;
	// System.Boolean IAPDemo::m_IsUnityChannelSelected
	bool ___m_IsUnityChannelSelected_12;
	// System.String IAPDemo::m_LastTransactionID
	String_t* ___m_LastTransactionID_13;
	// System.Boolean IAPDemo::m_IsLoggedIn
	bool ___m_IsLoggedIn_14;
	// IAPDemo/UnityChannelLoginHandler IAPDemo::unityChannelLoginHandler
	UnityChannelLoginHandler_t2253884029 * ___unityChannelLoginHandler_15;
	// System.Boolean IAPDemo::m_FetchReceiptPayloadOnPurchase
	bool ___m_FetchReceiptPayloadOnPurchase_16;
	// System.Boolean IAPDemo::m_PurchaseInProgress
	bool ___m_PurchaseInProgress_17;
	// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI> IAPDemo::m_ProductUIs
	Dictionary_2_t1568147840 * ___m_ProductUIs_18;
	// UnityEngine.GameObject IAPDemo::productUITemplate
	GameObject_t1756533147 * ___productUITemplate_19;
	// UnityEngine.RectTransform IAPDemo::contentRect
	RectTransform_t3349966182 * ___contentRect_20;
	// UnityEngine.UI.Button IAPDemo::restoreButton
	Button_t2872111280 * ___restoreButton_21;
	// UnityEngine.UI.Button IAPDemo::loginButton
	Button_t2872111280 * ___loginButton_22;
	// UnityEngine.UI.Button IAPDemo::validateButton
	Button_t2872111280 * ___validateButton_23;
	// UnityEngine.UI.Text IAPDemo::versionText
	Text_t356221433 * ___versionText_24;

public:
	inline static int32_t get_offset_of_m_Controller_2() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_Controller_2)); }
	inline Il2CppObject * get_m_Controller_2() const { return ___m_Controller_2; }
	inline Il2CppObject ** get_address_of_m_Controller_2() { return &___m_Controller_2; }
	inline void set_m_Controller_2(Il2CppObject * value)
	{
		___m_Controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Controller_2, value);
	}

	inline static int32_t get_offset_of_m_AppleExtensions_3() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_AppleExtensions_3)); }
	inline Il2CppObject * get_m_AppleExtensions_3() const { return ___m_AppleExtensions_3; }
	inline Il2CppObject ** get_address_of_m_AppleExtensions_3() { return &___m_AppleExtensions_3; }
	inline void set_m_AppleExtensions_3(Il2CppObject * value)
	{
		___m_AppleExtensions_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_AppleExtensions_3, value);
	}

	inline static int32_t get_offset_of_m_MoolahExtensions_4() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_MoolahExtensions_4)); }
	inline Il2CppObject * get_m_MoolahExtensions_4() const { return ___m_MoolahExtensions_4; }
	inline Il2CppObject ** get_address_of_m_MoolahExtensions_4() { return &___m_MoolahExtensions_4; }
	inline void set_m_MoolahExtensions_4(Il2CppObject * value)
	{
		___m_MoolahExtensions_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_MoolahExtensions_4, value);
	}

	inline static int32_t get_offset_of_m_SamsungExtensions_5() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_SamsungExtensions_5)); }
	inline Il2CppObject * get_m_SamsungExtensions_5() const { return ___m_SamsungExtensions_5; }
	inline Il2CppObject ** get_address_of_m_SamsungExtensions_5() { return &___m_SamsungExtensions_5; }
	inline void set_m_SamsungExtensions_5(Il2CppObject * value)
	{
		___m_SamsungExtensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_SamsungExtensions_5, value);
	}

	inline static int32_t get_offset_of_m_MicrosoftExtensions_6() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_MicrosoftExtensions_6)); }
	inline Il2CppObject * get_m_MicrosoftExtensions_6() const { return ___m_MicrosoftExtensions_6; }
	inline Il2CppObject ** get_address_of_m_MicrosoftExtensions_6() { return &___m_MicrosoftExtensions_6; }
	inline void set_m_MicrosoftExtensions_6(Il2CppObject * value)
	{
		___m_MicrosoftExtensions_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_MicrosoftExtensions_6, value);
	}

	inline static int32_t get_offset_of_m_UnityChannelExtensions_7() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_UnityChannelExtensions_7)); }
	inline Il2CppObject * get_m_UnityChannelExtensions_7() const { return ___m_UnityChannelExtensions_7; }
	inline Il2CppObject ** get_address_of_m_UnityChannelExtensions_7() { return &___m_UnityChannelExtensions_7; }
	inline void set_m_UnityChannelExtensions_7(Il2CppObject * value)
	{
		___m_UnityChannelExtensions_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_UnityChannelExtensions_7, value);
	}

	inline static int32_t get_offset_of_m_TransactionHistoryExtensions_8() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_TransactionHistoryExtensions_8)); }
	inline Il2CppObject * get_m_TransactionHistoryExtensions_8() const { return ___m_TransactionHistoryExtensions_8; }
	inline Il2CppObject ** get_address_of_m_TransactionHistoryExtensions_8() { return &___m_TransactionHistoryExtensions_8; }
	inline void set_m_TransactionHistoryExtensions_8(Il2CppObject * value)
	{
		___m_TransactionHistoryExtensions_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_TransactionHistoryExtensions_8, value);
	}

	inline static int32_t get_offset_of_m_IsGooglePlayStoreSelected_9() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_IsGooglePlayStoreSelected_9)); }
	inline bool get_m_IsGooglePlayStoreSelected_9() const { return ___m_IsGooglePlayStoreSelected_9; }
	inline bool* get_address_of_m_IsGooglePlayStoreSelected_9() { return &___m_IsGooglePlayStoreSelected_9; }
	inline void set_m_IsGooglePlayStoreSelected_9(bool value)
	{
		___m_IsGooglePlayStoreSelected_9 = value;
	}

	inline static int32_t get_offset_of_m_IsSamsungAppsStoreSelected_10() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_IsSamsungAppsStoreSelected_10)); }
	inline bool get_m_IsSamsungAppsStoreSelected_10() const { return ___m_IsSamsungAppsStoreSelected_10; }
	inline bool* get_address_of_m_IsSamsungAppsStoreSelected_10() { return &___m_IsSamsungAppsStoreSelected_10; }
	inline void set_m_IsSamsungAppsStoreSelected_10(bool value)
	{
		___m_IsSamsungAppsStoreSelected_10 = value;
	}

	inline static int32_t get_offset_of_m_IsCloudMoolahStoreSelected_11() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_IsCloudMoolahStoreSelected_11)); }
	inline bool get_m_IsCloudMoolahStoreSelected_11() const { return ___m_IsCloudMoolahStoreSelected_11; }
	inline bool* get_address_of_m_IsCloudMoolahStoreSelected_11() { return &___m_IsCloudMoolahStoreSelected_11; }
	inline void set_m_IsCloudMoolahStoreSelected_11(bool value)
	{
		___m_IsCloudMoolahStoreSelected_11 = value;
	}

	inline static int32_t get_offset_of_m_IsUnityChannelSelected_12() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_IsUnityChannelSelected_12)); }
	inline bool get_m_IsUnityChannelSelected_12() const { return ___m_IsUnityChannelSelected_12; }
	inline bool* get_address_of_m_IsUnityChannelSelected_12() { return &___m_IsUnityChannelSelected_12; }
	inline void set_m_IsUnityChannelSelected_12(bool value)
	{
		___m_IsUnityChannelSelected_12 = value;
	}

	inline static int32_t get_offset_of_m_LastTransactionID_13() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_LastTransactionID_13)); }
	inline String_t* get_m_LastTransactionID_13() const { return ___m_LastTransactionID_13; }
	inline String_t** get_address_of_m_LastTransactionID_13() { return &___m_LastTransactionID_13; }
	inline void set_m_LastTransactionID_13(String_t* value)
	{
		___m_LastTransactionID_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_LastTransactionID_13, value);
	}

	inline static int32_t get_offset_of_m_IsLoggedIn_14() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_IsLoggedIn_14)); }
	inline bool get_m_IsLoggedIn_14() const { return ___m_IsLoggedIn_14; }
	inline bool* get_address_of_m_IsLoggedIn_14() { return &___m_IsLoggedIn_14; }
	inline void set_m_IsLoggedIn_14(bool value)
	{
		___m_IsLoggedIn_14 = value;
	}

	inline static int32_t get_offset_of_unityChannelLoginHandler_15() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___unityChannelLoginHandler_15)); }
	inline UnityChannelLoginHandler_t2253884029 * get_unityChannelLoginHandler_15() const { return ___unityChannelLoginHandler_15; }
	inline UnityChannelLoginHandler_t2253884029 ** get_address_of_unityChannelLoginHandler_15() { return &___unityChannelLoginHandler_15; }
	inline void set_unityChannelLoginHandler_15(UnityChannelLoginHandler_t2253884029 * value)
	{
		___unityChannelLoginHandler_15 = value;
		Il2CppCodeGenWriteBarrier(&___unityChannelLoginHandler_15, value);
	}

	inline static int32_t get_offset_of_m_FetchReceiptPayloadOnPurchase_16() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_FetchReceiptPayloadOnPurchase_16)); }
	inline bool get_m_FetchReceiptPayloadOnPurchase_16() const { return ___m_FetchReceiptPayloadOnPurchase_16; }
	inline bool* get_address_of_m_FetchReceiptPayloadOnPurchase_16() { return &___m_FetchReceiptPayloadOnPurchase_16; }
	inline void set_m_FetchReceiptPayloadOnPurchase_16(bool value)
	{
		___m_FetchReceiptPayloadOnPurchase_16 = value;
	}

	inline static int32_t get_offset_of_m_PurchaseInProgress_17() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_PurchaseInProgress_17)); }
	inline bool get_m_PurchaseInProgress_17() const { return ___m_PurchaseInProgress_17; }
	inline bool* get_address_of_m_PurchaseInProgress_17() { return &___m_PurchaseInProgress_17; }
	inline void set_m_PurchaseInProgress_17(bool value)
	{
		___m_PurchaseInProgress_17 = value;
	}

	inline static int32_t get_offset_of_m_ProductUIs_18() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___m_ProductUIs_18)); }
	inline Dictionary_2_t1568147840 * get_m_ProductUIs_18() const { return ___m_ProductUIs_18; }
	inline Dictionary_2_t1568147840 ** get_address_of_m_ProductUIs_18() { return &___m_ProductUIs_18; }
	inline void set_m_ProductUIs_18(Dictionary_2_t1568147840 * value)
	{
		___m_ProductUIs_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_ProductUIs_18, value);
	}

	inline static int32_t get_offset_of_productUITemplate_19() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___productUITemplate_19)); }
	inline GameObject_t1756533147 * get_productUITemplate_19() const { return ___productUITemplate_19; }
	inline GameObject_t1756533147 ** get_address_of_productUITemplate_19() { return &___productUITemplate_19; }
	inline void set_productUITemplate_19(GameObject_t1756533147 * value)
	{
		___productUITemplate_19 = value;
		Il2CppCodeGenWriteBarrier(&___productUITemplate_19, value);
	}

	inline static int32_t get_offset_of_contentRect_20() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___contentRect_20)); }
	inline RectTransform_t3349966182 * get_contentRect_20() const { return ___contentRect_20; }
	inline RectTransform_t3349966182 ** get_address_of_contentRect_20() { return &___contentRect_20; }
	inline void set_contentRect_20(RectTransform_t3349966182 * value)
	{
		___contentRect_20 = value;
		Il2CppCodeGenWriteBarrier(&___contentRect_20, value);
	}

	inline static int32_t get_offset_of_restoreButton_21() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___restoreButton_21)); }
	inline Button_t2872111280 * get_restoreButton_21() const { return ___restoreButton_21; }
	inline Button_t2872111280 ** get_address_of_restoreButton_21() { return &___restoreButton_21; }
	inline void set_restoreButton_21(Button_t2872111280 * value)
	{
		___restoreButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___restoreButton_21, value);
	}

	inline static int32_t get_offset_of_loginButton_22() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___loginButton_22)); }
	inline Button_t2872111280 * get_loginButton_22() const { return ___loginButton_22; }
	inline Button_t2872111280 ** get_address_of_loginButton_22() { return &___loginButton_22; }
	inline void set_loginButton_22(Button_t2872111280 * value)
	{
		___loginButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___loginButton_22, value);
	}

	inline static int32_t get_offset_of_validateButton_23() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___validateButton_23)); }
	inline Button_t2872111280 * get_validateButton_23() const { return ___validateButton_23; }
	inline Button_t2872111280 ** get_address_of_validateButton_23() { return &___validateButton_23; }
	inline void set_validateButton_23(Button_t2872111280 * value)
	{
		___validateButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___validateButton_23, value);
	}

	inline static int32_t get_offset_of_versionText_24() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185, ___versionText_24)); }
	inline Text_t356221433 * get_versionText_24() const { return ___versionText_24; }
	inline Text_t356221433 ** get_address_of_versionText_24() { return &___versionText_24; }
	inline void set_versionText_24(Text_t356221433 * value)
	{
		___versionText_24 = value;
		Il2CppCodeGenWriteBarrier(&___versionText_24, value);
	}
};

struct IAPDemo_t823941185_StaticFields
{
public:
	// System.Action`1<System.String> IAPDemo::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_25;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_25() { return static_cast<int32_t>(offsetof(IAPDemo_t823941185_StaticFields, ___U3CU3Ef__amU24cache0_25)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_25() const { return ___U3CU3Ef__amU24cache0_25; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_25() { return &___U3CU3Ef__amU24cache0_25; }
	inline void set_U3CU3Ef__amU24cache0_25(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
