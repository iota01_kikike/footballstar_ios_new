﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<DataRow>
struct List_1_t1288251928;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataTable
struct  DataTable_t427943864  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> DataTable::<Columns>k__BackingField
	List_1_t1398341365 * ___U3CColumnsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<DataRow> DataTable::<Rows>k__BackingField
	List_1_t1288251928 * ___U3CRowsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CColumnsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DataTable_t427943864, ___U3CColumnsU3Ek__BackingField_0)); }
	inline List_1_t1398341365 * get_U3CColumnsU3Ek__BackingField_0() const { return ___U3CColumnsU3Ek__BackingField_0; }
	inline List_1_t1398341365 ** get_address_of_U3CColumnsU3Ek__BackingField_0() { return &___U3CColumnsU3Ek__BackingField_0; }
	inline void set_U3CColumnsU3Ek__BackingField_0(List_1_t1398341365 * value)
	{
		___U3CColumnsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CColumnsU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CRowsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DataTable_t427943864, ___U3CRowsU3Ek__BackingField_1)); }
	inline List_1_t1288251928 * get_U3CRowsU3Ek__BackingField_1() const { return ___U3CRowsU3Ek__BackingField_1; }
	inline List_1_t1288251928 ** get_address_of_U3CRowsU3Ek__BackingField_1() { return &___U3CRowsU3Ek__BackingField_1; }
	inline void set_U3CRowsU3Ek__BackingField_1(List_1_t1288251928 * value)
	{
		___U3CRowsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRowsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
