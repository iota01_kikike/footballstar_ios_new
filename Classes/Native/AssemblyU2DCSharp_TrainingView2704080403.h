﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.Dictionary`2<System.Int32,TrainingView/TileData>
struct Dictionary_2_t1256839627;
// UnityEngine.UI.Image
struct Image_t2042527209;
// DG.Tweening.Sequence
struct Sequence_t110643099;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>,System.Int32>
struct Func_2_t3951659556;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>,TrainingView/TileData>
struct Func_2_t4128796100;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView
struct  TrainingView_t2704080403  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject TrainingView::TileBase
	GameObject_t1756533147 * ___TileBase_2;
	// UnityEngine.GameObject TrainingView::ScorePanel
	GameObject_t1756533147 * ___ScorePanel_3;
	// UnityEngine.UI.Text TrainingView::HighScoreText
	Text_t356221433 * ___HighScoreText_4;
	// UnityEngine.UI.Text TrainingView::ScoreText
	Text_t356221433 * ___ScoreText_5;
	// UnityEngine.UI.Button TrainingView::DoubleButton
	Button_t2872111280 * ___DoubleButton_6;
	// UnityEngine.UI.Text TrainingView::ChanceText
	Text_t356221433 * ___ChanceText_7;
	// UnityEngine.UI.Text TrainingView::Double2Text
	Text_t356221433 * ___Double2Text_8;
	// UnityEngine.UI.Text TrainingView::DoubleRemainText
	Text_t356221433 * ___DoubleRemainText_9;
	// UnityEngine.UI.Button TrainingView::LeaderboardButton
	Button_t2872111280 * ___LeaderboardButton_10;
	// UnityEngine.UI.Button TrainingView::InfoButton
	Button_t2872111280 * ___InfoButton_11;
	// UnityEngine.UI.Button TrainingView::ResetButton
	Button_t2872111280 * ___ResetButton_12;
	// UnityEngine.Material TrainingView::GrayscaleMat
	Material_t193706927 * ___GrayscaleMat_13;
	// UnityEngine.Material TrainingView::TileOffMat
	Material_t193706927 * ___TileOffMat_14;
	// UnityEngine.Material TrainingView::TileOnMat
	Material_t193706927 * ___TileOnMat_15;
	// UnityEngine.GameObject[] TrainingView::tileList
	GameObjectU5BU5D_t3057952154* ___tileList_16;
	// System.Collections.Generic.Dictionary`2<System.Int32,TrainingView/TileData> TrainingView::traineeDict
	Dictionary_2_t1256839627 * ___traineeDict_17;
	// System.Int32 TrainingView::turn
	int32_t ___turn_18;
	// UnityEngine.Vector3 TrainingView::unitOriginScale
	Vector3_t2243707580  ___unitOriginScale_19;
	// UnityEngine.Vector3 TrainingView::unitZoomScale
	Vector3_t2243707580  ___unitZoomScale_20;
	// UnityEngine.Vector2 TrainingView::beginPos
	Vector2_t2243707579  ___beginPos_21;
	// System.Boolean TrainingView::isMoving
	bool ___isMoving_22;
	// System.Boolean TrainingView::finDragging
	bool ___finDragging_23;
	// UnityEngine.Vector3 TrainingView::unitCenterPos
	Vector3_t2243707580  ___unitCenterPos_24;
	// UnityEngine.UI.Image TrainingView::coinIcon
	Image_t2042527209 * ___coinIcon_27;
	// UnityEngine.UI.Image TrainingView::gemIcon
	Image_t2042527209 * ___gemIcon_28;
	// UnityEngine.Vector3 TrainingView::coinIconOriginScale
	Vector3_t2243707580  ___coinIconOriginScale_29;
	// UnityEngine.Vector3 TrainingView::gemIconOriginScale
	Vector3_t2243707580  ___gemIconOriginScale_30;
	// DG.Tweening.Sequence TrainingView::doubleSeq
	Sequence_t110643099 * ___doubleSeq_31;

public:
	inline static int32_t get_offset_of_TileBase_2() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___TileBase_2)); }
	inline GameObject_t1756533147 * get_TileBase_2() const { return ___TileBase_2; }
	inline GameObject_t1756533147 ** get_address_of_TileBase_2() { return &___TileBase_2; }
	inline void set_TileBase_2(GameObject_t1756533147 * value)
	{
		___TileBase_2 = value;
		Il2CppCodeGenWriteBarrier(&___TileBase_2, value);
	}

	inline static int32_t get_offset_of_ScorePanel_3() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___ScorePanel_3)); }
	inline GameObject_t1756533147 * get_ScorePanel_3() const { return ___ScorePanel_3; }
	inline GameObject_t1756533147 ** get_address_of_ScorePanel_3() { return &___ScorePanel_3; }
	inline void set_ScorePanel_3(GameObject_t1756533147 * value)
	{
		___ScorePanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___ScorePanel_3, value);
	}

	inline static int32_t get_offset_of_HighScoreText_4() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___HighScoreText_4)); }
	inline Text_t356221433 * get_HighScoreText_4() const { return ___HighScoreText_4; }
	inline Text_t356221433 ** get_address_of_HighScoreText_4() { return &___HighScoreText_4; }
	inline void set_HighScoreText_4(Text_t356221433 * value)
	{
		___HighScoreText_4 = value;
		Il2CppCodeGenWriteBarrier(&___HighScoreText_4, value);
	}

	inline static int32_t get_offset_of_ScoreText_5() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___ScoreText_5)); }
	inline Text_t356221433 * get_ScoreText_5() const { return ___ScoreText_5; }
	inline Text_t356221433 ** get_address_of_ScoreText_5() { return &___ScoreText_5; }
	inline void set_ScoreText_5(Text_t356221433 * value)
	{
		___ScoreText_5 = value;
		Il2CppCodeGenWriteBarrier(&___ScoreText_5, value);
	}

	inline static int32_t get_offset_of_DoubleButton_6() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___DoubleButton_6)); }
	inline Button_t2872111280 * get_DoubleButton_6() const { return ___DoubleButton_6; }
	inline Button_t2872111280 ** get_address_of_DoubleButton_6() { return &___DoubleButton_6; }
	inline void set_DoubleButton_6(Button_t2872111280 * value)
	{
		___DoubleButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___DoubleButton_6, value);
	}

	inline static int32_t get_offset_of_ChanceText_7() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___ChanceText_7)); }
	inline Text_t356221433 * get_ChanceText_7() const { return ___ChanceText_7; }
	inline Text_t356221433 ** get_address_of_ChanceText_7() { return &___ChanceText_7; }
	inline void set_ChanceText_7(Text_t356221433 * value)
	{
		___ChanceText_7 = value;
		Il2CppCodeGenWriteBarrier(&___ChanceText_7, value);
	}

	inline static int32_t get_offset_of_Double2Text_8() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___Double2Text_8)); }
	inline Text_t356221433 * get_Double2Text_8() const { return ___Double2Text_8; }
	inline Text_t356221433 ** get_address_of_Double2Text_8() { return &___Double2Text_8; }
	inline void set_Double2Text_8(Text_t356221433 * value)
	{
		___Double2Text_8 = value;
		Il2CppCodeGenWriteBarrier(&___Double2Text_8, value);
	}

	inline static int32_t get_offset_of_DoubleRemainText_9() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___DoubleRemainText_9)); }
	inline Text_t356221433 * get_DoubleRemainText_9() const { return ___DoubleRemainText_9; }
	inline Text_t356221433 ** get_address_of_DoubleRemainText_9() { return &___DoubleRemainText_9; }
	inline void set_DoubleRemainText_9(Text_t356221433 * value)
	{
		___DoubleRemainText_9 = value;
		Il2CppCodeGenWriteBarrier(&___DoubleRemainText_9, value);
	}

	inline static int32_t get_offset_of_LeaderboardButton_10() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___LeaderboardButton_10)); }
	inline Button_t2872111280 * get_LeaderboardButton_10() const { return ___LeaderboardButton_10; }
	inline Button_t2872111280 ** get_address_of_LeaderboardButton_10() { return &___LeaderboardButton_10; }
	inline void set_LeaderboardButton_10(Button_t2872111280 * value)
	{
		___LeaderboardButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___LeaderboardButton_10, value);
	}

	inline static int32_t get_offset_of_InfoButton_11() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___InfoButton_11)); }
	inline Button_t2872111280 * get_InfoButton_11() const { return ___InfoButton_11; }
	inline Button_t2872111280 ** get_address_of_InfoButton_11() { return &___InfoButton_11; }
	inline void set_InfoButton_11(Button_t2872111280 * value)
	{
		___InfoButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___InfoButton_11, value);
	}

	inline static int32_t get_offset_of_ResetButton_12() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___ResetButton_12)); }
	inline Button_t2872111280 * get_ResetButton_12() const { return ___ResetButton_12; }
	inline Button_t2872111280 ** get_address_of_ResetButton_12() { return &___ResetButton_12; }
	inline void set_ResetButton_12(Button_t2872111280 * value)
	{
		___ResetButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___ResetButton_12, value);
	}

	inline static int32_t get_offset_of_GrayscaleMat_13() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___GrayscaleMat_13)); }
	inline Material_t193706927 * get_GrayscaleMat_13() const { return ___GrayscaleMat_13; }
	inline Material_t193706927 ** get_address_of_GrayscaleMat_13() { return &___GrayscaleMat_13; }
	inline void set_GrayscaleMat_13(Material_t193706927 * value)
	{
		___GrayscaleMat_13 = value;
		Il2CppCodeGenWriteBarrier(&___GrayscaleMat_13, value);
	}

	inline static int32_t get_offset_of_TileOffMat_14() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___TileOffMat_14)); }
	inline Material_t193706927 * get_TileOffMat_14() const { return ___TileOffMat_14; }
	inline Material_t193706927 ** get_address_of_TileOffMat_14() { return &___TileOffMat_14; }
	inline void set_TileOffMat_14(Material_t193706927 * value)
	{
		___TileOffMat_14 = value;
		Il2CppCodeGenWriteBarrier(&___TileOffMat_14, value);
	}

	inline static int32_t get_offset_of_TileOnMat_15() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___TileOnMat_15)); }
	inline Material_t193706927 * get_TileOnMat_15() const { return ___TileOnMat_15; }
	inline Material_t193706927 ** get_address_of_TileOnMat_15() { return &___TileOnMat_15; }
	inline void set_TileOnMat_15(Material_t193706927 * value)
	{
		___TileOnMat_15 = value;
		Il2CppCodeGenWriteBarrier(&___TileOnMat_15, value);
	}

	inline static int32_t get_offset_of_tileList_16() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___tileList_16)); }
	inline GameObjectU5BU5D_t3057952154* get_tileList_16() const { return ___tileList_16; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_tileList_16() { return &___tileList_16; }
	inline void set_tileList_16(GameObjectU5BU5D_t3057952154* value)
	{
		___tileList_16 = value;
		Il2CppCodeGenWriteBarrier(&___tileList_16, value);
	}

	inline static int32_t get_offset_of_traineeDict_17() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___traineeDict_17)); }
	inline Dictionary_2_t1256839627 * get_traineeDict_17() const { return ___traineeDict_17; }
	inline Dictionary_2_t1256839627 ** get_address_of_traineeDict_17() { return &___traineeDict_17; }
	inline void set_traineeDict_17(Dictionary_2_t1256839627 * value)
	{
		___traineeDict_17 = value;
		Il2CppCodeGenWriteBarrier(&___traineeDict_17, value);
	}

	inline static int32_t get_offset_of_turn_18() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___turn_18)); }
	inline int32_t get_turn_18() const { return ___turn_18; }
	inline int32_t* get_address_of_turn_18() { return &___turn_18; }
	inline void set_turn_18(int32_t value)
	{
		___turn_18 = value;
	}

	inline static int32_t get_offset_of_unitOriginScale_19() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___unitOriginScale_19)); }
	inline Vector3_t2243707580  get_unitOriginScale_19() const { return ___unitOriginScale_19; }
	inline Vector3_t2243707580 * get_address_of_unitOriginScale_19() { return &___unitOriginScale_19; }
	inline void set_unitOriginScale_19(Vector3_t2243707580  value)
	{
		___unitOriginScale_19 = value;
	}

	inline static int32_t get_offset_of_unitZoomScale_20() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___unitZoomScale_20)); }
	inline Vector3_t2243707580  get_unitZoomScale_20() const { return ___unitZoomScale_20; }
	inline Vector3_t2243707580 * get_address_of_unitZoomScale_20() { return &___unitZoomScale_20; }
	inline void set_unitZoomScale_20(Vector3_t2243707580  value)
	{
		___unitZoomScale_20 = value;
	}

	inline static int32_t get_offset_of_beginPos_21() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___beginPos_21)); }
	inline Vector2_t2243707579  get_beginPos_21() const { return ___beginPos_21; }
	inline Vector2_t2243707579 * get_address_of_beginPos_21() { return &___beginPos_21; }
	inline void set_beginPos_21(Vector2_t2243707579  value)
	{
		___beginPos_21 = value;
	}

	inline static int32_t get_offset_of_isMoving_22() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___isMoving_22)); }
	inline bool get_isMoving_22() const { return ___isMoving_22; }
	inline bool* get_address_of_isMoving_22() { return &___isMoving_22; }
	inline void set_isMoving_22(bool value)
	{
		___isMoving_22 = value;
	}

	inline static int32_t get_offset_of_finDragging_23() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___finDragging_23)); }
	inline bool get_finDragging_23() const { return ___finDragging_23; }
	inline bool* get_address_of_finDragging_23() { return &___finDragging_23; }
	inline void set_finDragging_23(bool value)
	{
		___finDragging_23 = value;
	}

	inline static int32_t get_offset_of_unitCenterPos_24() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___unitCenterPos_24)); }
	inline Vector3_t2243707580  get_unitCenterPos_24() const { return ___unitCenterPos_24; }
	inline Vector3_t2243707580 * get_address_of_unitCenterPos_24() { return &___unitCenterPos_24; }
	inline void set_unitCenterPos_24(Vector3_t2243707580  value)
	{
		___unitCenterPos_24 = value;
	}

	inline static int32_t get_offset_of_coinIcon_27() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___coinIcon_27)); }
	inline Image_t2042527209 * get_coinIcon_27() const { return ___coinIcon_27; }
	inline Image_t2042527209 ** get_address_of_coinIcon_27() { return &___coinIcon_27; }
	inline void set_coinIcon_27(Image_t2042527209 * value)
	{
		___coinIcon_27 = value;
		Il2CppCodeGenWriteBarrier(&___coinIcon_27, value);
	}

	inline static int32_t get_offset_of_gemIcon_28() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___gemIcon_28)); }
	inline Image_t2042527209 * get_gemIcon_28() const { return ___gemIcon_28; }
	inline Image_t2042527209 ** get_address_of_gemIcon_28() { return &___gemIcon_28; }
	inline void set_gemIcon_28(Image_t2042527209 * value)
	{
		___gemIcon_28 = value;
		Il2CppCodeGenWriteBarrier(&___gemIcon_28, value);
	}

	inline static int32_t get_offset_of_coinIconOriginScale_29() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___coinIconOriginScale_29)); }
	inline Vector3_t2243707580  get_coinIconOriginScale_29() const { return ___coinIconOriginScale_29; }
	inline Vector3_t2243707580 * get_address_of_coinIconOriginScale_29() { return &___coinIconOriginScale_29; }
	inline void set_coinIconOriginScale_29(Vector3_t2243707580  value)
	{
		___coinIconOriginScale_29 = value;
	}

	inline static int32_t get_offset_of_gemIconOriginScale_30() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___gemIconOriginScale_30)); }
	inline Vector3_t2243707580  get_gemIconOriginScale_30() const { return ___gemIconOriginScale_30; }
	inline Vector3_t2243707580 * get_address_of_gemIconOriginScale_30() { return &___gemIconOriginScale_30; }
	inline void set_gemIconOriginScale_30(Vector3_t2243707580  value)
	{
		___gemIconOriginScale_30 = value;
	}

	inline static int32_t get_offset_of_doubleSeq_31() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403, ___doubleSeq_31)); }
	inline Sequence_t110643099 * get_doubleSeq_31() const { return ___doubleSeq_31; }
	inline Sequence_t110643099 ** get_address_of_doubleSeq_31() { return &___doubleSeq_31; }
	inline void set_doubleSeq_31(Sequence_t110643099 * value)
	{
		___doubleSeq_31 = value;
		Il2CppCodeGenWriteBarrier(&___doubleSeq_31, value);
	}
};

struct TrainingView_t2704080403_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>,System.Int32> TrainingView::<>f__am$cache0
	Func_2_t3951659556 * ___U3CU3Ef__amU24cache0_32;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,TrainingView/TileData>,TrainingView/TileData> TrainingView::<>f__am$cache1
	Func_2_t4128796100 * ___U3CU3Ef__amU24cache1_33;
	// System.Action`1<System.Int32> TrainingView::<>f__am$cache2
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache2_34;
	// System.Action`1<System.Int32> TrainingView::<>f__am$cache3
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache3_35;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_32() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403_StaticFields, ___U3CU3Ef__amU24cache0_32)); }
	inline Func_2_t3951659556 * get_U3CU3Ef__amU24cache0_32() const { return ___U3CU3Ef__amU24cache0_32; }
	inline Func_2_t3951659556 ** get_address_of_U3CU3Ef__amU24cache0_32() { return &___U3CU3Ef__amU24cache0_32; }
	inline void set_U3CU3Ef__amU24cache0_32(Func_2_t3951659556 * value)
	{
		___U3CU3Ef__amU24cache0_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_33() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403_StaticFields, ___U3CU3Ef__amU24cache1_33)); }
	inline Func_2_t4128796100 * get_U3CU3Ef__amU24cache1_33() const { return ___U3CU3Ef__amU24cache1_33; }
	inline Func_2_t4128796100 ** get_address_of_U3CU3Ef__amU24cache1_33() { return &___U3CU3Ef__amU24cache1_33; }
	inline void set_U3CU3Ef__amU24cache1_33(Func_2_t4128796100 * value)
	{
		___U3CU3Ef__amU24cache1_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_34() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403_StaticFields, ___U3CU3Ef__amU24cache2_34)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache2_34() const { return ___U3CU3Ef__amU24cache2_34; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache2_34() { return &___U3CU3Ef__amU24cache2_34; }
	inline void set_U3CU3Ef__amU24cache2_34(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache2_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_35() { return static_cast<int32_t>(offsetof(TrainingView_t2704080403_StaticFields, ___U3CU3Ef__amU24cache3_35)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache3_35() const { return ___U3CU3Ef__amU24cache3_35; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache3_35() { return &___U3CU3Ef__amU24cache3_35; }
	inline void set_U3CU3Ef__amU24cache3_35(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache3_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
