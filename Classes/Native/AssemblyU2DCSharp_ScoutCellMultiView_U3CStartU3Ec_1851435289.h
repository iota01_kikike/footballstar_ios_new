﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// ScoutCellMultiView
struct ScoutCellMultiView_t1304856306;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellMultiView/<Start>c__AnonStorey1
struct  U3CStartU3Ec__AnonStorey1_t1851435289  : public Il2CppObject
{
public:
	// System.Int32 ScoutCellMultiView/<Start>c__AnonStorey1::index
	int32_t ___index_0;
	// ScoutCellMultiView ScoutCellMultiView/<Start>c__AnonStorey1::$this
	ScoutCellMultiView_t1304856306 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey1_t1851435289, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey1_t1851435289, ___U24this_1)); }
	inline ScoutCellMultiView_t1304856306 * get_U24this_1() const { return ___U24this_1; }
	inline ScoutCellMultiView_t1304856306 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ScoutCellMultiView_t1304856306 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
