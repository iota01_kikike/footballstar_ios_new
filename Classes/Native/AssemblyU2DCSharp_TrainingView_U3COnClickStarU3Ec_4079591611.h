﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// TrainingView/<OnClickStar>c__AnonStorey4
struct U3COnClickStarU3Ec__AnonStorey4_t1350708256;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrainingView/<OnClickStar>c__AnonStorey5
struct  U3COnClickStarU3Ec__AnonStorey5_t4079591611  : public Il2CppObject
{
public:
	// System.Int32 TrainingView/<OnClickStar>c__AnonStorey5::gemPrice
	int32_t ___gemPrice_0;
	// TrainingView/<OnClickStar>c__AnonStorey4 TrainingView/<OnClickStar>c__AnonStorey5::<>f__ref$4
	U3COnClickStarU3Ec__AnonStorey4_t1350708256 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_gemPrice_0() { return static_cast<int32_t>(offsetof(U3COnClickStarU3Ec__AnonStorey5_t4079591611, ___gemPrice_0)); }
	inline int32_t get_gemPrice_0() const { return ___gemPrice_0; }
	inline int32_t* get_address_of_gemPrice_0() { return &___gemPrice_0; }
	inline void set_gemPrice_0(int32_t value)
	{
		___gemPrice_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3COnClickStarU3Ec__AnonStorey5_t4079591611, ___U3CU3Ef__refU244_1)); }
	inline U3COnClickStarU3Ec__AnonStorey4_t1350708256 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3COnClickStarU3Ec__AnonStorey4_t1350708256 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3COnClickStarU3Ec__AnonStorey4_t1350708256 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU244_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
