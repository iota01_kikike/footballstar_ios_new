﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.String[]
struct StringU5BU5D_t1642385972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FormHelper
struct  FormHelper_t3562480506  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] FormHelper::PositionList
	GameObjectU5BU5D_t3057952154* ___PositionList_2;
	// System.String[] FormHelper::posNameList
	StringU5BU5D_t1642385972* ___posNameList_3;

public:
	inline static int32_t get_offset_of_PositionList_2() { return static_cast<int32_t>(offsetof(FormHelper_t3562480506, ___PositionList_2)); }
	inline GameObjectU5BU5D_t3057952154* get_PositionList_2() const { return ___PositionList_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PositionList_2() { return &___PositionList_2; }
	inline void set_PositionList_2(GameObjectU5BU5D_t3057952154* value)
	{
		___PositionList_2 = value;
		Il2CppCodeGenWriteBarrier(&___PositionList_2, value);
	}

	inline static int32_t get_offset_of_posNameList_3() { return static_cast<int32_t>(offsetof(FormHelper_t3562480506, ___posNameList_3)); }
	inline StringU5BU5D_t1642385972* get_posNameList_3() const { return ___posNameList_3; }
	inline StringU5BU5D_t1642385972** get_address_of_posNameList_3() { return &___posNameList_3; }
	inline void set_posNameList_3(StringU5BU5D_t1642385972* value)
	{
		___posNameList_3 = value;
		Il2CppCodeGenWriteBarrier(&___posNameList_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
