﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Security_UnityEngine_Purchasing_Security_IAPSecuri3038093501.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.InvalidPKCS7Data
struct  InvalidPKCS7Data_t4123278833  : public IAPSecurityException_t3038093501
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
