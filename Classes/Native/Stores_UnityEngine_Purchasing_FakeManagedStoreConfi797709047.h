﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeManagedStoreConfig
struct  FakeManagedStoreConfig_t797709047  : public Il2CppObject
{
public:
	// System.Boolean UnityEngine.Purchasing.FakeManagedStoreConfig::catalogDisabled
	bool ___catalogDisabled_0;
	// System.Boolean UnityEngine.Purchasing.FakeManagedStoreConfig::testStore
	bool ___testStore_1;
	// System.String UnityEngine.Purchasing.FakeManagedStoreConfig::iapBaseUrl
	String_t* ___iapBaseUrl_2;
	// System.String UnityEngine.Purchasing.FakeManagedStoreConfig::eventBaseUrl
	String_t* ___eventBaseUrl_3;

public:
	inline static int32_t get_offset_of_catalogDisabled_0() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t797709047, ___catalogDisabled_0)); }
	inline bool get_catalogDisabled_0() const { return ___catalogDisabled_0; }
	inline bool* get_address_of_catalogDisabled_0() { return &___catalogDisabled_0; }
	inline void set_catalogDisabled_0(bool value)
	{
		___catalogDisabled_0 = value;
	}

	inline static int32_t get_offset_of_testStore_1() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t797709047, ___testStore_1)); }
	inline bool get_testStore_1() const { return ___testStore_1; }
	inline bool* get_address_of_testStore_1() { return &___testStore_1; }
	inline void set_testStore_1(bool value)
	{
		___testStore_1 = value;
	}

	inline static int32_t get_offset_of_iapBaseUrl_2() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t797709047, ___iapBaseUrl_2)); }
	inline String_t* get_iapBaseUrl_2() const { return ___iapBaseUrl_2; }
	inline String_t** get_address_of_iapBaseUrl_2() { return &___iapBaseUrl_2; }
	inline void set_iapBaseUrl_2(String_t* value)
	{
		___iapBaseUrl_2 = value;
		Il2CppCodeGenWriteBarrier(&___iapBaseUrl_2, value);
	}

	inline static int32_t get_offset_of_eventBaseUrl_3() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t797709047, ___eventBaseUrl_3)); }
	inline String_t* get_eventBaseUrl_3() const { return ___eventBaseUrl_3; }
	inline String_t** get_address_of_eventBaseUrl_3() { return &___eventBaseUrl_3; }
	inline void set_eventBaseUrl_3(String_t* value)
	{
		___eventBaseUrl_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventBaseUrl_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
