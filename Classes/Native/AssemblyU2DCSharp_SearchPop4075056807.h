﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchPop
struct  SearchPop_t4075056807  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SearchPop::TitleText
	Text_t356221433 * ___TitleText_2;
	// UnityEngine.UI.Image SearchPop::SearchIconImage
	Image_t2042527209 * ___SearchIconImage_3;
	// System.Action`1<System.Int32> SearchPop::Callback
	Action_1_t1873676830 * ___Callback_4;
	// System.String SearchPop::searchKey
	String_t* ___searchKey_5;
	// UnityEngine.Vector3 SearchPop::startPos
	Vector3_t2243707580  ___startPos_6;

public:
	inline static int32_t get_offset_of_TitleText_2() { return static_cast<int32_t>(offsetof(SearchPop_t4075056807, ___TitleText_2)); }
	inline Text_t356221433 * get_TitleText_2() const { return ___TitleText_2; }
	inline Text_t356221433 ** get_address_of_TitleText_2() { return &___TitleText_2; }
	inline void set_TitleText_2(Text_t356221433 * value)
	{
		___TitleText_2 = value;
		Il2CppCodeGenWriteBarrier(&___TitleText_2, value);
	}

	inline static int32_t get_offset_of_SearchIconImage_3() { return static_cast<int32_t>(offsetof(SearchPop_t4075056807, ___SearchIconImage_3)); }
	inline Image_t2042527209 * get_SearchIconImage_3() const { return ___SearchIconImage_3; }
	inline Image_t2042527209 ** get_address_of_SearchIconImage_3() { return &___SearchIconImage_3; }
	inline void set_SearchIconImage_3(Image_t2042527209 * value)
	{
		___SearchIconImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___SearchIconImage_3, value);
	}

	inline static int32_t get_offset_of_Callback_4() { return static_cast<int32_t>(offsetof(SearchPop_t4075056807, ___Callback_4)); }
	inline Action_1_t1873676830 * get_Callback_4() const { return ___Callback_4; }
	inline Action_1_t1873676830 ** get_address_of_Callback_4() { return &___Callback_4; }
	inline void set_Callback_4(Action_1_t1873676830 * value)
	{
		___Callback_4 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_4, value);
	}

	inline static int32_t get_offset_of_searchKey_5() { return static_cast<int32_t>(offsetof(SearchPop_t4075056807, ___searchKey_5)); }
	inline String_t* get_searchKey_5() const { return ___searchKey_5; }
	inline String_t** get_address_of_searchKey_5() { return &___searchKey_5; }
	inline void set_searchKey_5(String_t* value)
	{
		___searchKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___searchKey_5, value);
	}

	inline static int32_t get_offset_of_startPos_6() { return static_cast<int32_t>(offsetof(SearchPop_t4075056807, ___startPos_6)); }
	inline Vector3_t2243707580  get_startPos_6() const { return ___startPos_6; }
	inline Vector3_t2243707580 * get_address_of_startPos_6() { return &___startPos_6; }
	inline void set_startPos_6(Vector3_t2243707580  value)
	{
		___startPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
