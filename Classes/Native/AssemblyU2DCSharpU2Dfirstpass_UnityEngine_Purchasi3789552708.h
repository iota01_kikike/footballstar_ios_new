﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t3384326983;
// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t3694879381;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener
struct  IAPListener_t3789552708  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean UnityEngine.Purchasing.IAPListener::consumePurchase
	bool ___consumePurchase_2;
	// System.Boolean UnityEngine.Purchasing.IAPListener::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_3;
	// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPListener::onPurchaseComplete
	OnPurchaseCompletedEvent_t3384326983 * ___onPurchaseComplete_4;
	// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent UnityEngine.Purchasing.IAPListener::onPurchaseFailed
	OnPurchaseFailedEvent_t3694879381 * ___onPurchaseFailed_5;

public:
	inline static int32_t get_offset_of_consumePurchase_2() { return static_cast<int32_t>(offsetof(IAPListener_t3789552708, ___consumePurchase_2)); }
	inline bool get_consumePurchase_2() const { return ___consumePurchase_2; }
	inline bool* get_address_of_consumePurchase_2() { return &___consumePurchase_2; }
	inline void set_consumePurchase_2(bool value)
	{
		___consumePurchase_2 = value;
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_3() { return static_cast<int32_t>(offsetof(IAPListener_t3789552708, ___dontDestroyOnLoad_3)); }
	inline bool get_dontDestroyOnLoad_3() const { return ___dontDestroyOnLoad_3; }
	inline bool* get_address_of_dontDestroyOnLoad_3() { return &___dontDestroyOnLoad_3; }
	inline void set_dontDestroyOnLoad_3(bool value)
	{
		___dontDestroyOnLoad_3 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_4() { return static_cast<int32_t>(offsetof(IAPListener_t3789552708, ___onPurchaseComplete_4)); }
	inline OnPurchaseCompletedEvent_t3384326983 * get_onPurchaseComplete_4() const { return ___onPurchaseComplete_4; }
	inline OnPurchaseCompletedEvent_t3384326983 ** get_address_of_onPurchaseComplete_4() { return &___onPurchaseComplete_4; }
	inline void set_onPurchaseComplete_4(OnPurchaseCompletedEvent_t3384326983 * value)
	{
		___onPurchaseComplete_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseComplete_4, value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_5() { return static_cast<int32_t>(offsetof(IAPListener_t3789552708, ___onPurchaseFailed_5)); }
	inline OnPurchaseFailedEvent_t3694879381 * get_onPurchaseFailed_5() const { return ___onPurchaseFailed_5; }
	inline OnPurchaseFailedEvent_t3694879381 ** get_address_of_onPurchaseFailed_5() { return &___onPurchaseFailed_5; }
	inline void set_onPurchaseFailed_5(OnPurchaseFailedEvent_t3694879381 * value)
	{
		___onPurchaseFailed_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseFailed_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
