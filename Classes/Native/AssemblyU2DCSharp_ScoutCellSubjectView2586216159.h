﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ScoutCellView2404854735.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// ScoutCell
struct ScoutCell_t2879086938;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellSubjectView
struct  ScoutCellSubjectView_t2586216159  : public ScoutCellView_t2404854735
{
public:
	// UnityEngine.GameObject ScoutCellSubjectView::BGPanel
	GameObject_t1756533147 * ___BGPanel_2;
	// UnityEngine.UI.Text ScoutCellSubjectView::SubjectText
	Text_t356221433 * ___SubjectText_3;
	// UnityEngine.UI.Image ScoutCellSubjectView::RemainImage
	Image_t2042527209 * ___RemainImage_4;
	// UnityEngine.UI.Text ScoutCellSubjectView::RemainTimeText
	Text_t356221433 * ___RemainTimeText_5;
	// ScoutCell ScoutCellSubjectView::_parentCell
	ScoutCell_t2879086938 * ____parentCell_6;

public:
	inline static int32_t get_offset_of_BGPanel_2() { return static_cast<int32_t>(offsetof(ScoutCellSubjectView_t2586216159, ___BGPanel_2)); }
	inline GameObject_t1756533147 * get_BGPanel_2() const { return ___BGPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_BGPanel_2() { return &___BGPanel_2; }
	inline void set_BGPanel_2(GameObject_t1756533147 * value)
	{
		___BGPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___BGPanel_2, value);
	}

	inline static int32_t get_offset_of_SubjectText_3() { return static_cast<int32_t>(offsetof(ScoutCellSubjectView_t2586216159, ___SubjectText_3)); }
	inline Text_t356221433 * get_SubjectText_3() const { return ___SubjectText_3; }
	inline Text_t356221433 ** get_address_of_SubjectText_3() { return &___SubjectText_3; }
	inline void set_SubjectText_3(Text_t356221433 * value)
	{
		___SubjectText_3 = value;
		Il2CppCodeGenWriteBarrier(&___SubjectText_3, value);
	}

	inline static int32_t get_offset_of_RemainImage_4() { return static_cast<int32_t>(offsetof(ScoutCellSubjectView_t2586216159, ___RemainImage_4)); }
	inline Image_t2042527209 * get_RemainImage_4() const { return ___RemainImage_4; }
	inline Image_t2042527209 ** get_address_of_RemainImage_4() { return &___RemainImage_4; }
	inline void set_RemainImage_4(Image_t2042527209 * value)
	{
		___RemainImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___RemainImage_4, value);
	}

	inline static int32_t get_offset_of_RemainTimeText_5() { return static_cast<int32_t>(offsetof(ScoutCellSubjectView_t2586216159, ___RemainTimeText_5)); }
	inline Text_t356221433 * get_RemainTimeText_5() const { return ___RemainTimeText_5; }
	inline Text_t356221433 ** get_address_of_RemainTimeText_5() { return &___RemainTimeText_5; }
	inline void set_RemainTimeText_5(Text_t356221433 * value)
	{
		___RemainTimeText_5 = value;
		Il2CppCodeGenWriteBarrier(&___RemainTimeText_5, value);
	}

	inline static int32_t get_offset_of__parentCell_6() { return static_cast<int32_t>(offsetof(ScoutCellSubjectView_t2586216159, ____parentCell_6)); }
	inline ScoutCell_t2879086938 * get__parentCell_6() const { return ____parentCell_6; }
	inline ScoutCell_t2879086938 ** get_address_of__parentCell_6() { return &____parentCell_6; }
	inline void set__parentCell_6(ScoutCell_t2879086938 * value)
	{
		____parentCell_6 = value;
		Il2CppCodeGenWriteBarrier(&____parentCell_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
