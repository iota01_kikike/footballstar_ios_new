﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OptionView
struct  OptionView_t81925000  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject OptionView::MyRecordViewPanel
	GameObject_t1756533147 * ___MyRecordViewPanel_2;
	// UnityEngine.UI.Button OptionView::MyRecordButton
	Button_t2872111280 * ___MyRecordButton_3;
	// UnityEngine.UI.Button OptionView::FacebookButton
	Button_t2872111280 * ___FacebookButton_4;
	// UnityEngine.UI.Button OptionView::InstagramButton
	Button_t2872111280 * ___InstagramButton_5;
	// UnityEngine.UI.Button OptionView::GuideButton
	Button_t2872111280 * ___GuideButton_6;
	// UnityEngine.UI.Button OptionView::RateUsButton
	Button_t2872111280 * ___RateUsButton_7;
	// UnityEngine.UI.Button OptionView::CouponButton
	Button_t2872111280 * ___CouponButton_8;
	// UnityEngine.UI.Button OptionView::SoundButton
	Button_t2872111280 * ___SoundButton_9;
	// UnityEngine.UI.Button OptionView::LeaderboardButton
	Button_t2872111280 * ___LeaderboardButton_10;
	// UnityEngine.UI.Button OptionView::AchievementButton
	Button_t2872111280 * ___AchievementButton_11;
	// UnityEngine.UI.Button OptionView::CloudSaveButton
	Button_t2872111280 * ___CloudSaveButton_12;
	// UnityEngine.UI.Button OptionView::CloudLoadButton
	Button_t2872111280 * ___CloudLoadButton_13;
	// UnityEngine.UI.Button OptionView::CheatButton
	Button_t2872111280 * ___CheatButton_14;
	// UnityEngine.UI.Button OptionView::AllPlayerButton
	Button_t2872111280 * ___AllPlayerButton_15;
	// UnityEngine.UI.Button OptionView::UserResetButton
	Button_t2872111280 * ___UserResetButton_16;
	// UnityEngine.UI.Text OptionView::DebugText
	Text_t356221433 * ___DebugText_17;
	// UnityEngine.GameObject OptionView::LikeBox
	GameObject_t1756533147 * ___LikeBox_18;
	// UnityEngine.GameObject OptionView::HeartBox
	GameObject_t1756533147 * ___HeartBox_19;

public:
	inline static int32_t get_offset_of_MyRecordViewPanel_2() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___MyRecordViewPanel_2)); }
	inline GameObject_t1756533147 * get_MyRecordViewPanel_2() const { return ___MyRecordViewPanel_2; }
	inline GameObject_t1756533147 ** get_address_of_MyRecordViewPanel_2() { return &___MyRecordViewPanel_2; }
	inline void set_MyRecordViewPanel_2(GameObject_t1756533147 * value)
	{
		___MyRecordViewPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___MyRecordViewPanel_2, value);
	}

	inline static int32_t get_offset_of_MyRecordButton_3() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___MyRecordButton_3)); }
	inline Button_t2872111280 * get_MyRecordButton_3() const { return ___MyRecordButton_3; }
	inline Button_t2872111280 ** get_address_of_MyRecordButton_3() { return &___MyRecordButton_3; }
	inline void set_MyRecordButton_3(Button_t2872111280 * value)
	{
		___MyRecordButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___MyRecordButton_3, value);
	}

	inline static int32_t get_offset_of_FacebookButton_4() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___FacebookButton_4)); }
	inline Button_t2872111280 * get_FacebookButton_4() const { return ___FacebookButton_4; }
	inline Button_t2872111280 ** get_address_of_FacebookButton_4() { return &___FacebookButton_4; }
	inline void set_FacebookButton_4(Button_t2872111280 * value)
	{
		___FacebookButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___FacebookButton_4, value);
	}

	inline static int32_t get_offset_of_InstagramButton_5() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___InstagramButton_5)); }
	inline Button_t2872111280 * get_InstagramButton_5() const { return ___InstagramButton_5; }
	inline Button_t2872111280 ** get_address_of_InstagramButton_5() { return &___InstagramButton_5; }
	inline void set_InstagramButton_5(Button_t2872111280 * value)
	{
		___InstagramButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___InstagramButton_5, value);
	}

	inline static int32_t get_offset_of_GuideButton_6() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___GuideButton_6)); }
	inline Button_t2872111280 * get_GuideButton_6() const { return ___GuideButton_6; }
	inline Button_t2872111280 ** get_address_of_GuideButton_6() { return &___GuideButton_6; }
	inline void set_GuideButton_6(Button_t2872111280 * value)
	{
		___GuideButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___GuideButton_6, value);
	}

	inline static int32_t get_offset_of_RateUsButton_7() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___RateUsButton_7)); }
	inline Button_t2872111280 * get_RateUsButton_7() const { return ___RateUsButton_7; }
	inline Button_t2872111280 ** get_address_of_RateUsButton_7() { return &___RateUsButton_7; }
	inline void set_RateUsButton_7(Button_t2872111280 * value)
	{
		___RateUsButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___RateUsButton_7, value);
	}

	inline static int32_t get_offset_of_CouponButton_8() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___CouponButton_8)); }
	inline Button_t2872111280 * get_CouponButton_8() const { return ___CouponButton_8; }
	inline Button_t2872111280 ** get_address_of_CouponButton_8() { return &___CouponButton_8; }
	inline void set_CouponButton_8(Button_t2872111280 * value)
	{
		___CouponButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___CouponButton_8, value);
	}

	inline static int32_t get_offset_of_SoundButton_9() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___SoundButton_9)); }
	inline Button_t2872111280 * get_SoundButton_9() const { return ___SoundButton_9; }
	inline Button_t2872111280 ** get_address_of_SoundButton_9() { return &___SoundButton_9; }
	inline void set_SoundButton_9(Button_t2872111280 * value)
	{
		___SoundButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___SoundButton_9, value);
	}

	inline static int32_t get_offset_of_LeaderboardButton_10() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___LeaderboardButton_10)); }
	inline Button_t2872111280 * get_LeaderboardButton_10() const { return ___LeaderboardButton_10; }
	inline Button_t2872111280 ** get_address_of_LeaderboardButton_10() { return &___LeaderboardButton_10; }
	inline void set_LeaderboardButton_10(Button_t2872111280 * value)
	{
		___LeaderboardButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___LeaderboardButton_10, value);
	}

	inline static int32_t get_offset_of_AchievementButton_11() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___AchievementButton_11)); }
	inline Button_t2872111280 * get_AchievementButton_11() const { return ___AchievementButton_11; }
	inline Button_t2872111280 ** get_address_of_AchievementButton_11() { return &___AchievementButton_11; }
	inline void set_AchievementButton_11(Button_t2872111280 * value)
	{
		___AchievementButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___AchievementButton_11, value);
	}

	inline static int32_t get_offset_of_CloudSaveButton_12() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___CloudSaveButton_12)); }
	inline Button_t2872111280 * get_CloudSaveButton_12() const { return ___CloudSaveButton_12; }
	inline Button_t2872111280 ** get_address_of_CloudSaveButton_12() { return &___CloudSaveButton_12; }
	inline void set_CloudSaveButton_12(Button_t2872111280 * value)
	{
		___CloudSaveButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___CloudSaveButton_12, value);
	}

	inline static int32_t get_offset_of_CloudLoadButton_13() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___CloudLoadButton_13)); }
	inline Button_t2872111280 * get_CloudLoadButton_13() const { return ___CloudLoadButton_13; }
	inline Button_t2872111280 ** get_address_of_CloudLoadButton_13() { return &___CloudLoadButton_13; }
	inline void set_CloudLoadButton_13(Button_t2872111280 * value)
	{
		___CloudLoadButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___CloudLoadButton_13, value);
	}

	inline static int32_t get_offset_of_CheatButton_14() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___CheatButton_14)); }
	inline Button_t2872111280 * get_CheatButton_14() const { return ___CheatButton_14; }
	inline Button_t2872111280 ** get_address_of_CheatButton_14() { return &___CheatButton_14; }
	inline void set_CheatButton_14(Button_t2872111280 * value)
	{
		___CheatButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___CheatButton_14, value);
	}

	inline static int32_t get_offset_of_AllPlayerButton_15() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___AllPlayerButton_15)); }
	inline Button_t2872111280 * get_AllPlayerButton_15() const { return ___AllPlayerButton_15; }
	inline Button_t2872111280 ** get_address_of_AllPlayerButton_15() { return &___AllPlayerButton_15; }
	inline void set_AllPlayerButton_15(Button_t2872111280 * value)
	{
		___AllPlayerButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___AllPlayerButton_15, value);
	}

	inline static int32_t get_offset_of_UserResetButton_16() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___UserResetButton_16)); }
	inline Button_t2872111280 * get_UserResetButton_16() const { return ___UserResetButton_16; }
	inline Button_t2872111280 ** get_address_of_UserResetButton_16() { return &___UserResetButton_16; }
	inline void set_UserResetButton_16(Button_t2872111280 * value)
	{
		___UserResetButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___UserResetButton_16, value);
	}

	inline static int32_t get_offset_of_DebugText_17() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___DebugText_17)); }
	inline Text_t356221433 * get_DebugText_17() const { return ___DebugText_17; }
	inline Text_t356221433 ** get_address_of_DebugText_17() { return &___DebugText_17; }
	inline void set_DebugText_17(Text_t356221433 * value)
	{
		___DebugText_17 = value;
		Il2CppCodeGenWriteBarrier(&___DebugText_17, value);
	}

	inline static int32_t get_offset_of_LikeBox_18() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___LikeBox_18)); }
	inline GameObject_t1756533147 * get_LikeBox_18() const { return ___LikeBox_18; }
	inline GameObject_t1756533147 ** get_address_of_LikeBox_18() { return &___LikeBox_18; }
	inline void set_LikeBox_18(GameObject_t1756533147 * value)
	{
		___LikeBox_18 = value;
		Il2CppCodeGenWriteBarrier(&___LikeBox_18, value);
	}

	inline static int32_t get_offset_of_HeartBox_19() { return static_cast<int32_t>(offsetof(OptionView_t81925000, ___HeartBox_19)); }
	inline GameObject_t1756533147 * get_HeartBox_19() const { return ___HeartBox_19; }
	inline GameObject_t1756533147 ** get_address_of_HeartBox_19() { return &___HeartBox_19; }
	inline void set_HeartBox_19(GameObject_t1756533147 * value)
	{
		___HeartBox_19 = value;
		Il2CppCodeGenWriteBarrier(&___HeartBox_19, value);
	}
};

struct OptionView_t81925000_StaticFields
{
public:
	// System.Action`1<System.Int32> OptionView::<>f__am$cache0
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache0_20;
	// System.Action`1<System.Int32> OptionView::<>f__am$cache1
	Action_1_t1873676830 * ___U3CU3Ef__amU24cache1_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(OptionView_t81925000_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_21() { return static_cast<int32_t>(offsetof(OptionView_t81925000_StaticFields, ___U3CU3Ef__amU24cache1_21)); }
	inline Action_1_t1873676830 * get_U3CU3Ef__amU24cache1_21() const { return ___U3CU3Ef__amU24cache1_21; }
	inline Action_1_t1873676830 ** get_address_of_U3CU3Ef__amU24cache1_21() { return &___U3CU3Ef__amU24cache1_21; }
	inline void set_U3CU3Ef__amU24cache1_21(Action_1_t1873676830 * value)
	{
		___U3CU3Ef__amU24cache1_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
