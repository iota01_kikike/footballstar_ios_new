﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_Tacticsoft_TableView1276614623.h"
#include "AssemblyU2DCSharp_ScoutCellData2122787786.h"

// ScoutCellView
struct ScoutCellView_t2404854735;
// System.Collections.Generic.List`1<ScoutCellView>
struct List_1_t1773975867;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCell
struct  ScoutCell_t2879086938  : public TableViewCell_t1276614623
{
public:
	// System.Int32 ScoutCell::Index
	int32_t ___Index_2;
	// ScoutCellView ScoutCell::CellImageView
	ScoutCellView_t2404854735 * ___CellImageView_3;
	// ScoutCellView ScoutCell::CellSubjectView
	ScoutCellView_t2404854735 * ___CellSubjectView_4;
	// ScoutCellView ScoutCell::CellWideView
	ScoutCellView_t2404854735 * ___CellWideView_5;
	// ScoutCellView ScoutCell::CellMultiView
	ScoutCellView_t2404854735 * ___CellMultiView_6;
	// System.Collections.Generic.List`1<ScoutCellView> ScoutCell::_panelList
	List_1_t1773975867 * ____panelList_7;
	// ScoutCellData ScoutCell::_scoutCellData
	ScoutCellData_t2122787786  ____scoutCellData_8;

public:
	inline static int32_t get_offset_of_Index_2() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ___Index_2)); }
	inline int32_t get_Index_2() const { return ___Index_2; }
	inline int32_t* get_address_of_Index_2() { return &___Index_2; }
	inline void set_Index_2(int32_t value)
	{
		___Index_2 = value;
	}

	inline static int32_t get_offset_of_CellImageView_3() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ___CellImageView_3)); }
	inline ScoutCellView_t2404854735 * get_CellImageView_3() const { return ___CellImageView_3; }
	inline ScoutCellView_t2404854735 ** get_address_of_CellImageView_3() { return &___CellImageView_3; }
	inline void set_CellImageView_3(ScoutCellView_t2404854735 * value)
	{
		___CellImageView_3 = value;
		Il2CppCodeGenWriteBarrier(&___CellImageView_3, value);
	}

	inline static int32_t get_offset_of_CellSubjectView_4() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ___CellSubjectView_4)); }
	inline ScoutCellView_t2404854735 * get_CellSubjectView_4() const { return ___CellSubjectView_4; }
	inline ScoutCellView_t2404854735 ** get_address_of_CellSubjectView_4() { return &___CellSubjectView_4; }
	inline void set_CellSubjectView_4(ScoutCellView_t2404854735 * value)
	{
		___CellSubjectView_4 = value;
		Il2CppCodeGenWriteBarrier(&___CellSubjectView_4, value);
	}

	inline static int32_t get_offset_of_CellWideView_5() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ___CellWideView_5)); }
	inline ScoutCellView_t2404854735 * get_CellWideView_5() const { return ___CellWideView_5; }
	inline ScoutCellView_t2404854735 ** get_address_of_CellWideView_5() { return &___CellWideView_5; }
	inline void set_CellWideView_5(ScoutCellView_t2404854735 * value)
	{
		___CellWideView_5 = value;
		Il2CppCodeGenWriteBarrier(&___CellWideView_5, value);
	}

	inline static int32_t get_offset_of_CellMultiView_6() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ___CellMultiView_6)); }
	inline ScoutCellView_t2404854735 * get_CellMultiView_6() const { return ___CellMultiView_6; }
	inline ScoutCellView_t2404854735 ** get_address_of_CellMultiView_6() { return &___CellMultiView_6; }
	inline void set_CellMultiView_6(ScoutCellView_t2404854735 * value)
	{
		___CellMultiView_6 = value;
		Il2CppCodeGenWriteBarrier(&___CellMultiView_6, value);
	}

	inline static int32_t get_offset_of__panelList_7() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ____panelList_7)); }
	inline List_1_t1773975867 * get__panelList_7() const { return ____panelList_7; }
	inline List_1_t1773975867 ** get_address_of__panelList_7() { return &____panelList_7; }
	inline void set__panelList_7(List_1_t1773975867 * value)
	{
		____panelList_7 = value;
		Il2CppCodeGenWriteBarrier(&____panelList_7, value);
	}

	inline static int32_t get_offset_of__scoutCellData_8() { return static_cast<int32_t>(offsetof(ScoutCell_t2879086938, ____scoutCellData_8)); }
	inline ScoutCellData_t2122787786  get__scoutCellData_8() const { return ____scoutCellData_8; }
	inline ScoutCellData_t2122787786 * get_address_of__scoutCellData_8() { return &____scoutCellData_8; }
	inline void set__scoutCellData_8(ScoutCellData_t2122787786  value)
	{
		____scoutCellData_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
