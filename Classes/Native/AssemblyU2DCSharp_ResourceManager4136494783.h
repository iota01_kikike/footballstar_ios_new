﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// ResourceManager
struct ResourceManager_t4136494783;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Object>
struct Dictionary_2_t2936381379;
// UnityEngine.Shader
struct Shader_t2430389951;
// EZObjectPools.EZObjectPool
struct EZObjectPool_t3968219684;
// UnityEngine.Material
struct Material_t193706927;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceManager
struct  ResourceManager_t4136494783  : public MonoBehaviour_t1158329972
{
public:
	// EZObjectPools.EZObjectPool ResourceManager::CoinIconPool
	EZObjectPool_t3968219684 * ___CoinIconPool_5;
	// EZObjectPools.EZObjectPool ResourceManager::GemIconPool
	EZObjectPool_t3968219684 * ___GemIconPool_6;
	// UnityEngine.Material ResourceManager::GrayScaleMat
	Material_t193706927 * ___GrayScaleMat_9;

public:
	inline static int32_t get_offset_of_CoinIconPool_5() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___CoinIconPool_5)); }
	inline EZObjectPool_t3968219684 * get_CoinIconPool_5() const { return ___CoinIconPool_5; }
	inline EZObjectPool_t3968219684 ** get_address_of_CoinIconPool_5() { return &___CoinIconPool_5; }
	inline void set_CoinIconPool_5(EZObjectPool_t3968219684 * value)
	{
		___CoinIconPool_5 = value;
		Il2CppCodeGenWriteBarrier(&___CoinIconPool_5, value);
	}

	inline static int32_t get_offset_of_GemIconPool_6() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___GemIconPool_6)); }
	inline EZObjectPool_t3968219684 * get_GemIconPool_6() const { return ___GemIconPool_6; }
	inline EZObjectPool_t3968219684 ** get_address_of_GemIconPool_6() { return &___GemIconPool_6; }
	inline void set_GemIconPool_6(EZObjectPool_t3968219684 * value)
	{
		___GemIconPool_6 = value;
		Il2CppCodeGenWriteBarrier(&___GemIconPool_6, value);
	}

	inline static int32_t get_offset_of_GrayScaleMat_9() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783, ___GrayScaleMat_9)); }
	inline Material_t193706927 * get_GrayScaleMat_9() const { return ___GrayScaleMat_9; }
	inline Material_t193706927 ** get_address_of_GrayScaleMat_9() { return &___GrayScaleMat_9; }
	inline void set_GrayScaleMat_9(Material_t193706927 * value)
	{
		___GrayScaleMat_9 = value;
		Il2CppCodeGenWriteBarrier(&___GrayScaleMat_9, value);
	}
};

struct ResourceManager_t4136494783_StaticFields
{
public:
	// ResourceManager ResourceManager::_instance
	ResourceManager_t4136494783 * ____instance_2;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Object> ResourceManager::resourceCache
	Dictionary_2_t2936381379 * ___resourceCache_3;
	// UnityEngine.Shader ResourceManager::vertextLit
	Shader_t2430389951 * ___vertextLit_4;
	// UnityEngine.Color ResourceManager::ORANGE_COLOR
	Color_t2020392075  ___ORANGE_COLOR_7;
	// UnityEngine.Color ResourceManager::BEIGE_COLOR
	Color_t2020392075  ___BEIGE_COLOR_8;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783_StaticFields, ____instance_2)); }
	inline ResourceManager_t4136494783 * get__instance_2() const { return ____instance_2; }
	inline ResourceManager_t4136494783 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ResourceManager_t4136494783 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of_resourceCache_3() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783_StaticFields, ___resourceCache_3)); }
	inline Dictionary_2_t2936381379 * get_resourceCache_3() const { return ___resourceCache_3; }
	inline Dictionary_2_t2936381379 ** get_address_of_resourceCache_3() { return &___resourceCache_3; }
	inline void set_resourceCache_3(Dictionary_2_t2936381379 * value)
	{
		___resourceCache_3 = value;
		Il2CppCodeGenWriteBarrier(&___resourceCache_3, value);
	}

	inline static int32_t get_offset_of_vertextLit_4() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783_StaticFields, ___vertextLit_4)); }
	inline Shader_t2430389951 * get_vertextLit_4() const { return ___vertextLit_4; }
	inline Shader_t2430389951 ** get_address_of_vertextLit_4() { return &___vertextLit_4; }
	inline void set_vertextLit_4(Shader_t2430389951 * value)
	{
		___vertextLit_4 = value;
		Il2CppCodeGenWriteBarrier(&___vertextLit_4, value);
	}

	inline static int32_t get_offset_of_ORANGE_COLOR_7() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783_StaticFields, ___ORANGE_COLOR_7)); }
	inline Color_t2020392075  get_ORANGE_COLOR_7() const { return ___ORANGE_COLOR_7; }
	inline Color_t2020392075 * get_address_of_ORANGE_COLOR_7() { return &___ORANGE_COLOR_7; }
	inline void set_ORANGE_COLOR_7(Color_t2020392075  value)
	{
		___ORANGE_COLOR_7 = value;
	}

	inline static int32_t get_offset_of_BEIGE_COLOR_8() { return static_cast<int32_t>(offsetof(ResourceManager_t4136494783_StaticFields, ___BEIGE_COLOR_8)); }
	inline Color_t2020392075  get_BEIGE_COLOR_8() const { return ___BEIGE_COLOR_8; }
	inline Color_t2020392075 * get_address_of_BEIGE_COLOR_8() { return &___BEIGE_COLOR_8; }
	inline void set_BEIGE_COLOR_8(Color_t2020392075  value)
	{
		___BEIGE_COLOR_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
