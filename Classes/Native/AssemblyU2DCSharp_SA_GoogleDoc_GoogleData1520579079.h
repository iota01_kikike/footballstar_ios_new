﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.List`1<SA.GoogleDoc.DataComponentConnection>
struct List_1_t4030918141;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.GoogleData
struct  GoogleData_t1520579079  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<SA.GoogleDoc.DataComponentConnection> SA.GoogleDoc.GoogleData::connections
	List_1_t4030918141 * ___connections_2;

public:
	inline static int32_t get_offset_of_connections_2() { return static_cast<int32_t>(offsetof(GoogleData_t1520579079, ___connections_2)); }
	inline List_1_t4030918141 * get_connections_2() const { return ___connections_2; }
	inline List_1_t4030918141 ** get_address_of_connections_2() { return &___connections_2; }
	inline void set_connections_2(List_1_t4030918141 * value)
	{
		___connections_2 = value;
		Il2CppCodeGenWriteBarrier(&___connections_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
