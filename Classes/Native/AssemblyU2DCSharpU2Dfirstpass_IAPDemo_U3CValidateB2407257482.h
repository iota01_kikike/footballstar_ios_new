﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo/<ValidateButtonClick>c__AnonStorey1
struct  U3CValidateButtonClickU3Ec__AnonStorey1_t2407257482  : public Il2CppObject
{
public:
	// System.String IAPDemo/<ValidateButtonClick>c__AnonStorey1::txId
	String_t* ___txId_0;

public:
	inline static int32_t get_offset_of_txId_0() { return static_cast<int32_t>(offsetof(U3CValidateButtonClickU3Ec__AnonStorey1_t2407257482, ___txId_0)); }
	inline String_t* get_txId_0() const { return ___txId_0; }
	inline String_t** get_address_of_txId_0() { return &___txId_0; }
	inline void set_txId_0(String_t* value)
	{
		___txId_0 = value;
		Il2CppCodeGenWriteBarrier(&___txId_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
