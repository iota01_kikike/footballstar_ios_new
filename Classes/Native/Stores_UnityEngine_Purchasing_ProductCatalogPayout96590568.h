﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogPayout
struct  ProductCatalogPayout_t96590568  : public Il2CppObject
{
public:
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::t
	String_t* ___t_0;
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::st
	String_t* ___st_1;
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::d
	String_t* ___d_2;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t96590568, ___t_0)); }
	inline String_t* get_t_0() const { return ___t_0; }
	inline String_t** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(String_t* value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier(&___t_0, value);
	}

	inline static int32_t get_offset_of_st_1() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t96590568, ___st_1)); }
	inline String_t* get_st_1() const { return ___st_1; }
	inline String_t** get_address_of_st_1() { return &___st_1; }
	inline void set_st_1(String_t* value)
	{
		___st_1 = value;
		Il2CppCodeGenWriteBarrier(&___st_1, value);
	}

	inline static int32_t get_offset_of_d_2() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t96590568, ___d_2)); }
	inline String_t* get_d_2() const { return ___d_2; }
	inline String_t** get_address_of_d_2() { return &___d_2; }
	inline void set_d_2(String_t* value)
	{
		___d_2 = value;
		Il2CppCodeGenWriteBarrier(&___d_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
