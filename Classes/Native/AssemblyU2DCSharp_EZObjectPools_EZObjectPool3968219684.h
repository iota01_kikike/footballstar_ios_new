﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Collections.Generic.Dictionary`2<System.String,EZObjectPools.EZObjectPool>
struct Dictionary_2_t1588031650;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EZObjectPools.EZObjectPool
struct  EZObjectPool_t3968219684  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject EZObjectPools.EZObjectPool::Template
	GameObject_t1756533147 * ___Template_4;
	// System.String EZObjectPools.EZObjectPool::PoolName
	String_t* ___PoolName_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EZObjectPools.EZObjectPool::ObjectList
	List_1_t1125654279 * ___ObjectList_6;
	// System.Boolean EZObjectPools.EZObjectPool::AutoResize
	bool ___AutoResize_7;
	// System.Int32 EZObjectPools.EZObjectPool::PoolSize
	int32_t ___PoolSize_8;
	// System.Boolean EZObjectPools.EZObjectPool::InstantiateOnAwake
	bool ___InstantiateOnAwake_9;
	// System.Boolean EZObjectPools.EZObjectPool::Shared
	bool ___Shared_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EZObjectPools.EZObjectPool::AvailableObjects
	List_1_t1125654279 * ___AvailableObjects_11;

public:
	inline static int32_t get_offset_of_Template_4() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___Template_4)); }
	inline GameObject_t1756533147 * get_Template_4() const { return ___Template_4; }
	inline GameObject_t1756533147 ** get_address_of_Template_4() { return &___Template_4; }
	inline void set_Template_4(GameObject_t1756533147 * value)
	{
		___Template_4 = value;
		Il2CppCodeGenWriteBarrier(&___Template_4, value);
	}

	inline static int32_t get_offset_of_PoolName_5() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___PoolName_5)); }
	inline String_t* get_PoolName_5() const { return ___PoolName_5; }
	inline String_t** get_address_of_PoolName_5() { return &___PoolName_5; }
	inline void set_PoolName_5(String_t* value)
	{
		___PoolName_5 = value;
		Il2CppCodeGenWriteBarrier(&___PoolName_5, value);
	}

	inline static int32_t get_offset_of_ObjectList_6() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___ObjectList_6)); }
	inline List_1_t1125654279 * get_ObjectList_6() const { return ___ObjectList_6; }
	inline List_1_t1125654279 ** get_address_of_ObjectList_6() { return &___ObjectList_6; }
	inline void set_ObjectList_6(List_1_t1125654279 * value)
	{
		___ObjectList_6 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectList_6, value);
	}

	inline static int32_t get_offset_of_AutoResize_7() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___AutoResize_7)); }
	inline bool get_AutoResize_7() const { return ___AutoResize_7; }
	inline bool* get_address_of_AutoResize_7() { return &___AutoResize_7; }
	inline void set_AutoResize_7(bool value)
	{
		___AutoResize_7 = value;
	}

	inline static int32_t get_offset_of_PoolSize_8() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___PoolSize_8)); }
	inline int32_t get_PoolSize_8() const { return ___PoolSize_8; }
	inline int32_t* get_address_of_PoolSize_8() { return &___PoolSize_8; }
	inline void set_PoolSize_8(int32_t value)
	{
		___PoolSize_8 = value;
	}

	inline static int32_t get_offset_of_InstantiateOnAwake_9() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___InstantiateOnAwake_9)); }
	inline bool get_InstantiateOnAwake_9() const { return ___InstantiateOnAwake_9; }
	inline bool* get_address_of_InstantiateOnAwake_9() { return &___InstantiateOnAwake_9; }
	inline void set_InstantiateOnAwake_9(bool value)
	{
		___InstantiateOnAwake_9 = value;
	}

	inline static int32_t get_offset_of_Shared_10() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___Shared_10)); }
	inline bool get_Shared_10() const { return ___Shared_10; }
	inline bool* get_address_of_Shared_10() { return &___Shared_10; }
	inline void set_Shared_10(bool value)
	{
		___Shared_10 = value;
	}

	inline static int32_t get_offset_of_AvailableObjects_11() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684, ___AvailableObjects_11)); }
	inline List_1_t1125654279 * get_AvailableObjects_11() const { return ___AvailableObjects_11; }
	inline List_1_t1125654279 ** get_address_of_AvailableObjects_11() { return &___AvailableObjects_11; }
	inline void set_AvailableObjects_11(List_1_t1125654279 * value)
	{
		___AvailableObjects_11 = value;
		Il2CppCodeGenWriteBarrier(&___AvailableObjects_11, value);
	}
};

struct EZObjectPool_t3968219684_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,EZObjectPools.EZObjectPool> EZObjectPools.EZObjectPool::SharedPools
	Dictionary_2_t1588031650 * ___SharedPools_2;
	// UnityEngine.GameObject EZObjectPools.EZObjectPool::Marker
	GameObject_t1756533147 * ___Marker_3;

public:
	inline static int32_t get_offset_of_SharedPools_2() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684_StaticFields, ___SharedPools_2)); }
	inline Dictionary_2_t1588031650 * get_SharedPools_2() const { return ___SharedPools_2; }
	inline Dictionary_2_t1588031650 ** get_address_of_SharedPools_2() { return &___SharedPools_2; }
	inline void set_SharedPools_2(Dictionary_2_t1588031650 * value)
	{
		___SharedPools_2 = value;
		Il2CppCodeGenWriteBarrier(&___SharedPools_2, value);
	}

	inline static int32_t get_offset_of_Marker_3() { return static_cast<int32_t>(offsetof(EZObjectPool_t3968219684_StaticFields, ___Marker_3)); }
	inline GameObject_t1756533147 * get_Marker_3() const { return ___Marker_3; }
	inline GameObject_t1756533147 ** get_address_of_Marker_3() { return &___Marker_3; }
	inline void set_Marker_3(GameObject_t1756533147 * value)
	{
		___Marker_3 = value;
		Il2CppCodeGenWriteBarrier(&___Marker_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
