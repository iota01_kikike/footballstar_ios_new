﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern "C" void Context_t2636657155_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t2636657155_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t2636657155_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Context_t2636657155_0_0_0;
extern "C" void Escape_t169451053_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t169451053_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t169451053_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Escape_t169451053_0_0_0;
extern "C" void PreviousInfo_t581002487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t581002487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t581002487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PreviousInfo_t581002487_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613();
extern const Il2CppType AppDomainInitializer_t3898244613_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637();
extern const Il2CppType Swapper_t2637371637_0_0_0;
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3048875398_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
extern "C" void Slot_t2022531261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2022531261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2022531261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Slot_t2022531261_0_0_0;
extern "C" void Slot_t2267560602_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2267560602_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2267560602_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Slot_t2267560602_0_0_0;
extern "C" void Enum_t2459695545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t2459695545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t2459695545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Enum_t2459695545_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381();
extern const Il2CppType ReadDelegate_t3184826381_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132();
extern const Il2CppType WriteDelegate_t489908132_0_0_0;
extern "C" void MonoIOStat_t1621921065_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t1621921065_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t1621921065_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoIOStat_t1621921065_0_0_0;
extern "C" void MonoEnumInfo_t2335995564_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t2335995564_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t2335995564_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEnumInfo_t2335995564_0_0_0;
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t94157543_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t149559338_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
extern "C" void MonoResource_t3127387157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t3127387157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t3127387157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoResource_t3127387157_0_0_0;
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t2190036573_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEventInfo_t2190036573_0_0_0;
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t3646562144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoMethodInfo_t3646562144_0_0_0;
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t486106184_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoPropertyInfo_t486106184_0_0_0;
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1820634920_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
extern "C" void ResourceCacheItem_t333236149_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t333236149_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t333236149_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
extern "C" void ResourceInfo_t3933049236_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3933049236_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3933049236_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990();
extern const Il2CppType CrossContextDelegate_t754146990_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733();
extern const Il2CppType CallbackHandler_t362827733_0_0_0;
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t3485203212_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SerializationEntry_t3485203212_0_0_0;
extern "C" void StreamingContext_t1417235061_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t1417235061_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t1417235061_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StreamingContext_t1417235061_0_0_0;
extern "C" void DSAParameters_t1872138834_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1872138834_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1872138834_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DSAParameters_t1872138834_0_0_0;
extern "C" void RSAParameters_t1462703416_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1462703416_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1462703416_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RSAParameters_t1462703416_0_0_0;
extern "C" void SecurityFrame_t1002202659_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1002202659_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1002202659_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SecurityFrame_t1002202659_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264();
extern const Il2CppType ThreadStart_t3437517264_0_0_0;
extern "C" void ValueType_t3507792607_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3507792607_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3507792607_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ValueType_t3507792607_0_0_0;
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t4278378721_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
extern "C" void IntStack_t273560425_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t273560425_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t273560425_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IntStack_t273560425_0_0_0;
extern "C" void Interval_t2354235237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t2354235237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t2354235237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Interval_t2354235237_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113();
extern const Il2CppType CostDelegate_t1824458113_0_0_0;
extern "C" void UriScheme_t1876590943_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t1876590943_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t1876590943_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UriScheme_t1876590943_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t3226471752();
extern const Il2CppType Action_t3226471752_0_0_0;
extern "C" void CustomEventData_t1269126727_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomEventData_t1269126727_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomEventData_t1269126727_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomEventData_t1269126727_0_0_0;
extern "C" void UnityAnalyticsHandler_t3238795095_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityAnalyticsHandler_t3238795095_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityAnalyticsHandler_t3238795095_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UnityAnalyticsHandler_t3238795095_0_0_0;
extern "C" void DelegatePInvokeWrapper_AndroidJavaRunnable_t3501776228();
extern const Il2CppType AndroidJavaRunnable_t3501776228_0_0_0;
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationCurve_t3306541151_0_0_0;
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationEvent_t2428323300_0_0_0;
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimatorTransitionInfo_t2410896200_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413();
extern const Il2CppType LogCallback_t1867914413_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t642977590();
extern const Il2CppType LowMemoryCallback_t642977590_0_0_0;
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AssetBundleRequest_t2674559435_0_0_0;
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AsyncOperation_t3814632279_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346();
extern const Il2CppType PCMReaderCallback_t3007145346_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554();
extern const Il2CppType PCMSetPositionCallback_t421863554_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033();
extern const Il2CppType AudioConfigurationChangeHandler_t3743753033_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132();
extern const Il2CppType WillRenderCanvases_t3522132132_0_0_0;
extern "C" void Collision_t2876846408_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t2876846408_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t2876846408_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision_t2876846408_0_0_0;
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ControllerColliderHit_t4070855101_0_0_0;
extern "C" void Coroutine_t2299508840_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2299508840_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2299508840_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Coroutine_t2299508840_0_0_0;
extern "C" void CullingGroup_t1091689465_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CullingGroup_t1091689465_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210();
extern const Il2CppType StateChanged_t2480912210_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815();
extern const Il2CppType DisplaysUpdatedDelegate_t3423469815_0_0_0;
extern "C" void Event_t3028476042_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t3028476042_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t3028476042_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Event_t3028476042_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511();
extern const Il2CppType UnityAction_t4025899511_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033();
extern const Il2CppType FontTextureRebuildCallback_t1272078033_0_0_0;
extern "C" void Gradient_t3600583008_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Gradient_t3600583008_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455();
extern const Il2CppType WindowFunction_t3486805455_0_0_0;
extern "C" void GUIContent_t4210063000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t4210063000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t4210063000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIContent_t4210063000_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336();
extern const Il2CppType SkinChangedDelegate_t3594822336_0_0_0;
extern "C" void GUIStyle_t1799908754_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t1799908754_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t1799908754_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t3801000545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyleState_t3801000545_0_0_0;
extern "C" void HumanBone_t1529896151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t1529896151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t1529896151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HumanBone_t1529896151_0_0_0;
extern "C" void jvalue_t3412352577_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void jvalue_t3412352577_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void jvalue_t3412352577_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType jvalue_t3412352577_0_0_0;
extern "C" void Object_t1021602117_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t1021602117_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Object_t1021602117_0_0_0;
extern "C" void RaycastHit_t87180320_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t87180320_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t87180320_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit_t87180320_0_0_0;
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
extern "C" void RectOffset_t3387826427_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t3387826427_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t3387826427_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RectOffset_t3387826427_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180();
extern const Il2CppType UpdatedEventHandler_t3033456180_0_0_0;
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceRequest_t2560315377_0_0_0;
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ScriptableObject_t1975622470_0_0_0;
extern "C" void HitInfo_t1761367055_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t1761367055_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t1761367055_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HitInfo_t1761367055_0_0_0;
extern "C" void SkeletonBone_t345082847_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SkeletonBone_t345082847_0_0_0;
extern "C" void SliderHandler_t3550500579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SliderHandler_t3550500579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SliderHandler_t3550500579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SliderHandler_t3550500579_0_0_0;
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcAchievementDescriptionData_t960725851_0_0_0;
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
extern "C" void GcScoreData_t3676783238_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t3676783238_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t3676783238_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcUserProfileData_t3198293052_0_0_0;
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextGenerationSettings_t2543476768_0_0_0;
extern "C" void TextGenerator_t647235000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t647235000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t647235000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextGenerator_t647235000_0_0_0;
extern "C" void TrackedReference_t1045890189_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TrackedReference_t1045890189_0_0_0;
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WaitForSeconds_t3839502067_0_0_0;
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType YieldInstruction_t3462875981_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846();
extern const Il2CppType UnityPurchasingCallback_t2635187846_0_0_0;
extern "C" void DelegatePInvokeWrapper_EaseFunction_t3306356708();
extern const Il2CppType EaseFunction_t3306356708_0_0_0;
extern "C" void ColorOptions_t2213017305_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorOptions_t2213017305_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorOptions_t2213017305_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorOptions_t2213017305_0_0_0;
extern "C" void FloatOptions_t1421548266_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatOptions_t1421548266_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatOptions_t1421548266_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FloatOptions_t1421548266_0_0_0;
extern "C" void PathOptions_t2659884781_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PathOptions_t2659884781_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PathOptions_t2659884781_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PathOptions_t2659884781_0_0_0;
extern "C" void RectOptions_t3393635162_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOptions_t3393635162_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOptions_t3393635162_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RectOptions_t3393635162_0_0_0;
extern "C" void StringOptions_t2885323933_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringOptions_t2885323933_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringOptions_t2885323933_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StringOptions_t2885323933_0_0_0;
extern "C" void UintOptions_t2267095136_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UintOptions_t2267095136_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UintOptions_t2267095136_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UintOptions_t2267095136_0_0_0;
extern "C" void Vector3ArrayOptions_t2672570171_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Vector3ArrayOptions_t2672570171_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Vector3ArrayOptions_t2672570171_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Vector3ArrayOptions_t2672570171_0_0_0;
extern "C" void VectorOptions_t293385261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VectorOptions_t293385261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VectorOptions_t293385261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType VectorOptions_t293385261_0_0_0;
extern "C" void DelegatePInvokeWrapper_TweenCallback_t3697142134();
extern const Il2CppType TweenCallback_t3697142134_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3230812225();
extern const Il2CppType UnityNativePurchasingCallback_t3230812225_0_0_0;
extern "C" void RaycastResult_t21186376_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t21186376_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t21186376_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastResult_t21186376_0_0_0;
extern "C" void ColorTween_t3438117476_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t3438117476_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t3438117476_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorTween_t3438117476_0_0_0;
extern "C" void FloatTween_t2986189219_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t2986189219_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t2986189219_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FloatTween_t2986189219_0_0_0;
extern "C" void Resources_t2975512894_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t2975512894_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t2975512894_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Resources_t2975512894_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473();
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
extern "C" void Navigation_t1571958496_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t1571958496_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t1571958496_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Navigation_t1571958496_0_0_0;
extern "C" void SpriteState_t1353336012_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1353336012_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1353336012_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SpriteState_t1353336012_0_0_0;
extern "C" void ObscuredBool_t337339225_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredBool_t337339225_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredBool_t337339225_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredBool_t337339225_0_0_0;
extern "C" void ObscuredByte_t875138049_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredByte_t875138049_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredByte_t875138049_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredByte_t875138049_0_0_0;
extern "C" void ObscuredChar_t1936922347_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredChar_t1936922347_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredChar_t1936922347_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredChar_t1936922347_0_0_0;
extern "C" void ObscuredDecimal_t3299336390_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredDecimal_t3299336390_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredDecimal_t3299336390_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredDecimal_t3299336390_0_0_0;
extern "C" void ObscuredDouble_t3265587138_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredDouble_t3265587138_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredDouble_t3265587138_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredDouble_t3265587138_0_0_0;
extern "C" void ObscuredFloat_t3198542555_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredFloat_t3198542555_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredFloat_t3198542555_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredFloat_t3198542555_0_0_0;
extern "C" void ObscuredInt_t796441056_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredInt_t796441056_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredInt_t796441056_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredInt_t796441056_0_0_0;
extern "C" void ObscuredLong_t3872791569_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredLong_t3872791569_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredLong_t3872791569_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredLong_t3872791569_0_0_0;
extern "C" void ObscuredQuaternion_t2380454615_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredQuaternion_t2380454615_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredQuaternion_t2380454615_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredQuaternion_t2380454615_0_0_0;
extern "C" void ObscuredSByte_t4105365306_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredSByte_t4105365306_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredSByte_t4105365306_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredSByte_t4105365306_0_0_0;
extern "C" void ObscuredShort_t3487530033_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredShort_t3487530033_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredShort_t3487530033_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredShort_t3487530033_0_0_0;
extern "C" void ObscuredUInt_t693053169_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredUInt_t693053169_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredUInt_t693053169_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredUInt_t693053169_0_0_0;
extern "C" void ObscuredULong_t79425124_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredULong_t79425124_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredULong_t79425124_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredULong_t79425124_0_0_0;
extern "C" void ObscuredUShort_t4178408852_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredUShort_t4178408852_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredUShort_t4178408852_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredUShort_t4178408852_0_0_0;
extern "C" void ObscuredVector2_t544383870_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredVector2_t544383870_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredVector2_t544383870_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredVector2_t544383870_0_0_0;
extern "C" void ObscuredVector3_t544383869_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ObscuredVector3_t544383869_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ObscuredVector3_t544383869_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ObscuredVector3_t544383869_0_0_0;
extern "C" void DelegatePInvokeWrapper_ActDelegate_t1046046768();
extern const Il2CppType ActDelegate_t1046046768_0_0_0;
extern "C" void DelegatePInvokeWrapper_DownloadCompletedHanderler_t2341399827();
extern const Il2CppType DownloadCompletedHanderler_t2341399827_0_0_0;
extern "C" void DelegatePInvokeWrapper_DownloadingHanderler_t535197402();
extern const Il2CppType DownloadingHanderler_t535197402_0_0_0;
extern "C" void DelegatePInvokeWrapper_ShareCompletedHanderler_t523036664();
extern const Il2CppType ShareCompletedHanderler_t523036664_0_0_0;
extern "C" void DelegatePInvokeWrapper_DelegateOnChanged_t798682617();
extern const Il2CppType DelegateOnChanged_t798682617_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t619382744();
extern const Il2CppType GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t619382744_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t3126817269();
extern const Il2CppType GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t3126817269_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewDidDismissScreenCallback_t2257715507();
extern const Il2CppType GADUAdViewDidDismissScreenCallback_t2257715507_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewDidFailToReceiveAdWithErrorCallback_t2294077762();
extern const Il2CppType GADUAdViewDidFailToReceiveAdWithErrorCallback_t2294077762_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewDidReceiveAdCallback_t3611450851();
extern const Il2CppType GADUAdViewDidReceiveAdCallback_t3611450851_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewWillLeaveApplicationCallback_t1946169147();
extern const Il2CppType GADUAdViewWillLeaveApplicationCallback_t1946169147_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUAdViewWillPresentScreenCallback_t2607757429();
extern const Il2CppType GADUAdViewWillPresentScreenCallback_t2607757429_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUNativeCustomTemplateDidReceiveClick_t3121063597();
extern const Il2CppType GADUNativeCustomTemplateDidReceiveClick_t3121063597_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialDidDismissScreenCallback_t4025611083();
extern const Il2CppType GADUInterstitialDidDismissScreenCallback_t4025611083_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1829207408();
extern const Il2CppType GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1829207408_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialDidReceiveAdCallback_t3343584307();
extern const Il2CppType GADUInterstitialDidReceiveAdCallback_t3343584307_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialWillLeaveApplicationCallback_t216612155();
extern const Il2CppType GADUInterstitialWillLeaveApplicationCallback_t216612155_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADUInterstitialWillPresentScreenCallback_t387623197();
extern const Il2CppType GADUInterstitialWillPresentScreenCallback_t387623197_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCloseCallback_t2453903099();
extern const Il2CppType GADURewardBasedVideoAdDidCloseCallback_t2453903099_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCompleteCallback_t353067300();
extern const Il2CppType GADURewardBasedVideoAdDidCompleteCallback_t353067300_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376();
extern const Il2CppType GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidOpenCallback_t587935421();
extern const Il2CppType GADURewardBasedVideoAdDidOpenCallback_t587935421_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859();
extern const Il2CppType GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidRewardCallback_t129051320();
extern const Il2CppType GADURewardBasedVideoAdDidRewardCallback_t129051320_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdDidStartCallback_t25341677();
extern const Il2CppType GADURewardBasedVideoAdDidStartCallback_t25341677_0_0_0;
extern "C" void DelegatePInvokeWrapper_GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867();
extern const Il2CppType GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867_0_0_0;
extern "C" void AdvertisingResult_t1490481921_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AdvertisingResult_t1490481921_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AdvertisingResult_t1490481921_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AdvertisingResult_t1490481921_0_0_0;
extern "C" void ConnectionRequest_t128529763_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConnectionRequest_t128529763_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConnectionRequest_t128529763_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ConnectionRequest_t128529763_0_0_0;
extern "C" void ConnectionResponse_t426337455_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConnectionResponse_t426337455_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConnectionResponse_t426337455_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ConnectionResponse_t426337455_0_0_0;
extern "C" void EndpointDetails_t1960848347_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EndpointDetails_t1960848347_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EndpointDetails_t1960848347_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EndpointDetails_t1960848347_0_0_0;
extern "C" void NearbyConnectionConfiguration_t1133300345_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NearbyConnectionConfiguration_t1133300345_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NearbyConnectionConfiguration_t1133300345_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NearbyConnectionConfiguration_t1133300345_0_0_0;
extern "C" void SavedGameMetadataUpdate_t1415369233_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SavedGameMetadataUpdate_t1415369233_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SavedGameMetadataUpdate_t1415369233_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SavedGameMetadataUpdate_t1415369233_0_0_0;
extern "C" void Builder_t3624437952_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Builder_t3624437952_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Builder_t3624437952_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Builder_t3624437952_0_0_0;
extern "C" void NationSimpleInfo_t3057834637_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NationSimpleInfo_t3057834637_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NationSimpleInfo_t3057834637_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NationSimpleInfo_t3057834637_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnDocLoaded_t4025950952();
extern const Il2CppType OnDocLoaded_t4025950952_0_0_0;
extern "C" void DelegatePInvokeWrapper_ProgressDelegate_t1267277972();
extern const Il2CppType ProgressDelegate_t1267277972_0_0_0;
extern "C" void ScoutCellData_t2122787786_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScoutCellData_t2122787786_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScoutCellData_t2122787786_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ScoutCellData_t2122787786_0_0_0;
extern "C" void DelegatePInvokeWrapper_SelectStarPopViewDelegate_t2309401728();
extern const Il2CppType SelectStarPopViewDelegate_t2309401728_0_0_0;
extern "C" void DelegatePInvokeWrapper_ApplyTween_t105260918();
extern const Il2CppType ApplyTween_t105260918_0_0_0;
extern "C" void DelegatePInvokeWrapper_EasingFunction_t3881226392();
extern const Il2CppType EasingFunction_t3881226392_0_0_0;
extern "C" void TileData_t2249013992_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TileData_t2249013992_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TileData_t2249013992_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TileData_t2249013992_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[166] = 
{
	{ NULL, Context_t2636657155_marshal_pinvoke, Context_t2636657155_marshal_pinvoke_back, Context_t2636657155_marshal_pinvoke_cleanup, NULL, NULL, &Context_t2636657155_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t169451053_marshal_pinvoke, Escape_t169451053_marshal_pinvoke_back, Escape_t169451053_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t169451053_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t581002487_marshal_pinvoke, PreviousInfo_t581002487_marshal_pinvoke_back, PreviousInfo_t581002487_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t581002487_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t3898244613, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t3898244613_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2637371637, NULL, NULL, NULL, NULL, NULL, &Swapper_t2637371637_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3048875398_marshal_pinvoke, DictionaryEntry_t3048875398_marshal_pinvoke_back, DictionaryEntry_t3048875398_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3048875398_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t2022531261_marshal_pinvoke, Slot_t2022531261_marshal_pinvoke_back, Slot_t2022531261_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2022531261_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t2267560602_marshal_pinvoke, Slot_t2267560602_marshal_pinvoke_back, Slot_t2267560602_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2267560602_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t2459695545_marshal_pinvoke, Enum_t2459695545_marshal_pinvoke_back, Enum_t2459695545_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t2459695545_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t3184826381, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t3184826381_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t489908132, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t489908132_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t1621921065_marshal_pinvoke, MonoIOStat_t1621921065_marshal_pinvoke_back, MonoIOStat_t1621921065_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t1621921065_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t2335995564_marshal_pinvoke, MonoEnumInfo_t2335995564_marshal_pinvoke_back, MonoEnumInfo_t2335995564_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t2335995564_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t94157543_marshal_pinvoke, CustomAttributeNamedArgument_t94157543_marshal_pinvoke_back, CustomAttributeNamedArgument_t94157543_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t94157543_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_back, CustomAttributeTypedArgument_t1498197914_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t1498197914_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t149559338_marshal_pinvoke, ILTokenInfo_t149559338_marshal_pinvoke_back, ILTokenInfo_t149559338_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t149559338_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t3127387157_marshal_pinvoke, MonoResource_t3127387157_marshal_pinvoke_back, MonoResource_t3127387157_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t3127387157_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoEventInfo_t2190036573_marshal_pinvoke, MonoEventInfo_t2190036573_marshal_pinvoke_back, MonoEventInfo_t2190036573_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t2190036573_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t3646562144_marshal_pinvoke, MonoMethodInfo_t3646562144_marshal_pinvoke_back, MonoMethodInfo_t3646562144_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t3646562144_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t486106184_marshal_pinvoke, MonoPropertyInfo_t486106184_marshal_pinvoke_back, MonoPropertyInfo_t486106184_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t486106184_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1820634920_marshal_pinvoke, ParameterModifier_t1820634920_marshal_pinvoke_back, ParameterModifier_t1820634920_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1820634920_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t333236149_marshal_pinvoke, ResourceCacheItem_t333236149_marshal_pinvoke_back, ResourceCacheItem_t333236149_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t333236149_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3933049236_marshal_pinvoke, ResourceInfo_t3933049236_marshal_pinvoke_back, ResourceInfo_t3933049236_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3933049236_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t754146990, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t754146990_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t362827733, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t362827733_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t3485203212_marshal_pinvoke, SerializationEntry_t3485203212_marshal_pinvoke_back, SerializationEntry_t3485203212_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t3485203212_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t1417235061_marshal_pinvoke, StreamingContext_t1417235061_marshal_pinvoke_back, StreamingContext_t1417235061_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t1417235061_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1872138834_marshal_pinvoke, DSAParameters_t1872138834_marshal_pinvoke_back, DSAParameters_t1872138834_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1872138834_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1462703416_marshal_pinvoke, RSAParameters_t1462703416_marshal_pinvoke_back, RSAParameters_t1462703416_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1462703416_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t1002202659_marshal_pinvoke, SecurityFrame_t1002202659_marshal_pinvoke_back, SecurityFrame_t1002202659_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1002202659_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t3437517264, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t3437517264_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3507792607_marshal_pinvoke, ValueType_t3507792607_marshal_pinvoke_back, ValueType_t3507792607_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3507792607_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t4278378721_marshal_pinvoke, X509ChainStatus_t4278378721_marshal_pinvoke_back, X509ChainStatus_t4278378721_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t4278378721_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t273560425_marshal_pinvoke, IntStack_t273560425_marshal_pinvoke_back, IntStack_t273560425_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t273560425_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t2354235237_marshal_pinvoke, Interval_t2354235237_marshal_pinvoke_back, Interval_t2354235237_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t2354235237_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1824458113, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1824458113_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t1876590943_marshal_pinvoke, UriScheme_t1876590943_marshal_pinvoke_back, UriScheme_t1876590943_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t1876590943_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t3226471752, NULL, NULL, NULL, NULL, NULL, &Action_t3226471752_0_0_0 } /* System.Action */,
	{ NULL, CustomEventData_t1269126727_marshal_pinvoke, CustomEventData_t1269126727_marshal_pinvoke_back, CustomEventData_t1269126727_marshal_pinvoke_cleanup, NULL, NULL, &CustomEventData_t1269126727_0_0_0 } /* UnityEngine.Analytics.CustomEventData */,
	{ NULL, UnityAnalyticsHandler_t3238795095_marshal_pinvoke, UnityAnalyticsHandler_t3238795095_marshal_pinvoke_back, UnityAnalyticsHandler_t3238795095_marshal_pinvoke_cleanup, NULL, NULL, &UnityAnalyticsHandler_t3238795095_0_0_0 } /* UnityEngine.Analytics.UnityAnalyticsHandler */,
	{ DelegatePInvokeWrapper_AndroidJavaRunnable_t3501776228, NULL, NULL, NULL, NULL, NULL, &AndroidJavaRunnable_t3501776228_0_0_0 } /* UnityEngine.AndroidJavaRunnable */,
	{ NULL, AnimationCurve_t3306541151_marshal_pinvoke, AnimationCurve_t3306541151_marshal_pinvoke_back, AnimationCurve_t3306541151_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3306541151_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t2428323300_marshal_pinvoke, AnimationEvent_t2428323300_marshal_pinvoke_back, AnimationEvent_t2428323300_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t2428323300_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2410896200_marshal_pinvoke, AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back, AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2410896200_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t1867914413, NULL, NULL, NULL, NULL, NULL, &LogCallback_t1867914413_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t642977590, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t642977590_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t2674559435_marshal_pinvoke, AssetBundleRequest_t2674559435_marshal_pinvoke_back, AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t2674559435_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t3814632279_marshal_pinvoke, AsyncOperation_t3814632279_marshal_pinvoke_back, AsyncOperation_t3814632279_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t3814632279_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t3007145346, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t3007145346_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t421863554_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t3743753033_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3522132132, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3522132132_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision_t2876846408_marshal_pinvoke, Collision_t2876846408_marshal_pinvoke_back, Collision_t2876846408_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t2876846408_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t4070855101_marshal_pinvoke, ControllerColliderHit_t4070855101_marshal_pinvoke_back, ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t4070855101_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t2299508840_marshal_pinvoke, Coroutine_t2299508840_marshal_pinvoke_back, Coroutine_t2299508840_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2299508840_0_0_0 } /* UnityEngine.Coroutine */,
	{ NULL, CullingGroup_t1091689465_marshal_pinvoke, CullingGroup_t1091689465_marshal_pinvoke_back, CullingGroup_t1091689465_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t1091689465_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2480912210, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2480912210_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t3423469815_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t3028476042_marshal_pinvoke, Event_t3028476042_marshal_pinvoke_back, Event_t3028476042_marshal_pinvoke_cleanup, NULL, NULL, &Event_t3028476042_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t4025899511, NULL, NULL, NULL, NULL, NULL, &UnityAction_t4025899511_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t1272078033_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t3600583008_marshal_pinvoke, Gradient_t3600583008_marshal_pinvoke_back, Gradient_t3600583008_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3600583008_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t3486805455, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3486805455_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t4210063000_marshal_pinvoke, GUIContent_t4210063000_marshal_pinvoke_back, GUIContent_t4210063000_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t4210063000_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t3594822336_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t1799908754_marshal_pinvoke, GUIStyle_t1799908754_marshal_pinvoke_back, GUIStyle_t1799908754_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t1799908754_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t3801000545_marshal_pinvoke, GUIStyleState_t3801000545_marshal_pinvoke_back, GUIStyleState_t3801000545_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t3801000545_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HumanBone_t1529896151_marshal_pinvoke, HumanBone_t1529896151_marshal_pinvoke_back, HumanBone_t1529896151_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t1529896151_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, jvalue_t3412352577_marshal_pinvoke, jvalue_t3412352577_marshal_pinvoke_back, jvalue_t3412352577_marshal_pinvoke_cleanup, NULL, NULL, &jvalue_t3412352577_0_0_0 } /* UnityEngine.jvalue */,
	{ NULL, Object_t1021602117_marshal_pinvoke, Object_t1021602117_marshal_pinvoke_back, Object_t1021602117_marshal_pinvoke_cleanup, NULL, NULL, &Object_t1021602117_0_0_0 } /* UnityEngine.Object */,
	{ NULL, RaycastHit_t87180320_marshal_pinvoke, RaycastHit_t87180320_marshal_pinvoke_back, RaycastHit_t87180320_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t87180320_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t4063908774_marshal_pinvoke, RaycastHit2D_t4063908774_marshal_pinvoke_back, RaycastHit2D_t4063908774_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t4063908774_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t3387826427_marshal_pinvoke, RectOffset_t3387826427_marshal_pinvoke_back, RectOffset_t3387826427_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t3387826427_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t3033456180_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t2560315377_marshal_pinvoke, ResourceRequest_t2560315377_marshal_pinvoke_back, ResourceRequest_t2560315377_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t2560315377_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t1975622470_marshal_pinvoke, ScriptableObject_t1975622470_marshal_pinvoke_back, ScriptableObject_t1975622470_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t1975622470_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t1761367055_marshal_pinvoke, HitInfo_t1761367055_marshal_pinvoke_back, HitInfo_t1761367055_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t1761367055_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t345082847_marshal_pinvoke, SkeletonBone_t345082847_marshal_pinvoke_back, SkeletonBone_t345082847_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t345082847_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, SliderHandler_t3550500579_marshal_pinvoke, SliderHandler_t3550500579_marshal_pinvoke_back, SliderHandler_t3550500579_marshal_pinvoke_cleanup, NULL, NULL, &SliderHandler_t3550500579_0_0_0 } /* UnityEngine.SliderHandler */,
	{ NULL, GcAchievementData_t1754866149_marshal_pinvoke, GcAchievementData_t1754866149_marshal_pinvoke_back, GcAchievementData_t1754866149_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t1754866149_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t960725851_marshal_pinvoke, GcAchievementDescriptionData_t960725851_marshal_pinvoke_back, GcAchievementDescriptionData_t960725851_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t960725851_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t453887929_marshal_pinvoke, GcLeaderboard_t453887929_marshal_pinvoke_back, GcLeaderboard_t453887929_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t453887929_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t3676783238_marshal_pinvoke, GcScoreData_t3676783238_marshal_pinvoke_back, GcScoreData_t3676783238_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t3676783238_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t3198293052_marshal_pinvoke, GcUserProfileData_t3198293052_marshal_pinvoke_back, GcUserProfileData_t3198293052_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t3198293052_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t2543476768_marshal_pinvoke, TextGenerationSettings_t2543476768_marshal_pinvoke_back, TextGenerationSettings_t2543476768_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t2543476768_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t647235000_marshal_pinvoke, TextGenerator_t647235000_marshal_pinvoke_back, TextGenerator_t647235000_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t647235000_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t1045890189_marshal_pinvoke, TrackedReference_t1045890189_marshal_pinvoke_back, TrackedReference_t1045890189_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1045890189_0_0_0 } /* UnityEngine.TrackedReference */,
	{ NULL, WaitForSeconds_t3839502067_marshal_pinvoke, WaitForSeconds_t3839502067_marshal_pinvoke_back, WaitForSeconds_t3839502067_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t3839502067_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t3462875981_marshal_pinvoke, YieldInstruction_t3462875981_marshal_pinvoke_back, YieldInstruction_t3462875981_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t3462875981_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_UnityPurchasingCallback_t2635187846, NULL, NULL, NULL, NULL, NULL, &UnityPurchasingCallback_t2635187846_0_0_0 } /* UnityEngine.Purchasing.UnityPurchasingCallback */,
	{ DelegatePInvokeWrapper_EaseFunction_t3306356708, NULL, NULL, NULL, NULL, NULL, &EaseFunction_t3306356708_0_0_0 } /* DG.Tweening.EaseFunction */,
	{ NULL, ColorOptions_t2213017305_marshal_pinvoke, ColorOptions_t2213017305_marshal_pinvoke_back, ColorOptions_t2213017305_marshal_pinvoke_cleanup, NULL, NULL, &ColorOptions_t2213017305_0_0_0 } /* DG.Tweening.Plugins.Options.ColorOptions */,
	{ NULL, FloatOptions_t1421548266_marshal_pinvoke, FloatOptions_t1421548266_marshal_pinvoke_back, FloatOptions_t1421548266_marshal_pinvoke_cleanup, NULL, NULL, &FloatOptions_t1421548266_0_0_0 } /* DG.Tweening.Plugins.Options.FloatOptions */,
	{ NULL, PathOptions_t2659884781_marshal_pinvoke, PathOptions_t2659884781_marshal_pinvoke_back, PathOptions_t2659884781_marshal_pinvoke_cleanup, NULL, NULL, &PathOptions_t2659884781_0_0_0 } /* DG.Tweening.Plugins.Options.PathOptions */,
	{ NULL, RectOptions_t3393635162_marshal_pinvoke, RectOptions_t3393635162_marshal_pinvoke_back, RectOptions_t3393635162_marshal_pinvoke_cleanup, NULL, NULL, &RectOptions_t3393635162_0_0_0 } /* DG.Tweening.Plugins.Options.RectOptions */,
	{ NULL, StringOptions_t2885323933_marshal_pinvoke, StringOptions_t2885323933_marshal_pinvoke_back, StringOptions_t2885323933_marshal_pinvoke_cleanup, NULL, NULL, &StringOptions_t2885323933_0_0_0 } /* DG.Tweening.Plugins.Options.StringOptions */,
	{ NULL, UintOptions_t2267095136_marshal_pinvoke, UintOptions_t2267095136_marshal_pinvoke_back, UintOptions_t2267095136_marshal_pinvoke_cleanup, NULL, NULL, &UintOptions_t2267095136_0_0_0 } /* DG.Tweening.Plugins.Options.UintOptions */,
	{ NULL, Vector3ArrayOptions_t2672570171_marshal_pinvoke, Vector3ArrayOptions_t2672570171_marshal_pinvoke_back, Vector3ArrayOptions_t2672570171_marshal_pinvoke_cleanup, NULL, NULL, &Vector3ArrayOptions_t2672570171_0_0_0 } /* DG.Tweening.Plugins.Options.Vector3ArrayOptions */,
	{ NULL, VectorOptions_t293385261_marshal_pinvoke, VectorOptions_t293385261_marshal_pinvoke_back, VectorOptions_t293385261_marshal_pinvoke_cleanup, NULL, NULL, &VectorOptions_t293385261_0_0_0 } /* DG.Tweening.Plugins.Options.VectorOptions */,
	{ DelegatePInvokeWrapper_TweenCallback_t3697142134, NULL, NULL, NULL, NULL, NULL, &TweenCallback_t3697142134_0_0_0 } /* DG.Tweening.TweenCallback */,
	{ DelegatePInvokeWrapper_UnityNativePurchasingCallback_t3230812225, NULL, NULL, NULL, NULL, NULL, &UnityNativePurchasingCallback_t3230812225_0_0_0 } /* UnityEngine.Purchasing.UnityNativePurchasingCallback */,
	{ NULL, RaycastResult_t21186376_marshal_pinvoke, RaycastResult_t21186376_marshal_pinvoke_back, RaycastResult_t21186376_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t21186376_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t3438117476_marshal_pinvoke, ColorTween_t3438117476_marshal_pinvoke_back, ColorTween_t3438117476_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t3438117476_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t2986189219_marshal_pinvoke, FloatTween_t2986189219_marshal_pinvoke_back, FloatTween_t2986189219_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t2986189219_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t2975512894_marshal_pinvoke, Resources_t2975512894_marshal_pinvoke_back, Resources_t2975512894_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t2975512894_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t1946318473, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t1946318473_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t1571958496_marshal_pinvoke, Navigation_t1571958496_marshal_pinvoke_back, Navigation_t1571958496_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t1571958496_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1353336012_marshal_pinvoke, SpriteState_t1353336012_marshal_pinvoke_back, SpriteState_t1353336012_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1353336012_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ObscuredBool_t337339225_marshal_pinvoke, ObscuredBool_t337339225_marshal_pinvoke_back, ObscuredBool_t337339225_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredBool_t337339225_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredBool */,
	{ NULL, ObscuredByte_t875138049_marshal_pinvoke, ObscuredByte_t875138049_marshal_pinvoke_back, ObscuredByte_t875138049_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredByte_t875138049_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredByte */,
	{ NULL, ObscuredChar_t1936922347_marshal_pinvoke, ObscuredChar_t1936922347_marshal_pinvoke_back, ObscuredChar_t1936922347_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredChar_t1936922347_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredChar */,
	{ NULL, ObscuredDecimal_t3299336390_marshal_pinvoke, ObscuredDecimal_t3299336390_marshal_pinvoke_back, ObscuredDecimal_t3299336390_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredDecimal_t3299336390_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal */,
	{ NULL, ObscuredDouble_t3265587138_marshal_pinvoke, ObscuredDouble_t3265587138_marshal_pinvoke_back, ObscuredDouble_t3265587138_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredDouble_t3265587138_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble */,
	{ NULL, ObscuredFloat_t3198542555_marshal_pinvoke, ObscuredFloat_t3198542555_marshal_pinvoke_back, ObscuredFloat_t3198542555_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredFloat_t3198542555_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat */,
	{ NULL, ObscuredInt_t796441056_marshal_pinvoke, ObscuredInt_t796441056_marshal_pinvoke_back, ObscuredInt_t796441056_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredInt_t796441056_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredInt */,
	{ NULL, ObscuredLong_t3872791569_marshal_pinvoke, ObscuredLong_t3872791569_marshal_pinvoke_back, ObscuredLong_t3872791569_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredLong_t3872791569_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredLong */,
	{ NULL, ObscuredQuaternion_t2380454615_marshal_pinvoke, ObscuredQuaternion_t2380454615_marshal_pinvoke_back, ObscuredQuaternion_t2380454615_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredQuaternion_t2380454615_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion */,
	{ NULL, ObscuredSByte_t4105365306_marshal_pinvoke, ObscuredSByte_t4105365306_marshal_pinvoke_back, ObscuredSByte_t4105365306_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredSByte_t4105365306_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte */,
	{ NULL, ObscuredShort_t3487530033_marshal_pinvoke, ObscuredShort_t3487530033_marshal_pinvoke_back, ObscuredShort_t3487530033_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredShort_t3487530033_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredShort */,
	{ NULL, ObscuredUInt_t693053169_marshal_pinvoke, ObscuredUInt_t693053169_marshal_pinvoke_back, ObscuredUInt_t693053169_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredUInt_t693053169_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt */,
	{ NULL, ObscuredULong_t79425124_marshal_pinvoke, ObscuredULong_t79425124_marshal_pinvoke_back, ObscuredULong_t79425124_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredULong_t79425124_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredULong */,
	{ NULL, ObscuredUShort_t4178408852_marshal_pinvoke, ObscuredUShort_t4178408852_marshal_pinvoke_back, ObscuredUShort_t4178408852_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredUShort_t4178408852_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort */,
	{ NULL, ObscuredVector2_t544383870_marshal_pinvoke, ObscuredVector2_t544383870_marshal_pinvoke_back, ObscuredVector2_t544383870_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredVector2_t544383870_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2 */,
	{ NULL, ObscuredVector3_t544383869_marshal_pinvoke, ObscuredVector3_t544383869_marshal_pinvoke_back, ObscuredVector3_t544383869_marshal_pinvoke_cleanup, NULL, NULL, &ObscuredVector3_t544383869_0_0_0 } /* CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3 */,
	{ DelegatePInvokeWrapper_ActDelegate_t1046046768, NULL, NULL, NULL, NULL, NULL, &ActDelegate_t1046046768_0_0_0 } /* LogoLayer/ActDelegate */,
	{ DelegatePInvokeWrapper_DownloadCompletedHanderler_t2341399827, NULL, NULL, NULL, NULL, NULL, &DownloadCompletedHanderler_t2341399827_0_0_0 } /* NativeShare/DownloadCompletedHanderler */,
	{ DelegatePInvokeWrapper_DownloadingHanderler_t535197402, NULL, NULL, NULL, NULL, NULL, &DownloadingHanderler_t535197402_0_0_0 } /* NativeShare/DownloadingHanderler */,
	{ DelegatePInvokeWrapper_ShareCompletedHanderler_t523036664, NULL, NULL, NULL, NULL, NULL, &ShareCompletedHanderler_t523036664_0_0_0 } /* NativeShare/ShareCompletedHanderler */,
	{ DelegatePInvokeWrapper_DelegateOnChanged_t798682617, NULL, NULL, NULL, NULL, NULL, &DelegateOnChanged_t798682617_0_0_0 } /* FormPlayerUIView/DelegateOnChanged */,
	{ DelegatePInvokeWrapper_GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t619382744, NULL, NULL, NULL, NULL, NULL, &GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t619382744_0_0_0 } /* GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t3126817269, NULL, NULL, NULL, NULL, NULL, &GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t3126817269_0_0_0 } /* GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewDidDismissScreenCallback_t2257715507, NULL, NULL, NULL, NULL, NULL, &GADUAdViewDidDismissScreenCallback_t2257715507_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewDidFailToReceiveAdWithErrorCallback_t2294077762, NULL, NULL, NULL, NULL, NULL, &GADUAdViewDidFailToReceiveAdWithErrorCallback_t2294077762_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewDidReceiveAdCallback_t3611450851, NULL, NULL, NULL, NULL, NULL, &GADUAdViewDidReceiveAdCallback_t3611450851_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewWillLeaveApplicationCallback_t1946169147, NULL, NULL, NULL, NULL, NULL, &GADUAdViewWillLeaveApplicationCallback_t1946169147_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback */,
	{ DelegatePInvokeWrapper_GADUAdViewWillPresentScreenCallback_t2607757429, NULL, NULL, NULL, NULL, NULL, &GADUAdViewWillPresentScreenCallback_t2607757429_0_0_0 } /* GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback */,
	{ DelegatePInvokeWrapper_GADUNativeCustomTemplateDidReceiveClick_t3121063597, NULL, NULL, NULL, NULL, NULL, &GADUNativeCustomTemplateDidReceiveClick_t3121063597_0_0_0 } /* GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick */,
	{ DelegatePInvokeWrapper_GADUInterstitialDidDismissScreenCallback_t4025611083, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialDidDismissScreenCallback_t4025611083_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1829207408, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1829207408_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialDidReceiveAdCallback_t3343584307, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialDidReceiveAdCallback_t3343584307_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialWillLeaveApplicationCallback_t216612155, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialWillLeaveApplicationCallback_t216612155_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback */,
	{ DelegatePInvokeWrapper_GADUInterstitialWillPresentScreenCallback_t387623197, NULL, NULL, NULL, NULL, NULL, &GADUInterstitialWillPresentScreenCallback_t387623197_0_0_0 } /* GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCloseCallback_t2453903099, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidCloseCallback_t2453903099_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidCompleteCallback_t353067300, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidCompleteCallback_t353067300_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCompleteCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidOpenCallback_t587935421, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidOpenCallback_t587935421_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidRewardCallback_t129051320, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidRewardCallback_t129051320_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdDidStartCallback_t25341677, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdDidStartCallback_t25341677_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback */,
	{ DelegatePInvokeWrapper_GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867, NULL, NULL, NULL, NULL, NULL, &GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867_0_0_0 } /* GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback */,
	{ NULL, AdvertisingResult_t1490481921_marshal_pinvoke, AdvertisingResult_t1490481921_marshal_pinvoke_back, AdvertisingResult_t1490481921_marshal_pinvoke_cleanup, NULL, NULL, &AdvertisingResult_t1490481921_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.AdvertisingResult */,
	{ NULL, ConnectionRequest_t128529763_marshal_pinvoke, ConnectionRequest_t128529763_marshal_pinvoke_back, ConnectionRequest_t128529763_marshal_pinvoke_cleanup, NULL, NULL, &ConnectionRequest_t128529763_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.ConnectionRequest */,
	{ NULL, ConnectionResponse_t426337455_marshal_pinvoke, ConnectionResponse_t426337455_marshal_pinvoke_back, ConnectionResponse_t426337455_marshal_pinvoke_cleanup, NULL, NULL, &ConnectionResponse_t426337455_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.ConnectionResponse */,
	{ NULL, EndpointDetails_t1960848347_marshal_pinvoke, EndpointDetails_t1960848347_marshal_pinvoke_back, EndpointDetails_t1960848347_marshal_pinvoke_cleanup, NULL, NULL, &EndpointDetails_t1960848347_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.EndpointDetails */,
	{ NULL, NearbyConnectionConfiguration_t1133300345_marshal_pinvoke, NearbyConnectionConfiguration_t1133300345_marshal_pinvoke_back, NearbyConnectionConfiguration_t1133300345_marshal_pinvoke_cleanup, NULL, NULL, &NearbyConnectionConfiguration_t1133300345_0_0_0 } /* GooglePlayGames.BasicApi.Nearby.NearbyConnectionConfiguration */,
	{ NULL, SavedGameMetadataUpdate_t1415369233_marshal_pinvoke, SavedGameMetadataUpdate_t1415369233_marshal_pinvoke_back, SavedGameMetadataUpdate_t1415369233_marshal_pinvoke_cleanup, NULL, NULL, &SavedGameMetadataUpdate_t1415369233_0_0_0 } /* GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate */,
	{ NULL, Builder_t3624437952_marshal_pinvoke, Builder_t3624437952_marshal_pinvoke_back, Builder_t3624437952_marshal_pinvoke_cleanup, NULL, NULL, &Builder_t3624437952_0_0_0 } /* GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder */,
	{ NULL, NationSimpleInfo_t3057834637_marshal_pinvoke, NationSimpleInfo_t3057834637_marshal_pinvoke_back, NationSimpleInfo_t3057834637_marshal_pinvoke_cleanup, NULL, NULL, &NationSimpleInfo_t3057834637_0_0_0 } /* NationSimpleInfo */,
	{ DelegatePInvokeWrapper_OnDocLoaded_t4025950952, NULL, NULL, NULL, NULL, NULL, &OnDocLoaded_t4025950952_0_0_0 } /* OnDocLoaded */,
	{ DelegatePInvokeWrapper_ProgressDelegate_t1267277972, NULL, NULL, NULL, NULL, NULL, &ProgressDelegate_t1267277972_0_0_0 } /* ProgressDelegate */,
	{ NULL, ScoutCellData_t2122787786_marshal_pinvoke, ScoutCellData_t2122787786_marshal_pinvoke_back, ScoutCellData_t2122787786_marshal_pinvoke_cleanup, NULL, NULL, &ScoutCellData_t2122787786_0_0_0 } /* ScoutCellData */,
	{ DelegatePInvokeWrapper_SelectStarPopViewDelegate_t2309401728, NULL, NULL, NULL, NULL, NULL, &SelectStarPopViewDelegate_t2309401728_0_0_0 } /* SelectStarPopView/SelectStarPopViewDelegate */,
	{ DelegatePInvokeWrapper_ApplyTween_t105260918, NULL, NULL, NULL, NULL, NULL, &ApplyTween_t105260918_0_0_0 } /* StansAssets.Animation.iTween/ApplyTween */,
	{ DelegatePInvokeWrapper_EasingFunction_t3881226392, NULL, NULL, NULL, NULL, NULL, &EasingFunction_t3881226392_0_0_0 } /* StansAssets.Animation.iTween/EasingFunction */,
	{ NULL, TileData_t2249013992_marshal_pinvoke, TileData_t2249013992_marshal_pinvoke_back, TileData_t2249013992_marshal_pinvoke_cleanup, NULL, NULL, &TileData_t2249013992_0_0_0 } /* TrainingView/TileData */,
	NULL,
};
