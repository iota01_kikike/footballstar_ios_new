﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691307.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653222.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366305.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628228.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341311.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303226.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678216.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391299.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978317.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691400.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653315.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366398.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628321.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341404.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303319.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016402.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391392.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978414.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691497.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653412.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366495.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628418.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341501.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303416.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016499.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678406.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391489.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978755.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691838.h"
#include "DOTween_DG_Tweening_TweenParams2944325381.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "DOTween_DG_Tweening_LogBehaviour3505725029.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"
#include "DOTween_DG_Tweening_Plugins_Color2Plugin3433430606.h"
#include "DOTween_DG_Tweening_Plugins_DoublePlugin266400784.h"
#include "DOTween_DG_Tweening_Plugins_LongPlugin1941283029.h"
#include "DOTween_DG_Tweening_Plugins_UlongPlugin3231465400.h"
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin2378569512.h"
#include "DOTween_DG_Tweening_Plugins_PathPlugin4171842066.h"
#include "DOTween_DG_Tweening_Plugins_ColorPlugin4063724482.h"
#include "DOTween_DG_Tweening_Plugins_IntPlugin180838436.h"
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin1696644323.h"
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin664509336.h"
#include "DOTween_DG_Tweening_Plugins_RectPlugin391797831.h"
#include "DOTween_DG_Tweening_Plugins_UintPlugin1040977389.h"
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin2164285386.h"
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin2164361360.h"
#include "DOTween_DG_Tweening_Plugins_StringPlugin3620786088.h"
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions3910942986.h"
#include "DOTween_DG_Tweening_Plugins_FloatPlugin3639480371.h"
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin2164530409.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUti2241999250.h"
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager3052451537.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlPo168081159.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ABSPathD3294469411.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_CatmullR3014762178.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_LinearDe2073524639.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path2828565993.h"
#include "DOTween_DG_Tweening_CustomPlugins_PureQuaternionPl3400666973.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent696744215.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitFo639731943.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2782210651.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF1007991819.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF3988581919.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2034437344.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2341562412.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings873123119.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLo514961325.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "DOTween_DG_Tweening_Core_SequenceCallback2782183128.h"
#include "DOTween_DG_Tweening_Core_TweenManager1979661952.h"
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncr1969140739.h"
#include "DOTween_DG_Tweening_Core_Utils2524017187.h"
#include "DOTween_DG_Tweening_Core_Enums_FilterType1425068526.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (U3CU3Ec__DisplayClass51_0_t1424691307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[1] = 
{
	U3CU3Ec__DisplayClass51_0_t1424691307::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (U3CU3Ec__DisplayClass52_0_t2728653222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	U3CU3Ec__DisplayClass52_0_t2728653222::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (U3CU3Ec__DisplayClass53_0_t1142366305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	U3CU3Ec__DisplayClass53_0_t1142366305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (U3CU3Ec__DisplayClass54_0_t3575628228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	U3CU3Ec__DisplayClass54_0_t3575628228::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (U3CU3Ec__DisplayClass55_0_t1989341311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	U3CU3Ec__DisplayClass55_0_t1989341311::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (U3CU3Ec__DisplayClass56_0_t3293303226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	U3CU3Ec__DisplayClass56_0_t3293303226::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (U3CU3Ec__DisplayClass57_0_t1707016309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	U3CU3Ec__DisplayClass57_0_t1707016309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (U3CU3Ec__DisplayClass58_0_t1881678216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[1] = 
{
	U3CU3Ec__DisplayClass58_0_t1881678216::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (U3CU3Ec__DisplayClass59_0_t295391299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	U3CU3Ec__DisplayClass59_0_t295391299::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (U3CU3Ec__DisplayClass60_0_t3010978317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[1] = 
{
	U3CU3Ec__DisplayClass60_0_t3010978317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (U3CU3Ec__DisplayClass61_0_t1424691400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[1] = 
{
	U3CU3Ec__DisplayClass61_0_t1424691400::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (U3CU3Ec__DisplayClass62_0_t2728653315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2728653315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U3CU3Ec__DisplayClass63_0_t1142366398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[1] = 
{
	U3CU3Ec__DisplayClass63_0_t1142366398::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (U3CU3Ec__DisplayClass64_0_t3575628321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (U3CU3Ec__DisplayClass65_0_t1989341404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (U3CU3Ec__DisplayClass66_0_t3293303319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[1] = 
{
	U3CU3Ec__DisplayClass66_0_t3293303319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (U3CU3Ec__DisplayClass67_0_t1707016402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[1] = 
{
	U3CU3Ec__DisplayClass67_0_t1707016402::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (U3CU3Ec__DisplayClass68_0_t1881678309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[6] = 
{
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (U3CU3Ec__DisplayClass69_0_t295391392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[6] = 
{
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (U3CU3Ec__DisplayClass70_0_t3010978414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[1] = 
{
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (U3CU3Ec__DisplayClass71_0_t1424691497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (U3CU3Ec__DisplayClass72_0_t2728653412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[1] = 
{
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (U3CU3Ec__DisplayClass73_0_t1142366495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[1] = 
{
	U3CU3Ec__DisplayClass73_0_t1142366495::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (U3CU3Ec__DisplayClass74_0_t3575628418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[2] = 
{
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (U3CU3Ec__DisplayClass75_0_t1989341501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[2] = 
{
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (U3CU3Ec__DisplayClass76_0_t3293303416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[3] = 
{
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_property_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (U3CU3Ec__DisplayClass77_0_t1707016499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[2] = 
{
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (U3CU3Ec__DisplayClass78_0_t1881678406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[2] = 
{
	U3CU3Ec__DisplayClass78_0_t1881678406::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass78_0_t1881678406::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (U3CU3Ec__DisplayClass79_0_t295391489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[2] = 
{
	U3CU3Ec__DisplayClass79_0_t295391489::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass79_0_t295391489::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (U3CU3Ec__DisplayClass80_0_t3010978755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[2] = 
{
	U3CU3Ec__DisplayClass80_0_t3010978755::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass80_0_t3010978755::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (U3CU3Ec__DisplayClass81_0_t1424691838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[2] = 
{
	U3CU3Ec__DisplayClass81_0_t1424691838::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass81_0_t1424691838::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (TweenParams_t2944325381), -1, sizeof(TweenParams_t2944325381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[24] = 
{
	TweenParams_t2944325381_StaticFields::get_offset_of_Params_0(),
	TweenParams_t2944325381::get_offset_of_id_1(),
	TweenParams_t2944325381::get_offset_of_target_2(),
	TweenParams_t2944325381::get_offset_of_updateType_3(),
	TweenParams_t2944325381::get_offset_of_isIndependentUpdate_4(),
	TweenParams_t2944325381::get_offset_of_onStart_5(),
	TweenParams_t2944325381::get_offset_of_onPlay_6(),
	TweenParams_t2944325381::get_offset_of_onRewind_7(),
	TweenParams_t2944325381::get_offset_of_onUpdate_8(),
	TweenParams_t2944325381::get_offset_of_onStepComplete_9(),
	TweenParams_t2944325381::get_offset_of_onComplete_10(),
	TweenParams_t2944325381::get_offset_of_onKill_11(),
	TweenParams_t2944325381::get_offset_of_onWaypointChange_12(),
	TweenParams_t2944325381::get_offset_of_isRecyclable_13(),
	TweenParams_t2944325381::get_offset_of_isSpeedBased_14(),
	TweenParams_t2944325381::get_offset_of_autoKill_15(),
	TweenParams_t2944325381::get_offset_of_loops_16(),
	TweenParams_t2944325381::get_offset_of_loopType_17(),
	TweenParams_t2944325381::get_offset_of_delay_18(),
	TweenParams_t2944325381::get_offset_of_isRelative_19(),
	TweenParams_t2944325381::get_offset_of_easeType_20(),
	TweenParams_t2944325381::get_offset_of_customEase_21(),
	TweenParams_t2944325381::get_offset_of_easeOvershootOrAmplitude_22(),
	TweenParams_t2944325381::get_offset_of_easePeriod_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (TweenSettingsExtensions_t2285462830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (LogBehaviour_t3505725029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1833[4] = 
{
	LogBehaviour_t3505725029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (Tween_t278478013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[47] = 
{
	Tween_t278478013::get_offset_of_timeScale_4(),
	Tween_t278478013::get_offset_of_isBackwards_5(),
	Tween_t278478013::get_offset_of_id_6(),
	Tween_t278478013::get_offset_of_target_7(),
	Tween_t278478013::get_offset_of_updateType_8(),
	Tween_t278478013::get_offset_of_isIndependentUpdate_9(),
	Tween_t278478013::get_offset_of_onPlay_10(),
	Tween_t278478013::get_offset_of_onPause_11(),
	Tween_t278478013::get_offset_of_onRewind_12(),
	Tween_t278478013::get_offset_of_onUpdate_13(),
	Tween_t278478013::get_offset_of_onStepComplete_14(),
	Tween_t278478013::get_offset_of_onComplete_15(),
	Tween_t278478013::get_offset_of_onKill_16(),
	Tween_t278478013::get_offset_of_onWaypointChange_17(),
	Tween_t278478013::get_offset_of_isFrom_18(),
	Tween_t278478013::get_offset_of_isBlendable_19(),
	Tween_t278478013::get_offset_of_isRecyclable_20(),
	Tween_t278478013::get_offset_of_isSpeedBased_21(),
	Tween_t278478013::get_offset_of_autoKill_22(),
	Tween_t278478013::get_offset_of_duration_23(),
	Tween_t278478013::get_offset_of_loops_24(),
	Tween_t278478013::get_offset_of_loopType_25(),
	Tween_t278478013::get_offset_of_delay_26(),
	Tween_t278478013::get_offset_of_isRelative_27(),
	Tween_t278478013::get_offset_of_easeType_28(),
	Tween_t278478013::get_offset_of_customEase_29(),
	Tween_t278478013::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t278478013::get_offset_of_easePeriod_31(),
	Tween_t278478013::get_offset_of_typeofT1_32(),
	Tween_t278478013::get_offset_of_typeofT2_33(),
	Tween_t278478013::get_offset_of_typeofTPlugOptions_34(),
	Tween_t278478013::get_offset_of_active_35(),
	Tween_t278478013::get_offset_of_isSequenced_36(),
	Tween_t278478013::get_offset_of_sequenceParent_37(),
	Tween_t278478013::get_offset_of_activeId_38(),
	Tween_t278478013::get_offset_of_specialStartupMode_39(),
	Tween_t278478013::get_offset_of_creationLocked_40(),
	Tween_t278478013::get_offset_of_startupDone_41(),
	Tween_t278478013::get_offset_of_playedOnce_42(),
	Tween_t278478013::get_offset_of_position_43(),
	Tween_t278478013::get_offset_of_fullDuration_44(),
	Tween_t278478013::get_offset_of_completedLoops_45(),
	Tween_t278478013::get_offset_of_isPlaying_46(),
	Tween_t278478013::get_offset_of_isComplete_47(),
	Tween_t278478013::get_offset_of_elapsedDelay_48(),
	Tween_t278478013::get_offset_of_delayComplete_49(),
	Tween_t278478013::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (Tweener_t760404022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[2] = 
{
	Tweener_t760404022::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t760404022::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (TweenType_t169444141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	TweenType_t169444141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (UpdateType_t3357224513)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1837[4] = 
{
	UpdateType_t3357224513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (Color2Plugin_t3433430606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (DoublePlugin_t266400784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (LongPlugin_t1941283029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (UlongPlugin_t3231465400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (Vector3ArrayPlugin_t2378569512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (PathPlugin_t4171842066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (ColorPlugin_t4063724482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (IntPlugin_t180838436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (QuaternionPlugin_t1696644323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (RectOffsetPlugin_t664509336), -1, sizeof(RectOffsetPlugin_t664509336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	RectOffsetPlugin_t664509336_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (RectPlugin_t391797831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (UintPlugin_t1040977389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Vector2Plugin_t2164285386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Vector4Plugin_t2164361360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (StringPlugin_t3620786088), -1, sizeof(StringPlugin_t3620786088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1852[2] = 
{
	StringPlugin_t3620786088_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t3620786088_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (StringPluginExtensions_t3910942986), -1, sizeof(StringPluginExtensions_t3910942986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1853[5] = 
{
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (FloatPlugin_t3639480371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (Vector3Plugin_t2164530409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (OrientType_t1755667719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1857[5] = 
{
	OrientType_t1755667719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (PathOptions_t2659884781)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[15] = 
{
	PathOptions_t2659884781::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isRigidbody_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupRot_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupZRot_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (QuaternionOptions_t466049668)+ sizeof (Il2CppObject), sizeof(QuaternionOptions_t466049668 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[3] = 
{
	QuaternionOptions_t466049668::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_up_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (UintOptions_t2267095136)+ sizeof (Il2CppObject), sizeof(UintOptions_t2267095136_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1860[1] = 
{
	UintOptions_t2267095136::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Vector3ArrayOptions_t2672570171)+ sizeof (Il2CppObject), sizeof(Vector3ArrayOptions_t2672570171_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[3] = 
{
	Vector3ArrayOptions_t2672570171::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (NoOptions_t2508431845)+ sizeof (Il2CppObject), sizeof(NoOptions_t2508431845 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (ColorOptions_t2213017305)+ sizeof (Il2CppObject), sizeof(ColorOptions_t2213017305_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1863[1] = 
{
	ColorOptions_t2213017305::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (FloatOptions_t1421548266)+ sizeof (Il2CppObject), sizeof(FloatOptions_t1421548266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1864[1] = 
{
	FloatOptions_t1421548266::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (RectOptions_t3393635162)+ sizeof (Il2CppObject), sizeof(RectOptions_t3393635162_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[1] = 
{
	RectOptions_t3393635162::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (StringOptions_t2885323933)+ sizeof (Il2CppObject), sizeof(StringOptions_t2885323933_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1866[5] = 
{
	StringOptions_t2885323933::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (VectorOptions_t293385261)+ sizeof (Il2CppObject), sizeof(VectorOptions_t293385261_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[2] = 
{
	VectorOptions_t293385261::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VectorOptions_t293385261::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (SpecialPluginsUtils_t2241999250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (PluginsManager_t3052451537), -1, sizeof(PluginsManager_t3052451537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1873[18] = 
{
	PluginsManager_t3052451537_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__color2Plugin_15(),
	0,
	PluginsManager_t3052451537_StaticFields::get_offset_of__customPlugins_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (ControlPoint_t168081159)+ sizeof (Il2CppObject), sizeof(ControlPoint_t168081159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1874[2] = 
{
	ControlPoint_t168081159::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControlPoint_t168081159::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (ABSPathDecoder_t3294469411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (CatmullRomDecoder_t3014762178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (LinearDecoder_t2073524639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (Path_t2828565993), -1, sizeof(Path_t2828565993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1878[21] = 
{
	Path_t2828565993_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t2828565993_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t2828565993::get_offset_of_wpLengths_2(),
	Path_t2828565993::get_offset_of_type_3(),
	Path_t2828565993::get_offset_of_subdivisionsXSegment_4(),
	Path_t2828565993::get_offset_of_subdivisions_5(),
	Path_t2828565993::get_offset_of_wps_6(),
	Path_t2828565993::get_offset_of_controlPoints_7(),
	Path_t2828565993::get_offset_of_length_8(),
	Path_t2828565993::get_offset_of_isFinalized_9(),
	Path_t2828565993::get_offset_of_timesTable_10(),
	Path_t2828565993::get_offset_of_lengthsTable_11(),
	Path_t2828565993::get_offset_of_linearWPIndex_12(),
	Path_t2828565993::get_offset_of__incrementalClone_13(),
	Path_t2828565993::get_offset_of__incrementalIndex_14(),
	Path_t2828565993::get_offset_of__decoder_15(),
	Path_t2828565993::get_offset_of__changed_16(),
	Path_t2828565993::get_offset_of_nonLinearDrawWps_17(),
	Path_t2828565993::get_offset_of_targetPosition_18(),
	Path_t2828565993::get_offset_of_lookAtPosition_19(),
	Path_t2828565993::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (PureQuaternionPlugin_t3400666973), -1, sizeof(PureQuaternionPlugin_t3400666973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1879[1] = 
{
	PureQuaternionPlugin_t3400666973_StaticFields::get_offset_of__plug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (ABSSequentiable_t2284140720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[4] = 
{
	ABSSequentiable_t2284140720::get_offset_of_tweenType_0(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t2284140720::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (Debugger_t1404542751), -1, sizeof(Debugger_t1404542751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1883[1] = 
{
	Debugger_t1404542751_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (DOTweenComponent_t696744215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[4] = 
{
	DOTweenComponent_t696744215::get_offset_of_inspectorUpdater_2(),
	DOTweenComponent_t696744215::get_offset_of__unscaledTime_3(),
	DOTweenComponent_t696744215::get_offset_of__unscaledDeltaTime_4(),
	DOTweenComponent_t696744215::get_offset_of__duplicateToDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (U3CWaitForCompletionU3Ed__13_t639731943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[3] = 
{
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (U3CWaitForRewindU3Ed__14_t2782210651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[3] = 
{
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (U3CWaitForKillU3Ed__15_t1007991819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[3] = 
{
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (U3CWaitForElapsedLoopsU3Ed__16_t3988581919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (U3CWaitForPositionU3Ed__17_t2034437344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[4] = 
{
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (U3CWaitForStartU3Ed__18_t2341562412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[3] = 
{
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (DOTweenSettings_t873123119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[18] = 
{
	0,
	DOTweenSettings_t873123119::get_offset_of_useSafeMode_3(),
	DOTweenSettings_t873123119::get_offset_of_timeScale_4(),
	DOTweenSettings_t873123119::get_offset_of_useSmoothDeltaTime_5(),
	DOTweenSettings_t873123119::get_offset_of_maxSmoothUnscaledTime_6(),
	DOTweenSettings_t873123119::get_offset_of_showUnityEditorReport_7(),
	DOTweenSettings_t873123119::get_offset_of_logBehaviour_8(),
	DOTweenSettings_t873123119::get_offset_of_drawGizmos_9(),
	DOTweenSettings_t873123119::get_offset_of_defaultRecyclable_10(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoPlay_11(),
	DOTweenSettings_t873123119::get_offset_of_defaultUpdateType_12(),
	DOTweenSettings_t873123119::get_offset_of_defaultTimeScaleIndependent_13(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseType_14(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseOvershootOrAmplitude_15(),
	DOTweenSettings_t873123119::get_offset_of_defaultEasePeriod_16(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoKill_17(),
	DOTweenSettings_t873123119::get_offset_of_defaultLoopType_18(),
	DOTweenSettings_t873123119::get_offset_of_storeSettingsLocation_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (SettingsLocation_t514961325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[4] = 
{
	SettingsLocation_t514961325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (Extensions_t507052800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (SequenceCallback_t2782183128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (TweenManager_t1979661952), -1, sizeof(TweenManager_t1979661952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1895[31] = 
{
	0,
	0,
	0,
	TweenManager_t1979661952_StaticFields::get_offset_of_maxActive_3(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxTweeners_4(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxSequences_5(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveTweens_6(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveDefaultTweens_7(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveLateTweens_8(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveFixedTweens_9(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweens_10(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveDefaultTweens_11(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveLateTweens_12(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveFixedTweens_13(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweeners_14(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveSequences_15(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledTweeners_16(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledSequences_17(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totTweeners_18(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totSequences_19(),
	TweenManager_t1979661952_StaticFields::get_offset_of_isUpdateLoop_20(),
	TweenManager_t1979661952_StaticFields::get_offset_of__activeTweens_21(),
	TweenManager_t1979661952_StaticFields::get_offset_of__pooledTweeners_22(),
	TweenManager_t1979661952_StaticFields::get_offset_of__PooledSequences_23(),
	TweenManager_t1979661952_StaticFields::get_offset_of__KillList_24(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxActiveLookupId_25(),
	TweenManager_t1979661952_StaticFields::get_offset_of__requiresActiveReorganization_26(),
	TweenManager_t1979661952_StaticFields::get_offset_of__reorganizeFromId_27(),
	TweenManager_t1979661952_StaticFields::get_offset_of__minPooledTweenerId_28(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxPooledTweenerId_29(),
	TweenManager_t1979661952_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (CapacityIncreaseMode_t1969140739)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[4] = 
{
	CapacityIncreaseMode_t1969140739::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (Utils_t2524017187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (FilterType_t1425068526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1899[6] = 
{
	FilterType_t1425068526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
