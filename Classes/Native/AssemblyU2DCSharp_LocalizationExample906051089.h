﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizationExample
struct  LocalizationExample_t906051089  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GUIStyle LocalizationExample::GUITextStyle
	GUIStyle_t1799908754 * ___GUITextStyle_2;
	// System.Collections.Generic.List`1<System.String> LocalizationExample::Tokens
	List_1_t1398341365 * ___Tokens_5;

public:
	inline static int32_t get_offset_of_GUITextStyle_2() { return static_cast<int32_t>(offsetof(LocalizationExample_t906051089, ___GUITextStyle_2)); }
	inline GUIStyle_t1799908754 * get_GUITextStyle_2() const { return ___GUITextStyle_2; }
	inline GUIStyle_t1799908754 ** get_address_of_GUITextStyle_2() { return &___GUITextStyle_2; }
	inline void set_GUITextStyle_2(GUIStyle_t1799908754 * value)
	{
		___GUITextStyle_2 = value;
		Il2CppCodeGenWriteBarrier(&___GUITextStyle_2, value);
	}

	inline static int32_t get_offset_of_Tokens_5() { return static_cast<int32_t>(offsetof(LocalizationExample_t906051089, ___Tokens_5)); }
	inline List_1_t1398341365 * get_Tokens_5() const { return ___Tokens_5; }
	inline List_1_t1398341365 ** get_address_of_Tokens_5() { return &___Tokens_5; }
	inline void set_Tokens_5(List_1_t1398341365 * value)
	{
		___Tokens_5 = value;
		Il2CppCodeGenWriteBarrier(&___Tokens_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
