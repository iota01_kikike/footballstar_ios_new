﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.String
struct String_t;
// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t505099;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1
struct  U3CU3Ec__DisplayClass7_1_t2729388454  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::dic
	Dictionary_2_t309261261 * ___dic_0;
	// System.String UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::transactionId
	String_t* ___transactionId_1;
	// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0 UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass7_0_t505099 * ___CSU24U3CU3E8__locals1_2;

public:
	inline static int32_t get_offset_of_dic_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t2729388454, ___dic_0)); }
	inline Dictionary_2_t309261261 * get_dic_0() const { return ___dic_0; }
	inline Dictionary_2_t309261261 ** get_address_of_dic_0() { return &___dic_0; }
	inline void set_dic_0(Dictionary_2_t309261261 * value)
	{
		___dic_0 = value;
		Il2CppCodeGenWriteBarrier(&___dic_0, value);
	}

	inline static int32_t get_offset_of_transactionId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t2729388454, ___transactionId_1)); }
	inline String_t* get_transactionId_1() const { return ___transactionId_1; }
	inline String_t** get_address_of_transactionId_1() { return &___transactionId_1; }
	inline void set_transactionId_1(String_t* value)
	{
		___transactionId_1 = value;
		Il2CppCodeGenWriteBarrier(&___transactionId_1, value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t2729388454, ___CSU24U3CU3E8__locals1_2)); }
	inline U3CU3Ec__DisplayClass7_0_t505099 * get_CSU24U3CU3E8__locals1_2() const { return ___CSU24U3CU3E8__locals1_2; }
	inline U3CU3Ec__DisplayClass7_0_t505099 ** get_address_of_CSU24U3CU3E8__locals1_2() { return &___CSU24U3CU3E8__locals1_2; }
	inline void set_CSU24U3CU3E8__locals1_2(U3CU3Ec__DisplayClass7_0_t505099 * value)
	{
		___CSU24U3CU3E8__locals1_2 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E8__locals1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
