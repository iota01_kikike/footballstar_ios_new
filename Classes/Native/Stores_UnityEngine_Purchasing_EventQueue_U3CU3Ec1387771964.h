﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.EventQueue/<>c
struct U3CU3Ec_t1387771964;
// System.Action`1<System.String>
struct Action_1_t1831019615;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventQueue/<>c
struct  U3CU3Ec_t1387771964  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t1387771964_StaticFields
{
public:
	// UnityEngine.Purchasing.EventQueue/<>c UnityEngine.Purchasing.EventQueue/<>c::<>9
	U3CU3Ec_t1387771964 * ___U3CU3E9_0;
	// System.Action`1<System.String> UnityEngine.Purchasing.EventQueue/<>c::<>9__11_0
	Action_1_t1831019615 * ___U3CU3E9__11_0_1;
	// System.Action`1<System.String> UnityEngine.Purchasing.EventQueue/<>c::<>9__11_3
	Action_1_t1831019615 * ___U3CU3E9__11_3_2;
	// System.Action`1<System.String> UnityEngine.Purchasing.EventQueue/<>c::<>9__11_4
	Action_1_t1831019615 * ___U3CU3E9__11_4_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1387771964_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1387771964 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1387771964 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1387771964 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1387771964_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Action_1_t1831019615 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Action_1_t1831019615 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__11_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1387771964_StaticFields, ___U3CU3E9__11_3_2)); }
	inline Action_1_t1831019615 * get_U3CU3E9__11_3_2() const { return ___U3CU3E9__11_3_2; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3E9__11_3_2() { return &___U3CU3E9__11_3_2; }
	inline void set_U3CU3E9__11_3_2(Action_1_t1831019615 * value)
	{
		___U3CU3E9__11_3_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__11_3_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_4_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1387771964_StaticFields, ___U3CU3E9__11_4_3)); }
	inline Action_1_t1831019615 * get_U3CU3E9__11_4_3() const { return ___U3CU3E9__11_4_3; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3E9__11_4_3() { return &___U3CU3E9__11_4_3; }
	inline void set_U3CU3E9__11_4_3(Action_1_t1831019615 * value)
	{
		___U3CU3E9__11_4_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__11_4_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
