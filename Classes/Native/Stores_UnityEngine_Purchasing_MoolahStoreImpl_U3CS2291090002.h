﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t3256166369;
// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String>
struct Action_3_t4044292979;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4206626141;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23
struct  U3CStartPurchasePollingU3Ed__23_t2291090002  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::authGlobal
	String_t* ___authGlobal_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::transactionId
	String_t* ___transactionId_3;
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::purchaseSucceed
	Action_3_t3256166369 * ___purchaseSucceed_4;
	// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::purchaseFailed
	Action_3_t4044292979 * ___purchaseFailed_5;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<>4__this
	MoolahStoreImpl_t4206626141 * ___U3CU3E4__this_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<orderSuccess>5__1
	String_t* ___U3CorderSuccessU3E5__1_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<signstr>5__2
	String_t* ___U3CsignstrU3E5__2_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<sign>5__3
	String_t* ___U3CsignU3E5__3_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<param>5__4
	String_t* ___U3CparamU3E5__4_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<url>5__5
	String_t* ___U3CurlU3E5__5_11;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<pollingstr>5__6
	WWW_t2919945039 * ___U3CpollingstrU3E5__6_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<jsonPollingObjects>5__7
	Dictionary_2_t309261261 * ___U3CjsonPollingObjectsU3E5__7_13;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<code>5__8
	String_t* ___U3CcodeU3E5__8_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<pollingValues>5__9
	Dictionary_2_t309261261 * ___U3CpollingValuesU3E5__9_15;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<tradeSeq>5__10
	String_t* ___U3CtradeSeqU3E5__10_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<tradeState>5__11
	String_t* ___U3CtradeStateU3E5__11_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<productId>5__12
	String_t* ___U3CproductIdU3E5__12_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<Msg>5__13
	String_t* ___U3CMsgU3E5__13_19;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<receipt>5__14
	String_t* ___U3CreceiptU3E5__14_20;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_authGlobal_2() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___authGlobal_2)); }
	inline String_t* get_authGlobal_2() const { return ___authGlobal_2; }
	inline String_t** get_address_of_authGlobal_2() { return &___authGlobal_2; }
	inline void set_authGlobal_2(String_t* value)
	{
		___authGlobal_2 = value;
		Il2CppCodeGenWriteBarrier(&___authGlobal_2, value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier(&___transactionId_3, value);
	}

	inline static int32_t get_offset_of_purchaseSucceed_4() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___purchaseSucceed_4)); }
	inline Action_3_t3256166369 * get_purchaseSucceed_4() const { return ___purchaseSucceed_4; }
	inline Action_3_t3256166369 ** get_address_of_purchaseSucceed_4() { return &___purchaseSucceed_4; }
	inline void set_purchaseSucceed_4(Action_3_t3256166369 * value)
	{
		___purchaseSucceed_4 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseSucceed_4, value);
	}

	inline static int32_t get_offset_of_purchaseFailed_5() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___purchaseFailed_5)); }
	inline Action_3_t4044292979 * get_purchaseFailed_5() const { return ___purchaseFailed_5; }
	inline Action_3_t4044292979 ** get_address_of_purchaseFailed_5() { return &___purchaseFailed_5; }
	inline void set_purchaseFailed_5(Action_3_t4044292979 * value)
	{
		___purchaseFailed_5 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseFailed_5, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CU3E4__this_6)); }
	inline MoolahStoreImpl_t4206626141 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline MoolahStoreImpl_t4206626141 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(MoolahStoreImpl_t4206626141 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_6, value);
	}

	inline static int32_t get_offset_of_U3CorderSuccessU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CorderSuccessU3E5__1_7)); }
	inline String_t* get_U3CorderSuccessU3E5__1_7() const { return ___U3CorderSuccessU3E5__1_7; }
	inline String_t** get_address_of_U3CorderSuccessU3E5__1_7() { return &___U3CorderSuccessU3E5__1_7; }
	inline void set_U3CorderSuccessU3E5__1_7(String_t* value)
	{
		___U3CorderSuccessU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CorderSuccessU3E5__1_7, value);
	}

	inline static int32_t get_offset_of_U3CsignstrU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CsignstrU3E5__2_8)); }
	inline String_t* get_U3CsignstrU3E5__2_8() const { return ___U3CsignstrU3E5__2_8; }
	inline String_t** get_address_of_U3CsignstrU3E5__2_8() { return &___U3CsignstrU3E5__2_8; }
	inline void set_U3CsignstrU3E5__2_8(String_t* value)
	{
		___U3CsignstrU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsignstrU3E5__2_8, value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_9() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CsignU3E5__3_9)); }
	inline String_t* get_U3CsignU3E5__3_9() const { return ___U3CsignU3E5__3_9; }
	inline String_t** get_address_of_U3CsignU3E5__3_9() { return &___U3CsignU3E5__3_9; }
	inline void set_U3CsignU3E5__3_9(String_t* value)
	{
		___U3CsignU3E5__3_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsignU3E5__3_9, value);
	}

	inline static int32_t get_offset_of_U3CparamU3E5__4_10() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CparamU3E5__4_10)); }
	inline String_t* get_U3CparamU3E5__4_10() const { return ___U3CparamU3E5__4_10; }
	inline String_t** get_address_of_U3CparamU3E5__4_10() { return &___U3CparamU3E5__4_10; }
	inline void set_U3CparamU3E5__4_10(String_t* value)
	{
		___U3CparamU3E5__4_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparamU3E5__4_10, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E5__5_11() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CurlU3E5__5_11)); }
	inline String_t* get_U3CurlU3E5__5_11() const { return ___U3CurlU3E5__5_11; }
	inline String_t** get_address_of_U3CurlU3E5__5_11() { return &___U3CurlU3E5__5_11; }
	inline void set_U3CurlU3E5__5_11(String_t* value)
	{
		___U3CurlU3E5__5_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E5__5_11, value);
	}

	inline static int32_t get_offset_of_U3CpollingstrU3E5__6_12() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CpollingstrU3E5__6_12)); }
	inline WWW_t2919945039 * get_U3CpollingstrU3E5__6_12() const { return ___U3CpollingstrU3E5__6_12; }
	inline WWW_t2919945039 ** get_address_of_U3CpollingstrU3E5__6_12() { return &___U3CpollingstrU3E5__6_12; }
	inline void set_U3CpollingstrU3E5__6_12(WWW_t2919945039 * value)
	{
		___U3CpollingstrU3E5__6_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpollingstrU3E5__6_12, value);
	}

	inline static int32_t get_offset_of_U3CjsonPollingObjectsU3E5__7_13() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CjsonPollingObjectsU3E5__7_13)); }
	inline Dictionary_2_t309261261 * get_U3CjsonPollingObjectsU3E5__7_13() const { return ___U3CjsonPollingObjectsU3E5__7_13; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CjsonPollingObjectsU3E5__7_13() { return &___U3CjsonPollingObjectsU3E5__7_13; }
	inline void set_U3CjsonPollingObjectsU3E5__7_13(Dictionary_2_t309261261 * value)
	{
		___U3CjsonPollingObjectsU3E5__7_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonPollingObjectsU3E5__7_13, value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__8_14() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CcodeU3E5__8_14)); }
	inline String_t* get_U3CcodeU3E5__8_14() const { return ___U3CcodeU3E5__8_14; }
	inline String_t** get_address_of_U3CcodeU3E5__8_14() { return &___U3CcodeU3E5__8_14; }
	inline void set_U3CcodeU3E5__8_14(String_t* value)
	{
		___U3CcodeU3E5__8_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcodeU3E5__8_14, value);
	}

	inline static int32_t get_offset_of_U3CpollingValuesU3E5__9_15() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CpollingValuesU3E5__9_15)); }
	inline Dictionary_2_t309261261 * get_U3CpollingValuesU3E5__9_15() const { return ___U3CpollingValuesU3E5__9_15; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CpollingValuesU3E5__9_15() { return &___U3CpollingValuesU3E5__9_15; }
	inline void set_U3CpollingValuesU3E5__9_15(Dictionary_2_t309261261 * value)
	{
		___U3CpollingValuesU3E5__9_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpollingValuesU3E5__9_15, value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__10_16() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CtradeSeqU3E5__10_16)); }
	inline String_t* get_U3CtradeSeqU3E5__10_16() const { return ___U3CtradeSeqU3E5__10_16; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__10_16() { return &___U3CtradeSeqU3E5__10_16; }
	inline void set_U3CtradeSeqU3E5__10_16(String_t* value)
	{
		___U3CtradeSeqU3E5__10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtradeSeqU3E5__10_16, value);
	}

	inline static int32_t get_offset_of_U3CtradeStateU3E5__11_17() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CtradeStateU3E5__11_17)); }
	inline String_t* get_U3CtradeStateU3E5__11_17() const { return ___U3CtradeStateU3E5__11_17; }
	inline String_t** get_address_of_U3CtradeStateU3E5__11_17() { return &___U3CtradeStateU3E5__11_17; }
	inline void set_U3CtradeStateU3E5__11_17(String_t* value)
	{
		___U3CtradeStateU3E5__11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtradeStateU3E5__11_17, value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__12_18() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CproductIdU3E5__12_18)); }
	inline String_t* get_U3CproductIdU3E5__12_18() const { return ___U3CproductIdU3E5__12_18; }
	inline String_t** get_address_of_U3CproductIdU3E5__12_18() { return &___U3CproductIdU3E5__12_18; }
	inline void set_U3CproductIdU3E5__12_18(String_t* value)
	{
		___U3CproductIdU3E5__12_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CproductIdU3E5__12_18, value);
	}

	inline static int32_t get_offset_of_U3CMsgU3E5__13_19() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CMsgU3E5__13_19)); }
	inline String_t* get_U3CMsgU3E5__13_19() const { return ___U3CMsgU3E5__13_19; }
	inline String_t** get_address_of_U3CMsgU3E5__13_19() { return &___U3CMsgU3E5__13_19; }
	inline void set_U3CMsgU3E5__13_19(String_t* value)
	{
		___U3CMsgU3E5__13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMsgU3E5__13_19, value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__14_20() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t2291090002, ___U3CreceiptU3E5__14_20)); }
	inline String_t* get_U3CreceiptU3E5__14_20() const { return ___U3CreceiptU3E5__14_20; }
	inline String_t** get_address_of_U3CreceiptU3E5__14_20() { return &___U3CreceiptU3E5__14_20; }
	inline void set_U3CreceiptU3E5__14_20(String_t* value)
	{
		___U3CreceiptU3E5__14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreceiptU3E5__14_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
