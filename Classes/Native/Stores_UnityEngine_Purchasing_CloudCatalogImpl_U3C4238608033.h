﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.CloudCatalogImpl/<>c
struct U3CU3Ec_t4238608033;
// System.Func`3<System.Char,System.Int32,System.String>
struct Func_3_t2237848711;
// System.Func`3<System.String,System.String,System.String>
struct Func_3_t1214888473;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl/<>c
struct  U3CU3Ec_t4238608033  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t4238608033_StaticFields
{
public:
	// UnityEngine.Purchasing.CloudCatalogImpl/<>c UnityEngine.Purchasing.CloudCatalogImpl/<>c::<>9
	U3CU3Ec_t4238608033 * ___U3CU3E9_0;
	// System.Func`3<System.Char,System.Int32,System.String> UnityEngine.Purchasing.CloudCatalogImpl/<>c::<>9__12_0
	Func_3_t2237848711 * ___U3CU3E9__12_0_1;
	// System.Func`3<System.String,System.String,System.String> UnityEngine.Purchasing.CloudCatalogImpl/<>c::<>9__12_1
	Func_3_t1214888473 * ___U3CU3E9__12_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4238608033_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4238608033 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4238608033 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4238608033 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4238608033_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_3_t2237848711 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_3_t2237848711 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_3_t2237848711 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__12_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4238608033_StaticFields, ___U3CU3E9__12_1_2)); }
	inline Func_3_t1214888473 * get_U3CU3E9__12_1_2() const { return ___U3CU3E9__12_1_2; }
	inline Func_3_t1214888473 ** get_address_of_U3CU3E9__12_1_2() { return &___U3CU3E9__12_1_2; }
	inline void set_U3CU3E9__12_1_2(Func_3_t1214888473 * value)
	{
		___U3CU3E9__12_1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__12_1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
