﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"

// UnityEngine.Purchasing.INativeUnityChannelStore
struct INativeUnityChannelStore_t670691399;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl
struct  UnityChannelImpl_t1327714682  : public JSONStore_t1890359403
{
public:
	// UnityEngine.Purchasing.INativeUnityChannelStore UnityEngine.Purchasing.UnityChannelImpl::m_Bindings
	Il2CppObject * ___m_Bindings_22;
	// System.String UnityEngine.Purchasing.UnityChannelImpl::m_LastPurchaseError
	String_t* ___m_LastPurchaseError_23;
	// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Bindings_22() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t1327714682, ___m_Bindings_22)); }
	inline Il2CppObject * get_m_Bindings_22() const { return ___m_Bindings_22; }
	inline Il2CppObject ** get_address_of_m_Bindings_22() { return &___m_Bindings_22; }
	inline void set_m_Bindings_22(Il2CppObject * value)
	{
		___m_Bindings_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_Bindings_22, value);
	}

	inline static int32_t get_offset_of_m_LastPurchaseError_23() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t1327714682, ___m_LastPurchaseError_23)); }
	inline String_t* get_m_LastPurchaseError_23() const { return ___m_LastPurchaseError_23; }
	inline String_t** get_address_of_m_LastPurchaseError_23() { return &___m_LastPurchaseError_23; }
	inline void set_m_LastPurchaseError_23(String_t* value)
	{
		___m_LastPurchaseError_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_LastPurchaseError_23, value);
	}

	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t1327714682, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
