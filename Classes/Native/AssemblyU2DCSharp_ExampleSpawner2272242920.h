﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSpawner2
struct  ExampleSpawner2_t272242920  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 ExampleSpawner2::StartPos
	Vector2_t2243707579  ___StartPos_2;
	// System.Int32[] ExampleSpawner2::SpawnArray
	Int32U5BU5D_t3030399641* ___SpawnArray_3;
	// UnityEngine.GameObject ExampleSpawner2::spawnPattern
	GameObject_t1756533147 * ___spawnPattern_4;
	// System.Boolean ExampleSpawner2::IsStarted
	bool ___IsStarted_5;

public:
	inline static int32_t get_offset_of_StartPos_2() { return static_cast<int32_t>(offsetof(ExampleSpawner2_t272242920, ___StartPos_2)); }
	inline Vector2_t2243707579  get_StartPos_2() const { return ___StartPos_2; }
	inline Vector2_t2243707579 * get_address_of_StartPos_2() { return &___StartPos_2; }
	inline void set_StartPos_2(Vector2_t2243707579  value)
	{
		___StartPos_2 = value;
	}

	inline static int32_t get_offset_of_SpawnArray_3() { return static_cast<int32_t>(offsetof(ExampleSpawner2_t272242920, ___SpawnArray_3)); }
	inline Int32U5BU5D_t3030399641* get_SpawnArray_3() const { return ___SpawnArray_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_SpawnArray_3() { return &___SpawnArray_3; }
	inline void set_SpawnArray_3(Int32U5BU5D_t3030399641* value)
	{
		___SpawnArray_3 = value;
		Il2CppCodeGenWriteBarrier(&___SpawnArray_3, value);
	}

	inline static int32_t get_offset_of_spawnPattern_4() { return static_cast<int32_t>(offsetof(ExampleSpawner2_t272242920, ___spawnPattern_4)); }
	inline GameObject_t1756533147 * get_spawnPattern_4() const { return ___spawnPattern_4; }
	inline GameObject_t1756533147 ** get_address_of_spawnPattern_4() { return &___spawnPattern_4; }
	inline void set_spawnPattern_4(GameObject_t1756533147 * value)
	{
		___spawnPattern_4 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPattern_4, value);
	}

	inline static int32_t get_offset_of_IsStarted_5() { return static_cast<int32_t>(offsetof(ExampleSpawner2_t272242920, ___IsStarted_5)); }
	inline bool get_IsStarted_5() const { return ___IsStarted_5; }
	inline bool* get_address_of_IsStarted_5() { return &___IsStarted_5; }
	inline void set_IsStarted_5(bool value)
	{
		___IsStarted_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
