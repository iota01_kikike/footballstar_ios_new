﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem977711995.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogItem_U3197607536.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog2667590766.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalog_U3CU3E651058539.h"
#include "Stores_UnityEngine_Purchasing_ProductCatalogImpl2093457986.h"
#include "Stores_UnityEngine_Purchasing_ProductDefinitionExt4078059078.h"
#include "Stores_UnityEngine_Purchasing_ProfileData3353328249.h"
#include "Stores_UnityEngine_Purchasing_Promo1328780891.h"
#include "Stores_UnityEngine_Purchasing_PurchasingEvent2028586576.h"
#include "Stores_UnityEngine_Purchasing_PurchasingEvent_U3CU4258048749.h"
#include "Stores_UnityEngine_Purchasing_QueryHelper2073238582.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMod107230755.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo1310600573.h"
#include "Stores_UnityEngine_Purchasing_StoreCatalogImpl946505162.h"
#include "Stores_UnityEngine_Purchasing_StoreCatalogImpl_U3C3892298273.h"
#include "Stores_UnityEngine_Purchasing_StoreConfiguration2466794143.h"
#include "Stores_UnityEngine_Purchasing_StoreSpecificPurchas1224749471.h"
#include "Stores_UnityEngine_Purchasing_SubscriptionManager1070011402.h"
#include "Stores_UnityEngine_Purchasing_SubscriptionManager_3750601015.h"
#include "Stores_UnityEngine_Purchasing_SubscriptionInfo2237741923.h"
#include "Stores_UnityEngine_Purchasing_Result3399123711.h"
#include "Stores_UnityEngine_Purchasing_SubscriptionPeriodUn1045823750.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreProductTyp2549370150.h"
#include "Stores_UnityEngine_Purchasing_ReceiptParserExceptio479172278.h"
#include "Stores_UnityEngine_Purchasing_InvalidProductTypeEx1957211787.h"
#include "Stores_UnityEngine_Purchasing_NullProductIdExceptio622699332.h"
#include "Stores_UnityEngine_Purchasing_NullReceiptException2749137490.h"
#include "Stores_UnityEngine_Purchasing_StoreSubscriptionInf3399659024.h"
#include "Stores_UnityEngine_Purchasing_UnifiedReceipt2654419430.h"
#include "Stores_UnityEngine_Purchasing_FileReference4250167507.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil166323129.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil_U474491015.h"
#include "Stores_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Stores_U3CPrivateImplementationDetailsU3E___Static1542897494.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP1937545187.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP1611953012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2350381201.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3744281392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP1356006596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFPS432421409.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2432920949.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP1329736715.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP1493205377.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2998525377.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3651167660.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3479716230.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2282579641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LabelEffect1942779263.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3904105026.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3010799121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP1880715370.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2523981134.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3474909705.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1505541237.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2686455593.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1905710121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1635666085.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2708612136.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1152147286.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1804770419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3307996606.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2911810544.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O337339225.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O875138049.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1936922347.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3299336390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1508301530.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3265587138.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2034359298.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3198542555.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O656831461.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3872791569.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2721367689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1454344260.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3458667235.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_2380454615.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_4226477674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_4105365306.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3487530033.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O692732302.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O693053169.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_Ob79425124.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_4178408852.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O544383870.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_4003483512.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O544383869.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1070189624.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3880897212.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Basics2766259351.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Sequences3565862770.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NativeShare1150945090.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (ProductCatalogItem_t977711995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[9] = 
{
	ProductCatalogItem_t977711995::get_offset_of_id_0(),
	ProductCatalogItem_t977711995::get_offset_of_type_1(),
	ProductCatalogItem_t977711995::get_offset_of_storeIDs_2(),
	ProductCatalogItem_t977711995::get_offset_of_defaultDescription_3(),
	ProductCatalogItem_t977711995::get_offset_of_applePriceTier_4(),
	ProductCatalogItem_t977711995::get_offset_of_xiaomiPriceTier_5(),
	ProductCatalogItem_t977711995::get_offset_of_googlePrice_6(),
	ProductCatalogItem_t977711995::get_offset_of_descriptions_7(),
	ProductCatalogItem_t977711995::get_offset_of_payouts_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CU3Ec__DisplayClass21_0_t3197607536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[1] = 
{
	U3CU3Ec__DisplayClass21_0_t3197607536::get_offset_of_locale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (ProductCatalog_t2667590766), -1, sizeof(ProductCatalog_t2667590766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2302[3] = 
{
	ProductCatalog_t2667590766_StaticFields::get_offset_of_instance_0(),
	ProductCatalog_t2667590766::get_offset_of_enableCodelessAutoInitialization_1(),
	ProductCatalog_t2667590766::get_offset_of_products_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (U3CU3Ec_t651058539), -1, sizeof(U3CU3Ec_t651058539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2303[2] = 
{
	U3CU3Ec_t651058539_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t651058539_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (ProductCatalogImpl_t2093457986), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (ProductDefinitionExtensions_t4078059078), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (ProfileData_t3353328249), -1, sizeof(ProfileData_t3353328249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[20] = 
{
	ProfileData_t3353328249_StaticFields::get_offset_of_ProfileInstance_0(),
	ProfileData_t3353328249::get_offset_of_U3CAppIdU3Ek__BackingField_1(),
	ProfileData_t3353328249::get_offset_of_U3CUserIdU3Ek__BackingField_2(),
	ProfileData_t3353328249::get_offset_of_U3CSessionIdU3Ek__BackingField_3(),
	ProfileData_t3353328249::get_offset_of_U3CPlatformU3Ek__BackingField_4(),
	ProfileData_t3353328249::get_offset_of_U3CPlatformIdU3Ek__BackingField_5(),
	ProfileData_t3353328249::get_offset_of_U3CSdkVerU3Ek__BackingField_6(),
	ProfileData_t3353328249::get_offset_of_U3CDeviceIdU3Ek__BackingField_7(),
	ProfileData_t3353328249::get_offset_of_U3CBuildGUIDU3Ek__BackingField_8(),
	ProfileData_t3353328249::get_offset_of_U3CIapVerU3Ek__BackingField_9(),
	ProfileData_t3353328249::get_offset_of_U3CAdsGamerTokenU3Ek__BackingField_10(),
	ProfileData_t3353328249::get_offset_of_U3CTrackingOptOutU3Ek__BackingField_11(),
	ProfileData_t3353328249::get_offset_of_U3CAdsABGroupU3Ek__BackingField_12(),
	ProfileData_t3353328249::get_offset_of_U3CAdsGameIdU3Ek__BackingField_13(),
	ProfileData_t3353328249::get_offset_of_U3CStoreABGroupU3Ek__BackingField_14(),
	ProfileData_t3353328249::get_offset_of_U3CCatalogIdU3Ek__BackingField_15(),
	ProfileData_t3353328249::get_offset_of_U3CMonetizationIdU3Ek__BackingField_16(),
	ProfileData_t3353328249::get_offset_of_U3CStoreNameU3Ek__BackingField_17(),
	ProfileData_t3353328249::get_offset_of_U3CGameVersionU3Ek__BackingField_18(),
	ProfileData_t3353328249::get_offset_of_U3CStoreTestEnabledU3Ek__BackingField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (Promo_t1328780891), -1, sizeof(Promo_t1328780891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[9] = 
{
	Promo_t1328780891_StaticFields::get_offset_of_s_PromoPurchaser_0(),
	Promo_t1328780891_StaticFields::get_offset_of_s_Unity_1(),
	Promo_t1328780891_StaticFields::get_offset_of_s_RuntimePlatform_2(),
	Promo_t1328780891_StaticFields::get_offset_of_s_Logger_3(),
	Promo_t1328780891_StaticFields::get_offset_of_s_Version_4(),
	Promo_t1328780891_StaticFields::get_offset_of_s_Util_5(),
	Promo_t1328780891_StaticFields::get_offset_of_s_WebUtil_6(),
	Promo_t1328780891_StaticFields::get_offset_of_s_IsReady_7(),
	Promo_t1328780891_StaticFields::get_offset_of_s_ProductJSON_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (PurchasingEvent_t2028586576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[1] = 
{
	PurchasingEvent_t2028586576::get_offset_of_EventDict_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (U3CU3Ec_t4258048749), -1, sizeof(U3CU3Ec_t4258048749_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2310[3] = 
{
	U3CU3Ec_t4258048749_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4258048749_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_t4258048749_StaticFields::get_offset_of_U3CU3E9__2_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (QueryHelper_t2073238582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (StandardPurchasingModule_t4003664591), -1, sizeof(StandardPurchasingModule_t4003664591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[14] = 
{
	StandardPurchasingModule_t4003664591::get_offset_of_m_AppStorePlatform_1(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_NativeStoreProvider_2(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_RuntimePlatform_3(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_UseCloudCatalog_4(),
	StandardPurchasingModule_t4003664591_StaticFields::get_offset_of_ModuleInstance_5(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CutilU3Ek__BackingField_6(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CloggerU3Ek__BackingField_7(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CwebUtilU3Ek__BackingField_8(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CstoreInstanceU3Ek__BackingField_9(),
	StandardPurchasingModule_t4003664591_StaticFields::get_offset_of_AndroidStoreNameMap_10(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_CloudCatalog_11(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13(),
	StandardPurchasingModule_t4003664591::get_offset_of_windowsStore_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (StoreInstance_t107230755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[2] = 
{
	StoreInstance_t107230755::get_offset_of_U3CstoreNameU3Ek__BackingField_0(),
	StoreInstance_t107230755::get_offset_of_U3CinstanceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (MicrosoftConfiguration_t1310600573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[2] = 
{
	MicrosoftConfiguration_t1310600573::get_offset_of_useMock_0(),
	MicrosoftConfiguration_t1310600573::get_offset_of_module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (StoreCatalogImpl_t946505162), -1, sizeof(StoreCatalogImpl_t946505162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2315[6] = 
{
	StoreCatalogImpl_t946505162::get_offset_of_m_AsyncUtil_0(),
	StoreCatalogImpl_t946505162::get_offset_of_m_Logger_1(),
	StoreCatalogImpl_t946505162::get_offset_of_m_CatalogURL_2(),
	StoreCatalogImpl_t946505162::get_offset_of_m_StoreName_3(),
	StoreCatalogImpl_t946505162::get_offset_of_m_cachedStoreCatalogReference_4(),
	StoreCatalogImpl_t946505162_StaticFields::get_offset_of_profile_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (U3CU3Ec__DisplayClass10_0_t3892298273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[2] = 
{
	U3CU3Ec__DisplayClass10_0_t3892298273::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t3892298273::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (StoreConfiguration_t2466794143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	StoreConfiguration_t2466794143::get_offset_of_U3CandroidStoreU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (StoreSpecificPurchaseErrorCode_t1224749471)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[31] = 
{
	StoreSpecificPurchaseErrorCode_t1224749471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (SubscriptionManager_t1070011402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[3] = 
{
	SubscriptionManager_t1070011402::get_offset_of_receipt_0(),
	SubscriptionManager_t1070011402::get_offset_of_productId_1(),
	SubscriptionManager_t1070011402::get_offset_of_intro_json_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (U3CU3Ec_t3750601015), -1, sizeof(U3CU3Ec_t3750601015_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2320[2] = 
{
	U3CU3Ec_t3750601015_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3750601015_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (SubscriptionInfo_t2237741923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[18] = 
{
	SubscriptionInfo_t2237741923::get_offset_of_is_subscribed_0(),
	SubscriptionInfo_t2237741923::get_offset_of_is_expired_1(),
	SubscriptionInfo_t2237741923::get_offset_of_is_cancelled_2(),
	SubscriptionInfo_t2237741923::get_offset_of_is_free_trial_3(),
	SubscriptionInfo_t2237741923::get_offset_of_is_auto_renewing_4(),
	SubscriptionInfo_t2237741923::get_offset_of_is_introductory_price_period_5(),
	SubscriptionInfo_t2237741923::get_offset_of_productId_6(),
	SubscriptionInfo_t2237741923::get_offset_of_purchaseDate_7(),
	SubscriptionInfo_t2237741923::get_offset_of_subscriptionExpireDate_8(),
	SubscriptionInfo_t2237741923::get_offset_of_subscriptionCancelDate_9(),
	SubscriptionInfo_t2237741923::get_offset_of_remainedTime_10(),
	SubscriptionInfo_t2237741923::get_offset_of_introductory_price_11(),
	SubscriptionInfo_t2237741923::get_offset_of_introductory_price_period_12(),
	SubscriptionInfo_t2237741923::get_offset_of_introductory_price_cycles_13(),
	SubscriptionInfo_t2237741923::get_offset_of_freeTrialPeriod_14(),
	SubscriptionInfo_t2237741923::get_offset_of_subscriptionPeriod_15(),
	SubscriptionInfo_t2237741923::get_offset_of_free_trial_period_string_16(),
	SubscriptionInfo_t2237741923::get_offset_of_sku_details_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (Result_t3399123711)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2322[4] = 
{
	Result_t3399123711::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (SubscriptionPeriodUnit_t1045823750)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2323[6] = 
{
	SubscriptionPeriodUnit_t1045823750::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (AppleStoreProductType_t2549370150)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[5] = 
{
	AppleStoreProductType_t2549370150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (ReceiptParserException_t479172278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (InvalidProductTypeException_t1957211787), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (NullProductIdException_t622699332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (NullReceiptException_t2749137490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (StoreSubscriptionInfoNotSupportedException_t3399659024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (UnifiedReceipt_t2654419430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[1] = 
{
	UnifiedReceipt_t2654419430::get_offset_of_Payload_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (FileReference_t4250167507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	FileReference_t4250167507::get_offset_of_m_FilePath_0(),
	FileReference_t4250167507::get_offset_of_m_Logger_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (UnityUtil_t166323129), -1, sizeof(UnityUtil_t166323129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2332[4] = 
{
	UnityUtil_t166323129_StaticFields::get_offset_of_s_Callbacks_2(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_CallbacksPending_3(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_PcControlledPlatforms_4(),
	UnityUtil_t166323129::get_offset_of_pauseListeners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (U3CDelayedCoroutineU3Ed__41_t474491015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[5] = 
{
	U3CDelayedCoroutineU3Ed__41_t474491015::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedCoroutineU3Ed__41_t474491015::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedCoroutineU3Ed__41_t474491015::get_offset_of_coroutine_2(),
	U3CDelayedCoroutineU3Ed__41_t474491015::get_offset_of_delay_3(),
	U3CDelayedCoroutineU3Ed__41_t474491015::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305145), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2334[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U38ACBA6DEDC6FDA919C1CE7B29030BCA705F0CF41_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (__StaticArrayInitTypeSizeU3D368_t1542897494)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D368_t1542897494 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (U3CModuleU3E_t3783534233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2338[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (APITester_t1937545187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[3] = 
{
	APITester_t1937545187::get_offset_of_selectedTab_2(),
	APITester_t1937545187::get_offset_of_tabs_3(),
	APITester_t1937545187::get_offset_of_currentFPSLevel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (AFPSCounter_t1611953012), -1, sizeof(AFPSCounter_t1611953012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2343[46] = 
{
	0,
	0,
	0,
	0,
	0,
	AFPSCounter_t1611953012::get_offset_of_fpsCounter_7(),
	AFPSCounter_t1611953012::get_offset_of_memoryCounter_8(),
	AFPSCounter_t1611953012::get_offset_of_deviceInfoCounter_9(),
	AFPSCounter_t1611953012::get_offset_of_hotKey_10(),
	AFPSCounter_t1611953012::get_offset_of_circleGesture_11(),
	AFPSCounter_t1611953012::get_offset_of_hotKeyCtrl_12(),
	AFPSCounter_t1611953012::get_offset_of_hotKeyShift_13(),
	AFPSCounter_t1611953012::get_offset_of_hotKeyAlt_14(),
	AFPSCounter_t1611953012::get_offset_of_keepAlive_15(),
	AFPSCounter_t1611953012::get_offset_of_canvas_16(),
	AFPSCounter_t1611953012::get_offset_of_canvasScaler_17(),
	AFPSCounter_t1611953012::get_offset_of_externalCanvas_18(),
	AFPSCounter_t1611953012::get_offset_of_labels_19(),
	AFPSCounter_t1611953012::get_offset_of_anchorsCount_20(),
	AFPSCounter_t1611953012::get_offset_of_cachedVSync_21(),
	AFPSCounter_t1611953012::get_offset_of_cachedFrameRate_22(),
	AFPSCounter_t1611953012::get_offset_of_inited_23(),
	AFPSCounter_t1611953012::get_offset_of_gesturePoints_24(),
	AFPSCounter_t1611953012::get_offset_of_gestureCount_25(),
	AFPSCounter_t1611953012::get_offset_of_operationMode_26(),
	AFPSCounter_t1611953012::get_offset_of_forceFrameRate_27(),
	AFPSCounter_t1611953012::get_offset_of_forcedFrameRate_28(),
	AFPSCounter_t1611953012::get_offset_of_background_29(),
	AFPSCounter_t1611953012::get_offset_of_backgroundColor_30(),
	AFPSCounter_t1611953012::get_offset_of_backgroundPadding_31(),
	AFPSCounter_t1611953012::get_offset_of_shadow_32(),
	AFPSCounter_t1611953012::get_offset_of_shadowColor_33(),
	AFPSCounter_t1611953012::get_offset_of_shadowDistance_34(),
	AFPSCounter_t1611953012::get_offset_of_outline_35(),
	AFPSCounter_t1611953012::get_offset_of_outlineColor_36(),
	AFPSCounter_t1611953012::get_offset_of_outlineDistance_37(),
	AFPSCounter_t1611953012::get_offset_of_autoScale_38(),
	AFPSCounter_t1611953012::get_offset_of_scaleFactor_39(),
	AFPSCounter_t1611953012::get_offset_of_labelsFont_40(),
	AFPSCounter_t1611953012::get_offset_of_fontSize_41(),
	AFPSCounter_t1611953012::get_offset_of_lineSpacing_42(),
	AFPSCounter_t1611953012::get_offset_of_countersSpacing_43(),
	AFPSCounter_t1611953012::get_offset_of_paddingOffset_44(),
	AFPSCounter_t1611953012::get_offset_of_pixelPerfect_45(),
	AFPSCounter_t1611953012::get_offset_of_sortingOrder_46(),
	AFPSCounter_t1611953012_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (BaseCounterData_t2350381201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[14] = 
{
	0,
	0,
	0,
	0,
	BaseCounterData_t2350381201::get_offset_of_text_4(),
	BaseCounterData_t2350381201::get_offset_of_dirty_5(),
	BaseCounterData_t2350381201::get_offset_of_main_6(),
	BaseCounterData_t2350381201::get_offset_of_colorCached_7(),
	BaseCounterData_t2350381201::get_offset_of_inited_8(),
	BaseCounterData_t2350381201::get_offset_of_enabled_9(),
	BaseCounterData_t2350381201::get_offset_of_anchor_10(),
	BaseCounterData_t2350381201::get_offset_of_color_11(),
	BaseCounterData_t2350381201::get_offset_of_style_12(),
	BaseCounterData_t2350381201::get_offset_of_extraText_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (UpdatebleCounterData_t3744281392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[3] = 
{
	UpdatebleCounterData_t3744281392::get_offset_of_updateCoroutine_14(),
	UpdatebleCounterData_t3744281392::get_offset_of_cachedWaitForSecondsUnscaled_15(),
	UpdatebleCounterData_t3744281392::get_offset_of_updateInterval_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (DeviceInfoCounterData_t1356006596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[9] = 
{
	DeviceInfoCounterData_t1356006596::get_offset_of_platform_14(),
	DeviceInfoCounterData_t1356006596::get_offset_of_cpuModel_15(),
	DeviceInfoCounterData_t1356006596::get_offset_of_gpuModel_16(),
	DeviceInfoCounterData_t1356006596::get_offset_of_gpuApi_17(),
	DeviceInfoCounterData_t1356006596::get_offset_of_gpuSpec_18(),
	DeviceInfoCounterData_t1356006596::get_offset_of_ramSize_19(),
	DeviceInfoCounterData_t1356006596::get_offset_of_screenData_20(),
	DeviceInfoCounterData_t1356006596::get_offset_of_deviceModel_21(),
	DeviceInfoCounterData_t1356006596::get_offset_of_U3CLastValueU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (FPSCounterData_t432421409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[62] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FPSCounterData_t432421409::get_offset_of_warningLevelValue_26(),
	FPSCounterData_t432421409::get_offset_of_criticalLevelValue_27(),
	FPSCounterData_t432421409::get_offset_of_resetAverageOnNewScene_28(),
	FPSCounterData_t432421409::get_offset_of_resetMinMaxOnNewScene_29(),
	FPSCounterData_t432421409::get_offset_of_minMaxIntervalsToSkip_30(),
	FPSCounterData_t432421409::get_offset_of_OnFPSLevelChange_31(),
	FPSCounterData_t432421409::get_offset_of_newValue_32(),
	FPSCounterData_t432421409::get_offset_of_colorCachedMs_33(),
	FPSCounterData_t432421409::get_offset_of_colorCachedMin_34(),
	FPSCounterData_t432421409::get_offset_of_colorCachedMax_35(),
	FPSCounterData_t432421409::get_offset_of_colorCachedAvg_36(),
	FPSCounterData_t432421409::get_offset_of_colorCachedRender_37(),
	FPSCounterData_t432421409::get_offset_of_colorWarningCached_38(),
	FPSCounterData_t432421409::get_offset_of_colorWarningCachedMs_39(),
	FPSCounterData_t432421409::get_offset_of_colorWarningCachedMin_40(),
	FPSCounterData_t432421409::get_offset_of_colorWarningCachedMax_41(),
	FPSCounterData_t432421409::get_offset_of_colorWarningCachedAvg_42(),
	FPSCounterData_t432421409::get_offset_of_colorCriticalCached_43(),
	FPSCounterData_t432421409::get_offset_of_colorCriticalCachedMs_44(),
	FPSCounterData_t432421409::get_offset_of_colorCriticalCachedMin_45(),
	FPSCounterData_t432421409::get_offset_of_colorCriticalCachedMax_46(),
	FPSCounterData_t432421409::get_offset_of_colorCriticalCachedAvg_47(),
	FPSCounterData_t432421409::get_offset_of_currentAverageSamples_48(),
	FPSCounterData_t432421409::get_offset_of_currentAverageRaw_49(),
	FPSCounterData_t432421409::get_offset_of_accumulatedAverageSamples_50(),
	FPSCounterData_t432421409::get_offset_of_minMaxIntervalsSkipped_51(),
	FPSCounterData_t432421409::get_offset_of_renderTimeBank_52(),
	FPSCounterData_t432421409::get_offset_of_previousFrameCount_53(),
	FPSCounterData_t432421409::get_offset_of_milliseconds_54(),
	FPSCounterData_t432421409::get_offset_of_average_55(),
	FPSCounterData_t432421409::get_offset_of_averageMilliseconds_56(),
	FPSCounterData_t432421409::get_offset_of_averageNewLine_57(),
	FPSCounterData_t432421409::get_offset_of_averageSamples_58(),
	FPSCounterData_t432421409::get_offset_of_minMax_59(),
	FPSCounterData_t432421409::get_offset_of_minMaxMilliseconds_60(),
	FPSCounterData_t432421409::get_offset_of_minMaxNewLine_61(),
	FPSCounterData_t432421409::get_offset_of_minMaxTwoLines_62(),
	FPSCounterData_t432421409::get_offset_of_render_63(),
	FPSCounterData_t432421409::get_offset_of_renderNewLine_64(),
	FPSCounterData_t432421409::get_offset_of_renderAutoAdd_65(),
	FPSCounterData_t432421409::get_offset_of_colorWarning_66(),
	FPSCounterData_t432421409::get_offset_of_colorCritical_67(),
	FPSCounterData_t432421409::get_offset_of_colorRender_68(),
	FPSCounterData_t432421409::get_offset_of_U3CLastValueU3Ek__BackingField_69(),
	FPSCounterData_t432421409::get_offset_of_U3CLastMillisecondsValueU3Ek__BackingField_70(),
	FPSCounterData_t432421409::get_offset_of_U3CLastRenderValueU3Ek__BackingField_71(),
	FPSCounterData_t432421409::get_offset_of_U3CLastAverageValueU3Ek__BackingField_72(),
	FPSCounterData_t432421409::get_offset_of_U3CLastAverageMillisecondsValueU3Ek__BackingField_73(),
	FPSCounterData_t432421409::get_offset_of_U3CLastMinimumValueU3Ek__BackingField_74(),
	FPSCounterData_t432421409::get_offset_of_U3CLastMaximumValueU3Ek__BackingField_75(),
	FPSCounterData_t432421409::get_offset_of_U3CLastMinMillisecondsValueU3Ek__BackingField_76(),
	FPSCounterData_t432421409::get_offset_of_U3CLastMaxMillisecondsValueU3Ek__BackingField_77(),
	FPSCounterData_t432421409::get_offset_of_U3CCurrentFpsLevelU3Ek__BackingField_78(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (U3CUpdateCounterU3Ec__Iterator0_t2432920949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[8] = 
{
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U3CpreviousUpdateTimeU3E__1_0(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U3CpreviousUpdateFramesU3E__1_1(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U3CtimeElapsedU3E__1_2(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U3CframesChangedU3E__1_3(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U24this_4(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U24current_5(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U24disposing_6(),
	U3CUpdateCounterU3Ec__Iterator0_t2432920949::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (MemoryCounterData_t1329736715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MemoryCounterData_t1329736715::get_offset_of_precise_24(),
	MemoryCounterData_t1329736715::get_offset_of_total_25(),
	MemoryCounterData_t1329736715::get_offset_of_allocated_26(),
	MemoryCounterData_t1329736715::get_offset_of_monoUsage_27(),
	MemoryCounterData_t1329736715::get_offset_of_U3CLastTotalValueU3Ek__BackingField_28(),
	MemoryCounterData_t1329736715::get_offset_of_U3CLastAllocatedValueU3Ek__BackingField_29(),
	MemoryCounterData_t1329736715::get_offset_of_U3CLastMonoValueU3Ek__BackingField_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (U3CUpdateCounterU3Ec__Iterator0_t1493205377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[4] = 
{
	U3CUpdateCounterU3Ec__Iterator0_t1493205377::get_offset_of_U24this_0(),
	U3CUpdateCounterU3Ec__Iterator0_t1493205377::get_offset_of_U24current_1(),
	U3CUpdateCounterU3Ec__Iterator0_t1493205377::get_offset_of_U24disposing_2(),
	U3CUpdateCounterU3Ec__Iterator0_t1493205377::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (FPSLevel_t2998525377)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2351[4] = 
{
	FPSLevel_t2998525377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (OperationMode_t3651167660)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[4] = 
{
	OperationMode_t3651167660::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (DrawableLabel_t3479716230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[20] = 
{
	DrawableLabel_t3479716230::get_offset_of_container_0(),
	DrawableLabel_t3479716230::get_offset_of_anchor_1(),
	DrawableLabel_t3479716230::get_offset_of_newText_2(),
	DrawableLabel_t3479716230::get_offset_of_dirty_3(),
	DrawableLabel_t3479716230::get_offset_of_labelGameObject_4(),
	DrawableLabel_t3479716230::get_offset_of_labelTransform_5(),
	DrawableLabel_t3479716230::get_offset_of_labelFitter_6(),
	DrawableLabel_t3479716230::get_offset_of_labelGroup_7(),
	DrawableLabel_t3479716230::get_offset_of_uiTextGameObject_8(),
	DrawableLabel_t3479716230::get_offset_of_uiText_9(),
	DrawableLabel_t3479716230::get_offset_of_font_10(),
	DrawableLabel_t3479716230::get_offset_of_fontSize_11(),
	DrawableLabel_t3479716230::get_offset_of_lineSpacing_12(),
	DrawableLabel_t3479716230::get_offset_of_pixelOffset_13(),
	DrawableLabel_t3479716230::get_offset_of_background_14(),
	DrawableLabel_t3479716230::get_offset_of_backgroundImage_15(),
	DrawableLabel_t3479716230::get_offset_of_shadow_16(),
	DrawableLabel_t3479716230::get_offset_of_shadowComponent_17(),
	DrawableLabel_t3479716230::get_offset_of_outline_18(),
	DrawableLabel_t3479716230::get_offset_of_outlineComponent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (LabelAnchor_t2282579641)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[7] = 
{
	LabelAnchor_t2282579641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (LabelEffect_t1942779263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[4] = 
{
	LabelEffect_t1942779263::get_offset_of_enabled_0(),
	LabelEffect_t1942779263::get_offset_of_color_1(),
	LabelEffect_t1942779263::get_offset_of_distance_2(),
	LabelEffect_t1942779263::get_offset_of_padding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (AFPSRenderRecorder_t3904105026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[2] = 
{
	AFPSRenderRecorder_t3904105026::get_offset_of_recording_2(),
	AFPSRenderRecorder_t3904105026::get_offset_of_renderTime_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (UIUtils_t3010799121), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (WaitForSecondsUnscaled_t1880715370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[2] = 
{
	WaitForSecondsUnscaled_t1880715370::get_offset_of_waitTime_0(),
	WaitForSecondsUnscaled_t1880715370::get_offset_of_runUntil_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ACTkByte16_t2523981134)+ sizeof (Il2CppObject), sizeof(ACTkByte16_t2523981134 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2359[16] = 
{
	ACTkByte16_t2523981134::get_offset_of_b1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b3_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b4_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b5_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b6_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b7_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b8_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b9_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b10_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b11_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b12_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b13_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b14_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b15_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte16_t2523981134::get_offset_of_b16_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (ACTkByte4_t3474909705)+ sizeof (Il2CppObject), sizeof(ACTkByte4_t3474909705 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2360[4] = 
{
	ACTkByte4_t3474909705::get_offset_of_b1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte4_t3474909705::get_offset_of_b2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte4_t3474909705::get_offset_of_b3_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte4_t3474909705::get_offset_of_b4_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (ACTkByte8_t1505541237)+ sizeof (Il2CppObject), sizeof(ACTkByte8_t1505541237 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2361[8] = 
{
	ACTkByte8_t1505541237::get_offset_of_b1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b3_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b4_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b5_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b6_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b7_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ACTkByte8_t1505541237::get_offset_of_b8_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (Constants_t2686455593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (ActDetectorBase_t1905710121), -1, sizeof(ActDetectorBase_t1905710121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2363[12] = 
{
	0,
	0,
	0,
	ActDetectorBase_t1905710121_StaticFields::get_offset_of_detectorsContainer_5(),
	ActDetectorBase_t1905710121::get_offset_of_autoStart_6(),
	ActDetectorBase_t1905710121::get_offset_of_keepAlive_7(),
	ActDetectorBase_t1905710121::get_offset_of_autoDispose_8(),
	ActDetectorBase_t1905710121::get_offset_of_detectionEvent_9(),
	ActDetectorBase_t1905710121::get_offset_of_detectionAction_10(),
	ActDetectorBase_t1905710121::get_offset_of_detectionEventHasListener_11(),
	ActDetectorBase_t1905710121::get_offset_of_isRunning_12(),
	ActDetectorBase_t1905710121::get_offset_of_started_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (InjectionDetector_t1635666085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ObscuredCheatingDetector_t2708612136), -1, sizeof(ObscuredCheatingDetector_t2708612136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2365[8] = 
{
	0,
	0,
	ObscuredCheatingDetector_t2708612136_StaticFields::get_offset_of_instancesInScene_16(),
	ObscuredCheatingDetector_t2708612136::get_offset_of_floatEpsilon_17(),
	ObscuredCheatingDetector_t2708612136::get_offset_of_vector2Epsilon_18(),
	ObscuredCheatingDetector_t2708612136::get_offset_of_vector3Epsilon_19(),
	ObscuredCheatingDetector_t2708612136::get_offset_of_quaternionEpsilon_20(),
	ObscuredCheatingDetector_t2708612136_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (SpeedHackDetector_t1152147286), -1, sizeof(SpeedHackDetector_t1152147286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2366[15] = 
{
	0,
	0,
	0,
	0,
	SpeedHackDetector_t1152147286_StaticFields::get_offset_of_instancesInScene_18(),
	SpeedHackDetector_t1152147286::get_offset_of_interval_19(),
	SpeedHackDetector_t1152147286::get_offset_of_maxFalsePositives_20(),
	SpeedHackDetector_t1152147286::get_offset_of_coolDown_21(),
	SpeedHackDetector_t1152147286::get_offset_of_currentFalsePositives_22(),
	SpeedHackDetector_t1152147286::get_offset_of_currentCooldownShots_23(),
	SpeedHackDetector_t1152147286::get_offset_of_ticksOnStart_24(),
	SpeedHackDetector_t1152147286::get_offset_of_vulnerableTicksOnStart_25(),
	SpeedHackDetector_t1152147286::get_offset_of_prevTicks_26(),
	SpeedHackDetector_t1152147286::get_offset_of_prevIntervalTicks_27(),
	SpeedHackDetector_t1152147286_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (WallHackDetector_t1804770419), -1, sizeof(WallHackDetector_t1804770419_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2367[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	WallHackDetector_t1804770419::get_offset_of_rigidPlayerVelocity_20(),
	WallHackDetector_t1804770419_StaticFields::get_offset_of_instancesInScene_21(),
	WallHackDetector_t1804770419::get_offset_of_waitForEndOfFrame_22(),
	WallHackDetector_t1804770419::get_offset_of_checkRigidbody_23(),
	WallHackDetector_t1804770419::get_offset_of_checkController_24(),
	WallHackDetector_t1804770419::get_offset_of_checkWireframe_25(),
	WallHackDetector_t1804770419::get_offset_of_checkRaycast_26(),
	WallHackDetector_t1804770419::get_offset_of_wireframeDelay_27(),
	WallHackDetector_t1804770419::get_offset_of_raycastDelay_28(),
	WallHackDetector_t1804770419::get_offset_of_spawnPosition_29(),
	WallHackDetector_t1804770419::get_offset_of_maxFalsePositives_30(),
	WallHackDetector_t1804770419::get_offset_of_serviceContainer_31(),
	WallHackDetector_t1804770419::get_offset_of_solidWall_32(),
	WallHackDetector_t1804770419::get_offset_of_thinWall_33(),
	WallHackDetector_t1804770419::get_offset_of_wfCamera_34(),
	WallHackDetector_t1804770419::get_offset_of_foregroundRenderer_35(),
	WallHackDetector_t1804770419::get_offset_of_backgroundRenderer_36(),
	WallHackDetector_t1804770419::get_offset_of_wfColor1_37(),
	WallHackDetector_t1804770419::get_offset_of_wfColor2_38(),
	WallHackDetector_t1804770419::get_offset_of_wfShader_39(),
	WallHackDetector_t1804770419::get_offset_of_wfMaterial_40(),
	WallHackDetector_t1804770419::get_offset_of_shaderTexture_41(),
	WallHackDetector_t1804770419::get_offset_of_targetTexture_42(),
	WallHackDetector_t1804770419::get_offset_of_renderTexture_43(),
	WallHackDetector_t1804770419::get_offset_of_whLayer_44(),
	WallHackDetector_t1804770419::get_offset_of_raycastMask_45(),
	WallHackDetector_t1804770419::get_offset_of_rigidPlayer_46(),
	WallHackDetector_t1804770419::get_offset_of_charControllerPlayer_47(),
	WallHackDetector_t1804770419::get_offset_of_charControllerVelocity_48(),
	WallHackDetector_t1804770419::get_offset_of_rigidbodyDetections_49(),
	WallHackDetector_t1804770419::get_offset_of_controllerDetections_50(),
	WallHackDetector_t1804770419::get_offset_of_wireframeDetections_51(),
	WallHackDetector_t1804770419::get_offset_of_raycastDetections_52(),
	WallHackDetector_t1804770419::get_offset_of_wireframeDetected_53(),
	WallHackDetector_t1804770419_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (U3CInitDetectorU3Ec__Iterator0_t3307996606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[4] = 
{
	U3CInitDetectorU3Ec__Iterator0_t3307996606::get_offset_of_U24this_0(),
	U3CInitDetectorU3Ec__Iterator0_t3307996606::get_offset_of_U24current_1(),
	U3CInitDetectorU3Ec__Iterator0_t3307996606::get_offset_of_U24disposing_2(),
	U3CInitDetectorU3Ec__Iterator0_t3307996606::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (U3CCaptureFrameU3Ec__Iterator1_t2911810544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[6] = 
{
	U3CCaptureFrameU3Ec__Iterator1_t2911810544::get_offset_of_U3CpreviousActiveU3E__0_0(),
	U3CCaptureFrameU3Ec__Iterator1_t2911810544::get_offset_of_U3CdetectedU3E__0_1(),
	U3CCaptureFrameU3Ec__Iterator1_t2911810544::get_offset_of_U24this_2(),
	U3CCaptureFrameU3Ec__Iterator1_t2911810544::get_offset_of_U24current_3(),
	U3CCaptureFrameU3Ec__Iterator1_t2911810544::get_offset_of_U24disposing_4(),
	U3CCaptureFrameU3Ec__Iterator1_t2911810544::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ObscuredBool_t337339225)+ sizeof (Il2CppObject), sizeof(ObscuredBool_t337339225_marshaled_pinvoke), sizeof(ObscuredBool_t337339225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2370[6] = 
{
	ObscuredBool_t337339225_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredBool_t337339225::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredBool_t337339225::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredBool_t337339225::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredBool_t337339225::get_offset_of_fakeValueChanged_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredBool_t337339225::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ObscuredByte_t875138049)+ sizeof (Il2CppObject), sizeof(ObscuredByte_t875138049_marshaled_pinvoke), sizeof(ObscuredByte_t875138049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2371[5] = 
{
	ObscuredByte_t875138049_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredByte_t875138049::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredByte_t875138049::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredByte_t875138049::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredByte_t875138049::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ObscuredChar_t1936922347)+ sizeof (Il2CppObject), sizeof(ObscuredChar_t1936922347_marshaled_pinvoke), sizeof(ObscuredChar_t1936922347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2372[5] = 
{
	ObscuredChar_t1936922347_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredChar_t1936922347::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredChar_t1936922347::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredChar_t1936922347::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredChar_t1936922347::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (ObscuredDecimal_t3299336390)+ sizeof (Il2CppObject), sizeof(ObscuredDecimal_t3299336390_marshaled_pinvoke), sizeof(ObscuredDecimal_t3299336390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[6] = 
{
	ObscuredDecimal_t3299336390_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredDecimal_t3299336390::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDecimal_t3299336390::get_offset_of_hiddenValueOld_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDecimal_t3299336390::get_offset_of_hiddenValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDecimal_t3299336390::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDecimal_t3299336390::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (DecimalLongBytesUnion_t1508301530)+ sizeof (Il2CppObject), sizeof(DecimalLongBytesUnion_t1508301530 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2374[4] = 
{
	DecimalLongBytesUnion_t1508301530::get_offset_of_d_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DecimalLongBytesUnion_t1508301530::get_offset_of_l1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DecimalLongBytesUnion_t1508301530::get_offset_of_l2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DecimalLongBytesUnion_t1508301530::get_offset_of_b16_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (ObscuredDouble_t3265587138)+ sizeof (Il2CppObject), sizeof(ObscuredDouble_t3265587138_marshaled_pinvoke), sizeof(ObscuredDouble_t3265587138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2375[6] = 
{
	ObscuredDouble_t3265587138_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredDouble_t3265587138::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDouble_t3265587138::get_offset_of_hiddenValueOld_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDouble_t3265587138::get_offset_of_hiddenValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDouble_t3265587138::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredDouble_t3265587138::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (DoubleLongBytesUnion_t2034359298)+ sizeof (Il2CppObject), sizeof(DoubleLongBytesUnion_t2034359298 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2376[3] = 
{
	DoubleLongBytesUnion_t2034359298::get_offset_of_d_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DoubleLongBytesUnion_t2034359298::get_offset_of_l_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DoubleLongBytesUnion_t2034359298::get_offset_of_b8_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (ObscuredFloat_t3198542555)+ sizeof (Il2CppObject), sizeof(ObscuredFloat_t3198542555_marshaled_pinvoke), sizeof(ObscuredFloat_t3198542555_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2377[6] = 
{
	ObscuredFloat_t3198542555_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredFloat_t3198542555::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredFloat_t3198542555::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredFloat_t3198542555::get_offset_of_hiddenValueOld_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredFloat_t3198542555::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredFloat_t3198542555::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (FloatIntBytesUnion_t656831461)+ sizeof (Il2CppObject), sizeof(FloatIntBytesUnion_t656831461 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2378[3] = 
{
	FloatIntBytesUnion_t656831461::get_offset_of_f_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatIntBytesUnion_t656831461::get_offset_of_i_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatIntBytesUnion_t656831461::get_offset_of_b4_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (ObscuredInt_t796441056)+ sizeof (Il2CppObject), sizeof(ObscuredInt_t796441056_marshaled_pinvoke), sizeof(ObscuredInt_t796441056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2379[5] = 
{
	ObscuredInt_t796441056_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredInt_t796441056::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredInt_t796441056::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredInt_t796441056::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredInt_t796441056::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (ObscuredLong_t3872791569)+ sizeof (Il2CppObject), sizeof(ObscuredLong_t3872791569_marshaled_pinvoke), sizeof(ObscuredLong_t3872791569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2380[5] = 
{
	ObscuredLong_t3872791569_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredLong_t3872791569::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredLong_t3872791569::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredLong_t3872791569::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredLong_t3872791569::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (ObscuredPrefs_t2721367689), -1, sizeof(ObscuredPrefs_t2721367689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2381[15] = 
{
	0,
	0,
	0,
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_foreignSavesReported_3(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_cryptoKey_4(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_deviceId_5(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_deviceIdHash_6(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_onAlterationDetected_7(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_preservePlayerPrefs_8(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_onPossibleForeignSavesDetected_9(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_lockToDevice_10(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_readForeignSaves_11(),
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_emergencyMode_12(),
	0,
	ObscuredPrefs_t2721367689_StaticFields::get_offset_of_deprecatedDeviceId_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (DataType_t1454344260)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2382[15] = 
{
	DataType_t1454344260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (DeviceLockLevel_t3458667235)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2383[4] = 
{
	DeviceLockLevel_t3458667235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (ObscuredQuaternion_t2380454615)+ sizeof (Il2CppObject), sizeof(ObscuredQuaternion_t2380454615_marshaled_pinvoke), sizeof(ObscuredQuaternion_t2380454615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2384[6] = 
{
	ObscuredQuaternion_t2380454615_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredQuaternion_t2380454615_StaticFields::get_offset_of_initialFakeValue_1(),
	ObscuredQuaternion_t2380454615::get_offset_of_currentCryptoKey_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredQuaternion_t2380454615::get_offset_of_hiddenValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredQuaternion_t2380454615::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredQuaternion_t2380454615::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (RawEncryptedQuaternion_t4226477674)+ sizeof (Il2CppObject), sizeof(RawEncryptedQuaternion_t4226477674 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2385[4] = 
{
	RawEncryptedQuaternion_t4226477674::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RawEncryptedQuaternion_t4226477674::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RawEncryptedQuaternion_t4226477674::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RawEncryptedQuaternion_t4226477674::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (ObscuredSByte_t4105365306)+ sizeof (Il2CppObject), sizeof(ObscuredSByte_t4105365306_marshaled_pinvoke), sizeof(ObscuredSByte_t4105365306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2386[5] = 
{
	ObscuredSByte_t4105365306_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredSByte_t4105365306::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredSByte_t4105365306::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredSByte_t4105365306::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredSByte_t4105365306::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (ObscuredShort_t3487530033)+ sizeof (Il2CppObject), sizeof(ObscuredShort_t3487530033_marshaled_pinvoke), sizeof(ObscuredShort_t3487530033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2387[5] = 
{
	ObscuredShort_t3487530033_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredShort_t3487530033::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredShort_t3487530033::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredShort_t3487530033::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredShort_t3487530033::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (ObscuredString_t692732302), -1, sizeof(ObscuredString_t692732302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2388[5] = 
{
	ObscuredString_t692732302_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredString_t692732302::get_offset_of_currentCryptoKey_1(),
	ObscuredString_t692732302::get_offset_of_hiddenValue_2(),
	ObscuredString_t692732302::get_offset_of_fakeValue_3(),
	ObscuredString_t692732302::get_offset_of_inited_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (ObscuredUInt_t693053169)+ sizeof (Il2CppObject), sizeof(ObscuredUInt_t693053169_marshaled_pinvoke), sizeof(ObscuredUInt_t693053169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2389[5] = 
{
	ObscuredUInt_t693053169_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredUInt_t693053169::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredUInt_t693053169::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredUInt_t693053169::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredUInt_t693053169::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (ObscuredULong_t79425124)+ sizeof (Il2CppObject), sizeof(ObscuredULong_t79425124_marshaled_pinvoke), sizeof(ObscuredULong_t79425124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2390[5] = 
{
	ObscuredULong_t79425124_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredULong_t79425124::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredULong_t79425124::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredULong_t79425124::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredULong_t79425124::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (ObscuredUShort_t4178408852)+ sizeof (Il2CppObject), sizeof(ObscuredUShort_t4178408852_marshaled_pinvoke), sizeof(ObscuredUShort_t4178408852_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2391[5] = 
{
	ObscuredUShort_t4178408852_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredUShort_t4178408852::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredUShort_t4178408852::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredUShort_t4178408852::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredUShort_t4178408852::get_offset_of_inited_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (ObscuredVector2_t544383870)+ sizeof (Il2CppObject), sizeof(ObscuredVector2_t544383870_marshaled_pinvoke), sizeof(ObscuredVector2_t544383870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2392[6] = 
{
	ObscuredVector2_t544383870_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredVector2_t544383870_StaticFields::get_offset_of_initialFakeValue_1(),
	ObscuredVector2_t544383870::get_offset_of_currentCryptoKey_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredVector2_t544383870::get_offset_of_hiddenValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredVector2_t544383870::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredVector2_t544383870::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (RawEncryptedVector2_t4003483512)+ sizeof (Il2CppObject), sizeof(RawEncryptedVector2_t4003483512 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2393[2] = 
{
	RawEncryptedVector2_t4003483512::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RawEncryptedVector2_t4003483512::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (ObscuredVector3_t544383869)+ sizeof (Il2CppObject), sizeof(ObscuredVector3_t544383869_marshaled_pinvoke), sizeof(ObscuredVector3_t544383869_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2394[6] = 
{
	ObscuredVector3_t544383869_StaticFields::get_offset_of_cryptoKey_0(),
	ObscuredVector3_t544383869_StaticFields::get_offset_of_initialFakeValue_1(),
	ObscuredVector3_t544383869::get_offset_of_currentCryptoKey_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredVector3_t544383869::get_offset_of_hiddenValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredVector3_t544383869::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObscuredVector3_t544383869::get_offset_of_inited_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (RawEncryptedVector3_t1070189624)+ sizeof (Il2CppObject), sizeof(RawEncryptedVector3_t1070189624 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2395[3] = 
{
	RawEncryptedVector3_t1070189624::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RawEncryptedVector3_t1070189624::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RawEncryptedVector3_t1070189624::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (xxHash_t3880897212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (Basics_t2766259351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[2] = 
{
	Basics_t2766259351::get_offset_of_cubeA_2(),
	Basics_t2766259351::get_offset_of_cubeB_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (Sequences_t3565862770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[1] = 
{
	Sequences_t3565862770::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (NativeShare_t1150945090), -1, sizeof(NativeShare_t1150945090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2399[7] = 
{
	NativeShare_t1150945090_StaticFields::get_offset_of_share_2(),
	NativeShare_t1150945090_StaticFields::get_offset_of_OnDownloading_3(),
	NativeShare_t1150945090_StaticFields::get_offset_of_OnDownloadCompleted_4(),
	NativeShare_t1150945090_StaticFields::get_offset_of_OnShareCompleted_5(),
	NativeShare_t1150945090_StaticFields::get_offset_of_instnace_6(),
	NativeShare_t1150945090_StaticFields::get_offset_of_shareObject_7(),
	NativeShare_t1150945090_StaticFields::get_offset_of_listenner_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
