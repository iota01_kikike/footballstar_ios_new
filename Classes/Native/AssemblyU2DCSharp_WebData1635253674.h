﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebData
struct  WebData_t1635253674  : public Il2CppObject
{
public:
	// System.Int32 WebData::rs
	int32_t ___rs_0;
	// System.String WebData::msg
	String_t* ___msg_1;

public:
	inline static int32_t get_offset_of_rs_0() { return static_cast<int32_t>(offsetof(WebData_t1635253674, ___rs_0)); }
	inline int32_t get_rs_0() const { return ___rs_0; }
	inline int32_t* get_address_of_rs_0() { return &___rs_0; }
	inline void set_rs_0(int32_t value)
	{
		___rs_0 = value;
	}

	inline static int32_t get_offset_of_msg_1() { return static_cast<int32_t>(offsetof(WebData_t1635253674, ___msg_1)); }
	inline String_t* get_msg_1() const { return ___msg_1; }
	inline String_t** get_address_of_msg_1() { return &___msg_1; }
	inline void set_msg_1(String_t* value)
	{
		___msg_1 = value;
		Il2CppCodeGenWriteBarrier(&___msg_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
