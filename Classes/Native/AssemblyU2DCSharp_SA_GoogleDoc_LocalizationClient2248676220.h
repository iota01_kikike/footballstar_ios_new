﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LangCode810189361.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t1563811461;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.LocalizationClient
struct  LocalizationClient_t2248676220  : public Il2CppObject
{
public:
	// SA.GoogleDoc.LangCode SA.GoogleDoc.LocalizationClient::_langCode
	int32_t ____langCode_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>> SA.GoogleDoc.LocalizationClient::SheetDictionary
	Dictionary_2_t1563811461 * ___SheetDictionary_1;

public:
	inline static int32_t get_offset_of__langCode_0() { return static_cast<int32_t>(offsetof(LocalizationClient_t2248676220, ____langCode_0)); }
	inline int32_t get__langCode_0() const { return ____langCode_0; }
	inline int32_t* get_address_of__langCode_0() { return &____langCode_0; }
	inline void set__langCode_0(int32_t value)
	{
		____langCode_0 = value;
	}

	inline static int32_t get_offset_of_SheetDictionary_1() { return static_cast<int32_t>(offsetof(LocalizationClient_t2248676220, ___SheetDictionary_1)); }
	inline Dictionary_2_t1563811461 * get_SheetDictionary_1() const { return ___SheetDictionary_1; }
	inline Dictionary_2_t1563811461 ** get_address_of_SheetDictionary_1() { return &___SheetDictionary_1; }
	inline void set_SheetDictionary_1(Dictionary_2_t1563811461 * value)
	{
		___SheetDictionary_1 = value;
		Il2CppCodeGenWriteBarrier(&___SheetDictionary_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
