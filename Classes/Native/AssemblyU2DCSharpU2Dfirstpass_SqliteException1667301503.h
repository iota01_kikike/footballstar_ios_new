﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Exception1927440687.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SqliteException
struct  SqliteException_t1667301503  : public Exception_t1927440687
{
public:
	// System.Int32 SqliteException::errorCode
	int32_t ___errorCode_11;

public:
	inline static int32_t get_offset_of_errorCode_11() { return static_cast<int32_t>(offsetof(SqliteException_t1667301503, ___errorCode_11)); }
	inline int32_t get_errorCode_11() const { return ___errorCode_11; }
	inline int32_t* get_address_of_errorCode_11() { return &___errorCode_11; }
	inline void set_errorCode_11(int32_t value)
	{
		___errorCode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
