﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2282579641.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.ContentSizeFitter
struct ContentSizeFitter_t1325211874;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t2875670365;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Font
struct Font_t4239498691;
// LabelEffect
struct LabelEffect_t1942779263;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Shadow
struct Shadow_t4269599528;
// UnityEngine.UI.Outline
struct Outline_t1417504278;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.Labels.DrawableLabel
struct  DrawableLabel_t3479716230  : public Il2CppObject
{
public:
	// UnityEngine.GameObject CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::container
	GameObject_t1756533147 * ___container_0;
	// CodeStage.AdvancedFPSCounter.Labels.LabelAnchor CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::anchor
	uint8_t ___anchor_1;
	// System.Text.StringBuilder CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::newText
	StringBuilder_t1221177846 * ___newText_2;
	// System.Boolean CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::dirty
	bool ___dirty_3;
	// UnityEngine.GameObject CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::labelGameObject
	GameObject_t1756533147 * ___labelGameObject_4;
	// UnityEngine.RectTransform CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::labelTransform
	RectTransform_t3349966182 * ___labelTransform_5;
	// UnityEngine.UI.ContentSizeFitter CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::labelFitter
	ContentSizeFitter_t1325211874 * ___labelFitter_6;
	// UnityEngine.UI.HorizontalLayoutGroup CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::labelGroup
	HorizontalLayoutGroup_t2875670365 * ___labelGroup_7;
	// UnityEngine.GameObject CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::uiTextGameObject
	GameObject_t1756533147 * ___uiTextGameObject_8;
	// UnityEngine.UI.Text CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::uiText
	Text_t356221433 * ___uiText_9;
	// UnityEngine.Font CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::font
	Font_t4239498691 * ___font_10;
	// System.Int32 CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::fontSize
	int32_t ___fontSize_11;
	// System.Single CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::lineSpacing
	float ___lineSpacing_12;
	// UnityEngine.Vector2 CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::pixelOffset
	Vector2_t2243707579  ___pixelOffset_13;
	// LabelEffect CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::background
	LabelEffect_t1942779263 * ___background_14;
	// UnityEngine.UI.Image CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::backgroundImage
	Image_t2042527209 * ___backgroundImage_15;
	// LabelEffect CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::shadow
	LabelEffect_t1942779263 * ___shadow_16;
	// UnityEngine.UI.Shadow CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::shadowComponent
	Shadow_t4269599528 * ___shadowComponent_17;
	// LabelEffect CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::outline
	LabelEffect_t1942779263 * ___outline_18;
	// UnityEngine.UI.Outline CodeStage.AdvancedFPSCounter.Labels.DrawableLabel::outlineComponent
	Outline_t1417504278 * ___outlineComponent_19;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___container_0)); }
	inline GameObject_t1756533147 * get_container_0() const { return ___container_0; }
	inline GameObject_t1756533147 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(GameObject_t1756533147 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier(&___container_0, value);
	}

	inline static int32_t get_offset_of_anchor_1() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___anchor_1)); }
	inline uint8_t get_anchor_1() const { return ___anchor_1; }
	inline uint8_t* get_address_of_anchor_1() { return &___anchor_1; }
	inline void set_anchor_1(uint8_t value)
	{
		___anchor_1 = value;
	}

	inline static int32_t get_offset_of_newText_2() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___newText_2)); }
	inline StringBuilder_t1221177846 * get_newText_2() const { return ___newText_2; }
	inline StringBuilder_t1221177846 ** get_address_of_newText_2() { return &___newText_2; }
	inline void set_newText_2(StringBuilder_t1221177846 * value)
	{
		___newText_2 = value;
		Il2CppCodeGenWriteBarrier(&___newText_2, value);
	}

	inline static int32_t get_offset_of_dirty_3() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___dirty_3)); }
	inline bool get_dirty_3() const { return ___dirty_3; }
	inline bool* get_address_of_dirty_3() { return &___dirty_3; }
	inline void set_dirty_3(bool value)
	{
		___dirty_3 = value;
	}

	inline static int32_t get_offset_of_labelGameObject_4() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___labelGameObject_4)); }
	inline GameObject_t1756533147 * get_labelGameObject_4() const { return ___labelGameObject_4; }
	inline GameObject_t1756533147 ** get_address_of_labelGameObject_4() { return &___labelGameObject_4; }
	inline void set_labelGameObject_4(GameObject_t1756533147 * value)
	{
		___labelGameObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___labelGameObject_4, value);
	}

	inline static int32_t get_offset_of_labelTransform_5() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___labelTransform_5)); }
	inline RectTransform_t3349966182 * get_labelTransform_5() const { return ___labelTransform_5; }
	inline RectTransform_t3349966182 ** get_address_of_labelTransform_5() { return &___labelTransform_5; }
	inline void set_labelTransform_5(RectTransform_t3349966182 * value)
	{
		___labelTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___labelTransform_5, value);
	}

	inline static int32_t get_offset_of_labelFitter_6() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___labelFitter_6)); }
	inline ContentSizeFitter_t1325211874 * get_labelFitter_6() const { return ___labelFitter_6; }
	inline ContentSizeFitter_t1325211874 ** get_address_of_labelFitter_6() { return &___labelFitter_6; }
	inline void set_labelFitter_6(ContentSizeFitter_t1325211874 * value)
	{
		___labelFitter_6 = value;
		Il2CppCodeGenWriteBarrier(&___labelFitter_6, value);
	}

	inline static int32_t get_offset_of_labelGroup_7() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___labelGroup_7)); }
	inline HorizontalLayoutGroup_t2875670365 * get_labelGroup_7() const { return ___labelGroup_7; }
	inline HorizontalLayoutGroup_t2875670365 ** get_address_of_labelGroup_7() { return &___labelGroup_7; }
	inline void set_labelGroup_7(HorizontalLayoutGroup_t2875670365 * value)
	{
		___labelGroup_7 = value;
		Il2CppCodeGenWriteBarrier(&___labelGroup_7, value);
	}

	inline static int32_t get_offset_of_uiTextGameObject_8() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___uiTextGameObject_8)); }
	inline GameObject_t1756533147 * get_uiTextGameObject_8() const { return ___uiTextGameObject_8; }
	inline GameObject_t1756533147 ** get_address_of_uiTextGameObject_8() { return &___uiTextGameObject_8; }
	inline void set_uiTextGameObject_8(GameObject_t1756533147 * value)
	{
		___uiTextGameObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___uiTextGameObject_8, value);
	}

	inline static int32_t get_offset_of_uiText_9() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___uiText_9)); }
	inline Text_t356221433 * get_uiText_9() const { return ___uiText_9; }
	inline Text_t356221433 ** get_address_of_uiText_9() { return &___uiText_9; }
	inline void set_uiText_9(Text_t356221433 * value)
	{
		___uiText_9 = value;
		Il2CppCodeGenWriteBarrier(&___uiText_9, value);
	}

	inline static int32_t get_offset_of_font_10() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___font_10)); }
	inline Font_t4239498691 * get_font_10() const { return ___font_10; }
	inline Font_t4239498691 ** get_address_of_font_10() { return &___font_10; }
	inline void set_font_10(Font_t4239498691 * value)
	{
		___font_10 = value;
		Il2CppCodeGenWriteBarrier(&___font_10, value);
	}

	inline static int32_t get_offset_of_fontSize_11() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___fontSize_11)); }
	inline int32_t get_fontSize_11() const { return ___fontSize_11; }
	inline int32_t* get_address_of_fontSize_11() { return &___fontSize_11; }
	inline void set_fontSize_11(int32_t value)
	{
		___fontSize_11 = value;
	}

	inline static int32_t get_offset_of_lineSpacing_12() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___lineSpacing_12)); }
	inline float get_lineSpacing_12() const { return ___lineSpacing_12; }
	inline float* get_address_of_lineSpacing_12() { return &___lineSpacing_12; }
	inline void set_lineSpacing_12(float value)
	{
		___lineSpacing_12 = value;
	}

	inline static int32_t get_offset_of_pixelOffset_13() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___pixelOffset_13)); }
	inline Vector2_t2243707579  get_pixelOffset_13() const { return ___pixelOffset_13; }
	inline Vector2_t2243707579 * get_address_of_pixelOffset_13() { return &___pixelOffset_13; }
	inline void set_pixelOffset_13(Vector2_t2243707579  value)
	{
		___pixelOffset_13 = value;
	}

	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___background_14)); }
	inline LabelEffect_t1942779263 * get_background_14() const { return ___background_14; }
	inline LabelEffect_t1942779263 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(LabelEffect_t1942779263 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundImage_15() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___backgroundImage_15)); }
	inline Image_t2042527209 * get_backgroundImage_15() const { return ___backgroundImage_15; }
	inline Image_t2042527209 ** get_address_of_backgroundImage_15() { return &___backgroundImage_15; }
	inline void set_backgroundImage_15(Image_t2042527209 * value)
	{
		___backgroundImage_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundImage_15, value);
	}

	inline static int32_t get_offset_of_shadow_16() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___shadow_16)); }
	inline LabelEffect_t1942779263 * get_shadow_16() const { return ___shadow_16; }
	inline LabelEffect_t1942779263 ** get_address_of_shadow_16() { return &___shadow_16; }
	inline void set_shadow_16(LabelEffect_t1942779263 * value)
	{
		___shadow_16 = value;
		Il2CppCodeGenWriteBarrier(&___shadow_16, value);
	}

	inline static int32_t get_offset_of_shadowComponent_17() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___shadowComponent_17)); }
	inline Shadow_t4269599528 * get_shadowComponent_17() const { return ___shadowComponent_17; }
	inline Shadow_t4269599528 ** get_address_of_shadowComponent_17() { return &___shadowComponent_17; }
	inline void set_shadowComponent_17(Shadow_t4269599528 * value)
	{
		___shadowComponent_17 = value;
		Il2CppCodeGenWriteBarrier(&___shadowComponent_17, value);
	}

	inline static int32_t get_offset_of_outline_18() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___outline_18)); }
	inline LabelEffect_t1942779263 * get_outline_18() const { return ___outline_18; }
	inline LabelEffect_t1942779263 ** get_address_of_outline_18() { return &___outline_18; }
	inline void set_outline_18(LabelEffect_t1942779263 * value)
	{
		___outline_18 = value;
		Il2CppCodeGenWriteBarrier(&___outline_18, value);
	}

	inline static int32_t get_offset_of_outlineComponent_19() { return static_cast<int32_t>(offsetof(DrawableLabel_t3479716230, ___outlineComponent_19)); }
	inline Outline_t1417504278 * get_outlineComponent_19() const { return ___outlineComponent_19; }
	inline Outline_t1417504278 ** get_address_of_outlineComponent_19() { return &___outlineComponent_19; }
	inline void set_outlineComponent_19(Outline_t1417504278 * value)
	{
		___outlineComponent_19 = value;
		Il2CppCodeGenWriteBarrier(&___outlineComponent_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
