﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPManager
struct  IAPManager_t2850027101  : public MonoBehaviour_t1158329972
{
public:
	// System.String IAPManager::currentProductId
	String_t* ___currentProductId_7;

public:
	inline static int32_t get_offset_of_currentProductId_7() { return static_cast<int32_t>(offsetof(IAPManager_t2850027101, ___currentProductId_7)); }
	inline String_t* get_currentProductId_7() const { return ___currentProductId_7; }
	inline String_t** get_address_of_currentProductId_7() { return &___currentProductId_7; }
	inline void set_currentProductId_7(String_t* value)
	{
		___currentProductId_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentProductId_7, value);
	}
};

struct IAPManager_t2850027101_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController IAPManager::storeController
	Il2CppObject * ___storeController_2;
	// UnityEngine.Purchasing.IExtensionProvider IAPManager::extensionProvider
	Il2CppObject * ___extensionProvider_3;
	// System.Action`1<System.Boolean> IAPManager::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_storeController_2() { return static_cast<int32_t>(offsetof(IAPManager_t2850027101_StaticFields, ___storeController_2)); }
	inline Il2CppObject * get_storeController_2() const { return ___storeController_2; }
	inline Il2CppObject ** get_address_of_storeController_2() { return &___storeController_2; }
	inline void set_storeController_2(Il2CppObject * value)
	{
		___storeController_2 = value;
		Il2CppCodeGenWriteBarrier(&___storeController_2, value);
	}

	inline static int32_t get_offset_of_extensionProvider_3() { return static_cast<int32_t>(offsetof(IAPManager_t2850027101_StaticFields, ___extensionProvider_3)); }
	inline Il2CppObject * get_extensionProvider_3() const { return ___extensionProvider_3; }
	inline Il2CppObject ** get_address_of_extensionProvider_3() { return &___extensionProvider_3; }
	inline void set_extensionProvider_3(Il2CppObject * value)
	{
		___extensionProvider_3 = value;
		Il2CppCodeGenWriteBarrier(&___extensionProvider_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(IAPManager_t2850027101_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
