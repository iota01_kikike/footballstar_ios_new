﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Action`1<System.Int32>
struct Action_1_t1873676830;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleChancePop
struct  DoubleChancePop_t1912802536  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button DoubleChancePop::CloseButton
	Button_t2872111280 * ___CloseButton_2;
	// UnityEngine.UI.Button DoubleChancePop::YesButton
	Button_t2872111280 * ___YesButton_3;
	// System.Action`1<System.Int32> DoubleChancePop::Callback
	Action_1_t1873676830 * ___Callback_4;

public:
	inline static int32_t get_offset_of_CloseButton_2() { return static_cast<int32_t>(offsetof(DoubleChancePop_t1912802536, ___CloseButton_2)); }
	inline Button_t2872111280 * get_CloseButton_2() const { return ___CloseButton_2; }
	inline Button_t2872111280 ** get_address_of_CloseButton_2() { return &___CloseButton_2; }
	inline void set_CloseButton_2(Button_t2872111280 * value)
	{
		___CloseButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_2, value);
	}

	inline static int32_t get_offset_of_YesButton_3() { return static_cast<int32_t>(offsetof(DoubleChancePop_t1912802536, ___YesButton_3)); }
	inline Button_t2872111280 * get_YesButton_3() const { return ___YesButton_3; }
	inline Button_t2872111280 ** get_address_of_YesButton_3() { return &___YesButton_3; }
	inline void set_YesButton_3(Button_t2872111280 * value)
	{
		___YesButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___YesButton_3, value);
	}

	inline static int32_t get_offset_of_Callback_4() { return static_cast<int32_t>(offsetof(DoubleChancePop_t1912802536, ___Callback_4)); }
	inline Action_1_t1873676830 * get_Callback_4() const { return ___Callback_4; }
	inline Action_1_t1873676830 ** get_address_of_Callback_4() { return &___Callback_4; }
	inline void set_Callback_4(Action_1_t1873676830 * value)
	{
		___Callback_4 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
