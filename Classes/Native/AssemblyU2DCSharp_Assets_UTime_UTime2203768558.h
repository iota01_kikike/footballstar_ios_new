﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// Assets.UTime.UTime
struct UTime_t2203768558;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.UTime.UTime
struct  UTime_t2203768558  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct UTime_t2203768558_StaticFields
{
public:
	// Assets.UTime.UTime Assets.UTime.UTime::_instance
	UTime_t2203768558 * ____instance_3;

public:
	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(UTime_t2203768558_StaticFields, ____instance_3)); }
	inline UTime_t2203768558 * get__instance_3() const { return ____instance_3; }
	inline UTime_t2203768558 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(UTime_t2203768558 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
