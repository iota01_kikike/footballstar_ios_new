﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Purchasing.CodelessIAPStoreListener
struct CodelessIAPStoreListener_t1116138301;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton>
struct List_1_t2446958492;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPListener>
struct List_1_t3158673840;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t2667590766;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CodelessIAPStoreListener
struct  CodelessIAPStoreListener_t1116138301  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton> UnityEngine.Purchasing.CodelessIAPStoreListener::activeButtons
	List_1_t2446958492 * ___activeButtons_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPListener> UnityEngine.Purchasing.CodelessIAPStoreListener::activeListeners
	List_1_t3158673840 * ___activeListeners_2;
	// UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.CodelessIAPStoreListener::controller
	Il2CppObject * ___controller_4;
	// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.CodelessIAPStoreListener::extensions
	Il2CppObject * ___extensions_5;
	// UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.CodelessIAPStoreListener::catalog
	ProductCatalog_t2667590766 * ___catalog_6;

public:
	inline static int32_t get_offset_of_activeButtons_1() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301, ___activeButtons_1)); }
	inline List_1_t2446958492 * get_activeButtons_1() const { return ___activeButtons_1; }
	inline List_1_t2446958492 ** get_address_of_activeButtons_1() { return &___activeButtons_1; }
	inline void set_activeButtons_1(List_1_t2446958492 * value)
	{
		___activeButtons_1 = value;
		Il2CppCodeGenWriteBarrier(&___activeButtons_1, value);
	}

	inline static int32_t get_offset_of_activeListeners_2() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301, ___activeListeners_2)); }
	inline List_1_t3158673840 * get_activeListeners_2() const { return ___activeListeners_2; }
	inline List_1_t3158673840 ** get_address_of_activeListeners_2() { return &___activeListeners_2; }
	inline void set_activeListeners_2(List_1_t3158673840 * value)
	{
		___activeListeners_2 = value;
		Il2CppCodeGenWriteBarrier(&___activeListeners_2, value);
	}

	inline static int32_t get_offset_of_controller_4() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301, ___controller_4)); }
	inline Il2CppObject * get_controller_4() const { return ___controller_4; }
	inline Il2CppObject ** get_address_of_controller_4() { return &___controller_4; }
	inline void set_controller_4(Il2CppObject * value)
	{
		___controller_4 = value;
		Il2CppCodeGenWriteBarrier(&___controller_4, value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301, ___extensions_5)); }
	inline Il2CppObject * get_extensions_5() const { return ___extensions_5; }
	inline Il2CppObject ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(Il2CppObject * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier(&___extensions_5, value);
	}

	inline static int32_t get_offset_of_catalog_6() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301, ___catalog_6)); }
	inline ProductCatalog_t2667590766 * get_catalog_6() const { return ___catalog_6; }
	inline ProductCatalog_t2667590766 ** get_address_of_catalog_6() { return &___catalog_6; }
	inline void set_catalog_6(ProductCatalog_t2667590766 * value)
	{
		___catalog_6 = value;
		Il2CppCodeGenWriteBarrier(&___catalog_6, value);
	}
};

struct CodelessIAPStoreListener_t1116138301_StaticFields
{
public:
	// UnityEngine.Purchasing.CodelessIAPStoreListener UnityEngine.Purchasing.CodelessIAPStoreListener::instance
	CodelessIAPStoreListener_t1116138301 * ___instance_0;
	// System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::unityPurchasingInitialized
	bool ___unityPurchasingInitialized_3;
	// System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::initializationComplete
	bool ___initializationComplete_7;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301_StaticFields, ___instance_0)); }
	inline CodelessIAPStoreListener_t1116138301 * get_instance_0() const { return ___instance_0; }
	inline CodelessIAPStoreListener_t1116138301 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(CodelessIAPStoreListener_t1116138301 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}

	inline static int32_t get_offset_of_unityPurchasingInitialized_3() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301_StaticFields, ___unityPurchasingInitialized_3)); }
	inline bool get_unityPurchasingInitialized_3() const { return ___unityPurchasingInitialized_3; }
	inline bool* get_address_of_unityPurchasingInitialized_3() { return &___unityPurchasingInitialized_3; }
	inline void set_unityPurchasingInitialized_3(bool value)
	{
		___unityPurchasingInitialized_3 = value;
	}

	inline static int32_t get_offset_of_initializationComplete_7() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_t1116138301_StaticFields, ___initializationComplete_7)); }
	inline bool get_initializationComplete_7() const { return ___initializationComplete_7; }
	inline bool* get_address_of_initializationComplete_7() { return &___initializationComplete_7; }
	inline void set_initializationComplete_7(bool value)
	{
		___initializationComplete_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
