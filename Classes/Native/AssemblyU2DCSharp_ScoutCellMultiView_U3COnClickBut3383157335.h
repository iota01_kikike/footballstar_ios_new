﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Player
struct Player_t1147783557;
// ScoutCellMultiView/<OnClickButton>c__AnonStorey3
struct U3COnClickButtonU3Ec__AnonStorey3_t654273980;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellMultiView/<OnClickButton>c__AnonStorey2
struct  U3COnClickButtonU3Ec__AnonStorey2_t3383157335  : public Il2CppObject
{
public:
	// System.Int32 ScoutCellMultiView/<OnClickButton>c__AnonStorey2::cost
	int32_t ___cost_0;
	// Player ScoutCellMultiView/<OnClickButton>c__AnonStorey2::player
	Player_t1147783557 * ___player_1;
	// ScoutCellMultiView/<OnClickButton>c__AnonStorey3 ScoutCellMultiView/<OnClickButton>c__AnonStorey2::<>f__ref$3
	U3COnClickButtonU3Ec__AnonStorey3_t654273980 * ___U3CU3Ef__refU243_2;

public:
	inline static int32_t get_offset_of_cost_0() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey2_t3383157335, ___cost_0)); }
	inline int32_t get_cost_0() const { return ___cost_0; }
	inline int32_t* get_address_of_cost_0() { return &___cost_0; }
	inline void set_cost_0(int32_t value)
	{
		___cost_0 = value;
	}

	inline static int32_t get_offset_of_player_1() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey2_t3383157335, ___player_1)); }
	inline Player_t1147783557 * get_player_1() const { return ___player_1; }
	inline Player_t1147783557 ** get_address_of_player_1() { return &___player_1; }
	inline void set_player_1(Player_t1147783557 * value)
	{
		___player_1 = value;
		Il2CppCodeGenWriteBarrier(&___player_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_2() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey2_t3383157335, ___U3CU3Ef__refU243_2)); }
	inline U3COnClickButtonU3Ec__AnonStorey3_t654273980 * get_U3CU3Ef__refU243_2() const { return ___U3CU3Ef__refU243_2; }
	inline U3COnClickButtonU3Ec__AnonStorey3_t654273980 ** get_address_of_U3CU3Ef__refU243_2() { return &___U3CU3Ef__refU243_2; }
	inline void set_U3CU3Ef__refU243_2(U3COnClickButtonU3Ec__AnonStorey3_t654273980 * value)
	{
		___U3CU3Ef__refU243_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU243_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
