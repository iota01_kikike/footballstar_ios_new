﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnitUIView
struct  UnitUIView_t3306525905  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnitUIView::TargetTransform
	Transform_t3275118058 * ___TargetTransform_2;
	// UnityEngine.UI.Text UnitUIView::ValueText
	Text_t356221433 * ___ValueText_3;
	// UnityEngine.UI.Image UnitUIView::CoinImage
	Image_t2042527209 * ___CoinImage_4;
	// UnityEngine.UI.Image UnitUIView::StarImage
	Image_t2042527209 * ___StarImage_5;
	// UnityEngine.UI.Button UnitUIView::Button
	Button_t2872111280 * ___Button_6;
	// System.Single UnitUIView::originScaleX
	float ___originScaleX_7;
	// System.Single UnitUIView::originY
	float ___originY_8;
	// UnityEngine.Color[] UnitUIView::gradeColor
	ColorU5BU5D_t672350442* ___gradeColor_9;

public:
	inline static int32_t get_offset_of_TargetTransform_2() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___TargetTransform_2)); }
	inline Transform_t3275118058 * get_TargetTransform_2() const { return ___TargetTransform_2; }
	inline Transform_t3275118058 ** get_address_of_TargetTransform_2() { return &___TargetTransform_2; }
	inline void set_TargetTransform_2(Transform_t3275118058 * value)
	{
		___TargetTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___TargetTransform_2, value);
	}

	inline static int32_t get_offset_of_ValueText_3() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___ValueText_3)); }
	inline Text_t356221433 * get_ValueText_3() const { return ___ValueText_3; }
	inline Text_t356221433 ** get_address_of_ValueText_3() { return &___ValueText_3; }
	inline void set_ValueText_3(Text_t356221433 * value)
	{
		___ValueText_3 = value;
		Il2CppCodeGenWriteBarrier(&___ValueText_3, value);
	}

	inline static int32_t get_offset_of_CoinImage_4() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___CoinImage_4)); }
	inline Image_t2042527209 * get_CoinImage_4() const { return ___CoinImage_4; }
	inline Image_t2042527209 ** get_address_of_CoinImage_4() { return &___CoinImage_4; }
	inline void set_CoinImage_4(Image_t2042527209 * value)
	{
		___CoinImage_4 = value;
		Il2CppCodeGenWriteBarrier(&___CoinImage_4, value);
	}

	inline static int32_t get_offset_of_StarImage_5() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___StarImage_5)); }
	inline Image_t2042527209 * get_StarImage_5() const { return ___StarImage_5; }
	inline Image_t2042527209 ** get_address_of_StarImage_5() { return &___StarImage_5; }
	inline void set_StarImage_5(Image_t2042527209 * value)
	{
		___StarImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___StarImage_5, value);
	}

	inline static int32_t get_offset_of_Button_6() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___Button_6)); }
	inline Button_t2872111280 * get_Button_6() const { return ___Button_6; }
	inline Button_t2872111280 ** get_address_of_Button_6() { return &___Button_6; }
	inline void set_Button_6(Button_t2872111280 * value)
	{
		___Button_6 = value;
		Il2CppCodeGenWriteBarrier(&___Button_6, value);
	}

	inline static int32_t get_offset_of_originScaleX_7() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___originScaleX_7)); }
	inline float get_originScaleX_7() const { return ___originScaleX_7; }
	inline float* get_address_of_originScaleX_7() { return &___originScaleX_7; }
	inline void set_originScaleX_7(float value)
	{
		___originScaleX_7 = value;
	}

	inline static int32_t get_offset_of_originY_8() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___originY_8)); }
	inline float get_originY_8() const { return ___originY_8; }
	inline float* get_address_of_originY_8() { return &___originY_8; }
	inline void set_originY_8(float value)
	{
		___originY_8 = value;
	}

	inline static int32_t get_offset_of_gradeColor_9() { return static_cast<int32_t>(offsetof(UnitUIView_t3306525905, ___gradeColor_9)); }
	inline ColorU5BU5D_t672350442* get_gradeColor_9() const { return ___gradeColor_9; }
	inline ColorU5BU5D_t672350442** get_address_of_gradeColor_9() { return &___gradeColor_9; }
	inline void set_gradeColor_9(ColorU5BU5D_t672350442* value)
	{
		___gradeColor_9 = value;
		Il2CppCodeGenWriteBarrier(&___gradeColor_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
