﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t2951825130;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.SpreadsheetData
struct  SpreadsheetData_t567722816  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>> SA.GoogleDoc.SpreadsheetData::_worksheets
	Dictionary_2_t2951825130 * ____worksheets_0;

public:
	inline static int32_t get_offset_of__worksheets_0() { return static_cast<int32_t>(offsetof(SpreadsheetData_t567722816, ____worksheets_0)); }
	inline Dictionary_2_t2951825130 * get__worksheets_0() const { return ____worksheets_0; }
	inline Dictionary_2_t2951825130 ** get_address_of__worksheets_0() { return &____worksheets_0; }
	inline void set__worksheets_0(Dictionary_2_t2951825130 * value)
	{
		____worksheets_0 = value;
		Il2CppCodeGenWriteBarrier(&____worksheets_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
