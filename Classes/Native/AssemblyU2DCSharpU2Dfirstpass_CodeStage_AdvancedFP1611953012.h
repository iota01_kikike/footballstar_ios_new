﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP3651167660.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.String
struct String_t;
// CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData
struct FPSCounterData_t432421409;
// CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData
struct MemoryCounterData_t1329736715;
// CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData
struct DeviceInfoCounterData_t1356006596;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2574720772;
// CodeStage.AdvancedFPSCounter.Labels.DrawableLabel[]
struct DrawableLabelU5BU5D_t209901987;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.Font
struct Font_t4239498691;
// CodeStage.AdvancedFPSCounter.AFPSCounter
struct AFPSCounter_t1611953012;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.AFPSCounter
struct  AFPSCounter_t1611953012  : public MonoBehaviour_t1158329972
{
public:
	// CodeStage.AdvancedFPSCounter.CountersData.FPSCounterData CodeStage.AdvancedFPSCounter.AFPSCounter::fpsCounter
	FPSCounterData_t432421409 * ___fpsCounter_7;
	// CodeStage.AdvancedFPSCounter.CountersData.MemoryCounterData CodeStage.AdvancedFPSCounter.AFPSCounter::memoryCounter
	MemoryCounterData_t1329736715 * ___memoryCounter_8;
	// CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData CodeStage.AdvancedFPSCounter.AFPSCounter::deviceInfoCounter
	DeviceInfoCounterData_t1356006596 * ___deviceInfoCounter_9;
	// UnityEngine.KeyCode CodeStage.AdvancedFPSCounter.AFPSCounter::hotKey
	int32_t ___hotKey_10;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::circleGesture
	bool ___circleGesture_11;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::hotKeyCtrl
	bool ___hotKeyCtrl_12;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::hotKeyShift
	bool ___hotKeyShift_13;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::hotKeyAlt
	bool ___hotKeyAlt_14;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::keepAlive
	bool ___keepAlive_15;
	// UnityEngine.Canvas CodeStage.AdvancedFPSCounter.AFPSCounter::canvas
	Canvas_t209405766 * ___canvas_16;
	// UnityEngine.UI.CanvasScaler CodeStage.AdvancedFPSCounter.AFPSCounter::canvasScaler
	CanvasScaler_t2574720772 * ___canvasScaler_17;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::externalCanvas
	bool ___externalCanvas_18;
	// CodeStage.AdvancedFPSCounter.Labels.DrawableLabel[] CodeStage.AdvancedFPSCounter.AFPSCounter::labels
	DrawableLabelU5BU5D_t209901987* ___labels_19;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::anchorsCount
	int32_t ___anchorsCount_20;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::cachedVSync
	int32_t ___cachedVSync_21;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::cachedFrameRate
	int32_t ___cachedFrameRate_22;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::inited
	bool ___inited_23;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> CodeStage.AdvancedFPSCounter.AFPSCounter::gesturePoints
	List_1_t1612828711 * ___gesturePoints_24;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::gestureCount
	int32_t ___gestureCount_25;
	// CodeStage.AdvancedFPSCounter.OperationMode CodeStage.AdvancedFPSCounter.AFPSCounter::operationMode
	uint8_t ___operationMode_26;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::forceFrameRate
	bool ___forceFrameRate_27;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::forcedFrameRate
	int32_t ___forcedFrameRate_28;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::background
	bool ___background_29;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.AFPSCounter::backgroundColor
	Color_t2020392075  ___backgroundColor_30;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::backgroundPadding
	int32_t ___backgroundPadding_31;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::shadow
	bool ___shadow_32;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.AFPSCounter::shadowColor
	Color_t2020392075  ___shadowColor_33;
	// UnityEngine.Vector2 CodeStage.AdvancedFPSCounter.AFPSCounter::shadowDistance
	Vector2_t2243707579  ___shadowDistance_34;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::outline
	bool ___outline_35;
	// UnityEngine.Color CodeStage.AdvancedFPSCounter.AFPSCounter::outlineColor
	Color_t2020392075  ___outlineColor_36;
	// UnityEngine.Vector2 CodeStage.AdvancedFPSCounter.AFPSCounter::outlineDistance
	Vector2_t2243707579  ___outlineDistance_37;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::autoScale
	bool ___autoScale_38;
	// System.Single CodeStage.AdvancedFPSCounter.AFPSCounter::scaleFactor
	float ___scaleFactor_39;
	// UnityEngine.Font CodeStage.AdvancedFPSCounter.AFPSCounter::labelsFont
	Font_t4239498691 * ___labelsFont_40;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::fontSize
	int32_t ___fontSize_41;
	// System.Single CodeStage.AdvancedFPSCounter.AFPSCounter::lineSpacing
	float ___lineSpacing_42;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::countersSpacing
	int32_t ___countersSpacing_43;
	// UnityEngine.Vector2 CodeStage.AdvancedFPSCounter.AFPSCounter::paddingOffset
	Vector2_t2243707579  ___paddingOffset_44;
	// System.Boolean CodeStage.AdvancedFPSCounter.AFPSCounter::pixelPerfect
	bool ___pixelPerfect_45;
	// System.Int32 CodeStage.AdvancedFPSCounter.AFPSCounter::sortingOrder
	int32_t ___sortingOrder_46;

public:
	inline static int32_t get_offset_of_fpsCounter_7() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___fpsCounter_7)); }
	inline FPSCounterData_t432421409 * get_fpsCounter_7() const { return ___fpsCounter_7; }
	inline FPSCounterData_t432421409 ** get_address_of_fpsCounter_7() { return &___fpsCounter_7; }
	inline void set_fpsCounter_7(FPSCounterData_t432421409 * value)
	{
		___fpsCounter_7 = value;
		Il2CppCodeGenWriteBarrier(&___fpsCounter_7, value);
	}

	inline static int32_t get_offset_of_memoryCounter_8() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___memoryCounter_8)); }
	inline MemoryCounterData_t1329736715 * get_memoryCounter_8() const { return ___memoryCounter_8; }
	inline MemoryCounterData_t1329736715 ** get_address_of_memoryCounter_8() { return &___memoryCounter_8; }
	inline void set_memoryCounter_8(MemoryCounterData_t1329736715 * value)
	{
		___memoryCounter_8 = value;
		Il2CppCodeGenWriteBarrier(&___memoryCounter_8, value);
	}

	inline static int32_t get_offset_of_deviceInfoCounter_9() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___deviceInfoCounter_9)); }
	inline DeviceInfoCounterData_t1356006596 * get_deviceInfoCounter_9() const { return ___deviceInfoCounter_9; }
	inline DeviceInfoCounterData_t1356006596 ** get_address_of_deviceInfoCounter_9() { return &___deviceInfoCounter_9; }
	inline void set_deviceInfoCounter_9(DeviceInfoCounterData_t1356006596 * value)
	{
		___deviceInfoCounter_9 = value;
		Il2CppCodeGenWriteBarrier(&___deviceInfoCounter_9, value);
	}

	inline static int32_t get_offset_of_hotKey_10() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___hotKey_10)); }
	inline int32_t get_hotKey_10() const { return ___hotKey_10; }
	inline int32_t* get_address_of_hotKey_10() { return &___hotKey_10; }
	inline void set_hotKey_10(int32_t value)
	{
		___hotKey_10 = value;
	}

	inline static int32_t get_offset_of_circleGesture_11() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___circleGesture_11)); }
	inline bool get_circleGesture_11() const { return ___circleGesture_11; }
	inline bool* get_address_of_circleGesture_11() { return &___circleGesture_11; }
	inline void set_circleGesture_11(bool value)
	{
		___circleGesture_11 = value;
	}

	inline static int32_t get_offset_of_hotKeyCtrl_12() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___hotKeyCtrl_12)); }
	inline bool get_hotKeyCtrl_12() const { return ___hotKeyCtrl_12; }
	inline bool* get_address_of_hotKeyCtrl_12() { return &___hotKeyCtrl_12; }
	inline void set_hotKeyCtrl_12(bool value)
	{
		___hotKeyCtrl_12 = value;
	}

	inline static int32_t get_offset_of_hotKeyShift_13() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___hotKeyShift_13)); }
	inline bool get_hotKeyShift_13() const { return ___hotKeyShift_13; }
	inline bool* get_address_of_hotKeyShift_13() { return &___hotKeyShift_13; }
	inline void set_hotKeyShift_13(bool value)
	{
		___hotKeyShift_13 = value;
	}

	inline static int32_t get_offset_of_hotKeyAlt_14() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___hotKeyAlt_14)); }
	inline bool get_hotKeyAlt_14() const { return ___hotKeyAlt_14; }
	inline bool* get_address_of_hotKeyAlt_14() { return &___hotKeyAlt_14; }
	inline void set_hotKeyAlt_14(bool value)
	{
		___hotKeyAlt_14 = value;
	}

	inline static int32_t get_offset_of_keepAlive_15() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___keepAlive_15)); }
	inline bool get_keepAlive_15() const { return ___keepAlive_15; }
	inline bool* get_address_of_keepAlive_15() { return &___keepAlive_15; }
	inline void set_keepAlive_15(bool value)
	{
		___keepAlive_15 = value;
	}

	inline static int32_t get_offset_of_canvas_16() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___canvas_16)); }
	inline Canvas_t209405766 * get_canvas_16() const { return ___canvas_16; }
	inline Canvas_t209405766 ** get_address_of_canvas_16() { return &___canvas_16; }
	inline void set_canvas_16(Canvas_t209405766 * value)
	{
		___canvas_16 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_16, value);
	}

	inline static int32_t get_offset_of_canvasScaler_17() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___canvasScaler_17)); }
	inline CanvasScaler_t2574720772 * get_canvasScaler_17() const { return ___canvasScaler_17; }
	inline CanvasScaler_t2574720772 ** get_address_of_canvasScaler_17() { return &___canvasScaler_17; }
	inline void set_canvasScaler_17(CanvasScaler_t2574720772 * value)
	{
		___canvasScaler_17 = value;
		Il2CppCodeGenWriteBarrier(&___canvasScaler_17, value);
	}

	inline static int32_t get_offset_of_externalCanvas_18() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___externalCanvas_18)); }
	inline bool get_externalCanvas_18() const { return ___externalCanvas_18; }
	inline bool* get_address_of_externalCanvas_18() { return &___externalCanvas_18; }
	inline void set_externalCanvas_18(bool value)
	{
		___externalCanvas_18 = value;
	}

	inline static int32_t get_offset_of_labels_19() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___labels_19)); }
	inline DrawableLabelU5BU5D_t209901987* get_labels_19() const { return ___labels_19; }
	inline DrawableLabelU5BU5D_t209901987** get_address_of_labels_19() { return &___labels_19; }
	inline void set_labels_19(DrawableLabelU5BU5D_t209901987* value)
	{
		___labels_19 = value;
		Il2CppCodeGenWriteBarrier(&___labels_19, value);
	}

	inline static int32_t get_offset_of_anchorsCount_20() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___anchorsCount_20)); }
	inline int32_t get_anchorsCount_20() const { return ___anchorsCount_20; }
	inline int32_t* get_address_of_anchorsCount_20() { return &___anchorsCount_20; }
	inline void set_anchorsCount_20(int32_t value)
	{
		___anchorsCount_20 = value;
	}

	inline static int32_t get_offset_of_cachedVSync_21() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___cachedVSync_21)); }
	inline int32_t get_cachedVSync_21() const { return ___cachedVSync_21; }
	inline int32_t* get_address_of_cachedVSync_21() { return &___cachedVSync_21; }
	inline void set_cachedVSync_21(int32_t value)
	{
		___cachedVSync_21 = value;
	}

	inline static int32_t get_offset_of_cachedFrameRate_22() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___cachedFrameRate_22)); }
	inline int32_t get_cachedFrameRate_22() const { return ___cachedFrameRate_22; }
	inline int32_t* get_address_of_cachedFrameRate_22() { return &___cachedFrameRate_22; }
	inline void set_cachedFrameRate_22(int32_t value)
	{
		___cachedFrameRate_22 = value;
	}

	inline static int32_t get_offset_of_inited_23() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___inited_23)); }
	inline bool get_inited_23() const { return ___inited_23; }
	inline bool* get_address_of_inited_23() { return &___inited_23; }
	inline void set_inited_23(bool value)
	{
		___inited_23 = value;
	}

	inline static int32_t get_offset_of_gesturePoints_24() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___gesturePoints_24)); }
	inline List_1_t1612828711 * get_gesturePoints_24() const { return ___gesturePoints_24; }
	inline List_1_t1612828711 ** get_address_of_gesturePoints_24() { return &___gesturePoints_24; }
	inline void set_gesturePoints_24(List_1_t1612828711 * value)
	{
		___gesturePoints_24 = value;
		Il2CppCodeGenWriteBarrier(&___gesturePoints_24, value);
	}

	inline static int32_t get_offset_of_gestureCount_25() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___gestureCount_25)); }
	inline int32_t get_gestureCount_25() const { return ___gestureCount_25; }
	inline int32_t* get_address_of_gestureCount_25() { return &___gestureCount_25; }
	inline void set_gestureCount_25(int32_t value)
	{
		___gestureCount_25 = value;
	}

	inline static int32_t get_offset_of_operationMode_26() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___operationMode_26)); }
	inline uint8_t get_operationMode_26() const { return ___operationMode_26; }
	inline uint8_t* get_address_of_operationMode_26() { return &___operationMode_26; }
	inline void set_operationMode_26(uint8_t value)
	{
		___operationMode_26 = value;
	}

	inline static int32_t get_offset_of_forceFrameRate_27() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___forceFrameRate_27)); }
	inline bool get_forceFrameRate_27() const { return ___forceFrameRate_27; }
	inline bool* get_address_of_forceFrameRate_27() { return &___forceFrameRate_27; }
	inline void set_forceFrameRate_27(bool value)
	{
		___forceFrameRate_27 = value;
	}

	inline static int32_t get_offset_of_forcedFrameRate_28() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___forcedFrameRate_28)); }
	inline int32_t get_forcedFrameRate_28() const { return ___forcedFrameRate_28; }
	inline int32_t* get_address_of_forcedFrameRate_28() { return &___forcedFrameRate_28; }
	inline void set_forcedFrameRate_28(int32_t value)
	{
		___forcedFrameRate_28 = value;
	}

	inline static int32_t get_offset_of_background_29() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___background_29)); }
	inline bool get_background_29() const { return ___background_29; }
	inline bool* get_address_of_background_29() { return &___background_29; }
	inline void set_background_29(bool value)
	{
		___background_29 = value;
	}

	inline static int32_t get_offset_of_backgroundColor_30() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___backgroundColor_30)); }
	inline Color_t2020392075  get_backgroundColor_30() const { return ___backgroundColor_30; }
	inline Color_t2020392075 * get_address_of_backgroundColor_30() { return &___backgroundColor_30; }
	inline void set_backgroundColor_30(Color_t2020392075  value)
	{
		___backgroundColor_30 = value;
	}

	inline static int32_t get_offset_of_backgroundPadding_31() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___backgroundPadding_31)); }
	inline int32_t get_backgroundPadding_31() const { return ___backgroundPadding_31; }
	inline int32_t* get_address_of_backgroundPadding_31() { return &___backgroundPadding_31; }
	inline void set_backgroundPadding_31(int32_t value)
	{
		___backgroundPadding_31 = value;
	}

	inline static int32_t get_offset_of_shadow_32() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___shadow_32)); }
	inline bool get_shadow_32() const { return ___shadow_32; }
	inline bool* get_address_of_shadow_32() { return &___shadow_32; }
	inline void set_shadow_32(bool value)
	{
		___shadow_32 = value;
	}

	inline static int32_t get_offset_of_shadowColor_33() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___shadowColor_33)); }
	inline Color_t2020392075  get_shadowColor_33() const { return ___shadowColor_33; }
	inline Color_t2020392075 * get_address_of_shadowColor_33() { return &___shadowColor_33; }
	inline void set_shadowColor_33(Color_t2020392075  value)
	{
		___shadowColor_33 = value;
	}

	inline static int32_t get_offset_of_shadowDistance_34() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___shadowDistance_34)); }
	inline Vector2_t2243707579  get_shadowDistance_34() const { return ___shadowDistance_34; }
	inline Vector2_t2243707579 * get_address_of_shadowDistance_34() { return &___shadowDistance_34; }
	inline void set_shadowDistance_34(Vector2_t2243707579  value)
	{
		___shadowDistance_34 = value;
	}

	inline static int32_t get_offset_of_outline_35() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___outline_35)); }
	inline bool get_outline_35() const { return ___outline_35; }
	inline bool* get_address_of_outline_35() { return &___outline_35; }
	inline void set_outline_35(bool value)
	{
		___outline_35 = value;
	}

	inline static int32_t get_offset_of_outlineColor_36() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___outlineColor_36)); }
	inline Color_t2020392075  get_outlineColor_36() const { return ___outlineColor_36; }
	inline Color_t2020392075 * get_address_of_outlineColor_36() { return &___outlineColor_36; }
	inline void set_outlineColor_36(Color_t2020392075  value)
	{
		___outlineColor_36 = value;
	}

	inline static int32_t get_offset_of_outlineDistance_37() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___outlineDistance_37)); }
	inline Vector2_t2243707579  get_outlineDistance_37() const { return ___outlineDistance_37; }
	inline Vector2_t2243707579 * get_address_of_outlineDistance_37() { return &___outlineDistance_37; }
	inline void set_outlineDistance_37(Vector2_t2243707579  value)
	{
		___outlineDistance_37 = value;
	}

	inline static int32_t get_offset_of_autoScale_38() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___autoScale_38)); }
	inline bool get_autoScale_38() const { return ___autoScale_38; }
	inline bool* get_address_of_autoScale_38() { return &___autoScale_38; }
	inline void set_autoScale_38(bool value)
	{
		___autoScale_38 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_39() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___scaleFactor_39)); }
	inline float get_scaleFactor_39() const { return ___scaleFactor_39; }
	inline float* get_address_of_scaleFactor_39() { return &___scaleFactor_39; }
	inline void set_scaleFactor_39(float value)
	{
		___scaleFactor_39 = value;
	}

	inline static int32_t get_offset_of_labelsFont_40() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___labelsFont_40)); }
	inline Font_t4239498691 * get_labelsFont_40() const { return ___labelsFont_40; }
	inline Font_t4239498691 ** get_address_of_labelsFont_40() { return &___labelsFont_40; }
	inline void set_labelsFont_40(Font_t4239498691 * value)
	{
		___labelsFont_40 = value;
		Il2CppCodeGenWriteBarrier(&___labelsFont_40, value);
	}

	inline static int32_t get_offset_of_fontSize_41() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___fontSize_41)); }
	inline int32_t get_fontSize_41() const { return ___fontSize_41; }
	inline int32_t* get_address_of_fontSize_41() { return &___fontSize_41; }
	inline void set_fontSize_41(int32_t value)
	{
		___fontSize_41 = value;
	}

	inline static int32_t get_offset_of_lineSpacing_42() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___lineSpacing_42)); }
	inline float get_lineSpacing_42() const { return ___lineSpacing_42; }
	inline float* get_address_of_lineSpacing_42() { return &___lineSpacing_42; }
	inline void set_lineSpacing_42(float value)
	{
		___lineSpacing_42 = value;
	}

	inline static int32_t get_offset_of_countersSpacing_43() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___countersSpacing_43)); }
	inline int32_t get_countersSpacing_43() const { return ___countersSpacing_43; }
	inline int32_t* get_address_of_countersSpacing_43() { return &___countersSpacing_43; }
	inline void set_countersSpacing_43(int32_t value)
	{
		___countersSpacing_43 = value;
	}

	inline static int32_t get_offset_of_paddingOffset_44() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___paddingOffset_44)); }
	inline Vector2_t2243707579  get_paddingOffset_44() const { return ___paddingOffset_44; }
	inline Vector2_t2243707579 * get_address_of_paddingOffset_44() { return &___paddingOffset_44; }
	inline void set_paddingOffset_44(Vector2_t2243707579  value)
	{
		___paddingOffset_44 = value;
	}

	inline static int32_t get_offset_of_pixelPerfect_45() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___pixelPerfect_45)); }
	inline bool get_pixelPerfect_45() const { return ___pixelPerfect_45; }
	inline bool* get_address_of_pixelPerfect_45() { return &___pixelPerfect_45; }
	inline void set_pixelPerfect_45(bool value)
	{
		___pixelPerfect_45 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_46() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012, ___sortingOrder_46)); }
	inline int32_t get_sortingOrder_46() const { return ___sortingOrder_46; }
	inline int32_t* get_address_of_sortingOrder_46() { return &___sortingOrder_46; }
	inline void set_sortingOrder_46(int32_t value)
	{
		___sortingOrder_46 = value;
	}
};

struct AFPSCounter_t1611953012_StaticFields
{
public:
	// CodeStage.AdvancedFPSCounter.AFPSCounter CodeStage.AdvancedFPSCounter.AFPSCounter::<Instance>k__BackingField
	AFPSCounter_t1611953012 * ___U3CInstanceU3Ek__BackingField_47;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(AFPSCounter_t1611953012_StaticFields, ___U3CInstanceU3Ek__BackingField_47)); }
	inline AFPSCounter_t1611953012 * get_U3CInstanceU3Ek__BackingField_47() const { return ___U3CInstanceU3Ek__BackingField_47; }
	inline AFPSCounter_t1611953012 ** get_address_of_U3CInstanceU3Ek__BackingField_47() { return &___U3CInstanceU3Ek__BackingField_47; }
	inline void set_U3CInstanceU3Ek__BackingField_47(AFPSCounter_t1611953012 * value)
	{
		___U3CInstanceU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInstanceU3Ek__BackingField_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
