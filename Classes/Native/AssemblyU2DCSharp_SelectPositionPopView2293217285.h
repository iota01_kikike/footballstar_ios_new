﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// SelectPositionCell
struct SelectPositionCell_t908287989;
// Tacticsoft.TableView
struct TableView_t3179510217;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Player>>
struct Dictionary_2_t2431683951;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectPositionPopView
struct  SelectPositionPopView_t2293217285  : public MonoBehaviour_t1158329972
{
public:
	// SelectPositionCell SelectPositionPopView::CellPrefab
	SelectPositionCell_t908287989 * ___CellPrefab_2;
	// Tacticsoft.TableView SelectPositionPopView::TableView
	TableView_t3179510217 * ___TableView_3;
	// UnityEngine.UI.Button SelectPositionPopView::CloseButton
	Button_t2872111280 * ___CloseButton_4;
	// UnityEngine.UI.Button[] SelectPositionPopView::TabButton
	ButtonU5BU5D_t3071100561* ___TabButton_5;
	// UnityEngine.UI.Text SelectPositionPopView::NoPlayerText
	Text_t356221433 * ___NoPlayerText_6;
	// System.Int32 SelectPositionPopView::curTabIndex
	int32_t ___curTabIndex_7;
	// System.Int32 SelectPositionPopView::numInstancesCreated
	int32_t ___numInstancesCreated_8;
	// System.String SelectPositionPopView::position
	String_t* ___position_9;
	// System.Int32 SelectPositionPopView::formationNo
	int32_t ___formationNo_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Player>> SelectPositionPopView::playerListDict
	Dictionary_2_t2431683951 * ___playerListDict_11;

public:
	inline static int32_t get_offset_of_CellPrefab_2() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___CellPrefab_2)); }
	inline SelectPositionCell_t908287989 * get_CellPrefab_2() const { return ___CellPrefab_2; }
	inline SelectPositionCell_t908287989 ** get_address_of_CellPrefab_2() { return &___CellPrefab_2; }
	inline void set_CellPrefab_2(SelectPositionCell_t908287989 * value)
	{
		___CellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___CellPrefab_2, value);
	}

	inline static int32_t get_offset_of_TableView_3() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___TableView_3)); }
	inline TableView_t3179510217 * get_TableView_3() const { return ___TableView_3; }
	inline TableView_t3179510217 ** get_address_of_TableView_3() { return &___TableView_3; }
	inline void set_TableView_3(TableView_t3179510217 * value)
	{
		___TableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___TableView_3, value);
	}

	inline static int32_t get_offset_of_CloseButton_4() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___CloseButton_4)); }
	inline Button_t2872111280 * get_CloseButton_4() const { return ___CloseButton_4; }
	inline Button_t2872111280 ** get_address_of_CloseButton_4() { return &___CloseButton_4; }
	inline void set_CloseButton_4(Button_t2872111280 * value)
	{
		___CloseButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___CloseButton_4, value);
	}

	inline static int32_t get_offset_of_TabButton_5() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___TabButton_5)); }
	inline ButtonU5BU5D_t3071100561* get_TabButton_5() const { return ___TabButton_5; }
	inline ButtonU5BU5D_t3071100561** get_address_of_TabButton_5() { return &___TabButton_5; }
	inline void set_TabButton_5(ButtonU5BU5D_t3071100561* value)
	{
		___TabButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___TabButton_5, value);
	}

	inline static int32_t get_offset_of_NoPlayerText_6() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___NoPlayerText_6)); }
	inline Text_t356221433 * get_NoPlayerText_6() const { return ___NoPlayerText_6; }
	inline Text_t356221433 ** get_address_of_NoPlayerText_6() { return &___NoPlayerText_6; }
	inline void set_NoPlayerText_6(Text_t356221433 * value)
	{
		___NoPlayerText_6 = value;
		Il2CppCodeGenWriteBarrier(&___NoPlayerText_6, value);
	}

	inline static int32_t get_offset_of_curTabIndex_7() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___curTabIndex_7)); }
	inline int32_t get_curTabIndex_7() const { return ___curTabIndex_7; }
	inline int32_t* get_address_of_curTabIndex_7() { return &___curTabIndex_7; }
	inline void set_curTabIndex_7(int32_t value)
	{
		___curTabIndex_7 = value;
	}

	inline static int32_t get_offset_of_numInstancesCreated_8() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___numInstancesCreated_8)); }
	inline int32_t get_numInstancesCreated_8() const { return ___numInstancesCreated_8; }
	inline int32_t* get_address_of_numInstancesCreated_8() { return &___numInstancesCreated_8; }
	inline void set_numInstancesCreated_8(int32_t value)
	{
		___numInstancesCreated_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___position_9)); }
	inline String_t* get_position_9() const { return ___position_9; }
	inline String_t** get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(String_t* value)
	{
		___position_9 = value;
		Il2CppCodeGenWriteBarrier(&___position_9, value);
	}

	inline static int32_t get_offset_of_formationNo_10() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___formationNo_10)); }
	inline int32_t get_formationNo_10() const { return ___formationNo_10; }
	inline int32_t* get_address_of_formationNo_10() { return &___formationNo_10; }
	inline void set_formationNo_10(int32_t value)
	{
		___formationNo_10 = value;
	}

	inline static int32_t get_offset_of_playerListDict_11() { return static_cast<int32_t>(offsetof(SelectPositionPopView_t2293217285, ___playerListDict_11)); }
	inline Dictionary_2_t2431683951 * get_playerListDict_11() const { return ___playerListDict_11; }
	inline Dictionary_2_t2431683951 ** get_address_of_playerListDict_11() { return &___playerListDict_11; }
	inline void set_playerListDict_11(Dictionary_2_t2431683951 * value)
	{
		___playerListDict_11 = value;
		Il2CppCodeGenWriteBarrier(&___playerListDict_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
