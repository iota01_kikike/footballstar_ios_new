﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredString
struct  ObscuredString_t692732302  : public Il2CppObject
{
public:
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredString::currentCryptoKey
	String_t* ___currentCryptoKey_1;
	// System.Byte[] CodeStage.AntiCheat.ObscuredTypes.ObscuredString::hiddenValue
	ByteU5BU5D_t3397334013* ___hiddenValue_2;
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredString::fakeValue
	String_t* ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredString::inited
	bool ___inited_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredString_t692732302, ___currentCryptoKey_1)); }
	inline String_t* get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline String_t** get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(String_t* value)
	{
		___currentCryptoKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___currentCryptoKey_1, value);
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredString_t692732302, ___hiddenValue_2)); }
	inline ByteU5BU5D_t3397334013* get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(ByteU5BU5D_t3397334013* value)
	{
		___hiddenValue_2 = value;
		Il2CppCodeGenWriteBarrier(&___hiddenValue_2, value);
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredString_t692732302, ___fakeValue_3)); }
	inline String_t* get_fakeValue_3() const { return ___fakeValue_3; }
	inline String_t** get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(String_t* value)
	{
		___fakeValue_3 = value;
		Il2CppCodeGenWriteBarrier(&___fakeValue_3, value);
	}

	inline static int32_t get_offset_of_inited_4() { return static_cast<int32_t>(offsetof(ObscuredString_t692732302, ___inited_4)); }
	inline bool get_inited_4() const { return ___inited_4; }
	inline bool* get_address_of_inited_4() { return &___inited_4; }
	inline void set_inited_4(bool value)
	{
		___inited_4 = value;
	}
};

struct ObscuredString_t692732302_StaticFields
{
public:
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredString::cryptoKey
	String_t* ___cryptoKey_0;

public:
	inline static int32_t get_offset_of_cryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredString_t692732302_StaticFields, ___cryptoKey_0)); }
	inline String_t* get_cryptoKey_0() const { return ___cryptoKey_0; }
	inline String_t** get_address_of_cryptoKey_0() { return &___cryptoKey_0; }
	inline void set_cryptoKey_0(String_t* value)
	{
		___cryptoKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___cryptoKey_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
