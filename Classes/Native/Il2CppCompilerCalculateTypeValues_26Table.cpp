﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExampleSpawner2272242920.h"
#include "AssemblyU2DCSharp_GD_ExampleDataScript988157557.h"
#include "AssemblyU2DCSharp_LocalizationFromDocExample277634483.h"
#include "AssemblyU2DCSharp_LocalizationExample906051089.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_Localization2131359997.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LocalizationClient2248676220.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LocalizationConfig3673596687.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LocalizationPrefs1056290271.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LangCode810189361.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LangSection1360763029.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_LabelLocalizer2982418349.h"
#include "AssemblyU2DCSharp_GPGSIds2634104883.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdFailedToLo1756611910.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_NativeAdType1094124130.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdLoader554394170.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdLoader_Build54889671.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdPosition2595513602.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdRequest3179524098.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdRequest_Bu2008174359.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdSize3231673570.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_BannerView1745853549.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_CustomNative2658458077.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_CustomNative2034144705.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Gender3528073263.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Interstitial3805611425.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Mediation_Me1641207307.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_MobileAds801923040.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Reward1753549929.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_RewardBasedV2581948736.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Common_DummyClie1330686537.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Common_Utils1179620951.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_GoogleMobileAdsCl898766308.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeAdType3944121833.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_AdLoaderClien506419447.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_AdLoaderClie3126817269.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_AdLoaderClien619382744.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2837939223.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient3611450851.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2294077762.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2607757429.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2257715507.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient1946169147.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_CustomNative3776928493.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_CustomNative3121063597.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Externs2948936873.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial2538051773.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial3343584307.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial1829207408.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_InterstitialC387623197.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial4025611083.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_InterstitialC216612155.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_MobileAdsCli1398839205.h"
#include "AssemblyU2DCSharp_MonoPInvokeCallbackAttribute1970456718.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2282664017.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV4169257859.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi862929376.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi587935421.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVid25341677.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2453903099.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi129051320.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2167763867.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi353067300.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Utils984021165.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_DataSour891715793.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Response167331027.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatu2500511426.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb3023427661.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb3485669020.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Leaderb2134496543.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_VideoCa3658338718.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_VideoQu3712660584.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_VideoCa2139525564.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Gravity891689604.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_CommonT3679499024.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Events_3595530980.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_1490481921.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_C128529763.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_C426337455.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_1092627307.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_1960848347.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_2013212230.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_1133300345.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa4139147467.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2671736816.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2446036638.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa1500688787.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa1415369233.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa3624437952.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (ExampleSpawner2_t272242920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[4] = 
{
	ExampleSpawner2_t272242920::get_offset_of_StartPos_2(),
	ExampleSpawner2_t272242920::get_offset_of_SpawnArray_3(),
	ExampleSpawner2_t272242920::get_offset_of_spawnPattern_4(),
	ExampleSpawner2_t272242920::get_offset_of_IsStarted_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (GD_ExampleDataScript_t988157557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[3] = 
{
	GD_ExampleDataScript_t988157557::get_offset_of_MyString_2(),
	GD_ExampleDataScript_t988157557::get_offset_of_MyFloatArray_3(),
	GD_ExampleDataScript_t988157557::get_offset_of_MyDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (LocalizationFromDocExample_t277634483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[7] = 
{
	LocalizationFromDocExample_t277634483::get_offset_of_GUITextStyle_2(),
	0,
	0,
	LocalizationFromDocExample_t277634483::get_offset_of_LangCodes_5(),
	LocalizationFromDocExample_t277634483::get_offset_of_languages_6(),
	LocalizationFromDocExample_t277634483::get_offset_of_tokens_7(),
	LocalizationFromDocExample_t277634483::get_offset_of_currentLangCode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (LocalizationExample_t906051089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[4] = 
{
	LocalizationExample_t906051089::get_offset_of_GUITextStyle_2(),
	0,
	0,
	LocalizationExample_t906051089::get_offset_of_Tokens_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (Localization_t2131359997), -1, sizeof(Localization_t2131359997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2604[2] = 
{
	Localization_t2131359997_StaticFields::get_offset_of_OnLanguageChanged_0(),
	Localization_t2131359997_StaticFields::get_offset_of__Client_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (LocalizationClient_t2248676220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[2] = 
{
	LocalizationClient_t2248676220::get_offset_of__langCode_0(),
	LocalizationClient_t2248676220::get_offset_of_SheetDictionary_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (LocalizationConfig_t3673596687), -1, sizeof(LocalizationConfig_t3673596687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2606[8] = 
{
	LocalizationConfig_t3673596687::get_offset_of_LocalizationDocKey_2(),
	LocalizationConfig_t3673596687::get_offset_of_isLanguagesInfoOpen_3(),
	LocalizationConfig_t3673596687::get_offset_of_isWorksheetInfoOpen_4(),
	0,
	0,
	0,
	0,
	LocalizationConfig_t3673596687_StaticFields::get_offset_of_instance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (LocalizationPrefs_t1056290271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (LangCode_t810189361)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2608[5] = 
{
	LangCode_t810189361::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (LangSection_t1360763029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[2] = 
{
	LangSection_t1360763029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (LabelLocalizer_t2982418349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[3] = 
{
	LabelLocalizer_t2982418349::get_offset_of_Token_2(),
	LabelLocalizer_t2982418349::get_offset_of_Section_3(),
	LabelLocalizer_t2982418349::get_offset_of_LocalizableLabel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (GPGSIds_t2634104883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[34] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (AdFailedToLoadEventArgs_t1756611910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[1] = 
{
	AdFailedToLoadEventArgs_t1756611910::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (NativeAdType_t1094124130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2613[2] = 
{
	NativeAdType_t1094124130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (AdLoader_t554394170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[7] = 
{
	AdLoader_t554394170::get_offset_of_adLoaderClient_0(),
	AdLoader_t554394170::get_offset_of_OnAdFailedToLoad_1(),
	AdLoader_t554394170::get_offset_of_OnCustomNativeTemplateAdLoaded_2(),
	AdLoader_t554394170::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
	AdLoader_t554394170::get_offset_of_U3CAdUnitIdU3Ek__BackingField_4(),
	AdLoader_t554394170::get_offset_of_U3CAdTypesU3Ek__BackingField_5(),
	AdLoader_t554394170::get_offset_of_U3CTemplateIdsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (Builder_t54889671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[4] = 
{
	Builder_t54889671::get_offset_of_U3CAdUnitIdU3Ek__BackingField_0(),
	Builder_t54889671::get_offset_of_U3CAdTypesU3Ek__BackingField_1(),
	Builder_t54889671::get_offset_of_U3CTemplateIdsU3Ek__BackingField_2(),
	Builder_t54889671::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (AdPosition_t2595513602)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2616[8] = 
{
	AdPosition_t2595513602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (AdRequest_t3179524098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[9] = 
{
	0,
	0,
	AdRequest_t3179524098::get_offset_of_U3CTestDevicesU3Ek__BackingField_2(),
	AdRequest_t3179524098::get_offset_of_U3CKeywordsU3Ek__BackingField_3(),
	AdRequest_t3179524098::get_offset_of_U3CBirthdayU3Ek__BackingField_4(),
	AdRequest_t3179524098::get_offset_of_U3CGenderU3Ek__BackingField_5(),
	AdRequest_t3179524098::get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(),
	AdRequest_t3179524098::get_offset_of_U3CExtrasU3Ek__BackingField_7(),
	AdRequest_t3179524098::get_offset_of_U3CMediationExtrasU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (Builder_t2008174359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[7] = 
{
	Builder_t2008174359::get_offset_of_U3CTestDevicesU3Ek__BackingField_0(),
	Builder_t2008174359::get_offset_of_U3CKeywordsU3Ek__BackingField_1(),
	Builder_t2008174359::get_offset_of_U3CBirthdayU3Ek__BackingField_2(),
	Builder_t2008174359::get_offset_of_U3CGenderU3Ek__BackingField_3(),
	Builder_t2008174359::get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(),
	Builder_t2008174359::get_offset_of_U3CExtrasU3Ek__BackingField_5(),
	Builder_t2008174359::get_offset_of_U3CMediationExtrasU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (AdSize_t3231673570), -1, sizeof(AdSize_t3231673570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2619[9] = 
{
	AdSize_t3231673570::get_offset_of_isSmartBanner_0(),
	AdSize_t3231673570::get_offset_of_width_1(),
	AdSize_t3231673570::get_offset_of_height_2(),
	AdSize_t3231673570_StaticFields::get_offset_of_Banner_3(),
	AdSize_t3231673570_StaticFields::get_offset_of_MediumRectangle_4(),
	AdSize_t3231673570_StaticFields::get_offset_of_IABBanner_5(),
	AdSize_t3231673570_StaticFields::get_offset_of_Leaderboard_6(),
	AdSize_t3231673570_StaticFields::get_offset_of_SmartBanner_7(),
	AdSize_t3231673570_StaticFields::get_offset_of_FullWidth_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (BannerView_t1745853549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[6] = 
{
	BannerView_t1745853549::get_offset_of_client_0(),
	BannerView_t1745853549::get_offset_of_OnAdLoaded_1(),
	BannerView_t1745853549::get_offset_of_OnAdFailedToLoad_2(),
	BannerView_t1745853549::get_offset_of_OnAdOpening_3(),
	BannerView_t1745853549::get_offset_of_OnAdClosed_4(),
	BannerView_t1745853549::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (CustomNativeEventArgs_t2658458077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[1] = 
{
	CustomNativeEventArgs_t2658458077::get_offset_of_U3CnativeAdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (CustomNativeTemplateAd_t2034144705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[1] = 
{
	CustomNativeTemplateAd_t2034144705::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (Gender_t3528073263)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2623[4] = 
{
	Gender_t3528073263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (InterstitialAd_t3805611425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[6] = 
{
	InterstitialAd_t3805611425::get_offset_of_client_0(),
	InterstitialAd_t3805611425::get_offset_of_OnAdLoaded_1(),
	InterstitialAd_t3805611425::get_offset_of_OnAdFailedToLoad_2(),
	InterstitialAd_t3805611425::get_offset_of_OnAdOpening_3(),
	InterstitialAd_t3805611425::get_offset_of_OnAdClosed_4(),
	InterstitialAd_t3805611425::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (MediationExtras_t1641207307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[1] = 
{
	MediationExtras_t1641207307::get_offset_of_U3CExtrasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (MobileAds_t801923040), -1, sizeof(MobileAds_t801923040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	MobileAds_t801923040_StaticFields::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (Reward_t1753549929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[2] = 
{
	Reward_t1753549929::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Reward_t1753549929::get_offset_of_U3CAmountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (RewardBasedVideoAd_t2581948736), -1, sizeof(RewardBasedVideoAd_t2581948736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2628[10] = 
{
	RewardBasedVideoAd_t2581948736::get_offset_of_client_0(),
	RewardBasedVideoAd_t2581948736_StaticFields::get_offset_of_instance_1(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdCompleted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (DummyClient_t1330686537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[9] = 
{
	DummyClient_t1330686537::get_offset_of_OnAdLoaded_0(),
	DummyClient_t1330686537::get_offset_of_OnAdFailedToLoad_1(),
	DummyClient_t1330686537::get_offset_of_OnAdOpening_2(),
	DummyClient_t1330686537::get_offset_of_OnAdStarted_3(),
	DummyClient_t1330686537::get_offset_of_OnAdClosed_4(),
	DummyClient_t1330686537::get_offset_of_OnAdRewarded_5(),
	DummyClient_t1330686537::get_offset_of_OnAdLeavingApplication_6(),
	DummyClient_t1330686537::get_offset_of_OnAdCompleted_7(),
	DummyClient_t1330686537::get_offset_of_OnCustomNativeTemplateAdLoaded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (Utils_t1179620951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (GoogleMobileAdsClientFactory_t898766308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (NativeAdTypes_t3944121833)+ sizeof (Il2CppObject), sizeof(NativeAdTypes_t3944121833 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2638[1] = 
{
	NativeAdTypes_t3944121833::get_offset_of_CustomTemplateAd_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (AdLoaderClient_t506419447), -1, sizeof(AdLoaderClient_t506419447_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2639[8] = 
{
	AdLoaderClient_t506419447::get_offset_of_adLoaderPtr_0(),
	AdLoaderClient_t506419447::get_offset_of_adLoaderClientPtr_1(),
	AdLoaderClient_t506419447::get_offset_of_adTypes_2(),
	AdLoaderClient_t506419447::get_offset_of_customNativeTemplateCallbacks_3(),
	AdLoaderClient_t506419447::get_offset_of_OnCustomNativeTemplateAdLoaded_4(),
	AdLoaderClient_t506419447::get_offset_of_OnAdFailedToLoad_5(),
	AdLoaderClient_t506419447_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	AdLoaderClient_t506419447_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t3126817269), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t619382744), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (BannerClient_t2837939223), -1, sizeof(BannerClient_t2837939223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2642[17] = 
{
	BannerClient_t2837939223::get_offset_of_bannerViewPtr_0(),
	BannerClient_t2837939223::get_offset_of_bannerClientPtr_1(),
	BannerClient_t2837939223::get_offset_of_OnAdLoaded_2(),
	BannerClient_t2837939223::get_offset_of_OnAdFailedToLoad_3(),
	BannerClient_t2837939223::get_offset_of_OnAdOpening_4(),
	BannerClient_t2837939223::get_offset_of_OnAdClosed_5(),
	BannerClient_t2837939223::get_offset_of_OnAdLeavingApplication_6(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_12(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_13(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_14(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_15(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (GADUAdViewDidReceiveAdCallback_t3611450851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (GADUAdViewDidFailToReceiveAdWithErrorCallback_t2294077762), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (GADUAdViewWillPresentScreenCallback_t2607757429), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (GADUAdViewDidDismissScreenCallback_t2257715507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (GADUAdViewWillLeaveApplicationCallback_t1946169147), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (CustomNativeTemplateClient_t3776928493), -1, sizeof(CustomNativeTemplateClient_t3776928493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2648[4] = 
{
	CustomNativeTemplateClient_t3776928493::get_offset_of_customNativeAdPtr_0(),
	CustomNativeTemplateClient_t3776928493::get_offset_of_customNativeTemplateAdClientPtr_1(),
	CustomNativeTemplateClient_t3776928493::get_offset_of_clickHandler_2(),
	CustomNativeTemplateClient_t3776928493_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (GADUNativeCustomTemplateDidReceiveClick_t3121063597), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (Externs_t2948936873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (InterstitialClient_t2538051773), -1, sizeof(InterstitialClient_t2538051773_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[12] = 
{
	InterstitialClient_t2538051773::get_offset_of_interstitialPtr_0(),
	InterstitialClient_t2538051773::get_offset_of_interstitialClientPtr_1(),
	InterstitialClient_t2538051773::get_offset_of_OnAdLoaded_2(),
	InterstitialClient_t2538051773::get_offset_of_OnAdFailedToLoad_3(),
	InterstitialClient_t2538051773::get_offset_of_OnAdOpening_4(),
	InterstitialClient_t2538051773::get_offset_of_OnAdClosed_5(),
	InterstitialClient_t2538051773::get_offset_of_OnAdLeavingApplication_6(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (GADUInterstitialDidReceiveAdCallback_t3343584307), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1829207408), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (GADUInterstitialWillPresentScreenCallback_t387623197), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (GADUInterstitialDidDismissScreenCallback_t4025611083), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (GADUInterstitialWillLeaveApplicationCallback_t216612155), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (MobileAdsClient_t1398839205), -1, sizeof(MobileAdsClient_t1398839205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2657[1] = 
{
	MobileAdsClient_t1398839205_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (MonoPInvokeCallbackAttribute_t1970456718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (RewardBasedVideoAdClient_t2282664017), -1, sizeof(RewardBasedVideoAdClient_t2282664017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2659[18] = 
{
	RewardBasedVideoAdClient_t2282664017::get_offset_of_rewardBasedVideoAdPtr_0(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_rewardBasedVideoAdClientPtr_1(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdCompleted_9(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_11(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_12(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_13(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_14(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_15(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_16(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (GADURewardBasedVideoAdDidOpenCallback_t587935421), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (GADURewardBasedVideoAdDidStartCallback_t25341677), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (GADURewardBasedVideoAdDidCloseCallback_t2453903099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (GADURewardBasedVideoAdDidRewardCallback_t129051320), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (GADURewardBasedVideoAdDidCompleteCallback_t353067300), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (Utils_t984021165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (DataSource_t891715793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2669[3] = 
{
	DataSource_t891715793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (ResponseStatus_t167331027)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2670[8] = 
{
	ResponseStatus_t167331027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (UIStatus_t2500511426)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2671[9] = 
{
	UIStatus_t2500511426::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (LeaderboardStart_t3023427661)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2672[3] = 
{
	LeaderboardStart_t3023427661::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (LeaderboardTimeSpan_t3485669020)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2673[4] = 
{
	LeaderboardTimeSpan_t3485669020::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (LeaderboardCollection_t2134496543)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2674[3] = 
{
	LeaderboardCollection_t2134496543::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (VideoCaptureMode_t3658338718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	VideoCaptureMode_t3658338718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (VideoQualityLevel_t3712660584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2676[6] = 
{
	VideoQualityLevel_t3712660584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (VideoCaptureOverlayState_t2139525564)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2677[6] = 
{
	VideoCaptureOverlayState_t2139525564::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (Gravity_t891689604)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2678[6] = 
{
	Gravity_t891689604::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (CommonTypesUtil_t3679499024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (EventVisibility_t3595530980)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2680[3] = 
{
	EventVisibility_t3595530980::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (AdvertisingResult_t1490481921)+ sizeof (Il2CppObject), sizeof(AdvertisingResult_t1490481921_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2682[2] = 
{
	AdvertisingResult_t1490481921::get_offset_of_mStatus_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AdvertisingResult_t1490481921::get_offset_of_mLocalEndpointName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (ConnectionRequest_t128529763)+ sizeof (Il2CppObject), sizeof(ConnectionRequest_t128529763_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2683[2] = 
{
	ConnectionRequest_t128529763::get_offset_of_mRemoteEndpoint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionRequest_t128529763::get_offset_of_mPayload_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (ConnectionResponse_t426337455)+ sizeof (Il2CppObject), sizeof(ConnectionResponse_t426337455_marshaled_pinvoke), sizeof(ConnectionResponse_t426337455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2684[5] = 
{
	ConnectionResponse_t426337455_StaticFields::get_offset_of_EmptyPayload_0(),
	ConnectionResponse_t426337455::get_offset_of_mLocalClientId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionResponse_t426337455::get_offset_of_mRemoteEndpointId_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionResponse_t426337455::get_offset_of_mResponseStatus_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ConnectionResponse_t426337455::get_offset_of_mPayload_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (Status_t1092627307)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2685[7] = 
{
	Status_t1092627307::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (EndpointDetails_t1960848347)+ sizeof (Il2CppObject), sizeof(EndpointDetails_t1960848347_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2686[3] = 
{
	EndpointDetails_t1960848347::get_offset_of_mEndpointId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EndpointDetails_t1960848347::get_offset_of_mName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EndpointDetails_t1960848347::get_offset_of_mServiceId_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (InitializationStatus_t2013212230)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	InitializationStatus_t2013212230::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (NearbyConnectionConfiguration_t1133300345)+ sizeof (Il2CppObject), sizeof(NearbyConnectionConfiguration_t1133300345_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[4] = 
{
	0,
	0,
	NearbyConnectionConfiguration_t1133300345::get_offset_of_mInitializationCallback_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NearbyConnectionConfiguration_t1133300345::get_offset_of_mLocalClientId_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (ConflictResolutionStrategy_t4139147467)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2691[7] = 
{
	ConflictResolutionStrategy_t4139147467::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (SavedGameRequestStatus_t2671736816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2692[6] = 
{
	SavedGameRequestStatus_t2671736816::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (SelectUIStatus_t2446036638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[7] = 
{
	SelectUIStatus_t2446036638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (ConflictCallback_t1500688787), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (SavedGameMetadataUpdate_t1415369233)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[5] = 
{
	SavedGameMetadataUpdate_t1415369233::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t1415369233::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t1415369233::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t1415369233::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SavedGameMetadataUpdate_t1415369233::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (Builder_t3624437952)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[5] = 
{
	Builder_t3624437952::get_offset_of_mDescriptionUpdated_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t3624437952::get_offset_of_mNewDescription_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t3624437952::get_offset_of_mCoverImageUpdated_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t3624437952::get_offset_of_mNewPngCoverImage_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Builder_t3624437952::get_offset_of_mNewPlayedTime_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
