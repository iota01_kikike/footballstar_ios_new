﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"

// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t2691517565;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStore
struct  FakeStore_t3882981564  : public JSONStore_t1890359403
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.FakeStore::m_Biller
	Il2CppObject * ___m_Biller_22;
	// System.Collections.Generic.List`1<System.String> UnityEngine.Purchasing.FakeStore::m_PurchasedProducts
	List_1_t1398341365 * ___m_PurchasedProducts_23;
	// System.Boolean UnityEngine.Purchasing.FakeStore::purchaseCalled
	bool ___purchaseCalled_24;
	// System.String UnityEngine.Purchasing.FakeStore::<unavailableProductId>k__BackingField
	String_t* ___U3CunavailableProductIdU3Ek__BackingField_25;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.FakeStore::UIMode
	int32_t ___UIMode_26;

public:
	inline static int32_t get_offset_of_m_Biller_22() { return static_cast<int32_t>(offsetof(FakeStore_t3882981564, ___m_Biller_22)); }
	inline Il2CppObject * get_m_Biller_22() const { return ___m_Biller_22; }
	inline Il2CppObject ** get_address_of_m_Biller_22() { return &___m_Biller_22; }
	inline void set_m_Biller_22(Il2CppObject * value)
	{
		___m_Biller_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_Biller_22, value);
	}

	inline static int32_t get_offset_of_m_PurchasedProducts_23() { return static_cast<int32_t>(offsetof(FakeStore_t3882981564, ___m_PurchasedProducts_23)); }
	inline List_1_t1398341365 * get_m_PurchasedProducts_23() const { return ___m_PurchasedProducts_23; }
	inline List_1_t1398341365 ** get_address_of_m_PurchasedProducts_23() { return &___m_PurchasedProducts_23; }
	inline void set_m_PurchasedProducts_23(List_1_t1398341365 * value)
	{
		___m_PurchasedProducts_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_PurchasedProducts_23, value);
	}

	inline static int32_t get_offset_of_purchaseCalled_24() { return static_cast<int32_t>(offsetof(FakeStore_t3882981564, ___purchaseCalled_24)); }
	inline bool get_purchaseCalled_24() const { return ___purchaseCalled_24; }
	inline bool* get_address_of_purchaseCalled_24() { return &___purchaseCalled_24; }
	inline void set_purchaseCalled_24(bool value)
	{
		___purchaseCalled_24 = value;
	}

	inline static int32_t get_offset_of_U3CunavailableProductIdU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(FakeStore_t3882981564, ___U3CunavailableProductIdU3Ek__BackingField_25)); }
	inline String_t* get_U3CunavailableProductIdU3Ek__BackingField_25() const { return ___U3CunavailableProductIdU3Ek__BackingField_25; }
	inline String_t** get_address_of_U3CunavailableProductIdU3Ek__BackingField_25() { return &___U3CunavailableProductIdU3Ek__BackingField_25; }
	inline void set_U3CunavailableProductIdU3Ek__BackingField_25(String_t* value)
	{
		___U3CunavailableProductIdU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CunavailableProductIdU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_UIMode_26() { return static_cast<int32_t>(offsetof(FakeStore_t3882981564, ___UIMode_26)); }
	inline int32_t get_UIMode_26() const { return ___UIMode_26; }
	inline int32_t* get_address_of_UIMode_26() { return &___UIMode_26; }
	inline void set_UIMode_26(int32_t value)
	{
		___UIMode_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
