﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_1505541237.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble/DoubleLongBytesUnion
struct  DoubleLongBytesUnion_t2034359298 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble/DoubleLongBytesUnion::d
			double ___d_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___d_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble/DoubleLongBytesUnion::l
			int64_t ___l_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___l_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// CodeStage.AntiCheat.Common.ACTkByte8 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble/DoubleLongBytesUnion::b8
			ACTkByte8_t1505541237  ___b8_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			ACTkByte8_t1505541237  ___b8_2_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(DoubleLongBytesUnion_t2034359298, ___d_0)); }
	inline double get_d_0() const { return ___d_0; }
	inline double* get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(double value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_l_1() { return static_cast<int32_t>(offsetof(DoubleLongBytesUnion_t2034359298, ___l_1)); }
	inline int64_t get_l_1() const { return ___l_1; }
	inline int64_t* get_address_of_l_1() { return &___l_1; }
	inline void set_l_1(int64_t value)
	{
		___l_1 = value;
	}

	inline static int32_t get_offset_of_b8_2() { return static_cast<int32_t>(offsetof(DoubleLongBytesUnion_t2034359298, ___b8_2)); }
	inline ACTkByte8_t1505541237  get_b8_2() const { return ___b8_2; }
	inline ACTkByte8_t1505541237 * get_address_of_b8_2() { return &___b8_2; }
	inline void set_b8_2(ACTkByte8_t1505541237  value)
	{
		___b8_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
