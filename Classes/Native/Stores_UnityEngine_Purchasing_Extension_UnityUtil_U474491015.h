﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Purchasing.Extension.UnityUtil
struct UnityUtil_t166323129;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__41
struct  U3CDelayedCoroutineU3Ed__41_t474491015  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__41::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// System.Collections.IEnumerator UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__41::coroutine
	Il2CppObject * ___coroutine_2;
	// System.Int32 UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__41::delay
	int32_t ___delay_3;
	// UnityEngine.Purchasing.Extension.UnityUtil UnityEngine.Purchasing.Extension.UnityUtil/<DelayedCoroutine>d__41::<>4__this
	UnityUtil_t166323129 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__41_t474491015, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__41_t474491015, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_coroutine_2() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__41_t474491015, ___coroutine_2)); }
	inline Il2CppObject * get_coroutine_2() const { return ___coroutine_2; }
	inline Il2CppObject ** get_address_of_coroutine_2() { return &___coroutine_2; }
	inline void set_coroutine_2(Il2CppObject * value)
	{
		___coroutine_2 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_2, value);
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__41_t474491015, ___delay_3)); }
	inline int32_t get_delay_3() const { return ___delay_3; }
	inline int32_t* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(int32_t value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CDelayedCoroutineU3Ed__41_t474491015, ___U3CU3E4__this_4)); }
	inline UnityUtil_t166323129 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline UnityUtil_t166323129 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(UnityUtil_t166323129 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
