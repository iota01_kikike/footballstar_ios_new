﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3458667235.h"

// System.String
struct String_t;
// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs
struct  ObscuredPrefs_t2721367689  : public Il2CppObject
{
public:

public:
};

struct ObscuredPrefs_t2721367689_StaticFields
{
public:
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::foreignSavesReported
	bool ___foreignSavesReported_3;
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::cryptoKey
	String_t* ___cryptoKey_4;
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::deviceId
	String_t* ___deviceId_5;
	// System.UInt32 CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::deviceIdHash
	uint32_t ___deviceIdHash_6;
	// System.Action CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::onAlterationDetected
	Action_t3226471752 * ___onAlterationDetected_7;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::preservePlayerPrefs
	bool ___preservePlayerPrefs_8;
	// System.Action CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::onPossibleForeignSavesDetected
	Action_t3226471752 * ___onPossibleForeignSavesDetected_9;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs/DeviceLockLevel CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::lockToDevice
	uint8_t ___lockToDevice_10;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::readForeignSaves
	bool ___readForeignSaves_11;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::emergencyMode
	bool ___emergencyMode_12;
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredPrefs::deprecatedDeviceId
	String_t* ___deprecatedDeviceId_14;

public:
	inline static int32_t get_offset_of_foreignSavesReported_3() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___foreignSavesReported_3)); }
	inline bool get_foreignSavesReported_3() const { return ___foreignSavesReported_3; }
	inline bool* get_address_of_foreignSavesReported_3() { return &___foreignSavesReported_3; }
	inline void set_foreignSavesReported_3(bool value)
	{
		___foreignSavesReported_3 = value;
	}

	inline static int32_t get_offset_of_cryptoKey_4() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___cryptoKey_4)); }
	inline String_t* get_cryptoKey_4() const { return ___cryptoKey_4; }
	inline String_t** get_address_of_cryptoKey_4() { return &___cryptoKey_4; }
	inline void set_cryptoKey_4(String_t* value)
	{
		___cryptoKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___cryptoKey_4, value);
	}

	inline static int32_t get_offset_of_deviceId_5() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___deviceId_5)); }
	inline String_t* get_deviceId_5() const { return ___deviceId_5; }
	inline String_t** get_address_of_deviceId_5() { return &___deviceId_5; }
	inline void set_deviceId_5(String_t* value)
	{
		___deviceId_5 = value;
		Il2CppCodeGenWriteBarrier(&___deviceId_5, value);
	}

	inline static int32_t get_offset_of_deviceIdHash_6() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___deviceIdHash_6)); }
	inline uint32_t get_deviceIdHash_6() const { return ___deviceIdHash_6; }
	inline uint32_t* get_address_of_deviceIdHash_6() { return &___deviceIdHash_6; }
	inline void set_deviceIdHash_6(uint32_t value)
	{
		___deviceIdHash_6 = value;
	}

	inline static int32_t get_offset_of_onAlterationDetected_7() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___onAlterationDetected_7)); }
	inline Action_t3226471752 * get_onAlterationDetected_7() const { return ___onAlterationDetected_7; }
	inline Action_t3226471752 ** get_address_of_onAlterationDetected_7() { return &___onAlterationDetected_7; }
	inline void set_onAlterationDetected_7(Action_t3226471752 * value)
	{
		___onAlterationDetected_7 = value;
		Il2CppCodeGenWriteBarrier(&___onAlterationDetected_7, value);
	}

	inline static int32_t get_offset_of_preservePlayerPrefs_8() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___preservePlayerPrefs_8)); }
	inline bool get_preservePlayerPrefs_8() const { return ___preservePlayerPrefs_8; }
	inline bool* get_address_of_preservePlayerPrefs_8() { return &___preservePlayerPrefs_8; }
	inline void set_preservePlayerPrefs_8(bool value)
	{
		___preservePlayerPrefs_8 = value;
	}

	inline static int32_t get_offset_of_onPossibleForeignSavesDetected_9() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___onPossibleForeignSavesDetected_9)); }
	inline Action_t3226471752 * get_onPossibleForeignSavesDetected_9() const { return ___onPossibleForeignSavesDetected_9; }
	inline Action_t3226471752 ** get_address_of_onPossibleForeignSavesDetected_9() { return &___onPossibleForeignSavesDetected_9; }
	inline void set_onPossibleForeignSavesDetected_9(Action_t3226471752 * value)
	{
		___onPossibleForeignSavesDetected_9 = value;
		Il2CppCodeGenWriteBarrier(&___onPossibleForeignSavesDetected_9, value);
	}

	inline static int32_t get_offset_of_lockToDevice_10() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___lockToDevice_10)); }
	inline uint8_t get_lockToDevice_10() const { return ___lockToDevice_10; }
	inline uint8_t* get_address_of_lockToDevice_10() { return &___lockToDevice_10; }
	inline void set_lockToDevice_10(uint8_t value)
	{
		___lockToDevice_10 = value;
	}

	inline static int32_t get_offset_of_readForeignSaves_11() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___readForeignSaves_11)); }
	inline bool get_readForeignSaves_11() const { return ___readForeignSaves_11; }
	inline bool* get_address_of_readForeignSaves_11() { return &___readForeignSaves_11; }
	inline void set_readForeignSaves_11(bool value)
	{
		___readForeignSaves_11 = value;
	}

	inline static int32_t get_offset_of_emergencyMode_12() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___emergencyMode_12)); }
	inline bool get_emergencyMode_12() const { return ___emergencyMode_12; }
	inline bool* get_address_of_emergencyMode_12() { return &___emergencyMode_12; }
	inline void set_emergencyMode_12(bool value)
	{
		___emergencyMode_12 = value;
	}

	inline static int32_t get_offset_of_deprecatedDeviceId_14() { return static_cast<int32_t>(offsetof(ObscuredPrefs_t2721367689_StaticFields, ___deprecatedDeviceId_14)); }
	inline String_t* get_deprecatedDeviceId_14() const { return ___deprecatedDeviceId_14; }
	inline String_t** get_address_of_deprecatedDeviceId_14() { return &___deprecatedDeviceId_14; }
	inline void set_deprecatedDeviceId_14(String_t* value)
	{
		___deprecatedDeviceId_14 = value;
		Il2CppCodeGenWriteBarrier(&___deprecatedDeviceId_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
