﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<UnityEngine.WWW>
struct Action_1_t2721744421;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.UTime.UTime/<Download>c__Iterator0
struct  U3CDownloadU3Ec__Iterator0_t2940414941  : public Il2CppObject
{
public:
	// System.String Assets.UTime.UTime/<Download>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW Assets.UTime.UTime/<Download>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.Action`1<UnityEngine.WWW> Assets.UTime.UTime/<Download>c__Iterator0::callback
	Action_1_t2721744421 * ___callback_2;
	// System.Object Assets.UTime.UTime/<Download>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean Assets.UTime.UTime/<Download>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Assets.UTime.UTime/<Download>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator0_t2940414941, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator0_t2940414941, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator0_t2940414941, ___callback_2)); }
	inline Action_1_t2721744421 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t2721744421 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t2721744421 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator0_t2940414941, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator0_t2940414941, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator0_t2940414941, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
