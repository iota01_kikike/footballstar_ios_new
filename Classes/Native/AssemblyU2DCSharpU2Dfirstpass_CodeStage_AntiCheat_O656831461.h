﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_3474909705.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat/FloatIntBytesUnion
struct  FloatIntBytesUnion_t656831461 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat/FloatIntBytesUnion::f
			float ___f_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___f_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat/FloatIntBytesUnion::i
			int32_t ___i_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___i_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// CodeStage.AntiCheat.Common.ACTkByte4 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat/FloatIntBytesUnion::b4
			ACTkByte4_t3474909705  ___b4_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			ACTkByte4_t3474909705  ___b4_2_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_f_0() { return static_cast<int32_t>(offsetof(FloatIntBytesUnion_t656831461, ___f_0)); }
	inline float get_f_0() const { return ___f_0; }
	inline float* get_address_of_f_0() { return &___f_0; }
	inline void set_f_0(float value)
	{
		___f_0 = value;
	}

	inline static int32_t get_offset_of_i_1() { return static_cast<int32_t>(offsetof(FloatIntBytesUnion_t656831461, ___i_1)); }
	inline int32_t get_i_1() const { return ___i_1; }
	inline int32_t* get_address_of_i_1() { return &___i_1; }
	inline void set_i_1(int32_t value)
	{
		___i_1 = value;
	}

	inline static int32_t get_offset_of_b4_2() { return static_cast<int32_t>(offsetof(FloatIntBytesUnion_t656831461, ___b4_2)); }
	inline ACTkByte4_t3474909705  get_b4_2() const { return ___b4_2; }
	inline ACTkByte4_t3474909705 * get_address_of_b4_2() { return &___b4_2; }
	inline void set_b4_2(ACTkByte4_t3474909705  value)
	{
		___b4_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
