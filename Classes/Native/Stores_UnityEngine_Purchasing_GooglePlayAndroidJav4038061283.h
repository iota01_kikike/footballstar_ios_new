﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_AndroidJavaStore2772549594.h"

// Uniject.IUtil
struct IUtil_t2188430191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayAndroidJavaStore
struct  GooglePlayAndroidJavaStore_t4038061283  : public AndroidJavaStore_t2772549594
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.GooglePlayAndroidJavaStore::m_Util
	Il2CppObject * ___m_Util_1;

public:
	inline static int32_t get_offset_of_m_Util_1() { return static_cast<int32_t>(offsetof(GooglePlayAndroidJavaStore_t4038061283, ___m_Util_1)); }
	inline Il2CppObject * get_m_Util_1() const { return ___m_Util_1; }
	inline Il2CppObject ** get_address_of_m_Util_1() { return &___m_Util_1; }
	inline void set_m_Util_1(Il2CppObject * value)
	{
		___m_Util_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_Util_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
