﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "Stores_UnityEngine_Purchasing_TranslationLocale3543162129.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.LocalizedProductDescription
struct  LocalizedProductDescription_t1525635964  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.TranslationLocale UnityEngine.Purchasing.LocalizedProductDescription::googleLocale
	int32_t ___googleLocale_0;
	// System.String UnityEngine.Purchasing.LocalizedProductDescription::title
	String_t* ___title_1;
	// System.String UnityEngine.Purchasing.LocalizedProductDescription::description
	String_t* ___description_2;

public:
	inline static int32_t get_offset_of_googleLocale_0() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t1525635964, ___googleLocale_0)); }
	inline int32_t get_googleLocale_0() const { return ___googleLocale_0; }
	inline int32_t* get_address_of_googleLocale_0() { return &___googleLocale_0; }
	inline void set_googleLocale_0(int32_t value)
	{
		___googleLocale_0 = value;
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t1525635964, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier(&___title_1, value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t1525635964, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier(&___description_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
