﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserCashView
struct  UserCashView_t152063903  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image UserCashView::CoinIcon
	Image_t2042527209 * ___CoinIcon_2;
	// UnityEngine.UI.Image UserCashView::GemIcon
	Image_t2042527209 * ___GemIcon_3;
	// UnityEngine.UI.Text UserCashView::CoinText
	Text_t356221433 * ___CoinText_4;
	// UnityEngine.UI.Text UserCashView::GemText
	Text_t356221433 * ___GemText_5;
	// System.Int32 UserCashView::coinForAnim
	int32_t ___coinForAnim_6;
	// System.Int32 UserCashView::gemForAnim
	int32_t ___gemForAnim_7;
	// System.Int32 UserCashView::coinForAnimProgress
	int32_t ___coinForAnimProgress_8;
	// System.Int32 UserCashView::gemForAnimProgress
	int32_t ___gemForAnimProgress_9;

public:
	inline static int32_t get_offset_of_CoinIcon_2() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___CoinIcon_2)); }
	inline Image_t2042527209 * get_CoinIcon_2() const { return ___CoinIcon_2; }
	inline Image_t2042527209 ** get_address_of_CoinIcon_2() { return &___CoinIcon_2; }
	inline void set_CoinIcon_2(Image_t2042527209 * value)
	{
		___CoinIcon_2 = value;
		Il2CppCodeGenWriteBarrier(&___CoinIcon_2, value);
	}

	inline static int32_t get_offset_of_GemIcon_3() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___GemIcon_3)); }
	inline Image_t2042527209 * get_GemIcon_3() const { return ___GemIcon_3; }
	inline Image_t2042527209 ** get_address_of_GemIcon_3() { return &___GemIcon_3; }
	inline void set_GemIcon_3(Image_t2042527209 * value)
	{
		___GemIcon_3 = value;
		Il2CppCodeGenWriteBarrier(&___GemIcon_3, value);
	}

	inline static int32_t get_offset_of_CoinText_4() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___CoinText_4)); }
	inline Text_t356221433 * get_CoinText_4() const { return ___CoinText_4; }
	inline Text_t356221433 ** get_address_of_CoinText_4() { return &___CoinText_4; }
	inline void set_CoinText_4(Text_t356221433 * value)
	{
		___CoinText_4 = value;
		Il2CppCodeGenWriteBarrier(&___CoinText_4, value);
	}

	inline static int32_t get_offset_of_GemText_5() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___GemText_5)); }
	inline Text_t356221433 * get_GemText_5() const { return ___GemText_5; }
	inline Text_t356221433 ** get_address_of_GemText_5() { return &___GemText_5; }
	inline void set_GemText_5(Text_t356221433 * value)
	{
		___GemText_5 = value;
		Il2CppCodeGenWriteBarrier(&___GemText_5, value);
	}

	inline static int32_t get_offset_of_coinForAnim_6() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___coinForAnim_6)); }
	inline int32_t get_coinForAnim_6() const { return ___coinForAnim_6; }
	inline int32_t* get_address_of_coinForAnim_6() { return &___coinForAnim_6; }
	inline void set_coinForAnim_6(int32_t value)
	{
		___coinForAnim_6 = value;
	}

	inline static int32_t get_offset_of_gemForAnim_7() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___gemForAnim_7)); }
	inline int32_t get_gemForAnim_7() const { return ___gemForAnim_7; }
	inline int32_t* get_address_of_gemForAnim_7() { return &___gemForAnim_7; }
	inline void set_gemForAnim_7(int32_t value)
	{
		___gemForAnim_7 = value;
	}

	inline static int32_t get_offset_of_coinForAnimProgress_8() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___coinForAnimProgress_8)); }
	inline int32_t get_coinForAnimProgress_8() const { return ___coinForAnimProgress_8; }
	inline int32_t* get_address_of_coinForAnimProgress_8() { return &___coinForAnimProgress_8; }
	inline void set_coinForAnimProgress_8(int32_t value)
	{
		___coinForAnimProgress_8 = value;
	}

	inline static int32_t get_offset_of_gemForAnimProgress_9() { return static_cast<int32_t>(offsetof(UserCashView_t152063903, ___gemForAnimProgress_9)); }
	inline int32_t get_gemForAnimProgress_9() const { return ___gemForAnimProgress_9; }
	inline int32_t* get_address_of_gemForAnimProgress_9() { return &___gemForAnimProgress_9; }
	inline void set_gemForAnimProgress_9(int32_t value)
	{
		___gemForAnimProgress_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
