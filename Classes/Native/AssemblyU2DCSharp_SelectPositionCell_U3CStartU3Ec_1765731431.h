﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SelectPositionCell
struct SelectPositionCell_t908287989;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectPositionCell/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t1765731431  : public Il2CppObject
{
public:
	// System.Int32 SelectPositionCell/<Start>c__AnonStorey0::rowIndex
	int32_t ___rowIndex_0;
	// SelectPositionCell SelectPositionCell/<Start>c__AnonStorey0::$this
	SelectPositionCell_t908287989 * ___U24this_1;

public:
	inline static int32_t get_offset_of_rowIndex_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1765731431, ___rowIndex_0)); }
	inline int32_t get_rowIndex_0() const { return ___rowIndex_0; }
	inline int32_t* get_address_of_rowIndex_0() { return &___rowIndex_0; }
	inline void set_rowIndex_0(int32_t value)
	{
		___rowIndex_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1765731431, ___U24this_1)); }
	inline SelectPositionCell_t908287989 * get_U24this_1() const { return ___U24this_1; }
	inline SelectPositionCell_t908287989 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SelectPositionCell_t908287989 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
