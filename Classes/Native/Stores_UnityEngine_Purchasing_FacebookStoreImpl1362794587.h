﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_JSONStore1890359403.h"

// UnityEngine.Purchasing.INativeFacebookStore
struct INativeFacebookStore_t3367400535;
// Uniject.IUtil
struct IUtil_t2188430191;
// UnityEngine.Purchasing.FacebookStoreImpl
struct FacebookStoreImpl_t1362794587;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FacebookStoreImpl
struct  FacebookStoreImpl_t1362794587  : public JSONStore_t1890359403
{
public:
	// UnityEngine.Purchasing.INativeFacebookStore UnityEngine.Purchasing.FacebookStoreImpl::m_Native
	Il2CppObject * ___m_Native_22;

public:
	inline static int32_t get_offset_of_m_Native_22() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t1362794587, ___m_Native_22)); }
	inline Il2CppObject * get_m_Native_22() const { return ___m_Native_22; }
	inline Il2CppObject ** get_address_of_m_Native_22() { return &___m_Native_22; }
	inline void set_m_Native_22(Il2CppObject * value)
	{
		___m_Native_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_Native_22, value);
	}
};

struct FacebookStoreImpl_t1362794587_StaticFields
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.FacebookStoreImpl::util
	Il2CppObject * ___util_23;
	// UnityEngine.Purchasing.FacebookStoreImpl UnityEngine.Purchasing.FacebookStoreImpl::instance
	FacebookStoreImpl_t1362794587 * ___instance_24;

public:
	inline static int32_t get_offset_of_util_23() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t1362794587_StaticFields, ___util_23)); }
	inline Il2CppObject * get_util_23() const { return ___util_23; }
	inline Il2CppObject ** get_address_of_util_23() { return &___util_23; }
	inline void set_util_23(Il2CppObject * value)
	{
		___util_23 = value;
		Il2CppCodeGenWriteBarrier(&___util_23, value);
	}

	inline static int32_t get_offset_of_instance_24() { return static_cast<int32_t>(offsetof(FacebookStoreImpl_t1362794587_StaticFields, ___instance_24)); }
	inline FacebookStoreImpl_t1362794587 * get_instance_24() const { return ___instance_24; }
	inline FacebookStoreImpl_t1362794587 ** get_address_of_instance_24() { return &___instance_24; }
	inline void set_instance_24(FacebookStoreImpl_t1362794587 * value)
	{
		___instance_24 = value;
		Il2CppCodeGenWriteBarrier(&___instance_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
