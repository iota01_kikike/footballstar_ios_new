﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare_Example
struct  NativeShare_Example_t211902429  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D NativeShare_Example::m_texture
	Texture2D_t3542995729 * ___m_texture_2;
	// UnityEngine.UI.Slider NativeShare_Example::m_slider
	Slider_t297367283 * ___m_slider_3;
	// UnityEngine.EventSystems.EventSystem NativeShare_Example::eventSystem
	EventSystem_t3466835263 * ___eventSystem_4;

public:
	inline static int32_t get_offset_of_m_texture_2() { return static_cast<int32_t>(offsetof(NativeShare_Example_t211902429, ___m_texture_2)); }
	inline Texture2D_t3542995729 * get_m_texture_2() const { return ___m_texture_2; }
	inline Texture2D_t3542995729 ** get_address_of_m_texture_2() { return &___m_texture_2; }
	inline void set_m_texture_2(Texture2D_t3542995729 * value)
	{
		___m_texture_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_texture_2, value);
	}

	inline static int32_t get_offset_of_m_slider_3() { return static_cast<int32_t>(offsetof(NativeShare_Example_t211902429, ___m_slider_3)); }
	inline Slider_t297367283 * get_m_slider_3() const { return ___m_slider_3; }
	inline Slider_t297367283 ** get_address_of_m_slider_3() { return &___m_slider_3; }
	inline void set_m_slider_3(Slider_t297367283 * value)
	{
		___m_slider_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_slider_3, value);
	}

	inline static int32_t get_offset_of_eventSystem_4() { return static_cast<int32_t>(offsetof(NativeShare_Example_t211902429, ___eventSystem_4)); }
	inline EventSystem_t3466835263 * get_eventSystem_4() const { return ___eventSystem_4; }
	inline EventSystem_t3466835263 ** get_address_of_eventSystem_4() { return &___eventSystem_4; }
	inline void set_eventSystem_4(EventSystem_t3466835263 * value)
	{
		___eventSystem_4 = value;
		Il2CppCodeGenWriteBarrier(&___eventSystem_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
