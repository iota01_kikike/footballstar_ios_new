﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_ScoutCellType764865946.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellData
struct  ScoutCellData_t2122787786 
{
public:
	// System.Int32 ScoutCellData::Index
	int32_t ___Index_0;
	// System.String ScoutCellData::Key
	String_t* ___Key_1;
	// System.Single ScoutCellData::CellHeight
	float ___CellHeight_2;
	// ScoutCellType ScoutCellData::CellType
	int32_t ___CellType_3;
	// System.String ScoutCellData::Subject
	String_t* ___Subject_4;
	// System.String ScoutCellData::Subtitle
	String_t* ___Subtitle_5;
	// System.String ScoutCellData::CostText
	String_t* ___CostText_6;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Key_1() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___Key_1)); }
	inline String_t* get_Key_1() const { return ___Key_1; }
	inline String_t** get_address_of_Key_1() { return &___Key_1; }
	inline void set_Key_1(String_t* value)
	{
		___Key_1 = value;
		Il2CppCodeGenWriteBarrier(&___Key_1, value);
	}

	inline static int32_t get_offset_of_CellHeight_2() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___CellHeight_2)); }
	inline float get_CellHeight_2() const { return ___CellHeight_2; }
	inline float* get_address_of_CellHeight_2() { return &___CellHeight_2; }
	inline void set_CellHeight_2(float value)
	{
		___CellHeight_2 = value;
	}

	inline static int32_t get_offset_of_CellType_3() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___CellType_3)); }
	inline int32_t get_CellType_3() const { return ___CellType_3; }
	inline int32_t* get_address_of_CellType_3() { return &___CellType_3; }
	inline void set_CellType_3(int32_t value)
	{
		___CellType_3 = value;
	}

	inline static int32_t get_offset_of_Subject_4() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___Subject_4)); }
	inline String_t* get_Subject_4() const { return ___Subject_4; }
	inline String_t** get_address_of_Subject_4() { return &___Subject_4; }
	inline void set_Subject_4(String_t* value)
	{
		___Subject_4 = value;
		Il2CppCodeGenWriteBarrier(&___Subject_4, value);
	}

	inline static int32_t get_offset_of_Subtitle_5() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___Subtitle_5)); }
	inline String_t* get_Subtitle_5() const { return ___Subtitle_5; }
	inline String_t** get_address_of_Subtitle_5() { return &___Subtitle_5; }
	inline void set_Subtitle_5(String_t* value)
	{
		___Subtitle_5 = value;
		Il2CppCodeGenWriteBarrier(&___Subtitle_5, value);
	}

	inline static int32_t get_offset_of_CostText_6() { return static_cast<int32_t>(offsetof(ScoutCellData_t2122787786, ___CostText_6)); }
	inline String_t* get_CostText_6() const { return ___CostText_6; }
	inline String_t** get_address_of_CostText_6() { return &___CostText_6; }
	inline void set_CostText_6(String_t* value)
	{
		___CostText_6 = value;
		Il2CppCodeGenWriteBarrier(&___CostText_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ScoutCellData
struct ScoutCellData_t2122787786_marshaled_pinvoke
{
	int32_t ___Index_0;
	char* ___Key_1;
	float ___CellHeight_2;
	int32_t ___CellType_3;
	char* ___Subject_4;
	char* ___Subtitle_5;
	char* ___CostText_6;
};
// Native definition for COM marshalling of ScoutCellData
struct ScoutCellData_t2122787786_marshaled_com
{
	int32_t ___Index_0;
	Il2CppChar* ___Key_1;
	float ___CellHeight_2;
	int32_t ___CellType_3;
	Il2CppChar* ___Subject_4;
	Il2CppChar* ___Subtitle_5;
	Il2CppChar* ___CostText_6;
};
