﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// DBManager
struct DBManager_t2841981931;
// SqliteDatabase
struct SqliteDatabase_t84303685;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DBManager
struct  DBManager_t2841981931  : public MonoBehaviour_t1158329972
{
public:
	// System.String DBManager::DB_NAME
	String_t* ___DB_NAME_2;
	// System.String DBManager::DB_SECRET_KEY
	String_t* ___DB_SECRET_KEY_3;
	// SqliteDatabase DBManager::_db
	SqliteDatabase_t84303685 * ____db_5;

public:
	inline static int32_t get_offset_of_DB_NAME_2() { return static_cast<int32_t>(offsetof(DBManager_t2841981931, ___DB_NAME_2)); }
	inline String_t* get_DB_NAME_2() const { return ___DB_NAME_2; }
	inline String_t** get_address_of_DB_NAME_2() { return &___DB_NAME_2; }
	inline void set_DB_NAME_2(String_t* value)
	{
		___DB_NAME_2 = value;
		Il2CppCodeGenWriteBarrier(&___DB_NAME_2, value);
	}

	inline static int32_t get_offset_of_DB_SECRET_KEY_3() { return static_cast<int32_t>(offsetof(DBManager_t2841981931, ___DB_SECRET_KEY_3)); }
	inline String_t* get_DB_SECRET_KEY_3() const { return ___DB_SECRET_KEY_3; }
	inline String_t** get_address_of_DB_SECRET_KEY_3() { return &___DB_SECRET_KEY_3; }
	inline void set_DB_SECRET_KEY_3(String_t* value)
	{
		___DB_SECRET_KEY_3 = value;
		Il2CppCodeGenWriteBarrier(&___DB_SECRET_KEY_3, value);
	}

	inline static int32_t get_offset_of__db_5() { return static_cast<int32_t>(offsetof(DBManager_t2841981931, ____db_5)); }
	inline SqliteDatabase_t84303685 * get__db_5() const { return ____db_5; }
	inline SqliteDatabase_t84303685 ** get_address_of__db_5() { return &____db_5; }
	inline void set__db_5(SqliteDatabase_t84303685 * value)
	{
		____db_5 = value;
		Il2CppCodeGenWriteBarrier(&____db_5, value);
	}
};

struct DBManager_t2841981931_StaticFields
{
public:
	// DBManager DBManager::_instance
	DBManager_t2841981931 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(DBManager_t2841981931_StaticFields, ____instance_4)); }
	inline DBManager_t2841981931 * get__instance_4() const { return ____instance_4; }
	inline DBManager_t2841981931 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(DBManager_t2841981931 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
