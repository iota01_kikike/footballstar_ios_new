﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ScoutCell
struct ScoutCell_t2879086938;
// Tacticsoft.TableView
struct TableView_t3179510217;
// System.Collections.Generic.List`1<ScoutCellData>
struct List_1_t1491908918;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutTableViewController
struct  ScoutTableViewController_t2445029075  : public MonoBehaviour_t1158329972
{
public:
	// ScoutCell ScoutTableViewController::CellPrefab
	ScoutCell_t2879086938 * ___CellPrefab_2;
	// Tacticsoft.TableView ScoutTableViewController::TableView
	TableView_t3179510217 * ___TableView_3;
	// System.Int32 ScoutTableViewController::_numInstancesCreated
	int32_t ____numInstancesCreated_4;
	// System.Collections.Generic.List`1<ScoutCellData> ScoutTableViewController::DataList
	List_1_t1491908918 * ___DataList_5;

public:
	inline static int32_t get_offset_of_CellPrefab_2() { return static_cast<int32_t>(offsetof(ScoutTableViewController_t2445029075, ___CellPrefab_2)); }
	inline ScoutCell_t2879086938 * get_CellPrefab_2() const { return ___CellPrefab_2; }
	inline ScoutCell_t2879086938 ** get_address_of_CellPrefab_2() { return &___CellPrefab_2; }
	inline void set_CellPrefab_2(ScoutCell_t2879086938 * value)
	{
		___CellPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___CellPrefab_2, value);
	}

	inline static int32_t get_offset_of_TableView_3() { return static_cast<int32_t>(offsetof(ScoutTableViewController_t2445029075, ___TableView_3)); }
	inline TableView_t3179510217 * get_TableView_3() const { return ___TableView_3; }
	inline TableView_t3179510217 ** get_address_of_TableView_3() { return &___TableView_3; }
	inline void set_TableView_3(TableView_t3179510217 * value)
	{
		___TableView_3 = value;
		Il2CppCodeGenWriteBarrier(&___TableView_3, value);
	}

	inline static int32_t get_offset_of__numInstancesCreated_4() { return static_cast<int32_t>(offsetof(ScoutTableViewController_t2445029075, ____numInstancesCreated_4)); }
	inline int32_t get__numInstancesCreated_4() const { return ____numInstancesCreated_4; }
	inline int32_t* get_address_of__numInstancesCreated_4() { return &____numInstancesCreated_4; }
	inline void set__numInstancesCreated_4(int32_t value)
	{
		____numInstancesCreated_4 = value;
	}

	inline static int32_t get_offset_of_DataList_5() { return static_cast<int32_t>(offsetof(ScoutTableViewController_t2445029075, ___DataList_5)); }
	inline List_1_t1491908918 * get_DataList_5() const { return ___DataList_5; }
	inline List_1_t1491908918 ** get_address_of_DataList_5() { return &___DataList_5; }
	inline void set_DataList_5(List_1_t1491908918 * value)
	{
		___DataList_5 = value;
		Il2CppCodeGenWriteBarrier(&___DataList_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
