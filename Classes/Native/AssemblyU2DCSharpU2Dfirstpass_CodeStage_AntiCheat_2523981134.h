﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte16
struct  ACTkByte16_t2523981134 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b8
	uint8_t ___b8_7;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b9
	uint8_t ___b9_8;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b10
	uint8_t ___b10_9;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b11
	uint8_t ___b11_10;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b12
	uint8_t ___b12_11;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b13
	uint8_t ___b13_12;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b14
	uint8_t ___b14_13;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b15
	uint8_t ___b15_14;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b16
	uint8_t ___b16_15;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}

	inline static int32_t get_offset_of_b9_8() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b9_8)); }
	inline uint8_t get_b9_8() const { return ___b9_8; }
	inline uint8_t* get_address_of_b9_8() { return &___b9_8; }
	inline void set_b9_8(uint8_t value)
	{
		___b9_8 = value;
	}

	inline static int32_t get_offset_of_b10_9() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b10_9)); }
	inline uint8_t get_b10_9() const { return ___b10_9; }
	inline uint8_t* get_address_of_b10_9() { return &___b10_9; }
	inline void set_b10_9(uint8_t value)
	{
		___b10_9 = value;
	}

	inline static int32_t get_offset_of_b11_10() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b11_10)); }
	inline uint8_t get_b11_10() const { return ___b11_10; }
	inline uint8_t* get_address_of_b11_10() { return &___b11_10; }
	inline void set_b11_10(uint8_t value)
	{
		___b11_10 = value;
	}

	inline static int32_t get_offset_of_b12_11() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b12_11)); }
	inline uint8_t get_b12_11() const { return ___b12_11; }
	inline uint8_t* get_address_of_b12_11() { return &___b12_11; }
	inline void set_b12_11(uint8_t value)
	{
		___b12_11 = value;
	}

	inline static int32_t get_offset_of_b13_12() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b13_12)); }
	inline uint8_t get_b13_12() const { return ___b13_12; }
	inline uint8_t* get_address_of_b13_12() { return &___b13_12; }
	inline void set_b13_12(uint8_t value)
	{
		___b13_12 = value;
	}

	inline static int32_t get_offset_of_b14_13() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b14_13)); }
	inline uint8_t get_b14_13() const { return ___b14_13; }
	inline uint8_t* get_address_of_b14_13() { return &___b14_13; }
	inline void set_b14_13(uint8_t value)
	{
		___b14_13 = value;
	}

	inline static int32_t get_offset_of_b15_14() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b15_14)); }
	inline uint8_t get_b15_14() const { return ___b15_14; }
	inline uint8_t* get_address_of_b15_14() { return &___b15_14; }
	inline void set_b15_14(uint8_t value)
	{
		___b15_14 = value;
	}

	inline static int32_t get_offset_of_b16_15() { return static_cast<int32_t>(offsetof(ACTkByte16_t2523981134, ___b16_15)); }
	inline uint8_t get_b16_15() const { return ___b16_15; }
	inline uint8_t* get_address_of_b16_15() { return &___b16_15; }
	inline void set_b16_15(uint8_t value)
	{
		___b16_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
