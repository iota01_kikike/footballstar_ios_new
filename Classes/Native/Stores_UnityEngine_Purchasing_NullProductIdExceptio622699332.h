﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "Stores_UnityEngine_Purchasing_ReceiptParserExceptio479172278.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.NullProductIdException
struct  NullProductIdException_t622699332  : public ReceiptParserException_t479172278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
