﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AdvancedFP2350381201.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData
struct  DeviceInfoCounterData_t1356006596  : public BaseCounterData_t2350381201
{
public:
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::platform
	bool ___platform_14;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::cpuModel
	bool ___cpuModel_15;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::gpuModel
	bool ___gpuModel_16;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::gpuApi
	bool ___gpuApi_17;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::gpuSpec
	bool ___gpuSpec_18;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::ramSize
	bool ___ramSize_19;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::screenData
	bool ___screenData_20;
	// System.Boolean CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::deviceModel
	bool ___deviceModel_21;
	// System.String CodeStage.AdvancedFPSCounter.CountersData.DeviceInfoCounterData::<LastValue>k__BackingField
	String_t* ___U3CLastValueU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_platform_14() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___platform_14)); }
	inline bool get_platform_14() const { return ___platform_14; }
	inline bool* get_address_of_platform_14() { return &___platform_14; }
	inline void set_platform_14(bool value)
	{
		___platform_14 = value;
	}

	inline static int32_t get_offset_of_cpuModel_15() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___cpuModel_15)); }
	inline bool get_cpuModel_15() const { return ___cpuModel_15; }
	inline bool* get_address_of_cpuModel_15() { return &___cpuModel_15; }
	inline void set_cpuModel_15(bool value)
	{
		___cpuModel_15 = value;
	}

	inline static int32_t get_offset_of_gpuModel_16() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___gpuModel_16)); }
	inline bool get_gpuModel_16() const { return ___gpuModel_16; }
	inline bool* get_address_of_gpuModel_16() { return &___gpuModel_16; }
	inline void set_gpuModel_16(bool value)
	{
		___gpuModel_16 = value;
	}

	inline static int32_t get_offset_of_gpuApi_17() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___gpuApi_17)); }
	inline bool get_gpuApi_17() const { return ___gpuApi_17; }
	inline bool* get_address_of_gpuApi_17() { return &___gpuApi_17; }
	inline void set_gpuApi_17(bool value)
	{
		___gpuApi_17 = value;
	}

	inline static int32_t get_offset_of_gpuSpec_18() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___gpuSpec_18)); }
	inline bool get_gpuSpec_18() const { return ___gpuSpec_18; }
	inline bool* get_address_of_gpuSpec_18() { return &___gpuSpec_18; }
	inline void set_gpuSpec_18(bool value)
	{
		___gpuSpec_18 = value;
	}

	inline static int32_t get_offset_of_ramSize_19() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___ramSize_19)); }
	inline bool get_ramSize_19() const { return ___ramSize_19; }
	inline bool* get_address_of_ramSize_19() { return &___ramSize_19; }
	inline void set_ramSize_19(bool value)
	{
		___ramSize_19 = value;
	}

	inline static int32_t get_offset_of_screenData_20() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___screenData_20)); }
	inline bool get_screenData_20() const { return ___screenData_20; }
	inline bool* get_address_of_screenData_20() { return &___screenData_20; }
	inline void set_screenData_20(bool value)
	{
		___screenData_20 = value;
	}

	inline static int32_t get_offset_of_deviceModel_21() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___deviceModel_21)); }
	inline bool get_deviceModel_21() const { return ___deviceModel_21; }
	inline bool* get_address_of_deviceModel_21() { return &___deviceModel_21; }
	inline void set_deviceModel_21(bool value)
	{
		___deviceModel_21 = value;
	}

	inline static int32_t get_offset_of_U3CLastValueU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(DeviceInfoCounterData_t1356006596, ___U3CLastValueU3Ek__BackingField_22)); }
	inline String_t* get_U3CLastValueU3Ek__BackingField_22() const { return ___U3CLastValueU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CLastValueU3Ek__BackingField_22() { return &___U3CLastValueU3Ek__BackingField_22; }
	inline void set_U3CLastValueU3Ek__BackingField_22(String_t* value)
	{
		___U3CLastValueU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLastValueU3Ek__BackingField_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
