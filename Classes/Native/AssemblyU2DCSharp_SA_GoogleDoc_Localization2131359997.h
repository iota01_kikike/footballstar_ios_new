﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action
struct Action_t3226471752;
// SA.GoogleDoc.LocalizationClient
struct LocalizationClient_t2248676220;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.Localization
struct  Localization_t2131359997  : public Il2CppObject
{
public:

public:
};

struct Localization_t2131359997_StaticFields
{
public:
	// System.Action SA.GoogleDoc.Localization::OnLanguageChanged
	Action_t3226471752 * ___OnLanguageChanged_0;
	// SA.GoogleDoc.LocalizationClient SA.GoogleDoc.Localization::_Client
	LocalizationClient_t2248676220 * ____Client_1;

public:
	inline static int32_t get_offset_of_OnLanguageChanged_0() { return static_cast<int32_t>(offsetof(Localization_t2131359997_StaticFields, ___OnLanguageChanged_0)); }
	inline Action_t3226471752 * get_OnLanguageChanged_0() const { return ___OnLanguageChanged_0; }
	inline Action_t3226471752 ** get_address_of_OnLanguageChanged_0() { return &___OnLanguageChanged_0; }
	inline void set_OnLanguageChanged_0(Action_t3226471752 * value)
	{
		___OnLanguageChanged_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnLanguageChanged_0, value);
	}

	inline static int32_t get_offset_of__Client_1() { return static_cast<int32_t>(offsetof(Localization_t2131359997_StaticFields, ____Client_1)); }
	inline LocalizationClient_t2248676220 * get__Client_1() const { return ____Client_1; }
	inline LocalizationClient_t2248676220 ** get_address_of__Client_1() { return &____Client_1; }
	inline void set__Client_1(LocalizationClient_t2248676220 * value)
	{
		____Client_1 = value;
		Il2CppCodeGenWriteBarrier(&____Client_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
