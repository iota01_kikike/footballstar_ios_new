﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"

// User
struct User_t719925459;
// ScoutCellMultiView
struct ScoutCellMultiView_t1304856306;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoutCellMultiView/<OnClickButton>c__AnonStorey3
struct  U3COnClickButtonU3Ec__AnonStorey3_t654273980  : public Il2CppObject
{
public:
	// User ScoutCellMultiView/<OnClickButton>c__AnonStorey3::user
	User_t719925459 * ___user_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt ScoutCellMultiView/<OnClickButton>c__AnonStorey3::key
	ObscuredInt_t796441056  ___key_1;
	// ScoutCellMultiView ScoutCellMultiView/<OnClickButton>c__AnonStorey3::$this
	ScoutCellMultiView_t1304856306 * ___U24this_2;

public:
	inline static int32_t get_offset_of_user_0() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey3_t654273980, ___user_0)); }
	inline User_t719925459 * get_user_0() const { return ___user_0; }
	inline User_t719925459 ** get_address_of_user_0() { return &___user_0; }
	inline void set_user_0(User_t719925459 * value)
	{
		___user_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_0, value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey3_t654273980, ___key_1)); }
	inline ObscuredInt_t796441056  get_key_1() const { return ___key_1; }
	inline ObscuredInt_t796441056 * get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(ObscuredInt_t796441056  value)
	{
		___key_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnClickButtonU3Ec__AnonStorey3_t654273980, ___U24this_2)); }
	inline ScoutCellMultiView_t1304856306 * get_U24this_2() const { return ___U24this_2; }
	inline ScoutCellMultiView_t1304856306 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ScoutCellMultiView_t1304856306 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
