﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SA_GoogleDoc_RanageDirection1288290907.h"

// SA.GoogleDoc.Cell
struct Cell_t3987632660;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SA.GoogleDoc.CellRange
struct  CellRange_t4079182527  : public Il2CppObject
{
public:
	// SA.GoogleDoc.RanageDirection SA.GoogleDoc.CellRange::direction
	int32_t ___direction_0;
	// SA.GoogleDoc.Cell SA.GoogleDoc.CellRange::StartCell
	Cell_t3987632660 * ___StartCell_1;
	// System.Boolean SA.GoogleDoc.CellRange::useLinebreak
	bool ___useLinebreak_2;
	// System.Int32 SA.GoogleDoc.CellRange::LineLength
	int32_t ___LineLength_3;

public:
	inline static int32_t get_offset_of_direction_0() { return static_cast<int32_t>(offsetof(CellRange_t4079182527, ___direction_0)); }
	inline int32_t get_direction_0() const { return ___direction_0; }
	inline int32_t* get_address_of_direction_0() { return &___direction_0; }
	inline void set_direction_0(int32_t value)
	{
		___direction_0 = value;
	}

	inline static int32_t get_offset_of_StartCell_1() { return static_cast<int32_t>(offsetof(CellRange_t4079182527, ___StartCell_1)); }
	inline Cell_t3987632660 * get_StartCell_1() const { return ___StartCell_1; }
	inline Cell_t3987632660 ** get_address_of_StartCell_1() { return &___StartCell_1; }
	inline void set_StartCell_1(Cell_t3987632660 * value)
	{
		___StartCell_1 = value;
		Il2CppCodeGenWriteBarrier(&___StartCell_1, value);
	}

	inline static int32_t get_offset_of_useLinebreak_2() { return static_cast<int32_t>(offsetof(CellRange_t4079182527, ___useLinebreak_2)); }
	inline bool get_useLinebreak_2() const { return ___useLinebreak_2; }
	inline bool* get_address_of_useLinebreak_2() { return &___useLinebreak_2; }
	inline void set_useLinebreak_2(bool value)
	{
		___useLinebreak_2 = value;
	}

	inline static int32_t get_offset_of_LineLength_3() { return static_cast<int32_t>(offsetof(CellRange_t4079182527, ___LineLength_3)); }
	inline int32_t get_LineLength_3() const { return ___LineLength_3; }
	inline int32_t* get_address_of_LineLength_3() { return &___LineLength_3; }
	inline void set_LineLength_3(int32_t value)
	{
		___LineLength_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
