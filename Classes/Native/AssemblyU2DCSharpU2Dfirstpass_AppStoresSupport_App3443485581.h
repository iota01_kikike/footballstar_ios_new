﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

// System.String
struct String_t;
// AppStoresSupport.AppStoreSetting
struct AppStoreSetting_t4246608350;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSettings
struct  AppStoreSettings_t3443485581  : public ScriptableObject_t1975622470
{
public:
	// System.String AppStoresSupport.AppStoreSettings::UnityClientID
	String_t* ___UnityClientID_2;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientKey
	String_t* ___UnityClientKey_3;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientRSAPublicKey
	String_t* ___UnityClientRSAPublicKey_4;
	// AppStoresSupport.AppStoreSetting AppStoresSupport.AppStoreSettings::XiaomiAppStoreSetting
	AppStoreSetting_t4246608350 * ___XiaomiAppStoreSetting_5;

public:
	inline static int32_t get_offset_of_UnityClientID_2() { return static_cast<int32_t>(offsetof(AppStoreSettings_t3443485581, ___UnityClientID_2)); }
	inline String_t* get_UnityClientID_2() const { return ___UnityClientID_2; }
	inline String_t** get_address_of_UnityClientID_2() { return &___UnityClientID_2; }
	inline void set_UnityClientID_2(String_t* value)
	{
		___UnityClientID_2 = value;
		Il2CppCodeGenWriteBarrier(&___UnityClientID_2, value);
	}

	inline static int32_t get_offset_of_UnityClientKey_3() { return static_cast<int32_t>(offsetof(AppStoreSettings_t3443485581, ___UnityClientKey_3)); }
	inline String_t* get_UnityClientKey_3() const { return ___UnityClientKey_3; }
	inline String_t** get_address_of_UnityClientKey_3() { return &___UnityClientKey_3; }
	inline void set_UnityClientKey_3(String_t* value)
	{
		___UnityClientKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___UnityClientKey_3, value);
	}

	inline static int32_t get_offset_of_UnityClientRSAPublicKey_4() { return static_cast<int32_t>(offsetof(AppStoreSettings_t3443485581, ___UnityClientRSAPublicKey_4)); }
	inline String_t* get_UnityClientRSAPublicKey_4() const { return ___UnityClientRSAPublicKey_4; }
	inline String_t** get_address_of_UnityClientRSAPublicKey_4() { return &___UnityClientRSAPublicKey_4; }
	inline void set_UnityClientRSAPublicKey_4(String_t* value)
	{
		___UnityClientRSAPublicKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___UnityClientRSAPublicKey_4, value);
	}

	inline static int32_t get_offset_of_XiaomiAppStoreSetting_5() { return static_cast<int32_t>(offsetof(AppStoreSettings_t3443485581, ___XiaomiAppStoreSetting_5)); }
	inline AppStoreSetting_t4246608350 * get_XiaomiAppStoreSetting_5() const { return ___XiaomiAppStoreSetting_5; }
	inline AppStoreSetting_t4246608350 ** get_address_of_XiaomiAppStoreSetting_5() { return &___XiaomiAppStoreSetting_5; }
	inline void set_XiaomiAppStoreSetting_5(AppStoreSetting_t4246608350 * value)
	{
		___XiaomiAppStoreSetting_5 = value;
		Il2CppCodeGenWriteBarrier(&___XiaomiAppStoreSetting_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
