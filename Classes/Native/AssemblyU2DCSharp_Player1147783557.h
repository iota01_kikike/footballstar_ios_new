﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Person3241917763.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CodeStage_AntiCheat_O796441056.h"

// CodeStage.AntiCheat.ObscuredTypes.ObscuredString
struct ObscuredString_t692732302;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t1147783557  : public Person_t3241917763
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Player::No
	ObscuredInt_t796441056  ___No_0;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Player::ModelNo
	ObscuredInt_t796441056  ___ModelNo_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Player::NationNo
	ObscuredInt_t796441056  ___NationNo_2;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Player::Name
	ObscuredString_t692732302 * ___Name_3;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Player::ShortName
	ObscuredString_t692732302 * ___ShortName_4;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Player::Skin
	ObscuredString_t692732302 * ___Skin_5;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Player::BackNumber
	ObscuredInt_t796441056  ___BackNumber_6;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Player::Position
	ObscuredString_t692732302 * ___Position_7;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString Player::Grade
	ObscuredString_t692732302 * ___Grade_8;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt Player::Overall
	ObscuredInt_t796441056  ___Overall_9;

public:
	inline static int32_t get_offset_of_No_0() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___No_0)); }
	inline ObscuredInt_t796441056  get_No_0() const { return ___No_0; }
	inline ObscuredInt_t796441056 * get_address_of_No_0() { return &___No_0; }
	inline void set_No_0(ObscuredInt_t796441056  value)
	{
		___No_0 = value;
	}

	inline static int32_t get_offset_of_ModelNo_1() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___ModelNo_1)); }
	inline ObscuredInt_t796441056  get_ModelNo_1() const { return ___ModelNo_1; }
	inline ObscuredInt_t796441056 * get_address_of_ModelNo_1() { return &___ModelNo_1; }
	inline void set_ModelNo_1(ObscuredInt_t796441056  value)
	{
		___ModelNo_1 = value;
	}

	inline static int32_t get_offset_of_NationNo_2() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___NationNo_2)); }
	inline ObscuredInt_t796441056  get_NationNo_2() const { return ___NationNo_2; }
	inline ObscuredInt_t796441056 * get_address_of_NationNo_2() { return &___NationNo_2; }
	inline void set_NationNo_2(ObscuredInt_t796441056  value)
	{
		___NationNo_2 = value;
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Name_3)); }
	inline ObscuredString_t692732302 * get_Name_3() const { return ___Name_3; }
	inline ObscuredString_t692732302 ** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(ObscuredString_t692732302 * value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier(&___Name_3, value);
	}

	inline static int32_t get_offset_of_ShortName_4() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___ShortName_4)); }
	inline ObscuredString_t692732302 * get_ShortName_4() const { return ___ShortName_4; }
	inline ObscuredString_t692732302 ** get_address_of_ShortName_4() { return &___ShortName_4; }
	inline void set_ShortName_4(ObscuredString_t692732302 * value)
	{
		___ShortName_4 = value;
		Il2CppCodeGenWriteBarrier(&___ShortName_4, value);
	}

	inline static int32_t get_offset_of_Skin_5() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Skin_5)); }
	inline ObscuredString_t692732302 * get_Skin_5() const { return ___Skin_5; }
	inline ObscuredString_t692732302 ** get_address_of_Skin_5() { return &___Skin_5; }
	inline void set_Skin_5(ObscuredString_t692732302 * value)
	{
		___Skin_5 = value;
		Il2CppCodeGenWriteBarrier(&___Skin_5, value);
	}

	inline static int32_t get_offset_of_BackNumber_6() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___BackNumber_6)); }
	inline ObscuredInt_t796441056  get_BackNumber_6() const { return ___BackNumber_6; }
	inline ObscuredInt_t796441056 * get_address_of_BackNumber_6() { return &___BackNumber_6; }
	inline void set_BackNumber_6(ObscuredInt_t796441056  value)
	{
		___BackNumber_6 = value;
	}

	inline static int32_t get_offset_of_Position_7() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Position_7)); }
	inline ObscuredString_t692732302 * get_Position_7() const { return ___Position_7; }
	inline ObscuredString_t692732302 ** get_address_of_Position_7() { return &___Position_7; }
	inline void set_Position_7(ObscuredString_t692732302 * value)
	{
		___Position_7 = value;
		Il2CppCodeGenWriteBarrier(&___Position_7, value);
	}

	inline static int32_t get_offset_of_Grade_8() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Grade_8)); }
	inline ObscuredString_t692732302 * get_Grade_8() const { return ___Grade_8; }
	inline ObscuredString_t692732302 ** get_address_of_Grade_8() { return &___Grade_8; }
	inline void set_Grade_8(ObscuredString_t692732302 * value)
	{
		___Grade_8 = value;
		Il2CppCodeGenWriteBarrier(&___Grade_8, value);
	}

	inline static int32_t get_offset_of_Overall_9() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Overall_9)); }
	inline ObscuredInt_t796441056  get_Overall_9() const { return ___Overall_9; }
	inline ObscuredInt_t796441056 * get_address_of_Overall_9() { return &___Overall_9; }
	inline void set_Overall_9(ObscuredInt_t796441056  value)
	{
		___Overall_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
